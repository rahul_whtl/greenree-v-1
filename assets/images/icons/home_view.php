<?php $this->load->view('public/templates/header') ?>
<script type="text/javascript">
	 jQuery(document).ready(function() {  
         jQuery(".carousel-inner").swiperight(function() {  
              jQuery(this).parent().carousel('prev');  
                });  
           jQuery(".carousel-inner").swipeleft(function() {  
              jQuery(this).parent().carousel('next');  
       });  
    }); 
</script>
<div class="banner-part">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12 home-slider">
			<?php if ($banners): ?>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="3200">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php foreach ($banners as $key => $banner): ?>
						<li data-target="#carousel-example-generic" data-slide-to="<?php echo $key ?>" class="<?php echo $key ? 'active' : '' ?>"></li>
						<?php endforeach ?>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<?php 
						$first = true;
						foreach ($banners as $key => $banner): ?>
							<div class="item <?php echo $first ? 'active' : '' ?>">
								<a href="<?php echo base_url($banner->url); ?>"><img src="<?php echo base_url($banner->image) ?>" alt="..." ></a>
								<?php $first= false; ?>
								<?php if ($banner->caption): ?>
									<div class="lead carousel-caption">
										<?php echo $banner->caption ?>
									</div>
								<?php endif ?>
								<div class="banner-text">
									<p><?php echo $banner->html; ?></p>	
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			<?php endif ?>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 banner-side-image">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image1">
						<a href="<?php echo base_url('scrap_request_view'); ?>" class="sell_now_image">
							<img src="<?php echo base_url('assets/images/icons/girl-with-mobile.jpg'); ?>" alt="girl-with-mobile">
						</a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image2">
						<a href="<?php echo base_url('shop');  ?>">
							<img src="<?php echo base_url('assets/images/icons/buy_our_products.jpg'); ?>" alt="gift-box">
						</a>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image3">
						<a href="<?php echo base_url('user_products');  ?>">
							<img src="<?php echo base_url('assets/images/icons/preowned_items.jpg'); ?>" alt="billing-machine">
						</a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image4">
						<a href="<?php echo base_url('common_wishlist_view');  ?>">
							<img src="<?php echo base_url('assets/images/icons/wishlist-image.jpg'); ?>" alt="wishlist-image">
						</a>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<div class="about-us">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>About Us</h3>
			</div>
			<div class="col-md-5 col-sm-5 dustbin-box">
				<img src="<?php echo base_url('assets/images/icons/cartonbox-recycling.jpg'); ?>" alt="dustbin-box.img">
			</div>
			<div class="col-md-7 col-sm-7 about-us-text">
				<p>At GreenREE we think that we can keep our planet Green by reusing the limited natural resources to its maximum extent.</p>
				<div class="quote">Our 3R principle is:<p class="quote-text">Reuse and Reduce the Recycle waste.</p></div>
				<p>If you also think that this is a revolutionary step, then <strong>sell your paper waste to us</strong> and see the wonders happening in saving the resources.</p> 
			<div class="about-us-howitworks"><a href="<?php echo base_url('about_us') ?>">How it works....</a></div>	
            <a class="button" href="<?php echo base_url('scrap_request_view') ?>">Sell Now</a>
			</div>
			
		</div>
	</div>
</div>
<div class="the-process">
	<div class="container">
		<div class="row">
				<div class="col-md-5 carton-boxes">
					<h4>Carton Boxes / Paper Scrap:</h4>
            		<p>If you wish to sell your carton Boxes / old Paper, it's very simple. Contact us for picking up your scrap and we shall do the rest.</p>
            		<ul>
            			<li>In our website, click on the <b>Sell Scrap</b> option.</li>
            			<li>Fill out the necessary <b>details</b>.</li>
            			<li>Click on <b>Submit</b> option.</li>
            		</ul>
				</div>
				<div class="col-md-3 the-process-text">
					<p>The<span>Process</span></p>
				</div>
				<div class="col-md-4 sell-paid">
					<h4>Sell and get paid:</h4>
					<p>You will get paid for the scrap collected from your doorstep at best prices. </p>
				</div>
		</div>
		<div class="row">
            	<div class="col-md-4 blue-arrow">
            		
            	</div>
				<div class="col-md-4 pick-up">
					<h4>Pick up:</h4>
					<p>On receiving your order, our executive will come at your scheduled time and date and pick up the necessary carton boxes and scrap papers.</p>
				</div>
				<div class="col-md-4 orange-arrow">
					
				</div>			
		</div>
	</div>
</div>
<div class="reuse-reduce carton-box">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-xs-12 col-sm-12">
				<div class="facts">
					<p><em>If you worry that our future gernerations will have nothing but pollution and contamination everywhere, </em><em style="color: #37C176;">Join GreenRee revolution in our 3R initiative which is, <mark> Reuse and Reduce the Recycle </mark> of waste. Recycling Process can save energy but not Pollution.</em></p>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12 reuse-reduce-logo">
				<div class="reuse-reduce-image">
					<img src="<?php echo base_url('assets/images/icons/reuse-reduce2.jpg'); ?>" alt="reuse-reduce.png">
				</div>
			</div>
		</div>
	</div>
</div>
<?php /* <div class="globe-part">
	<div class="container">
		<div class="row">
			<div class="col-md-12 globe-part-text">
				<p>Let’s make our mother earth a better place to live in! </p>
			</div>
			<div class="row globe-part-content">
				<div class="col-md-6 col-sm-5 globe-part-img">
					<img src="<?php echo base_url('assets/images/icons/globe-image.jpg'); ?>" alt="globe-image">
				</div>
				<div class="col-md-6 col-sm-7 globe-part-list">
					<span>GOING PLASTIC</span><span> FREE</span>
					<ul>
						<li>
							<h4>Ditch bottle beverages</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has been the industry's.</p>
						</li>
						<li>
							<h4>Bring your own bag</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has been the industry's.</p>
						</li>
						<li>
							<h4>buy in bulk</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has been the industry's.</p>
						</li>
						<li>
							<h4>use reusable utensils</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has been the industry's.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div> */ ?>
<div class="reuse-reduce even">
	<div class="container">
		<div class="row">
				<div class="col-md-6 col-xs-12 col-sm-6 reuse-reduce-logo">
					<div class="reuse-reduce-image">
						<img src="<?php echo base_url('assets/images/icons/carton-box-1.jpg'); ?>" alt="Reuse Cartoon Box">
					</div>
				</div>
				<div class="col-md-6 col-xs-12 col-sm-6 printer">
					<div class="printer-part">
						<p class="top">Save<span>Boxes</span></p>
						<p class="middle">Don't Dump it</p>
						<p class="bottom">Regular Process: The carton boxes are bundled, crushed and loaded in big truck, then its melted
	and the boxes are remade.</p>
	<p class="bottom1">GreenREE process: Carton boxes will be segregated based on size quality and condition. Will be sold to different category buyers. Unusable will be sent to recycling plants</p>
	<p class="bottom2"><a href="#">Read Details about our process of reusing carton box </a></p>
					</div>
				</div>
		</div>
	</div>
</div>
<div class="reuse-reduce">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-6 reuse-reduce-logo">
				<div class="reuse-reduce-image">
					<img src="<?php echo base_url('assets/images/icons/old-newspapers-scrap.jpg'); ?>" alt="Reuse Paper">
				</div>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6 printer">
				<div class="printer-part printer-part1">
					<p class="top">Save<span>Paper</span></p>
					<p class="middle">Don't Dump it <span>Reuse it in some way or other.</span></p>
					<p class="bottom">Recycle it at last step. <span>That's what we exactly do at GreenREE</span></p>
					<p class="bottom1"><a href="#">Read Details about our process of reusing newspaper</a></p>
				</div>
			</div>
		</div>
	</div>
</div>				


<?php $this->load->view('public/templates/footer') ?>