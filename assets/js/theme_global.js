jQuery(document).ready(function(){
    function validateEmail(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(String(email).toLowerCase());
    }
	var global_id = jQuery('#global_id').val();
	var tokan = jQuery('#tokan').val();
	var cookiee = jQuery('#cookiee').val();
    jQuery('.close').click(function(event){
      event.preventDefault();
      jQuery("#myForm").css('display','none');
    });
    jQuery('.sell_now').click(function(event){
        event.preventDefault();
        var pathname = window.location.pathname;
        var array    = pathname.split('/');
        var value    = array[2];
        var user     = global_id;
        // if(value == 'login'){
        //     jQuery('#myForm').css("display", "none");
        // }
        // else{
            if(user == '') {
                jQuery('#myForm').css("display", "block");
                jQuery("#myForm mobile_number").val('');
                jQuery("#myForm otp").val('');
                var txt = '<p></p>';
                jQuery('#myForm .div_danger1,#myForm .div_danger').html(txt);
                jQuery('#myForm .div_danger1 p,#myForm .div_danger p').css('color','white');  
            }
            else
                {
                window.location.href = window.location.origin+'/sell-scrap';
            }
        // }
    });    
    jQuery('.generate_otp').click(function(event){
        event.preventDefault();
        var pathname = window.location.pathname;
        var array    = pathname.split('/');
        var value    = array[2];
        if(value == 'login'){
            jQuery('#myForm').css("display", "none");
        }
        else{
            jQuery('#myForm').css("display", "block");
        }
    });
    jQuery('.form-popup .generate_otp_btn,.form-popup .resend_otp_btn').click(function(){
        var contact_number = jQuery('.form-popup .mobile_number').val();
        var pattern = /^\d+$/;
    	var length = contact_number.toString().length;
    	if(contact_number == '' || contact_number == undefined || !(pattern.test(contact_number)) || length<10 || length >10){
    		var txt = "<p>Please Enter the Valid Contact Number.</p>";
            jQuery('.form-popup .div_danger').html(txt);
            jQuery('.form-popup .div_danger p').css('color','red');
    		return; 
    	}
        // if(contact_number == '' || contact_number == undefined) {
        //     var txt = '<p>Please Enter Mobile Number.</p>';
        //     jQuery('.form-popup .div_danger').html(txt);
        //     jQuery('.form-popup .div_danger p').css('color','red');      
        //     return;
        // }
       
        var txt1 = '';
        jQuery('.form-popup .div_danger').html(txt1);
        jQuery('.form-popup .div_danger p').css('color','#ffffff');   
        jQuery('.form-popup .otp_div').removeClass('hide');
        jQuery('.form-popup .generate_otp_btn').addClass('hide');
        var btn_val = jQuery(this).val(); 
        var req_data = { 
            contact_number: contact_number
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url : window.location.origin+'/mobile_verification',
            data : req_data,
            'success' : function(data) {  //console.log(data);
                var va = jQuery.parseJSON(data);      
                if(va['results'] == 1) {
                    jQuery('.form-popup .submit_otp_btn').attr('value','Submit OTP');
                    jQuery('.form-popup .submit_otp_btn').removeClass('verify_otp');
                    jQuery('.form-popup .submit_otp_btn').addClass('submit_otp');  
                    loginOtp(contact_number);
                }
                else{
                    jQuery('.form-popup .submit_otp_btn').attr('value','Verify OTP');
                    jQuery('.form-popup .submit_otp_btn').addClass('verify_otp');
                    jQuery('.form-popup .submit_otp_btn').removeClass('submit_otp');
                }
                if(btn_val=='Resend OTP')
                    jQuery(".div_danger1").html('<p style="color:#26A65B">OTP sent to your number.</p>');
            },
            'error' : function(request,error)
            {console.log("ram1");
                alert("Request: "+JSON.stringify(request));
            }
        });
    });
    
    jQuery('.inquiry .product_generate_otp_btn,.inquiry .product_resend_otp_btn').click(function(){
        var contact_number = jQuery('.inquiry .mobile_number').val();
        var email          = jQuery('.inquiry .email').val();
        var name           = jQuery('.inquiry .name').val();
        var pattern = /^\d+$/;
    	var pattern_name = /^[a-zA-Z ]*$/;//alert(pattern_name.test(name));
    	var length = contact_number.toString().length;
    	var error = false;
    	if (pattern_name.test(name) == false) {
    	    error = true;
    	    var p = '<p>Please enter only Character.</p>';
    	    jQuery('.text-danger-name').html(p);
    	    jQuery('.inquiry .text-danger-name p').css('color','#a94442');
        }
        if (name == '' || name == undefined) {
    	    error = true;
    	    var p = '<p>Please enter your name.</p>';
    	    jQuery('.text-danger-name').html(p);
    	    jQuery('.inquiry .text-danger-name p').css('color','#a94442');
        }
        if(!validateEmail(email)){
		    //jQuery(".form-group-email").addClass("has-error");
    		var p = "<p>Please enter valid email id.</p>"
            jQuery(".inquiry .text-danger-email").html(p);                
            jQuery('.inquiry .text-danger-email p').css('color','#a94442');
            error = true;
		}
        if (email == '' || email == undefined) {
            error = true;
            var p = '<p>Please enter email id.</p>';
    	    jQuery('.inquiry .text-danger-email').html(p);             
            jQuery('.inquiry .text-danger-email p').css('color','#a94442');
        }
    	if (!(pattern.test(contact_number)) || length<10 || length >10) {
            error = true;
            var p = '<p>Please enter valid phone number.</p>';
    	    jQuery('.inquiry .text-danger-phone').html(p);
    	    jQuery('.inquiry .text-danger-phone p').css('color','#a94442');
        }
        if (contact_number == '' || contact_number == undefined) {
            error = true;
            var p = '<p>Please enter phone number.</p>';
    	    jQuery('.inquiry .text-danger-phone').html(p);
    	    jQuery('.inquiry .text-danger-phone p').css('color','#a94442');
        }
        if(error){
            return false;
        }
        
        
        // if(name == '' || name == undefined ) {
        //     if(email == '' || email == undefined ) {
        //         var txt = '<p>Please Enter Email Address.</p>';
        //         jQuery('.inquiry .text-danger-email').html(txt);
        //         jQuery('.inquiry .text-danger-email p').css('color','red');
        //     }
        //     else{
        //         var txt = ' ';
        //         jQuery('.inquiry .text-danger-email').html(txt);
        //         jQuery('.inquiry .text-danger-email p').css('color','#fff');
        //     }
        //     if(contact_number == '' || contact_number == undefined ) {
        //         var txt = '<p>Please Enter Mobile Number.</p>';
        //         jQuery('.inquiry .text-danger-phone').html(txt);
        //         jQuery('.inquiry .text-danger-phone p').css('color','red');
        //     }
        //     else{
        //         var txt = ' ';
        //         jQuery('.inquiry .text-danger-phone').html(txt);
        //         jQuery('.inquiry .text-danger-phone p').css('color','#fff');
        //     }
        //     var txt = '<p>Please Enter Name.</p>';
        //     jQuery('.inquiry .text-danger-name').html(txt);
        //     jQuery('.inquiry .text-danger-name p').css('color','red');      
        //     return;
        // }
        // else{
        //     var txt = ' ';
        //     jQuery('.inquiry .text-danger-name').html(txt);
        //     jQuery('.inquiry .text-danger-name p').css('color','#fff');
        // }
        // if(email == '' || email == undefined ) {
        //     if(contact_number == '' || contact_number == undefined ) {
        //         var txt = '<p>Please Enter Mobile Number.</p>';
        //         jQuery('.inquiry .text-danger-phone').html(txt);
        //         jQuery('.inquiry .text-danger-phone p').css('color','red');    
        //     }
        //     else{
        //         var txt = ' ';
        //         jQuery('.inquiry .text-danger-phone').html(txt);
        //         jQuery('.inquiry .text-danger-phone p').css('color','#fff');
        //     }
        //     var txt = '<p>Please Enter Email Address.</p>';
        //     jQuery('.inquiry .text-danger-email').html(txt);
        //     jQuery('.inquiry .text-danger-email p').css('color','red');
        //     return;
        // }
        // else{
        //     var txt = ' ';
        //     jQuery('.inquiry .text-danger-email').html(txt);
        //     jQuery('.inquiry .text-danger-email p').css('color','#fff');
        // }
        // if(contact_number == '' || contact_number == undefined ) {
        //     var txt = '<p>Please Enter Mobile Number.</p>';
        //     jQuery('.inquiry .text-danger-phone').html(txt);
        //     jQuery('.inquiry .text-danger-phone p').css('color','red');      
        //     return;
        // }
        // else{
        //     var txt = ' ';
        //     jQuery('.inquiry .text-danger-phone').html(txt);
        //     jQuery('.inquiry .text-danger-phone p').css('color','#fff');
        // }
        
        var txt1 = '';
        jQuery('.inquiry .div_danger').html(txt1);
        jQuery('.inquiry .otp_div .text-danger').html(txt1);
        jQuery('.inquiry .div_danger p').css('color','#ffffff');   
        jQuery('.inquiry .otp_div').removeClass('hide');
        jQuery('.inquiry .product_generate_otp_btn').addClass('hide');
        var req_data = { 
            contact_number: contact_number
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url :  window.location.origin+'/mobile_verification',
            data : req_data,
            'success' : function(data) {  
                //console.log(data);
                var va = jQuery.parseJSON(data); 
                //alert(va['results']);
                if(va['results'] == 1) {
                    jQuery('.inquiry .submit_otp_btn').attr('value','Submit OTP');
                    jQuery('.inquiry .submit_otp_btn').removeClass('verify_otp');
                    jQuery('.inquiry .submit_otp_btn').addClass('submit_otp');  
                    loginOtp(contact_number);
                    jQuery("input[type='submit']").prop('disabled', false); 
                }
                else{
                    alert("this is your otp "+va['otp']);
                    jQuery('.inquiry .submit_otp_btn').attr('value','Verify OTP');
                    jQuery('.inquiry .submit_otp_btn').addClass('verify_otp');
                    jQuery('.inquiry .submit_otp_btn').removeClass('submit_otp');
                    jQuery("input[type='submit']").prop('disabled', false);
                    //jQuery('.inquiry .');
                }
            },
            'error' : function(request,error)
            {//console.log("ram1");
                alert("Request: "+JSON.stringify(request));
            }
        });
    });
    
    // jQuery('.resend_otp_btn').click(function(){
    //     var contact_number = jQuery('.mobile_number').val();
    //     if(contact_number == '') {
    //         var txt = '<p>Please Enter Mobile Number.</p>';
    //         jQuery('.div_danger').html(txt);
    //         jQuery('.div_danger p').css('color','red');      
    //         return;
    //     }
    //     var txt1 = '';
    //     jQuery('.div_danger').html(txt1);
    //     jQuery('.div_danger p').css('color','#ffffff');   
    //     jQuery('.otp_div').removeClass('hide');
    //     jQuery('.generate_otp_btn').addClass('hide');
    //     console.log("ram");
    //     jQuery.ajax({
    //         type : 'POST',
    //         url : "<?php// echo base_url('mobile_verification'); ?>",
    //         data : { '<?php //echo $this->security->get_csrf_token_name(); ?>':'<?php //echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
    //         'success' : function(data) { // console.log(data);
    //             console.log("ram1");
    //             var va = jQuery.parseJSON(data);           
    //             //alert(va['results']);
    //             if(va['results'] == 1) {
    //                 jQuery('.submit_otp_btn').attr('value','Submit OTP');
    //                 jQuery('.submit_otp_btn').removeClass('verify_otp');
    //                 jQuery('.submit_otp_btn').addClass('submit_otp');   
    //                 loginOtp(contact_number);
    //             }
    //             else{alert("this is your otp "+va['otp']);
    //                 jQuery('.submit_otp_btn').attr('value','Verify OTP');
    //                 jQuery('.submit_otp_btn').addClass('verify_otp');
    //                 jQuery('.submit_otp_btn').removeClass('submit_otp');
    //             }
    //         },
    //         'error' : function(request,error)
    //         {console.log("ram1");
    //             alert("Request: "+JSON.stringify(request));
    //         }
    //     });
    // });
    function loginOtp(contact_number){
        var req_data = { 
            contact_number: contact_number
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url :  window.location.origin+'/otp_generation',
            data : req_data,
            'success' : function(data) {  //console.log(data);           
               var va = jQuery.parseJSON(data);
               //alert('this is your OTP '+va['otp']);
            },
            'error' : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
            }
        });
    }   
    jQuery('#apartment_otp_submit').click(function(event){
        var apartment_otp = jQuery('.apartment-otp-text').val();
        var apartment_otp_scrap_id = jQuery('.apartment-otp-scrap-id').val();
        if(apartment_otp=='' || apartment_otp == undefined){
            jQuery('.apartment-otp-danger').html('<p>Please Enter your pre-approved OTP</p>');
            jQuery('.apartment-otp-danger').css('color','#a94442');
            return false;
        }
        var req_data = { 
            apartment_otp: apartment_otp,
            scrap_id : apartment_otp_scrap_id
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url :  window.location.origin+'/apartment_otp',
            data : req_data,
            'success' : function(data){  console.log(data);          
               var va = jQuery.parseJSON(data);
               console.log(va.result);
               if (va =='yes'){
                    jQuery('.apartment-otp-page .alert-inbox-container').html('<div class="alert alert-info alert-inbox" role="alert"><p>Your OTP is submited successfully.</p></div>');
                    var modal2 = document.getElementById('apartment-otp-page-modal');
                    modal2.style.display = "block";
               }
               else{
                    jQuery('.apartment-otp-page .alert-inbox-container').html('<div class="alert alert-danger alert-inbox" role="alert"><p>Error.</p></div>');
               }
            },
            'error' : function(request,error){
                alert("Request: "+JSON.stringify(request));
            }
        });
    });
    jQuery("body").on('click',"#myForm .verify_otp",function(){
        var contact_number = jQuery('.mobile_number').val();
        var otp = jQuery('#myForm .otp').val();
        if(contact_number == '') {
            var txt = '<p>Please Enter Mobile Number.</p>';
            jQuery('.div_danger').html(txt);
            jQuery('.div_danger p').css('color','red');      
            return;
        }
        else{
            var txt1 = '';
            jQuery('.div_danger').html(txt1);
            jQuery('.div_danger p').css('color','#ffffff'); 
        }
        if(otp == '') {
            var txt = '<p>Invalid OTP.</p>';
            jQuery('.div_danger1').html(txt);
            jQuery('.div_danger1 p').css('color','red');      
            return;
        }
        else{
            var txt1 = '';
            jQuery('.div_danger1').html(txt1);
            jQuery('.div_danger1 p').css('color','#ffffff');   
        }
        
        var req_data = { 
            contact_number: contact_number,
            otp: otp
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url : window.location.origin+'/verify_register_otp',
            data : req_data,
            'success' : function(data) {  
                //alert(data);
               if(data==1){
                   verifyOtp(contact_number,otp);
               }
               if(data==2){
                    var txt1 = '<p>Your Otp is expired.</p>';
                    jQuery('.div_danger1').html(txt1);
                    jQuery('.div_danger1 p').css('color','red'); 
               }if(data==3){
                    var txt1 = '<p>You Entered Wrong Otp.</p>';
                    jQuery('.div_danger1').html(txt1);
                    jQuery('.div_danger1 p').css('color','red'); 
               }
                
            },
            'error' : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
            }
        });
    });
    function verifyOtp(phone,otp) {
        var create_user = 'create_user';//alert("done");
        var req_data = { 
            phone:phone,
            otp:otp,
            create_user:create_user
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url : window.location.origin+'/register_login',
            data : req_data,
            'success' : function(data) {  
                 window.location.href = window.location.origin+'/sell-scrap';
            },
            'error' : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
            }
        });
    }
    
    jQuery("body").on('click', '#myForm .submit_otp',function(){
    	var contact_number = jQuery('#myForm  .mobile_number').val();//alert(contact_number);
        var otp = jQuery('#myForm .otp').val();
        if(contact_number == '') {
            var txt = '<p>Please Enter Mobile Number.</p>';
            jQuery('.div_danger').html(txt);
            jQuery('.div_danger p').css('color','red');      
            return;
        }
        else{
            var txt1 = '';
            jQuery('.div_danger').html(txt1);
            jQuery('.div_danger p').css('color','#ffffff'); 
        }
        if(otp == '') {
            var txt = '<p>Please Enter OTP.</p>';
            jQuery('.div_danger1').html(txt);
            jQuery('.div_danger1 p').css('color','#a94442');      
            return;
        }
        else{
            var txt1 = '';
            jQuery('.div_danger1').html(txt1);
            jQuery('.div_danger1 p').css('color','#ffffff');   
        }  
        var req_data = { 
            contact_number:contact_number,
            otp:otp
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
	        type : 'POST',
	        url : window.location.origin+'/verify_otp',
            data : req_data,
	        'success' : function(data) {     //alert(data);         
	            if(data == 1){
	        		jQuery(".otp-resend_btn").removeClass("has-error");
	        		jQuery("#myForm #sell_scrap_login_otp").click();;
	        	}
	        	else if(data == 2){
	        		jQuery(".otp-resend_btn").addClass("has-error");
	        		var p = "<p>OTP is expired. Please try again.</p>"
	                jQuery(".div_danger1").html(p);
                    jQuery('.div_danger1 p').css('color','#a94442');  
	        		return false; 
	        	}
	        	else if(data == 3){
	        		jQuery(".otp-resend_btn").addClass("has-error");
	        		var p = "<p>Incorrect OTP. Please try again.</p>"
	                jQuery(".div_danger1").html(p);
                    jQuery('.div_danger1 p').css('color','#a94442');  
	        		return false; 
	        	}
	        	else{
	        		jQuery(".otp-resend_btn").addClass("has-error");
	        		var p = "<p>Error in login.</p>"
	                jQuery(".div_danger1").html(p);
                    jQuery('.div_danger1 p').css('color','#a94442');  
	        		return false; 
	        	}
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
    
    jQuery("body").on('click',"#myForm #sell_scrap_login_otp",function(){
        var contact_number = jQuery('#myForm  .mobile_number').val();
        var otp = jQuery('#myForm .otp').val();
         var req_data = { 
            contact_number:contact_number,
            otp:otp
        };
        req_data[tokan] = cookiee;
        jQuery.ajax({
            type : 'POST',
            url : window.location.origin+'/login_otp',
            data : req_data,
            'success' : function(data) {       
                
                window.location.href = window.location.origin+'/sell-scrap';
                
            },
            'error' : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
            }
        }); 
    });
     
     var doc_height = jQuery(document).height();
     jQuery(window).scroll(function(){
	  var header = jQuery('.header'),
	  	  width = jQuery(window).width(),
	      scroll = $(window).scrollTop();
	      //console.log('height - '+doc_height);
	  if (scroll >= 145 && width >1024 && doc_height > 1500){ 
	      //console.log('add');
	      header.addClass('sticky');
	  }else{ 
	      //console.log('remove');
	      header.removeClass('sticky');
	  }
	});
	
    jQuery(".thumbnail.wishlist-tiles").on('click', function(){
        jQuery(this).toggleClass("wishlist-product-info");
    });
	
});