<?php $this->load->view('admin/templates/header', array(
	'link' => 'Scrap',
	'breadcrumbs' => array(
		0 => array('name'=>'Customer','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper{overflow-x:scroll !important; }
</style>

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">Scrap Request</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>
	<div class="row">
		<div class="pick_up_date col-sm-3 form-group">
		    <div class='input-group date' id='datetimepicker6'>
              	<input type='text' name="pick_up_date" id="pick_up_date" class="form-group form-control input-lg" placeholder="Select Pick Up Date" />
              	<span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
              	</span>
            </div>
		</div>
		<div class="pick_up_date_slot col-sm-2 form-group">
		    <div class='input-group date' id='datetimepickermax'>
              	<input type='text' name="pick_up_time_max" id="pick_up_time_max" class="form-group form-control input-lg" placeholder="Select Pick Up Time" />
              	<span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
              	</span>
            </div>
		</div>
		<div class="assign_to_customer col-sm-2">
			<input type="button" name="send_notification" id="send_notification" value="Send Notification" class="btn btn-lg btn-primary send_notification">
		</div>
		<div class="col-sm-5">
			<?php $attributes = array('class' => 'assign-to-vendor');
			 echo form_open('assign-to-vendor', $attributes); ?>
				<select name="vendor_id" id="vendor_id_list" class="input-lg">
					<?php 
						   $i = 1;						
						   foreach($data['vendors_list'] as $vendor){ 
							   if(isset($data['vendor_id'])) {?>
								<option value="<?php echo $vendor['id'] ?>" <?php if($data['vendor_id'] == $vendor['id']) { ?> selected="selected" <?php } ?>><?php echo $vendor['name'] ?></option>
							   <?php }else{	
								    if($i == 1){ ?>
										<option value="<?php echo $vendor['id'] ?>" selected="selected"><?php echo $vendor['name'] ?></option>
									<?php }else { ?>
										<option value="<?php echo $vendor['id'] ?>"><?php echo $vendor['name'] ?></option>
									<?php }
									$i++; 
								}
							}?>
				</select>
				<input type="hidden" name="scrap_ids" id="scrap_ids"  <?php if(isset($data['vendor_id'])) {?> value="<?php echo $data['scrapids'] ?>" <?php } ?> />
				<?php if($data['already_assigned']):?>
					<input type="hidden" name="force_assign" id="force_assign" value="1"/>
				<?php endif ?>	
				<input type="submit" name="submit" id="scrap_ids_btn" class="btn btn-lg btn-primary" value="Assign to Vendor"/>
			<?php echo form_close();?>
		</div>
	</div>
	<div class="row">
		<div class="add_customer form-group col-md-6" >
			<input type="text" name="add_customer_group" id="add_customer_group" class="form-group input-lg add_customer_group" placeholder="Enter Group Number">
			<input type="button" name="add_customer_btn" id="add_customer_btn" value="Add Customer" class="btn btn-lg btn-primary add_customer_btn">
		</div>
		<div class="send_information_div col-md-3" style="float: right;">
			<input type="button" name="send_information" id="send_information" value="Send Information" class="btn btn-lg btn-primary send_information"  style="float: right;">
			<label style="float: right;">Send SMS or email to user</label><br>
		</div>
	</div>
	<div class="row">
		<script type="text/javascript">
			jQuery(document).ready(function() {
			   var url      =  window.location.href;//alert(url);
			   url = url.split("/");
			   if (url[4] == 'edit' || url[4] == 'add') {
			   	jQuery('#add_customer_group').hide();
			   	jQuery('#add_customer_btn').hide();
			   	jQuery('#scrap_ids').hide();
			   	jQuery('#scrap_ids_btn').hide();
			   	jQuery('#vendor_id_list').hide();
			   	jQuery('#send_notification').hide();
			   	jQuery('.pick_up_date').hide();
			   	jQuery('.pick_up_date_slot').hide();
			   	jQuery('.send_information_div').hide();
			   	jQuery('.datetime-input').attr('autocomplete','off');
			   }
			});
		</script>
	</div>
	<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
    <!--<div class="output"></div>-->
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
<!-- //Modify Email part Start -->
<div id="modify_email" class="form-popup2">
  <div class="modal-content scrap_admin_message">
    <span class="close">&times;</span>
    <div>
    	<br>
        <label for="email"><b>Do You Want To Modify Send Notification Email?</b></label>
        <p>This is Your email</p><br>
        <div class="email_text"></div>
        <button class="btn modify_yes">Modify and Send</button>
        <button class="btn modify_no">Send Without Modification</button>
        <button class="btn dont_add">Cancel</button>
    </div>
  </div>
</div>

<div id="modify_email_text" class="form-popup2">
  <div class="modal-content scrap_admin_message">
    <span class="close">&times;</span>
    <div>
    	<br>
        <label for="email"><b>Write here Modify Text.</b></label><br>
         <textarea class="form-control" rows="5" id="email_comment"></textarea>
         <br/>
        <button class="btn modify_close">Send</button>
        <button class="btn dont_add">Cancel</button>
    </div>
  </div>
</div>
<!--send information to customer part pop up start-->
<div id="send_info" class="form-popup2">
  <div class="modal-content send_info_message">
    <span class="close">&times;</span>
    <div>
    	<br>
        <label for="email"><b>Write your message.</b></label><br>
         <textarea class="form-control" rows="5" id="send_text"></textarea>
         <div class="sms_inforamtion"><span class="total_character">0</span><span> Characters ( </span><span class="total_sms_credit">0</span><span> SMS credit )</span></div>
        <br/>
        <br/>
        <button class="btn btn-info send_email" value="Email">Email</button>
        <button class="btn btn-primary send_sms" value="SMS">SMS</button>
        <button class="btn btn-success send_both" value="Email and SMS">Email and SMS</button>
        <button class="btn btn-danger dont_send">Cancel</button>
    </div>
  </div>
</div>
<!--send information to customer part pop up end-->

<!-- Vendor already assigned popup start -->
<?php if($data['already_assigned']):?>
<div id="already-assigned" class="already-assigned">
  	<div class="modal-content already_assigned_message">
    	<span class="close">&times;</span>
	    <div>
	    	<br>
	        <label for="head"><b>These requests are already assigned to another vendor.</b></label><br>
	        <div class="already_assigned_id">
	        	<?php foreach($data['already_assigned_id'] as $already_assigned_id): ?>
	        		<span><?php echo $already_assigned_id; ?></span>
	        	<?php endforeach ?>
	        </div>
	        <br/>
	        <!-- <a href="<?php //echo base_url('assign_another_vendor');?>"> --><button class="btn btn-success assign">Assign to Vendor</button><!-- </a> -->
	        <button class="btn btn-danger dont_assign">Cancel</button>
	    </div>
  	</div>
</div>
<?php endif ?>
<!-- Vendor already assigned popup end -->

<script type="text/javascript">
jQuery(function() {
    var url      =  window.location.href;//alert(url);
    url = url.split("/");
    if (url[4] == 'edit') {
    	var reward_point = jQuery('input#reward_point').val();
    	var scrap_request_type = jQuery('#field-request_type').text();
    	if(scrap_request_type =='Non-Periodic'){
    	    jQuery('.periodic_request_status_pause').prop('checked', false);
    	    jQuery('#periodic_request_status_field_box').hide();
    	}
    // 	else{
    // 	    jQuery('#request_status_field_box').hide();
    // 	}
	    if (reward_point<1 || reward_point=='') {
	    	jQuery('input#reward_point').prop("readonly",false);
	    }
	    else{
	    	jQuery('input#reward_point').prop('readonly', true);
	    }
    }	
});
var modal  = document.getElementById('modify_email_text');
var modal1 = document.getElementById('modify_email');
var modal2 = document.getElementById('send_info');
<?php if($data['already_assigned']):?>
var modal3 = document.getElementById('already-assigned');
<?php endif ?>
jQuery(document).ready(function(){
	jQuery('.close, .dont_add, .dont_send').click(function(){
		modal.style.display  = "none";
		modal1.style.display = "none";
		modal2.style.display = "none";
		<?php if($data['already_assigned']):?>
			modal3.style.display = "none";
		<?php endif ?>
		location.reload();
	});
	jQuery("body").on('click','.close_scrap_notification,.close_scrap_success,.close_send_info,.close_send_info_model,.dont_assign',function(event){
		modal.style.display  = "none";
		modal1.style.display = "none";
		modal2.style.display = "none";
		<?php if($data['already_assigned']):?>
			modal3.style.display = "none";
		<?php endif ?>
      	location.reload();
	});
	$(document).ready(function() {
  		$(function() {
			$('#datetimepicker6').datetimepicker({
                format: "YYYY-MM-DD HH:mm:ss"
			});
  		});
	});
	jQuery(document).ready(function() {
  		jQuery(function() {
			jQuery('#datetimepickermax').datetimepicker({
                format: "HH:mm:ss"
			});
  		});
	});
	// jQuery(document).ready(function() {
 //  		jQuery(function() {
	// 		jQuery('#datetimepickermin').datetimepicker({
 //                format: "HH:mm:ss"
	// 		});
 //  		});
	// });
});	
$(function () {
	
	//CHECK ALL BOXES
	$('.select-all').click(function () {
			
			if (!$(this).hasClass('checkall')) {
				var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
	            $('input[type="checkbox"]', allPages).prop('checked', false);
	        } else {
	        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
	        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
	        }
	        $(this).toggleClass('checkall');
				
	});
	
  	jQuery("body").on('click', '#scrap_ids_btn',function(event){

  		if(!jQuery("#force_assign").length){
	  	    var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
			var myarray = [];
			$('input[type="checkbox"]:checked', allPages).each(function(){
				myarray.push(jQuery(this).parent().next().text());
			});	
			jQuery("#scrap_ids").val(myarray.toString());

			if(jQuery.isEmptyObject(myarray)){
				alert("Please select list");
				jQuery(this).val('Assign to Vendor');
				return false;
			}
		}

  	});
  	
  	jQuery('.assign').on('click',function(e){
  		jQuery('#scrap_ids_btn').trigger("click");
  	});
	
	//Send Information to user part start
	jQuery('.send_information').click(function(){
	    var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return false;
		}
		jQuery("#send_text").val('');
		jQuery('.total_sms_credit').text('0');
		jQuery('.total_character').text('0');
		jQuery("#send_info").css('display','block');
	});
	jQuery('.send_email,.send_sms,.send_both').click(function () {
		var email = jQuery(this).val();
		var sms = jQuery(this).val();
		var both = jQuery(this).val();
		var send_text = jQuery('#send_text').val();
		var send_action = '';
		if(email!='' || email !=undefined){
			send_action = email;
		}
		else if(sms!='' || sms !=undefined){
			send_action = sms;
		}
		else if(both!='' || both !=undefined){
			send_action = both;
		}
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return false;
		}
		if(send_text=='' || send_text==undefined){
			alert("Please write message.");
			return false;
		}
		if(send_action=='' || send_text==undefined){
			alert("Error.");
			return false;
		}
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('send_information'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,send_text:send_text,send_action:send_action},
	        'success' : function(data) { 
	        	if(data == 1){
		            var txt = "<span class='close close_send_info'>&times; </span><div><br><p>Messge is empty. Please write message and try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else if(data == 2){
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>An error has been occurred. Please try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else if(data == 3){
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>You did not selected any customer. Please select customer and try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else if(data == 4){
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>Messge sent to user/users successfully.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else{
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>An error has been occurred. Please try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	jQuery('#send_text').keyup(function () {
		var len = jQuery('#send_text').val().length;
		var total_sms_credit = len/160;
		jQuery('.total_character').text(len);
		jQuery('.total_sms_credit').text(Math.floor(total_sms_credit)+1);
		if(len%160==0){
			jQuery('.total_sms_credit').text(Math.floor(total_sms_credit));
		}
	})
	//Send Information to user part end
	
	//Modify Email part Start
	jQuery('.send_notification').click(function(){
	    var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return;
		}
		var pick_up_date = jQuery('#pick_up_date').val();
		if(jQuery.isEmptyObject(pick_up_date)){
       	alert("Please Assaign Pick Up Date");
		return;
        }
        var email_text = "<p>Hi {customer name},</p><p>Admin has assaigned the following schedule for your Scrap pick up.</p><p>Scrap pick up date : dd/mm/yyy</p><p>Scrap pick up time : hh:mm:ss am/pm</p><p>{ Your Modify email part will append here.}</p>	<p>Please give your Response for Availability during Scrap Pick up.</p><span>Yes</span>	<span>No</span><br/><br/><p>Regards,<br/>Greenree</p>";
		jQuery("#modify_email").css('display','block');
		jQuery(".email_text").html(email_text);
	});
	jQuery('.modify_yes').click(function(){
		jQuery("#modify_email").css('display','none');
		jQuery("#modify_email_text").css('display','block');
	});

	//Modify Email part End
	jQuery('.modify_close,.modify_no').click(function () {
		var email_comment = jQuery('#email_comment').val();
		var pick_up_date = jQuery('#pick_up_date').val();
		// var pick_up_min_time = jQuery('#pick_up_time_min').val();
		var pick_up_max_time = jQuery('#pick_up_time_max').val();
		//alert(pick_up_min_time);
		//alert(pick_up_max_time);return false;
		if(jQuery.isEmptyObject(pick_up_date)){
			alert("Please Assaign Pick Up Date");
		return;
        }
        if(jQuery.isEmptyObject(pick_up_max_time)){
			alert("Please Assaign Pick Up Time Slot");
		return;
        }
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return;
		}
		
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('send_notification'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,pick_up_date:pick_up_date,email_comment:email_comment,pick_up_max_time:pick_up_max_time},
	        'success' : function(data) {             
	            var txt = "<span class='close close_scrap_success'>&times; </span><div><br><p>Customer Notified Successfully.</p><br> <input type='button' class='btn btn-primary close_scrap_notification' value='Close'>";
	            jQuery('.scrap_admin_message').html(txt);
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});

	jQuery('.add_customer_btn').click(function () {
       // e.preventDefault();
        var add_customer_group = jQuery('#add_customer_group').val();
        if(jQuery.isEmptyObject(add_customer_group)){
	       	alert("Please Add Customer Group Id");
			return;
        }
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return;
		}
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('add_customer'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,add_customer_group:add_customer_group},
	        'success' : function(data) {      
				jQuery("#modify_email_text").css('display','block');
	        	var txt = "<span class='close close_scrap_success'>&times; </span><div><br><p>Customer Added Successfully.</p><br> <input type='button' class='btn btn-primary close_scrap_notification' value='Close'>";
	            jQuery('.scrap_admin_message').html(txt); 
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	
});

jQuery('document').ready(function(){
		
	var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
	$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
		
});

</script>
<?php $this->load->view('admin/templates/footer'); ?>
