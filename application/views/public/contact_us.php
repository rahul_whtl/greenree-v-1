<?php $this->load->view('public/templates/header', array(
	'title' => 'Contact Us')) ?>
<div class="contact-us-page">
	<div class="container">
		<div class="col-md-12 text-center" style="margin-bottom: 30px;">    
		    <div class="alert alert-info hide">		    
		        <p><strong>Success!</strong> Your Request Is Sent Successfully.</p>
		    </div>    
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 contact-us-title">
				<h1 class="text-center">CONTACT US</h1> 
			</div>
			<div class="col-md-6 col-sm-6 contact-us-text">
				<!--<span>
					<h3>Contact Us</h3>
				</span>-->
				<ul class="get-in-touch">
			        <li><span class="text">Address</span>: <span>GreenREE Pvt Ltd.</span></li>
			        <li><span class="text">Call or Whatsapp</span>: <a href="tel:9804940000">9804940000</a></li>
			        <li><span class="text">Email</span>: <a href="mailto:contact@greenree.com">contact@greenree.com</a></li>
			        <!--<li><span class="text">Website</span>: <a href="#">www.greenree.com</a></li>-->
			    </ul>
			</div>
			<div class="col-md-6 col-sm-6 contact-us-input">
				<div class="form-group">
				  <label for="usr">Name<span class="red">*</span></label>
				  <input type="text" class="form-control" id="customer_name">
				</div>
				<div class="form-group">
				  <label for="usr">Email<span class="red">*</span></label>
				  <input type="text" class="form-control" id="customer_email">
				</div>
				<div class="form-group">
				  <label for="usr">Phone<span class="red">*</span></label>
				  <input type="text" class="form-control" id="customer_phone">
				</div>
				<div class="form-group">
				  <label for="msg">Message<span class="red">*</span></label>
				  <textarea class="form-control" rows="5" id="customer_msg"></textarea>
				</div>
				<div class="form-group">
				  <button class="btn btn-lg btn-success contact-us-submit">Submit</button> 
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>

<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery(".contact-us-submit").click(function(){
			var customer_name  = jQuery('#customer_name').val();
			var customer_email  = jQuery('#customer_email').val();
			var customer_phone  = jQuery('#customer_phone').val();
			var customer_msg  = jQuery('#customer_msg').val();
			if (customer_name=='') {
				alert('Your name is empty.');
				return;
			}
			if (customer_phone=='') {
				alert('Your Phone is empty.');
				return;
			}
			if (customer_email=='') {
				alert('Your Email is empty.');
				return;
			}
			if (customer_msg=='') {
				alert('Your Message is empty.');
				return;
			}
			jQuery.ajax({
				type : 'POST',
			    url : "<?php echo base_url('contact_us_msg'); ?>",
			    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',customer_name:customer_name,customer_phone:customer_phone,customer_email:customer_email,customer_msg:customer_msg},
			    'success' : function(data) {             
			        jQuery('.alert-info').removeClass('hide');
			        jQuery("#customer_name").val('');
			        jQuery("#customer_phone").val('');
			        jQuery("#customer_email").val('');
			        jQuery("#customer_msg").val('');
			    },
			    'error' : function(request,error)
			    {
			        alert("Request: "+JSON.stringify(request));
			    }
			});
		});	
	});	
</script>

