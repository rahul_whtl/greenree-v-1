<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Profile',
	'link' => 'account',
	'sub_link' => 'profile'
)); ?>
<div class="my-profile">
	<div class="container">
		<div class="page-header">
		    <h4 class="lead">
		    	<span class="glyphicon glyphicon-user" style="margin-right:10px"></span>
			    Edit profile
		    </h4>
		    <?php if(strpos($user->email,'@')): ?><?php //echo $user->email; ?> 
    		    <p>
    		    	To change password, click this link <?php echo anchor('change_password', 'Change Password') ?>
    		    </p>
		    <?php endif ?>
		</div>

		<?php echo form_open_multipart(uri_string()); ?>
			<div class="row">
		        <div class="col-xs-12 col-sm-4 col-md-3">
		            <div class="form-group <?= form_error('userfile') ? 'has-error' : '' ?>">
		                <div class="panel panel-default">
		                    <div class="panel-heading text-center">
		                        <label class="panel-title control-label" for="userfile">
		                        	Profile Photo
		                        </label>
		                    </div>
		                    <div class="fileinput fileinput-new" data-provides="fileinput">
		                        <div class="fileinput-new thumbnail text-warning">
		                        	<?php if($user->avatar){ ?>
			                        	<img src="<?=base_url('assets/images/my_profile/'.$user->avatar) ?>">
			                        <?php }else { ?>
			                        	<img src="<?=base_url('assets/images/my_profile/'.'avtar_image.jpg') ?>">
			                        <?php } ?>
		                        </div>
		                        <div class="fileinput-preview fileinput-exists thumbnail">
		                        </div>
		                        <div class="btn-group btn-block">
		                            <div class="btn btn-success btn-file">
		                                <span class="fileinput-new">Select image</span>
		                                <span class="fileinput-exists">Change</span>
		                                <input type="file" name="userfile">
		                            </div>
		                            <a href="#" class="btn btn-danger remove-image" data-dismiss="fileinput">Remove</a>
		                            <input type="hidden" name="remove_flag" id="remove_flag">
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="text-danger"><?=form_error('userfile') ? form_error('userfile') : '&nbsp' ?></div>
		        </div>
		        <div class="col-xs-12 col-sm-8 col-md-9 user-profile-form">
		            <div class="row profile-data">
			            <!-- <div class="col-md-4">
			                <div class="form-group <?//= form_error('username') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="name">Username <?//= form_error('username') ? '('.form_error('username').')' : '' ?></label>
			                    <input class="form-control" type="text" name="username" value="<?php //if(strpos($user->username,'@')): ?><?php //echo $user->username; ?><?php //else: ?><?php// echo ''; ?><?php// endif ?>">
			                </div>
		                </div> -->
			            <div class="col-md-4 form-group-email">
			                <div class="form-group <?= form_error('email') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="name">Email address<span class="red">*</span> <?= form_error('email') ? ' ' : '' ?></label>
			                    <input class="form-control" id='profile_email' type="text" name="email" value="<?php if(strpos($user->email,'@')): ?><?php echo $user->email; ?><?php else: ?><?php echo ''; ?><?php endif ?>">
			                    <input class="form-control email1 hide" type="text" name="email1" value="<?php echo $user->email; ?>">
			                    <span class="text-danger text-danger-email"><?=form_error('email') ? form_error('email') : '' ?></span>
			                </div>
			            </div>
		                <div class="col-md-4 form-group-name ">
		                    <div class="form-group <?= form_error('first_name') ? ' ' : '' ?>">
		                        <label class="control-label" for="first_name">Name <span class="red">*</span> <?= form_error('first_name') ? '(required)' : '' ?></label>
		                        <input class="form-control" id='profile_name' type="text" name="first_name" value="<?= set_value('first_name') ? set_value('first_name') : $user->first_name.$user->last_name ?>">
		                        <span class="text-danger text-danger-name"></span>
		                    </div>
		                </div>
		               <!--  <div class="col-md-4">
		                    <div class="form-group <?//= form_error('last_name') ? 'has-error' : '' ?>">
		                        <label class="control-label" for="last_name">Last name <?//= form_error('last_name') ? '(required)' : '' ?></label>
		                        <input class="form-control" type="text" name="last_name" value="<?//= set_value('last_name') ? set_value('last_name') : $user->last_name ?>">
		                    </div>
		                </div> -->
			            <div class="col-md-4 form-group-phone">
			                <div class="form-group <?= form_error('phone') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="phone">Phone<span class="red">*</span> <?= form_error('phone') ? ' ' : '' ?></label>
			                    <input class="form-control" id='profile_phone' type="text" name="phone" value="<?= set_value('phone') ? set_value('phone') : $user->phone ?>">
			                    <span class="text-danger text-danger-phone"><?=form_error('phone') ? form_error('phone') : '' ?></span>
			                </div>
			            </div>
			            <div class="col-md-12 mobile_verification_div hide">
			                <div class="col-md-8 col-xs-12 text-right" style="float: right;">
			                    <span class="text-danger text-danger-otp"></span>
			                    <label class="control-label" for="otp">Verify Your Phone Number</label>
			                    <input type="button" class="btn btn-md btn-primary mobile_verify" value="Generate OTP">
			                    <input type="button" class="btn btn-md btn-primary mobile_verification_cancel" value="Cancel">
			                </div>
			            </div>
			            <?php foreach($user_address as $user_address ): ?>
			            <div class="col-md-4 form-group-country">
			                <div class="form-group <?= form_error('country') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="country">Country <span class="red">*</span> <?= form_error('country') ? '' : '' ?></label>
			                    <select name="country" id="country" class="form-control">
			                        <option value="0" selected> - Country - </option>
			                        <?php if($user_address->country == ''){ ?>
			                        <?php if(sizeof($countries) == 1){ ?>
                            			    <option value="<?php echo $countries[0]['loc_name'];?>" data-val="<?php echo $countries[0]['loc_id'];?>" selected><?php echo $countries[0]['loc_name'];?></option> 
			                        <?php }else{ foreach($countries as $country) { ?>
                    					<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>">
                    						<?php echo $country['loc_name'];?>
                    					</option>
			                        <?php } } }else{ ?>
                            		<?php foreach($countries as $country) { ?>
                    					<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>" <?php echo ($user_address->country == $country['loc_name'])? "selected":"" ?>>
                    						<?php echo $country['loc_name'];?>
                    					</option>
                    				<?php } } ?>
			                    </select>
			                    <span class="text-danger text-danger-country"><?=form_error('country') ? form_error('country') : '' ?></span>
			                </div>
			            </div>
			            <div class="col-md-4 form-group-state">
			                <div class="form-group <?= form_error('state') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="state">State <span class="red">*</span> <?= form_error('state') ? ' ' : '' ?></label>
			                    <select name="state" id="state" class="form-control">
			                        <option value="0" selected> - State - </option>
			                        <?php if($user_address->country == ''){ ?>
			                        <?php if(sizeof($states) == 1){ ?>
                            			    <option value="<?php echo $states[0]['loc_name'];?>" data-val="<?php echo $states[0]['loc_id'];?>" selected><?php echo $states[0]['loc_name'];?></option> 
			                        <?php }else{ foreach($states as $state) { ?>
                    					<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>">
                    						<?php echo $state['loc_name'];?>
                    					</option>
			                        <?php } } }else{ ?>
			                        <?php foreach($states as $state) { ?>
										<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>" <?php echo ($user_address->state == $state['loc_name'])? "selected":"" ?>>
											<?php echo $state['loc_name'];?>
										</option>
									<?php } } ?>
								</select>
								<span class="text-danger text-danger-state"></span>
			                </div>
			            </div>
			            <div class="col-md-4 form-group-city">
			                <div class="form-group <?= form_error('city') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="city">City <span class="red">*</span> <?= form_error('city') ? '' : '' ?></label>
			                    <input list="city_list" name="city" id="city" class="form-control" autocomplete="off" value="<?php if($user_address->city) echo $user_address->city;elseif($user_address->country == '' && sizeof($cities) == 1)echo $cities[0]['loc_name'];?>" placeholder="Select or Enter your City">
								<datalist id="city_list">
									<?php if($user_address->country == ''){ ?>
                    				<?php if(sizeof($cities) == 1){ ?>
                    				    <option value="<?php echo $cities[0]['loc_name'];?>" data-val="<?php echo $cities[0]['loc_id'];?>"    selected><?php echo $cities[0]['loc_name'];?></option>
                    				<?php }else{ foreach($cities as $city) { ?>
                    				<option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>">
                    					<?php echo $city['loc_name'];?>
                    				</option>
                    			    <?php } ?>
			                        <?php } }else{
			                            foreach($cities as $city) { ?>
										<option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>" <?php echo ($user_address->city == $city['loc_name'])? "selected":"" ?>>
											<?php echo $city['loc_name'];?>
										</option>
									<?php } } ?>
								</datalist>
								<span class="text-danger text-danger-city"></span>
			                </div>
			            </div>
			            <div class="col-md-4 form-group-locality">
			                <div class="form-group <?= form_error('locality') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="locality">Locality <span class="red">*</span> <?= form_error('locality') ? '' : '' ?></label>
			                    <input list="locality_list" name="locality" id="locality" class="form-control" autocomplete="off" value="<?php if($user_address->locality) echo $user_address->locality;elseif($user_address->country == '' && sizeof($localities) == 1)echo $localities[0]['loc_name'];?>" placeholder="Select or Enter your Locality">
			                    <datalist id="locality_list">
			                        <?php if($user_address->country == ''){ ?>
                    			    <?php if(sizeof($localities) == 1){ ?>
                    					   <option value="<?php echo $localities[0]['loc_name'];?>" data-val="<?php echo $localities[0]['loc_id'];?>" selected><?php echo $localities[0]['loc_name'];?>
                    					   </option>
                    				<?php }else{ foreach($localities as $locality) { ?>
                    				    <option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>">
                    					<?php echo $locality['loc_name'];?>
                    					</option>
                    			    <?php } ?>
			                        <?php } }else{
			                            foreach($localities as $locality) { ?>
										<option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>" <?php echo ($user_address->locality == $locality['loc_name'])? "selected":"" ?>>
											<?php echo $locality['loc_name'];?>
										</option>
									<?php } } ?>
			                    </datalist>
			                    <span class="text-danger text-danger-locality"><?=form_error('locality') ? form_error('locality') : '' ?></span>
			                </div>
			            </div>
			            <div class="col-md-4 form-group-apartment-name">
			                <div class="form-group <?= form_error('apartment_name') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="apartment_name">Apartment<span class="red">*</span> <?= form_error('apartment_name') ? '' : '' ?></label>
			                    <input list="apartment_list" name="apartment_name" id="apartment" class="form-control" autocomplete="off" value="<?php if($user_address->apartment_name) echo $user_address->apartment_name;elseif($user_address->country == '' && sizeof($apartments) == 1)echo $apartments[0]['loc_name'];?>" placeholder="Select or Enter your Apartment">
			                    <datalist id="apartment_list"  data-dropup-auto="false">
			                    	<?php if($user_address->country == ''){ ?>
                        			    <?php if(sizeof($apartments) == 1){ ?>
                        				    <option value="<?php echo $apartments[0]['loc_name'];?>" data-val="<?php echo $apartments[0]['loc_id'];?>" selected><?php echo $apartments[0]['loc_name'];?></option>
                        				    <option value="Others" data-val="Others">
											Others
											</option>
                        				<?php }else{ foreach($apartments as $apartment) { ?>
                        				<option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>">
                        					<?php echo $apartment['loc_name'];?>
                        				</option>
                        			    <?php } } }else{
			                            foreach($apartments as $apartment) { ?>
										<option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>" <?php echo ($user_address->apartment_name == $apartment['loc_name'])? "selected":"" ?>>
											<?php echo $apartment['loc_name'];?>
										</option>
									<?php } } ?>	
			                    </datalist>
			                    <span class="text-danger text-danger-apartment"><?=form_error('apartment_name') ? form_error('apartment_name') : '' ?></span>
			                </div>
			            </div>	
			            <div class="col-md-4 form-group-flat-no">
			                <div class="form-group <?= form_error('flat_no') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="flat_no">Flat No.<span class="red">*</span> <?= form_error('flat_no') ? '' : '' ?></label>
			                    <input class="form-control" id='profile_flat_no'type="text" name="flat_no" value="<?php echo $user_address->flat_no; ?>">
			                    <span class="text-danger text-danger-flat_no"><?=form_error('flat_no') ? form_error('flat_no') : '' ?></span>
			                </div>
			            </div>
						<?php endforeach ?>	
		            </div>
		            <div class="col-md-12">
                        <div class="request-info-box">
                    		<span class="notification_icon"></span> 
                    	    <div class="alert alert_msg1">
                    	    </div> 
                        </div>
                    </div>
		            <div class="col-md-12">
						<input type="submit" class="btn btn-lg btn-primary" value="Update Profile" id="profile_update" name="edit_user" />
					</div>
		        </div>
		    </div>
	    	<?php if($this->session->flashdata('alert') != '') {
  					$alert = $this->session->flashdata('alert'); ?>
    				<div class="alert alert-<?php echo $alert['type'] ?>">
    					<?php echo $alert['message'] ?>
    				</div>
	    		<?php }	?>
<div id="myModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div>
    	<h4>Enter your current password.</h4>
		<span>Are You Sure You Want To Update Your Profile Details.</span><br><br>
		<div>
			<?php if ( validation_errors() AND form_error('old_password') ): ?>
				<div class="alert alert-danger animated fadeInDown" id="message">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo form_error('old_password') ?>
				</div>
			<?php endif ?>

            <input class="form-control" type="password" name="old_password" value=""><br>
		</div>
    	<input type="button" class="btn btn-lg btn-block btn-primary" name="edit_user1" value="Continue" />
    </div>
  </div>
</div>
	</div>
</div>
    <?php if ( validation_errors() AND form_error('old_password') ): ?>
    	<script type="text/javascript">
	    	$(document).ready(function() {
		    	$('#enter-password').modal('show')
	    	});
    	</script>
    <?php endif ?>
<?php echo form_close() ?>
<script type="text/javascript">
    function validateEmail(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(String(email).toLowerCase());
    }
    
	jQuery(document).ready(function(){
		var phone = '<?php echo $user->phone ?>';
		//for address part end
		var country_value = jQuery('#country option:selected', this).val();
		var state_value = jQuery('#state option:selected', this).val();
		var city_value = jQuery('#city').val();
		var locality_value = jQuery('#locality', this).val();
		if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
        	jQuery('#state').attr("disabled", true);
        	jQuery('#city').attr("disabled", true);
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(state_value=='' || state_value=='- State -' || state_value=='0'){
        	jQuery('#city').attr("disabled", true);
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(city_value=='' || city_value=='- city -'){
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(locality_value=='' || locality_value=='- apartment -'){
        	jQuery('#apartment').attr("disabled", true);
        }
		//for address part end
	    jQuery('.profile-data #profile_phone').on('change',function(){
	    	jQuery('.profile-data .mobile_verification_div').removeClass('hide');
	    	jQuery('.profile-data .mobile_verification_div').css('height','60px');
	    });
	    jQuery("body").on('click',".profile-data .mobile_verification_cancel",function(){
	    	jQuery('.profile-data #profile_phone').val(phone);
	    	jQuery('.profile-data .mobile_verification_div').addClass('hide');
	    });
	    jQuery("body").on('click',".profile-data .mobile_verify,.profile-data .mobile_verification_resend",function(){
	    	var contact_number = jQuery('.profile-data #profile_phone').val();
        	var pattern = /^\d+$/;
        	var length = contact_number.toString().length;
        	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
        		jQuery(".form-group-number").addClass("has-error");
        		var p = "<p>Please Enter the Valid Contact Number.</p>";
                jQuery(".profile-data .text-danger-otp").html(p);
        		return; 
        	} 
        	var btn_val = jQuery(this).val();   
            jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('mobile_verification'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
		        success : function(data) {
		        	var data = JSON.parse(data);
		        	if(data.results == "Number is Invalid.")
		        	{
		        		jQuery(".profile-data .text-danger-otp").addClass("has-error");
		        		var p = "<p>Number is Invalid.</p>"
		                jQuery(".profile-data .text-danger-otp").html(p);
		        		return; 
		        	}
		        	if(data.results==1){
		        		jQuery(".profile-data .text-danger-otp").addClass("has-error");
		        		var p = "<p>Phone Number is Already Registered.</p>"
		        		jQuery(".profile-data .text-danger-otp").html(p);
		        		return;
		        	}
		        	//alert(data.otp);
		        	var txt = '<label class="control-label" for="otp">Enter Your OTP<span class="red">*</span></label><input class="form-control" id="otp_number" type="text"><span class="text-danger text-danger-otp"></span><input type="button" class="btn btn-md btn-primary mobile_verification_submit" value="Submit OTP"><input type="button" class="btn btn-md btn-primary mobile_verification_resend" value="Resend OTP"><input type="button" class="btn btn-md btn-primary mobile_verification_cancel" style="margin-top:10px" value="Cancel">';
		            jQuery(".profile-data .text-danger-otp").removeClass("has-error");
		        	jQuery(".profile-data .mobile_verification_div div").html(txt); 
		        	jQuery(".profile-data .mobile_verification_div").css('height','140px');
		        	if(btn_val=='Resend OTP')
						jQuery(".text-danger-otp").html('<p style="color:#26A65B">OTP sent to your number.</p>');
		        },
		        error : function(request,error)
		        {
		            alert("Request : "+JSON.stringify(request));
		        }
		    });
	    });
	    jQuery("body").on('click',".profile-data .mobile_verification_submit",function(){
			var contact_number = jQuery('.profile-data #profile_phone').val();
	        	var otp = jQuery('#otp_number').val();
	        	var pattern = /^\d+$/;
	        	var length = otp.toString().length;
	        	if(!otp || !(pattern.test(otp)) || length<4 || length >4){
	        		jQuery(".form-group-reg-otp-div").addClass("has-error");
	        		var p = "<p>Please Enter Coreect OTP Number.</p>"
	                jQuery(".profile-data .text-danger-otp").html(p);
	        		return false; 
	        	}
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('verify_otp'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp },
			        'success' : function(data) {  
			        	if (data == 1) {
			        		tag = "<div class='alert alert-success'><p><strong>Success!</strong> Your Phone is Verified Successfully.</p><span class='pre_phone_check hide'>1</span></div>";
					        // jQuery(".form-group-phone").removeClass("has-error");
				         //    jQuery(".text-danger-phone").removeClass("has-error");	
				            // jQuery(".text-danger-phone p").remove(); 
				        	jQuery(".profile-data .mobile_verification_div div").html(tag); 
			                jQuery(".create_user").attr("disabled", false);
			        	}
			        	if (data == 2) {
			        		jQuery(".profile-data .text-danger-otp").addClass("has-error");
			        		var p = "<p>OTP is Expired.</p>"
			        		jQuery(".profile-data .text-danger-otp").html(p);
			        		return;
			        	}
			        	if (data == 3) {
			        		jQuery(".profile-data .text-danger-otp").addClass("has-error");
			        		var p = "<p>Insert Correct OTP.</p>"
			        		jQuery(".profile-data .text-danger-otp").html(p);
			        		return;
			        	}
			        	if (data == 4) {
			        		jQuery(".profile-data .text-danger-otp").addClass("has-error");
			        		var p = "<p>Error.</p>"
			        		jQuery(".profile-data .text-danger-otp").html(p);
			        		return;
			        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
	    });

	    jQuery('#country').on('change',function(){
	    		country_value = jQuery(this).val();
	            var country_id = jQuery('option:selected', this).attr('data-val');
	            var token = jQuery('input[name="gre_tokan"]').val();
	            var data = {country : country_id, gre_tokan : token };
	            jQuery('#state').find('option').not(':first').remove();
	            jQuery('#city').val('');
        		jQuery('#locality').val('');
        		jQuery('#apartment').val('');
        		jQuery('#city_list').html('');
        		jQuery('#locality_list').html('');
        		jQuery('#apartment_list').html('');
	            jQuery.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo base_url('get_state'); ?>',
		            cache: false,
		            beforeSend : function(){
		                jQuery('#state').attr("disabled", true);
		                jQuery('#city').attr("disabled", true);
		                jQuery('#locality').attr("disabled", true);
		                jQuery('#apartment').attr("disabled", true);
		            },
		            success: function(response){
		            	var state_list = jQuery.parseJSON(response);
		            	for(var i = 0; i < state_list.length; i++) {
                            var obj = state_list[i];
                            console.log(" test city "+obj.loc_id);
                            jQuery('#state').append(jQuery("<option></option>")
                    .attr("value",obj.loc_name)
                    .attr("data-val",obj.loc_id)
                    .text(obj.loc_name));
                        }
                        if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
                        	jQuery('#state').attr("disabled", true);
		                }
		                else{
                        	jQuery('#state').attr("disabled", false);
		                }
		            }
		        });
	    });
	    
	    jQuery('#state').on('change',function(){
	    		var state_value = jQuery(this).val();
	            var state_id = jQuery('option:selected', this).attr('data-val');
	            var token = jQuery('input[name="gre_tokan"]').val();
	            var data = {state : state_id, gre_tokan : token };
	            jQuery('#city').val('');
        		jQuery('#locality').val('');
        		jQuery('#apartment').val('');
        		jQuery('#city_list').html('');
        		jQuery('#locality_list').html('');
        		jQuery('#apartment_list').html('');
	            jQuery('#city').find('option').not(':first').remove();
	            jQuery.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo base_url('get_city'); ?>',
		            cache: false,
		            beforeSend : function(){
		                jQuery('#city').attr("disabled", true);
	                	jQuery('#locality').attr("disabled", true);
	                	jQuery('#apartment').attr("disabled", true);
		            },
		            success: function(response){
		            	var city_list = jQuery.parseJSON(response);
		            	for(var i = 0; i < city_list.length; i++) {
                            var obj = city_list[i];
                            jQuery('#city_list').append(jQuery("<option></option>")
                    .attr("value",obj.loc_name)
                    .attr("data-val",obj.loc_id)
                    .text(obj.loc_name));
                        }
                        if(state_value=='' || state_value==' - state - ' || state_value=='0'){
                        	jQuery('#city').attr("disabled", true);
		                }
		                else{
                        	jQuery('#city').attr("disabled", false);
		                }
		            }
		        });
	    }); 
	    
	    jQuery('#city').on('change',function(){
	    		var city_value = jQuery(this).val();
        		var city_id = jQuery('#city_list').find("[value='" + city_value + "']").attr('data-val');
        		jQuery('#locality').val('');
        		jQuery('#apartment').val('');
        		jQuery('#locality_list').html('');
        		jQuery('#apartment_list').html('');
                var token = jQuery('input[name="gre_tokan"]').val();
                var data = {city : city_id, gre_tokan : token };
                jQuery('#locality').find('option').not(':first').remove();
                jQuery.ajax({
    	        	type: 'POST',
    	        	data: data,
    	            url: '<?php echo base_url('get_locality'); ?>',
    	            cache: false,
    	            beforeSend : function(){
		                jQuery('#locality').attr("disabled", true);
	                	jQuery('#apartment').attr("disabled", true);
		            },
    	            success: function(response){
    	            	var locality_list = jQuery.parseJSON(response);
    	            	for(var i = 0; i < locality_list.length; i++) {
                            var obj = locality_list[i];
                            jQuery('#locality_list').append(jQuery("<option></option>")
                    .attr("value",obj.loc_name)
                    .attr("data-val",obj.loc_id)
                    .text(obj.loc_name));
                        }
                        jQuery('#locality').append(jQuery("<option></option>")
                    .attr("value",'Others').attr("data-val",'Others').text('Others'));
                        if(city_value==''){
		                	jQuery('#locality').attr("disabled", true);
		                }
		                else{
                        	jQuery('#locality').attr("disabled", false);
		                }
    	            }
    	        });
	    }); 
        
        jQuery('#locality').on('change',function(){
        		var locality_value = jQuery(this).val();
        		var locality_id = jQuery('#locality_list').find("[value='" + locality_value + "']").attr('data-val');
        		jQuery('#apartment').val('');
        		jQuery('#apartment_list').html('');
                var token = jQuery('input[name="global_cookiee"]').val();
                var data = {locality : locality_id, gre_tokan : token };
                jQuery('#apartment').find('option').not(':first').remove();
                jQuery.ajax({
    	        	type: 'POST',
    	        	data: data,
    	            url: '<?php echo base_url('get_apartment'); ?>',
    	            cache: false,
    	            beforeSend : function(){
		                jQuery('#apartment').attr("disabled", true);
		            },
    	            success: function(response){
    	            	var apartment_list = jQuery.parseJSON(response);
    	            	for(var i = 0; i < apartment_list.length; i++) {
                            var obj = apartment_list[i];
                            jQuery('#apartment_list').append(jQuery("<option></option>")
                    .attr("value",obj.loc_name)
                    .attr("data-val",obj.loc_id)
                    .text(obj.loc_name));
                        }
                        jQuery('#apartment').attr("disabled", false);
                        if(locality_value==''){
		                	jQuery('#apartment').attr("disabled", true);
		                }
    	            }
    	        });
	    });
	    
		jQuery('.remove-image').click(function(){
			jQuery('.fileinput-new img').attr('src',"");
			jQuery('#remove_flag').val("1");
		}); 
		jQuery("#profile_update").click(function(event){
		    var name      = jQuery('#profile_name').val();
		    var email     = jQuery.trim(jQuery('#profile_email').val());
		    var phone     = jQuery('#profile_phone').val();
		    var pre_phone = '<?php echo $user->phone ?>';
		    var pre_phone_check = jQuery('.pre_phone_check').text();
		    var country   = jQuery('#country').val();
			var state     = jQuery('#state').val();
			var city      = jQuery('#city').val();
			var locality  = jQuery('#locality').val();
			var apartment = jQuery('#apartment').val();
		    var pin       = jQuery('#profile_pin').val();
		    var flat_no   = jQuery('#profile_flat_no').val();
		    jQuery('.text-danger').text('');
		    error = false;
		    //alert(city);return false;
		    if(name == '') {
				jQuery('.text-danger-name').text('Please enter your name');
				error = true;
			}
			if(!isNaN(name)){
			    jQuery('.text-danger-name').text('Please enter only characters');
				error = true;
			}
			if(name.length < 3){
			    jQuery('.text-danger-name').text('Please enter atleast three characters');
				error = true;
			}
			if(email == ''){
				jQuery('.text-danger-email').text('Please enter your email');
				error = true;
			}
			if(!validateEmail(email)){
			 	jQuery('.text-danger-email').text('Please enter valid email');
				error = true;
			}
			if(phone == ''){
			    jQuery('.text-danger-phone').text('Please enter phone number');
				error = true;
			}
			if(phone != pre_phone){
				if (pre_phone_check != 1) {
				    jQuery('.text-danger-otp').html('<p style="color:#a94442">Please verify OTP.</P>');
					error = true;
				}
			}
		    if(country == '0'){
			    jQuery('.text-danger-country').text('Please select country');
			    error = true;
			}
			if(state == '0'){
			    jQuery('.text-danger-state').text('Please select state');
			    error = true;
			}
			if(city == ''){
			    jQuery('.text-danger-city').text('Please select city');
			    error = true;
			}
			if(locality == ''){
			    jQuery('.text-danger-locality').text('Please select locality');
			    error = true;
			}
			if(apartment == ''){
			    jQuery('.text-danger-apartment').text('Please select apartment');
				error = true;
			}
			if(flat_no == ''){
			    jQuery('.text-danger-flat_no').text('Please enter flat number');
				error = true;
			}
// 			if(pin == ''){
// 			    jQuery('.text-danger-pin').text('Please Enter Pin');
// 				error = true;
// 			}
			if(error){
			    var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
			    return false;
			}else{
			    var txt1 = '<p></p>';
            	jQuery('.alert_msg1').html(txt1);
	            return true;
			}	            
		});
		
		jQuery(".profile-data .form-group-email #profile_email").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-email .text-danger').html(txt);
        	jQuery('.profile-data .form-group-email').css('color','#333');
		});
		jQuery(".profile-data .form-group-name input").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-name .text-danger').html(txt);
        	jQuery('.profile-data .form-group-name').css('color','#333');
		});
		jQuery(".profile-data .form-group-phone input").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-phone .text-danger').html(txt);
        	jQuery('.profile-data .form-group-phone').css('color','#333');
		});
		jQuery(".profile-data .form-group-country #country").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-country .text-danger').html(txt);
        	jQuery('.profile-data .form-group-country').css('color','#333');
		});
		jQuery(".profile-data .form-group-state #state").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-state .text-danger').html(txt);
        	jQuery('.profile-data .form-group-state').css('color','#333');
		});
		jQuery(".profile-data .form-group-city input").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-city .text-danger').html(txt);
        	jQuery('.profile-data .form-group-city').css('color','#333');
		});
		jQuery(".profile-data .form-group-locality input").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-locality .text-danger').html(txt);
        	jQuery('.profile-data .form-group-locality').css('color','#333');
		});
		jQuery(".profile-data .form-group-apartment-name input").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-apartment-name .text-danger').html(txt);
        	jQuery('.profile-data .form-group-apartment-name').css('color','#333');
		});
		jQuery(".profile-data .form-group-flat-no input").focus(function(){
		    var txt = '';
        	jQuery('.profile-data .form-group-flat-no .text-danger').html(txt);
        	jQuery('.profile-data .form-group-flat-no').css('color','#333');
		});
    });
</script>
<?php $this->load->view('public/templates/footer') ?>