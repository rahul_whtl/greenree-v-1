<div class="dashboard-header user-dashboard-header">
	<div class="container">
	    <a href="#" class="mobile-menu-link"><!--<i class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></i>--><span>User Menu</span><span class="caret"></span></a>
		<ul class="nav nav-tabs responsive-tab" role="tablist">
		    <li role="presentation" class="<?php echo ($active == 'orders') ? 'active' : '' ?>">
		        <?php echo anchor('user_dashboard/my-orders', 'Shop Orders') ?>
		    </li>
		    <!-- <li role="presentation" class="<?php// echo ($active == 'carts') ? 'active' : '' ?>">
		        <?php //echo anchor('user_dashboard/carts', 'Saved Carts') ?>
		    </li> -->
		    <li role="presentation" class="<?php echo ($active == 'points') ? 'active' : '' ?>">
		        <?php echo anchor('user_dashboard/my-wallet-cash', 'Wallet Cash') ?>
		    </li>
		    <li role="presentation" class="<?php echo ($active == 'redeem') ? 'active' : '' ?>">
		        <?php echo anchor('redeem_request_details', 'Redeem History') ?>
		    </li> 
		    <li role="presentation" class="<?php echo ($active == 'scrap') ? 'active' : '' ?>">
		        <?php echo anchor('user_dashboard/my-scrap-orders', 'Scrap Orders') ?>
		    </li>
		    <li role="presentation" class="<?php echo ($active == 'items') ? 'active' : '' ?>">
		        <?php echo anchor('user_dashboard/my-products', 'My Products') ?>
		    </li>
		    <li role="presentation" class="<?php echo ($active == 'wishlist') ? 'active' : '' ?>">
		        <?php echo anchor('user_dashboard/my-wishlist', 'My Wishlist') ?>
		    </li>
		</ul>
	</div>
</div>
<?php if(($active1=='dashboard')): ?>
    <div class="container">
    <?php // Alert users to errors, changes and notifications ?>
    <?php if (validation_errors()): ?>
        <div class="alert alert-danger">Check the form for errors and try again.</div>
    <?php else: ?>
        <?php if (! empty($message)): ?>
            <div id="message"> <?php echo $message ?> </div>
        <?php endif ?>
    <?php endif ?>
    <?php // End of Alerts ?>
    </div>
<?php endif ?>
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".mobile-menu-link").click(function(event){
				event.preventDefault();
				jQuery(".responsive-tab").slideToggle();
				jQuery(".user-dashboard-header .caret").toggleClass('active');
			});	
		});
</script>