<?php $this->load->view('public/templates/header', array(
	'title' => 'Wallet Cash - GreenREE',
	'menu_active' =>'my_wallet'
)) ?>

<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'points')) ?>

<div class="my-orders">
	<div class="container">
		<div class="col-md-12 alert-inbox-container dashboard-alert-redeem-button text-center hide" style="position:relative;margin-bottom: 20px;">
			<div class="alert alert-info alert-inbox" role="alert">
				<p>Successfully Sent Your Request.</p>
			</div>
		</div>
		<?php foreach($user_details as $result):?>
			<?php if(!empty($result->total_reward_point)): ?>
				<div class="col-md-12 redeem text-right">
					<input type="button" name="redeem_btn_modal" id='redeem_btn_modal' class="btn btn-lg btn-primary" value="Redeem Wallet Cash">
				</div>
			<?php endif ?>
		<?php endforeach ?>
		<div class="lead page-header">Wallet Cash</div>
		<div class="row">
			<div class="col-md-12">
			    <div class="panel-default panel-inverse">
    			    <div class="panel-heading">
    						Available Wallet Cash : <?php foreach ($user_details as $value) {
    							echo $value->total_reward_point;}?>
    				</div>
				</div>
				<div class="panel panel-default panel-inverse">
					<div class="panel-body">
						<ul class="nav nav-tabs" role="tablist"> 
							<li role="presentation" class="active">
								<a href="#points-history" aria-controls="points-history" role="tab" data-toggle="tab">
									Wallet Cash History
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane" id="points-history" style="display: block;">
								<?php if (empty($reward_points)): ?>
									<div class="alert alert-warning" style="margin-top:20px;margin-bottom:0px;">
										There is no Wallet Cash available.
									</div>
								<?php else: ?>
									<table class="table table-striped" style="margin:0;font-size:100%">
										<thead class="text-muted">
											<tr>
												<th>
													<div data-toggle="tooltip" >
														Order Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Request Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Redeem Request Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Credit Wallet Cash
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Debit Wallet Cash
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Date Ordered
													</div>
												</th>
											</tr>
										</thead>
										<tbody class="reward_points_history">
										
										<?php foreach ($reward_points as $value){ ?>
											<?php if(empty($value->credit_reward_point) && empty($value->debit_reward_point)): continue;?>
											<?php else:?>
											<tr>
												<td>
													<?php if(empty($value->order_id)):?>
														<?php echo '--'; ?>
													<?php else:?>	
													<a href="<?php echo site_url().'user_dashboard/my-orders/'.$value->order_id ?>"><?php 
													echo $value->order_id; ?>
													</a><?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->scrap_request_id)):?>
														<?php echo '--'; ?>
													<?php else:?>
														<a href="<?php echo site_url().'scrap_details_view/'.$value->scrap_request_id ?>">
															<?php 
													 echo str_pad((string)$value->scrap_request_id, 8, "0", STR_PAD_LEFT); ?>
															</a>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->redeem_request_id)):?>
														<?php echo '--'; ?>
													<?php else:?>
														<a href="<?php echo site_url().'redeem_request_details' ?>"><?php echo 
													 str_pad((string)$value->redeem_request_id, 8, "0", STR_PAD_LEFT); ?>
															</a>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->credit_reward_point)):?>
														<?php echo '--'; ?>
													<?php else: ?>	
														<?php echo $value->credit_reward_point;?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->debit_reward_point)):?>
														<?php echo '--'; ?>
													<?php else: ?>
													<?php echo $value->debit_reward_point;?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php echo date('d-m-Y H:i:s' ,strtotime($value->date));?>
												</td>
											</tr>
											<?php endif ?>
										<?php } ?>
										</tbody>
									</table>
									<div class="text-center" style="margin-top: 20px;">
									<input type="button" name="reward_point_btn" class="show_button btn btn-primary" value="Show More"></div>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	<!--<div class="lead page-header">My Redeem History</div>
		<div class="row">
			<div class="col-md-12">
			    <div class="panel-default panel-inverse">
			    <div class="panel-heading">
						Available Reward Points : <?php// foreach($user_details as $result)echo $result->total_reward_point;?>
				</div>
				</div>
				<div class="panel panel-default panel-inverse">
					<div class="panel-body">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#points-history" aria-controls="points-history" role="tab" data-toggle="tab">
									Redeem History
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane" id="points-history" style="display: block;">
								<?php// if (empty($redeem)): ?>
									<div class="alert alert-warning" style="margin-top:20px;margin-bottom:0px;">
										There are no reward points available.
									</div>
								<?php// else: ?>
									<table class="table table-striped" style="margin:0;font-size:100%">
										<thead class="text-muted">
											<tr>
												<th>
													<div data-toggle="tooltip" >
														Reddem Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Total Reward Point
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Request Date
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Status	
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Transaction Completed Date
													</div>
												</th>
											</tr>
										</thead>
										<tbody class="redeem_points_history">
										<?php //foreach ($redeem as $value){ ?>
											<tr>
												<td>
													<?php //if(empty($value->id)):?>
														<?php// echo '--'; ?>
													<?php// else:?>	
													<?php// echo $value->id; ?>
													<?php// endif ?>
												</td>
												<td class="text-center">
													<?php //if(empty($value->total_reward_point)):?>
														<?php //echo '--'; ?>
													<?php //else:?>
															<?php// echo $value->total_reward_point; ?>
													<?php //endif ?>
												</td>
												<td class="text-center">
													<?php //if(date('Y-m-d', strtotime($value->request_date))== '-0001-11-30'):?>
														<?php// echo 'NOT AVAILABLE'; ?>
													<?php //else:?><?php// echo //date('jS M Y' ,strtotime(
												//	 $value->request_date)); ?>
													<?php //endif ?> 
												</td>
												<td class="text-center">
													<?php //if(empty($value->status)):?>
														<?php //echo '--'; ?>
													<?php// else: ?>	
														<?php// echo $value->status;?>
													<?php //endif ?>
												</td>
												<td class="text-center">
													<?php// if(date('Y-m-d', strtotime($value->status_modify_date))== '-0001-11-30'):?>
														<?php //echo 'NOT AVAILABLE'; ?>
													<?php //else:?><?php// echo date('jS M Y' ,strtotime(
													 //$value->status_modify_date)); ?>
													<?php //endif ?>
												</td>
											</tr>
										<?php// } ?>
										</tbody>
									</table>
									<div class="text-center" style="margin-top: 20px;">
										<input type="button" name="redeem_show" class="redeem_show btn btn-primary" value="Show More">
									</div>
								<?php //endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
		<div id="myModal" class="modal">
          <div class="modal-content redeem_modal">
            <span class="close">&times;</span>
            <div>
            	<h4>Redeem wallet cash</h4>
				<span>Wallet cash will be credited to PayTM account linked to your mobile number <?= $user->phone ?>. If you want to receive cash in a different PayTM account, please contact us at 9804940000.</span><br><br>
            	<input type="button" class="btn btn-md btn-success" value="Submit" id="redeem_btn">
            	<input type="button" class="btn btn-md btn-danger" value="Cancel" id="redeem_btn_cancel">
            </div>
          </div>
        </div>
	</div>
</div>

<script type="text/javascript">
var modal = document.getElementById('myModal');
jQuery(document).ready(function(){	
	jQuery('.reward_points_history tr:gt(2)').hide();
	jQuery('.show_button').click(function() {
	    jQuery('.reward_points_history tr:gt(2)').toggle(1000);
	    var text = jQuery('.show_button').attr('value') == 'Show More' ? 'Show Less' : 'Show More';
	    jQuery('.show_button').attr('value',text);
	});
// 	jQuery('.redeem_points_history tr:gt(2)').hide();
// 	jQuery('.redeem_show').click(function() {
// 	    jQuery('.redeem_points_history tr:gt(2)').toggle(1000);
// 	    var text = jQuery('.redeem_show').attr('value') == 'Show More' ? 'Show Less' : 'Show More';
// 	    jQuery('.redeem_show').attr('value',text);
// 	});
    jQuery('#redeem_btn_modal').click(function(e){
		e.preventDefault();
		modal.style.display = "block";
	});
	jQuery("body").on('click','#redeem_btn_cancel,.close',function(event){
		modal.style.display = "none";
	});
	window.onclick = function(event) {
  		if (event.target == modal) {
    		modal.style.display = "none";
  		}
	}
	jQuery("body").on('click', '#redeem_btn',function(){
		jQuery.ajax({
	        'type' : 'POST',
	        'url' : "<?php echo base_url('redeem_request'); ?>",
	        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' },
	        'success' : function(data) {           
				jQuery(".alert-inbox-container").removeClass("hide");
				if(data==1){
				    modal.style.display = "none";
            		tag = "<p><strong>NOTICE! </strong> We Already Recieved Your Request. Please Wait For Some Time</p>";
            		jQuery(".alert-inbox-container").addClass("alert-danger");
            		jQuery(".alert-inbox-container").html(tag);
        		}
	        	if(data==2){
	        	    modal.style.display = "none";
	        		tag = "<p><strong>Success! </strong> Successfully Sent Your Request.</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-success");
	        		jQuery(".alert-inbox-container").removeClass("alert-danger");
	        		jQuery(".alert-inbox-container").html(tag);	
	        	}
	        	if(data==3){
	        	    modal.style.display = "none";
	        		tag = "<p><strong>Warning!</strong> User Not Availble.</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-danger");
	        		jQuery(".alert-inbox-container").removeClass("alert-success");
	        		jQuery(".alert-inbox-container").html(tag);
	        	}
	        	if(data==4){
	        	    modal.style.display = "none";
	        		tag = "<p><strong>Warning!</strong> You Don't Have Suffcient Reward Point.</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-danger");
	        		jQuery(".alert-inbox-container").removeClass("alert-success");
	        		jQuery(".alert-inbox-container").html(tag);
	        	}
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });          
	});
});

</script>	
<?php $this->load->view('public/templates/footer') ?>