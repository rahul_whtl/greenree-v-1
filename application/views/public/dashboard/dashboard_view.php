<?php $this->load->view('public/templates/header', array(
	'title' => 'Dashboard - GreenREE',
	'link' => 'account','active1'=>'dashboard'
)) ?>

<?php $this->load->view('public/dashboard/dashboard_header', array('active' => FALSE)) ?>
<!-- <div class="buy-product-box">
	<div class="container">
		<div class="col-md-4 buy_product">
			<input type="button" name="buy_product_btn" id='buy_product_btn' class="btn btn-lg btn-primary" value="Buy Your Product">
		</div>
		<div class="col-md-8 alert-inbox-container text-center hide" style="position:relative">
			<div class="alert alert-info alert-inbox" role="alert">
				<p>Successfully Sent Your Request.</p>
			</div>
		</div>
	</div>	
</div> -->
<div class="dashboard-box">
	<div class="container">
		<?php
			 if(!$user->email_verified && $user->email && strpos($user->email, '@') !== false && empty($this->session->flashdata('message'))){ ?>
				<div class="col-md-12" style="margin-bottom: 30px;">    
				    <div class="alert_msg"><!-- if scrap_order_check == 1; means customer is One Time Sell customer and his last request in Process(not completed) -->
				    	<div class="notification_div">
							<span class="notification_icon"></span>
						    <div class="alert alert-info">	
								<p><strong>Alert ! </strong>You haven't verified your email Id yet. Please click on the button to resend verify email notice to your inbox. <a href="<?php echo base_url('resend_verify_email') ?>"><input type="button" class="btn btn-md btn-primary" value="Resend verify email" id="resend_btn" style="margin-left:30px;"></a></p>
							</div>
						</div>
					</div>    
				</div>
		<?php }else if(!empty($this->session->flashdata('message'))){ ?>
				<div class="alert alert-msg-email alert-success">
					 <?php echo $this->session->flashdata('message');  ?>			 
				</div>
		<?php }?>
		<div class="lead page-header">My Dashboard</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-body">
								<h1 class="text-muted" style="margin-top:0"><span class="glyphicon glyphicon-briefcase"></span></h1>
								<div class="h5" style="margin-top:0;">
									<?php echo anchor('user_dashboard/my-orders', 'Shop Orders') ?>
								</div>
								View and Manage Shop Orders
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-body">
								<h1 class="text-muted" style="margin-top:0"><span class="glyphicon glyphicon-briefcase"></span></h1>
								<div class="h5" style="margin-top:0;">
									<?php echo anchor('user_dashboard/my-wallet-cash', 'My Wallet Cash') ?>
								</div>
								View Wallet Cash Hisory
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-body">
								<h1 class="text-muted" style="margin-top:0"><span class="glyphicon glyphicon-tags"></span></h1>
								<div class="h5" style="margin-top:0;">
									<?php echo anchor('redeem_request_details', 'Redeem History') ?>
								</div>
								View Redeem History
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-body">
								<h1 class="text-muted" style="margin-top:0"><span class="glyphicon glyphicon-tags"></span></h1>
								<div class="h5" style="margin-top:0;">
									<?php echo anchor('user_dashboard/my-scrap-orders', 'My Scrap Orders') ?>
								</div>
								View Scrap Orders
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-body">
								<h1 class="text-muted" style="margin-top:0"><span class="glyphicon glyphicon-tags"></span></h1>
								<div class="h5" style="margin-top:0;">
									<?php echo anchor('user_dashboard/my-products', 'My Products') ?>
								</div>
								Manage Your Products Listing
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-body">
								<h1 class="text-muted" style="margin-top:0"><span class="glyphicon glyphicon-shopping-cart"></span></h1>
								<div class="h5" style="margin-top:0;">
									<?php echo anchor('user_dashboard/my-wishlist', 'My Wishlist') ?>
								</div>
								Manage Your Wishlist
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
</div>

<script type="text/javascript">
jQuery(window).on("load", function () {
    if(jQuery(".alert-msg-email")){
    	jQuery("#message").hide();
    }
});
	jQuery(document).ready(function(){
			jQuery("body").on('click', '#buy_product_btn',function(){
			var product_id = 1234;
			var price      = 10;
			jQuery.ajax({
		        'type' : 'POST',
		        'url' : "<?php echo base_url('buy_product'); ?>",
		        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',product_id:product_id,price:price },
		        'success' : function(data) { alert(data);            
					jQuery(".alert-inbox-container").removeClass("hide");
					if(data==1){
	        		tag = "<p><strong>NOTICE! </strong> We Already Recieved Your Request. Please Wait For Some Time</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-danger");
	        		jQuery(".alert-inbox-container").html(tag);
	        		}
		        	if(data==2){
		        		tag = "<p><strong>Success! </strong> Successfully Sent Your Request.</p>";
		        		jQuery(".alert-inbox-container").addClass("alert-success");
		        		jQuery(".alert-inbox-container").rmoveClass("alert-danger");
		        		jQuery(".alert-inbox-container").html(tag);	
		        	}
		        	if(data==3){
		        		tag = "<p><strong>Warning!</strong> User Not Availble.</p>";
		        		jQuery(".alert-inbox-container").addClass("alert-danger");
		        		jQuery(".alert-inbox-container").removeClass("alert-success");
		        		jQuery(".alert-inbox-container").html(tag);
		        	}
		        	if(data==4){
		        		tag = "<p><strong>Warning!</strong> You Don't Have Suffcient Reward Point.</p>";
		        		jQuery(".alert-inbox-container").addClass("alert-danger");
		        		jQuery(".alert-inbox-container").removeClass("alert-success");
		        		jQuery(".alert-inbox-container").html(tag);
		        	}
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });          
		});
	});	
</script>

<?php $this->load->view('public/templates/footer') ?>
