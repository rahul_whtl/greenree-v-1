<?php $this->load->view('public/templates/header', array(
	'title' => 'Shop Orders - GreenREE',
	'link' => 'account'
)) ?>

<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'orders')) ?>
<div class="my-orders">
	<div class="container">
		<div class="lead page-header">Shop Orders</div>

		<?php if (empty($order_data)): ?>
			<div class="alert alert-warning">There are no orders available to view.</div>
		<?php else: ?>
		    <div class="panel-default panel-inverse">
    		    <div class="panel-heading">
    					<h4 class="panel-title">Orders History</h4>
    			</div>
			</div>
			<div class="panel panel-default panel-inverse">
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="">Order Number</th>
							<th class="text-center">Total Items</th>
							<th class="text-center">Total Value</th>
							<th class="text-center">Date</th>
							<th class="text-center">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($order_data as $row): $order_number = $row[$this->flexi_cart_admin->db_column('order_summary', 'order_number')]; ?>
						<tr>
							<td>
								<?php echo anchor('user_dashboard/my-orders/'.$order_number, $order_number) ?>
							</td>
							<td class="text-center">
								<?php echo number_format($row[$this->flexi_cart_admin->db_column('order_summary', 'total_items')]); ?>
							</td>
							<td class="text-center">
								<?php echo '&#8377;'." ".$row[$this->flexi_cart_admin->db_column('order_summary', 'total')]; ?>
							</td>
							<td class="text-center">
								<?php echo date('jS M Y', strtotime($row[$this->flexi_cart_admin->db_column('order_summary', 'date')])); ?>
							</td>
							<td class="text-center">
								<?php echo $row[$this->flexi_cart_admin->db_column('order_status', 'status')]; ?>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>		
	</div>
</div>		
<?php endif ?>

<?php $this->load->view('public/templates/footer') ?>