<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Change Password',
	'link' => 'account',
	// 'sub_link' => 'profile',
	// 'breadcrumbs' => array(
	// 	0 => array('name'=>'Dashboard','link'=>'user-dashboard'),
	// 	1 => array('name'=>'Profile','link'=>'profile'),
	// 	2 => array('name'=>'Change Password','link'=>FALSE)
	// )
)); ?>

<?php echo form_open(current_url()); ?>
<div class="change-password-part">
	<div class="container">
		<p>&nbsp;</p>
		<a href="<?php echo base_url('profile');?>">	
			<input type="button" class="btn btn-lg btn-primary" value="Back To Profile" />
		</a>
		<!--<div class="lead text-center">

			Change your password
			<!-- <h4>
				<small>
					Enter your new password to change your old password, If you have forgotten your password,
					the go to the <?php //echo anchor('forgot_password', 'Forgot Password Page') ?>.</small>
			</h4>

		</div>-->
    <div class="page-header">
	    <h4 class="lead">Change your password</h4>
    </div>    
    <div class="row change-password-form">
        <div class="col-md-12 col-sm-12">
    		<div class="row user_login">
    			<div class="col-xs-12 col-md-12">	
    				<div class="col-xs-12 col-md-12 form-group-password">
    					<div class="form-group <?= form_error('password') ? 'has-error' : '' ?>">
    						<label class="control-label" for="password">New Password<span class="red">*</span></label>
    						<input class="form-control password" type="password" name="password" value="<?= set_value('password') ?>" />
    						<div class="text-danger text-danger-password"><?= form_error('password') ? form_error('password') : '&nbsp' ?></div>
    					</div>
    				</div>
    				<div class="col-xs-12 col-md-12 form-group-confirm-password">
    					<div class="form-group <?= form_error('password_confirm') ? 'has-error' : '' ?>">
    						<label class="control-label" for="password_confirm">Confirm  New Password<span class="red">*</span></label>
    						<input class="form-control password_confirm" type="password" name="password_confirm" value="<?= set_value('password_confirm') ?>" />
    						<div class="text-danger text-danger-confirm-password"><?= form_error('password_confirm') ? form_error('password_confirm') : '&nbsp' ?></div>
    					</div>
    				</div>
    				
    				<div class="well-sm">
    					<input type="button" class="btn btn-lg btn-primary update_password" value="Update Password" data-toggle="modal" data-target="#confirm-alert-modal" name="change_password"/>
    				</div>
    				
    			</div>
    		</div>
		</div>
		<!--<div class="col-md-4 col-sm-4">-->
  <!--  		<div class="panel panel-default">-->
  <!--  			<div class="panel-heading">-->
  <!--  				<h4 class="panel-title">Password Tips</h4>-->
  <!--  			</div>-->
  <!--  			<div class="panel-body">-->
  <!--  				<ul class="row" style="padding-left: 15px;">-->
  <!--  					<li>-->
  <!--  						<strong>Use a strong password.</strong>-->
  <!--  						8 Characters and above, we recommend a mixture of alpha-numeric characters or symbols-->
  <!--  					</li>-->
  <!--  					<li>-->
  <!--  						<strong>Memorize your password.</strong>-->
  <!--  						Never write it down, we recommend logging in as much as possible without checking the "remember me".-->
  <!--  					</li>-->
  <!--  				</ul>-->
  <!--  			</div>-->
  <!--  		</div>-->
  <!--  	</div>-->
	</div>

		<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-content">
				<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Are You Sure, You want to Change Password ? </h4>
				</div>
				<div class="modal-body">
					<!--<div class="form-group <?//= form_error('old_password') ? 'has-error' : '' ?>">-->
					<!--	<input class="form-control" type="password" name="old_password" value="<?//= set_value('old_password') ?>" />-->
					<!--	<div class="text-danger"><?//= form_error('old_password') ? form_error('old_password') : '&nbsp' ?></div>-->
					<!--</div>-->
					<input type="submit" class="btn btn-lg btn-primary" name="change_password" value="Yes" />
					<input type="button" class="btn btn-lg btn-primary" id="pause_cancel" value="No"/>
				</div>
			</div>
		</div>
	</div>
</div>	
<?php echo form_close();?>
</div><script type="text/javascript">
var modal = document.getElementById('myModal');
jQuery(document).ready(function(){
	jQuery('.update_password').click(function(e){
		e.preventDefault();
		var password = jQuery('.password').val();
		var confirm_password = jQuery('.password_confirm').val();
		var password_length = password.length;
		if (password == '' || password == undefined) {
			var txt = '<p>Please Enter Valid Password.</p>';
			jQuery('.text-danger-password').html(txt);
			jQuery('.change-password-form .form-group-password').css('color','#a94442');
			if (confirm_password == '' || confirm_password == undefined) {
				var txt = '<p>Please Enter Valid Confirm Password.</p>';
				jQuery('.text-danger-confirm-password').html(txt);
				jQuery('.change-password-form .form-group-confirm-password').css('color','#a94442');
				return;
			}
			else{
				var txt = '<p></p>';
				jQuery('.text-danger-confirm-password').html(txt);
				jQuery('change-password-form .form-group-confirm-password').css('color','#333');
			}
			return;
		}
		else{
			var txt = '<p></p>';
				jQuery('.text-danger-password').html(txt);
				jQuery('.change-password-form .form-group-password').css('color','#333');
		}
		if (password_length < 8) {
			var txt = '<p>The above field must be at least 8 characters in length.</p>';
			jQuery('.text-danger-password').html(txt);
			jQuery('.change-password-form .form-group-password').css('color','#a94442');
			return;
		}
		else{
			var txt = '<p></p>';
				jQuery('.text-danger-password').html(txt);
				jQuery('.change-password-form .form-group-password').css('color','#333');
		}
		if (confirm_password == '' || confirm_password == undefined) {
			var txt = '<p>Please Enter Valid Confirm Password.</p>';
			jQuery('.text-danger-confirm-password').html(txt);
			jQuery('change-password-form .form-group-confirm-password').css('color','#a94442');
			return;
		}
		else{
			var txt = '<p></p>';
			jQuery('.text-danger-confirm-password').html(txt);
			jQuery('change-password-form .form-group-confirm-password').css('color','#333');
		}
		if (password != confirm_password) {
			var txt = '<p>Passwords do not match.</p>';
			jQuery('.text-danger-confirm-password').html(txt);
			jQuery('change-password-form .form-group-confirm-password').css('color','#a94442');
			return;
		}
		else{
			var txt = '<p></p>';
			jQuery('.text-danger-confirm-password').html(txt);
			jQuery('change-password-form .form-group-confirm-password').css('color','#333');
		}
		modal.style.display = "block";
	});
	jQuery('.close,#pause_cancel').click(function(){
		modal.style.display = "none";
	});
	window.onclick = function(event) {
  		if (event.target == modal) {
    		modal.style.display = "none";
  		}
	}
	jQuery(".change-password-form .password").focus(function(){
	    var txt = '';
    	jQuery('.change-password-form .text-danger-password').html(txt);
    	jQuery('.change-password-form .form-group-password').css('color','#333');
	});
	jQuery(".change-password-form .password_confirm").focus(function(){
	    var txt = '';
    	jQuery('.change-password-form .text-danger-confirm-password').html(txt);
    	jQuery('.change-password-form .form-group-confirm-password').css('color','#333');
	});
});
</script>

<?php $this->load->view('public/templates/footer') ?>