<?php $this->load->view('public/templates/header', array(
	'title' => 'Information',
	'link' => 'verify_email'
)) ?>
<div class="alert-box">
	<div class="container">
		<?php if($code): ?>
		<div class="alert-inbox-container text-center" style="position:relative">
			<div class="alert alert-info alert-inbox" role="alert">
				<p>Thanks For Verifying Your Email. Below Your Password is Mentioned. Please Don't Share Your Password With Anyone.</p>
			</div>
		</div>	
		<div>
			<p style="margin-top: 50px;"><strong>Password : </strong><?php echo $code; ?></p><br><br>
			<p style="margin-bottom: 50px;"><strong>Note : </strong> Once You login You can change Your Password.</p>
		</div>
		<?php else:?>
		<div>
			<div>
			<p style="margin-top: 50px;"><strong>This link is already expired.</p><br><br>
			<p style="margin-bottom: 50px;"><strong>Note : </strong>Use Forgot Password Link To recover your password.</p>
		</div>
	<?php endif ?>
		</div>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>