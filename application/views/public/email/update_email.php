<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
     <tr>
      <td align="center" valign="top" bgcolor="#f5f5f5">
        <!-- 600px container (white background) -->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 640px;
                 max-width: 600px;background-color: #fff;">
            <tr>
            	<td style="padding-left: 20px;border-bottom: 1px solid #CCC; width:45%">
            		<img src="<?php echo base_url('assets/images/icons/greenree-logo.png')?>" style="max-width:100%;"/>
            	</td>
            	<td style="padding-right: 20px;border-bottom: 1px solid #CCC;
    padding-top: 20px;padding-bottom: 20px;font-family: Arial; font-size:14px;    height: 30px;vertical-align: middle;line-height: 30px;text-align: right; width: 55%;">
            		<span style="margin-right: 10px;">Follow Us On : </span> <a href="https://www.facebook.com/ReuseUpcyle" target="_blank" style="float: right;"><img src="<?php echo base_url('assets/images/icons/rsz_facebook-7-xxl.png')?>" style="float: right;"></a>
            	</td>            	
            </tr>
            <tr>
            	<td colspan="2" style="padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px">
                    <?php if($request_type=='Periodic'): ?>
                    	<?php if($email_for=='user'): ?>
                    		<p style="padding-left: 20px;padding-right: 20px;">Hi <?php echo $user_name;?>,</p>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Your scrap request is paused succesfully.</p>
                    		<p style="padding-left: 20px;padding-right: 20px;">Your Periodic scrap request id :  <a href="<?php echo base_url('user_dashboard/my-scrap-orders');?>"><?php echo $scrap_id; ?></a></p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Note : </strong> You can resume your scrap request anytime.</p>
                    		<br/>
                    		<br/>
                    		<p>Regards,<br/>GreenREE</p>
                    	<?php else: ?>
                    		<p style="padding-left: 20px;padding-right: 20px;">Hi Admin,</p>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">One Scrap request is paused.</p>
                    		<p style="padding-left: 20px;padding-right: 20px;">Scrap request details :- </p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Scrap request id : </strong><a href="<?php echo base_url('scrap-request-admin');?>"><?php echo $scrap_id; ?></a></p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Scrap request type : </strong><?php echo $request_type; ?></p>
                    		<br/>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Regards,<br/>GreenRee</p>
                    	<?php endif ?>
                    <?php elseif($request_type=='Non-Periodic'): ?>
                    	<?php if($email_for=='user'): ?>
                    		<p style="padding-left: 20px;padding-right: 20px;">Hi <?php echo $user_name;?>,</p>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Your scrap request is canceled succesfully.</p>
                    		<p style="padding-left: 20px;padding-right: 20px;">Your one time sell scrap request id : <a href="<?php echo base_url('user_dashboard/my-scrap-orders');?>"><?php echo $scrap_id; ?></a></p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Note : </strong> You can book new scrap request anytime.</p>
                    		<br/>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Regards,<br/>GreenREE</p>
                    	<?php else: ?>
                    		<p>Hi Admin,</p>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">One Scrap request is canceled.</p>
                    		<p style="padding-left: 20px;padding-right: 20px;">Scrap request details :- </p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Scrap request id : </strong><a href="<?php echo base_url('scrap-request-admin');?>"><?php echo $scrap_id; ?></a></p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Scrap Request Type : </strong><?php echo $request_type; ?></p>
                    		<br/>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Regards,<br/>GreenRee</p>
                    	<?php endif ?>
                    <?php else: ?>	
                    	<?php if($email_for=='user'): ?>
                    		<p style="padding-left: 20px;padding-right: 20px;">Hi <?php echo $user_name;?>,</p>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Your scrap request is resumed succesfully.</p>
                    		<p style="padding-left: 20px;padding-right: 20px;">Your Periodic scrap request id : <a href="<?php echo base_url('user_dashboard/my-scrap-orders');?>"><?php echo $scrap_id; ?></a></p>
                    		<br/>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Regards,<br/>GreenREE</p>
                    	<?php else: ?>
                    		<p style="padding-left: 20px;padding-right: 20px;">Hi Admin,</p>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">One Scrap request is resumed.</p>
                    		<p style="padding-left: 20px;padding-right: 20px;">Scrap request details :- </p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Scrap request id : </strong><a href="<?php echo base_url('scrap-request-admin');?>"><?php echo $scrap_id; ?></a></p>
                    		<p style="padding-left: 20px;padding-right: 20px;"><strong>Scrap Request Type : </strong><?php echo 'Periodic'; ?></p>
                    		<br/>
                    		<br/>
                    		<p style="padding-left: 20px;padding-right: 20px;">Regards,<br/>GreenRee</p>
                    	<?php endif ?>
                    <?php endif ?>
                </td>	
            </tr>
            <tr>
            	<td bgcolor="#333" colspan="2" height="50" style="text-align: center;font-family: Arial; font-size:11px;">
            	    <span style="padding-left: 0;padding-right: 5px;"><a href="<?php echo base_url('sell-scrap') ?>" style="color: #fff;text-decoration: none;font-size:11px">SELL SCRAP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('shop') ?>" style="color: #fff;text-decoration: none;;font-size:11px">SHOP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('old-2-gold') ?>" style="color: #fff;text-decoration: none;font-size:11px">Old-2-Gold</a></span>
            	    <span style="padding-left: 10px;padding-right: 0;"><a href="<?php echo base_url('wishlist') ?>" style="color: #fff;text-decoration: none;font-size:11px">WISHLIST</a></span>
                </td>
            </tr>
            <tr>
            	<td bgcolor="#25a55b" colspan="2" height="30" style="font-family: Arial; font-size:12px; text-align: center; color: #fff">© 2019 GreenREE Pvt Ltd. All Copyright  Reserved.</td>
            </tr>
           </table>
       </td>
      </tr>
</table>            