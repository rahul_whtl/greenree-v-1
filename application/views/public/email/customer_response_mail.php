<?php $this->load->view('public/templates/header', array(
	'title' => 'Information',
	'link' => 'customer_availability'
)) ?>
<div class="apartment-otp-page">
	<div class="container">
		<div class="alert-box">
		<?php if (!empty($response)): ?>
			<?php if (!empty($response_status=='Late Response')): ?>
				<div class="alert-inbox-container text-center" style="position:relative">
					<div class="alert alert-warning alert-inbox" role="alert">
						<p>Your response is delayed. Your pick up might not happen on schedule time. Either cancel and book new scrap request or call on our helpline no. <a href="tel:980-494-0000">9804940000</a></p>
					</div>
				</div>
			<?php else: ?>
				<div class="alert-inbox-container text-center" style="position:relative">
					<div class="alert alert-info alert-inbox" role="alert">
						<p>Thanks For Response. We will Shortly send you Vendor Deatails.</p>
					</div>
				</div>	
			<?php endif ?>
			<div>
				<div class="apartment-otp col-md-6 form-group">
					<p style="margin-top: 50px;"><strong>Your Response : </strong><?php echo $response; ?></p><br>
					<label class="control-label">Enter your pre-approved OTP : </label>
					<input type="text" class="apartment-otp-text form-control input-md" placeholder="Pre-approved OTP">
					<input type="hidden" class="apartment-otp-scrap-id form-control input-md" value="<?php echo $scrap_id;?>">
					<div class="apartment-otp-danger"></div>
					<input type="button" class="btn btn-md btn-success" value="Submit" id="apartment_otp_submit"/>
					<a href="<?php echo base_url();?>">
						<input type="button" class="btn btn-md btn-danger" value="Skip"/>
					</a>
				</div>
			</div>
		<?php else: ?>	
			<div class="alert-inbox-container text-center" style="position:relative">
				<div class="alert alert-info alert-inbox" role="alert">
					<p>This link is expired or Unauthorized aceess.</p>
				</div>
			</div>
			<div>
				<p style="margin-top: 50px;">Please Contact Us For Support.</p><br><br>
			</div>
		<?php endif ?>
		</div>
		<div id="apartment-otp-page-modal" class="modal2">
            <div class="modal-content">
                <span class="close">&times;</span>
                <div>
                	<h4>Thanks for pre-approved OTP.</h4>
            		<span>Please be available in the assigned time slot. We will do our best to complete pickup on time.</span><br><br>
                	<input type="button" class="cancel btn btn-md btn-success" value="Close" id="cancel">
                </div>
            </div>
        </div>
	</div>
</div>
<script>
var modal2 = document.getElementById('apartment-otp-page-modal');
jQuery(document).ready(function(){
	jQuery('.cancel').click(function(e){
		e.preventDefault();
		modal2.style.display = "block";
		window.location.href = window.location.origin;
	});
	jQuery("body").on('click','.close',function(event){
		modal2.style.display = "none";
		window.location.href = window.location.origin;
	});
	window.onclick = function(event) {
  		if (event.target == modal2) {
    		modal2.style.display = "none";
  		}
	}
});
</script>
<?php $this->load->view('public/templates/footer') ?>