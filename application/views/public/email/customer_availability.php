<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
     <tr>
      <td align="center" valign="top" bgcolor="#f5f5f5">
        <!-- 600px container (white background) -->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 640px;
                 max-width: 600px;background-color: #fff;">
            <tr>
            	<td style="padding-left: 20px;border-bottom: 1px solid #CCC; width:45%">
            		<img src="<?php echo base_url('assets/images/icons/greenree-logo.png')?>" style="max-width:100%;"/>
            	</td>
            	<td style="padding-right: 20px;border-bottom: 1px solid #CCC;
    padding-top: 20px;padding-bottom: 20px;font-family: Arial; font-size:14px;    height: 30px;vertical-align: middle;line-height: 30px;text-align: right; width: 55%;">
            		<span style="margin-right: 10px;">Follow Us On : </span> <a href="https://www.facebook.com/ReuseUpcyle" target="_blank" style="float: right;"><img src="<?php echo base_url('assets/images/icons/rsz_facebook-7-xxl.png')?>" style="float: right;"></a>
            	</td>            	
            </tr>
            <tr>
            	<td colspan="2" style="padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px">
                	<?php if($email_for == 'user'):?>	
                		<div style="text-align: center">
                			<img src="" style="display: inline-block;" />
                		</div>
                		<p style="font-family: Arial; font-size:14px;">Hi <?php echo $name?>,</p>
                		<br/>
                		<p style="font-family: Arial; font-size:14px;">Admin has assaigned the following schedule for your Scrap pick up.</p>
                		<p style="font-family: Arial; font-size:14px;">Your Scrap request details :-</p>
                		<p style="font-family: Arial; font-size:14px;">
                		    Scrap request id : <a href="<?php echo base_url('user_dashboard/my-scrap-orders')?>"><?php echo $scrap_id;?></a>
                		</p>
                		<p style="font-family: Arial; font-size:14px;">
                		    Request type : <?php echo $request_type;?>
                		</p>
                		<?php $date = explode(" ", $pick_up_date);$time = $date[1]; $date = $date[0];?>
                		<p style="font-family: Arial; font-size:14px;">Scrap pick up date : <?php echo date('d/m/Y', strtotime($date)); ?></p>
                		<p style="font-family: Arial; font-size:14px;">Scrap pick up time : <?php echo date('h:i:s a', strtotime($time)).' - '.date('h:i:s a', strtotime($pick_up_max_time)); ?></p>
                		<?php if($email_comment): ?>
                		<p style="font-family: Arial; font-size:14px;"><?php echo $email_comment; ?></p>
                		<?php endif ?>	
                		<p style="font-family: Arial; font-size:14px;">Please give your response for availability during Scrap pick up.</p>
                		<span style="font-family: Arial; font-size:14px;"><a href="<?php echo base_url('customer_availability/'.$yes.'/'.$scrap_id.'/'.$date); ?>" target='_blank'>Yes</a></span>
                		<span><a href="<?php echo base_url("customer_availability/".$no."/".$scrap_id."/".$date); ?>" target='_blank'>No</a></span>
                		<br />
                		<br />
                		<p style="font-family: Arial; font-size:14px;">Regards,<br/>GreenREE</p>
                	<?php else: ?>
                		<div style="text-align: center">
                			<img src="" style="display: inline-block;" />
                		</div>
                		<p style="font-family: Arial; font-size:14px;">Hi Admin,</p>
                		<br/>
                		<p style="font-family: Arial; font-size:14px;">One email is sent to user regarding customer availability during Scrap pick up. </p>
                		<p style="font-family: Arial; font-size:14px;">Customer  Scrap request details :-</p>
                		<p style="font-family: Arial; font-size:14px;">Customer name : <?php echo $name?></p>
                		<p style="font-family: Arial; font-size:14px;">
                		    Scrap request id : <a href="<?php echo base_url('scrap-request-admin')?>"><?php echo $scrap_id;?></a>
                		</p>
                		<p style="font-family: Arial; font-size:14px;">
                		    Request type : <?php echo $request_type;?>
                		</p>
                		<?php $date = explode("%", $pick_up_date);$time = $date[1]; $date = $date[0];?>
                		<p style="font-family: Arial; font-size:14px;">Scrap pick up date : <?php echo date('d/m/Y', strtotime($date)); ?></p>
                		<p style="font-family: Arial; font-size:14px;">Scrap pick up time : <?php echo date('h:i:s a', strtotime($time)).' - '.date('h:i:s a', strtotime($pick_up_max_time)); ?></p>
                		<?php if($email_comment): ?>
                		<p style="font-family: Arial; font-size:14px;"><?php echo $email_comment; ?></p>
                		<?php endif ?>
                		<br />
                		<br />
                		<p style="font-family: Arial; font-size:14px;">Regards,<br/>GreenREE</p>
                	<?php endif ?>	
                </td>	
            </tr>
            <tr>
            	<td bgcolor="#333" colspan="2" height="50" style="text-align: center;font-family: Arial; font-size:11px;">
            	    <span style="padding-left: 0;padding-right: 5px;"><a href="<?php echo base_url('sell-scrap') ?>" style="color: #fff;text-decoration: none;font-size:11px">SELL SCRAP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('shop') ?>" style="color: #fff;text-decoration: none;;font-size:11px">SHOP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('old-2-gold') ?>" style="color: #fff;text-decoration: none;font-size:11px">Old-2-Gold</a></span>
            	    <span style="padding-left: 10px;padding-right: 0;"><a href="<?php echo base_url('wishlist') ?>" style="color: #fff;text-decoration: none;font-size:11px">WISHLIST</a></span>
                </td>
            </tr>
            <tr>
            	<td bgcolor="#25a55b" colspan="2" height="30" style="font-family: Arial; font-size:12px; text-align: center; color: #fff">© 2019 GreenREE Pvt Ltd. All Copyright  Reserved.</td>
            </tr>
           </table>
       </td>
      </tr>
</table>      