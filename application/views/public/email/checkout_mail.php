<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
     <tr>
      <td align="center" valign="top" bgcolor="#f5f5f5">
        <!-- 600px container (white background) -->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 640px;
                 max-width: 600px;background-color: #fff;">
            <tr>
            	<td style="padding-left: 20px;border-bottom: 1px solid #CCC; width:45%">
            		<img src="<?php echo base_url('assets/images/icons/greenree-logo.png')?>" style="max-width:100%;"/>
            	</td>
            	<td style="padding-right: 20px;border-bottom: 1px solid #CCC;
    padding-top: 20px;padding-bottom: 20px;font-family: Arial; font-size:14px;    height: 30px;vertical-align: middle;line-height: 30px;text-align: right; width: 55%;">
            		<span style="margin-right: 10px;">Follow Us On : </span> <a href="https://www.facebook.com/ReuseUpcyle" target="_blank" style="float: right;"><img src="<?php echo base_url('assets/images/icons/rsz_facebook-7-xxl.png')?>" style="float: right;"></a>
            	</td>            	
            </tr>
            <tr>
            	<td colspan="2" style="padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px">
            		<?php if($email_for=='user'):?>
						<p style="font-family: Arial; font-size:14px;">Hi <?php echo $username?>,</p>
						<br />
						<p style="font-family: Arial; font-size:14px;">Your Order is placed succesfully.</p>
						<p style="font-family: Arial; font-size:16px;"><strong>Order ID : </strong> <?php echo $order_id; ?></p>
						<table border="1" cellpadding="10" style="border-collapse: collapse">
							<thead style="font-family: Arial; font-size:14px;">
								<tr>
									<th width="300" style="text-align:left">Item Name</th>
									<th>Quantity</th>
									<th>Item Price (Rs.)</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($order_details as $order_details):?>	
							  <tr style="font-family: Arial; font-size:14px;">
							    <td><?php echo $order_details->ord_det_item_name; ?></td>
							    <td><?php echo $order_details->ord_det_quantity; ?></td>
							    <td><?php echo $order_details->ord_det_price_total; ?></td>
							  </tr>
							<?php endforeach ?>
							  <!--<?php// if($savings_total>0): ?>-->
    					<!--		  <tr style="font-family: Arial; font-size:14px;">-->
    					<!--    		 <td colspan="2" style="text-align:right"><strong>Total Price :</strong></td>-->
    					<!--    		 <td><?php //echo $ord_total; ?></td>-->
    					<!--    	  </tr>-->
							  <!--<?php //endif ?>-->
							  <tr style="font-family: Arial; font-size:14px;">
					    		 <td colspan="2" style="text-align:right"><strong>Total Price :</strong></td>
					    		 <td><?php echo $ord_total; ?></td>
					    	  </tr>
					    	  <tr style="font-family: Arial; font-size:14px;">
					    		 <td colspan="2" style="text-align:right"><strong>Wallet Cash Used: </strong></td>
					    		 <td><?php echo $debit_reward_point; ?></td>
					    	  </tr>
					    	  <tr style="font-family: Arial; font-size:14px;">
					    		 <td colspan="2" style="text-align:right"><strong>Balance Due: </strong></td>
					    		 <td><?php echo $cash_collect; ?></td>
					    	  </tr>
							</tbody>
						</table>
						<p style="font-family: Arial; font-size:15px;">Please click on the <a href="<?php echo base_url('user_dashboard/my-orders/'.$order_id) ?>">link</a> to see the order details in your dashboard</p>
						<br />
						<p style="font-family: Arial; font-size:15px;">Regards,<br/>GreenREE</p>
					<?php else: ?>
						<p style="font-family: Arial; font-size:14px;">Hi Admin,</p>
						<br />
						<p style="font-family: Arial; font-size:14px;">One user booked product.</p>
						<p style="font-family: Arial; font-size:16px;"><strong>Order ID : </strong> <?php echo $order_id; ?></p>
						<div>
							<h4 style="font-family: Arial; font-size:16px;">User Contact Details:</h4>
							<p style="font-family: Arial; font-size:14px;">Email Address : <?php echo $email; ?></p>
							<p style="font-family: Arial; font-size:14px;">Phone : <?php echo $phone; ?></p>
						</div>
						<div>
							<p style="font-family: Arial; font-size:15px;"><strong style="font-weight: 600; font-size:16px;">User Address : </strong><?php echo $flat_no.', '.$apartment_name.', '.$locality.', '.$city.', '.$state.', '.$country; ?><p>
						</div>
						<table border="1" cellpadding="10" style="border-collapse: collapse;text-align:center">
							<thead>
								<tr style="font-family: Arial; font-size:15px;">
									<th  width="300" style="text-align:left">Item Name</th>
									<th>Quantity </th>
									<th>item Price</th>
								</tr>
							</thead>
							<tbody>
							  <?php foreach($order_details as $order_details):?>	
							  <tr style="font-family: Arial; font-size:14px;">
							    <td><?php echo $order_details->ord_det_item_name; ?></td>
							    <td><?php echo $order_details->ord_det_quantity; ?></td>
							    <td><?php echo $order_details->ord_det_price_total; ?></td>
							  </tr>
							<?php endforeach ?>  
							  <tr style="font-family: Arial; font-size:14px;">
					    		 <td colspan="2" style="text-align:right"><strong>Total Price :</strong></td>
					    		 <td><?php echo $ord_total; ?></td>
					    	  </tr>
					    	  <tr style="font-family: Arial; font-size:14px;">
					    		 <td colspan="2" style="text-align:right"><strong>Wallet Cash Used: </strong></td>
					    		 <td><?php echo $debit_reward_point; ?></td>
					    	  </tr>
					    	  <tr style="font-family: Arial; font-size:14px;">
					    		 <td colspan="2" style="text-align:right"><strong>Balance Due: </strong></td>
					    		 <td><?php echo $cash_collect; ?></td>
					    	  </tr>
							</tbody>
						</table>
						<br />
						<br />
						<p style="font-family: Arial; font-size:15px;">Regards,<br/>GreenREE</p>
					<?php endif ?>
            	</td>
            </tr>
            <tr>
            	<td bgcolor="#333" colspan="2" height="50" style="text-align: center;font-family: Arial; font-size:11px;">
            	    <span style="padding-left: 0;padding-right: 5px;"><a href="<?php echo base_url('sell-scrap') ?>" style="color: #fff;text-decoration: none;font-size:11px">SELL SCRAP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('shop') ?>" style="color: #fff;text-decoration: none;;font-size:11px">SHOP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('old-2-gold') ?>" style="color: #fff;text-decoration: none;font-size:11px">Old-2-Gold</a></span>
            	    <span style="padding-left: 10px;padding-right: 0;"><a href="<?php echo base_url('wishlist') ?>" style="color: #fff;text-decoration: none;font-size:11px">WISHLIST</a></span>
                </td>
            </tr>
            <tr>
            	<td bgcolor="#25a55b" colspan="2" height="30" style="font-family: Arial; font-size:12px; text-align: center; color: #fff">© 2019 GreenREE Pvt Ltd. All Copyright  Reserved.</td>
            </tr>
           </table>
       </td>
      </tr>
</table>                