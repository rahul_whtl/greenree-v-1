<?php $this->load->view('public/templates/header', array(
	'title' => 'Dashboard',
	'link' => 'account','active1'=>'dashboard'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'items')) ?>

<div class="user-product-repost">
	<div class="container">
        <?php echo form_open_multipart(current_url().'#form',array('id'=>'edit-product','method'=>'post')) ?>
        <div class="row repost_product_form">
        	<div class="col-xs-12 col-sm-4 col-md-3">
                <div class="form-group <?= form_error('userfile') ? 'has-error' : '' ?>">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <label class="panel-title control-label" for="userfile">
                            	Product Image
                            </label>
                        </div>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail text-warning">
                            	<?php if($product->images){ ?>
                                	<img src="<?=base_url($this->data['app']['file_path_product'].$product->images) ?>">
                                <?php }else { ?>
                                	<div style="margin:3rem 0">No image selected</div>
                                <?php } ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail">
                            </div>
                            <div class="btn-group btn-block">
                                <div class="btn btn-success btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="product_img">
                                </div>
                                <a href="#" class="btn btn-danger remove-image" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                    <div class="text-danger"><?=form_error('product_img') ? form_error('product_img') : '&nbsp' ?></div>
                </div>
            </div>
        	<?php //print_r($product);?>
            <div class="col-xs-12 col-sm-8 col-md-9">
            	<div class="col-md-12 form-group-name form-group <?=form_error('name') ? 'has-error' : '' ?>" style="margin-bottom: 15px;">
            		<label for="usr">Item<span class="red">*</span></label>
            		<input type="text" name="name" id="product_name" class="form-control product_name" value="<?=$product->item_name ?>">
            		<div class="text-danger"><?=form_error('name') ? form_error('name') : '' ?></div>
            	</div>
            	<div class="col-md-4 form-group-price" style="margin-bottom: 15px;">
            		<label for="usr">Price(Rs.)<span class="red">*</span></label>
            		<input type="text" name="price" id="product_price" class="form-control product_price" value="<?=$product->estimated_price ?>">
            		<div class="text-danger"><?=form_error('price') ? form_error('price') : '' ?></div>
            	</div>
            	<div class="col-md-4 form-group-quantity" style="margin-bottom: 15px;">
            		<label for="usr">Quantity Available<span class="red">*</span></label>
            		<input type="number" min="1" name="quantity" id="product_quantity" class="form-control product_quantity" value="<?=$product->quantity ?>">
            		<div class="text-danger"><?=form_error('quantity') ? form_error('quantity') : '' ?></div>
            	</div>
            	<div class="col-md-4 form-group-category" style="margin-bottom: 15px;" class="form-group <?=form_error('category') ? 'has-error' : '' ?>"> 
            		<label for="usr">Category<span class="red">*</span></label>
            		<select name="category" id="product_category">
            		<option value="">Select</option>
            		<?php foreach ($categories as $key => $category){ ?>
            			<option value="<?php echo $category->id ?>" <?=($category->id == $product_cat->category_id ? "selected" : "" ); ?>><?=$category->name ?></option>
            		<?php } ?>
            		</select>
            		<div class="text-danger"><?=form_error('category') ? form_error('category') : '' ?></div>
            	</div>
            	<div class="col-md-12" style="margin-bottom: 35px;"> 
            		<label for="usr">Description:</label>
            		<textarea name="description" id="product_description" class="form-control product_description" rows="5" maxlength="100"><?= $product->description ?></textarea>
            	</div>
            	<input type="hidden" name="id" value="<?=$product->id ?>" />
            	<input type="hidden" name="old_img" value="<?=$product->images ?>" />
            	<div style="margin-bottom: 15px;">
            		<div class="col-sm-6">
            			<input type="submit" name="update_btn" class="btn btn-success form-control update_btn" value="Update" id="update_btn">
            		</div>
            	</div>
            </div>	
        </div>
        <?php echo form_close() ?>
        <p>&nbsp;</p>
	</div>
</div>
<script>
    jQuery(document).ready(function(){
        
        jQuery("#product_category").find('option:contains(Others)').appendTo(jQuery("#product_category"));
        
        jQuery('.remove-image').click(function(){
            jQuery('.fileinput-new img').attr('src','');
        });
		jQuery(".user-product-repost .update_btn").click(function(event){
            var product_name = jQuery(".repost_product_form #product_name").val();
		    var product_price  = jQuery('.repost_product_form #product_price').val();
		    var product_quantity = jQuery('.repost_product_form #product_quantity').val();
		    var product_category = jQuery('.repost_product_form #product_category').val();
		    
		    if (product_name=="" || product_name== undefined) {
            	var txt = '<p>Please Enter Product Name.</p>';
            	jQuery('.repost_product_form .form-group-name .text-danger').html(txt);
            	jQuery('.repost_product_form .form-group-name').css('color','#a94442');
            	if(product_price=='' || product_price == undefined){
			    	var txt = '<p>Please Enter Product Price.</p>';
	            	jQuery('.repost_product_form .form-group-price .text-danger').html(txt);
	            	jQuery('.repost_product_form .form-group-price').css('color','#a94442');
	            	if (product_quantity == ""   || product_quantity == undefined) {
	            	    var txt = '<p>Please Enter Product Quantity.</p>';
    	            	jQuery('.repost_product_form .form-group-quantity .text-danger').html(txt);
    	            	jQuery('.repost_product_form .form-group-quantity').css('color','#a94442');
    		        	if (product_category == "select" || product_category == ""   || product_category == undefined) {
        		        	var txt = '<p>Please Select Product Category.</p>';
        	            	jQuery('.repost_product_form .form-group-category .text-danger').html(txt);
        	            	jQuery('.repost_product_form .form-group-category').css('color','#a94442');
        		        	return false;
        		        }
    		        	return false;
    		        }
			        return false;
			    }
				return false;
            }
            else if(product_price=='' || product_price == undefined){
		    	var txt = '<p>Please Enter Product Price.</p>';
            	jQuery('.repost_product_form .form-group-price .text-danger').html(txt);
            	jQuery('.repost_product_form .form-group-price').css('color','#a94442');
            	if (product_quantity == ""   || product_quantity == undefined) {
		        	var txt = '<p>Please Enter Product Quantity.</p>';
	            	jQuery('.repost_product_form .form-group-quantity .text-danger').html(txt);
	            	jQuery('.repost_product_form .form-group-quantity').css('color','#a94442');
		        	if (product_category == "select" || product_category == ""   ||product_category == undefined) {
    		        	var txt = '<p>Please Select Product Category.</p>';
        	            	jQuery('.repost_product_form .form-group-category .text-danger').html(txt);
        	            	jQuery('.repost_product_form .form-group-category').css('color','#a94442');
    		        	return false;
    		        }
		        	return false;
		        }
		        return false;
		    }
	    	else if (product_quantity == ""   || product_quantity == undefined) {
	        	var txt = '<p>Please Enter Product Quantity.</p>';
            	jQuery('.repost_product_form .form-group-quantity .text-danger').html(txt);
            	jQuery('.repost_product_form .form-group-quantity').css('color','#a94442');
	        	if (product_category == "select" || product_category == ""   || product_category == undefined) {
		        	var txt = '<p>Please Select Product Category.</p>';
	            	jQuery('.repost_product_form .form-group-category .text-danger').html(txt);
	            	jQuery('.repost_product_form .form-group-category').css('color','#a94442');
		        	return false;
		        }
	        	return false;
	        }
	        else if (product_category == "select" || product_category == ""   || product_category == undefined) {
	        	var txt = '<p>Please Select Product Category.</p>';
            	jQuery('.repost_product_form .form-group-category .text-danger').html(txt);
            	jQuery('.repost_product_form .form-group-category').css('color','#a94442');
	        	return false;
	        }
	        else
	            return true;
		});
		jQuery(".repost_product_form .form-group-name input").focus(function(){
		    var txt = '';
        	jQuery('.repost_product_form .form-group-name .text-danger').html(txt);
        	jQuery('.repost_product_form .form-group-name').css('color','#333');
		});
		jQuery(".repost_product_form .form-group-price input").focus(function(){
		    var txt = '';
        	jQuery('.repost_product_form .form-group-price .text-danger').html(txt);
        	jQuery('.repost_product_form .form-group-price').css('color','#333');
		});
		jQuery(".repost_product_form .form-group-quantity input").focus(function(){
		    var txt = '';
        	jQuery('.repost_product_form .form-group-quantity .text-danger').html(txt);
        	jQuery('.repost_product_form .form-group-quantity').css('color','#333');
		});
		jQuery(".repost_product_form .form-group-category select").click(function(){
		    var txt = '';
        	jQuery('.repost_product_form .form-group-category .text-danger').html(txt);
        	jQuery('.repost_product_form .form-group-category').css('color','#333');
		});
    });
</script>
<?php $this->load->view('public/templates/footer') ?>