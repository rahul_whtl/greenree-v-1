<?php $this->load->view('public/templates/header', array(
	'title' => 'Dashboard',
	'link' => 'account'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'items')) ?>

<div class="closing-view">
	<div class="container">
	
	<?php if(!empty($this->session->flashdata('error'))){ ?> 
	<div class="message">
		<div class="alert alert-danger">
			 <?php echo $this->session->flashdata('error');  ?>			 
		</div>
	</div>
	<?php } ?>
	
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 closing-view-div">
    	
    	<?php echo form_open(current_url().'#form', 'class="tumbnail close-product"') ?>
			<?php if (validation_errors()): ?>
				<div class="alert alert-danger animated fadeInDown" id="message">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					The form was submitted with errors, please check the form and try again.
				</div>
			<?php else: ?>
                <div class="message">
            		<div class="alert alert-info">
            			 Your product will be permanently closed.		 
            		</div>
            	</div>
			<?php endif ?>
			<div class="row">

				<?php if ($this->data['user']): // Don't include for logged in users. we shall get if from the DB. ?>
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group <?=form_error('reason') ? 'has-error' : '' ?>">
						<label class="control-label" for="reason">Reason for closing</label>
						<select class="form-control" name="reason" id="reason" value="<?=set_value('reason') ?>">
							<option value="Soled Out">Sold Out</option>
							<option value="Not Interested">Not Interested</option>
							<option value="Other">Other</option>
						</select>
						<div class="text-danger"><?=form_error('reason') ? form_error('reason') : '&nbsp' ?></div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 close-comment" style="">
					<div class="form-group">
						<label class="control-label" for="close-comment">Comment</label>
						<textarea name="close-comment" class="form-control" rows="5"><?=set_value('close-comment') ?></textarea>
					</div>
				</div>
				
				<input type="hidden" value="<?php echo $product_id ?>" name="prod_id"/>
				
				<?php endif ?>
			</div>
			
			<div class="form-group row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-right">
					<input type="submit" name="close_product" value="Submit" class="btn btn-md btn-primary"></input>
					<a href="<?php echo base_url('user_dashboard/my-products'); ?>"><input type="button" value="Cancel" class="btn btn-md btn-danger"></input></a>
				</div>
			</div>
			
		<?php echo form_close() ?>
		<!-- end of Inquiry Form -->
    	
    </div>	
</div>
	<p>&nbsp;</p>
	</div>
</div>
<script>
jQuery(document).ready(function(){
	
	jQuery('#reason').on('change', function (e) {
    var optionSelected = this.value;
	    if(optionSelected == 'Other'){
			jQuery('.close-comment').slideToggle();
		}else{
			jQuery('.close-comment').hide("slow");
		}
	});
	
});
</script>
<?php $this->load->view('public/templates/footer') ?>