<?php $this->load->view('public/templates/header', array(
	'title' => "Buying a preowned or used item is honoured - It's a Green cause. - GreenREE",
	'title_description' => 'Before buying or selling a pre-owned or used item at anywhere else, just try GreenREE, no charges from either side.',
	'menu_active' =>'user_products')) ?>
	<!--<div class="banner-section">
	<div class="container">
		<p class="banner-caption">Pre Owned Items</p>
	</div>
</div>-->
<div class="products sell">
    <p>&nbsp;</p>
	<div class="container">
		<?php /* <div class="alert-inbox-container">
			<div class="notification_div">
				<span class="notification_icon"></span>
				<div class="alert alert-info alert-inbox" role="alert">
					<p>If you want to sell the product, Please click on button to add your product.  <a href="<?php echo base_url("dashboard/add-new-product") ?>" class="btn btn-md btn-primary">Add my product for sale</a></p>
				</div>
			</div>
		</div> */ ?>
<div class="row">
    <div class="col-md-2 col-sm-3 col-xs-12">
        
        <?php if(sizeof($categories)){ ?>
        <div class="hidden-xs">
            <a href="<?php echo base_url('dashboard/add-new-product');?>" class="shop-add-new-product"><input type="button" class="btn btn-primary btn-md"value="Add My Product"></a>
		<div class="lead">Shop by category</div>
        <ul class="list">
        <?php foreach ($categories as $key => $category){ 
                if(!empty($category->products)) {
            ?>
            <li><?php echo anchor('old-2-gold/category/'.$category->slug, $category->name) ?></li>
        <?php } }?>
        </ul>
		</div>
		<?php } ?>
		
		<?php if(sizeof($categories)){ ?>
		<a href="<?php echo base_url('dashboard/add-new-product');?>" class="shop-add-new-product1"><input type="button" class="btn btn-primary btn-md"value="Add My Product"></a>
		<div class="dashboard-header hidden-sm hidden-md hidden-lg mobile-cat">
        	<div class="container">
        	    <a href="#" class="mobile-menu-link"><span>Shop by category</span><span class="caret"></span></a>
    		    <ul class="nav nav-tabs responsive-tab" role="tablist">
        		    <?php if(sizeof($categories)){ ?>
                    <?php foreach ($categories as $key => $category){
                            if(!empty($category->products)){    
                            ?>
                        <li><?php echo anchor('old-2-gold/category/'.$category->slug, $category->name) ?></li>
                    <?php } } } ?>
        		</ul>
        	</div>
        </div>
        <?php } ?>
        
        <script type="text/javascript">
        		jQuery(document).ready(function(){
        			jQuery(".mobile-menu-link").click(function(event){
        				event.preventDefault();
        				jQuery(".mobile-cat .responsive-tab").slideToggle();
        				jQuery(".mobile-cat .caret").toggleClass('active');
        			});	
        		});
        </script>
		
	</div>
      <div class="col-md-10 col-sm-9 col-xs-12 item-wrap user_product_cat">
		<?php $this->load->view('public/user_products/user_product_tiles_view', array(
			'type' => 'tiles',
			'cols' => 'col-xs-6 col-sm-4 col-md-3 col-lg-3',
			'products' => $products,
		)) ?>
		
		</div>
    </div>
	</div>
</div>
<p>&nbsp;</p>

<?php $this->load->view('public/templates/footer') ?>