<?php $this->load->helper('text') ?>
<?php //print_r($products); ?>
	<?php foreach ($products as $key => $product): ?>
		<div class="item <?php echo $cols ?>">
			<div class="thumbnail text-center">
			    <a href="<?php echo base_url('old-2-gold/product/'.$product->slug) ?>">
				<?php if ($product->images): ?>
					<img src="<?php echo base_url($this->data['app']['file_path_product'].$product->images) ?>" class="group list-group-image" style="margin-bottom:2px">
				<?php else: ?>
					<img src="<?php echo base_url() ?>assets/system/no_image.jpg" class="group list-group-image" style="width:100%">
				<?php endif ?>
				</a>
				<div class="caption" style="padding:0">
					<div class="form-group" title="<?php echo character_limiter($product->description, 100) ?>">
						<?php echo anchor('old-2-gold/product/'.$product->slug, character_limiter($product->item_name, 20)) ?>
					</div>
					<strong><?php echo $this->flexi_cart->get_currency_value($product->estimated_price) ?></strong>
				</div>
			</div>
		</div>
	<?php endforeach ?>
<?php if (isset($this->pagination)): ?>
	<!-- Pagination Link -->
	<?php echo $this->pagination->create_links() ?>
<?php endif ?>
