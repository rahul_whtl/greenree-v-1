<?php $this->load->view('public/templates/header') ?>
<div class="banner-part">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12 home-slider">
			<?php if ($banners): ?>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-bottom: 2rem">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php foreach ($banners as $key => $banner): ?>
						<li data-target="#carousel-example-generic" data-slide-to="<?php echo $key ?>" class="<?php echo $key ? 'active' : '' ?>"></li>
						<?php endforeach ?>
					</ol>
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<?php 
						$first = true;
						foreach ($banners as $key => $banner): ?>
							<div class="item <?php echo $first ? 'active' : '' ?>">
								<img src="<?php echo base_url($banner->image) ?>" alt="..." style="width:100%">
								<?php if ($banner->caption): ?>
									<div class="lead carousel-caption">
										<?php echo $banner->caption ?>
									</div>
								<?php $first= false; endif ?>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			<?php endif ?>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 banner-side-image">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image1">
						<img src="<?php echo base_url('assets/images/icons/girl-with-mobile.jpg') ?>" alt="girl-with-mobile">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image2">
						<img src="<?php echo base_url('assets/images/icons/gift-box.jpg') ?>" alt="gift-box">
					</div>
				</div>	
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image3">
						<img src="<?php echo base_url('assets/images/icons/billing-machine.jpg') ?>" alt="billing-machine">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 banner-side-image4">
						<img src="<?php echo base_url('assets/images/icons/wishlist-image.jpg') ?>" alt="wishlist-image">
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<div class="products sell">
	<div class="container">
		<div style="margin: 20px 0;">
			<input type="button" name="add_new_product" id="add_new_product" class="btn btn-success btn-lg" value="Add New Product">
		</div>
<?php foreach ($categories as $key => $category): ?>
		<div class="lead page-header">
			<?php echo anchor('category/'.$category->slug.'/'.url_title($category->name), $category->name) ?>
		</div>
		<?php $this->load->view('public/user_products/user_product_tiles_view', array(
			'type' => 'tiles',
			'cols' => 'col-xs-6 col-sm-4 col-md-3 col-lg-3-5',
			'products' => $category->products,
		)) ?>
		<p class="text-center">
			<?php //echo anchor('category/'.$category->slug.'/'.url_title($category->name), 'View More '.$category->name) ?>
		</p>
	<?php endforeach ?>
	</div>
</div>


<?php $this->load->view('public/templates/footer') ?>