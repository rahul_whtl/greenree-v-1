<?php

$breadcrumbs = array();
// Create page breadcrumbs from category tiers
foreach ($category->pagination as $category)
{
	// Add formatted names and links to the breadcrumbs
	array_push($breadcrumbs, array(
		'name' => $category->name,
		'link' => 'old-2-gold/category/'.$category->slug
	));
}
// Add the product to the breadcrumbs
array_push($breadcrumbs, array(
	'name' => $product->item_name,
	'link' => FALSE // No need for linking this very page
));

?>
<?php $this->load->view('public/templates/header', array(
	'title' => $product->item_name.' - GreenREE',
	'title_description' => $product->description,
	'menu_active' =>'user_products'
)); ?>
<div class="product-view user-product-view">
	<div class="container">
	    <?php if(!empty($product)): ?>
    		<div class="alert-inbox-container text-center" style="position:relative">
    		    <div class="notification_div">
        			<div class="alert alert-info alert-inbox" role="alert">
        				<p>If you want to sell the product, Please click on button to add your product. <a href="<?php echo base_url('dashboard/add-new-product'); ?>" class="btn btn-md btn-primary">Add my product</a></p>
        			</div>
        		</div>
    		</div>
        	<?php if(!empty($this->session->flashdata('alert'))){ ?>
        	    <?php $alert = $this->session->flashdata('alert'); ?>
        		<div class="alert alert-<?php echo $alert['type']?>">
        			 <?php echo $alert['message']; ?>			 
        		</div>
	        <?php } ?>
            <h1 class="page-header product-name">
            	<?php echo $product->item_name ?>
            </h1>
            <div class="row">
            	<!-- carousel -->
            	<div class="col-xs-12 col-sm-6 col-md-7 col-lg-6">
            		<div class="text-center">
            			<?php if ($product->images): ?>
            				<img src="<?php echo base_url($this->data['app']['file_path_product'].$product->images) ?>" class="img-responsive" style="display:initial">
            			<?php else: ?>
            				<img src="<?php echo base_url() ?>assets/system/no_image.jpg" class="group list-group-image" style="width:100%">
            			<?php endif ?>
            		</div>
            	</div>
                <!-- end of carousel -->
            
            	<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
            		<p><?php echo $product->description ?></p>
            		<p><strong>Location</strong> : <?php echo $product->apartment_name.', '.$product->locality.', '.$product->city.', '.$product->state; ?></p>
            		<!-- //.' - '.$product->pin.' '.$product->country ; ?></p> -->
            		<p><strong>Available Quantity</strong> : <?php echo$product->quantity ?></p>
            		<div class="product-details"></div>
            		<?php //if($this->data['user']){ ?>
            		<!-- Inqury Form. -->
            		<?php //echo form_open(current_url().'#form', 'class="tumbnail"') ?>
            			<?php if (validation_errors()): ?>
            				<div class="alert alert-danger animated fadeInDown" id="message">
            					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            						<span aria-hidden="true">&times;</span>
            					</button>
            					The form was submitted with errors, please check the form and try again.
            				</div>
            			<?php endif ?>
            			<div class="form-group">
            				<input type="button" name="<?php echo $user->id ;?>" value="View contact info" class="btn btn-md btn-primary" id="send_inquiry">
            			</div>
            		<?php //echo form_close() ?>
            		<!-- end of Inquiry Form -->
            		<?php //}else{ ?>
            			<!-- Trigger the modal with a button -->
            			<button type="button" class="btn btn-md btn-primary hide" data-toggle="modal" data-target="#myModal" id="send_inquiry1">View contact info</button>
            		<?php //} ?>
            	</div>
            </div>
            <hr />
            <!--<div class="text-center">-->
            <!--	<button id="nav-to-top" class="btn btn-md btn-default" style="position: fixed;bottom:10px;right: 10px;z-index: 100;width:auto;">-->
            <!--		<span class="glyphicon glyphicon-chevron-up"></span> up-->
            <!--	</button>-->
            <!--</div>-->
    </div>
        <?php else :?>
            <div class="notification_div">
				<p>There is no information available for this product.</p>
    		</div>
        <?php endif ?>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please enter your details</h4>
      </div>
      <div class="modal-body">
      	<?php //echo form_open(current_url().'#form', 'class="tumbnail"') ?>
			<div class="row inquiry">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group">
						<label class="control-label" for="name">Name</label>
						<input type="text" class="form-control name" name="name"/>
						<div class="text-danger-name"></div>
					</div>
				</div>
				<input type="hidden"  />
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group">
						<label class="control-label" for="email">Email address</label>
						<input type="text" class="form-control email"/>
						<div class="text-danger-email"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group">
						<label class="control-label" for="phone">Phone</label>
						<input type="text" class="form-control mobile_number" name="phone"/>
						<div class="text-danger-phone"></div><br>
						<button type="button" class="btn product_generate_otp_btn">Generate OTP</button><br>
						 <div class="otp_div hide">
					        <input type="text" placeholder="Enter OTP" class="otp form-control otp_value" name="otp"/>
					        <input type="button" class="btn product_resend_otp_btn" value="Resend OTP">
					        <div class="text-danger-otp"></div>
					    </div>
					</div>
				</div>
			</div>
			
			<div class="form-group row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-right">
					<input type="submit" name="inquiry_product" value="View contact info" class="btn btn-md btn-primary inquiry_product" disabled>
					<input type="button" id="inquiry_product_verify_otp" value="Send" class="btn btn-md btn-primary hide">
				</div>
			</div>
		<?php// echo form_close() ?>
        </div>
    </div>

  </div>
</div>
<div id="product_info_popup" class="product_info_popup">
  <div class="modal-content">
    <div class="alert alert-success col-md-10"><strong>Success!</strong> Your inquiry is sent to seller successfully.</div>
    <span class="close_info cancel close" style="margin:0">&times;</span>
    <div class="div-container" method="POST" id="div-container">
    	<label for="customer_info_heading"><h3>Seller Information</h3></label>
	    <div class="product_info_text">
	    	
	    </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
function validateEmail(email) {
   var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   return re.test(String(email).toLowerCase());
}
var modal = document.getElementById('product_info_popup');
	jQuery(document).ready(function(){
	    jQuery('.close_info').click(function(event){
          event.preventDefault();
          jQuery("#product_info_popup").css('display','none');
        });
        window.onclick = function(event) {
      		if (event.target == modal) {
        		modal.style.display = "none";
      		}
    	}
		jQuery('#send_inquiry').click(function(event){
			var user_id  = jQuery("#send_inquiry").attr('name');
			var pathname = window.location.pathname;
	        var array    = pathname.split('/');
	        var slug    = array[3];
	        var inquiry_product = 'inquiry_product';
			if (user_id =='' || user_id == undefined ) {
				jQuery('#send_inquiry1').click(); 
				jQuery('.inquiry .name').val('');
				jQuery('.inquiry .email').val('');
				jQuery('.inquiry .mobile_number').val('');
				var txt = ' ';
                jQuery('.inquiry .text-danger-email,.inquiry .text-danger-phone, .inquiry .text-danger-name').html(txt);
                jQuery('.inquiry .text-danger-email p,.inquiry .text-danger-phone p, .inquiry .text-danger-name p').css('color','#fff');
			}else{
				jQuery.ajax({
		            type : 'POST',
		            url : "<?php echo base_url('old-2-gold/product'); ?>"+'/'+slug,
		            data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',slug:slug,inquiry_product:inquiry_product},
		            'success' : function(data){ 
		                //console.log(data);
		            	var data = jQuery.parseJSON(data);
    	            	var new_data = '<p><strong>Description : </strong>'+data.description+'</p><p><strong>Location : </strong>'+data.apartment_name+', '+data.locality+', '+data.city+', '+data.state+', '+'</p><p><strong>Available Quantity : </strong>'+data.quantity+'</p><p><strong>Name : </strong>'+data.first_name+'</p><p><strong>Phone : </strong>'+data.phone+'</p><p><strong>Email : </strong>'+data.email+'</p>';
		            	jQuery('.product_info_text').html(new_data);
	                    jQuery('#product_info_popup').css("display", "block");
	                    jQuery('#myModal').css("display", "none");
		            },
		            'error' : function(request,error){
		                alert("Request: "+JSON.stringify(request));
		            }
	        	});
			}
		});
		jQuery("body").on('click', '.inquiry_product',function(){
        	var name     = jQuery('.inquiry .name').val();
	        var email    = jQuery('.inquiry .email').val();
	        var contact_number = jQuery('.inquiry .mobile_number').val();
	        var otp      = jQuery('.inquiry .otp_value').val();
	        var error    = false;
        	if (name == '' || name == undefined) {
        	    error = true;
        	    var p = '<p>Please enter ypur name.</p>';
        	    jQuery('.text-danger-name').html(p);
	        }
	        if (email == '' || email == undefined) {
	            error = true;
	            var p = '<p>Please enter email id.</p>';
        	    jQuery('.text-danger-email').html(p);
	        }
	        if(!validateEmail(email)){
			    //jQuery(".form-group-email").addClass("has-error");
        		var p = "<p>Please enter valid email id.</p>"
                jQuery(".text-danger-email").html(p);
                error = true;
			}
	        if (contact_number == '' || contact_number == undefined) {
	            error = true;
	            var p = '<p>Please enter phone number.</p>';
        	    jQuery('.text-danger-phone').html(p);
	        }
	        if (otp == '' || otp == undefined) {
	            error = true;
	            var p = '<p>Please enter otp.</p>';
        	    jQuery('.text-danger-otp').html(p);
    	        jQuery('.text-danger-otp').css('color','#a94442');
	        }
	        if(error){
	            return false;
	        }
	        jQuery.ajax({
		        'type' : 'POST',
		        'url' : "<?php echo base_url('verify_otp'); ?>",
		        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp },
		        'success' : function(data) {   console.log(data);         
		            if(data == 1){
		                var tag = "<p></p>";
		        		jQuery(".text-danger-otp").html(tag);
		        		jQuery("#inquiry_product_verify_otp").click();
		        	}
		        	else if(data == 2){
		                var p = "<p>OTP is expired. Please try again.</p>"
		                jQuery(".text-danger-otp").html(p);
    	                jQuery('.text-danger-otp').css('color','#a94442');
		        		return false; 
		        	}
		        	else if(data == 3){
		        		var p = "<p>Incorrect OTP. Please try again.</p>"
		                jQuery(".text-danger-otp").html(p);
    	                jQuery('.text-danger-otp').css('color','#a94442');
		        		return false; 
		        	}
		        	else{
		                var p = "<p>Error</p>"
		                jQuery(".text-danger-otp").html(p);
    	                jQuery('.text-danger-otp').css('color','#a94442');
		        		return false; 
		        	}
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });
		});
		jQuery('#inquiry_product_verify_otp').click(function(event){
			//event.preventDefault();
			var pathname = window.location.pathname;
	        var array    = pathname.split('/');
	        var slug     = array[3];
	        var inquiry_product = 'inquiry_product';
	        var name     = jQuery('.inquiry .name').val();
	        var email    = jQuery('.inquiry .email').val();
	        var phone    = jQuery('.inquiry .mobile_number').val();
	        var otp      = jQuery('.inquiry .otp_value').val();
	        if (otp == '' || otp == undefined) {
	        	alert('error');return;
	        }
			jQuery.ajax({
	            type : 'POST',
	            url : "<?php echo base_url('old-2-gold/product'); ?>"+'/'+slug,
	            data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',slug:slug,inquiry_product:inquiry_product,name:name,email:email,phone:phone,otp:otp},
	            'success' : function(data){
	            	var data = jQuery.parseJSON(data);
	            	// console.log(data);
	            	//alert(data);
	            	var new_data = '<p><strong>Description : </strong>'+data.description+'</p><p><strong>Location : </strong>'+data.apartment_name+', '+data.locality+', '+data.city+', '+data.state+', '+'</p><p><strong>Available Quantity : </strong>'+data.quantity+'</p><p><strong>Name : </strong>'+data.first_name+'</p><p><strong>Phone : </strong>'+data.phone+'</p><p><strong>Email : </strong>'+data.email+'</p>';
	            	jQuery('.product_info_text').html(new_data);
                    jQuery('#product_info_popup').css("display", "block");
	                jQuery('#myModal').css("display", "none");    
	            },
	            'error' : function(request,error){
	                alert("Request: "+JSON.stringify(request));
	            }
        	});
		});
		jQuery('.inquiry .mobile_number').on('focus', function(){
		   var p = '';
		   jQuery('.text-danger-phone').html(p);
		});
		jQuery(".inquiry .email").focus(function(){
		    var p = '';
		    jQuery('.text-danger-email').html(p);
    	});
    	jQuery(".inquiry .name").focus(function(){
		   var p = '';
		   jQuery('.text-danger-name').html(p);
    	});
    	jQuery("body").on('click',".inquiry .otp_value",function(){
		   var p = '';
		   jQuery('.text-danger-otp').html(p);
    	});
	});
</script>
<style type="text/css">
	.modal-backdrop{position: relative;}
	.product-view .modal-content{width: auto;}
</style>
<?php $this->load->view('public/templates/footer') ?>