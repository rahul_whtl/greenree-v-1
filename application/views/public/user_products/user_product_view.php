<?php $this->load->view('public/templates/header', array(
	'title' => 'My Products - GreenREE',
	'link' => 'account'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'items')) ?>

<div class="product-view dashboard-product-view">
	<div class="container">
	
	<?php if(!empty($this->session->flashdata('message'))){ ?> 
	<div class="message">
		<div class="alert alert-success">
			 <?php echo $this->session->flashdata('message');  ?>			 
		</div>
	</div>
	<?php } ?>
	
    <div class="product-list">
		<div class="lead page-header">
			My Product List
		</div>
		<div class="dashboard-add-product-btn">
			<a href="<?php echo base_url('dashboard/add-new-product'); ?>" class="btn btn-success btn-lg dashboard-product-list-add-btn">Add New Product</a>
		</div>
		
		<?php if(!empty($products)){ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						Product List
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="Periodic">
								<table class="table table-striped" style="margin:0;font-size:100%;vertical-align:middle">
									<thead class="text-muted">
										<tr>
										    <th>
												<div  class="text-center" data-toggle="tooltip" >
													Image
												</div>
											</th>
											<th>
												<div class="text-center" data-toggle="tooltip" >
													Name
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Price
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Available Quantity
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Status
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Category
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Action
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($products as $product){ ?>
										<tr>
										    <td class="text-center" style="width:100px">
												<?php if ($product->images): ?>
                            					<img src="<?php echo base_url($this->data['app']['file_path_product'].$product->images) ?>" class="group list-group-image" style="margin-bottom:2px">
                            				<?php else: ?>
                            					<img src="<?php echo base_url() ?>assets/system/no_image.jpg" class="group list-group-image" style="width:100%">
                            				<?php endif ?>
											</td>
											<td class="text-center">
												<?php echo $product->item_name ?>
											</td>
											<td class="text-center">
												<?php echo $product->estimated_price ?>
											</td>
											<td class="text-center">
												<?php echo $product->quantity ?>
											</td>
											<td class="text-center">
												<?php 
												$today = strtotime(date('Y-m-d H:i:s'));
												$due_date = strtotime($product->expiry_datetime);
												if($product->status == 1 && $today < $due_date){?>
													ACTIVE
												<?php }else if($product->status == 1 && $today > $due_date){?>
													<span class="blinking">EXPIRED</span>
												<?php }else{ ?>
													CLOSED
												<?php } ?>				
											</td>
											<td class="text-center">
												<?php echo $product->category ?>
											</td>
											<td class="text-center" style="min-width: 462px;">
							<?php if($product->status == 1 && $today < $due_date){ ?>
							
									<a href="<?php echo base_url(); ?>edit_my_product/<?php echo $product->id ?>" class="btn btn-success btn-md edit_btn dashboard-product-list-edit-btn">Edit</a>
									<a href="<?php echo base_url(); ?>view_buyer_inquiry/<?php echo $product->id ?>" class="btn btn-warning btn-md dashboard-product-list-view-inquiries-btn">View Inquiries</a>
									<a href="<?php echo base_url(); ?>close_my_product/<?php echo $product->id ?>" class="btn btn-warning btn-md remove_btn dashboard-product-list-close-btn">Close Now</a>
									
								<?php }else if($product->status == 1 && $today > $due_date){ ?>
								
									<a href="<?php echo base_url(); ?>repost_product/<?php echo $product->id ?>" class="btn btn-success btn-md edit_btn dashboard-product-list-repost-btn">Repost</a>
									<a href="<?php echo base_url(); ?>edit_repost_product/<?php echo $product->id ?>" class="btn btn-success btn-md edit_btn dashboard-product-list-edit-btn edit-and-repost">Edit and Repost</a>
									<a href="<?php echo base_url(); ?>view_buyer_inquiry/<?php echo $product->id ?>" class="btn btn-warning btn-md dashboard-product-list-view-inquiries-btn">View Inquiries</a>
									<a href="<?php echo base_url(); ?>close_my_product/<?php echo $product->id ?>" class="btn btn-warning btn-md remove_btn dashboard-product-list-close-btn">Close Now</a>
									
								<?php }else{ ?>
									<a href="<?php echo base_url(); ?>view_buyer_inquiry/<?php echo $product->id ?>" class="btn btn-warning btn-md ">View Inquiries</a>
								<?php } ?>
			
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }else{ ?>
			<p>Sorry! There are no product.</p>
			<br /><br /><br />
		<?php } ?>
    </div>
		<!--<div class="text-center">-->
		<!--	<button id="nav-to-top" class="btn btn-lg btn-default" style="position: fixed;bottom:10px;right: 10px;z-index: 100;width:auto;">-->
		<!--		<span class="glyphicon glyphicon-chevron-up"></span> up-->
		<!--	</button>-->
		<!--</div>-->
	</div>
</div>

<?php $this->load->view('public/templates/footer') ?>