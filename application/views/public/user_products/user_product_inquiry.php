<?php $this->load->view('public/templates/header', array(
	'title' => 'Dashboard',
	'link' => 'account'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'items')) ?>

<div class="product-view">
	<div class="container">
	
<div class="product-list">
	<div class="container">
		
		<div class="lead page-header">
			<?php echo $product; ?>
		</div>
		<div style="margin: 0 0 10px; float: right">
			<a class="btn btn-info btn-lg" href="<?php echo base_url('user_dashboard/my-products'); ?>">Back to my products</a> 
		</div>
		
		<?php if(!empty($inquiries)){ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						Inquiry list for product - <?php echo $product ?>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="Periodic">
								<table class="table table-striped" style="margin:0;font-size:100%">
									<thead class="text-muted">
										<tr>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Name
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Email
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Phone
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($inquiries as $inquiry){ ?>
										<tr>
											<td class="text-center">
												<?php echo $inquiry->name ?>
											</td>
											<td class="text-center">
												<?php echo $inquiry->email ?>
											</td>
											<td class="text-center">
												<?php echo $inquiry->phone ?>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php }else{ ?>
			<p>Sorry! There are no inquiries received for this product.</p>
			<br /><br /><br />
		<?php } ?>
</div>
</div>
		<div class="text-center">
			<button id="nav-to-top" class="btn btn-lg btn-default" style="position: fixed;bottom:10px;right: 10px;z-index: 100;width:auto;">
				<span class="glyphicon glyphicon-chevron-up"></span> up
			</button>
		</div>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>