<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Add User Products',
	'link' => 'account','active1'=>'dashboard'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'items')) ?>

<div class="product-view">
	<div class="container">
	
<?php echo form_open_multipart('dashboard/add-new-product',array('id'=>'add-product','method'=>'post')) ?>
<div class="row">
	
	<div class="col-xs-12 col-sm-4 col-md-3">
        <div class="form-group <?= form_error('userfile') ? 'has-error' : '' ?>">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <label class="panel-title control-label" for="userfile">
                    	Product Image
                    </label>
                </div>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail text-warning">
                        <div style="margin:3rem 0">No image selected</div>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail">
                    </div>
                    <div class="btn-group btn-block">
                        <div class="btn btn-success btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" id="userfile" name="product_img">
                        </div>
                        <a href="#" class="btn btn-danger remove-image" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
            <p class="text-danger-image">Please upload a file less than 5mb.<br>Supported file format are jpeg, png</p>
            <div class="text-danger"><?=form_error('product_img') ? form_error('product_img') : '&nbsp' ?></div>
        </div>
    </div>
	   
    <div class="col-xs-12 col-sm-8 col-md-9 add-product-form">
    	<div class="col-md-12 form-group-name <?=form_error('name') ? 'has-error' : '' ?>" style="margin-bottom: 15px;">
    		<label for="usr">Item<span class="red">*</span></label>
    		<input type="text" name="name" id="product_name" class="form-control product_name" value="<?=set_value('name') ?>">
    		<div class="text-danger text-danger-name"><?=form_error('name') ? form_error('name') : '' ?></div>
    	</div>
    	<div class="col-md-4 form-group form-group-price <?=form_error('price') ? 'has-error' : '' ?>" style="margin-bottom: 15px;">
    		<label for="usr">Price:(Rs.)<span class="red">*</span></label>
    		<input type="text" name="price" id="product_price" class="form-control product_price" value="<?=set_value('price') ?>">
    		<div class="text-danger text-danger-price"><?=form_error('price') ? form_error('price') : '' ?></div>
    	</div>
    	<div class="col-md-4 form-group form-group-quantity <?=form_error('quantity') ? 'has-error' : '' ?>" style="margin-bottom: 15px;">
    		<label for="usr">Quantity Available<span class="red">*</span></label>
    		<input type="number" name="quantity" id="product_quantity" class="form-control product_quantity" min="1" value="<?=set_value('quantity') ?>">
    		<div class="text-danger text-danger-quantity"><?=form_error('quantity') ? form_error('quantity') : '' ?></div>
    	</div>
    	<div class="col-md-4 last-col form-group form-group-category <?=form_error('category') ? 'has-error' : '' ?>" style="margin-bottom: 15px;"> 
    		<label for="usr">Category<span class="red">*</span></label>
    		<select name="category" id="product_category" class="form-control">
    		<option value="0">Select</option>
    		<?php foreach ($categories as $key => $category){ ?>
    			<option value="<?php echo $category->id ?>" <?php echo ($category->id == set_value('category') ? 'selected' : '') ?>><?=$category->name ?></option>
    		<?php } ?>
    		</select>
    		<div class="text-danger text-danger-category"><?=form_error('category') ? form_error('category') : '' ?></div>
    	</div>
    	
    	<?php if(empty(strpos($user->email,'@'))): ?>
    	<div class="col-md-6 form-group-user-name <?=form_error('user_name') ? 'has-error' : '' ?>" style="margin-bottom: 15px;">
    		<label for="usr">Name<span class="red">*</span></label>
    		<input type="text" name="user_name" id="user_name" class="form-control user-name" value="<?=set_value('user_name') ?>">
    		<div class="text-danger text-danger-user-name"><?=form_error('user_name') ? form_error('user_name') : '' ?></div>
    	</div>
    	<div class="col-md-6 form-group-email <?=form_error('email') ? 'has-error' : '' ?>" style="margin-bottom: 15px;">
    		<label for="usr">Email<span class="red">*</span></label>
    		<input type="text" name="email" id="email" class="form-control product_name" value="<?=set_value('email') ?>">
    		<div class="text-danger text-danger-email"><?=form_error('email') ? form_error('email') : '' ?></div>
    	</div>
    	<?php endif?>
    	<?php if(empty($users_address)){ ?>
    	<div class="col-md-4" style="margin-bottom: 35px;"> 
    		<label for="usr">Country<span class="red">*</span></label>
    		<select name="country" id="country" class="form-control">
    		    <option value="0"> - Country - </option>
        		<?php if(sizeof($countries) == 1){ ?>
			    <option value="<?php echo $countries[0]['loc_name'];?>" data-val="<?php echo $countries[0]['loc_id'];?>" selected><?php echo $countries[0]['loc_name'];?></option>    
			    <?php }else{ foreach($countries as $country) { ?>
				<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>">
					<?php echo $country['loc_name'];?>
				</option>
			    <?php } }?>
    		</select>
    		<div class="text-danger text-danger-country"></div>
    	</div>
    	<div class="col-md-4 form-group-state" style="margin-bottom: 35px;"> 
    		<label for="usr">State<span class="red">*</span></label>
    		<select name="state" id="state" class="form-control">
				<option value="0" selected="selected"> - State - </option>
				<?php if(sizeof($states) == 1){ ?>
			    <option value="<?php echo $states[0]['loc_name'];?>" data-val="<?php echo $states[0]['loc_id'];?>" selected><?php echo $states[0]['loc_name'];?></option>
			    <?php }else{ foreach($states as $state) { ?>
				<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>">
					<?php echo $state['loc_name'];?>
				</option>
			    <?php } }?>
			</select>
			<div class="text-danger text-danger-state"></div>
    	</div>
    	<div class="col-md-4 form-group-city" style="margin-bottom: 35px;"> 
    		<label for="usr">City<span class="red">*</span></label>
    		<input list="city_list" name="city" id="city" class="form-control" autocomplete="off" value="<?php if($user_address->city) echo $user_address->city;elseif($user_address->country == '' && sizeof($cities) == 1)echo $cities[0]['loc_name'];?>" placeholder="Select or Enter your City">
    		<datalist id="city_list">
                <?php if(sizeof($cities) == 1){ ?>
                    <option value="<?php echo $cities[0]['loc_name'];?>" data-val="<?php echo $cities[0]['loc_id'];?>"    selected><?php echo $cities[0]['loc_name'];?></option>
                <?php }else{ foreach($cities as $city) { ?>
                <option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>">
                    <?php echo $city['loc_name'];?>
                </option>
                <?php } } ?>
            </datalist>
		    <div class="text-danger text-danger-city"></div>
    	</div>
    	<div class="col-md-4 form-group-locality" style="margin-bottom: 35px;"> 
    		<label for="usr">Locality<span class="red">*</span></label>
    		<input list="locality_list" name="locality" id="locality" class="form-control" autocomplete="off" value="<?php if($user_address->locality) echo $user_address->locality;elseif($user_address->country == '' && sizeof($localities) == 1)echo $localities[0]['loc_name'];?>" placeholder="Select or Enter your Locality">
    		<datalist id="locality_list">
                <?php if(sizeof($localities) == 1){ ?>
                       <option value="<?php echo $localities[0]['loc_name'];?>" data-val="<?php echo $localities[0]['loc_id'];?>" selected><?php echo $localities[0]['loc_name'];?>
                       </option>
                <?php }else{ foreach($localities as $locality) { ?>
                    <option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>">
                    <?php echo $locality['loc_name'];?>
                    </option>
                <?php } } ?>
            </datalist>
		    <div class="text-danger text-danger-locality"></div>
    	</div>
    	<div class="col-md-4 form-group-apartment" style="margin-bottom: 35px;">
    		<label for="usr">Apartment<span class="red">*</span></label>
    		<input list="apartment_list" name="apartment" id="apartment" class="form-control" autocomplete="off" value="<?php if($user_address->apartment_name) echo $user_address->apartment_name;elseif($user_address->country == '' && sizeof($apartments) == 1)echo $apartments[0]['loc_name'];?>" placeholder="Select or Enter your Apartment">
    		<datalist id="apartment_list"  data-dropup-auto="false">
                <?php if(sizeof($apartments) == 1){ ?>
                    <option value="<?php echo $apartments[0]['loc_name'];?>" data-val="<?php echo $apartments[0]['loc_id'];?>" selected><?php echo $apartments[0]['loc_name'];?></option>
                    <option value="Others" data-val="Others">
                    Others
                    </option>
                <?php }else{ foreach($apartments as $apartment) { ?>
                <option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>">
                    <?php echo $apartment['loc_name'];?>
                </option>
                <?php } } ?>    
            </datalist>
		    <div class="text-danger text-danger-apartment"></div>
    	</div>
    	<div class="col-md-4 form-group-flat_no" style="margin-bottom: 35px;">
    		<label for="flat_no">Flat No<span class="red">*</span></label>
    		<input type="text" name="flat_no" id="flat_no" class="form-control" value="<?=set_value('flat_no') ?>">
    		<div class="text-danger text-danger-flat_no"></div>
    	</div>
    	
    	<?php } ?>
    	
    	<div class="col-md-12" style="margin-bottom: 35px;"> 
    		<label for="usr">Description</label>
    		<textarea name="description" id="product_description" class="form-control product_description" rows="5" maxlength="100"><?=set_value('description') ?></textarea>
    	</div>
    	<div style="margin-bottom: 15px;">
    		<div class="col-xs-6 col-sm-6">
    			<input type="submit" name="add_btn" class="btn btn-success form-control add_btn" value="Add" id="add_btn">
    		</div>
    		<div class="col-xs-6 col-sm-6">
    			<input type="reset" name="remove_btn" class="btn btn-warning  form-control" value="Clear">
    		</div>
    	</div>
    </div>	
</div>
<?php echo form_close() ?>
<p>&nbsp;</p>
	</div>
<script>
    var input = document.getElementById('userfile');
    var img_size = '';
    jQuery(document).ready(function(){
        //for address part end
        var country_value = jQuery('#country option:selected', this).val();
        var state_value = jQuery('#state option:selected', this).val();
        var city_value = jQuery('#city').val();
        var locality_value = jQuery('#locality', this).val();
        if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
            jQuery('#state').attr("disabled", true);
            jQuery('#city').attr("disabled", true);
            jQuery('#locality').attr("disabled", true);
            jQuery('#apartment').attr("disabled", true);
        }
        else if(state_value=='' || state_value=='- State -' || state_value=='0'){
            jQuery('#city').attr("disabled", true);
            jQuery('#locality').attr("disabled", true);
            jQuery('#apartment').attr("disabled", true);
        }
        else if(city_value=='' || city_value=='- city -'){
            jQuery('#locality').attr("disabled", true);
            jQuery('#apartment').attr("disabled", true);
        }
        else if(locality_value=='' || locality_value=='- locality -'){
            jQuery('#apartment').attr("disabled", true);
        }
        //for address part end
        input.addEventListener("change", function() {
            var file  = this.files[0];
            var img = new Image();
            img.onload = function() {
                var sizes = {
                    width:this.width,
                    height: this.height
                };
                URL.revokeObjectURL(this.src);
            }
            var objectURL = URL.createObjectURL(file);
            //console.log('change: file', file.size);
            img.src = objectURL;
            img_size = file.size;
            if(file.size>5600000){
                img_size = '';
                tag = 'Error! Please upload a file less than 5mb. Supported file format are jpeg, png';
                jQuery('.text-danger-image').text(tag);
			    jQuery('.text-danger-image').css('color','#a94442');
			    jQuery(".remove-image").click()
            }
            else if(file.size<=1000000){
                jQuery('.text-danger-image').text('');
                jQuery('.text-danger-image').css('color','#333');
            }
            else{
                jQuery('.text-danger-image').text('Please upload a file less than 5mb.');
                jQuery('.text-danger-image').css('color','#333');
            }
        });
        jQuery("#country option:contains('India')").attr('selected', 'selected');
	    var countries = <?php echo json_encode($countries) ?>;
	    if(countries.length > 1){
	        setTimeout(function(){ jQuery("#country").trigger("change"); }, 3000);
	    }
	    jQuery('#country').on('change',function(){
            country_value = jQuery(this).val();
            var country_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {country : country_id, gre_tokan : token };
            jQuery('#state').find('option').not(':first').remove();
            jQuery('#city').val('');
            jQuery('#locality').val('');
            jQuery('#apartment').val('');
            jQuery('#city_list').html('');
            jQuery('#locality_list').html('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_state'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#state').attr("disabled", true);
                    jQuery('#city').attr("disabled", true);
                    jQuery('#locality').attr("disabled", true);
                    jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var state_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < state_list.length; i++) {
                        var obj = state_list[i];
                        jQuery('#state').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
                        jQuery('#state').attr("disabled", true);
                    }
                    else{
                        jQuery('#state').attr("disabled", false);
                    }
	            }
	        });
	    });
	    jQuery('#state').on('change',function(){
            state_value = jQuery(this).val();
            var state_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {state : state_id, gre_tokan : token };
            jQuery('#city').find('option').not(':first').remove();
            jQuery('#city').val('');
            jQuery('#locality').val('');
            jQuery('#apartment').val('');
            jQuery('#city_list').html('');
            jQuery('#locality_list').html('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_city'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#city').attr("disabled", true);
                    jQuery('#locality').attr("disabled", true);
                    jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var city_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < city_list.length; i++) {
                        var obj = city_list[i];
                        jQuery('#city_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(state_value=='' || state_value==' - State - ' || state_value=='0'){
                        jQuery('#city').attr("disabled", true);
                    }
                    else{
                        jQuery('#city').attr("disabled", false);
                    }
	            }
	        });
	    }); 
	    jQuery('#city').on('change',function(){
            var city_value = jQuery(this).val();
            var city_id = jQuery('#city_list').find("[value='" + city_value + "']").attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {city : city_id, gre_tokan : token };
            jQuery('#locality').find('option').not(':first').remove();
            jQuery('#locality').val('');
            jQuery('#apartment').val('');
            jQuery('#locality_list').html('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_locality'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#locality').attr("disabled", true);
                    jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var locality_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < locality_list.length; i++) {
                        var obj = locality_list[i];
                        jQuery('#locality_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(city_value==''){
                        jQuery('#locality').attr("disabled", true);
                    }
                    else{
                        jQuery('#locality').attr("disabled", false);
                    }
	            }
	        });
	    });
        jQuery('#locality').on('change',function(){
            var locality_value = jQuery(this).val();
            var locality_id = jQuery('#locality_list').find("[value='" + locality_value + "']").attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            var data = {locality : locality_id, gre_tokan : token };
            jQuery('#apartment').find('option').not(':first').remove();
            jQuery('#apartment').val('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_apartment'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var apartment_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < apartment_list.length; i++) {
                        var obj = apartment_list[i];
                        jQuery('#apartment_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    jQuery('#apartment').attr("disabled", false);
                    if(locality_value==''){
                        jQuery('#apartment').attr("disabled", true);
                    }
	            }
	        });
	    });
        jQuery("#product_category").find('option:contains(Others)').appendTo(jQuery("#product_category"));
        
        jQuery('.remove-image').click(function(){
            jQuery('.fileinput-new img').attr('src','');
        });
		jQuery(".add_btn").click(function(event){
            var product_name = jQuery("#product_name").val();
		    var product_price  = jQuery('#product_price').val();
		    var product_quantity = jQuery('#product_quantity').val();
		    var product_category = jQuery('#product_category').val();
		    var address_check = '<?php echo $user->email ;?>';
		    var country   = jQuery('#country').val();
			var state     = jQuery('#state').val();
			var city      = jQuery('#city').val();
			var locality  = jQuery('#locality').val();user_name
			var apartment = jQuery('#apartment').val();
			var flat_no   = jQuery('#flat_no').val();
			var user_name = jQuery('#user_name').val();
			var email     = jQuery('#email').val();
			jQuery('.text-danger').text('');
			var error = false;
	        if (product_name == "") {
            	jQuery('.text-danger-name').text('Please Enter Product Name');
            	jQuery('.form-group-name').addClass('error');
	        	error = true;
	        }
	        if (product_price == "") {
            	jQuery('.text-danger-price').text('Please Enter Product Price');
            	jQuery('.form-group-price').addClass('error');
	        	error = true;
	        }
	        if (product_quantity == "") {
            	jQuery('.text-danger-quantity').text('Please Enter Product Quantity');
            	jQuery('.form-group-quantity').addClass('error');
	        	error = true;
	        }
	        if (product_category == "0") {
            	jQuery('.text-danger-category').text('Please Select Product Category');
            	jQuery('.form-group-category').addClass('error');
	        	error = true;
	        }
	        if(user_name == ''){
			    jQuery('.text-danger-user-name').text('Please enter your name');
			    jQuery('.form-group-user-name').addClass('error');
			    error = true;
			}
			if(address_check == ''){
			    if(email == ''){
    			    jQuery('.text-danger-email').text('Please enter your email');
    			    jQuery('.form-group-email').addClass('error');
    			    error = true;
    			}
    			
    			if(country == '0' || country == '' || country == undefined){
    			    jQuery('.text-danger-country').text('Please Select Country');
    			    jQuery('.form-group-country').addClass('error');
    			    error = true;
    			}
    			if(state == '0' || state == '' || state == undefined){
    			    jQuery('.text-danger-state').text('Please Select State');
    			    jQuery('.form-group-state').addClass('error');
    			    error = true;
    			}
    			if(city == '0' || city == '' || city == undefined){
    			    jQuery('.text-danger-city').text('Please Select City');
    			    jQuery('.form-group-city').addClass('error');
    			    error = true;
    			}
    			if(locality == '0' || locality == '' || locality == undefined){
    			    jQuery('.text-danger-locality').text('Please Select Locality');
    			    jQuery('.form-group-locality').addClass('error');
    			    error = true;
    			}
    			if(apartment == '0' || apartment == '' || apartment == undefined){
    			    jQuery('.text-danger-apartment').text('Please Select Apartment');
    			    jQuery('.form-group-apartment').addClass('error');
    				error = true;
    			}
    			if(flat_no == '' || flat_no == undefined){
    			    jQuery('.text-danger-flat_no').text('Please Enter Flat Number');
    			    jQuery('.form-group-flat_no').addClass('error');
    				error = true;
    			}   
			}
			if(img_size == '' || img_size == undefined){
			    jQuery('.text-danger-image').text('Please upload a file .');
			    jQuery('.text-danger-image').css('color','#a94442');
				error = true;
			}
			if(error){
			    return false;
			}else{
			    return true;
			}
		});
		jQuery('.add-product-form #product_name').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
        jQuery('.add-product-form #product_price').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
        jQuery('.add-product-form #product_quantity').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
        jQuery('.add-product-form #user_name').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
        jQuery('.add-product-form #email').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
        jQuery(".add-product-form #country").focus(function(){
            var txt = '<p></p>';
            jQuery('.text-danger-country').html(txt);
        });
        jQuery(".add-product-form #state").focus(function(){
            var txt = '<p></p>';
            jQuery('.text-danger-state').html(txt);
            jQuery('.text-danger-city p').css('color','#333');
        });
        jQuery(".add-product-form #city").focus(function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery('.text-danger-city').html(txt);
            jQuery('.text-danger-city p').css('color','#333');
        });
        jQuery(".add-product-form #locality").focus(function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery('.text-danger-locality').html(txt);
            jQuery('.text-danger-locality p').css('color','#333');
        });
        jQuery(".add-product-form #apartment").focus(function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery('.text-danger-apartment').html(txt);
            jQuery('.text-danger-apartment p ').css('color','#333');
        });
        jQuery('.add-product-form #flat_no').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
		jQuery(".add-product-form select").on('change', function(){
		    var value =  jQuery(this).val();
		    if(value !== '0'){
		        jQuery(this).parent().removeClass('error');
		        jQuery(this).next().text('');
		    }else{
		        jQuery(this).parent().addClass('error');
		    }
		});
    });
</script>
<?php $this->load->view('public/templates/footer') ?>