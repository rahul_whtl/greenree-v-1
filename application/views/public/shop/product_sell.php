<?php $this->load->view('public/templates/header', array(
	'title' => 'Best Upcycled and eco-friendly items - GreenREE',
	'title_description' => 'Rewinding back to traditional products is the key to stay green.',
	'menu_active' =>'shop'
)) ?>
<!--<div class="banner-section">
    <div class="container">
    	<p class="banner-caption">Shop</p>
	</div>
</div>-->
<div class="products shop">
    <p>&nbsp;</p>
	<div class="container">
<div class="row">
    <div class="col-md-2 col-sm-3 col-xs-12">
        
        <?php if(sizeof($categories)){ ?>
        <div class="hidden-xs">
		<div class="lead">Shop by category</div>
        <ul class="list">
        <?php foreach ($categories as $key => $category){
                if(!empty($category->products)){ ?>
            <li><?php echo anchor('category/'.$category->slug.'/'.url_title($category->name), $category->name) ?></li>
        <?php } }?>
        </ul>
		</div>
		<?php } ?>
		
		<?php if(sizeof($categories)){ ?>
		<div class="dashboard-header hidden-sm hidden-md hidden-lg mobile-cat">
        	<div class="container">
        	    <a href="#" class="mobile-menu-link"><span>Shop by category</span><span class="caret"></span></a>
        		<ul class="nav nav-tabs responsive-tab" role="tablist">
        		    
                    <?php foreach ($categories as $key => $category){
                            if(!empty($category->products)){
                        ?>
                        <li class="nav-bar-btn"><?php echo anchor('category/'.$category->slug.'/'.url_title($category->name), $category->name) ?></li>
                    <?php } } ?>
        		</ul>
        	</div>
        </div>
        <?php } ?>
        
        <script type="text/javascript">
        		jQuery(document).ready(function(){
        			jQuery(".mobile-menu-link").click(function(event){
        				event.preventDefault();
        				jQuery(".mobile-cat .responsive-tab").slideToggle();
        				jQuery(".mobile-cat .caret").toggleClass('active');
        			});	
        		});
        </script>
		
	</div>
	
		<div class="col-md-10 col-sm-9 col-xs-12 item-wrap shop">
		<?php $this->load->view('public/products/products_tiles_view', array(
			'type' => 'tiles',
			'cols' => 'col-xs-6 col-sm-4 col-md-3 col-lg-3',
			'products' => $products
		)) ?>
		</div>
</div>

		<!-- <div class="shop-section row" id="shop-section">
			<h2>Shop</h2>
			<p><b>How we re-use your scrap, few items we sell back to you with added value:</b></p>
			<div class="col-md-3">
				<img src="<?php //echo base_url('assets/images/icons/shop_section.jpg'); ?>">
			</div>
			<div class="col-md-9">
				<p>Until we really show how we re-use your scrap, you should not believe us.</p>
				<ul>
					<li>Good quality Carton boxes picked away from your doors as scrap are available for sale to retailers or anyone at very cheap prices.
					</li>
					<li>We donate some percentage of collected newspapers to institutes for physically challenged people where they can make wonderful decorative and useful items and sell back to you through our platform.
					</li>
					<li>If you wished to donate your scrap instead of selling it, then don’t regret, you are giving not only money but the job and confidence to needy ones.
					</li>
					<li>So what are you waiting for, Just go to Shop page and buy useful items made out of  your own scrap. 
					</li>
				</ul>
				<p><b>GreenRee note:</b></p>
				<ul>
					<li>Possibilities of re-using the scrap are uncountable, what we have listed here are only few. </li>
					<li>Use our GreenREE App in all forms to make us successful in each such possibility and make this world Clean and Green.</li>
				</ul>
			</div>
		</div> -->
	</div>
</div>


<?php $this->load->view('public/templates/footer') ?>