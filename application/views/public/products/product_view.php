<?php

$breadcrumbs = array();
// Create page breadcrumbs from category tiers
foreach ($category->pagination as $category)
{
	// Add formatted names and links to the breadcrumbs
	array_push($breadcrumbs, array(
		'name' => $category->name,
		'link' => 'category/'.$category->slug.'/'.url_title($category->name)
	));
}
// Add the product to the breadcrumbs
array_push($breadcrumbs, array(
	'name' => $product->name,
	'link' => FALSE // No need for linking this very page
));

?>

<?php $this->load->view('public/templates/header', array(
	'title' => $product->name.' - GreenREE',
	'breadcrumbs' => $breadcrumbs,
    'menu_active' =>'shop'
)); ?>
<div class="product-view shop-view greenree-shop-view">
	<div class="container">

<div class="row">

<div class="product-slider col-xs-12 col-sm-6 col-md-5 col-lg-5">
  <div id="carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner upper-image">
      	<?php $first = true;
      	      foreach ($product->images as $key => $img):
      	?>
      <div class="item <?php echo $first ? 'active' : '' ?>"> 
      <?php $first= false; ?>	
			<div class="text-center inner-item <?= ($key == 0) ? 'active' : '' ?>">
				<img src="<?php echo $img->url ?>" class="img-responsive">
			</div>
      </div>
	<?php endforeach ?>
	  <div class="item <?php echo $first ? 'active' : '' ?>"> 
	  	<div class="text-center inner-item">
      	<img src="<?php echo $product->thumb ?>" class="img-responsive"> 
      </div>
      </div>
    </div>
  </div>
  <div class="clearfix">
    <div id="thumbcarousel" class="carousel slide" data-interval="false">
      <div class="carousel-inner">
        <div class="item active">
        <?php $count = 0;foreach ($product->images as $key => $img): ?>		
          <div data-target="#carousel" data-slide-to="<?php echo $count;?>" class="thumb">
          	<img src="<?php echo $img->url ?>" alt="" style="height:50px" class="img-thumbnail hoverable">
          </div>
         <?php $count++; ?> 
        <?php endforeach ?>  
          <div data-target="#carousel" data-slide-to="<?php echo $count;?>" class="thumb">
          	<img src="<?php echo $product->thumb ?>" alt="" style="height:50px" class="img-thumbnail hoverable">
          </div>
        </div>
      </div>
      <!-- /carousel-inner --> 
  	</div>
    <!-- /thumbcarousel --> 
  </div>
</div>


	<!-- carousel -->
	<!--<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">-->
	<!--	<div class="thumbnail main-img text-center">-->
	<!--		<?php //if ($variant AND !empty($variant->images) !== ''): ?>-->
	<!--			<?php// echo 'Variant Image takes priority over product Image(s)'; ?>-->
				<?php //if (count($variant->images) == 1): // Only one image, no need for a carousel ?>
	<!--				<?php// foreach ($variant->images as $key => $img): ?>-->
	<!--					<img src="<?php echo $img->url ?>" class="img-responsive" style="max-height:350px;display:initial">-->
	<!--				<?php //endforeach ?>-->
				<?php //else: // Multiple images, use bootstrap carousel ?>
	<!--				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">-->
	<!--					<ol class="carousel-indicators">-->
	<!--						<?php// foreach ($variant->images as $key => $img): ?>-->
	<!--							<li data-target="#carousel-example-generic" data-slide-to="<?php //echo $key ?>" class="<?php //echo ($key === 0) ? 'active' : '' ?>"></li>-->
	<!--						<?php// endforeach ?>-->
	<!--					</ol>-->

	<!--					<div class="carousel-inner" role="listbox">-->
	<!--						<?php //foreach ($variant->images as $key => $img): ?>-->
	<!--							<div class="text-center item <?//= ($key == 0) ? 'active' : '' ?>">-->
	<!--								<img src="<?php echo $img->url ?>" class="img-responsive" style="max-height:350px;display:initial">-->
	<!--							</div>-->
	<!--						<?php //endforeach ?>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			<?php //endif ?>-->
	<!--		<?php //else: ?>-->
	<!--		    <?php //echo "sdfsdsfdF";?>-->
				<?php //if ($product->images): // There are product images ?>
					<?php //if (count($product->images) == 1): // Only one image, no need for a carousel ?>
	<!--					<?php// foreach ($product->images as $key => $img): ?>-->
	<!--						<img src="<?php echo $img->url ?>" class="img-responsive" style="max-height:350px;display:initial">-->
	<!--					<?php// endforeach ?>-->
					<?php// else: // Multiple images, use bootstrap carousel ?>
	<!--					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">-->
	<!--						<ol class="carousel-indicators">-->
	<!--							<?php //foreach ($product->images as $key => $img): ?>-->
	<!--								<li data-target="#carousel-example-generic" data-slide-to="<?php //echo $key ?>" class="<?php// echo ($key === 0) ? 'active' : '' ?>"></li>-->
	<!--							<?php //endforeach ?>-->
	<!--						</ol>-->

	<!--						<div class="carousel-inner" role="listbox">-->
	<!--							<?php// foreach ($product->images as $key => $img): ?>-->
	<!--								<div class="text-center item <?//= ($key == 0) ? 'active' : '' ?>">-->
	<!--									<img src="<?php// echo $img->url ?>" class="img-responsive" style="max-height:350px;display:initial">-->
	<!--								</div>-->
	<!--							<?php //endforeach ?>-->
	<!--						</div>-->
	<!--					</div>-->
	<!--				<?php// endif ?>-->
				<?php// else: // No product images, use thumbnail ?>
	<!--				<img src="<?php //echo $product->thumb ?>" class="img-responsive" style="display:initial; width:100%">-->
	<!--			<?php// endif ?>-->
	<!--		<?php //endif ?>-->

	<!--	</div>-->

	<!--	<a href="<?php// echo current_url() ?>">-->
	<!--		<img src="<?php// echo $product->thumb ?>" alt="" style="height:50px" class="img-thumbnail hoverable">-->
	<!--	</a>-->
	<!--	<?php //foreach ($variant_thumbs as $row): ?>-->
	<!--		<a href="<?php //echo $row['link'] ?>">-->
	<!--			<img src="<?php// echo $row['image'] ?>" alt="" style="height:50px" class="img-thumbnail hoverable">-->
	<!--		</a>-->
	<!--	<?php //endforeach ?>-->
	<!--</div>-->
    <!-- end of carousel -->

	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

		<div class="panel panel-default">
			<?php echo form_open(current_url(), 'class="panel-body"') ?>
				<?php echo form_hidden('id', $product->id) ?>
				<?php echo form_hidden('name', $product->name) ?>
				<?php echo form_hidden('name', $product->name) ?>

				<?php if ($variant AND !empty($variant->images) !== ''): ?>
					<?php // Variant Image takes priority over product Image(s) ?>
					<?php echo form_hidden('thumb', $variant->images[0]->url) ?>
				<?php else: ?>
					<?php echo form_hidden('thumb', $product->thumb) ?>
				<?php endif ?>

				<?php if ($variant AND $variant->price > 0): ?>
					<?php echo form_hidden('price', $variant->price) ?>
				<?php else: ?>
					<?php echo form_hidden('price', $product->price) ?>
				<?php endif ?>

				<?php if ($variant AND $variant->weight > 0): ?>
					<?php echo form_hidden('weight', $variant->weight) ?>
				<?php else: ?>
					<?php echo form_hidden('weight', $product->weight) ?>
				<?php endif ?>
                
                <h1 class="page-header product-name">
                	<?php echo $product->name ?>
                </h1>
                
                <div class="description form-group">
            		<?php echo $product->description ?>
            	</div>
            	
                <div class="lead price form-group text-left">
        			<label>
        				<?php if ($variant AND $variant->price > 0): ?>
        					<s class="text-danger"><?php echo $this->flexi_cart->get_currency_value($product->price) ?></s>
        					<span class="text-success"><?php echo $this->flexi_cart->get_currency_value($variant->price) ?></span>
        				<?php else: ?>
        					<span class="text-success">Price: <?php echo $this->flexi_cart->get_currency_value($product->price) ?></span>
        				<?php endif ?>
        			</label>
        		</div>
		        
				<div class="form-group quantity-wrap">
					<label class="control-label">Quantity</label>
					<input type="number" name="quantity" class="input-lg" id="quantity" value="1">
					<input type="submit" name="add_to_cart" id="add_to_cart" value="Add to Cart" class="btn btn-md btn-block btn-success">
				</div>

				<div class="row">
					<?php foreach ($product_options as $option): ?>
						<!-- <div class="col-xs-6">
							<div class="list-group">
								<a class="list-group-item disabled">
									<strong><?php echo $option['name'] ?></strong>
								</a>
								<?php foreach ($option['values'] as $value): ?>
									<a href="<?php echo $value['link'] ?>" class="list-group-item <?php echo (in_array($value['id'], $variant_selected) ? 'active' : '') ?>">
										<?php echo $value['name'] ?>
									</a>
								<?php endforeach ?>
							</div>
						</div> -->
					<?php endforeach ?>


					<?php foreach ($product_options as $option): ?>
						<div class="col-xs-6">
							<div class="form-group <?php echo form_error('options[]') ? 'has-error' : '' ?>">
								<label for="" class="control-label"><?php echo $option['name'] ?></label>
								<select class="form-control" name="options[<?php echo $option['name'] ?>]" id="<?php echo $option['name'] ?>">
									<option value="Any <?php echo $option['name'] ?>">Any <?php echo $option['name'] ?></option>
									<?php foreach ($option['values'] as $value): ?>
										<option value="<?php echo $value['name'] ?>" <?php echo (in_array($value['id'], $variant_selected)) ? 'selected="selected"' : '' ?>>
											<?php echo $value['name'] ?>
										</option>
									<?php endforeach ?>
								</select>
							</div>
						</div>
					<?php endforeach ?>
				</div>
			<?php echo form_close() ?>
		</div>
	</div>

</div>

<?php if ($related_products): ?>
	<div class="breadcrumb" style="margin-top:5rem;margin-bottom:4rem">Related Products</div>
	<?php $this->load->view('public/products/products_tiles_view', array(
		'type' => 'tiles',
		'cols' => 'col-md-3 col-lg-2-5',
		'products' => $related_products
	)) ?>
<?php endif ?>
<!-- <hr>

<?php //if ($random_products): ?>
	<div class="breadcrumb" style="margin-top:5rem">You may also Like</div>
	<?php// $this->load->view('public/products/products_tiles_view', array(
		//'type' => 'tiles',
		//'cols' => 'col-md-3 col-lg-2-5',
		//'products' => $random_products
	//)) ?>
<?php //endif ?> -->


<!-- <div class="text-center">
	<button id="nav-to-top" class="btn btn-lg btn-default" style="position: fixed;bottom:10px;right: 10px;z-index: 100;width:auto;">
		<span class="glyphicon glyphicon-chevron-up"></span> up
	</button>
</div> -->
</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
	/*	jQuery("#add_to_cart").click(function(event){
	   	    //event.preventDefault();
	   	    var quantity     = jQuery("#quantity").val();
	   	    var color        = jQuery("#color").val();
	   	    var size         = jQuery("#size").val();
	   	    var product_name = jQuery(".product-name").text();
	   	    if(color=='Any color'){
                alert('Please Select Color');
                return;
            }
            if (size == "Any size") {
            	alert('Please Select Size');
            	return;
            }
	   }); */
	    jQuery(".view_user_cart").click(function(event){
	   	    window.location.href = "<?php echo base_url('cart'); ?>";
	    });
	});	
</script>
<?php $this->load->view('public/templates/footer') ?>