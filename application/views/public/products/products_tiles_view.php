<?php $this->load->helper('text') ?>

	<?php foreach ($products as $key => $product): ?>
		<div class="item <?php echo $cols ?>">
			<div class="thumbnail list text-center">
			    <a href="<?php echo base_url('product/'.$product->slug) ?>">
				<?php if ($product->thumb): ?>
					<img src="<?php echo base_url($product->thumb) ?>" class="group list-group-image" style="margin-bottom:2px">
				<?php else: ?>
					<img src="<?php echo base_url() ?>assets/system/no_image.jpg" class="group list-group-image" style="width:100%">
				<?php endif ?>
				</a>
				<div class="caption" style="padding:0">
					<div class="form-group" title="<?php $str = strstr($product->description, '</p>', true); echo substr($str, 3);?>">
						<?php echo anchor('product/'.$product->slug, character_limiter($product->name, 20)) ?>
					</div>
					<strong><?php echo $this->flexi_cart->get_currency_value($product->price) ?></strong>
				</div>
			</div>
		</div>
	<?php endforeach ?>
<?php if (isset($this->pagination)): ?>
	<!-- Pagination Link -->
	<?php echo $this->pagination->create_links() ?>
<?php endif ?>
