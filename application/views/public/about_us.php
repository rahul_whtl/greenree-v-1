<?php $this->load->view('public/templates/header', array(
	'title' => 'Best donation, make each one self earning through GreenREE - GreenREE',
	'title_description' => 'If you wished to donate your scrap instead of selling it, then don’t regret, through GreenREE you are giving job and confidence to needy ones - a real donation.')) ?>
<div class="about-us-page">
	<div class="container">
		<div class="sell-scrap row" id="sell-scrap">
			<div class="inner-section row">
			    <h1>How it works</h1>
				<h3>Sell Scrap</h3>
				<p class="sell-scrap-subheading"><b>You can sell your scrap in any of two ways:</b></p>
				<div class="col-md-3 col-sm-3 scrap-image">
					<div class="col-md-12 col-sm-12">
						<img src="<?php echo base_url('assets/images/icons/news_papper.png'); ?>">
						<div>At market rate </div>
					</div>
					<div class="col-md-12 col-sm-12">
						<img src="<?php echo base_url('assets/images/icons/carton-box-1.jpg'); ?>">
						<div>Market rate is &#8377; 5/Kg. We buy per box as we reuse not recycle so you get much better value than market rate </div>
					</div>
				</div>
				<div class="col-md-9">
					<p class="periodic-heading"><b>1. Periodic Sell</b></p>
					<ul class="periodic-list">
						<li> We prefer this option as it is convenient for you and efficient for us. So use this option to make us successful.
						</li>
						<li>Upon setting sell request of this type, you will be given schedule of your scrap pick-up on any days of your choice along with a pack of rubber bands.
						</li>
						<li>You will be reminded again a day before for your scheduled pick-up.						
						</li>
						<li>Just keep your scrap outside your doorstep on the pick-up eve just as     you keep milk coupons. Our goodies (rubber bands) will make it easy for you. 
						</li>
						<li>Our wizards will carry away your scrap early morning without disturbing your sleep.
						</li>
						<li>By the time you wake up, money will be loaded in your cash wallet.
						</li>
					</ul></span>
					<span>
						<p class="non-periodic-heading"><b>2. One Time Sell</b></p>
						<ul class="non-periodic">
							<li>In case you don’t anticipate that you will have scrap periodically.</li>
						</ul>
					</span>
					<div class="info col-sm-9">
					  <p><strong>Just a note on trust: Our system is cheat proof at all levels</strong></p>
					</div>
				</div>
			</div>
		</div>
		<div class="preowned row" id="preowned">
			<div class="inner-section row">
				<h3>Sell / Buy Pre-Owned Items</h3>
				<p class="preowned-heading"><b>Sell not only paper scrap:</b></p>
				<div class="col-md-3 col-sm-3 align-right">
					<img src="<?php echo base_url('assets/images/Pre-owned-items.jpg'); ?>">
				</div>
				<div class="col-md-9 col-sm-9">
					<ul class="preowned-list">
						<li>We pledged to use everything lying waste in your homes. We acted on the saying “One person's Scrap is another person's Gold”. So we have brought up a platform where  you can Sell / Buy pre-owned items within your vicinity.
						</li>
						<li>It can be a Car, Home appliance, books OR anything that you can think of.
						</li>
						<li>If you wish to sell, post ad with your contact details, buyers will contact you directly.
						</li>
						<li>If you wish to buy pre-owned items on sale, Just contact the buyer directly on the contact details given against the item.    
						</li>
					</ul>
					<div class="info">
					  <p><strong><span class="green-text">Green</span><span class="orange-text">REE</span> note: As our mission is to clean up home and planet by encouraging re-use, we don’t charge anything from buyer OR seller for  this service.</strong></p>
					</div>
				</div>
			</div>
		</div>
		<div class="shop-section row" id="shop-section">
			<div class="inner-section row">
				<h3>Shop</h3>
				<p class="shop-section-heading"><b>How we re-use your scrap, few items we sell back to you with added value:</b></p>
				<div class="col-md-3 col-sm-3">
					<img src="<?php echo base_url('assets/images/shop.jpg'); ?>">
				</div>
				<div class="col-md-9 col-sm-9">
					<ul class='shop-section-list'>
						<li>Good quality Carton boxes picked away from your doors as scrap are available for sale to retailers or anyone at very cheap prices.
						</li>
						<li>We donate some percentage of collected newspapers to institutes for physically challenged people where they can make wonderful decorative and useful items and sell back to you through our platform.
						</li>
						<li>If you wished to donate your scrap instead of selling it, then don’t regret, you are giving not only money but the job and confidence to needy ones.
						</li>
						<li>So what are you waiting for, Just go to Shop page and buy useful items made out of  your own scrap. 
						</li>
					</ul>
					<div class="info">
					    <p><strong><span class="green-text">Green</span><span class="orange-text">REE</span> note: Possibilities of re-using the scrap are uncountable, what we have listed here are only few.</strong></p>
					</div>
				</div>
			</div>
		</div>
		<div class="wishlist row" id="wishlist">
			<div class="inner-section row">
				<h3>Wishlist</h3>
				<p class="wishlist-subheading"><b>What is this tab for?</b></p>
				<div class="col-md-3 col-sm-3 align-right">
					<img src="<?php echo base_url('assets/images/Wishlist.jpg'); ?>">
				</div>
				<div class="col-md-9 col-sm-9">
					<ul class="wishlist-list">
						<!--<li>In case you are looking for some pre-owned item and don’t find that on items for sale, Just  Add to Wishlist and sellers will contact you.-->
						<!--</li>-->
						<li>In case you want to sell long lying unused item in your home to someone who is really needing it, Check the wishlist - you may find the buyer there.</li>
						<li>Wow !! That has never been there. 
						</li>
						<li>We added this feature as there may be passive sellers around you who can get active once they see there are potential buyers there.
						</li>
						<li>Once any of our registered users see the wishlist, they will contact you directly on the contact no. you provided against wish listed item.    
						</li>
					</ul>
					<div class="info">
					  <p><strong><span class="green-text">Green</span><span class="orange-text">REE</span> note: As our mission is to encourage re-use, we don’t charge anything from buyer OR seller for  this service.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

    jQuery(document).ready(function(){
        var url = 'http://clients.whtl.co.in/greenree/about_us#shop-section';
        var id = url.substring(url.lastIndexOf('#') + 1);
        if(window.location.hash){
            if(jQuery(window).width() > 768){
    		    jQuery('html,body').animate({scrollTop: jQuery("#"+id).offset().top-220},2000);	
            }else{
                jQuery('html,body').animate({scrollTop: jQuery("#"+id).offset().top},'slow');	
            }
    	}
    });
</script>
<?php $this->load->view('public/templates/footer') ?>