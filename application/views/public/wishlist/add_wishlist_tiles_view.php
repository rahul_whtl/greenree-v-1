<?php
    $this->load->helper('text');
    foreach ($products as $key => $product):
        ?>
		<div class="item <?php echo $cols ?>">
			<div class="thumbnail wishlist-tiles">
				<div class="caption">
					<div class="form-group" title="<?php echo character_limiter($product->description, 100) ?>">
						<span><strong><?php echo character_limiter($product->item_name, 20) ?></strong></span>
					</div>
					<div class="form-group" title="<?php echo $product->item_name ?>">
						<span><strong>Description : </strong><?php echo character_limiter($product->description, 100) ?></span>
					</div>
					<div class="form-group" title="<?php echo $product->item_name ?>">
						<span><strong>Quantity Needed : </strong><?php echo character_limiter($product->quantity, 20) ?></span>
					</div>
				</div>
			</div>
		</div>
<?php endforeach ?>