<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Edit My Wishlist',
	'link' => 'account','active1' => 'dashboard'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'wishlist')) ?>

<?php echo form_open_multipart(current_url().'#form',array('id'=>'edit-product','method'=>'post')) ?>
<div class="wishlist-edit-view">
	<div class="container">
    
	<?php if(!empty($this->session->flashdata('message'))){ ?>
		<div class="alert alert-success">
			 <?php echo $this->session->flashdata('message');  ?>			 
		</div>
	<?php }else if(!empty($this->session->flashdata('error'))){ ?> 
		<div class="alert alert-danger">
			 <?php echo $this->session->flashdata('error');  ?>
		</div>
	<?php } ?>
	
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 update-wishlist-form wishlist-form row">
    	<h2 class="text-center">Edit Your Wishlist Details</h2>
    	<div class="product_div product_name_div col-md-4 <?=form_error('name') ? 'has-error' : '' ?>">
    		<label for="usr">Item:<span class="red">*</span></label>
    		<input type="text" name="name" id="product_name" class="form-control product_name" value="<?=$product->item_name ?>">
    		<div class="text-danger"><?=form_error('name') ? form_error('name') : '&nbsp' ?></div>
    	</div>
    	<div class="product_div product_quantity_div col-md-4">
    		<label for="usr">Quantity Needed:</label>
    		<input type="text" name="quantity" id="product_quantity" class="form-control product_quantity" value="<?=$product->quantity ?>">
    	</div>
    	<div class="product_div product_category_div col-md-4 <?=form_error('category') ? 'has-error' : '' ?>"> 
    		<label for="usr">Category<span class="red">*</span></label>
    		<select name="category" id="wishlist_category">
    		<option value="">Select</option>
    		<?php foreach ($categories as $key => $category){ ?>
    			<option value="<?php echo $category->id ?>" <?=($category->id == $product_cat->category_id ? "selected" : "" ); ?>><?=$category->name ?></option>
    		<?php } ?>
    		</select>
    		<div class="text-danger"><?=form_error('category') ? form_error('category') : '&nbsp' ?></div>
    	</div>
    	<div class="product_div product_description_div col-md-12 <?=form_error('description') ? 'has-error' : '' ?>"> 
            <label for="usr">Description<span class="red">*</span></label>
            <textarea name="description" id="product_description" class="form-control product_description" rows="5" maxlength="100"><?=$product->description ?></textarea>
            <div class="text-danger"><?=form_error('description') ? form_error('description') : '&nbsp' ?>
        </div>
    	</div>	
    	<input type="hidden" name="id" value="<?=$product->id ?>" />
    	<div class="product_div product_update_btn_div">
    		<div class="col-sm-6">
    			<input type="submit" name="update_btn" class="btn btn-success form-control wishlist_update_btn" value="Update" id="update_btn">
    		</div>
    	</div>
    </div>
<?php echo form_close() ?>
	</div>
	<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
	    
	    jQuery("#wishlist_category").find('option:contains(Others)').appendTo(jQuery("#wishlist_category"));
        
		jQuery(".wishlist_update_btn").click(function(event){
		    var name        = jQuery('.update-wishlist-form #product_name').val();
		    var category    = jQuery('.update-wishlist-form #wishlist_category').val();
		    var description = jQuery('.update-wishlist-form #product_description').val();
		    
		    if (name=="" || name== undefined) {
            	var txt = '<p>Please Enter Your Wishlist Name.</p>';
            	jQuery('.update-wishlist-form .product_name_div .text-danger').html(txt);
        	    jQuery('.update-wishlist-form .product_name_div').css('color','#a94442');
            	if (category == ""   || category == undefined || category == 'select') {
            	    var txt = '<p>Please Select Wishlist Category.</p>';
	            	jQuery('.update-wishlist-formm .product_category_div .text-danger').html(txt);
            	    jQuery('.update-wishlist-form .product_category_div').css('color','#a94442');
		        	if (description == ""   || description == undefined) {
            	        var txt = '<p>Please Enter Wishlist Description.</p>';
    		        	jQuery('.update-wishlist-form .product_description_div .text-danger').html(txt);
            	        jQuery('.update-wishlist-form .product_description_div').css('color','#a94442');
    		        	return false;
    		        }
		        	return false;
		        }
				return false;
            }
            else if (category == ""   || category == undefined) {
        	    var txt = '<p>Please Select Wishlist Category.</p>';
            	jQuery('.update-wishlist-form .product_category_div .text-danger').html(txt);
        	    jQuery('.update-wishlist-form .product_category_div').css('color','#a94442');
            	if (description == ""   || description == undefined) {
        	        var txt = '<p>Please Enter Wishlist Description.</p>';
    	        	jQuery('.update-wishlist-form .product_description_div .text-danger').html(txt);
        	        jQuery('.update-wishlist-form .product_description_div').css('color','#a94442');
    	        	return false;
    	        }
            	return false;
            }
            else if (description == ""   || description == undefined) {
    	        var txt = '<p>Please Enter Wishlist Description.</p>';
            	jQuery('.update-wishlist-form .product_description_div .text-danger').html(txt);
    	        jQuery('.update-wishlist-form .product_description_div').css('color','#a94442');
            	return false;
            }
	        else
	            return true;
		});
		jQuery(".update-wishlist-form .product_name_div input").focus(function(){
		    var txt = '';
        	jQuery('.update-wishlist-form .product_name_div .text-danger').html(txt);
        	jQuery('.update-wishlist-form .product_name_div').css('color','#333');
		});
		jQuery(".update-wishlist-form .product_description_div textarea").focus(function(){
		    var txt = '';
        	jQuery('.update-wishlist-form .product_description_div .text-danger').html(txt);
        	jQuery('.update-wishlist-form .product_description_div').css('color','#333');
		});
		jQuery(".update-wishlist-form .product_category_div #wishlist_category").focus(function(){
		    var txt = '';
        	jQuery('.update-wishlist-form .product_category_div .text-danger').html(txt);
        	jQuery('.update-wishlist-form .product_category_div').css('color','#333');
		});
    });
</script>
<?php $this->load->view('public/templates/footer') ?>