<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Add My Wishlist',
	'link' => 'account'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'items')) ?>

<div class="product-view">
	<div class="container">
    
	<?php if(!empty($this->session->flashdata('alert'))){ ?>
	<?php $alert = $this->session->flashdata('alert'); ?>
		<div class="alert alert-<?php echo $alert['type']?>">
			 <?php echo $alert['message']; ?>			 
		</div>
	<?php } ?>
	
<?php echo form_open_multipart('add_new_whishlist',array('id'=>'add-wish','method'=>'post')) ?>
<div class="row">
	
	<div class="col-xs-12 col-sm-4 col-md-3">
        <div class="form-group <?= form_error('userfile') ? 'has-error' : '' ?>">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <label class="panel-title control-label" for="userfile">
                    	Image
                    </label>
                </div>
                
            </div>
        </div>
    </div>
	   
    <div class="col-xs-12 col-sm-8 col-md-9">
    	<div style="margin-bottom: 15px;">
    		<label for="usr">Item:</label>
    		<input type="text" name="name" id="product_name" class="form-control product_name">
    	</div>
    	<div style="margin-bottom: 15px;">
    		<label for="usr">Price:</label>
    		<input type="text" name="price" id="product_price" class="form-control product_price">
    	</div>
    	<div style="margin-bottom: 15px;">
    		<label for="usr">Quantity:</label>
    		<input type="text" name="quantity" id="product_quantity" class="form-control product_quantity">
    	</div>
    	<div style="margin-bottom: 15px;"> 
    		<label for="usr">Location:</label>
    		<input type="text" name="location" id="product_location" class="form-control product_location">
    	</div>
    	<div style="margin-bottom: 15px;">
    		<label for="usr">Status:</label>
    		<select class="form-control" name="status">
    			<option value="1" selected>Active</option>
    			<option value="0">Inactive</option>
    		</select>
    	</div>
    	<div style="margin-bottom: 15px;"> 
    		<label for="usr">Description:</label>
    		<textarea name="description" id="product_description" class="form-control product_description" rows="5" maxlength="100"></textarea>
    	</div>
    	<div style="margin-bottom: 15px;"> 
    		<label for="usr">Category:</label>
    		<select name="category">
    		<option value="">Select</option>
    		<option value="others">Others</option>
    		<?php foreach ($categories as $key => $category){ ?>
    			<option value="<?php echo $category->id ?>"><?=$category->name ?></option>
    		<?php } ?>
    		</select>
    	</div>
    	<div style="margin-bottom: 15px;">
    		<div class="col-sm-6">
    			<input type="submit" name="add_btn" class="btn btn-success form-control add_btn" value="Add" id="add_btn">
    		</div>
    		<div class="col-sm-6">
    			<input type="reset" name="remove_btn" class="btn btn-warning  form-control" value="Clear">
    		</div>
    	</div>
    </div>	
</div>
<?php echo form_close() ?>
<p>&nbsp;</p>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>