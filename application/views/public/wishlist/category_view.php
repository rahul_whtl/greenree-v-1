<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - User Wishlists Category',
	'link' => (!empty($category->pagination)) ? $category->pagination[0]->slug : $category->slug,
	'sub_link' => 'profile',
	'menu_active' =>'wishlist'
)); ?>
<div class="container">
<?php
// No Items returned - and the user was not filtering
if ( ! $products AND ! $_SERVER['QUERY_STRING']):
?>
	<div class="alert alert-warning">There are no wishlist yet.</div>
<?php else: ?>
	<ul class="breadcrumb">
		<?php foreach ($category->pagination as $row): ?>
			<li>
				<?php
				// If the category we are viewing matches a category in pagination,
				// Do not create an anchor link for it because we are viewing it already
				if ($category->id == $row->id): ?>
					<?php echo $row->name ?>
				<?php else: ?>
					<?php echo anchor('wishlist/category/'.$row->slug, $row->name) ?>
				<?php endif ?>
			</li>
		<?php endforeach ?>
	</ul>

	<div class="row">
		<div class="col-md-2 col-sm-3">
			<div class="lead"><?php echo $category->name ?></div>
			<?php if ($category->sub_categories): ?>
				<div class="page-header" style="margin-bottom:5px">Categories</div>
				<div class="filter-container">
					<ul class="nav nav-pills nav-stacked">
						<?php foreach ($category->sub_categories as $row): ?>
							<li>
								<?php echo anchor('wishlist/category/'.$row->slug, $row->name) ?>
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			<?php endif ?>
            
			<?php // Show Attributes as Filter Options ?>
			<?php foreach ($category->attributes as $index => $attribute): ?>
				<div class="page-header" style="margin-bottom:5px">
					<?php echo $attribute->name ?>
				</div>
				<div class="filter-container">
					<ul class="nav nav-pills nav-stacked">
						<?php foreach ($attribute->descriptions as $key => $description): ?>
							<li class="<?php echo ($this->input->get('ATB') == $description->id) ? 'active' : '' ?>">
								<?php echo anchor(current_url().'?'.($this->input->get('ATB') ? preg_replace('/(^|&)ATB=[^&]*/', '&ATB='.$description->id, $_SERVER['QUERY_STRING']) : $_SERVER['QUERY_STRING'].'&ATB='.$description->id), $description->name) ?>
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			<?php endforeach ?>
			
			<?php if(sizeof($categories) > 1){ ?>
			 <div class="hidden-xs">
			<p>&nbsp;</p>
			<div class="lead">Wishlists by category</div>
            <ul class="list">
            <?php foreach ($categories as $key => $current_cat){ ?>
                <?php if(!empty($current_cat->products) && $current_cat->id !== $category->id) { ?>
                <li><?php echo anchor('wishlist/category/'.$current_cat->slug, $current_cat->name) ?></li>
                        
            <?php } }?>
            </ul>
            </div>
			<?php } ?>
			
			<?php if(sizeof($categories) > 1){ ?>
			<div class="dashboard-header hidden-sm hidden-md hidden-lg mobile-cat">
            	<div class="container">
            	    <a href="#" class="mobile-menu-link"><span>Wishlists from  other category</span><span class="caret"></span></a>
        		    <ul class="nav nav-tabs responsive-tab" role="tablist">
            		    <?php foreach ($categories as $key => $current_cat){ ?>
                            <?php if(!empty($current_cat->products) && $current_cat->id !== $category->id) { ?>
                            <li><?php echo anchor('wishlist/category/'.$current_cat->slug, $current_cat->name) ?></li>
                                    
                        <?php } }?>
            		</ul>
            	</div>
            </div>
            <?php } ?>
            
            <script type="text/javascript">
            		jQuery(document).ready(function(){
            			jQuery(".mobile-menu-link").click(function(event){
            				event.preventDefault();
            				jQuery(".responsive-tab").slideToggle();
            			});	
            		});
            </script>
			
		</div>
		<div class="col-md-10 col-sm-9 wishlist_cat item-wrap">

			<?php if ( ! $products): // NO items found. Because user's sorting options returned Nothing. ?>
				<div class="alert alert-warning">
					We could not find anything based on your sorting options. Please try again or <?= anchor(current_url().'?'.($this->input->get('cate') ? '&cate='.$this->input->get('cate') : '').'&reset=true', 'Reset Filters', 'class="alert-link"') ?>.
				</div>
			<?php else: ?>
				<?php $this->load->view('public/wishlist/wishlist_tiles_view', array(
					'type' => 'tiles',
					'row_class' => 'category-grid',
					'cols' => 'col-xs-6 col-sm-3 col-md-3',
					'products' => $products,
				)) ?>
				<?php echo $this->pagination->create_links() ?>
			<?php endif ?>
		</div>
	</div>
	<p>&nbsp;</p>
<?php endif ?>
</div>
<div id="mynewForm" class="new-form-popup">
  <div class="modal-content">
    <span class="close_info cancel close">&times;</span>
    <div class="div-container" method="POST" id="div-container">
    	<label for="customer_info_heading"><h3>Customer Information</h3></label>
	    <div class="customer_info_text">
	    	
	    </div>
    </div>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.close_info').click(function(event){
      event.preventDefault();
      jQuery("#mynewForm").css('display','none');
    });
    jQuery('.wishlist-info').click(function(event){
        // event.preventDefault();
        var pathname = window.location.pathname;
        var array    = pathname.split('/');
        var value    = array[2];
        var user_id  = jQuery(this).attr('name');
        var user     = '<?php echo $user->id;?>';
        var wishlist = jQuery(this).data('item');
        if(user == '') {
            //jQuery('#mynewForm').css("display", "block");
            window.location.href = '<?php echo base_url('wishlist_info_login') ?>';
        }
        else{
            jQuery.ajax({
	            type : 'POST',
	            url : "<?php echo base_url('wishlist_user_info'); ?>",
	            data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',user_id:user_id, wishlist:wishlist},
	            'success' : function(data){ 
	            	console.log(data);
	            	var data = jQuery.parseJSON(data);
	            	var new_data = '<div><strong>Name : </strong>'+data.customer_name+'</div><div><strong>Phone : </strong>'+data.phone+'</div><div><strong>Email : </strong>'+data.email+'</div><div><strong>Address : </strong>'+data.apartment_name+', '+data.locality+', '+data.city+', '+data.state+'</div>';
	            	jQuery('.customer_info_text').html(new_data);
	                jQuery('#mynewForm').css("display", "block");
	            },
	            'error' : function(request,error){
	                alert("Request: "+JSON.stringify(request));
	            }
	        });
        }
    }); 
});   
</script>
<?php $this->load->view('public/templates/footer') ?>