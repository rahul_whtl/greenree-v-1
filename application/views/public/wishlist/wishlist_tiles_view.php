<?php
$this->load->helper('text');
foreach ($products as $key => $product):
        ?>
		<div class="item <?php echo $cols ?>">
			<div class="thumbnail wishlist-tiles">
				<!-- <?php// if ($product->images): ?>
					<img src="<?php// echo base_url($this->data['app']['file_path_product'].$product->images) ?>" class="group list-group-image" style="margin-bottom:2px">
				<?php// else: ?>
					<img src="<?php //echo base_url() ?>assets/system/no_image.jpg" class="group list-group-image" style="width:100%">
				<?php //endif ?> -->
				<div class="caption">
					<div class="form-group" title="<?php echo character_limiter($product->description, 100) ?>">
						<span><strong><?php echo character_limiter($product->item_name, 20) ?></strong></span>
					</div>
					<div class="form-group wishlist-tiles-description-info" title="<?php echo $product->item_name ?>">
						<span><strong>Description : </strong><?php echo character_limiter($product->description, 100) ?></span>
					</div>
					<div class="form-group" title="<?php echo $product->item_name ?>">
						<span><strong>Quantity Needed : </strong><?php echo character_limiter($product->quantity, 20) ?></span>
					</div>
					<div class="form-group " title="<?php echo $product->item_name ?>">
						<?php //if($user): ?>
							<!-- <a href="<?php// echo base_url('wishlist_user_info').'/'.$user->id ?>"> -->
								<button class="btn btn-md btn-primary wishlist-info" name="<?php echo $product->user_id;?>" data-item="<?php echo $product->item_name ?>">Contact Customer</button>
							<!-- </a> -->
						<?php// else: ?>
							<!-- <a href="<?php// echo base_url('wishlist_info_login') ?>"> -->
								<!-- <button class="btn btn-lg btn-primary" id="wishlist-info">Contact Customer</button> -->
							<!-- </a> -->
						<?php //endif ?>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach ?>