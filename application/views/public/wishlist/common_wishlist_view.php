<?php $this->load->view('public/templates/header', array(
	'title' => 'Wishlist at GreenREE saves your time in exploring preowned items - GreenREE',
	'title_description' => 'You are looking for something, your neighbour might be ready to sell, inform him through GreenREE, no charges from either side.',
	'menu_active' =>'wishlist'
)) ?>
<!--<div class="banner-section">
    <div class="container">
    	<p class="banner-caption">Wishlist</p>
	</div>
</div>-->
<div class="common-wishlist">
	<div class="container">
		<div class="alert-inbox-container">
			<div class="notification_div">
				<div class="alert alert-info alert-inbox" role="alert">
					<p>If you are looking for something that is not present in pre-owned items, let us know by adding to wishlist, sellers will contact you. <a href="<?php echo base_url("user_dashboard/add-wishlist") ?>" class="btn btn-md btn-primary">Add to my wishlist</a></p>
				</div>
			</div>
		</div>
		<p>&nbsp;</p>
		<?php $this->load->helper('text') ?>
		
		
       <?php /* <div class="row wishlist-tiles-view">
<?php foreach ($categories as $key => $category): ?>
		<!--<div class="lead page-header">-->
		<!--	<?php //echo $category->name; ?>-->
		<!--</div>-->
            
		<?php $this->load->view('public/wishlist/wishlist_tiles_view', array(
			'type' => 'tiles',
			'cols' => 'col-xs-6 col-sm-3 col-md-3',
			'products' => $category->products,
		)) ?>
        
        <?php if (isset($this->pagination)): ?>
    	    <!-- Pagination Link -->
    	    <?php echo $this->pagination->create_links() ?>
        <?php endif ?>
    
		<!-- <p class="text-center">
			<?php //echo anchor('category/user/'.$category->slug, 'View More '.$category->name) ?>
		</p> -->
	<?php endforeach ?>
	</div> */ ?>
	
	
	<div class="row">
    <div class="col-md-2 col-sm-3 col-xs-12">
        
        <?php if(sizeof($categories)){
        ?>
        <div class="hidden-xs">
		<div class="lead">Wishlists by category</div>
        <ul class="list">
        <?php foreach ($categories as $key => $category){ 
              if(!empty($category->products)) { ?>
            <li><?php echo anchor('wishlist/category/'.$category->slug, $category->name) ?></li>
        <?php } }?>
        </ul>
		</div>
		<?php } ?>
		
		<?php if(sizeof($categories)){ ?>
		<div class="dashboard-header hidden-sm hidden-md hidden-lg mobile-cat">
        	<div class="container"> 
        	    <a href="#" class="mobile-menu-link"><span>Wishlists by category</span><span class="caret"></span></a>
    		    <ul class="nav nav-tabs responsive-tab" role="tablist">
        		    <?php if(sizeof($categories)){ ?>
                    <?php foreach ($categories as $key => $category){
                            if(!empty($category->products)){    
                            ?>
                        <li><?php echo anchor('wishlist/category/'.$category->slug, $category->name) ?></li>
                    <?php } } } ?>
        		</ul>
        	</div>
        </div>
        <?php } ?>
        
        <script type="text/javascript">
        		jQuery(document).ready(function(){
        			jQuery(".mobile-menu-link").click(function(event){
        				event.preventDefault();
        				jQuery(".mobile-cat .responsive-tab ").slideToggle();
        				jQuery(".mobile-cat .caret").toggleClass('active');
        			});	
        		});
        </script>
		
	</div>
      <div class="col-md-10 col-sm-9 col-xs-12 item-wrap wishlist_cat">
		<?php $this->load->view('public/wishlist/wishlist_tiles_view', array(
			'type' => 'tiles',
			'cols' => 'col-xs-6 col-sm-4 col-md-3 col-lg-3',
			'products' => $products,
		)) ?>
		
		</div>
    </div>
	
	
	</div>
</div>

<div id="mynewForm" class="new-form-popup common-wishlist-modal">
  <div class="modal-content">
    <span class="close_info cancel close">&times;</span>
    <div class="div-container" method="POST" id="div-container">
    	<label for="customer_info_heading"><h3>Customer Information</h3></label>
	    <div class="customer_info_text">
	    	
	    </div>
    </div>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.close_info').click(function(event){
      event.preventDefault();
      jQuery("#mynewForm").css('display','none');
    });
    jQuery('.wishlist-info').click(function(event){
        // event.preventDefault();
        var pathname = window.location.pathname;
        var array    = pathname.split('/');
        var value    = array[2];
        var user_id  = jQuery(this).attr('name');
        var user     = '<?php echo $user->id;?>';
        var wishlist = jQuery(this).data('item');
        if(user == '') {
            //jQuery('#mynewForm').css("display", "block");
            window.location.href = '<?php echo base_url('wishlist_info_login') ?>';
        }
        else{
            jQuery.ajax({
	            type : 'POST',
	            url : "<?php echo base_url('wishlist_user_info'); ?>",
	            data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',user_id:user_id, wishlist:wishlist},
	            'success' : function(data){ 
	            	console.log(data);
	            	var data = jQuery.parseJSON(data);
	            	if(data.customer_name)
	            	    var customer_name = data.customer_name;
	            	else
	            	    var customer_name = 'NA';
	            	if(data.phone)
	            	    var phone = data.phone;
	            	else
	            	    var phone = 'NA';
	            	if(data.email)
	            	    var email = data.email;
	            	else
	            	    var email = 'NA';
	            	if(data.apartment_name)
	            	    var address = data.apartment_name+', '+data.locality+', '+data.city+', '+data.state;
	            	else
	            	    var address = 'NA';
	            	var new_data = '<div><strong>Name : </strong>'+customer_name+'</div><div><strong>Phone : </strong>'+phone+'</div><div><strong>Email : </strong>'+email+'</div><div><strong>Address : </strong>'+address+'</div>';
	            	jQuery('.customer_info_text').html(new_data);
	                jQuery('#mynewForm').css("display", "block");
	            },
	            'error' : function(request,error){
	                alert("Request: "+JSON.stringify(request));
	            }
	        });
        }
    }); 
});   
</script>
<p>&nbsp;</p>
<?php $this->load->view('public/templates/footer') ?>
