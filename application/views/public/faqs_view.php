<?php $this->load->view('public/templates/header', array(
	'title' => 'FAQs - GreenREE',
	'title_description' => 'Frequently Asked Questions',
)) ?>
<div class="faqs">
	<div class="container">
		<h2>Frequently Asked Questions</h2>
		<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
		<h2 class="accordion">How will I get paid</h2>
		<div class="panel">
		  <p>On pick-up of your scrap from your doorstep, amount will be immediately credited to your cash wallet. You can transfer the amount to paytm wallet anytime you want.</p>
		</div>

		<h2 class="accordion">How much time it will take to transfer the money from my cash wallet to paytm account. </h2>
		<div class="panel">
		  <p>It will done within 24 hours after you request to transfer</p>
		</div>

		<h2 class="accordion">Where can I use Cash Wallet for other than cash transfer to paytm?</h2>
		<div class="panel">
		  <p>You can use it for shoping with us from Shop / Buy our products page. As these are from direct manufacturers so you get here at very cheap rates</p>
		</div>

		<h2 class="accordion">What are the products you sell</h2>
		<div class="panel">
		  <p>One category is decorative and utility items that are made out of scrap. Other is cookware and storage utensils that helps to maintain your - health good and planet Green</p>
		</div>

		<h2 class="accordion">Why should I sell my pre owned items on <span class="green-text">Green</span><span class="orange-text">REE</span></h2>
		<div class="panel">
		  <p>GreenREE brings buyers and sellers close from same locality so chances of deals happening becomes more when you find seller in your neighbourhood. GreenREE doesn't charge anything from buyer/seller</p>
		</div>
		
		<h2 class="accordion">For what is the wishlist page about ?</h2>
		<div class="panel">
		  <!--<p>I am having a good condition single bed lying without use for long time but I just don't want to throw it away. But if I find someone really looking for it in my society I woiuld be happy to sell</p>-->
		  <!--<p>Wishlist page has requests from such seekers in your neighbourhood. In case you are looking for something, add it to your wishlist</p>-->
		  <p>In case you want to sell long lying unused item in your home to someone who is really needing it, Check the wishlist - you may find the buyer there.</p>
		</div>
		  
		<h2 class="accordion">How can I contribute more such as donation etc.</h2>
		<div class="panel">
		  <p>The real donation is to make somone self capable to earn, Buy our products and make unprivileged people to earn</p>
		</div>
		
</div>
	<script>
		jQuery(document).ready(function(){
			jQuery('.accordion').click(function(){
				jQuery('.accordion').next().css('maxHeight',null);
				//jQuery('.accordion').removeClass('active');
				jQuery(this).toggleClass('active');
				var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight +100 +"px";
			    } 
			});
		});
	// var acc = document.getElementsByClassName("accordion");
	// var i;

	// for (i = 0; i < acc.length; i++) {
	//   acc[i].addEventListener("click", function() {

	//     this.classList.addClass("active");
	//     var panel = this.nextElementSibling;
	//     if (panel.style.maxHeight){
	//       panel.style.maxHeight = null;
	//     } else {
	//       panel.style.maxHeight = panel.scrollHeight + "px";
	//     } 
	//   });
	// }
	</script>
<?php $this->load->view('public/templates/footer') ?>
<style>
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding-left: 15px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 20px;
  transition: 0.4s;
  line-height: 2em;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: #777;
  font-weight: bold;
  float: right;
  margin-right: 20px;
  font-size: 35px;
}

.active:after {
  content: "\2212";
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
.active + .panel p{
  padding-top: 15px;
  padding-bottom: 15px;
}
</style>