<?php $this->load->library('store'); $owner = $this->store->owner(); ?>
<!-- Modal -->
<div id="myForm" class="form-popup" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <form class="form-container">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please enter your Mobile Number</h4>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!--<label class="control-label" for="email">Mobile Number</label>-->
                        <input type="text" placeholder="Enter Mobile Number" class="mobile_number" name="container" required/>
                        <div class="div_danger"></div>
                    </div>
                </div>
                <button type="button" class="btn btn-md btn-success generate_otp_btn">Generate OTP</button>
                <div class="col-xs-12 col-sm-12 col-md-12 otp_div hide">   
                    <label class="control-label" for="otp"><b>OTP</b></label>
                    <input type="text" placeholder="Enter OTP" class="form-group otp" name="otp" required>
                    <div class="div_danger1"></div>
                    <input type="button" class="btn btn-success btn-md submit_otp_btn" value="Submit OTP">
                    <input type="button" class="btn btn-success btn-md hide" id="sell_scrap_login_otp" value="OTP">
                    <input type="button" class="btn btn-md btn-primary resend_otp_btn" value="Resend OTP">
                </div>
                <div class="otp_login_help">If you have any problem login with OTP, contact us <a href="tel:980-494-0000">9804940000</a></div>
            </div>
        </div>
    </div>
    </form>
  </div>
</div>
</div>

<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="col-md-12 footer-top-text">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 text-center footer-top-logo-part">
                        <a href="<?php echo base_url(); ?>"><div class="footer-top-logo"></div></a>
                        <div class="footer-top-logo-text">At <span class="green-text">Green</span><span class="orange-text">REE</span>, our aim is to keep our planet <span class="green-text">Green</span> by re-using the limited natural resources to maximum extent. Be a part of this revolutionary step, sell everything that is lying waste in your home and join us in promoting <span class="orange-text">REusE</span>.</div>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 text-center quick-links-part">
                        <div class="lead footer-top-header">Quick Links</div>
                        <ul class="quick-links">
                            <li><a href="<?php echo base_url('about_us'); ?>">How It Works</a></li>
                            <li><a href="<?php echo base_url('faqs'); ?>">FAQ's</a></li>
                            <li><a href="<?php echo base_url('contact_us'); ?>">Contact Us</a></li>
                            <li><a href="<?php echo base_url('privacy-policy'); ?>">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4 text-center get-in-touch-part">
                        <div class="lead footer-top-header">Get in Touch</div>
                        <ul class="get-in-touch">
                            <li><span class="text">Call or Whatsapp</span>: <a href="tel:980-494-0000">9804940000</a></li>
                            <li><span class="text">Email</span>: <a href="mailto:contact@greenree.com">contact@greenree.com</a></li>
                            <!--<li><span class="text">Website</span>: <a href="#">www.greenree.com</a></li>
                            <li><span class="text">Address</span>: <span>ABC Pvt Ltd.</span></li>-->
                        </ul>
                        <a href="https://search.google.com/local/writereview?placeid=ChIJ0c76TAQVrjsRnY2cWcGCPP0">
                            <div class="google-reviews-wrapper">
                              <h4>Google Rating</h4>
                              <svg class="google-reviews-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="44" width="44"><g fill="none" fill-rule="evenodd"><path d="M482.56 261.36c0-16.73-1.5-32.83-4.29-48.27H256v91.29h127.01c-5.47 29.5-22.1 54.49-47.09 71.23v59.21h76.27c44.63-41.09 70.37-101.59 70.37-173.46z" fill="#4285f4"></path><path d="M256 492c63.72 0 117.14-21.13 156.19-57.18l-76.27-59.21c-21.13 14.16-48.17 22.53-79.92 22.53-61.47 0-113.49-41.51-132.05-97.3H45.1v61.15c38.83 77.13 118.64 130.01 210.9 130.01z" fill="#34a853"></path><path d="M123.95 300.84c-4.72-14.16-7.4-29.29-7.4-44.84s2.68-30.68 7.4-44.84V150.01H45.1C29.12 181.87 20 217.92 20 256c0 38.08 9.12 74.13 25.1 105.99l78.85-61.15z" fill="#fbbc05"></path><path d="M256 113.86c34.65 0 65.76 11.91 90.22 35.29l67.69-67.69C373.03 43.39 319.61 20 256 20c-92.25 0-172.07 52.89-210.9 130.01l78.85 61.15c18.56-55.78 70.59-97.3 132.05-97.3z" fill="#ea4335"></path><path d="M20 20h472v472H20V20z"></path></g></svg>
                              <div id="google-reviews"></div>
                              <div id="schema"></div>
                            </div>
                        </a>
                        <link rel="stylesheet" href="https://cdn.rawgit.com/stevenmonson/googleReviews/master/google-places.css">
                        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAGmwZicoabOLlWYNb88UkjrXdFWIAKpnA&signed_in=true&libraries=places"></script>

                        <script>
                        jQuery(document).ready(function( $ ) {
                           $("#google-reviews").googlePlaces({
                                placeId: 'ChIJ0c76TAQVrjsRnY2cWcGCPP0', 
                                render: ['rating'],
                                min_rating: 1,
                                max_rows:4,
                           });
                        });
                        </script>
                        <script type="text/javascript">
                            (function($) {

    var namespace = 'googlePlaces';

    $.googlePlaces = function(element, options) {

        var defaults = {
              placeId: 'ChIJ0c76TAQVrjsRnY2cWcGCPP0' // placeId provided by google api documentation
            , render: ['reviews']
            , min_rating: 0
            , max_rows: 0
            , map_plug_id: 'map-plug'
            , rotateTime: false
            , shorten_names: true
            , schema:{
                  displayElement: '#schema'
                , type: 'Store'
                , beforeText: 'Google Users Have Rated'
                , middleText: 'Based on'
                , afterText: 'ratings and reviews'
            }
            , address:{
                displayElement: "#google-address"
              }
            , phone:{
                displayElement: "#google-phone"
            }
            , staticMap:{
                  displayElement: "#google-static-map"
                , width: 512
                , height: 512
                , zoom: 17
                , type: "roadmap"
            }
            , hours:{
                displayElement: "#google-hours"
            }
        };

        var plugin = this;

        plugin.settings = {}

        var $element = $(element),
             element = element;

        plugin.init = function() {
          plugin.settings = $.extend({}, defaults, options);
          plugin.settings.schema = $.extend({}, defaults.schema, options.schema);
          $element.html("<div id='" + plugin.settings.map_plug_id + "'></div>"); // create a plug for google to load data into
          initialize_place(function(place){
            plugin.place_data = place;

            // Trigger event before render
            $element.trigger('beforeRender.' + namespace);

            if(plugin.settings.render.indexOf('rating') > -1){
              renderRating(plugin.place_data.rating);
            }
            // render specified sections
            if(plugin.settings.render.indexOf('reviews') > -1){
              renderReviews(plugin.place_data.reviews);
              if(!!plugin.settings.rotateTime) {
                  initRotation();
              }
            }
            if(plugin.settings.render.indexOf('address') > -1){
              renderAddress(
                  capture_element(plugin.settings.address.displayElement)
                , plugin.place_data.adr_address
              );
            }
            if(plugin.settings.render.indexOf('phone') > -1){
              renderPhone(
                  capture_element(plugin.settings.phone.displayElement)
                , plugin.place_data.formatted_phone_number
              );
            }
            if(plugin.settings.render.indexOf('staticMap') > -1){
              renderStaticMap(
                  capture_element(plugin.settings.staticMap.displayElement)
                , plugin.place_data.formatted_address
              );
            }
            if(plugin.settings.render.indexOf('hours') > -1){
              renderHours(
                  capture_element(plugin.settings.hours.displayElement)
                , plugin.place_data.opening_hours
              );
            }

            // render schema markup
            addSchemaMarkup(
                capture_element(plugin.settings.schema.displayElement)
              , plugin.place_data
            );

            // Trigger event after render
            $element.trigger('afterRender.' + namespace);

          });
        }

        var capture_element = function(element){
          if(element instanceof jQuery){
            return element;
          }else if(typeof element == 'string'){
            try{
              var ele = $(element);
              if( ele.length ){
                return ele;
              }else{
                throw 'Element [' + element + '] couldnt be found in the DOM. Skipping '+element+' markup generation.';
              }
            }catch(e){
              console.warn(e);
            }
          }
        }

        var initialize_place = function(c){
          var map = new google.maps.Map(document.getElementById(plugin.settings.map_plug_id));

          var request = {
            placeId: plugin.settings.placeId
          };

          var service = new google.maps.places.PlacesService(map);

          service.getDetails(request, function(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
              c(place);
            }
          });
        }

        var sort_by_date = function(ray) {
          ray.sort(function(a, b){
            var keyA = new Date(a.time),
            keyB = new Date(b.time);
            // Compare the 2 dates
            if(keyA < keyB) return -1;
            if(keyA > keyB) return 1;
            return 0;
          });
          return ray;
        }

        var filter_minimum_rating = function(reviews){
          for (var i = reviews.length -1; i >= 0; i--) {
            if(reviews[i].rating < plugin.settings.min_rating){
              reviews.splice(i,1);
            }
          }
          return reviews;
        }

        var renderRating = function(rating){
            var html = "";
            var star = renderAverageStars(rating);
            html = "<div class='average-rating'><h4>"+star+"</h4></div>";
            $element.append(html);
        }

        var shorten_name = function(name) {
          if (name.split(" ").length > 1) {
            var xname = "";
            xname = name.split(" ");
            return xname[0] + " " + xname[1][0] + ".";
          }
        }

        var renderReviews = function(reviews){
          reviews = sort_by_date(reviews);
          reviews = filter_minimum_rating(reviews);
          var html = "";
          var row_count = (plugin.settings.max_rows > 0)? plugin.settings.max_rows - 1 : reviews.length - 1;
          // make sure the row_count is not greater than available records
          row_count = (row_count > reviews.length-1)? reviews.length -1 : row_count;
          for (var i = row_count; i >= 0; i--) {
            var stars = renderStars(reviews[i].rating);
            var date = convertTime(reviews[i].time);
            if(plugin.settings.shorten_names == true) {
              var name = shorten_name(reviews[i].author_name);
            } else {
              var name = reviews[i].author_name + "</span><span class='review-sep'>, </span>";
            };
            html = html+"<div class='review-item'><div class='review-meta'><span class='review-author'>"+name+"<span class='review-date'>"+date+"</span></div>"+stars+"<p class='review-text'>"+reviews[i].text+"</p></div>"
          };
          $element.append(html);
        }

        var renderHours = function(element, data){
          if(element instanceof jQuery){
            var html = "<ul>";
            data.weekday_text.forEach(function(day){
              html += "<li>"+day+"</li>";
            });
            html += "</ul>";
            element.append(html);
          }
        }

        var renderStaticMap = function(element, data){
          if(element instanceof jQuery){
            var map = plugin.settings.staticMap;
            element.append(
              "<img src='https://maps.googleapis.com/maps/api/staticmap"+
                "?size="+map.width+"x"+map.height+
                "&zoom="+map.zoom+
                "&maptype="+map.type+
                "&markers=size:large%7Ccolor:red%7C"+data+"'>"+
              "</img>");
          }
        }

        var renderAddress = function(element, data){
          if(element instanceof jQuery){
            element.append(data);
          }
        }

        var renderPhone = function(element, data){
          if(element instanceof jQuery){
            element.append(data);
          }
        }

        var initRotation = function() {
            var $reviewEls = $element.children('.review-item');
            var currentIdx = $reviewEls.length > 0 ? 0 : false;
            $reviewEls.hide();
            if(currentIdx !== false) {
                $($reviewEls[currentIdx]).show();
                setInterval(function(){
                    if(++currentIdx >= $reviewEls.length) {
                        currentIdx = 0;
                    }
                    $reviewEls.hide();
                    $($reviewEls[currentIdx]).fadeIn('slow');
                }, plugin.settings.rotateTime);
            }
        }

        var renderStars = function(rating){
          var stars = "<div class='review-stars'><ul>";

          // fill in gold stars
          for (var i = 0; i < rating; i++) {
            stars = stars+"<li><i class='star'></i></li>";
          };

          // fill in empty stars
          if(rating < 5){
            for (var i = 0; i < (5 - rating); i++) {
              stars = stars+"<li><i class='star inactive'></i></li>";
            };
          }
          stars = stars+"</ul></div>";
          return stars;
        }

        var renderAverageStars = function(rating){
            var stars = "<div class='review-stars'><ul><li><i>"+rating+"&nbsp;</i></li>";
            var activeStars = parseInt(rating);
            var inactiveStars = 5 - activeStars;
            var width = (rating - activeStars) * 100 + '%';

            // fill in gold stars
            for (var i = 0; i < activeStars; i++) {
              stars += "<li><i class='star'></i></li>";
            };

            // fill in empty stars
            if(inactiveStars > 0){
              for (var i = 0; i < inactiveStars; i++) {
                  if (i === 0) {
                      stars += "<li style='position: relative;'><i class='star inactive'></i><i class='star' style='position: absolute;top: 0;left: 0;overflow: hidden;width: "+width+"'></i></li>";
                  } else {
                      stars += "<li><i class='star inactive'></i></li>";
                  }
              };
            }
            stars += "</ul></div>";
            return stars;
        }

        var convertTime = function(UNIX_timestamp){
          var a = new Date(UNIX_timestamp * 1000);
          var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          var time = months[a.getMonth()] + ' ' + a.getDate() + ', ' + a.getFullYear();
          return time;
        }

        var addSchemaMarkup = function(element, placeData) {
          var reviews = placeData.reviews;
          console.log(placeData);
          var lastIndex = reviews.length - 1;
          var reviewPointTotal = 0;
          var schema = plugin.settings.schema;
          for (var i = lastIndex; i >= 0; i--) {
            reviewPointTotal += reviews[i].rating;
          };
          // Set totals and averages - may be used later.
          //var averageReview = reviewPointTotal / ( reviews.length );
          var averageReview = placeData.rating;
          /*if(element instanceof jQuery){
            element.append( '<span itemscope="" itemtype="http://schema.org/' + schema.type + '">'
            +  '<meta itemprop="url" content="' + location.origin + '">'
            +  schema.beforeText + ' <span itemprop="name">' + placeData.name + '</span> '
            +  '<span itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">'
            +    '<span itemprop="ratingValue">' + averageReview.toFixed(2) + '</span>/<span itemprop="bestRating">5</span> '
            +  schema.middleText + ' <span itemprop="ratingCount">' + placeData.user_ratings_total + '</span> '
            +  schema.afterText
            +  '</span>'
            +'</span>');
          }*/
          if(element instanceof jQuery){
            element.append( '<span itemscope="" itemtype="http://schema.org/' + schema.type + '">'
            +  '<meta itemprop="url" content="' + location.origin + '">'
            +  ' <span itemprop="name" class="company_name">' + placeData.name + '</span> '
            +  '<span itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">'
            +    '<span itemprop="ratingValue" class="average_rating">' + averageReview.toFixed(2) + '</span><span itemprop="bestRating" class="total_star_rating">5</span> '
            +  schema.middleText + ' <span itemprop="ratingCount">' + placeData.user_ratings_total + '</span> '
            +  schema.afterText
            +  '</span>'
            +'</span>');
          }
        }

        plugin.init();

    }

    $.fn.googlePlaces = function(options) {

        return this.each(function() {
            if (undefined == $(this).data(namespace)) {
                var plugin = new $.googlePlaces(this, options);
                $(this).data(namespace, plugin);
            }
        });

    }

})(jQuery);
                        </script>
                    </div>
                </div>
            </div>    
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="col-md-12">
                <div class="row text-center">
                    <div class="col-md-12">
                        <small>&copy 2019 GreenREE Proprietary. All Copyright  Reserved.</small>
                    </div>
                    <!--<div class="col-xs-6 text-right">-->
                    <!--    <small style="font-size:13px"><a href="https://whtl.co.in" target="_blank" title="Website Designing Company Bangalore" style="color:#FFF">Website Design</a> by WHTL</small>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/global.js"></script>
<script src="<?php echo base_url('assets/js/slider-mobile-swipe.js') ?>"></script>
 <!-- for calender -->
 <script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker1.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/datetimepicker.js'); ?>"></script>
 <!-- for calender -->
<?php
// For example if a user submitted a form from a modal.
// You wand to trigger that modal in event of errors for instance
$redirect_inner = $this->session->flashdata('location');
?>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
            });

        $('#nav-to-top').click(function(){
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });

        $('.dropdown-toggle').dropdown();


        <?php if (isset($redirect_inner['toggle'])): ?>
            <?php if ($redirect_inner['toggle'] === 'modal'): ?>
                $("<?php echo $redirect_inner['target'] ?>").modal('show');
            <?php endif ?>
        <?php endif ?>

        $('input[data-toggle="loading"]').click(function() {$(this).val('Working ...')} );
        $('button[data-toggle="loading"]').click(function() {$(this).html('Working ...')} );

        $(".add-to-cart").submit(function(e) {
            // var form = $(this);
            // $.ajax({
            //     type: form.attr('method'),
            //     url: form.attr('action'),
            //     data: form.serialize(), // serializes the form's elements.
            //     success: function(data)
            //     {
            //         $('body').append(data)
            //         $('#mini-cart').html(data);
            //         var cartTotal = $('#cart-item-no').html();
            //         $('#cart-item-no').html(cartTotal++);
            //     }
            // });

            // e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    })
    var global_id = '<?php echo $user->id; ?>';
    
</script>
<input type ='hidden' name='global_id' id='global_id' value='<?php echo $user->id; ?>'>
<input type ='hidden' name='global_tokan' id='tokan' value='<?php echo $this->security->get_csrf_token_name(); ?>'>
<input type ='hidden' name='global_cookiee' id='cookiee' value='<?php echo $this->security->get_csrf_hash(); ?>'>

<?php if (isset($script)): ?>
	<?php echo $script ?>
<?php endif ?>
<a href="#0" class="cd-top js-cd-top">Top</a>
<script src="<?php echo base_url('assets/js/scroll-top.js') ?>"></script>
<script src="<?php echo base_url('assets/js/theme_global.js') ?>"></script>
</body>
</html>