
<?php
$cart_data = $this->flexi_cart->cart_contents();
if ( ! isset($require)) $require = NULL; 
if ( ! isset($title)) $title = false;
if ( ! isset($title_description)) $title_description = false;
if ( ! isset($link)) $link = false;
if ( ! isset($sub_link)) $sub_link = false;

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/icons/favicon.png'); ?>" type="image/png" style="width: 20px;" />
    <meta charset="utf-8">
    <meta name="description" content="<?php echo $title_description; ?>"> 
    <meta name="robots" content="index, follow">
    <meta name="copyright" content="Copyright © <?php echo date('Y').', '.$owner->name ?>, All Rights Reserved">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/jasny-bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/cropper.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/styles.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/greenree.css') ?>" />

    <script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
    <?php /*<script src="<?php echo base_url('assets/js/generic-theme.js') ?>"></script> */ ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136517748-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-136517748-1');
    </script>
</head>

<body>
	<div class="header">
    <div class="upper-header">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6 col-sm-6 col-xs-7 float-left">
	                <span class="mail"><a href="mailto:contact@greenree.com">contact@greenree.com</a></span>
	            </div>
	            <div class="col-md-6 col-sm-6 col-xs-5 float-right">
	            	<ul class="upper-header-social">
	                    <li class="fb"><a href="https://www.facebook.com/GreenReee" target="_blank"></a></li>
	                    <li class="instagram"><a href="https://www.instagram.com/greenreee" target="_blank"></a></li>
	                    <li class="google"><a href="#"></a></li>
	                    <li class="twiter"><a href="#"></a></li>
	                </ul> 
	            	<ul class="top-nav">
		                <li><a href="<?php echo base_url('faqs'); ?>" class="faq">FAQs</a></li>
		                <li><a href="<?php echo base_url('about_us'); ?>" class="contact">How It Works</a></li>
		            </ul>
	            </div>
	        </div>
	    </div>
	</div>
<?php// print_r($user);?>
    <!-- upper header part start -->  
    <nav class="navbar navbar-default" style="border-radius:0;margin:0;border:0;background:inherit;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="<?php echo base_url()?>" >
                    <img src="<?php echo base_url('assets/images/icons/greenree-logo.png'); ?>">
                </a>
                <ul class="user-and-cart">
                <?php if ($user): ?>
		                        <li role="presentation" class="nav-bar-btn user-drop-down dashboard scroll-item dropdown <?php echo ($link === 'account') ? 'active' : '' ?>">
		                            <a href="#" class="login-user dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		                                <?php if ($user->avatar): ?>
		                                    <img src="<?= base_url('/assets/images/my_profile/'.$user->avatar) ?>" alt=""
		                                    >
		                                <?php else: ?>
		                                <span class="glyphicon glyphicon-user"></span>
		                                <?php endif ?>
		                                <?php if($user->first_name): ?>
		                                    <?= $user->first_name ?>
		                                <?php else: ?>
		                                <?php echo $user->last_name; ?>
		                                <?php endif ?>    
		                                <span class="caret"></span>
		                            </a>
		                            <ul class="dropdown-menu">
		                                <li><?php echo anchor('user-dashboard', '<i class="glyphicon glyphicon-dashboard" style="margin-right:10px;"></i> Dashboard') ?></li>
		                                <li><?php echo anchor('profile', '<i class="glyphicon glyphicon-user" style="margin-right:10px;"></i> My Profile') ?></li>
		                                <li><?php echo anchor('logout', '<i class="glyphicon glyphicon-log-out" style="margin-right:10px;"></i> Logout', 'style="color:#a94442"') ?></li>
		                            </ul>
		                        </li>
                    <?php else: ?>
                        <li role="presentation" class="nav-bar-btn <?php// echo ($link === 'login') ? 'active' : '' ?>">
                            <a class="login-user" href="<?= site_url('login') ?>">
                                <span style="margin-right:5px"></span>
                                Login & Signup
                            </a>
                        </li>
                        
                    <?php endif ?>
                	<?php// if ($user): ?>
                                <li role="presentation" class="nav-bar-btn cart_btn scroll-item dropdown <?php echo ($link === 'cart') ? 'active' : '' ?>">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-shopping-cart"></span>
                                        <span class="label <?php echo (0>1) ? 'label-success' : 'label-default' ?>" id="cart-item-no">
                                            <?php echo $cart_data['summary']['total_items'] ?>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu" style="padding:1px;" id="mini-cart">
                                        <?php $this->load->view('public/cart/cart_data', array(
                                            'cart_data'=>$cart_data
                                        )) ?>
                                    </ul>
                                </li>
                                <?php //endif ?>
                            
                </ul>
            </div>
            <div class="row">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="col-md-6 nav navbar-nav">
                        <ul>
                            <li class="nav-bar-btn"><a href="<?= base_url() ?>">Home</a></li>
                            <!-- <li class="nav-bar-btn"><a href="<?//= base_url('about_us'); ?>">HOW IT WORKS</a></li> -->
                            <!-- <li class="nav-bar-btn"><a href="#">ABOUT US</a></li> -->
                            <!-- <li class="nav-bar-btn"><a href="<?php //echo base_url('contact_us'); ?>">CONTACT US</a></li> -->
                            <li class="sell_now nav-bar-btn"><a href="<?= site_url('sell-scrap') ?>">Sell Scrap</a></li>
                            <li class="nav-bar-btn"><a href="<?= base_url('shop');  ?>">Shop</a></li>
                            <li class="nav-bar-btn"><a href="<?= base_url('old-2-gold');  ?>">Old-2-Gold</a></li>
                            <li class="nav-bar-btn"><a href="<?= base_url('wishlist');  ?>">Wishlist</a>
                            </li>
                            <!--<li class="nav-bar-btn"><a href="<?//= base_url('wishlist');  ?>">Wishlist</a>
                            </li>-->
                            <li class="nav-bar-btn"><a href="<?= base_url('user_dashboard/my-wallet-cash');  ?>">My Wallet</a>
                            </li>
                            <li class="nav-bar-btn"><a href="<?= base_url('faqs');  ?>">FAQs</a></li>
                            <li class="nav-bar-btn"><a href="<?= base_url('about_us');  ?>">How It Works</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
<!-- upper header part end -->     
    <div class="container text-muted nav-bar">
        <div class="row">
            <div class="col-md-3 greenree-logo">
                <a href="<?= base_url() ?>" class="text-muted">
                    <img src="<?php echo base_url('assets/images/icons/greenree-logo.png'); ?>">
                </a>
                <!-- <a href="<?php //echo site_url() ?>" class="text-muted">
                    <img alt="Brand" src="<?php// echo base_url($this->store->owner()->logo) ?>" style="margin-top:5px;width:45px;">
                    <div style="margin-top:5px;font-size:16px;font-weight:bold;"><?php //echo $owner->name ?></div>
                </a> -->
            </div>
            <div class="col-md-9 menu-list">
                <ul class="nav nav-pills navbar-right" style="overflow:initial;margin-top:20px;margin-bottom:10px">
                    <li class="nav-bar-btn <?php echo ($menu_active == 'home') ? 'active' : '' ?>"><a href="<?= base_url() ?>">Home</a></li>
                    <!-- <li class="nav-bar-btn"><a href="<?//= base_url('about_us'); ?>">HOW IT WORKS</a></li> -->
                    
                    <!-- <li class="nav-bar-btn"><a href="<?php //echo base_url('contact_us'); ?>">CONTACT US</a></li> -->
                    
                    <li role="presentation" class="nav-bar-btn scroll-item <?php echo ($menu_active == 'sell_now') ? 'active' : '' ?>">
                        <a class="sell_now" href="<?= site_url('sell-scrap') ?>">
                            Sell Scrap
                        </a>
                    </li>
                    <li class="nav-bar-btn <?php echo ($menu_active == 'shop') ? 'active' : '' ?>"><a href="<?php  echo base_url('shop');  ?>">Shop<!-- Buy Our Products (Best Out of Scrap) --></a></li>
                   <!--  <li><a class="wallet_cash" href="<?php // echo base_url('user_dashboard/my-wallet-cash');  ?>">My Cash Wallet</a></li> -->
                    <li class="nav-bar-btn <?php echo ($menu_active == 'user_products') ? 'active' : '' ?>"><a href="<?= base_url('old-2-gold');  ?>"><!-- Buy / Sell  -->Old-2-Gold</a></li>
                    <li role="presentation" class="nav-bar-btn <?php echo ($menu_active == 'wishlist') ? 'active' : '' ?>">
                        <a href="<?= site_url('wishlist') ?>">
                            Wishlist
                        </a>
                    </li>
                    <li class="nav-bar-btn <?php echo ($menu_active == 'my_wallet') ? 'active' : '' ?>"><a href="<? if(!empty($user->first_name)): echo base_url('user_dashboard/my-wallet-cash') ?><?php else: echo base_url('user_dashboard/my-wallet-cash');  ?> <?php endif ?>">My Wallet</a>
                    </li>
                    <?php
                    // Admin has setup various currencies
                    if ($this->flexi_cart->get_currency_data()): ?>
                        <li role="presentation" class="scroll-item dropdown">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php //echo $this->flexi_cart->currency_name() ?> <span class="caret"></span>
                            </a> -->
                           <!--  <ul class="dropdown-menu">
                                <?php //foreach ($this->flexi_cart->get_currency_data() as $currency): ?>
                                    <li class="<?php// echo ($this->flexi_cart->currency_name() == $currency['curr_name']) ? 'active' : '' ?>">
                                        <?php// $redirect = ($_SERVER['QUERY_STRING'] !== '') ? '?'.$_SERVER['QUERY_STRING'] : '' ?>
                                        <?php// echo anchor('cart/set_currency'.'?currency='.$currency['curr_name'].'&redirect='.current_url().$redirect, $currency['curr_name']) ?>
                                    </li>
                                <?php //endforeach ?>
                            </ul> -->
                        </li>
                    <!-- <?php// else:
                    // No setup currencies, use defaults.
                    ?> -->
                       <!--  <li role="presentation" class="disabled"><a href="#"><?php// echo $this->flexi_cart->currency_name() ?></a></li> -->
                    <?php endif ?>
                    <?php if ($user): ?>
                        <li role="presentation" class="nav-bar-btn user-drop-down dashboard scroll-item dropdown <?php echo ($link === 'account') ? 'active' : '' ?>">
                            <a href="#" class="login-user dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php if ($user->avatar): ?>
                                    <img src="<?= base_url('/assets/images/my_profile/'.$user->avatar) ?>" alt=""
                                    >
                                <?php else: ?>
                                <span class="glyphicon glyphicon-user"></span>
                                <?php endif ?>
                                <?php if($user->first_name): ?>
                                    <?= $user->first_name ?>
                                <?php else: ?>
                                <?php echo $user->last_name; ?>
                                <?php endif ?>    
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><?php echo anchor('user-dashboard', '<i class="glyphicon glyphicon-dashboard" style="margin-right:10px;"></i> Dashboard') ?></li>
                                <li><?php echo anchor('profile', '<i class="glyphicon glyphicon-user" style="margin-right:10px;"></i> My Profile') ?></li>
                                <li><?php echo anchor('logout', '<i class="glyphicon glyphicon-log-out" style="margin-right:10px;"></i> Logout', 'style="color:#a94442"') ?></li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li role="presentation" class="nav-bar-btn <?php// echo ($link === 'login') ? 'active' : '' ?>">
                            <a class="login-user" href="<?= site_url('login') ?>">
                                <span style="margin-right:5px"></span>
                                Login & Signup
                            </a>
                        </li>
                        
                    <?php endif ?>
                    <?php// if ($user): ?>
                    <li role="presentation" class="cart_btn scroll-item dropdown <?php echo ($link === 'cart') ? 'active' : '' ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-shopping-cart">Cart<ul class="dropdown-menu" style="padding:1px;" id="mini-cart">
                            <?php $this->load->view('public/cart/cart_data', array(
                                'cart_data'=>$cart_data
                            )) ?>
                        </ul></span>
                            <span class="label <?php echo (0>1) ? 'label-success' : 'label-default' ?>" id="cart-item-no">
                                <?php echo $cart_data['summary']['total_items'] ?>
                            </span>
                        </a>
                    </li>
                    <?php //endif ?>
                   
                </ul>
            </div>
        </div>
    </div>
    <hr class="header-hr">
	</div>

    <div id="body">
<div class="validation_errors_msg">
    <div class="container">
        <?php // Breadcrumbs for pages ?>
        <?php if (!isset($breadcrumbs)) $breadcrumbs = array(); ?>
        <?php if ($breadcrumbs): ?>
            <ol class="breadcrumb">
                <?php foreach ($breadcrumbs as $nav): ?>
                    <li class="<?php echo ($nav['link']) ? '' : 'active' ?>">
                        <?php echo ($nav['link']) ? anchor($nav['link'], $nav['name']) : $nav['name'] ?>
                    </li>
                <?php endforeach ?>
            </ol>
        <?php endif ?>
        <?php // End of breadcrumbs ?>
    <?php if(!($active1=='dashboard')): ?>
        <?php // Alert users to errors, changes and notifications ?>
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger">Check the form for errors and try again.</div>
        <?php else: ?>
            <?php if (! empty($message)): ?>
                    <div class="alert alert-inbox-session alert-inbox" role="alert">
                        <span><?php echo $message ?></span>
                    </div>
            <?php endif ?>
        <?php endif ?>
        <?php // End of Alerts ?>
    <?php endif ?>    
    </div>

<!-- <div class="form-popup" id="myForm">
  <a class="close text-right" href="#" style="font-size: 40px;">×</a> 
  <form class="form-container" method="POST" id="Form">
    <label for="email"><b>Enter Your Mobile Number</b></label>
    <input type="text" placeholder="Enter Mobile Number" class="mobile_number" name="container" required>
    <div class="div_danger"></div>
    <button type="submit" class="btn generate_otp_btn">Generate OTP</button>
    <div class="otp_div hide">   
        <label for="otp"><b>OTP</b></label>
        <input type="text" placeholder="Enter OTP" class="otp" name="otp" required>
        <div class="div_danger1"></div>
        <input type="button" class="btn submit_otp_btn" value="Submit OTP">
        <input type="button" class="btn resend_otp_btn" value="Resend OTP">
    </div>
  </form>
</div> -->



</div>