<?php $this->load->view('public/templates/header', array(
	'title' => 'Dashboard',
	'link' => 'account'
)) ?>

<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'scrap')) ?>
<div class="scrap-details-view">
	<div class="container">
		<?php foreach ($scrap as $value) { ?>		
		<div class="lead page-header" style="padding-bottom: 40px;">
			<div class="col-md-4">
				<span>
					<strong>Scrap Request Number:</strong>
					<?php if(empty($value->id)):?>
						<?php echo '--'; ?>
					<?php else:?>
						<?php echo " ".$value->id; ?>
					<?php endif ?>
				</span>
			</div>
			<div class="col-md-8">
				<span>
					<strong style="float: left;">Scrap Requested Date:</strong>
					<?php if(date('Y-m-d', strtotime($value->requested_date))== '1970-01-01'):?>
						<?php echo ' '; ?>
					<?php else:?>
						<?php echo date('jS M Y', strtotime($value->requested_date)); ?>
					<?php endif ?>
				</span>
				<span style="float: right;">
					<strong>Scrap Request Status: </strong>
					<?php if(empty($value->periodic_request_status)):?>
						<?php echo '--'; ?>
					<?php else:?>
						<?php echo "Active"; ?>
					<?php endif ?>
				</span>
			</div>
		</div>
		<?php }?>		
		<?php if (empty($result)): ?>
		<div class="alert alert-warning">There are no Periodic Scrap Request History Available. You  are a new Customer.</div>
		<?php else: ?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default panel-inverse">
							<div class="panel-heading">
								Periodic Scrap Request History
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane" id="points-history" style="display: block;">
										<?php if (empty($result)): ?>
										<div class="alert alert-warning" style="margin-top:20px;margin-bottom:0px;">
										There are no Periodic Scrap Request History Available. You  are a new Customer.
										</div>
										<?php else: ?>
										<table class="table table-striped" style="margin:0;font-size:100%">
											<thead class="text-muted">
												<tr>
													<th>
														<div data-toggle="tooltip">
														Picked Up Date
														</div>
													</th>
													<th class="text-center">
														<div data-toggle="tooltip">
														Customer Presence
														</div>
													</th>
													<th class="text-center">
														<div data-toggle="tooltip" >
														Scrap Weight
														</div>
													</th>
													<th class="text-center">
														<div data-toggle="tooltip">
														Scrap Details
														</div>
													</th>
													<th class="text-center">
														<div data-toggle="tooltip">
														Point
														</div>
													</th>
													<th class="text-center">
														<div data-toggle="tooltip">
														Not Picked Up Reason
														</div>
													</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($result as $value){ ?>
												<tr>
													<td>
														<?php if(date('Y-m-d', strtotime($value->picked_up_date))== '1970-01-01'):?>
															<?php echo '--'; ?>
														<?php else:?>	
															<?php echo date('jS M Y' ,strtotime($value->picked_up_date)) ; ?>
														<?php endif ?>
													</td>
													<td class="text-center">
														<?php if(empty($value->customer_presence)):?>
															<?php echo '--'; ?>
														<?php else:?>
															<?php echo $value->customer_presence; ?>
														<?php endif ?>
													</td>
													<td class="text-center">
														<?php if(empty($value->scrap_weight)):?>
															<?php echo '--'; ?>
														<?php else:?>
															<?php echo $value->scrap_weight; ?>
														<?php endif ?>
													</td>
													<td class="text-center">
														<?php if(empty($value->scrap_details)):?>
															<?php echo '--'; ?>
														<?php else: ?>	
															<?php echo $value->scrap_details;?>
														<?php endif ?>
													</td>
													<td class="text-center">
														<?php if(empty($value->reward_point)):?>
															<?php echo '--'; ?>
														<?php else: ?>
														<?php echo $value->reward_point;?>
														<?php endif ?>
													</td>
													<td class="text-center">
														<?php if(empty($value->not_picked_reason)):?>
															<?php echo '--'; ?>
														<?php else: ?>
															<?php echo $value->not_picked_reason;?>
														<?php endif ?>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
										<?php endif ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
    		<?php endif ?>	
    	</div>
	</div>		

<?php $this->load->view('public/templates/footer') ?>