<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Update Scrap Request',
	'link' => 'scrap_view'
)) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'scrap')) ?>
<?php if(!empty($scrap_details['0']->scrap_details)){
	$arr = (explode(",",$scrap_details['0']->scrap_details));
}?>
<?php //print_r($scrap_details['0']); ?>
<div class="update">
	<div class="container">
		<div class="col-md-12 text-center update-info hide">
		    <div class="alert alert_msg alert-info" style="padding: 10px;margin-top: 20px;">		    
		        <p><strong>Success!</strong> Your Request Is Updated Successfully.</p>
		    </div>    
		</div>
		<div class="col-md-12 text-center" style="margin-bottom: 30px;">
			<h2>Update Your Scrap Request <label class="control-label" style="font-size: 22px;">( Request Id : <?php echo $scrap_id;?></label>, <?php if($scrap_details['0']->request_type == 'Periodic'): ?>
					<label style="font-size: 22px; font-weight: 700"><?php echo 'Periodic, Prefered pickup date '.date("d-m-Y", strtotime($scrap_details['0']->prefered_pickup_date)).' )';?></label>
					<?php else: ?>
					<label style="font-size: 22px;font-weight: 700"><?php echo 'Non-Periodic, Prefered pickup date '.date("d-m-Y", strtotime($scrap_details['0']->prefered_pickup_date)).' )';?></label>
				<?php endif ?>
			</h2>
		</div>
		<div class="col-md-12">
			<div class="col-md-4">
				<div class="form-group customer-presence-day">	 
					<label class="control-label customer-presence-day-heading" style="margin-bottom: 20px;">1. Your prefer day of picking scrap</label>
					<div class="customer-presence-day two-options">
						<!-- <?php// if($scrap_details['0']->request_type == 'Periodic'):?> -->
							<!-- <div class="customer-presence-day two-options">
								<label class="customer-presence-day-text sat"><input type="radio" name="prefered_pickup_day" class="prefered_pickup_day" id="prefered_pickup_day" value="Saturday" <?php// if($scrap_details['0']->prefered_pickup_day == 'Saturday')echo 'checked'; ?>><span>Saturday</span></label>
								<label class="customer-presence-day-text sun"><input type="radio" name="prefered_pickup_day" class="prefered_pickup_day" id="prefered_pickup_day" value="Sunday" <?php// if($scrap_details['0']->prefered_pickup_day == 'Sunday')echo 'checked'; ?>><span>Sunday</span></label>
							</div> -->
						<?php //else: ?>
							<div class="datetimepicker_parent">
								<div class='input-group date' id='datetimepicker'>
					              	<input type='text' name="to_date" id="prefered_pickup_day" class="form-control prefered_pickup_day hide" placeholder="Select Pick Up Date"/>
					            </div>	
					        </div>    	
							<!-- <div class='input-group date' id='datetimepicker'>
				              	<input type='text' name="to_date" id="prefered_pickup_day" class="form-control prefered_pickup_day" placeholder="Select Pick Up Date"/>
				              	<span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				              	</span>
				            </div> -->
						<?php// endif ?>	
					</div>
					<div class="text-danger"></div>
				</div>
			</div>
			<?php if($scrap_details['0']->request_type == 'Non-Periodic'):?>
			<div class="col-md-4">
				<div class="form-group form-group-scrap-type">
					<label class="control-label"  style="font-size: 20px; display: block; margin-bottom: 20px;">2. Scrap description </label>
					<div class="scrap_comment_div text-center">
						<textarea class="form-control" rows="5" id="scrap_comment"><?php echo $scrap_details['0']->scrap_comment; ?></textarea>
					</div>
				</div>
			</div>
			<?php endif ?>	
			<?php if($scrap_details['0']->request_type == 'Periodic'):?>
			<div class="col-md-4">
				<div class="form-group form-group-customer-presence edit-scrap-type">
					<label class="control-label"  style="font-size: 20px; display: block; margin-bottom: 20px;">2. Preferred Pickup Frequency </label>
					<div class="pick_up_frequency text-center">
						<?php echo "<select class='form-control''>".$pickup_frequency."</select>"; ?>
					</div>
				</div>
			</div>
			<?php endif?>
			<?php if($scrap_details['0']->request_type == 'Non-Periodic'):?>
			<div class="col-md-4">
				<div class="form-group form-group-scrap-type">
					<label class="control-label"  style="font-size: 20px; display: block; margin-bottom: 20px;">3. Are you ok with keeping scrap outside door on pickup eve? </label>
					<div>
						<label class="request-type-text present-in-pickup" style="font-size: 18px;font-weight: 500;"><input type="radio" name="customer_presence" class="customer_presence change_customer_presence2" id="customer_presence" value="Yes" <?php if($scrap_details['0']->customer_presence == 'Yes')echo 'checked'; ?>><span>Yes.</span></label>
						<label class="request-type-text not-present-in-pickup" style="font-size: 18px; font-weight: 500; margin-right:20px;"><input type="radio" name="customer_presence" class="customer_presence change_customer_presence1" id="customer_presence" value="No" <?php if($scrap_details['0']->customer_presence == 'No')echo 'checked'; ?>><span>No.</span></label>
					</div>
				</div>
			</div><?php endif ?>	
			<?php if($scrap_details['0']->request_type == 'Periodic'):?>
			<div class="col-md-4">
				<div class="form-group form-group-customer-presence edit-scrap-type">
					<label class="control-label"  style="font-size: 20px; display: block; margin-bottom: 20px;">3. Are you ok with keeping scrap outside door on pickup eve?</label>
					<div>
						<label class="request-type-text present-in-pickup" style="font-size: 18px;font-weight: 500;"><input type="radio" name="customer_presence" class="customer_presence change_customer_presence2" id="customer_presence" value="Yes" <?php if($scrap_details['0']->customer_presence == 'Yes')echo 'checked'; ?>><span>Yes.</span></label>
						<label class="request-type-text not-present-in-pickup" style="font-size: 18px; font-weight: 500;"><input type="radio" name="customer_presence" class="customer_presence change_customer_presence1" id="customer_presence" value="No" <?php if($scrap_details['0']->customer_presence == 'No')echo 'checked'; ?>><span>No.</span></label>
					</div>
				</div>
			</div><?php endif?>
		</div>
		<div class="col-md-12 form-group text-center" id="otp">
			<input type="submit" name="submit_scrap_update" id="submit_scrap_update" value="Update Request" class="btn btn-lg btn-primary submit_scrap_update"  />
			<div class="text-danger"></div>
		</div>
	</div>
</div>

<?php $this->load->view('public/templates/footer') ?>
<script type="text/javascript">
 jQuery(document).ready(function(){
 	var date = new Date();
	date.setDate(date.getDate());
	jQuery(function(){
		jQuery('#datetimepicker').datetimepicker({
			defaultDate: '<?php echo $scrap_details['0']->prefered_pickup_date; ?>',
            format: "YYYY-MM-DD",
            minDate: new Date().setHours(0,0,0,0),
            daysOfWeekDisabled: [<?php foreach($pickup_config_days as $disabled_days){
            	echo $disabled_days->sunday==1?'0,':'';
            	echo $disabled_days->monday==1?'1,':'';
            	echo $disabled_days->tuesday==1?'2,':'';
            	echo $disabled_days->wednesday==1?'3,':'';
            	echo $disabled_days->thursday==1?'4,':'';
            	echo $disabled_days->friday==1?'5,':'';
            	echo $disabled_days->saturday==1?'6':'';
            	}?>],
            disabledDates: [
            		<?php foreach($pickup_config_date as $holiday_details){
            			echo 'moment("'.$holiday_details->holiday_date.'"),';	
                    }?>
                ],
            keepInvalid:true,
            inline: true   
		});
		jQuery('.today').removeClass('active');
	});
    jQuery(".change_customer_presence1").click(function(){
 	    jQuery(".change_customer_presence2").attr('checked', false);
 	    jQuery(this).attr('checked', true);
    });
    jQuery(".update-customer-presence-day1").click(function(){
 	    jQuery(".update-customer-presence-day2").attr('checked', false);
 	    jQuery(this).attr('checked', true);
    }); 
    jQuery(".update-customer-presence-day2").click(function(){
 	    jQuery(".update-customer-presence-day1").attr('checked', false);
 	    jQuery(this).attr('checked', true);
    }); 
    jQuery(".change_customer_presence2").click(function(){
   	    jQuery(".change_customer_presence1").attr('checked', false);
   	    jQuery(this).attr('checked', true);
   	    jQuery(".form-group-customer-presence").removeClass('hide');
        jQuery(".form-group-customer-presence").show();
    }); 
    jQuery(".submit_scrap_update").click(function(){
    	var scrap_id          = "<?php echo $scrap_details['0']->id;?>";
   	    var user_id           = <?php echo $user->id?>;
   	    var customer_presence = jQuery('input[name=customer_presence]:checked').val();
   	    var interval          = 0;
   	    var request_type      = "<?php echo $scrap_details['0']->request_type;?>";
   	    var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
	    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
	    var scrap_comment     = '';
   	    var cartoon_boxes     = '';
   	    var paper             = '';
   	    var error             = false;
   	    if(prefered_pickup_date == '' || prefered_pickup_date == undefined){
   	    	prefered_pickup_date ='<?php echo $scrap_details['0']->prefered_pickup_date; ?>';
   	    	prefered_pickup_day = '<?php echo $scrap_details['0']->prefered_pickup_day; ?>';
   	    	if(prefered_pickup_date == '' || prefered_pickup_date == undefined){
		    	var txt = '<p>Please Select Your Prefered day for Picking Scrap.</p>';
	        	jQuery('.customer-presence-day .text-danger').html(txt);
	        	jQuery('.customer-presence-day .text-danger p').css('color','red');
		        error = true;
		    }    
	    }
	    if (customer_presence == "" || customer_presence == undefined) {
        	var txt = '<p>Please Select Your Presence Type During Sell.</p>';
    		jQuery('.text-danger-scrap-type').html(txt);
    		jQuery('.text-danger-scrap-type p').css('color','red');
        	error = true; 
        }
   	    if(request_type=='Periodic'){
        	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
        }
        else{
        	scrap_comment = jQuery('#scrap_comment').val();
        } 
        if (error){
        	return false;
        }
        jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('update_request'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',interval:interval,customer_presence:customer_presence,request_type:request_type,user_id:user_id,cartoon_boxes:cartoon_boxes,scrap_id:scrap_id,paper:paper,prefered_pickup_day:prefered_pickup_day,prefered_pickup_date:prefered_pickup_date,scrap_comment:scrap_comment},
	        'success' : function(data) { 
	            var tag = '<strong>Success!</strong> Your request is updated successfully. Your prefered day for scrap picked up : '+prefered_pickup_day+' and customer presence during scrap picked up : '+customer_presence;
	            jQuery('.alert_msg').html(tag);
	            jQuery('.update-info').removeClass('hide');
	        },
	        'error' : function(request,error){
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
    }); 
    jQuery('#prefered_pickup_day').on('focus', function(){
		var txt = '';
		jQuery('.customer-presence-day .text-danger').html(txt);
	}); 
});
</script>
<style type="text/css">
	.update div{padding: 0;}
</style>