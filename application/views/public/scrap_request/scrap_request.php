<?php $this->load->view('public/templates/header', array(
	'title' => 'Best place to Sell Scrap - GreenREE',
	'title_description' => 'GreenREE provides you the facility to pickup scrap from your doorstep at your convenience. Paper Scrap + more..',
	'link' => 'scrap_view',
	'menu_active' =>'sell_now'
)); ?>



<?php if(empty($users_address)): ?>

<div class="request-address-box">
	<div class="container">
		<div class="col-md-12 request_details">
			<h2 class="text-center request_details_header">Enter Your Address</h2>
			<div class="col-md-6 first-half">
				<div class="col-md-12">
					<div class="form-group form-group-name">
	<?php if($user_details['0']->first_name || $user_details['0']->last_name): ?>
						<input type="text" name="name" id="name" autocomplete="__away" placeholder="Name*" value="<?php echo $user_details['0']->first_name.' '.$user_details['0']->last_name; ?>" class="form-control input-lg name">
	<?php else: ?>
						<input type="text" name="name" id="name" autocomplete="__away" placeholder="Name*" class="form-control input-lg name">
	<?php endif ?>
						<div class="text-danger text-danger-name"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 second-half">
				<div class="col-md-12">
					<div class="form-group form-group-email">
	<?php if(strpos($user_details['0']->email, '@') !== false): ?>
						<input type="text" name="email" id="email" placeholder="Email*" value="<?php echo $user_details['0']->email; ?>" class="form-control input-lg email" readonly>
	<?php else:?>
						<input type="text" name="email" id="email" placeholder="Email*" class="form-control input-lg email">
	<?php endif ?>
						<div class="text-danger text-danger-email"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 first-half">
				<div class="col-md-12">
					<div class="form-group form-group-country">
						<select name="country" id="country" class="form-control  input-lg country">
	            		    <option value="0"> - Country - </option>
	                		<?php if(sizeof($countries) == 1){ ?>
	        			    <option value="<?php echo $countries[0]['loc_name'];?>" data-val="<?php echo $countries[0]['loc_id'];?>" selected><?php echo $countries[0]['loc_name'];?></option>    
	        			    <?php }else{ foreach($countries as $country) { ?>
	        				<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>">
	        					<?php echo $country['loc_name'];?>
	        				</option>
	        			    <?php } }?>
	        		    </select>
						<div class="text-danger text-danger-country"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 second-half">
				<div class="col-md-12">
					<div class="form-group form-group-state">
						<select name="state" id="state" class="form-control input-lg state">
	        				<option value="0" selected="selected"> - State - </option>
	        				<?php if(sizeof($states) == 1){ ?>
	        			    <option value="<?php echo $states[0]['loc_name'];?>" data-val="<?php echo $states[0]['loc_id'];?>" selected><?php echo $states[0]['loc_name'];?></option>
	        			    <?php }else{ foreach($states as $state) { ?>
	        				<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>">
	        					<?php echo $state['loc_name'];?>
	        				</option>
	        			    <?php } }?>
	        			</select>
						<div class="text-danger text-danger-state state"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 first-half">
				<div class="col-md-12">
					<div class="form-group form-group-city">
						<input list="city_list" name="city" id="city" class="form-control input-lg city" autocomplete="__away" value="<?php if($user_address->city) echo $user_address->city;elseif($user_address->country == '' && sizeof($cities) == 1)echo $cities[0]['loc_name'];?>" placeholder="Select or Enter your City">
						<datalist id="city_list">
	        				<?php if(sizeof($cities) == 1){ ?>
	        				    <option value="<?php echo $cities[0]['loc_name'];?>" data-val="<?php echo $cities[0]['loc_id'];?>"    selected><?php echo $cities[0]['loc_name'];?></option>
	        				<?php }else{ foreach($cities as $city) { ?>
	        				<option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>">
	        					<?php echo $city['loc_name'];?>
	        				</option>
	        			    <?php } } ?>
						</datalist>
					    <div class="text-danger text-danger-city"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 second-half">
				<div class="col-md-12">
					<div class="form-group form-group-locality">
						<input list="locality_list" name="locality" id="locality" class="form-control input-lg locality" autocomplete="__away" value="<?php if($user_address->locality) echo $user_address->locality;elseif($user_address->country == '' && sizeof($localities) == 1)echo $localities[0]['loc_name'];?>" placeholder="Select or Enter your Locality">
						<datalist id="locality_list">
	        			    <?php if(sizeof($localities) == 1){ ?>
	        					   <option value="<?php echo $localities[0]['loc_name'];?>" data-val="<?php echo $localities[0]['loc_id'];?>" selected><?php echo $localities[0]['loc_name'];?>
	        					   </option>
	        				<?php }else{ foreach($localities as $locality) { ?>
	        				    <option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>">
	        					<?php echo $locality['loc_name'];?>
	        					</option>
	        			    <?php } } ?>
	                    </datalist>
						<div class="text-danger text-danger-locality"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 first-half">	
				<div class="col-md-12">
					<div class="form-group form-group-apartment">
						<input list="apartment_list" name="apartment" id="apartment" class="form-control input-lg apartment" autocomplete="__away" value="<?php if($user_address->apartment_name) echo $user_address->apartment_name;elseif($user_address->country == '' && sizeof($apartments) == 1)echo $apartments[0]['loc_name'];?>" placeholder="Select or Enter your Apartment">
						<datalist id="apartment_list"  data-dropup-auto="false">
	        			    <?php if(sizeof($apartments) == 1){ ?>
	        				    <option value="<?php echo $apartments[0]['loc_name'];?>" data-val="<?php echo $apartments[0]['loc_id'];?>" selected><?php echo $apartments[0]['loc_name'];?></option>
	        				    <option value="Others" data-val="Others">
								Others
								</option>
	        				<?php }else{ foreach($apartments as $apartment) { ?>
	        				<option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>">
	        					<?php echo $apartment['loc_name'];?>
	        				</option>
	        			    <?php } } ?>	
	                    </datalist>
						<div class="text-danger text-danger-apartment"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 second-half">
				<div class="col-md-12">
					<div class="form-group form-group-flat_no">
						<input type="text" class="form-control input-lg" id="flat_no" placeholder="Flat No.*" name="flat_no" value="" />
						<div class="text-danger text-danger-flat_no"></div>
					</div>
				</div>
			</div>
			<div class="col-md-12 submit_request_id">
				<div class="col-md-12 form-group" id="otp">
					<input type="button" name="submit_address" id="submit_address" value="Proceed" class="btn btn-lg btn-success submit_address"/>
					<div class="text-danger"></div>
				</div>
				<!-- <button class="btn btn-success hide address-next-btn pull-right" value="Next" style="background: #5cb85c url('<?php// echo base_url()?>assets/images/arrow-next.png')no-repeat  37px 4px;padding-right: 25px">Next</button> -->
			</div>
		</div>
    </div>
    <div class="container">
        <div class="request-info-box">
    		<span class="notification_icon"></span> 
    	    <div class="alert alert_msg1">
    	    </div> 
        </div>
    </div>
</div>
<?php endif ?>

<?php if(!empty($users_address)){ ?>
    <div class="container">
        <div class="request-info-box hide">
    		<span class="notification_icon"></span> 
    	    <div class="alert alert_msg1">
    	    </div> 
        </div>
    </div>
<?php } ?>
<div class="request-box <?php echo empty($users_address)?'hide':'' ?>">
	<div class="container">
		<div class="col-md-12 request_details1">
			<h2 class="text-center request_details1_header">Enter Your Request Details</h2>	
		</div>
		<div class="col-md-12"> 
			<div class="col-md-4">
				<div class="form-group form-group-request-type">
						<label class="control-label request-type-heading">1. How  would you like to sell your scrap? <label class="how_it_works">
							<a href="<?php echo base_url('about_us');?>">(How It Works?)</a>
						</label></label>
						<div class="sell-scrap-type two-options">
							<label class="request-type-text req-type-1" data-placement="right"><input type="radio" name="request_type" title="You can chose the day and frequency of pickup of your choice and we will remind you a day before through sms." data-toggle="tooltip" class="request_type change_customer_presence2" id="request_type" value="Periodic"><span>Periodically</span></label>
							<label  class="request-type-text req-type-2" data-placement="right"><input type="radio" name="request_type" class="request_type change_customer_presence1" title="Chose the date from calendar and we will pick it up on that day." data-toggle="tooltip" id="request_type" value="Non-Periodic" ><span>One Time Sell</span></label>
						</div>
					<div class="text-danger text-danger-request-type"></div>
					<div class="text-request-id"></div>
					<!--<div class="pre_prefered_pickup_date"></div> -->
					<div class="pick_up_frequency text-center"></div>
					<div class="scrap_comment_div text-center"></div>
					<div class="periodic_scrap_comment text-center"></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group customer-presence-day">	 
					<label class="control-label customer-presence-day-heading">2. Your prefer day of picking scrap</label>
					<div class="customer-presence-day two-options">
						<div class="datetimepicker_parent">
							<div class='input-group date' id='datetimepicker'>
				              	<input type='text' name="to_date" id="prefered_pickup_day" class="form-control prefered_pickup_day hide" placeholder="Select Pick Up Date"/>
				            </div>
						</div>
					</div>
					<div class="text-danger text-danger-customer-presence-day"></div>
				</div>
			</div>
			
			<div class="col-md-4 customer-presence-section">
				<div class="form-group form-group-customer-presence">	 
					<label class="control-label request-type-heading">3. Are you ok with keeping scrap outside door on pickup eve? <label class="how_it_works">
					</label></label>
					<div class="sell-scrap-type two-options">
						<label class="request-type-text present-in-pickup"><input type="radio" title="No need to be present during sell. We will pick up your scrap." data-toggle="tooltip" data-placement="bottom" name="customer_presence" class="customer_presence change_customer_presence" id="customer_presence" value="Yes" checked><span>Yes</span></label>
						<label class="request-type-text not-present-in-pickup"><input type="radio" name="customer_presence" title="Not efficient for us and time-consuming for you as well but still if you have some reason to be personally present during sell, we can work out convenient time for you." data-toggle="tooltip" data-placement="bottom" class="customer_presence change_customer_presence" id="customer_presence" value="No"><span>No</span></label>
					</div>
					<div class="text-danger text-danger-customer-presence"></div>
				</div>
			</div>
		<?php if(!empty($users_address)):  ?>
	        <div class="col-md-12">
				<div class="col-md-12 form-group" id="scrap_update">
					<input type="submit" name="submit_scrap_request" id="submit_scrap_request" value="Submit Request" class="btn btn-lg btn-primary submit_scrap_request"  />
					<div class="text-danger"></div>
				</div>
			</div> 
		<?php endif ?>
		</div>
		<?php if(empty($users_address)):  ?>
			<input type="submit" name="submit_request" id="submit_request" value="Submit Request" class="btn btn-lg btn-primary submit_request"/>
			<input type="button" class="btn btn-success address-prev-btn pull-left" value="Prev" style="margin-bottom: 40px;background: #5cb85c url('<?php echo base_url()?>assets/images/arrow-prev.png')no-repeat  3px 4px;padding-left: 30px"> 
		<?php endif ?>
	</div>
</div>
 
<!--modal part start -->
<div id="confirm_scrap_request_box" class="modal">
  <div class="modal-content scrap_admin_message">
    <span class="close">&times;</span>
    <div>
    	<br>
        <h3>Confirm the Details : </h3>
        <div class="scrap_request_msg"></div>
        <button class="btn btn-md btn-success modify">Submit</button>
        <button class="btn btn-md btn-danger cancel">Cancel</button>
    </div>
  </div>
</div><?php print_r($sunday) ?>
<!--modal part end -->
<?php $this->load->view('public/templates/footer') ?>
<script type="text/javascript">
    var modal = document.getElementById('confirm_scrap_request_box');
    function validateEmail(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(String(email).toLowerCase());
    }
	jQuery(document).ready(function(){ 
		//calender part start
		var date = new Date();
		date.setDate(date.getDate());
		<?php if(!empty($users_address)):?>
		jQuery(function() {
			jQuery('#datetimepicker').datetimepicker({
				inline: true,
                format: "YYYY-MM-DD",
                minDate: new Date().setHours(0,0,0,0),
                daysOfWeekDisabled: [<?php foreach($pickup_config_days as $disabled_days ){
                	echo $disabled_days->sunday==1?'0,':'';
                	echo $disabled_days->monday==1?'1,':'';
                	echo $disabled_days->tuesday==1?'2,':'';
                	echo $disabled_days->wednesday==1?'3,':'';
                	echo $disabled_days->thursday==1?'4,':'';
                	echo $disabled_days->friday==1?'5,':'';
                	echo $disabled_days->saturday==1?'6':'';
                }
                	?>],
                keepInvalid:true,	
                disabledDates: [
                		<?php foreach($pickup_config_date as $holiday_details){
                			echo 'moment("'.$holiday_details->holiday_date.'"),';	
                        }?>
                    ],
			});
			jQuery('.today').removeClass('active');
  		});
		<?php endif ?>
		function loadCalndar(config_day,disabled_dates){
			var disabled_days = new Array();
			if(config_day['sunday'] == 1)
				disabled_days.push(0);
			if(config_day['monday'] == 1)
				disabled_days.push(1);
			if(config_day['tuesday'] == 1)
				disabled_days.push(2);
			if(config_day['wednesday'] == 1)
				disabled_days.push(3);
			if(config_day['thursday'] == 1)
				disabled_days.push(4);
			if(config_day['friday'] == 1)
				disabled_days.push(5);
			if(config_day['saturday'] == 1)
				disabled_days.push(6);
			jQuery('#datetimepicker').datetimepicker({
				inline: true,
                format: "YYYY-MM-DD",
                minDate: new Date().setHours(0,0,0,0),
                daysOfWeekDisabled: disabled_days,
                keepInvalid:true,	
                disabledDates: disabled_dates,
			});
			jQuery('.today').removeClass('active');
		}

		jQuery('body').on('click', '.submit_address,.submit_address1', function(){
			var serviceAddressCheck = jQuery('.submit_address1').text();
        	        var user_id = "<?php echo $user->id;?>";
			var country   = jQuery('#country').val();
			var country_id = jQuery('option:selected', this).attr('data-val');
			var state     = jQuery('#state').val();
			var state_id  = jQuery('option:selected', this).attr('data-val');
			var city      = jQuery('#city').val();
			var city_id   = jQuery('#city_list').find("[value='" + city + "']").attr('data-val');
			var locality  = jQuery('#locality').val();
			var locality_id = jQuery('#locality_list').find("[value='" + locality + "']").attr('data-val');
			var apartment   = jQuery('#apartment').val();
			var apartment_id = jQuery('#apartment_list').find("[value='" + apartment + "']").attr('data-val');
			var flat_no   = jQuery('#flat_no').val();
			var name      = jQuery('#name').val();
			var email     = jQuery('#email').val();
			var error = false;
			if(name == ''){
				jQuery('.text-danger-name').text('Please Enter Your Name');
				error = true;
			}
			if(!isNaN(name)){
			    jQuery('.text-danger-name').text('Please Enter Only Characters');
				error = true;
			}
			if(name.length < 3){
			    jQuery('.text-danger-name').text('Please Enter Atleast Three Characters');
				error = true;
			}
			if(email == ''){
				jQuery('.text-danger-email').text('Please Enter Your Email');
				error = true;
			}
			if(!validateEmail(email)){
			 	jQuery('.text-danger-email').text('Please Enter Valid Email');
				error = true;
			}
			if(country == '0'){
			    jQuery('.text-danger-country').text('Please Select Country');
			    error = true;
			}
			if(state == '0'){
			    jQuery('.text-danger-state').text('Please Select State');
			    error = true;
			}
			if(city == '0' || city == '' || city == undefined){
			    jQuery('.text-danger-city').text('Please Select City');
			    error = true;
			}
			if(locality == '0' || locality == '' || locality == undefined){
			    jQuery('.text-danger-locality').text('Please Select Locality');
			    error = true;
			}
			if(apartment == '0' || apartment == '' || apartment == undefined){
			    jQuery('.text-danger-apartment').text('Please Select Apartment');
				error = true;
			}
			if(flat_no == ''){
			    jQuery('.text-danger-flat_no').text('Please Enter Flat Number');
				error = true;
			}
			if(error){
			    var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
			    return false;
			}
			else{
                               if(serviceAddressCheck=="Proceed"){
					submitAddress(country_id, state_id, city_id, locality_id, apartment_id, email);
				}
				else{
					// for checking users address is available or not in our service area part start
					var newUser = 1;
				    jQuery.ajax({
						type : 'POST',
					    url : "<?php echo base_url('serviceAddressCheck'); ?>",
					    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',user_id:user_id,newUser:newUser,country:country,state:state,city:city,locality:locality,apartment:apartment},
					    'success' : function(data) {  
					        if (data<6) {
							    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p class="alert alert-info"> We are not yet operational in your locality/apartment, but you can still proceed placing the scrap request and admin will review and may accept.</p></div><button class="btn btn-md btn-success submit_address1">Proceed</button><a class="btn btn-md btn-danger cancel" href="<?php echo base_url() ?>">Cancel and go to Home Page</a></div>';
					        	jQuery(".scrap_admin_message").html(tag);
					        	jQuery("#confirm_scrap_request_box").css('display','block');
					        }
					        else{
							    submitAddress(country_id, state_id, city_id, locality_id, apartment_id, email);
					        }
					    },
					    'error' : function(request,error)
					    {
					        alert("Request: "+JSON.stringify(request));
					    }
					});
					// for checking users address is available or not in our service area part end
				}
			}
		});
               
                 function submitAddress(country_id, state_id, city_id, locality_id, apartment_id, email){			
				jQuery.ajax({
		        	type: 'POST',
		            url: '<?php echo base_url('load_calnder'); ?>',
		            data: { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',country_id:country_id, state_id:state_id, city_id:city_id,locality_id:locality_id, apartment_id:apartment_id,email:email},
		            success: function(response){
		            	var calender_config = jQuery.parseJSON(response); 
		            	if(calender_config.email_checked==1){
		            		jQuery('.text-danger-email').text('Email address already exists.');
		            	} 
		            	else{
			            	var config_days = calender_config.pickup_config_days;
			            	var config_date = calender_config.pickup_config_date;
			            	var config_day = new Array();
			            	var disabled_dates = new Array();
			            	for(var i=0; i<config_days.length; i++) {
							    if(config_days[i]['monday'] == 1)
							    	config_day['monday'] = 1;
							    if(config_days[i]['tuesday'] == 1)
							    	config_day['tuesday'] = 1;
							    if(config_days[i]['wednesday'] == 1)
							    	config_day['wednesday'] = 1;
							    if(config_days[i]['thursday'] == 1)
							    	config_day['thursday'] = 1;
							    if(config_days[i]['friday'] == 1)
							    	config_day['friday'] = 1;
							    if(config_days[i]['saturday'] == 1)
							    	config_day['saturday'] = 1;
							    if(config_days[i]['sunday'] == 1)
							    	config_day['sunday'] = 1;
							}
			            	for(var i=0;i<config_date.length;i++)
			            		disabled_dates.push(config_date[i]['holiday_date']);
			            	loadCalndar(config_day,disabled_dates);
			            	jQuery('.request-address-box').addClass('hide');
			            	jQuery('.request-box').removeClass('hide');
			            }
		            },
		            'error' : function(request,error){
				        alert("Request: "+JSON.stringify(request));
				    }
		        });
		}
		//Calender part is end

		jQuery('body').on('click', '.address-prev-btn', function(){
			jQuery('.request-box').addClass('hide');
			jQuery('.request-address-box').removeClass('hide');
			jQuery('.address-next-btn').removeClass('hide');
		});

		jQuery('body').on('click', '.address-next-btn', function(){

			jQuery('.request-box').removeClass('hide');
			jQuery('.address-prev-btn').removeClass('hide');
			jQuery('.request-address-box').addClass('hide');
			jQuery('.address-next-btn').addClass('hide');
		});

		//for address part start
		var country_value = jQuery('#country option:selected', this).val();
		var state_value = jQuery('#state option:selected', this).val();
		var city_value = jQuery('#city').val();
		var locality_value = jQuery('#locality', this).val();
		if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
        	jQuery('#state').attr("disabled", true);
        	jQuery('#city').attr("disabled", true);
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(state_value=='' || state_value=='- State -' || state_value=='0'){
        	jQuery('#city').attr("disabled", true);
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(city_value=='' || city_value=='- city -'){
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(locality_value=='' || locality_value=='- locality -'){
        	jQuery('#apartment').attr("disabled", true);
        }
		//for address part end
    	jQuery("body").on('click','.close, .cancel,.submit_address1',function(event){
    		modal.style.display  = "none";
    	});
    	jQuery("body").on('click','.close_scrap_notification,.close_scrap_success',function(event){
    		modal.style.display  = "none";
          	location.reload();
    	});
    	jQuery("body").on('click','.update_close,.scrap_popup_close',function(event){
    		modal.style.display  = "none";
          	location.reload();
    	});
    	window.onclick = function(event) {
      		if (event.target == modal) {
        		modal.style.display = "none";
      		}
    	}
        jQuery('body').on('click', '.submit_scrap_update', function() {
        	    var user_id  = <?php echo $user->id?>;
                    var scrap_id = jQuery("#submit_scrap_update").attr("name");
		    var customer_presence = jQuery('input[name=customer_presence]:checked').val();
		    var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
		    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
		    var request_type = jQuery('input[name=request_type]:checked').val();
		    var scrap_comment = '';
		    var interval      = 0;
		    var cartoon_boxes = '';
		    var paper         = '';
		    var error         = false;
		    if(request_type=='Periodic'){
		    	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
		    }
		    else{
		    	scrap_comment = jQuery("#scrap_comment").val();
		    }
        	if (customer_presence == "select" || customer_presence == ""   ||customer_presence == undefined) {
	        	var txt = '<p>Please select an option</p>';
            	jQuery('.text-danger-customer-presence').html(txt);
            	jQuery('.text-danger-customer-presence').css('color','red');
	        	error = true;
	        }
	        if(prefered_pickup_date == '' || prefered_pickup_date == undefined){
	   	    	if(request_type=='Periodic'){
	   	    		prefered_pickup_date = '<?php echo $periodic['0']->prefered_pickup_date?>';
	   	    		prefered_pickup_day = '<?php echo $periodic['0']->prefered_pickup_day?>';
			    }    
			    else{
			    	prefered_pickup_date = '<?php echo $non_periodic['0']->prefered_pickup_date?>';
	   	    		prefered_pickup_day = '<?php echo $non_periodic['0']->prefered_pickup_day?>';
			    }
		    }
		    if (request_type=="" || request_type== undefined) {
            	var txt = '<p class="alert-danger">Please select scrap request type</p>';
            	var txt1 = '<p>Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-request-type').html(txt);
            	jQuery('.text-danger-request-type').css('color','red');
            	error = true;
            }
		    if (error){
		    	return false;
		    }
		    // for checking users address is available or not in our service area part start
			var newUser = 0;
		    jQuery.ajax({
				type : 'POST',
			    url : "<?php echo base_url('serviceAddressCheck'); ?>",
			    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',user_id:user_id, newUser:newUser},
			    'success' : function(data) {  
			        if (data<6) {
					    if(request_type == 'Non-Periodic')
			    			request_type ='One Time Sell';
					    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p>You like to sell your scrap : '+request_type+'</p><p>Your prefer day of picking scrap : '+prefered_pickup_day+'</p><p>You are ok with keeping scrap outside door on pickup eve : '+customer_presence+'</p><p class="alert alert-info"> We are not yet operational in your locality/apartment, but you can still proceed placing the scrap request and admin will review and may accept.</p></div><button class="btn btn-md btn-success submit_scrap_update1">Submit</button><a class="btn btn-md btn-danger cancel" href="<?php echo base_url() ?>">Cancel and go to Home Page</a></div>';
			        	jQuery(".scrap_admin_message").html(tag);
			        	jQuery("#confirm_scrap_request_box").css('display','block');
			        }
			        else{
					    if(request_type == 'Non-Periodic')
			    			request_type ='One Time Sell';
					    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p>You like to sell your scrap : '+request_type+'</p><p>Your prefer day of picking scrap : '+prefered_pickup_day+'</p><p>You are ok with keeping scrap outside door on pickup eve : '+customer_presence+'</p></div><button class="btn btn-md btn-success submit_scrap_update1">Submit</button><button class="btn btn-md btn-danger cancel">Cancel</button></div>';
			        	jQuery(".scrap_admin_message").html(tag);
			        	jQuery("#confirm_scrap_request_box").css('display','block');
			        }
			    },
			    'error' : function(request,error)
			    {
			        alert("Request: "+JSON.stringify(request));
			    }
			});
			// for checking users address is available or not in our service area part end
        });
		jQuery(".change_customer_presence1").click(function(){
	 	    jQuery(".change_customer_presence2").attr('checked', false);
	 	    jQuery(this).attr('checked', true);
            jQuery(".form-group-customer-presence").removeClass('hide');
            jQuery(".form-group-customer-presence").show();
            jQuery(".form-group-interval").show();
            jQuery(".form-group-scrap-type").addClass('hide');
            var txt = '<p></p>';
			jQuery('.text-danger-request-type,.text-danger-customer-presence-day,.text-danger-customer-presence').html(txt);
			jQuery('.text-danger-request-type,.text-danger-customer-presence-day,.text-danger-customer-presence').css('color','#ffffff');
            jQuery('.text-request-id').html('');
            jQuery('.text-danger-request-type,.text-danger-customer-presence-day,.text-danger-customer-presence').css('color','#ffffff');
            jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
            jQuery(".pick_up_frequency").html("");
            jQuery(".periodic_scrap_comment").html("");
            jQuery(".pre_prefered_pickup_date").html("");
            var pre_prefered_pickup_date = '<?php echo date('d-m-Y',strtotime($non_periodic['0']->prefered_pickup_date))?>';
            var nonperiodic_scrap_id ='<?php foreach($non_periodic as $nonperiodic_id){echo $nonperiodic_id->id;} ?>';
            // date = "<?php //foreach($non_periodic as $nonperiodic_id){echo $nonperiodic_id->prefered_pickup_date;}  ?>";
            var non_periodic_scrap_comment ='<?php foreach($non_periodic as $presence){echo $presence->scrap_comment;} ?>';
            var non_periodic_customer_presence ='<?php foreach($non_periodic as $presence){echo $presence->customer_presence;} ?>';
            var non_periodic_customer_presence_day ='<?php foreach($non_periodic as $presence){echo $presence->prefered_pickup_day;} ?>';
            var non_periodic_customer_presence_date_next ='<?php foreach($non_periodic as $presence){echo $presence->prefered_pickup_date_next;} ?>';
            var scrap_comment = '<label>Good to know - What all you have in the scrap</label><textarea class="form-control" rows="5" id="scrap_comment" placeholder="Ex: Laptop, e waste, Paper, Carton box, Books, Notebooks, Clothes .."></textarea>';
            jQuery('.scrap_comment_div').html(scrap_comment);
            if (nonperiodic_scrap_id != '') {
            	var btn = '<input type="submit" name="<?php foreach($non_periodic as $nonperiodic_id){echo $nonperiodic_id->id;} ?>" id="submit_scrap_update" value="Update Request" class="btn btn-lg btn-primary submit_scrap_update"  />';
            	jQuery('.text-request-id').html('Scrap request id : '+nonperiodic_scrap_id);
            	jQuery('.pre_prefered_pickup_date').html('Preferred pickup date : '+pre_prefered_pickup_date);
            	jQuery('#scrap_comment').val(non_periodic_scrap_comment);
            	jQuery('#datetimepicker').data("DateTimePicker").date(non_periodic_customer_presence_date_next);
            	if(non_periodic_customer_presence == 'Yes'){
            		jQuery("input[name=customer_presence][value='Yes']").prop("checked",true);
            		jQuery("input[name=customer_presence][value='No']").prop("checked",false);
            		if(non_periodic_customer_presence_day == 'Sunday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",true);
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            	}
	            	else if(non_periodic_customer_presence_day == 'Saturday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",true);
	            	}
	            	else{
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
            	}else{
            		jQuery("input[name=customer_presence][value='Yes']").prop("checked",false);
            		jQuery("input[name=customer_presence][value='No']").prop("checked",true);
            		if(non_periodic_customer_presence_day == 'Sunday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",true);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
	            	else if(non_periodic_customer_presence_day == 'Saturday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",true);
	            	}
	            	else{
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
            	}
            	jQuery("#scrap_update").html(btn);
            	jQuery("#submit_scrap_request").css('background','#428bca');
            }
            else{
            	var btn = '<input type="submit" name="submit_scrap_request" id="submit_scrap_request" value="Submit Request" class="btn btn-lg btn-primary submit_scrap_request"  />';
            	jQuery("input[name=customer_presence][value='Yes']").prop("checked",true);
            	jQuery("input[name=customer_presence][value='No']").prop("checked",false);
            	jQuery("#scrap_update").html(btn);
            	jQuery("#submit_scrap_request").css('background','#000');
            }
	    }); 
	    jQuery(".change_customer_presence2").click(function(){
	   	    jQuery(".change_customer_presence1").attr('checked', false);
	   	    jQuery(this).attr('checked', true);
	   	    jQuery(".form-group-customer-presence").removeClass('hide');
            jQuery(".form-group-customer-presence").show();
            jQuery(".form-group-interval").show();
            jQuery(".form-group-scrap-type").addClass('hide');
            jQuery(".sun").removeClass('hide');
            jQuery(".sat").removeClass('hide');
            jQuery('.scrap_comment_div').html('');
            var periodic_scrap_id ='<?php foreach($periodic as $periodic_id){echo $periodic_id->id.'<br>';} ?>';
            var periodic_customer_presence ='<?php foreach($periodic as $presence){echo $presence->customer_presence;} ?>';
            var periodic_customer_presence_day ='<?php foreach($periodic as $presence){echo $presence->prefered_pickup_day;} ?>';
            var periodic_customer_presence_date_next ='<?php foreach($periodic as $presence){echo $presence->prefered_pickup_date_next;} ?>';
            var pickup_frequency = '<label>Preferred Pickup Frequency</label><?php echo "<select>".$pickup_frequency."</select>"; ?>';
            var pre_prefered_pickup_date = '<?php echo date('d-m-Y',strtotime($periodic['0']->prefered_pickup_date))?>';
            jQuery(".pick_up_frequency").html(pickup_frequency);
            jQuery(".periodic_scrap_comment").html("<span>Good to chose this option if you have newspapers to sell regularly.</span>");
            jQuery(".pick_up_frequency select").addClass('form-control');
            jQuery('.text-request-id').html('');
            jQuery('.pre_prefered_pickup_date').html('');
            var txt = '<p></p>';
			jQuery('.text-danger-request-type,.text-danger-customer-presence-day,.text-danger-customer-presence').html(txt);
			jQuery('.text-danger-request-type,.text-danger-customer-presence-day,.text-danger-customer-presence').css('color','#ffffff');
            jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
    		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
            if (periodic_scrap_id != '') {
            	var btn = '<input type="submit" name="<?php foreach($periodic as $periodic_id){echo $periodic_id->id;} ?>" id="submit_scrap_update" value="Update Request" class="btn btn-lg btn-primary submit_scrap_update"  />';
            	jQuery('.text-request-id').html('Scrap request id : '+periodic_scrap_id);
            	jQuery('.pre_prefered_pickup_date').html('Preferred pickup date : '+pre_prefered_pickup_date);
            	jQuery('#datetimepicker').data("DateTimePicker").date(periodic_customer_presence_date_next);
            	
            	if(periodic_customer_presence == 'Yes'){
            		jQuery("input[name=customer_presence][value='Yes']").prop("checked",true);
            		jQuery("input[name=customer_presence][value='No']").prop("checked",false);
            		if(periodic_customer_presence_day == 'Sunday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",true);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
	            	else if(periodic_customer_presence_day == 'Saturday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",true);
	            	}
	            	else{
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
            	}else{
            		
            		jQuery("input[name=customer_presence][value='Yes']").prop("checked",false);
            		jQuery("input[name=customer_presence][value='No']").prop("checked",true);
            		if(periodic_customer_presence_day == 'Sunday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",true);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
	            	else if(periodic_customer_presence_day == 'Saturday'){
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",true);
	            	}
	            	else{
	            		jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	            		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	            	}
            	}
            	jQuery("#scrap_update").html(btn);
            	jQuery("#submit_scrap_request").css('background','#428bca');
            }
            else{
            	var btn = '<input type="submit" name="submit_scrap_request" id="submit_scrap_request" value="Submit Request" class="btn btn-lg btn-primary submit_scrap_request"  />';
            	jQuery("input[name=customer_presence][value='Yes']").prop("checked",true);
            	jQuery("input[name=customer_presence][value='No']").prop("checked",false);
            	jQuery("#scrap_update").html(btn);
            	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
    			jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
            	jQuery("#submit_scrap_request").css('background','#000');
            }
	    });
	    jQuery('body').on('click', '.submit_scrap_update1', function(){
   			var scrap_id = jQuery("#submit_scrap_update").attr("name");
			var customer_presence = jQuery('input[name=customer_presence]:checked').val();
			var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
		    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
		    if(prefered_pickup_date == '' || prefered_pickup_date == undefined){
	   	    	if(request_type=='Periodic'){
	   	    		prefered_pickup_date = '<?php echo $periodic['0']->prefered_pickup_date?>';
	   	    		prefered_pickup_day = '<?php echo $periodic['0']->prefered_pickup_day?>';
			    }    
			    else{
			    	prefered_pickup_date = '<?php echo $non_periodic['0']->prefered_pickup_date?>';
	   	    		prefered_pickup_day = '<?php echo $non_periodic['0']->prefered_pickup_day?>';
			    }
		    }
		    var request_type = jQuery('input[name=request_type]:checked').val();
		    var scrap_comment = '';
		    var interval      = 0;
		    if(request_type=='Periodic'){
		    	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
		    }
		    else{
		    	scrap_comment = jQuery("#scrap_comment").val();
		    }
		    jQuery.ajax({
				type : 'POST',
			    url : "<?php echo base_url('update_scrap'); ?>",
			    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',scrap_id:scrap_id,request_type:request_type,customer_presence:customer_presence,interval:interval,prefered_pickup_day:prefered_pickup_day,scrap_comment:scrap_comment,prefered_pickup_date:prefered_pickup_date},
			    'success' : function(response) {
			    	jQuery('.text-request-id').html(' ');
			    	if (response == 'true'){
			        	tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-success"><p><strong>SUCCESS!</strong> Your scrap request is updated successfully.</p></div>';
		        	}else if(response == 'Canceled'){
						tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-success"><p><strong>Sorry!</strong> You can\'t update canceled scrap request.</p></div>';
					}else if(response == 'Complete'){
						tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-success"><p><strong>Sorry!</strong> You can\'t update completed scrap request.</p></div>';
					}else if(response == 'Process'){
						tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-success"><p><strong>Sorry, can\'t update!</strong> Your scrap request is under processing.</p></div>';
					}else {
						tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-success"><p><strong>SUCCESS!</strong> Your scrap request is updated successfully.</p></div>';
					}
					jQuery(".scrap_admin_message").html(tag);
			    },
			    'error' : function(request,error)
			    {
			        alert("Request: "+JSON.stringify(request));
			    }
			});
		}); 
		jQuery('body').on('click', '.submit_scrap_request', function(){
                    event.preventDefault();
		    var user_id = <?php echo $user->id?>;
		    var customer_presence = jQuery('input[name=customer_presence]:checked').val();
		    var request_type  = jQuery('input[name=request_type]:checked').val();
		    var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
		    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
		    var scrap_comment = '';
		    var interval      = 0;
		    var cartoon_boxes = '';
		    var paper         = '';
		    var error         = false;
		    if(request_type == 'Periodic'){
		    	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
		    }
		    else{
		    	scrap_comment = jQuery("#scrap_comment").val();
		    }
		    if(request_type=="" || request_type== undefined) {
            	var txt = '<p>Please select scrap request type</p>';
            	var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-request-type').html(txt);
            	jQuery('.text-danger-request-type').css('color','red');
            	error = true;
            }
        	if (customer_presence == "select" || customer_presence == ""   ||customer_presence == undefined) {
	        	var txt = '<p>Please select an option</p>';
            	jQuery('.text-danger-customer-presence').html(txt);
            	jQuery('.text-danger-customer-presence p').css('color','red');
	        	error = true;
	        }
            if(prefered_pickup_day=='' || prefered_pickup_day == undefined){
		    	var txt = '<p>Please select your prefered day for picking scrap</p>';
		    	var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-customer-presence-day').html(txt);
            	jQuery('.text-danger-customer-presence-day p').css('color','red');
	        	error = true;
		    }
		    if (error){
		    	return false;
		    }
		    // for checking users address is available or not in our service area part start
			var newUser = 0;
		    jQuery.ajax({
				type : 'POST',
			    url : "<?php echo base_url('serviceAddressCheck'); ?>",
			    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',user_id:user_id,newUser:newUser},
			    'success' : function(data) {  
			        if (data<6) {
					    if(request_type == 'Non-Periodic')
			    			request_type ='One Time Sell';
					    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p>You like to sell your scrap : '+request_type+'</p><p>Your prefer day of picking scrap : '+prefered_pickup_day+'</p><p>You are ok with keeping scrap outside door on pickup eve : '+customer_presence+'</p><p class="alert alert-info"> We are not yet operational in your locality/apartment, but you can still proceed placing the scrap request and admin will review and may accept.</p></div><button class="btn submit_scrap_request1 btn-md btn-success">Submit</button><a class="btn btn-md btn-danger cancel" href="<?php echo base_url() ?>">Cancel and go to Home Page</a></div>';
					    var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
			        	jQuery('.alert_msg1').html(txt1);
			        	jQuery(".scrap_admin_message").html(tag);
			        	jQuery("#confirm_scrap_request_box").css('display','block');
			        }
			        else{
			        	if(request_type == 'Non-Periodic')
			    			request_type ='One Time Sell';
					    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p>You like to sell your scrap : '+request_type+'</p><p>Your prefer day of picking scrap : '+prefered_pickup_day+'</p><p>You are ok with keeping scrap outside door on pickup eve : '+customer_presence+'</p></div><button class="btn submit_scrap_request1 btn-md btn-success">Submit</button><button class="btn cancel btn-md btn-danger">Cancel</button></div>';
					    var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
			        	jQuery('.alert_msg1').html(txt1);
			        	jQuery(".scrap_admin_message").html(tag);
			        	jQuery("#confirm_scrap_request_box").css('display','block');
			        }
			    },
			    'error' : function(request,error)
			    {
			        alert("Request: "+JSON.stringify(request));
			    }
			});
			// for checking users address is available or not in our service area part end
        });
		jQuery('body').on('click', '.submit_scrap_request1', function(event) {
		    event.preventDefault();
		    var user_id = <?php echo $user->id?>;
		    var customer_presence = jQuery('input[name=customer_presence]:checked').val();
		    var request_type  = jQuery('input[name=request_type]:checked').val();
		    var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
		    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
		    var scrap_comment = '';
		    var cartoon_boxes = '';
		    var interval      = 0;
		    var paper         = '';
		    if(request_type == 'Periodic'){
		    	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
		    }
		    else{
		    	scrap_comment = jQuery("#scrap_comment").val();
		    }
			jQuery.ajax({
				type : 'POST',
			    url : "<?php echo base_url('address_check'); ?>",
			    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',request_type:request_type,customer_presence:customer_presence,interval:interval,user_id:user_id,cartoon_boxes:cartoon_boxes,paper:paper,prefered_pickup_day:prefered_pickup_day,scrap_comment:scrap_comment,prefered_pickup_date:prefered_pickup_date},
			    'success' : function(data) {             
			        if (data=='no') {
			        	tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-danger"><p><strong>SERVICE ADDRESS!</strong> Please Fill your service Address.</p></div>';
    		            jQuery('.text-request-id').html(' ');
    		        	jQuery(".request_type").attr('checked', false);
    		        	jQuery(".customer_presence").attr('checked', false);
    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
    		        	jQuery(".scrap_admin_message").html(tag);
			        }
			        else{
			        	submitRquest(request_type,customer_presence,interval,prefered_pickup_day,prefered_pickup_date);
			        }
			    },
			    'error' : function(request,error)
			    {
			        alert("Request: "+JSON.stringify(request));
			    }
			});
		}); 
        function submitRquest(request_type,customer_presence,interval,prefered_pickup_day,prefered_pickup_date){
        	var user_id = "<?php echo $user->id;?>";
		 	var cartoon_boxes = '';
	   	    var paper         = '';
		    var scrap_comment = '';
		    if(request_type == 'Non-Periodic'){
            	scrap_comment = jQuery("#scrap_comment").val();
            }   
	        jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('scrap_request'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',customer_presence:customer_presence,request_type:request_type,user_id:user_id,interval:interval,cartoon_boxes:cartoon_boxes,paper:paper,prefered_pickup_day:prefered_pickup_day,scrap_comment:scrap_comment,prefered_pickup_date:prefered_pickup_date},
		        'success' : function(data) {console.log(va);
		        	var va = jQuery.parseJSON(data); 
		        	console.log(va);
		        	if(va['check_other_address']<6){
		        		if(va['check_other_address']==1){
		        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
		        		}
		        		if(va['check_other_address']==2){
		        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
		        		}
		        		if(va['check_other_address']==3){
		        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
		        		}
		        		if(va['check_other_address']==4){
		        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
		        		}
		        		if(va['check_other_address']==5){
		        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
		        		}
		        	}
		        	else{
		        		if(va['results']==1){
			        		tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-danger"><p><strong>Booking Not Confirmed !</strong> Error.</p></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
			        	}
			        	if(va['results']==2){
			        		tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-danger"><span><strong>NOT BOOKED!</strong> You already have an active periodic scrap request. If you want to modify your request </span><a href="<?php echo base_url("user_dashboard/my-scrap-orders");?>"><input type="button" class="btn btn-primary" value="Click here" id="update_btn" style="margin-left:30px;"></a></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
			        	}
			        	if(va['results']==3){
			        		tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="alert alert-danger"><span><strong>NOT BOOKED!</strong> You already have a periodic scrap request but it is paused currently.</span><a href="<?php echo base_url("user_dashboard/my-scrap-orders");?>"><input type="button" class="btn btn-primary" value="Click here to resume" id="update_btn" style="margin-left:30px;"></a></div>';
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
			        	}
			        	if (va['results'] ==4) {
			        		if(request_type == 'Non-Periodic')
			        			request_type ='One Time Sell';
				        	tag = "<span class='close scrap_popup_close'>&times;</span><div><br><label for='email'></label><div class='alert alert-success'><p><strong>Success!</strong> Your "+ request_type +" Request is placed successfully. Your Request Id : "+va['scrap_id']+"</p></div>";
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
			        	}
			        	if(va['results'] == 5){
			        		tag = "<span class='close'>&times;</span><div><br><label for='email'></label><div class='alert alert-danger'><span><strong>NOT BOOKED!</strong> You already have an active one-time sell scrap request. If you want to modify your request </span><a href='<?php echo base_url('user_dashboard/my-scrap-orders');?>'><input type='button' class='btn btn-primary' value='Click Here' id='update_btn' style='margin-left:30px;'></a></div>";
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
			        		
			        	}	        	
			        	if(va['results'] == 0){
			        		tag = "<span class='close'>&times;</span><div><br><label for='email'></label><div class='alert alert-danger'><p><strong>Error!</strong> Email address already exists</p></div>";
	    		            jQuery('.text-request-id').html(' ');
	    		        	jQuery(".request_type").attr('checked', false);
	    		        	jQuery(".customer_presence").attr('checked', false);
	    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
	                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
	    		        	jQuery(".scrap_admin_message").html(tag);
			        	}
		        	}
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });
		}
	    jQuery("#country option:contains('India')").attr('selected', 'selected');
	    var countries = <?php echo json_encode($countries) ?>;
	    if(countries.length > 1){
	       setTimeout(function(){ jQuery("#country").trigger("change"); }, 3000);    
	    }
	    jQuery('#country').on('change',function(){
	    	country_value = jQuery(this).val();
            var country_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            var data = {country : country_id, gre_tokan : token };
            jQuery('#state').find('option').not(':first').remove();
            jQuery('#city').val('');
    		jQuery('#locality').val('');
    		jQuery('#apartment').val('');
    		jQuery('#city_list').html('');
    		jQuery('#locality_list').html('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_state'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#state').attr("disabled", true);
	                jQuery('#city').attr("disabled", true);
	                jQuery('#locality').attr("disabled", true);
	                jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var state_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < state_list.length; i++) {
                        var obj = state_list[i];
                        jQuery('#state').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
                    	jQuery('#state').attr("disabled", true);
	                }
	                else{
                    	jQuery('#state').attr("disabled", false);
	                }
	            }
	        });
	    });
	    jQuery('#state').on('change',function(){
	    	state_value = jQuery(this).val();
            var state_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            var data = {state : state_id, gre_tokan : token };
            jQuery('#city').find('option').not(':first').remove();
            jQuery('#city').val('');
    		jQuery('#locality').val('');
    		jQuery('#apartment').val('');
    		jQuery('#city_list').html('');
    		jQuery('#locality_list').html('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_city'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#city').attr("disabled", true);
	                jQuery('#locality').attr("disabled", true);
                	jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var city_list = jQuery.parseJSON(response);
	            	jQuery('#city').append(jQuery("<option>sdfdsf</option>"));
	            	for(var i = 0; i < city_list.length; i++) {
                        var obj = city_list[i];
                        jQuery('#city_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(state_value=='' || state_value==' - State - ' || state_value=='0'){
                    	jQuery('#city').attr("disabled", true);
	                }
	                else{
                    	jQuery('#city').attr("disabled", false);
	                }
	            }
	        });
	    }); 	    
	    jQuery('#city').on('change',function(){
            var city_value = jQuery(this).val();
            city_value = city_value.trim();
    		var city_id = jQuery('#city_list').find("[value='" + city_value + "']").attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            var data = {city : city_id, gre_tokan : token };
            jQuery('#locality').find('option').not(':first').remove();
            jQuery('#locality').val('');
    		jQuery('#apartment').val('');
    		jQuery('#locality_list').html('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_locality'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#locality').attr("disabled", true);
                	jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var locality_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < locality_list.length; i++) {
                        var obj = locality_list[i];
                        jQuery('#locality_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(city_value==''){
	                	jQuery('#locality').attr("disabled", true);
	                }
	                else{
                    	jQuery('#locality').attr("disabled", false);
	                }
	            }
	        });
	    }); 	   
        jQuery('#locality').on('change',function(){
            var locality_value = jQuery(this).val();
            locality_value = locality_value.trim();
    		var locality_id = jQuery('#locality_list').find("[value='" + locality_value + "']").attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            var data = {locality : locality_id, gre_tokan : token };
            jQuery('#apartment').find('option').not(':first').remove();
    		jQuery('#apartment').val('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_apartment'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var apartment_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < apartment_list.length; i++) {
                        var obj = apartment_list[i];
                        jQuery('#apartment_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    jQuery('#apartment').attr("disabled", false);
                    if(locality_value==''){
	                	jQuery('#apartment').attr("disabled", true);
	                }
	            }
	        });
	    });        
        jQuery('body').on('click', '#submit_request', function() {
		    var customer_presence = jQuery('input[name=customer_presence]:checked').val();
            var request_type = jQuery('input[name=request_type]:checked').val();
            var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
		    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
			var country   = jQuery('#country').val();
			var state     = jQuery('#state').val();
			var city      = jQuery('#city').val();
			var locality  = jQuery('#locality').val();
			var pin       = jQuery('#pin').val();
			var apartment = jQuery('#apartment').val();
			var flat_no   = jQuery('#flat_no').val();
			var user_id   = "<?php echo $user->id;?>";
			var name      = jQuery('#name').val();
			var email     = jQuery('#email').val();
            var cartoon_boxes = '';
            var scrap_comment = '';
	   	    var paper         = '';
            var interval      = 0;
            var error         = false;
            jQuery('.text-danger').text('');
            if(request_type == 'Periodic'){
            	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
            }   
            else{
            	scrap_comment = jQuery("#scrap_comment").val()
            }         
        	if (customer_presence == "select" || customer_presence == ""   ||customer_presence == undefined) {
	        	var txt = '<p>Please select customer presence</p>';
            	jQuery('.text-danger-customer-presence').html(txt);
            	jQuery('.text-danger-customer-presence').css('color','red');
	        	error = true;
	        }
            if(request_type=="" || request_type== undefined) {
            	var txt = '<p>Please select request type</p>';
            	var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-request-type').html(txt);
            	jQuery('.text-danger-request-type').css('color','red');
            	error = true;
            }
            if(prefered_pickup_day=='' || prefered_pickup_day == undefined){
		    	var txt = '<p>Please select your preferred day for scrap pickup.</p>';
		    	var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-customer-presence-day').html(txt);
            	jQuery('.text-danger-customer-presence-day p').css('color','red');
	        	error = true;
		    }			
			if(name == '') {
				jQuery('.text-danger-name').text('Please Enter Your Name');
				error = true;
			}
			if(!isNaN(name)){
			    jQuery('.text-danger-name').text('Please Enter Only Characters');
				error = true;
			}
			if(name.length < 3){
			    jQuery('.text-danger-name').text('Please Enter Atleast Three Characters');
				error = true;
			}
			if(email == ''){
				jQuery('.text-danger-email').text('Please Enter Your Email');
				error = true;
			}
			if(!validateEmail(email)){
			 	jQuery('.text-danger-email').text('Please Enter Valid Email');
				error = true;
			}
			if(country == '0'){
			    jQuery('.text-danger-country').text('Please Select Country');
			    error = true;
			}
			if(state == '0'){
			    jQuery('.text-danger-state').text('Please Select State');
			    error = true;
			}
			if(city == '0' || city == '' || city == undefined){
			    jQuery('.text-danger-city').text('Please Select City');
			    error = true;
			}
			if(locality == '0' || locality == '' || locality == undefined){
			    jQuery('.text-danger-locality').text('Please Select Locality');
			    error = true;
			}
			if(apartment == '0' || apartment == '' || apartment == undefined){
			    jQuery('.text-danger-apartment').text('Please Select Apartment');
				error = true;
			}
			if(flat_no == ''){
			    jQuery('.text-danger-flat_no').text('Please Enter Flat Number');
				error = true;
			}
			if(error){
			    var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
			    return false;
			}
			// for checking users address is available or not in our service area part start
			var newUser = 1;
		    jQuery.ajax({
				type : 'POST',
			    url : "<?php echo base_url('serviceAddressCheck'); ?>",
			    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',user_id:user_id, newUser:newUser,country:country,state:state,city:city,locality:locality,apartment:apartment},
			    'success' : function(data) {  
			        if (data<6) {
					    if(request_type == 'Non-Periodic')
			    			request_type ='One Time Sell';
					    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p>You like to sell your scrap : '+request_type+'</p><p>Your prefer day of picking scrap : '+prefered_pickup_day+'</p><p>You are ok with keeping scrap outside door on pickup eve : '+customer_presence+'</p><p class="alert alert-info"> We are not yet operational in your locality/apartment, but you can still proceed placing the scrap request and admin will review and may accept.</p></div><button class="btn submit_request1  btn-md btn-success" id="submit_request1">Submit</button><a class="btn btn-md btn-danger cancel" href="<?php echo base_url() ?>">Cancel and go to Home Page</a></div>';
			        	jQuery(".scrap_admin_message").html(tag);
			        	jQuery("#confirm_scrap_request_box").css('display','block');
			        }
			        else{
						if(request_type == 'Non-Periodic')
			    			request_type ='One Time Sell';
					    tag = '<span class="close">&times;</span><div><br><h3>Confirm the Details : </h3><div class="scrap_request_msg"><p>You like to sell your scrap : '+request_type+'</p><p>Your prefer day of picking scrap : '+prefered_pickup_day+'</p><p>You are ok with keeping scrap outside door on pickup eve : '+customer_presence+'</p></div><button class="btn submit_request1  btn-md btn-success" id="submit_request1">Submit</button><button class="btn cancel btn-md btn-danger">Cancel</button></div>';
			        	jQuery(".scrap_admin_message").html(tag);
			        	jQuery("#confirm_scrap_request_box").css('display','block');	
			        }
			    },
			    'error' : function(request,error)
			    {
			        alert("Request: "+JSON.stringify(request));
			    }
			});
			// for checking users address is available or not in our service area part end
        });
        jQuery('body').on('click', '#submit_request1', function() {
        	customer_presence = jQuery('input[name=customer_presence]:checked').val();
            var request_type = jQuery('input[name=request_type]:checked').val();
            var prefered_pickup_date = jQuery('#prefered_pickup_day').val();
		    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		    var prefered_pickup_day = weekday[new Date(prefered_pickup_date).getDay()];
		    var country   = jQuery('#country').val();
			var state     = jQuery('#state').val();
			var city      = jQuery('#city').val();
			var locality  = jQuery('#locality').val();
			var pin       = jQuery('#pin').val();
			var apartment = jQuery('#apartment').val();
			var flat_no   = jQuery('#flat_no').val();
			var user_id   = "<?php echo $user->id;?>";
			var name      = jQuery('#name').val();
			var email     = jQuery('#email').val();
			var cartoon_boxes = '';
	   	    var paper         = '';
			var error         = false;
            var interval      = 0;
		    var scrap_comment = '';
			jQuery('.text-danger').text('');
			if(request_type == 'Periodic'){
            	interval = jQuery('.pick_up_frequency select option:selected').val()*7;
            } 
            else{
		    	scrap_comment = jQuery("#scrap_comment").val();
		    }
            if (request_type=="" || request_type== undefined) {
            	var txt = '<p>Please select request type</p>';
            	var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-request-type').html(txt);
            	jQuery('.text-danger-request-type').css('color','red');
				error = true;
            }
            if(prefered_pickup_day=='' || prefered_pickup_day == undefined){
		    	var txt = '<p>Please select your preferred day for scrap pickup.</p>';
		    	var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
            	jQuery('.text-danger-customer-presence-day').html(txt);
            	jQuery('.text-danger-customer-presence-day p').css('color','red');
	        	error = true;
		    }
		    if (customer_presence == "select" || customer_presence == ""   ||customer_presence == undefined) {
	        	var txt = '<p>Please select customer presence</p>';
            	jQuery('.text-danger-customer-presence').html(txt);
            	jQuery('.text-danger-customer-presence').css('color','red');
	        	error = true;
	        }
            
			if(name == ''){
				jQuery('.text-danger-name').text('Please Enter Your Name');
				error = true;
			}
			if(!isNaN(name)){
			    jQuery('.text-danger-name').text('Please Enter Only Characters');
				error = true;
			}
			if(name.length < 3){
			    jQuery('.text-danger-name').text('Please Enter Atleast Three Characters');
				error = true;
			}
			if(email == ''){
				jQuery('.text-danger-email').text('Please Enter Your Email');
				error = true;
			}
			if(!validateEmail(email)){
			 	jQuery('.text-danger-email').text('Please Enter Valid Email');
				error = true;
			}
			if(country == '0'){
			    jQuery('.text-danger-country').text('Please Select Country');
			    error = true;
			}
			if(state == '0'){
			    jQuery('.text-danger-state').text('Please Select State');
			    error = true;
			}
			if(city == '0'){
			    jQuery('.text-danger-city').text('Please Select City');
			    error = true;
			}
			if(locality == '0'){
			    jQuery('.text-danger-locality').text('Please Select Locality');
			    error = true;
			}
			if(apartment == '0'){
			    jQuery('.text-danger-apartment').text('Please Select Apartment');
				error = true;
			}
			if(flat_no == ''){
			    jQuery('.text-danger-flat_no').text('Please Enter Flat Number');
				error = true;
			}
			if(error){
			    var txt1 = '<p class="alert-danger">Please fix errors in the form above and resubmit</p>';
            	jQuery('.alert_msg1').html(txt1);
			    return false;
			}
			else{
	        	jQuery.ajax({
					type : 'POST',
			        url : "<?php echo base_url('request_scrap'); ?>",
			        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',country:country,state:state,city:city,locality:locality,pin:pin,apartment:apartment,flat_no:flat_no,customer_presence:customer_presence,request_type:request_type,user_id:user_id,interval:interval,cartoon_boxes:cartoon_boxes,paper:paper,name:name,email:email,prefered_pickup_day:prefered_pickup_day,scrap_comment:scrap_comment,prefered_pickup_date:prefered_pickup_date},
			        'success' : function(data) {  
			        	var va = jQuery.parseJSON(data); 
			        	if(va['check_other_address']<6){
			        		if(va['check_other_address']==1){
			        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
			        		}
			        		if(va['check_other_address']==2){
			        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
			        		}
			        		if(va['check_other_address']==3){
			        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
			        		}
			        		if(va['check_other_address']==4){
			        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
			        		}
			        		if(va['check_other_address']==5){
			        			tag = '<span class="close update_close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-info"><p><strong>Under Review!</strong> Your scrap request is under review. You will be notified once approved by GreenREE admin.</p></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
			        		}
			        	}
			        	else{
			        		if(va['results']==1){
				        		tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-danger"><p><strong>NOT BOOKED!</strong> Error.</p></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
				        	}
				        	if(va['results']==2){
				        		tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="scrap_request_msg"><div class="alert alert-danger"><span><strong>NOT BOOKED!</strong> You already have an active periodic scrap request. If you want to modify your request </span><a href="<?php echo base_url("user_dashboard/my-scrap-orders");?>"><input type="button" class="btn btn-primary" value="Click here" id="update_btn" style="margin-left:30px;"></a></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
				        	}   
				        	if(va['results']==3){
				        		tag = '<span class="close">&times;</span><div><br><label for="email"></label><div class="alert alert-danger"><span><strong>NOT BOOKED!</strong> You already have a periodic scrap request but it is paused currently.</span><a href="<?php echo base_url("user_dashboard/my-scrap-orders");?>"><input type="button" class="btn btn-primary" value="Click here to resume" id="update_btn" style="margin-left:30px;"></a></div>';
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
				        	}
				        	if (va['results'] ==4) {
				        		if(request_type == 'Non-Periodic')
				        			request_type = 'One Time Sell';
				        		tag = "<span class='close scrap_popup_close'>&times;</span><div><br><label for='email'></label><div class='alert alert-success'><p><strong>Success!</strong> Your "+ request_type +" Request is placed successfully. Your Request Id : "+va['scrap_id']+"</p></div>";
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
				        	}
				        	if(va['results'] == 5){
				        		tag = "<span class='close'>&times;</span><div><br><label for='email'></label><div class='alert alert-danger'><span><strong>NOT BOOKED!</strong> You already have an active one-time sell scrap request. If you want to modify your request </span><a href='<?php echo base_url('user_dashboard/my-scrap-orders');?>'><input type='button' class='btn btn-primary' value='Click Here' id='update_btn' style='margin-left:30px;'></a></div>";
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
				        	}
				        	if(va['results'] == 0){
			        			tag = "<span class='close'>&times;</span><div><br><label for='email'></label><div class='alert alert-danger'><p><strong>Error!</strong> Email address already exists</p></div>";
		    		            jQuery('.text-request-id').html(' ');
		    		        	jQuery(".request_type").attr('checked', false);
		    		        	jQuery(".customer_presence").attr('checked', false);
		    		        	jQuery("input[name=customer_presence_day][value='Sunday']").prop("checked",false);
		                		jQuery("input[name=customer_presence_day][value='Saturday']").prop("checked",false);
		    		        	jQuery(".scrap_admin_message").html(tag);
				        	}
			        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			}
		});
		jQuery('.first-half #name').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-name').html(txt);
			jQuery('.alert_msg1').html(txt);
		});  
		jQuery('.second-half #email').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-email').html(txt);
			jQuery('.alert_msg1').html(txt);
		});  
		jQuery('.first-half #country').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-country').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
		jQuery('.second-half #state').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-state').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
		jQuery('.first-half #city').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-city').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
		jQuery('.second-half #locality').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-locality').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
		jQuery('.first-half #apartment').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-apartment').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
		jQuery('.second-half #flat_no').on('focus', function(){
			var txt = '';
			jQuery('.text-danger-flat_no').html(txt);
            jQuery('.alert_msg1').html(txt);
		});
		jQuery('.customer_presence_day').on('focus', function(){
			var txt = '<p></p>';
			jQuery('.text-danger-customer-presence-day').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
		jQuery('.customer_presence').on('focus', function(){
			var txt = '<p></p>';
			jQuery('.text-danger-customer-presence').html(txt);
			jQuery('.alert_msg1').html(txt);
		});
    });
</script>