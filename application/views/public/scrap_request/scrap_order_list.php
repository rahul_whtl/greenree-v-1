<?php $this->load->view('public/templates/header', array(
	'title' => 'Scrap Orders - GreenREE',
	'link' => 'account'
)) ?>
<?php //print_r($non_periodic); ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'scrap')) ?>
<div class="scrap-order-list">
	<div class="container">
		<div class="request-info-box hide">
			<div class="col-md-12 text-center">    
			    <div class="alert alert_msg">		    
			        <p><strong>Success!</strong> Your Request Is Updated Successfully.</p>
			    </div>    
			</div>
		</div>	
		<?php if(!empty($scrp_request)): ?>
		<div class="request-info-box">
			<div class="container">
				<div class="col-md-12 text-center">
				    <div class="alert alert_msg alert-info">		    
				        <p><strong>Alert ! </strong>You Already Have Active Scrap  Requests.</p>
				    </div>    
				</div>
			</div>
		</div><?php endif ?>
	<!-- <div class="col-md-12" style="margin: 20px 0 40px 0;">
		<div class="col-sm-3 form-group form-group-scrap-type hide">
			<label class="control-label" style="font-size: 20px; margin-bottom: 0;">Scrap Type</label>
			<label class="checkbox" style="font-size: 20px;font-weight: 500;"><input type="checkbox" name="paper" value="paper" id="paper" style=" margin-top: 8px">Paper</label>
			<label class="checkbox" style="font-size: 20px;font-weight: 500;"><input type="checkbox" name="cartoon_boxes" id="cartoon_boxes" value="cartoon_boxes" style=" margin-top: 8px">Cartoon Boxes</label>
			<div class="text-danger text-danger-scrap-type"></div>
		</div>
		<div class="col-sm-5 form-group-scrap-type hide" style="padding-top: 25px;">
			<div class="form-group" id="non-periodic-update">
				<input type="submit" name="non_periodic_update" id="non_periodic_update" value="Update Request" class="btn btn-lg btn-primary non_periodic_update"  />
				<div class="text-danger"></div>
			</div>
		</div>
		<div class="col-sm-5 form-group form-group-customer-presence hide">
			<label class="control-label"  style="font-size: 20px;">Customer Presence</label><br>
			<label style="font-size: 18px;font-weight: 500"><input type="radio" name="customer_presence" class="customer_presence change_customer_presence2" id="customer_presence" value="yes">I want to be Present During Sell.</label><br>
			<label style="font-size: 18px; font-weight: 500"><input type="radio" name="customer_presence" class="customer_presence change_customer_presence1" id="customer_presence" value="no">I don't want to be Present During Sell.</label>
			<div class="text-danger text-danger-customer-presence"></div>
		</div>
		<div class="col-sm-5 form-group-customer-presence hide" style="padding-top: 25px;">
			<div class="form-group" id="periodic-update">
				<input type="submit" name="periodic_update" id="periodic_update" value="Update Request" class="btn btn-lg btn-primary periodic_update"  />
				<div class="text-danger"></div>
			</div>
		</div>
	</div>	 -->
	<div class="lead page-header">My Scrap Requests</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						Scrap Requests
					</div>
					<div class="panel-body">
						<ul class="nav nav-tabs scrap_list_table" role="tablist">
							<li role="presentation" class="active">
								<a href="#Periodic" class="periodic" aria-controls="Periodic" role="tab" data-toggle="tab">
									Periodic
								</a>
							</li>
							<li role="presentation">
								<a href="#Non-Periodic" class="non-periodic" aria-controls="Non-Periodic" role="tab" data-toggle="tab">
									Non Periodic
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="Periodic">
								<?php if (empty($periodic)): ?>
									<div class="alert alert-warning">
										<p>There are no periodic requests available.</p><br>
										<a href="<?php echo base_url('sell-scrap'); ?>" class="btn btn-success btn-lg scrap-order-list-add-btn">Place New Scrap Request</a>
									</div>
								<?php else: ?>
									<table class="table table-striped">
										<thead class="text-muted">
											<tr>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Request Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Requested Date
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Last Pickup Date
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Next Pickup Date	
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Reason for not Pickup
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Periodic Status
													</div>
												</th>
												
												<th class="text-center">
													<div data-toggle="tooltip" >
														Request Status
													</div>
												</th>
												
												<th class="text-center" style="min-width: 250px;">
													<div data-toggle="tooltip" >
														Action
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
										<?php foreach ($periodic as $value){ ?>
											<tr>
												<td class="text-center">
													<?php if(empty($value->id)):?>
														<?php echo '--'; ?>
													<?php else:?>
													<?php echo anchor('scrap_details_view/'.$value->id,$value->id); ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if((date('Y-m-d', strtotime($value->requested_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->requested_date))== '1970-01-01')):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo date('jS M Y' ,strtotime($value->requested_date)); ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if((date('Y-m-d', strtotime($value->last_pickup_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->last_pickup_date))== '1970-01-01')):?>
														<?php echo '--'; ?>
													<?php else:?><?php echo date('jS M Y' ,strtotime(
													 $value->last_pickup_date)); ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if((date('Y-m-d', strtotime($value->next_pickup_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->next_pickup_date))== '1970-01-01')):?>
														<?php echo '--'; ?>
													<?php else: ?>	
														<?php echo date('jS M Y' ,strtotime(
													 $value->next_pickup_date));?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->not_picked_reason)):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo $value->not_picked_reason; ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if($value->periodic_request_status==1):?>
														<?php echo 'ACTIVE'; ?>
													<?php else:?>
														<?php echo 'INACTIVE'; ?>
													<?php endif ?>				
												</td>
												
												<td class="text-center">
													<?php echo $value->request_status; ?>			
												</td>
												
												<td class="text-center">
													<?php if($value->periodic_request_status==1/* && ($value->request_status=='Under Review' || $value->request_status=='Placed' || $value->request_status=='System' )*/):?>
														<a href="<?php echo base_url('update_scrap_request').'/'.$value->id; ?>">
														<input type="button" name="<?php echo $value->id; ?>" value="Edit" class="btn btn-success periodic_update_btn1"></a>
														<input type="button" name="<?php echo $value->id; ?>" value="Pause" class="btn btn-danger pause">
													<?php elseif($value->periodic_request_status == 0 && $value->request_status != 'Under Review'):?>
														<input type="button" name="<?php echo $value->id; ?>" value="Resume" class="btn btn-success resume">
													<?php endif ?>
												</td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								<?php endif ?>
							</div>
							<div role="tabpanel" class="tab-pane" id="Non-Periodic">
								<?php if (empty($non_periodic)): ?>
									<div class="alert alert-warning">
										<p>There are no One-time sell requests available.</p><br>
										<a href="<?php echo base_url('sell-scrap'); ?>" class="btn btn-success btn-lg">Place New Scrap Request</a>
									</div>
								<?php else: ?>
									<table class="table table-striped">
										<thead class="text-muted">
											<tr>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Request Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Requested Date
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Pickup Date
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Weight	
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Details
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Wallet Cash
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Reason for not Pickup
													</div>
												</th>
												<!-- <th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Details
													</div>
												</th> -->
												<th class="text-center">
													<div data-toggle="tooltip" >
														Request Status
													</div>
												</th>
												<th class="text-center" style="min-width: 250px;">
													<div data-toggle="tooltip" >
														Action
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
										<?php foreach ($non_periodic as $value){ ?>
											<tr>
												<td class="text-center">
													<?php if(empty($value->id)):?>
														<?php echo '--'; ?>
													<?php else:?>
													<?php echo $value->id; ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if((date('Y-m-d', strtotime($value->requested_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->requested_date))== '1970-01-01')):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo date('jS M Y' ,strtotime($value->requested_date)); ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if((date('Y-m-d', strtotime($value->next_pickup_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->next_pickup_date))== '1970-01-01')):?>
														<?php echo '--'; ?>
													<?php else:?><?php echo date('jS M Y' ,strtotime(
													 $value->next_pickup_date)); ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->scrap_weight)):?>
														<?php echo '--'; ?>
													<?php else: ?>	
														<?php echo $value->scrap_weight;?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->scrap_details)):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo $value->scrap_details; ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->reward_point)):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo $value->reward_point; ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->not_picked_reason)):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo $value->not_picked_reason; ?>
													<?php endif ?>
												</td>
												<?php /* <td class="text-center">
													<?php if(empty($value->scrap_details)):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo $value->not_picked_reason; ?>
													<?php endif ?>
												</td> */?>
												<td class="text-center">
													<?php if(empty($value->request_status)):?>
														<?php echo '--'; ?>
													<?php else:?>
														<?php echo $value->request_status; ?>
													<?php endif ?>				
												</td>
												<td class="text-center">
													<?php
													if($value->request_status=='Under Review' || $value->request_status=='Placed'):?>	  <a href="<?php echo base_url('update_scrap_request').'/'.$value->id; ?>"><input type="button" name="update" value="Edit" class="btn btn-success non-periodic-update1 update1"></a>
															<input type="button" name="<?php echo $value->id; ?>" value="Cancel" class="btn btn-danger non_periodic_cancel cancel">
													<?php else:?>
														<?php echo '--';?>
													<?php endif ?>
													
													<!-- <input type="button" style="float: right;" name="action_btn" value="Resume" class="btn btn-primary start"> -->
												</td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
<!--Resume pop up part start -->
<div id="myModal1" class="modal1">
  <div class="modal-content cancel_yes_content">
    <span class="close">&times;</span>
    <div>
    	<h4>Your scrap request will be resumed</h4>
    	
		<span>Are you sure you want to resume this scrap request?</span><br><br>
    	<input type="submit" class="cancel_yes btn btn-md btn-success" value="Yes" id="cancel_yes">
    	<input type="button" class="cancel_cancel btn btn-md btn-danger" value="No" id="cancel_cancel">
    </div>
  </div>
</div>
<!--Resume pop up part end -->
<!--Pause pop up part start -->
<div id="myModal" class="modal">
  <div class="modal-content pause_yes_content">
    <span class="close">&times;</span>
    <div>
    	<h4>Your scrap request will be paused</h4>
		<span>Are you sure you want to pause this scrap request?</span><br><br>
    	<input type="submit" class="pause_yes btn btn-md btn-success" value="Yes" id="pause_yes">
    	<input type="button" class="pause_cancel btn btn-md btn-danger" value="No" id="pause_cancel">
    </div>
  </div>
</div>
<!--Pause pop up part end -->
<!--Non Periodic cancel pop up part start -->
<div id="myModal2" class="modal2">
  <div class="modal-content non_periodic_yes_content">
    <span class="close">&times;</span>
    <div>
    	<h4>Your scrap request will be canceled</h4>
		<span>Are you sure you want to cancel this scrap request?</span><br><br>
    	<input type="submit" class="non_periodic_yes btn btn-md btn-success" value="Yes" id="non_periodic_yes">
    	<input type="button" class="non_periodic_no btn btn-md btn-danger" value="No" id="non_periodic_no">
    </div>
  </div>
</div>
<!--Non Periodic cancel pop up part end -->
<script type="text/javascript">
var modal = document.getElementById('myModal');
jQuery(document).ready(function(){
	jQuery('.pause').click(function(e){
		e.preventDefault();
		modal.style.display = "block";
	});
	jQuery("body").on('click','.close_reload',function(event){
		modal.style.display = "none";
		location.reload(); 
	});
	jQuery("body").on('click','.close,#pause_cancel',function(event){
		modal.style.display = "none";
	});
	window.onclick = function(event) {
  		if (event.target == modal) {
    		modal.style.display = "none";
  		}
	}
});
var modal1 = document.getElementById('myModal1');
jQuery(document).ready(function(){
	jQuery('.resume').click(function(e){
		e.preventDefault();
		modal1.style.display = "block";
	});
	jQuery("body").on('click','.cancel_reload',function(event){
		modal1.style.display = "none";
		location.reload(); 
	});
	jQuery("body").on('click','.close,#cancel_cancel',function(event){
		modal1.style.display = "none";
	});
	window.onclick = function(event) {
  		if (event.target == modal1) {
    		modal1.style.display = "none";
  		}
	}
});
var modal2 = document.getElementById('myModal2');
jQuery(document).ready(function(){
	jQuery('.cancel').click(function(e){
		e.preventDefault();
		modal2.style.display = "block";
	});
	jQuery("body").on('click','.non_periodic_reload',function(event){
		modal1.style.display = "none";
		location.reload(); 
	});
	jQuery("body").on('click','.close,#non_periodic_no',function(event){
		modal1.style.display = "none";
	});
	window.onclick = function(event) {
  		if (event.target == modal2) {
    		modal2.style.display = "none";
  		}
	}
});
	jQuery(document).ready(function(){
		jQuery(".pause_yes").click(function(event){
	   	    event.preventDefault();
	   	    var request_action = jQuery('.pause').val();
	   	    var scrap_id = jQuery('.pause').attr("name");
	   	    if (scrap_id == '' || scrap_id==undefined) {
	   	    	return;
	   	    }
	   	    else{
	   	        request_action = 0;
	   	    }
	   	    	//alert(request_action);
            jQuery.ajax({
			type : 'POST',
		    url : "<?php echo base_url('scrap_order_action'); ?>",
		    data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',request_action:request_action,scrap_id:scrap_id},
	        'success' : function(data) {
	                var tag = ' <span class="close close_reload">&times;</span><div><span>Your Periodic scrap request is paused. </span></div>';
	                jQuery(".pause_yes_content").html(tag);
	        	//	modal.style.display = "none";
	        	    //location.reload(); 
	        	  //  jQuery(".alert").removeClass("alert-success");
            // 		jQuery(".alert").addClass("alert-info");
	           // 	jQuery(".request-info-box").removeClass("hide");
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
		    });
		}); 
		jQuery(".cancel_yes").click(function(event){
		   	    event.preventDefault();
		   	    var request_action = jQuery('.resume').val();
		   	    var scrap_id = jQuery('.resume').attr("name");
		   	    if (scrap_id == '' || scrap_id==undefined) {
		   	    	return;
		   	    }
		   	    else{
		   	    	request_action = 1;
		   	    }
                jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('scrap_order_action'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',request_action:request_action,scrap_id:scrap_id},
		        'success' : function(data) {
	                var tag = ' <span class="close cancel_reload">&times;</span><div><span>Your Periodic scrap request is resumed. </span></div>';
	                jQuery(".cancel_yes_content").html(tag);
		        		// modal1.style.display = "none";
		        	 //   location.reload(); 
		        		// jQuery(".alert").removeClass("alert-info");
	           // 		jQuery(".alert").addClass("alert-success");
		          //  	jQuery(".request-info-box").removeClass("hide");
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });
		}); 
		jQuery(".non_periodic_yes").click(function(event){
		   	    event.preventDefault();
		   	    var scrap_id = jQuery('.non_periodic_cancel').attr("name");
		   	    if (scrap_id == '' ) {
		   	        return;
		   	    }
		   	    else{
		   	    	request_action = 'Canceled';
		   	    }
		   	    	
                jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('scrap_order_action1'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',request_action:request_action,scrap_id:scrap_id},
		        'success' : function(data) {
		            var tag = ' <span class="close non_periodic_reload">&times;</span><div><span>Your one time sell scrap request is canceled. </span></div>';
	                jQuery(".non_periodic_yes_content").html(tag);
		        		// modal2.style.display = "none"; 
		        	 //   jQuery(".alert").removeClass("alert-success");
	           // 		jQuery(".alert").addClass("alert-info");
		          //  	jQuery(".request-info-box").removeClass("hide");
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });
		}); 
		// jQuery(".start").click(function(event){
		//    	    event.preventDefault();
		//    	    var request_action = jQuery('.pause').val();
		//    	    if (request_action == 'Pause') {
		//    	    	request_action = 1;
		//    	    }
		//    	    else
		//    	    	return;
  //               jQuery.ajax({
		// 		type : 'POST',
		//         url : "<?php// echo base_url('scrap_order_action'); ?>",
		//         data : { '<?php //echo $this->security->get_csrf_token_name(); ?>':'<?php //echo $this->security->get_csrf_hash(); ?>',request_action:request_action},
		//         'success' : function(data) {
		//         		jQuery(".alert").removeClass("alert-info");
	 //            		jQuery(".alert").addClass("alert-success");
		//             	jQuery(".request-info-box").removeClass("hide");
		//         },
		//         'error' : function(request,error)
		//         {
		//             alert("Request: "+JSON.stringify(request));
		//         }
		//     });
		// }); 
		jQuery(".periodic_update_btn").click(function(event){
		    var value1 = jQuery('.panel-body .nav-tabs .active').text(); 
		    jQuery('.form-group-customer-presence').removeClass('hide');
		}); 
		jQuery(".update").click(function(event){
		    var value1 = jQuery('.panel-body .nav-tabs .active').text(); 
		    jQuery('.form-group-scrap-type').removeClass('hide');
		});


	});
</script>
<?php $this->load->view('public/templates/footer') ?>