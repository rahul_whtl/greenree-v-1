<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Cart',
	'link' => 'cart'
)) ?>
<div class="cart-view">
	<div class="container">
<div class="lead page-header">
	Cart Content
	<div class="clearfix"></div>
</div>

<?php if (empty($cart_items)): ?>
	<div class="alert alert-warning">
		You currently have no items in your shopping cart!
	</div>
<?php else: ?>
	<?php 
		$total_price = $this->flexi_cart->total();
		$total_price = str_replace("&#x20B9;", " ", $total_price);
        $total_price = substr($total_price, 0, -1) . '';
        $total_price = str_replace("&#x20B9;", "", $total_price);
	?>
	<?php if($total_reward_point < $total_price): ?>
	<div class="alert alert-warning text-center">
	  	<strong>Warning!</strong> You have insufficient wallet cash. But you can still complete your purchase by paying balance amount as <strong>cash on delivery</strong>.
	</div>
	<?php endif ?>
	<?= form_open('cart/checkout',array('id'=>'checkout-form')) ?>
		<div id="cart_content">
			<div id="ajax_content">
			    <div class="cart-items-wrap">
				    <table id="cart_items" class="table">
					<thead>
						<tr>
							<th class="item">Item</th>
							<th>Quantity</th>
							<th class="text-center">Price</th>
							<th class="text-center">Total</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$i = 0;
					    foreach($cart_items as $row) { $i++; ?>
							<tr>
								<td>
									<table>
										<tr class="cart-item-thumb">
											<td rowspan="3">
												<?php if (isset($row['thumb'])): ?>
													<img src="<?php echo $row['thumb'] ?>" style="height:40px;margin-right: 10px;">
												<?php else: ?>
													<span class="glyphicon glyphicon-picture text-muted" style="font-size:40px;margin-right: 10px;"></span>
												<?php endif ?>
											</td>
										</tr>
										<tr>
											<td><input type="text" name="item_id" value="<?= $row['id'] ?>" hidden>
												<strong class="item-name"><?= $row['name'] ?></strong>
											</td>
										</tr>
										<?php  ?>
									</table>
									<?php
									$status_msg = $this->flexi_cart->item_status_message($row['row_id'], 'text-danger');
										echo (! empty($status_msg)) ? $status_msg.'<br/>' : NULL;
									?>
								</td>
								<td>
									<?php
									?>
									<div class="input-group" style="width: 160px;">
										<input type="number" name="items[<?php echo $i;?>][quantity]" value="<?php echo $row['quantity'];?>" maxlength="3" class="form-control quantity-box"/>
			                            <span class="input-group-btn quantity-update">
			                                <div class="btn-group" role="group" aria-label="Basic example">
			                                    <button type="submit" name="update" value="&plusmn;" class="btn btn-primary" title="Update Quantity">
			                                        <b class="input-group">Update cart</b>
			                                    </button>
			                                </div>
			                            </span>
			                        </div>

								 	<?php ?>
								</td>
								<td class="text-center">
									<?php 
										if ($this->flexi_cart->item_discount_status($row['row_id'])) 
										{
											if ($row['non_discount_quantity'] == 0)
											{
												echo '<s>'.$row['price'].'</s><br/>';
											}
											
											else
											{
												echo $row['non_discount_quantity'].' @ '.$row['price'].'<br/>';
											}
											
											if ($row['discount_quantity'] > 0)
											{
												echo $row['discount_quantity'].' @ '. $row['discount_price'];
											}
										}
										else
										{
											echo $row['price'];
										}
									?>
								</td>
								<td class="text-center">
									<?php 
										if ($row['discount_quantity'] > 0)
										{
											echo '<s>'.$row['price_total'].'</s><br/>';
											echo $row['discount_price_total'].'<br/>';
										}
										else
										{
											echo $row['price_total'];
										}
									?>
								</td>
								<td class="text-center">
									<?= form_hidden('items['.$i.'][row_id]', $row['row_id']) ?>
										<button type="button" class="close" style="float: initial;" onClick="javascript: window.location='<?= site_url('delete_cart_item/'.$row['row_id']) ?>'">
								            <span aria-hidden="true">&times;</span>
								            <span class="sr-only">Close</span>
								        </button>
								</td>
							</tr>
							<?php 
							if ($this->flexi_cart->item_discount_status($row['row_id'], FALSE)) { 
							?>
							<tr class="discount">
								<td colspan="4">
									Discount: <?php echo $this->flexi_cart->item_discount_description($row['row_id']);?>
									<?php echo anchor('unset_discount/'.$this->flexi_cart->item_discount_id($row['row_id']), 'Remove Discount', 'class="btn btn-xs btn-danger"'); ?>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					<tfoot>
						<?php 
							if ($this->flexi_cart->item_summary_savings_total(FALSE) > 0) { 
						?>
							<tr class="discount">
								<th colspan="3">Item Summary Discount Savings Total</th> 
								<td class="text-center"><?php echo $this->flexi_cart->item_summary_savings_total();?></td>
							</tr>
						<?php } ?>
						<tr>
							<th colspan="3">Item Summary Total</th>
							<td class="text-center"><div class="lead"><?php echo $this->flexi_cart->item_summary_total();?></div></td>
						</tr>
					</tfoot>
				</table>
                </div>
				<?php 
					$free_shipping_discount = $this->flexi_cart->get_discount_requirements(5, FALSE);
					if ($free_shipping_discount['value'] > 0):
				?>
					<div class="alert alert-info">
						Spend another <?php echo $this->flexi_cart->get_currency_value($free_shipping_discount['value']);?> and get free worldwide shipping!
					</div>
				<?php endif ?>

				<?php?>
				
				<?php ?>

				<div class="panel panel-primary green-panel">
					<div class="panel-heading" style="text-transform:uppercase">Cart Summary</div>
					<table class="table table-striped" id="cart_summary">
						<tbody>
							<tr>
								<td>
									Item Summary Total
								</td>
								<td>
									<?php echo $this->flexi_cart->item_summary_total();?>
								</td>
							</tr>			
						<?php if ($this->flexi_cart->summary_discount_status()) { ?>
							<tr class="discount">
								<th>Discount Summary</th>
								<td>&nbsp;</td>
							</tr>
							
						<?php if ($this->flexi_cart->item_summary_discount_status()) { ?>
							<tr class="discount">
								<th>
									<span class="pad_l_20">
										&raquo; Item discount discount savings
									</span>
								</th>
								<td>
									<?php echo $this->flexi_cart->item_summary_savings_total();?>
								</td>
							</tr>
						<?php } ?>
						<?php foreach($discounts as $discount) { ?>
							<tr class="discount">
								<th>
									<span class="pad_l_20">
										&raquo; <?php echo $discount['description'];?>
									<?php if (! empty($discount['id'])) { ?>
										<?php echo anchor('unset_discount/'.$discount['id'], 'Remove', 'class="btn btn-xs btn-danger"') ?>
									<?php } ?>
									</span>
								</th>
								<td><?php echo $discount['value'];?></td>
							</tr>
						<?php } ?>
							<tr class="discount">
								<th>Discount Savings Total</th>
								<td><?php echo $this->flexi_cart->cart_savings_total();?></td>
							</tr>
						<?php } ?>
						<?php if ($this->flexi_cart->surcharge_status()) { ?>
							<tr class="surcharge">
								<th>Surcharge Summary</th>
								<td>&nbsp;</td>
							</tr>
						<?php foreach($surcharges as $surcharge) { ?>
							<tr class="surcharge">
								<th>
									<span class="pad_l_20">
										&raquo; <?php echo $surcharge['description'];?>
										: <a href="<?php echo base_url() ?>standard_library/unset_surcharge/<?php echo $surcharge['id']; ?>">Remove</a>
									</span>
								</th>
								<td><?php echo $surcharge['value'];?></td>
							</tr>
						<?php } ?>
							<tr class="surcharge">
								<th>Surcharge Total</th>
								<td><?php echo $this->flexi_cart->surcharge_total();?></td>
							</tr>
						<?php } ?>

						<?php if ($this->flexi_cart->reward_voucher_status()) { ?>
							<tr class="voucher">
								<th>Reward Voucher Summary</th>
								<td>&nbsp;</td>
							</tr>
						<?php foreach($reward_vouchers as $voucher) { ?>
							<tr class="voucher">
								<th>
									<span class="pad_l_20">
										&raquo; <?php echo $voucher['description'];?>
										: <a href="<?php echo base_url() ?>standard_library/unset_discount/<?php echo $voucher['id']; ?>">Remove</a>
									</span>
								</th>
								<td><?php echo $voucher['value'];?></td>
							</tr>
						<?php } ?>
							<tr class="voucher">
								<th>Reward Voucher Total</th>
								<td><?php echo $this->flexi_cart->reward_voucher_total();?></td>
							</tr>
						<?php } ?>

						</tbody>
						<tfoot>
							<!--<tr>-->
							<!--	<td>-->
							<!--		Sub Total (ex. tax)-->
							<!--	</td>-->
							<!--	<td>-->
							<!--		<?php //echo $this->flexi_cart->sub_total();?>-->
							<!--	</td>-->
							<!--</tr>-->
							<!--<tr>-->
							<!--	<td>-->
							<!--		<?php //echo $this->flexi_cart->tax_name()." @ ".$this->flexi_cart->tax_rate(); ?>-->
							<!--	</td>-->
							<!--	<td>-->
							<!--		<?php// echo $this->flexi_cart->tax_total();?>-->
							<!--	</td>-->
							<!--</tr>							-->
							<tr class="grand_total">
								<th>Grand Total</th>
								<th><?php echo $this->flexi_cart->total();?></th>
							</tr>
						</tfoot>
					</table>
				</div>
	<?php if(empty($users_address)){ ?>
	<div class="panel panel-primary green-panel">
	    <div class="panel-heading" style="text-transform:uppercase">Enter Your Address</div>
	    <p>&nbsp;</p>
	    <div class="row">
	        <div class="col-md-6 first-half">
		<div class="col-md-12">
			<div class="form-group form-group-name">
				<input type="text" name="name" id="name" placeholder="Name*" value="<?php echo ($user_details['0']->first_name)?$user_details['0']->first_name.' '.$user_details['0']->last_name:""; ?>" class="form-control input-lg name">
				<div class="text-danger text-danger-name"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group form-group-country">
				<select name="country" class="form-control input-lg country" id="country">
					<option value="0"> - Country - </option>
				    <?php if(sizeof($countries) == 1){ ?>
				    <option value="<?php echo $countries[0]['loc_name'];?>" data-val="<?php echo $countries[0]['loc_id'];?>" selected><?php echo $countries[0]['loc_name'];?></option>    
				    <?php }else{ foreach($countries as $country) { ?>
					<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>">
						<?php echo $country['loc_name'];?>
					</option>
				    <?php } }?>
				</select>
				<div class="text-danger text-danger-country"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group form-group-city">
			    <input list="city_list" name="city" id="city" class="form-control input-lg city" autocomplete="off" value="<?php if($user_address->city) echo $user_address->city;elseif($user_address->country == '' && sizeof($cities) == 1)echo $cities[0]['loc_name'];?>" placeholder="Select or Enter your City">
				<datalist id="city_list">
	                <?php if(sizeof($cities) == 1){ ?>
	                    <option value="<?php echo $cities[0]['loc_name'];?>" data-val="<?php echo $cities[0]['loc_id'];?>"    selected><?php echo $cities[0]['loc_name'];?></option>
	                <?php }else{ foreach($cities as $city) { ?>
	                <option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>">
	                    <?php echo $city['loc_name'];?>
	                </option>
	                <?php } } ?>
	            </datalist>
				<div class="text-danger text-danger-city"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group form-group-apartment">
			    <input list="apartment_list" name="apartment" id="apartment" class="form-control input-lg apartment" autocomplete="off" value="<?php if($user_address->apartment_name) echo $user_address->apartment_name;elseif($user_address->country == '' && sizeof($apartments) == 1)echo $apartments[0]['loc_name'];?>" placeholder="Select or Enter your Apartment">
				<datalist id="apartment_list"  data-dropup-auto="false">
	                <?php if(sizeof($apartments) == 1){ ?>
	                    <option value="<?php echo $apartments[0]['loc_name'];?>" data-val="<?php echo $apartments[0]['loc_id'];?>" selected><?php echo $apartments[0]['loc_name'];?></option>
	                    <option value="Others" data-val="Others">
	                    Others
	                    </option>
	                <?php }else{ foreach($apartments as $apartment) { ?>
	                <option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>">
	                    <?php echo $apartment['loc_name'];?>
	                </option>
	                <?php } } ?>    
	            </datalist>
				<div class="text-danger text-danger-apartment"></div>
			</div>
		</div>
	</div>
    <div class="col-md-6 second-half">
		<div class="col-md-12">
			<div class="form-group form-group-email">
				<?php if(strpos($user_details['0']->email, '@') !== false): ?>
					<input type="text" name="email" id="email" placeholder="Email*" value="<?php echo ($user_details['0']->email)?$user_details['0']->email:""; ?>" class="form-control input-lg email" readonly>
				<?php else:?>
					<input type="text" name="email" id="email" placeholder="Email*" class="form-control input-lg email">
				<?php endif ?>
				<div class="text-danger text-danger-email"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group form-group-state">
			    <select name="state" id="state" class="form-control input-lg state">
					<option value="0" selected="selected"> - State - </option>
					<?php if(sizeof($states) == 1){ ?>
				    <option value="<?php echo $states[0]['loc_name'];?>" data-val="<?php echo $states[0]['loc_id'];?>" selected><?php echo $states[0]['loc_name'];?></option>
				    <?php }else{ foreach($states as $state) { ?>
					<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>">
						<?php echo $state['loc_name'];?>
					</option>
				    <?php } }?>
				</select>
				<div class="text-danger text-danger-state state"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group form-group-locality">
			    <input list="locality_list" name="locality" id="locality" class="form-control input-lg locality" autocomplete="off" value="<?php if($user_address->locality) echo $user_address->locality;elseif($user_address->country == '' && sizeof($localities) == 1)echo $localities[0]['loc_name'];?>" placeholder="Select or Enter your Locality">
				<datalist id="locality_list">
	                <?php if(sizeof($localities) == 1){ ?>
	                       <option value="<?php echo $localities[0]['loc_name'];?>" data-val="<?php echo $localities[0]['loc_id'];?>" selected><?php echo $localities[0]['loc_name'];?>
	                       </option>
	                <?php }else{ foreach($localities as $locality) { ?>
	                    <option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>">
	                    <?php echo $locality['loc_name'];?>
	                    </option>
	                <?php } } ?>
	            </datalist>
				<div class="text-danger text-danger-locality"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group form-group-flat_no">
				<input type="text" class="form-control input-lg" id="flat_no" placeholder="Flat No.*" name="flat_no" value="" />
				<div class="text-danger text-danger-flat_no"></div>
			</div>
		</div>
	</div>
	    </div>
	    <p>&nbsp;</p>
	 </div>    
	<?php }else { ?>
	    <div class="panel panel-primary green-panel">
	    <div class="panel-heading" style="text-transform:uppercase">Shipping Address</div>
	    <div class="panel-content">
	    <ul class="list">
	       <li><strong>Name : </strong><?php echo $user_details['0']->first_name.' '.$user_details['0']->last_name ?></li> 
	       <li><strong>Email : </strong><?php echo $user_details['0']->email ?></li> 
	       <li><strong>Phone : </strong><?php echo $user_details['0']->phone ?></li> 
	       <li><strong>Country : </strong><?php echo $users_address['0']->country ?></li> 
	       <li><strong>State : </strong><?php echo $users_address['0']->state ?></li> 
	       <li><strong>City : </strong><?php echo $users_address['0']->city ?></li> 
	       <li><strong>Locality : </strong><?php echo $users_address['0']->locality ?></li> 
	       <li><strong>Apartment : </strong><?php echo $users_address['0']->apartment_name ?></li> 
	       <li id="flat_no_text"><strong>Flat No : </strong><?php echo $users_address['0']->flat_no ?></li> 
	    </ul>
	    <span class="profile-update-notice"><Strong>Note : </Strong>In case you want to update shipping address, please update it in your profile</span>
	    </div>
	 </div>
	 
	<?php } ?>
	 
</div>
		
<?php if(empty($users_address)){ ?>		


<?php } ?>

		<p>&nbsp;</p>
				
				<?php if (! $this->flexi_cart->location_shipping_status()): ?>
					<div class="alert alert-warning">
						<strong>Shipping Warning!</strong>
						There are items in your cart that cannot be shipped to your current shipping location.
					</div>
				<?php endif ?>

				<div class="well well-sm">
					<div class="pull-left">
						<div class="btn-group">
							<input type="submit" name="update" value="Update Cart" class="btn btn-lg btn-success"/>
							<input type="submit" name="clear" value="Clear Cart" class="btn btn-lg btn-default"/>
							<!--<input type="submit" name="destroy" value="Destroy Cart" class="btn btn-lg btn-warning" title="Destroy Cart will reset the cart to default values."/>-->
						</div>
					</div>
					<div class="pull-right">
					<?php //if($total_reward_point > $total_price): ?>
						<input type="hidden" name="is_checkout" id="is_checkout" value="">
						<input type="submit" name="checkout" class="btn btn-lg btn-primary checkout-button" value="Confirm Order">
						<?php //echo anchor('checkout', 'Confirm Order', 'class="btn btn-lg btn-primary checkout-button"') ?>
					<?php// else: ?>
					<?php// echo "";?>	
					<?php// endif ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	<?= form_close() ?>
<script type="text/javascript">
    function validateEmail(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(String(email).toLowerCase());
    }
    
    jQuery(document).ready(function(){
    
	jQuery(".checkout-button").click(function(e){
		    //e.preventDefault();
		    var users_address = jQuery('#flat_no_text').text();
			var country   = jQuery('#country').val();
			var state     = jQuery('#state').val();
			var city      = jQuery('#city').val();
			var locality  = jQuery('#locality').val();
			var apartment = jQuery('#apartment').val();
			var flat_no   = jQuery('#flat_no').val();
			var user_id   = "<?php echo $user->id;?>";
			var name      = jQuery('#name').val();
			var email     = jQuery('#email').val();
			jQuery('.text-danger').text('');
        
			var error = false;
			
		    if(users_address==''){
    			if(name == '') {
    				jQuery('.text-danger-name').text('Please Enter Your Name');
    				error = true;
    			}
    			
    			if(!isNaN(name)){
    			    jQuery('.text-danger-name').text('Please Enter Only Characters');
    				error = true;
    			}
    			if(name.length < 3){
    			    jQuery('.text-danger-name').text('Please Enter Atleast Three Characters');
    				error = true;
    			}
    			if(email == ''){
    				jQuery('.text-danger-email').text('Please Enter Your Email');
    				error = true;
    			}
    			if(!validateEmail(email)){
    			 	jQuery('.text-danger-email').text('Please Enter Valid Email');
    				error = true;
    			}
    			if(country == '0'){
    			    jQuery('.text-danger-country').text('Please Select Country');
    			    error = true;
    			}
    			if(state == '0'){
    			    jQuery('.text-danger-state').text('Please Select State');
    			    error = true;
    			}
    			if(city == '0'){
    			    jQuery('.text-danger-city').text('Please Select City');
    			    error = true;
    			}
    			if(locality == '0'){
    			    jQuery('.text-danger-locality').text('Please Select Locality');
    			    error = true;
    			}
    			if(apartment == '0'){
    			    jQuery('.text-danger-apartment').text('Please Select Apartment');
    				error = true;
    			}
    			if(flat_no == ''){
    			    jQuery('.text-danger-flat_no').text('Please Enter Flat Number');
    				error = true;
    			}
		    }
			if(error){
			    return false;
			}
			else{
			    jQuery.ajax({
			        'type':"POST",
			        'context':this,
			        'url':"<?php echo base_url('check_email_exist'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email},
			        'success': function(data) { 
			            console.log(data);
			            if(data==1){
			                console.log('sdfdsfdsf');
			                jQuery("#is_checkout").val("true");
			                jQuery("#checkout-form").submit();
			               
			            }
			            else{
			                jQuery('.text-danger-email').text('Email address already exists. Please try again.');
			                
			            } 
			    	}   
				});
    			return false;
    		}
		});
    });
</script>
<script>
	jQuery(document).ready(function(){
	    //for address part end
        var country_value = jQuery('#country option:selected', this).val();
        var state_value = jQuery('#state option:selected', this).val();
        var city_value = jQuery('#city').val();
        var locality_value = jQuery('#locality', this).val();
        if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
            jQuery('#state').attr("disabled", true);
            jQuery('#city').attr("disabled", true);
            jQuery('#locality').attr("disabled", true);
            jQuery('#apartment').attr("disabled", true);
        }
        else if(state_value=='' || state_value=='- State -' || state_value=='0'){
            jQuery('#city').attr("disabled", true);
            jQuery('#locality').attr("disabled", true);
            jQuery('#apartment').attr("disabled", true);
        }
        else if(city_value=='' || city_value=='- city -'){
            jQuery('#locality').attr("disabled", true);
            jQuery('#apartment').attr("disabled", true);
        }
        else if(locality_value=='' || locality_value=='- locality -'){
            jQuery('#apartment').attr("disabled", true);
        }
        //for address part end
	    jQuery("#country option:contains('India')").attr('selected', 'selected');
	    var countries = <?php echo json_encode($countries) ?>;
	    if(countries.length > 1){
	        setTimeout(function(){ jQuery("#country").trigger("change"); }, 3000);
	    }
	    jQuery('#country').on('change',function(){
	    	country_value = jQuery('option:selected', this).val();
            var country_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {country : country_id, gre_tokan : token };
            jQuery('#state').find('option').not(':first').remove();
            jQuery('#city').val('');
            jQuery('#locality').val('');
            jQuery('#apartment').val('');
            jQuery('#city_list').html('');
            jQuery('#locality_list').html('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_state'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#state').attr("disabled", true);
                    jQuery('#city').attr("disabled", true);
                    jQuery('#locality').attr("disabled", true);
                    jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var state_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < state_list.length; i++) {
                        var obj = state_list[i];
                        jQuery('#state').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
                        jQuery('#state').attr("disabled", true);
                    }
                    else{
                        jQuery('#state').attr("disabled", false);
                    }
	            }
	        });
	    });
	    jQuery('#state').on('change',function(){
	    	state_value = jQuery(this).val();
	    	var state_value = jQuery('option:selected', this).val();
            var state_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {state : state_id, gre_tokan : token };
            jQuery('#city').find('option').not(':first').remove();
            jQuery('#city').val('');
            jQuery('#locality').val('');
            jQuery('#apartment').val('');
            jQuery('#city_list').html('');
            jQuery('#locality_list').html('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_city'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#city').attr("disabled", true);
                    jQuery('#locality').attr("disabled", true);
                    jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var city_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < city_list.length; i++) {
                        var obj = city_list[i];
                        jQuery('#city_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(state_value=='' || state_value==' - State - ' || state_value=='0'){
                        jQuery('#city').attr("disabled", true);
                    }
                    else{
                        jQuery('#city').attr("disabled", false);
                    }
	            }
	        });
	    }); 
	    jQuery('#city').on('change',function(){
            var city_value = jQuery(this).val();
            var city_id = jQuery('#city_list').find("[value='" + city_value + "']").attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {city : city_id, gre_tokan : token };
            jQuery('#locality').find('option').not(':first').remove();
            jQuery('#locality').val('');
            jQuery('#apartment').val('');
            jQuery('#locality_list').html('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_locality'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#locality').attr("disabled", true);
                    jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var locality_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < locality_list.length; i++) {
                        var obj = locality_list[i];
                        jQuery('#locality_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    
                    if(city_value==''){
                        jQuery('#locality').attr("disabled", true);
                    }
                    else{
                        jQuery('#locality').attr("disabled", false);
                    }
	            }
	        });
	    }); 
	    jQuery('#locality').on('change',function(){
            var locality_value = jQuery(this).val();
            var locality_id = jQuery('#locality_list').find("[value='" + locality_value + "']").attr('data-val');
            var token = jQuery('input[name="gre_tokan"]').val();
            var data = {locality : locality_id, gre_tokan : token };
            jQuery('#apartment').find('option').not(':first').remove();
            jQuery('#apartment').val('');
            jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data: data,
	            url: '<?php echo base_url('get_apartment'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var apartment_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < apartment_list.length; i++) {
                        var obj = apartment_list[i];
                        jQuery('#apartment_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    jQuery('#apartment').attr("disabled", false);
                    if(locality_value==''){
                        jQuery('#apartment').attr("disabled", true);
                    }
	            }
	        });
	    }); 
	    jQuery('#name').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).next().text('');
        });
        jQuery('#email').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).next().text('');
        });
        jQuery("#country").focus(function(){
            var txt = '<p></p>';
            jQuery('.text-danger-country').html(txt);
        });
        jQuery("#state").focus(function(){
            var txt = '<p></p>';
            jQuery('.text-danger-state').html(txt);
            jQuery('.text-danger-city p').css('color','#333');
        });
        jQuery("#city").focus(function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery('.text-danger-city').html(txt);
            jQuery('.text-danger-city p').css('color','#333');
        });
        jQuery("#locality").focus(function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery('.text-danger-locality').html(txt);
            jQuery('.text-danger-locality p').css('color','#333');
        });
        jQuery("#apartment").focus(function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery('.text-danger-apartment').html(txt);
            jQuery('.text-danger-apartment p ').css('color','#333');
        });
        jQuery('#flat_no').on('focus', function(){
            var txt = '<p></p>';
            jQuery(this).parent().removeClass('error');
            jQuery(this).next().text('');
        });
	    
	});
	
	$(function() {
		// Ajax Cart Update Example
		// Submit the cart form if a shipping option select or input element is changed.
		$('select[name^="shipping"], input[name^="shipping"]').on('change', function()
		{
			// Loop through shipping select and input fields creating object of their names and values that will then be submitted via 'post'
			var data = new Object();
			$('select[name^="shipping"], input[name^="shipping"]').each(function()
			{
				data[$(this).attr('name')] = $(this).val();
			});
			
			// Set 'update' so controller knows to run update method.
			data['update'] = true;

			// !IMPORTANT NOTE: As of CI 2.0, if csrf (cross-site request forgery) protection is enabled via CI's config,
			// this must be included to submit the token.
			data['gre_tokan'] = $('input[name="gre_tokan"]').val();

			// $('#cart_content').load('<?php echo current_url();?> #ajax_content', data);


		        $.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo current_url(); ?>',
		            cache: true,
		            beforeSend: function(){
		            },
		            success: function(data){
		            	$('body').html(data)
		                // container.addClass('animated zoomOut');
		                // setTimeout(function(){
			               //  container.remove();
		                // }, 550);
		            },
		            complete: function(data){
		            	$('body').append(data)
		            }
		        });
		});
	});
</script>
<?php endif ?>

<?php // Display saved cart session for user to load or delete ?>
<div class="modal fade" id="load-cart-modal" tabindex="-1" role="dialog" aria-labelledby="load-cart-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="panel panel-default panel-inverse" style="margin:0">
				<div class="panel-heading">
					<strong>LOAD SAVED CART</strong>
					<button type="button" class="close" data-dismiss="modal">
	                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	                </button>
				</div>
				<?php if ($saved_cart_data): ?>
					<table class="table">
						<tbody>
							<?php foreach($saved_cart_data as $row): ?>
								<tr>
									<td>
										Saved on : 
										<?php echo date('jS M Y @ H:i', strtotime($row[$this->flexi_cart->db_column('db_cart_data','date')])); ?>
									</td>
									<td class="text-right">
										<a href="<?php echo site_url('load_cart_data/'.$row[$this->flexi_cart->db_column('db_cart_data','id')]) ?>" class="btn btn-xs btn-primary">
											Load
										</a>
										<a href="<?php echo site_url('delete_cart_data/'.$row[$this->flexi_cart->db_column('db_cart_data','id')]) ?>" class="btn btn-xs btn-danger">
											Delete
										</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				<?php else: ?>
					<div class="alert alert-warning" style="margin:0">
						<?php if ($user): ?>
							There are currently no saved carts to load.
						<?php else: ?>
							You must be logged in to load saved carts.
							<?php echo anchor('login', 'Login', 'class="alert-link"') ?>
						<?php endif ?>
					</div>
				<?php endif ?>
				<div class="panel-footer">
					<small>Note: Only saved carts for orders that have not been completed are listed.</small>
				</div>
			</div>
        </div>
    </div>
</div>
</div></div>
<style type="text/css">
	input[type=number]::-webkit-inner-spin-button {
  opacity: 1;
}
</style>
<?php $this->load->view('public/templates/footer') ?>