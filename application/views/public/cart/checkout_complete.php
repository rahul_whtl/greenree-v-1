<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Checkout Complete',
	'link' => 'cart'
)) ?>

<!-- <ul class="breadcrumb">
	<li>
		<?php //echo anchor('cart', 'Shopping Cart') ?>
	</li>
	<li class="active">Checkout</li>
</ul> -->
<div class="checkout-complete">
	<div class="container">
<h1 class="lead text-center text-success">Order Confirmation</h1>
<?php //echo $order_number?>
<div class="alert alert-success">
	<div class="text-left">
	    <div class="alert-success-heading">
	        <p>The Order has been placed successfully. Here are your order details : </p>
	    </div>
    	<div class="col-md-3">
    		Order No. : <?php echo anchor('user_dashboard/my-orders/'.$order_number, $order_number) ?>
    	</div>
    	<div class="col-md-3">
		    Total Price : &#x20B9; <?php echo number_format( (float) $total_price , 2, '.', '');?>
    	</div> 
	    <div class="col-md-3">
		    Debited Wallet Cash : &#x20B9; <?php echo number_format( (float) $debit_reward_point , 2, '.', '');?>
		</div>
		<div class="col-md-3">
		    Balance Due : &#x20B9; <?php echo number_format( (float) $cash_collect , 2, '.', '');?>
	    </div>
	    <div class="text-center thanks-part">Thank you for your business.</div>
    </div>
</div>

<?php if ($products): ?>
	<h4 class="page-header">
	    Continue Shopping
	</h4>
	<?php $this->load->view('public/products/products_tiles_view', array(
		'type' => 'tiles',
		'cols' => 'col-xs-6 col-sm-4 col-md-3 col-lg-2-5',
		'products' => $products,
	)) ?>
<?php endif ?>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>