
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5">
     <tr>
      <td align="center" valign="top" bgcolor="#f5f5f5">
        <!-- 600px container (white background) -->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 640px;
                 max-width: 600px;background-color: #fff;">
            <tr>
            	<td style="padding-left: 20px;border-bottom: 1px solid #CCC; width:45%">
            		<img src="<?php echo base_url('assets/images/icons/greenree-logo.png')?>" style="max-width:100%;"/>
            	</td>
            	<td style="padding-right: 20px;border-bottom: 1px solid #CCC;
    padding-top: 20px;padding-bottom: 20px;font-family: Arial; font-size:14px;    height: 30px;vertical-align: middle;line-height: 30px;text-align: right; width: 55%;">
            		<span style="margin-right: 10px;">Follow Us On : </span> <a href="https://www.facebook.com/greenree.rekartnow.3" target="_blank" style="float: right;"><img src="<?php echo base_url('assets/images/icons/rsz_facebook-7-xxl.png')?>" style="float: right;"></a>
            	</td>            	
            </tr>
            <tr>
            	<td colspan="2" style="padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px">
					<!-- <h1><?php //echo sprintf(lang('email_forgot_password_heading'), $identity);?></h1> -->
					<p>Hi <?php echo $name.',';?></p>
					<p><?php echo sprintf(lang('email_forgot_password_subheading'), anchor('reset_password/'. $forgotten_password_code, lang('email_forgot_password_link')));?></p>
					<br />
					<p>Regards,<br/>GreenREE</p>
				</td>
            </tr>
            <tr>
            	<td bgcolor="#333" colspan="2" height="50" style="text-align: center;font-family: Arial; font-size:11px;">
            	    <span style="padding-left: 0;padding-right: 5px;"><a href="<?php echo base_url('sell-scrap') ?>" style="color: #fff;text-decoration: none;font-size:11px">SELL SCRAP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('shop') ?>" style="color: #fff;text-decoration: none;;font-size:11px">SHOP</a></span>
            	    <span style="padding-left: 10px;padding-right: 5px;"><a href="<?php echo base_url('pre-owned-items') ?>" style="color: #fff;text-decoration: none;font-size:11px">PRE-OWNED ITEMS</a></span>
            	    <span style="padding-left: 10px;padding-right: 0;"><a href="<?php echo base_url('wishlist') ?>" style="color: #fff;text-decoration: none;font-size:11px">WISHLIST</a></span>
                </td>
            </tr>
            <tr>
            	<td bgcolor="#25a55b" colspan="2" height="30" style="font-family: Arial; font-size:12px; text-align: center; color: #fff">© 2019 GreenREE Pvt Ltd. All Copyright  Reserved.</td>
            </tr>
           </table>
       </td>
      </tr>
</table>                