	<?php $this->load->view('public/templates/header', array(
		'title' => 'GreenREE - Login',
		'link' => 'login'
	)) ?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
	<div class="login-form-box" ng-app="validationApp">
		<div class="container">
		<!-- login with otp part is start -->
			<div ng-controller="mainController">
				<?php echo form_open('/login_otp',array("ng-submit"=>"submitForm()","name"=>"userForm")); ?>
				<div class="col-md-5">
					<div class="row user_login">
						<div class="col-md-12 logi-otp-div" style="margin:0 0 20px 0px;">
							<h3>Login with Mobile Number </h3>
							<?php if ($this->session->flashdata('msg')): ?>
				            <div class="alert alert-danger">Verify Your Email Before Login.</div>
				       		<?php endif ?>
				       		<?php if ($this->session->flashdata('msg1')): ?>
				            <div class="alert alert-danger">Incorrect OTP.</div>
				       		<?php endif ?>
				       		<?php if ($this->session->flashdata('msg2')): ?>
				            <div class="alert alert-danger">Incorrect OTP.</div>
				       		<?php endif ?>
				       		<?php if ($this->session->flashdata('msg3')): ?>
				            <div class="alert alert-danger">Incorrect Contact Number.</div>
				       		<?php endif ?>
							<input type="text" name="sell_now" value="<?php echo $sell_now;?>" hidden>
						</div>
						<div class="col-md-12" ng-class="{ 'has-error' : userForm.contact_number.$invalid && !userForm.contact_number.$pristine }" style="margin:0 0 20px 0px">
							<div class="form-group form-group-number">
								<label class="control-label">Mobile Number<span class="red">*</span></label>
								<input type="text" class="form-control input-lg" id="contact_number" name="contact_number" ng-model="user.contact_number" ng-minlength="10" ng-maxlength="10" value="" ng-pattern="/^[0-9]*$/" required />
								<p ng-show="userForm.contact_number.$invalid  && !userForm.contact_number.$pristine">Please Enter the Valid Contact Number.</p>
								<div class="text-danger text-danger-number"></div>
							</div>
						</div>	
						<div class="col-md-12" style="margin:0 0 20px 0px;">
							<div class="form-group" id="otp">
								<input type="button" name="login_user_otp" id="genrate_otp" value="Genrate OTP" class="btn btn-lg btn-primary " ng-disabled="userForm.$invalid"  />
								<div class="text-danger"></div>
							</div>
						</div>
						<div class="col-md-12 text-center login_otp" style="margin:0 0 20px 0px">
						</div>	
					</div>
				</div>
				<?php echo form_close();?>
			</div>
		<!-- login with otp part is end -->
			<div class="col-md-2"><span class="or"></span></div>
				<divng-controller="newController">
				<?php echo form_open(current_url(), array("class"=>"text-left login-form","ng-submit"=>"submitForm1()","name"=>"userForm1")); ?>
					<div class="col-md-5">
						<div class="row user_login">
							<div class="col-md-12" style="margin:0 0 20px;">
								<h3>Login</h3>
								<input type="text" name="sell_now" value="<?php echo $sell_now;?>" hidden> 
							</div>
							<div class="col-md-12" ng-class="{ 'has-error' : userForm1.username.$invalid && !userForm1.username.$pristine }">
								<div class="form-group form-group-username <?= form_error('username') ? 'has-error' : '' ?>">
									<label class="control-label">Email Address<span class="red">*</span></label>
									<input type="email" class="form-control input-lg" id="username" name="username" ng-model="user.username" ng-pattern="email-format" value="<?php echo set_value('username') ?>" required/>
									<span ng-show="userForm1.username.$invalid && !userForm1.username.$pristine">Enter valid email.</span>
									<div class="text-danger text-danger-username "><?= form_error('username') ? form_error('username') : '' ?></div>
								</div>
							</div>
							<div class="col-md-12" ng-class="{ 'has-error' : userForm1.password.$invalid && !userForm1.password.$pristine }">
								<div class="form-group form-group-password <?= form_error('password') ? 'has-error' : '' ?>" id="password_otp">
									<label class="control-label">Password<span class="red">*</span></label>
									<input type="password" class="form-control input-lg form-group-password-otp" name="password" id="password" ng-model="user.password" value="<?php echo set_value('password') ?>" ng-minlength="8"  required/>
									<span ng-show="userForm1.password.$invalid && !userForm1.password.$pristine">Enter at least 8 characters.</span>
									<div class="text-danger text-danger-password"><?= form_error('password') ? form_error('password') : '' ?></div>
								</div>
							</div>
							<div class="col-md-12 row" style="padding-right: 0;">
								<div class="col-md-5 lgn-btn">
									<div class="checkbox" style="margin-top:0">
										<!--<label for="remember_me" class="login-remember-me">
											<?php echo form_checkbox('remember_me', '1', set_value('remember_me') ? TRUE : FALSE) ?>
											Remember Me
										</label>-->
										<div class="form-group">
											<input type="submit" name="login_user" id="submit" value="Login" class="btn btn-lg btn-primary" ng-disabled="userForm1.$invalid"/>
										</div>
									</div>
								</div>
								<div class="col-md-5" style="padding-right: 0; float: right;">
									<div class="form-group text-right">
										<?php echo anchor('forgot_password', 'Forgot your password?') ?>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	<?php// else: ?>
	<!-- Registration part start -->
	<div class="registration hide">
		<div class="container">
			<?php
			$attributes = array('class' => 'register-form', 'id' => 'register-form');
			echo form_open_multipart('register', $attributes); ?>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row new-registration">
				    <div class="col-md-12" style="margin:0 0 20px;">
						<h3>Create New Account</h3>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<div class="form-group form-group-name<?= form_error('first_name') ? 'has-error' : '' ?>">
							<label class="control-label first_name" for="first_name">Full Name<span class="red">*</span></label>
							<input class="form-control first_name1 input-lg" type="text" name="first_name" />
							<div class="text-danger text-danger-name"><?= form_error('first_name') ? form_error('first_name') : '&nbsp' ?></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<div class="form-group form-group-phone">
							<label class="control-label" for="phone">Phone Number<span class="red">*</span></label>
							<input class="form-control input-lg" id="verify_phone" type="phone" name="phone" />
						<div class="text-danger text-danger-phone"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<div class="form-group verify_number_part">
							<input class="btn btn-lg btn-primary verify_number" type="button" id="verify_number" value="Verify Number" />
							<div class="text-danger"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<div class="form-group form-group-email <?= form_error('email') ? 'has-error' : '' ?>">
							<label class="control-label" for="email">Email Address<span class="red">*</span></label>
							<input class="form-control reg_email input-lg" type="email" name="email" value="<?= set_value('email') ?>"/>
							<div class="text-danger text-danger-email"><?= form_error('email') ? form_error('email') : '&nbsp' ?></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<input type="submit" name="create_user" id="create_user" value="Register my Account" class="btn btn-lg btn-primary create_user" disabled/>
					</div>
					
					<input type="hidden" name="create_user" value="create_user"/>
				</div>
				</div>
			</div>
	        <?php echo form_close();?>
		</div>
	</div>
	<!-- Registration part end -->
	
	<div class="alert-box alert-box-login">
		<div class="container">
			<div class="alert-inbox-container">
				<div class="notification_div">
				<div class="alert alert-info alert-inbox" role="alert">
					<span>If you do not have an Account, Please click on button
					to create an account.  <input type="submit" name="login_user" id="registration-page-link" value="Create Account" class="btn btn-md btn-primary registration-page-link"/></span>
				</div></div>
			</div>
		</div>
	</div>
	
	<hr> 

<script>
var validationApp = angular.module('validationApp', []);
// create angular controller
validationApp.controller('mainController', function($scope) {
	// function to submit the form after all validation has occurred			
	$scope.submitForm = function() {
		// check to make sure the form is completely valid
		if ($scope.userForm.$valid) {
			//alert('our form is amazing');
		}
	};
});
// create angular controller
validationApp.controller('newController', function($scope) {
	// function to submit the form after all validation has occurred			
	$scope.submitForm1 = function() {
		// check to make sure the form is completely valid
		if ($scope.userForm1.$valid) {
			alert('our form is amazing');
		}
	};
});
</script>
	<script type="text/javascript">
	    function validateEmail(email) {
	       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	       return re.test(String(email).toLowerCase());
	    }
	    
		jQuery(document).ready(function(){
			jQuery("body").on('click', '#login-page-link',function(){
				var tag = '<p>If you do not have an Account, Please click on button				to create an account.  <input type="submit" name="login_user" id="registration-page-link" value="Create Account" class="btn btn-md btn-primary registration-page-link"/></p>';
				jQuery('.first_name1,.reg_email,#otp1,#verify_phone').val('');
				jQuery(".login-form-box").removeClass("hide");
				jQuery(".registration").addClass("hide");
				jQuery(".alert-info").html(tag);            
			});
			jQuery("body").on('click', ".registration-page-link,.create_new_account",function(){
			    var contact_number = jQuery('#contact_number').val();
				var tag = '<p>If you have already an Account, Please click on button				to Log in.  <input type="submit" id="login-page-link" value="Login" class="btn btn-md btn-primary login-page-link"/></p>';
				jQuery('#contact_number,#username,#password_otp input,#otp').val(''); 
				jQuery('#verify_phone').val(contact_number);
				jQuery(".login-form-box").addClass("hide");
	            jQuery(".registration").removeClass("hide");
				jQuery(".alert-info").html(tag);
			}); 
			 
			jQuery("#submit").on('click',function(){ 
			    
			 //    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				// var email = jQuery.trim(jQuery('#username').val());
				// var password = jQuery.trim(jQuery('#password').val());
				// var error = false;
				
				// jQuery('.text-danger').html('');
				// if(email == '') {
				// 	jQuery(".form-group-username").addClass("has-error");
				// 	var p = "<p>Please Enter Your Email ID</p>";
				//     jQuery(".text-danger-username").html(p);	
				//     error = true;
				// }
				// if(!re.test(String(email).toLowerCase())){
				// 	jQuery(".form-group-username").addClass("has-error");
				// 	var p = "<p>Please Enter Valid Email ID</p>";
				//     jQuery(".text-danger-username").html(p);	
				//     error = true;
				// }
				// if(password == ''){
				// 	jQuery(".form-group-password").addClass("has-error");
				// 	var p = "<p>Please Enter Your Password</p>";
				//     jQuery(".text-danger-password").html(p);	
				//     error = true;
				// }
                
				//if(error){
				//	return false;
				//}else{
				    jQuery('.login-form').submit();    
				//}
				
			    
			});
			 
	        jQuery("#submit_otp").click(function(){
	        	var contact_number = jQuery('#contact_number').val();
	        	var otp = jQuery('#otp').val();alert(otp);alert(contact_number);return;
	        	var tag = '<label class="control-label">Enter your OTP which is sent on your registered mobile number :</label>	<input type="text" class="form-control input-lg" name="otp" />';
				jQuery("#password_otp").html(tag);     
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('login_otp'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp },
			        'success' : function(data) {              
			            alert('otp sent to your number'+data);
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			});

	        
	        jQuery("body").on('click',"#genrate_otp,#resend_otp",function(){
	        	var contact_number = jQuery('#contact_number').val();
	        	// var pattern = /^\d+$/;
	        	// var length = contact_number.toString().length;
	        	// if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
	        	// 	jQuery(".form-group-number").addClass("has-error");
	        	// 	var p = "<p>Please Enter the Valid Contact Number.</p>";
	         //        jQuery(".text-danger-number").html(p);
	        	// 	return; 
	        	// }          
				jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('otp_generation'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
		        success : function(data) {
		        	var data = JSON.parse(data);
		        	if(data.results == 2 || data == 2)
		        	{
		        		jQuery(".form-group-number").addClass("has-error");
		        		var p = "<p>Number is not Registered.<strong class='create_new_account' style='color:#26A65B;cursor:pointer'> Create New Account</strong></p>"
		                jQuery(".text-danger-number").html(p);
		        		return; 
		        	}
		        	else{
						var tag = '<div class="row otp-resend_btn"><div class="col-md-5"><label class="control-label">Enter OTP:</label>		<input type="text" class="form-control input-lg" id="otp" name="otp" /></div><div class="col-md-5"><input type="button" name="resend_otp" id="resend_otp" value="Resend OTP" class="btn btn-lg btn-primary" style="margin-left: 40px; margin-top:28px;" /></div></div>';
						var tag1 = '<div class="form-group"><input type="submit" name="user_otp_verification" id="submit_otp" value="Login With OTP" class="btn btn-lg btn-primary" />		</div>';
						jQuery(".form-group-number").removeClass("has-error");
			            jQuery(".text-danger-number").removeClass("has-error");	
			            jQuery(".text-danger-number p").remove(); 
			        	jQuery("#otp").html(tag); 
						jQuery(".login_otp").html(tag1);
						alert(data.otp);
		        	}
		        },
		        error : function(request,error)
		        {
		            alert("Request : "+JSON.stringify(request));
		        }
		    });
			});

	        //genrate otp for registration
	        jQuery("#verify_number").click(function(){
	        	var contact_number = jQuery('#verify_phone').val();
	        	var name =  jQuery('.first_name1').val();
	        	var pattern = /^\d+$/;
	        	var pattern_name = /^[a-zA-Z ]*$/;//alert(pattern_name.test(name));
	        	var length = contact_number.toString().length;
	        	if (name == '' || pattern_name.test(name) == false) {
				    jQuery(".form-group-name").addClass("has-error");
	        		var p = "<p>Please Enter Only Character.</p>"
	                jQuery(".text-danger-name").html(p);
	                if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
		        		jQuery(".form-group-phone").addClass("has-error");
		        		var p = "<p>Please Enter Valid Contact Number.</p>"
		                jQuery(".text-danger-phone").html(p);
		        		return; 
		        	}
		        	return;
				}
	        	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
	        		jQuery(".form-group-phone").addClass("has-error");
	        		var p = "<p>Please Enter Valid Contact Number.</p>"
	                jQuery(".text-danger-phone").html(p);
	                if (!name || pattern_name.test(name) == false) {
					    jQuery(".form-group-name").addClass("has-error");
		        		var p = "<p>Please Enter Only Character.</p>"
		                jQuery(".text-danger-name").html(p);
			        	return;
					}
	        		return; 
	        	}
				var tag = '<div class="row"><div class="col-md-5 form-group-reg-otp-div"><label class="control-label">Enter OTP:</label><input type="text" class="form-control input-lg form-group-reg-otp" id="otp1" name="otp" /><div class="text-danger text-danger-reg-otp"></div></div><div class="col-md-3"><input type="button" name="verify_register_otp" id="verify_register_otp" value="Verify OTP" class="btn btn-lg btn-primary" style="margin-left: 15px; margin-top:27px;" /></div><div class="col-md-4"><input type="button" name="reg_resend_otp" id="reg_resend_otp" value="Resend OTP" class="btn btn-lg btn-primary" style="margin-left: 28px; margin-top:27px;" /></div></div>';

				jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('mobile_verification'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
		        success : function(data) {
		        	var data = JSON.parse(data);
		        	console.log(data);
		        	if(data.results == "Number is Invalid.")
		        	{
		        		jQuery(".form-group-phone").addClass("has-error");
		        		var p = "<p>Number is Invalid.</p>"
		                jQuery(".text-danger-phone").html(p);
		        		return; 
		        	}
		        	if(data.results==1){
		        		jQuery(".form-group-phone").addClass("has-error");
		        		var p = "<p>Phone Number is Already Registered.</p>"
		        		jQuery(".text-danger-phone").html(p);
		        		return;
		        	}
		        	alert(data.otp);
		            jQuery(".form-group-phone").removeClass("has-error");
		            jQuery(".text-danger-phone").removeClass("has-error");	
		            jQuery(".text-danger-phone p").remove(); 
		        	jQuery(".verify_number_part").html(tag); 
		        },
		        error : function(request,error)
		        {
		            alert("Request : "+JSON.stringify(request));
		        }
		    });
			}); 
			//genrate otp for registration

	        jQuery("body").on('click',"#reg_resend_otp",function(){
	        	var contact_number = jQuery('#verify_phone').val();
	        	var name =  jQuery('.first_name1').val();
	        	var pattern = /^\d+$/;//alert('hi');
	        	var pattern_name = /^[a-zA-Z ]*$/;
	        	var length = contact_number.toString().length;
	        	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
	        		jQuery(".form-group-phone").addClass("has-error");
	        		var p = "<p>Please Enter Valid Contact Number.</p>"
	                jQuery(".text-danger-phone").html(p);
	        		return; 
	        	}
				if (name == '' || pattern_name.test(name) == false) {
				    jQuery(".form-group-name").addClass("has-error");
	        		var p = "<p>Please Enter Only Character.</p>"
	                jQuery(".text-danger-name").html(p);
	                return;
				}
	        	jQuery.ajax({
					type : 'POST',
			        url : "<?php echo base_url('mobile_verification'); ?>",
			        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
			        success : function(data) {
			        	var data = JSON.parse(data);
			        	if(data.results == "Number is Invalid.")
			        	{
			        		jQuery(".form-group-phone").addClass("has-error");
			        		var p = "<p>Number is Invalid.</p>"
			                jQuery(".text-danger-phone").html(p);
			        		return; 
			        	}
			        	if(data.results==1){
			        		jQuery(".form-group-phone").addClass("has-error");
			        		var p = "<p>Phone Number is Already Registered.</p>"
			        		jQuery(".text-danger-phone").html(p);
			        		return;
			        	}
			        	alert(data.otp);
			        },
			        error : function(request,error)
			        {
			            alert("Request : "+JSON.stringify(request));
			        }
			    });
	        });	
	        //verify otp for registration part   
			jQuery("body").on('click', '#verify_register_otp', function(){
	        	var contact_number = jQuery('#verify_phone').val();
	        	var otp = jQuery('#otp1').val();
	        	var pattern = /^\d+$/;
	        	var length = otp.toString().length;
	        	if(!otp || !(pattern.test(otp)) || length<4 || length >4){
	        		jQuery(".form-group-reg-otp-div").addClass("has-error");
	        		var p = "<p>Please Enter Coreect OTP Number.</p>"
	                jQuery(".text-danger-reg-otp").html(p);
	        		return; 
	        	}
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('verify_register_otp'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp },
			        'success' : function(data) {  
			        	if (data == 1) {
			        		tag = "<div class='alert alert-success'><p><strong>Success!</strong> Your Phone is Verified Successfully.</p></div>";
					        jQuery(".form-group-phone").removeClass("has-error");
				            jQuery(".text-danger-phone").removeClass("has-error");	
				            jQuery(".text-danger-phone p").remove(); 
				        	jQuery(".verify_number_part").html(tag); 
			                jQuery(".create_user").attr("disabled", false);
			        	}
			        	if (data == 2) {
			        		jQuery(".form-group-reg-otp-div").addClass("has-error");
			        		var p = "<p>OTP is Expired.</p>"
			        		jQuery(".text-danger-reg-otp").html(p);
			        		return;
			        	}
			        	if (data == 3) {
			        		jQuery(".form-group-reg-otp-div").addClass("has-error");
			        		var p = "<p>Insert Correct OTP.</p>"
			        		jQuery(".text-danger-reg-otp").html(p);
			        		return;
			        	}
			        	if (data == 4) {
			        		jQuery(".text-danger-reg-otp").addClass("has-error");
			        		var p = "<p>Error.</p>"
			        		jQuery(".text-danger-reg-otp").html(p);
			        		return;
			        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			});
	    
	        
			jQuery("body").on('click', '#create_user', function(e){
			    e.preventDefault();
	        	var email = jQuery('.reg_email').val();
	        	var first_name = jQuery('.first_name1').val();//alert(first_name);
	        	if (email == '') {
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please Enter Your Valid Email Id.</p>"
	                jQuery(".text-danger-email").html(p);
	                return;
				}else if(!validateEmail(email)){
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please Enter Valid Email Id.</p>"
	                jQuery(".text-danger-email").html(p);
	                return;
				}
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('check_email'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email },
			        'success' : function(data) {  
			        	if (data == 1) {
			        		jQuery(".form-group-email").addClass("has-error");
			        		var p = "<p>Email is Already Exists.</p>"
			        		jQuery(".text-danger-email").html(p);
			        		return;
			        	}
			        	if (data == 2) {//alert("hi");
			        		jQuery(".form-group-email").removeClass("has-error");
			        		var p = "<p></p>"
			        		jQuery(".text-danger-email").html(p);
			        		var email 		= jQuery('.reg_email').val();
			        		var phone 		= jQuery('#verify_phone').val();
			        		var first_name  = jQuery('.first_name1').val();
			        		var create_user = 'create_user';
			        		
			        		jQuery('#register-form').submit();
			        		//fromRregister(email,phone,first_name,create_user);
			        		/*var tag         = '<div class="alert alert-info alert-inbox" role="alert">				<span>If you do not have an Account, Please click on button	to create an account.  <input type="submit" name="login_user" id="registration-page-link" value="Create Account" class="btn btn-md btn-primary registration-page-link"/></span></div>';
							jQuery(".alert").html(tag);*/
			        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			});

			function fromRregister(email,phone,first_name,create_user){
			    jQuery.ajax({
			        'type':"POST",
			        'url':"<?php echo base_url('register'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email,phone:phone,first_name:first_name,create_user:create_user },
			        'success': function(resp) { 
			        	window.location.href = "<?php echo base_url('login'); ?>"; 
			    	}   
				});
			}
			jQuery('.first_name1').on('focus', function(){
			   jQuery('.form-group-name').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-name').html(p);
			});
			jQuery('#verify_phone').on('focus', function(){
			   jQuery('.form-group-phone').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-phone').html(p);
			});
			jQuery('.reg_email').on('focus', function(){
			   jQuery('.form-group-email').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-email').html(p);
			});
			jQuery("body").on('focus', '.form-group-reg-otp', function(){
			   jQuery('.form-group-reg-otp-div').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-reg-otp').html(p);
			});
			jQuery('#username').on('focus', function(){
			   jQuery('.form-group-username').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-username').html(p);
			});
			jQuery('.form-group-password-otp').on('focus', function(){
			   jQuery('#password_otp').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-password').html(p);
			});
			jQuery('#contact_number').on('focus', function(){
			   jQuery('.form-group-number').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-number').html(p);
			});
	    });
	</script>

	<?php $this->load->view('public/templates/footer') ?>