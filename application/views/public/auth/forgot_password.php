<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Forgot Password',
	'link' => 'login'
)) ?>
<div class="forget-password">
	<div class="container">
		<div class="page-header">
			<h4 class="lead">Forgot My Password</h4>
			<p>
				We will send you and email with further instructions on how to change your password.
			</p>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Password Reset</h4>
					</div>
					<div class="panel-body">
						<?php echo form_open(current_url(), 'class="form-inline forgot-password-form"'); ?>
							<div class="form-group">
								<label for="identity">Email Address<span class="red">*</span></label>
							</div>
							<div class="form-group forget-password-email">
								<input type="text" class="forget-password-email-identity form-control form-control-lg" id="identity" name="identity" value="<?php echo (!empty($email))?$email:set_value('identity') ?>" />
							</div>
							
							<div class="form-group">
								<input type="button" id="submit1" value="Send Email" class="btn btn-primary submit1"/>
								<input type="submit" name="login_user" id="send_email" value="Send Email" class="btn btn-primary submit hide"/>
							</div>
							
            	    		<!--otp part-->
            	    		<br>
                        	<div class="form-group forget-password-otp-text hide">
                        		<label class="control-label">OTP:</label>
                        		<input type="text" name="new-otp" id="new-otp" class="form-control">
                        		<input type="button" id="forget-password-otp-submit" class="btn  btn-primary forget-password-otp-submit" value="Verify OTP">
                        		<input type="button" name="forget-password-otp-resend" id="forget-password-otp-resend" class="btn btn-primary forget-password-otp-resend" value="Resend OTP">
                        	</div>
            	    		<!--otp part end-->
						<?php echo form_close(); ?>
						    <div class='text-danger-email'></div>
						    <?php if($this->session->flashdata('alert') != '') {
              					$alert = $this->session->flashdata('alert'); ?>
                				<div class="has-error" style="color:#FF3F5F">
                					<?php echo $alert['message'] ?>
                				</div>
            	    		<?php }	?>
					</div>
				</div>
			</div>
			<!--<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Password Tips</h4>
					</div>
					<div class="panel-body">
						<ul class="row">
							<li>
								<strong>Use a strong password.</strong>
								8 Characters and above, we recommend a mixture of alpha-numeric characters or symbols
							</li>
							<li>
								<strong>Memorize your password.</strong>
								Never write it down, we recommend logging in as much as possible without checking the "remember me".
							</li>
						</ul>
					</div>
				</div>
			</div>-->
		</div>
	</div>
</div>
<script type="text/javascript">
    function validateEmail(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(String(email).toLowerCase());
    }
    jQuery(document).ready(function(){
        jQuery("body").on('click',".forget-password #submit",function(event){
            event.preventDefault();
            var email = jQuery('.forget-password-email-identity').val();//alert(email);return false;
            if(email == ''){
                var txt = '<p>Please Enter Your Correct Email Address.</p>';
                jQuery('.forget-password-email').addClass('has-error');
				jQuery('.text-danger-email').html(txt);
				jQuery('.text-danger-email').css('color','#FF3F5F');
				return false;
			}else if(!validateEmail(email)){
			 	var txt = '<p>Please Enter Your Correct Email Address.</p>';
			 	jQuery('.forget-password-email').addClass('has-error');
				jQuery('.text-danger-email').html(txt);
				jQuery('.text-danger-email').css('color','#FF3F5F');
				return false;
			}
			else{
			    var txt = '<p></p>';
			 	jQuery('.forget-password-email').removeClass('has-error');
				jQuery('.text-danger-email').html(txt);
				jQuery('.text-danger-email').css('color','#FFFFFF');
				//jQuery('.forgot-password-form').submit();
				//return true;
			}
			
		});		
		jQuery(".forget-password .forget-password-email input").focus(function(){
		    var txt = '';
        	jQuery('.forget-password-email').removeClass('has-error');
        	jQuery('.text-danger-email').css('color','#555;');
        	jQuery('.text-danger-email').html(txt);
		});
			//verify otp
		jQuery("body").on('click',"#forget-password-otp-submit",function(event){
        	event.preventDefault();
        	var email = jQuery('#identity').val();
        	var otp = jQuery('#new-otp').val();
			jQuery.ajax({
    			type : 'POST',
    	        url : "<?php echo base_url('forgot_password_verify_otp'); ?>",
    	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email,otp:otp},
    	        success : function(data) {
    	        	if (data == 1) {
		        		$("#send_email").click();
		        	}
		        	if (data == 2) {
		        	    jQuery('#new-otp').val('');
		        		tag = "<p>OTP has expired.</p>";
			        	jQuery(".text-danger-email").html(tag); 
			        	jQuery(".text-danger-email p").css('color','#FF0000'); 
			        	//jQuery(".forget-password-otp-text").addClass("hide");
		        		return false;
		        	}
		        	if (data == 3) {
		        	    jQuery('#new-otp').val('');
		        		tag = "<p>Incorrect OTP.</p>";
			        	jQuery(".text-danger-email").html(tag);
			        	jQuery(".text-danger-email p").css('color','#FF0000');
			        	//jQuery(".forget-password-otp-text").addClass("hide");
		        		return false;
		        	}
		        	if (data == 4) {
		        	    jQuery('#new-otp').val('');
		        		tag = "<p>Email address does not exists.</p>";
			        	jQuery(".text-danger-email").html(tag);
			        	jQuery(".text-danger-email p").css('color','#FF0000');
			        //	jQuery(".forget-password-otp-text").addClass("hide");
		        		return false;
		        	}
    	        },
    	        error : function(request,error)
    	        {
    	            alert("Request : "+JSON.stringify(request));
    	        }
    	    });
    	});
    //	resend and send otp
    	jQuery("#submit1,#forget-password-otp-resend").click(function(){
        	var email = jQuery('#identity').val();
            if(email == ''){
                var txt = '<p>Please enter email address.</p>';
                jQuery('.forget-password-email').addClass('has-error');
				jQuery('.text-danger-email').html(txt);
				jQuery('.text-danger-email').css('color','#FF3F5F');
				return false;
			}else if(!validateEmail(email)){
			 	var txt = '<p>Please enter correct email address.</p>';
			 	jQuery('.forget-password-email').addClass('has-error');
				jQuery('.text-danger-email').html(txt);
				jQuery('.text-danger-email').css('color','#FF3F5F');
				return false;
			}
			else{
			    var txt = '<p></p>';
			 	jQuery('.forget-password-email').removeClass('has-error');
				jQuery('.text-danger-email').html(txt);
				jQuery('.text-danger-email').css('color','#FFFFFF');
				//jQuery('.forgot-password-form').submit();
				//return true;
			}
	        jQuery.ajax({
		        'type' : 'POST',
		        'url' : "<?php echo base_url('forgot_password_send_otp'); ?>",
		        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email },
		        'success' : function(data) {     
		            var result =JSON.parse(data);
		            console.log(data);
		            if (result['result'] == 1) {
		                //alert(result['otp']);
		        		tag = "<p>OTP has been sent to your linked mobile number. Please Verify OTP to recieve reset password mail.</p>";
			        	jQuery(".text-danger-email").html(tag); 
			        	jQuery(".forget-password-otp-text").attr("disabled", false);
			        	jQuery(".text-danger-email p").css('color','#26A65B');
		                jQuery(".forget-password-otp-text").removeClass("hide");
		        	}
		        	if (result['result'] == 2) {
		        		tag = "<p>Error.</p>";
			        	jQuery(".text-danger-email").html(tag); 
			        	jQuery(".text-danger-email p").css('color','#FF0000');
			        	jQuery(".forget-password-otp-text").addClass("hide");
		        		return;
		        	}
		        	if (result['result'] == 3) {
		        		tag = "<p>Email address does not exists.</p>";
			        	jQuery(".text-danger-email").html(tag);
			        	jQuery(".text-danger-email p").css('color','#FF0000');
			        	jQuery(".forget-password-otp-text").addClass("hide");
		        		return;
		        	}
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });
		});
	});
</script>
<?php $this->load->view('public/templates/footer') ?>