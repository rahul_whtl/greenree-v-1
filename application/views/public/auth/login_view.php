	<?php $this->load->view('public/templates/header', array(
		'title' => 'GreenREE - Login',
		'link' => 'login'
	)) ?>

	<div class="login-form-box">
		<div class="container">
		<!-- login with otp part is start -->
			<?php echo form_open('/login_otp'); ?>
			<div class="col-md-5">
				<div class="row user_login">
					<div class="col-md-12 logi-otp-div" style="margin:0 0 20px 0px;">
						<h3>Login with Mobile Number </h3>
						<?php if ($this->session->flashdata('msg')): ?>
			            <div class="alert alert-danger">Verify Your Email Before Login.</div>
			       		<?php endif ?>
			       		<?php if ($this->session->flashdata('msg1')): ?>
			            <div class="alert alert-danger">Incorrect OTP.</div>
			       		<?php endif ?>
			       		<?php if ($this->session->flashdata('msg2')): ?>
			            <div class="alert alert-danger">Incorrect OTP.</div>
			       		<?php endif ?>
			       		<?php if ($this->session->flashdata('msg3')): ?>
			            <div class="alert alert-danger">Incorrect Contact Number.</div>
			       		<?php endif ?>
						<input type="text" name="sell_now" value="<?php echo $sell_now;?>" hidden>
					</div>
					<div class="col-md-12" style="margin:0 0 20px 0px">
						<div class="form-group form-group-number">
							<label class="control-label">Mobile Number<span class="red">*</span></label>
							<input type="text" class="form-control input-lg" id="contact_number" name="contact_number" value="" />
							<div class="text-danger text-danger-number"></div>
						</div>
					</div>	
					<div class="col-md-12" style="margin:0 0 20px 0px;">
						<div class="form-group" id="otp">
							<input type="button" name="login_user_otp" id="genrate_otp" value="Send OTP" class="btn btn-mg btn-primary "  />
							<div class="text-danger"></div>
						</div>
					</div>
					<div class="col-md-12 text-center login_otp" style="margin:0 0 20px 0px">
					    
					</div>	
				</div>
			</div>
			<?php echo form_close();?>
		<!-- login with otp part is end -->
			<div class="col-md-2"><span class="or"></span></div>
			<?php echo form_open(current_url(), 'class="text-left login-form"'); ?>
			<div class="col-md-5">
				<div class="row user_login">
					<div class="col-md-12" style="margin:0 0 20px;">
						<h3>Login</h3>
						<input type="text" name="sell_now" value="<?php echo $sell_now;?>" hidden> 
					</div>
					<div class="col-md-12">
						<div class="form-group form-group-username <?= form_error('username') ? 'has-error' : '' ?>">
							<label class="control-label">Email Address<span class="red">*</span></label>
							<input type="text" class="form-control input-lg" id="username" name="username" value="<?php echo set_value('username') ?>" />
							<div class="text-danger text-danger-username "><?= form_error('username') ? form_error('username') : '' ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group form-group-password <?= form_error('password') ? 'has-error' : '' ?>" id="password_otp">
							<label class="control-label">Password<span class="red">*</span></label>
							<input type="password" class="form-control input-lg form-group-password-otp" name="password" id="password" value="<?php echo set_value('password') ?>" />
							<div class="text-danger text-danger-password"><?= form_error('password') ? form_error('password') : '' ?></div>
						</div>
					</div>
					<div class="col-md-12 row" style="padding-right: 0;">
						<div class="col-md-5 lgn-btn">
							<div class="checkbox" style="margin-top:0">
								<!--<label for="remember_me" class="login-remember-me">
									<?php echo form_checkbox('remember_me', '1', set_value('remember_me') ? TRUE : FALSE) ?>
									Remember Me
								</label>-->
								<div class="form-group">
									<input type="submit" name="login_user" id="submit" value="Login" class="btn btn-md btn-primary"/>
								</div>
							</div>
						</div>
						<div class="col-md-5" style="padding-right: 0; float: right;">
							<div class="form-group text-right">
								<?php echo anchor('forgot_password', 'Forgot your password?','class="forget-pwd"') ?>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<?php echo form_close();?>

		</div>
	</div>
	<?php// else: ?>
	<!-- Registration part start -->
	<div class="registration hide">
		<div class="container">
			<?php
			$attributes = array('class' => 'register-form', 'id' => 'register-form');
			echo form_open_multipart('register', $attributes); ?>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row new-registration">
			    	<div>
			    		<span class="step1-tab" style="background: #26A65B; padding: 5px 10px;float: left;">Step 1</span>
				    	<span class="step2-tab" style="background: #DFDFDF; padding: 5px 10px;float: left;">Step 2</span>
			    	</div>
				    <div class="col-md-12" style="margin:0 0 20px;">
						<h3>Create New Account</h3>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12 registration-box">
						<div class="form-group step1 form-group-name<?= form_error('first_name') ? 'has-error' : '' ?>">
							<label class="control-label first_name" for="first_name">Name<span class="red">*</span></label>
							<input class="form-control first_name1 input-lg" type="text" name="first_name" />
							<div class="text-danger text-danger-name"><?= form_error('first_name') ? form_error('first_name') : '&nbsp' ?></div>
						</div>
						<div class="form-group step1 form-group-phone">
							<label class="control-label" for="phone">Phone Number<span class="red">*</span></label>
							<input class="form-control input-lg" id="verify_phone" type="phone" name="phone" />
							<input class="form-control input-lg" id="verify_phone_hidden" type="hidden" name="phone_hidden" value="0" />
							<input type="hidden" class="form-control input-lg" id="verify_phone_hidden1" name="phone_hidden1" value="0" />
							<div class="text-danger text-danger-phone"></div>
						</div>
						<div class="form-group step1 verify_number_part">
							<input class="btn btn-md btn-primary verify_number" type="button" id="verify_number" value="Verify Number" />
							<div class="text-danger"></div>
						</div>
						<div class="form-group step1 form-group-email <?= form_error('email') ? 'has-error' : '' ?>">
							<label class="control-label" for="email">Email Address<span class="red">*</span></label>
							<input class="form-control reg_email input-lg" type="email" name="email" value="<?= set_value('email') ?>" disabled/>
							<div class="text-danger text-danger-email"><?= form_error('email') ? form_error('email') : '&nbsp' ?></div>
						</div>
        				<div class="form-group step1 form-group-reg-password <?= form_error('password') ? 'has-error' : '' ?>">
        					<label class="control-label" for="password">Password<span class="red">*</span></label>
        					<input class="form-control reg_password input-lg" type="password" name="password" value="<?= set_value('password') ?>" disabled/>
        					<div class="text-danger text-danger-reg-password"><?= form_error('password') ? form_error('password') : '' ?></div>
        				</div>
        				<div class="form-group form-group-reg-confirm-password step1 <?= form_error('password_confirm') ? 'has-error' : '' ?>">
        					<label class="control-label" for="password_confirm">Confirm Password<span class="red">*</span></label>
        					<input class="form-control reg_confirm_password input-lg" type="password" name="password_confirm" value="<?= set_value('password_confirm') ?>" disabled/>
        					<div class="text-danger text-danger-confirm-password"><?= form_error('password_confirm') ? form_error('password_confirm') : '&nbsp' ?></div>
        				</div>
						<input class="btn btn-md btn-success hide step1 step1-next-btn" type="button" id="step1-next-btn" value="Next"/>
        				<!-- Address part start -->
			            <div class="step2 hide form-group-country">
			                <div class="form-group <?= form_error('country') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="country">Country <span class="red">*</span> <?= form_error('country') ? '' : '' ?></label>
			                    <select name="country" id="country" class="form-control  input-lg country">
			            		    <option value="0"> - Country - </option>
			                		<?php if(sizeof($countries) == 1){ ?>
			        			    <option value="<?php echo $countries[0]['loc_name'];?>" data-val="<?php echo $countries[0]['loc_id'];?>" selected><?php echo $countries[0]['loc_name'];?></option>    
			        			    <?php }else{ foreach($countries as $country) { ?>
			        				<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>">
			        					<?php echo $country['loc_name'];?>
			        				</option>
			        			    <?php } }?>
			        		    </select>
			                    <span class="text-danger text-danger-country"><?=form_error('country') ? form_error('country') : '' ?></span>
			                </div>
			            </div>
			            <div class="step2 hide form-group-state">
			                <div class="form-group <?= form_error('state') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="state">State <span class="red">*</span> <?= form_error('state') ? ' ' : '' ?></label>
			                    <select name="state" id="state" class="form-control input-lg state">
			        				<option value="0" selected="selected"> - State - </option>
			        				<?php if(sizeof($states) == 1){ ?>
			        			    <option value="<?php echo $states[0]['loc_name'];?>" data-val="<?php echo $states[0]['loc_id'];?>" selected><?php echo $states[0]['loc_name'];?></option>
			        			    <?php }else{ foreach($states as $state) { ?>
			        				<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>">
			        					<?php echo $state['loc_name'];?>
			        				</option>
			        			    <?php } }?>
			        			</select>
								<span class="text-danger text-danger-state"></span>
			                </div>
			            </div>
			            <div class="step2 hide form-group-city">
			                <div class="form-group <?= form_error('city') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="city">City <span class="red">*</span> <?= form_error('city') ? '' : '' ?></label>
			                    <input list="city_list" name="city" id="city" class="form-control input-lg city" autocomplete="off" value="<?php if($user_address->city) echo $user_address->city;elseif($user_address->country == '' && sizeof($cities) == 1)echo $cities[0]['loc_name'];?>" placeholder="Select or Enter your City">
								<datalist id="city_list">
			        				<?php if(sizeof($cities) == 1){ ?>
			        				    <option value="<?php echo $cities[0]['loc_name'];?>" data-val="<?php echo $cities[0]['loc_id'];?>"    selected><?php echo $cities[0]['loc_name'];?></option>
			        				<?php }else{ foreach($cities as $city) { ?>
			        				<option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>">
			        					<?php echo $city['loc_name'];?>
			        				</option>
			        			    <?php } } ?>
								</datalist>
								<span class="text-danger text-danger-city"></span>
			                </div>
			            </div>
			            <div class="step2 hide form-group-locality">
			                <div class="form-group <?= form_error('locality') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="locality">Locality <span class="red">*</span> <?= form_error('locality') ? '' : '' ?></label>
			                    <input list="locality_list" name="locality" id="locality" class="form-control input-lg locality" autocomplete="off" value="<?php if($user_address->locality) echo $user_address->locality;elseif($user_address->country == '' && sizeof($localities) == 1)echo $localities[0]['loc_name'];?>" placeholder="Select or Enter your Locality">
								<datalist id="locality_list">
			        			    <?php if(sizeof($localities) == 1){ ?>
			        					   <option value="<?php echo $localities[0]['loc_name'];?>" data-val="<?php echo $localities[0]['loc_id'];?>" selected><?php echo $localities[0]['loc_name'];?>
			        					   </option>
			        				<?php }else{ foreach($localities as $locality) { ?>
			        				    <option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>">
			        					<?php echo $locality['loc_name'];?>
			        					</option>
			        			    <?php } } ?>
			                    </datalist>
			                    <span class="text-danger text-danger-locality"><?=form_error('locality') ? form_error('locality') : '' ?></span>
			                </div>
			            </div>
			            <div class="step2 hide form-group-apartment-name">
			                <div class="form-group <?= form_error('apartment_name') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="apartment_name">Apartment<span class="red">*</span> <?= form_error('apartment_name') ? '' : '' ?></label>
			                    <input list="apartment_list" name="apartment" id="apartment" class="form-control input-lg apartment" autocomplete="off" value="<?php if($user_address->apartment_name) echo $user_address->apartment_name;elseif($user_address->country == '' && sizeof($apartments) == 1)echo $apartments[0]['loc_name'];?>" placeholder="Select or Enter your Apartment">
								<datalist id="apartment_list"  data-dropup-auto="false">
			        			    <?php if(sizeof($apartments) == 1){ ?>
			        				    <option value="<?php echo $apartments[0]['loc_name'];?>" data-val="<?php echo $apartments[0]['loc_id'];?>" selected><?php echo $apartments[0]['loc_name'];?></option>
			        				    <option value="Others" data-val="Others">
										Others
										</option>
			        				<?php }else{ foreach($apartments as $apartment) { ?>
			        				<option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>">
			        					<?php echo $apartment['loc_name'];?>
			        				</option>
			        			    <?php } } ?>	
			                    </datalist>
			                    <span class="text-danger text-danger-apartment"><?=form_error('apartment_name') ? form_error('apartment_name') : '' ?></span>
			                </div>
			            </div>	
			            <div class="step2 hide form-group-flat-no">
			                <div class="form-group <?= form_error('flat_no') ? 'has-error' : '' ?>">
			                    <label class="control-label" for="flat_no">Flat No.<span class="red">*</span> <?= form_error('flat_no') ? '' : '' ?></label>
			                    <input type="text" class="form-control input-lg" id="flat_no" placeholder="Flat No.*" name="flat_no" value="" />
			                    <span class="text-danger text-danger-flat_no"><?=form_error('flat_no') ? form_error('flat_no') : '' ?></span>
			                </div>
			            </div>
        				<!-- Address part end -->
						<input type="submit" name="create_user" id="create_user" value="Register my Account" class="btn btn-md btn-primary create_user step2 hide"/>
						<div class="step2-prev-div" style="padding: 20px 0 0 0;">
        					<input class="btn btn-md btn-success hide step2 step2-prev-btn" type="button" id="step2-prev-btn" value="Prev"/>
						</div>
					</div>
					<input type="hidden" name="create_user" value="create_user"/>
				</div>
			</div>
	        <?php echo form_close();?>
		</div>
	</div>
	<!-- Registration part end -->
	
	<div class="alert-box alert-box-login">
		<div class="container">
			<div class="alert-inbox-container">
				<div class="notification_div">
				<div class="alert alert-info alert-inbox" role="alert">
					<span>If you do not have an Account, Please click on button
					to create an account.  <input type="submit" name="login_user" id="registration-page-link" value="Create Account" class="btn btn-md btn-primary registration-page-link"/></span>
				</div></div>
			</div>
		</div>
	</div>
	
	<hr> 


	<script type="text/javascript">
	    function validateEmail(email) {
	       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	       return re.test(String(email).toLowerCase());
	    }
	    
		jQuery(document).ready(function(e){
		    //for address part end
			var country_value = jQuery('#country option:selected', this).val();
			var state_value = jQuery('#state option:selected', this).val();
			var city_value = jQuery('#city').val();
			var locality_value = jQuery('#locality', this).val();
			if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
	        	jQuery('#state').attr("disabled", true);
	        	jQuery('#city').attr("disabled", true);
	        	jQuery('#locality').attr("disabled", true);
	        	jQuery('#apartment').attr("disabled", true);
	        }
	        else if(state_value=='' || state_value=='- State -' || state_value=='0'){
	        	jQuery('#city').attr("disabled", true);
	        	jQuery('#locality').attr("disabled", true);
	        	jQuery('#apartment').attr("disabled", true);
	        }
	        else if(city_value=='' || city_value=='- city -'){
	        	jQuery('#locality').attr("disabled", true);
	        	jQuery('#apartment').attr("disabled", true);
	        }
	        else if(locality_value=='' || locality_value=='- locality -'){
	        	jQuery('#apartment').attr("disabled", true);
	        }
	        jQuery("#country option:contains('India')").attr('selected', 'selected');
		    var countries = <?php echo json_encode($countries) ?>;
		    if(countries.length > 1){
		       setTimeout(function(){ jQuery("#country").trigger("change"); }, 3000);    
		    }
		    jQuery('#country').on('change',function(){
		    	country_value = jQuery(this).val();
	            var country_id = jQuery('option:selected', this).attr('data-val');
	            var token = jQuery('input[name="global_cookiee"]').val();
	            var data = {country : country_id, gre_tokan : token };
	            jQuery('#state').find('option').not(':first').remove();
	            jQuery('#city').val('');
	    		jQuery('#locality').val('');
	    		jQuery('#apartment').val('');
	    		jQuery('#city_list').html('');
	    		jQuery('#locality_list').html('');
	    		jQuery('#apartment_list').html('');
	            jQuery.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo base_url('get_state'); ?>',
		            cache: false,
		            beforeSend : function(){
		                jQuery('#state').attr("disabled", true);
		                jQuery('#city').attr("disabled", true);
		                jQuery('#locality').attr("disabled", true);
		                jQuery('#apartment').attr("disabled", true);
		            },
		            success: function(response){
		            	var state_list = jQuery.parseJSON(response);
		            	for(var i = 0; i < state_list.length; i++) {
	                        var obj = state_list[i];
	                        jQuery('#state').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
	                    }
	                    if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
	                    	jQuery('#state').attr("disabled", true);
		                }
		                else{
	                    	jQuery('#state').attr("disabled", false);
		                }
		            }
		        });
		    });
		    jQuery('#state').on('change',function(){
		    	state_value = jQuery(this).val();
	            var state_id = jQuery('option:selected', this).attr('data-val');
	            var token = jQuery('input[name="global_cookiee"]').val();
	            var data = {state : state_id, gre_tokan : token };
	            jQuery('#city').find('option').not(':first').remove();
	            jQuery('#city').val('');
	    		jQuery('#locality').val('');
	    		jQuery('#apartment').val('');
	    		jQuery('#city_list').html('');
	    		jQuery('#locality_list').html('');
	    		jQuery('#apartment_list').html('');
	            jQuery.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo base_url('get_city'); ?>',
		            cache: false,
		            beforeSend : function(){
		                jQuery('#city').attr("disabled", true);
		                jQuery('#locality').attr("disabled", true);
	                	jQuery('#apartment').attr("disabled", true);
		            },
		            success: function(response){
		            	var city_list = jQuery.parseJSON(response);
		            	jQuery('#city').append(jQuery("<option>sdfdsf</option>"));
		            	for(var i = 0; i < city_list.length; i++) {
	                        var obj = city_list[i];
	                        jQuery('#city_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
	                    }
	                    if(state_value=='' || state_value==' - State - ' || state_value=='0'){
	                    	jQuery('#city').attr("disabled", true);
		                }
		                else{
	                    	jQuery('#city').attr("disabled", false);
		                }
		            }
		        });
		    }); 	    
		    jQuery('#city').on('change',function(){
	            var city_value = jQuery(this).val();
	            city_value = city_value.trim();
	    		var city_id = jQuery('#city_list').find("[value='" + city_value + "']").attr('data-val');
	            var token = jQuery('input[name="global_cookiee"]').val();
	            var data = {city : city_id, gre_tokan : token };
	            jQuery('#locality').find('option').not(':first').remove();
	            jQuery('#locality').val('');
	    		jQuery('#apartment').val('');
	    		jQuery('#locality_list').html('');
	    		jQuery('#apartment_list').html('');
	            jQuery.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo base_url('get_locality'); ?>',
		            cache: false,
		            beforeSend : function(){
		                jQuery('#locality').attr("disabled", true);
	                	jQuery('#apartment').attr("disabled", true);
		            },
		            success: function(response){
		            	var locality_list = jQuery.parseJSON(response);
		            	for(var i = 0; i < locality_list.length; i++) {
	                        var obj = locality_list[i];
	                        jQuery('#locality_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
	                    }
	                    if(city_value==''){
		                	jQuery('#locality').attr("disabled", true);
		                }
		                else{
	                    	jQuery('#locality').attr("disabled", false);
		                }
		            }
		        });
		    }); 	   
	        jQuery('#locality').on('change',function(){
	            var locality_value = jQuery(this).val();
	            locality_value = locality_value.trim();
	    		var locality_id = jQuery('#locality_list').find("[value='" + locality_value + "']").attr('data-val');
	            var token = jQuery('input[name="global_cookiee"]').val();
	            var data = {locality : locality_id, gre_tokan : token };
	            jQuery('#apartment').find('option').not(':first').remove();
	    		jQuery('#apartment').val('');
	    		jQuery('#apartment_list').html('');
	            jQuery.ajax({
		        	type: 'POST',
		        	data: data,
		            url: '<?php echo base_url('get_apartment'); ?>',
		            cache: false,
		            beforeSend : function(){
		                jQuery('#apartment').attr("disabled", true);
		            },
		            success: function(response){
		            	var apartment_list = jQuery.parseJSON(response);
		            	for(var i = 0; i < apartment_list.length; i++) {
	                        var obj = apartment_list[i];
	                        jQuery('#apartment_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
	                    }
	                    jQuery('#apartment').attr("disabled", false);
	                    if(locality_value==''){
		                	jQuery('#apartment').attr("disabled", true);
		                }
		            }
		        });
		    }); 
			//for address part end
		    jQuery('.forget-pwd').on('click',function(e){
		        
		        e.preventDefault();
		        var url = jQuery(this).attr('href');
		        var username = jQuery('#username').val();
		        
		        if(username != ''){
    		        jQuery.ajax({
    			        'type' : 'POST',
    			        'url' : "<?php echo base_url('forgot_pwd_redirect'); ?>",
    			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:username},
    			        'success' : function(data) {              
    			            if(data == 'success'){
    			                window.location.href = url;    
    			            }
    			        },
    			        'error' : function(request,error)
    			        {
    			            alert("Request: "+JSON.stringify(request));
    			        }
    			    });
		        }else{
		           window.location.href = url;
		        }
			    return false;
		         
		    });
		    
			jQuery("body").on('click', '#login-page-link',function(){
				var tag = '<p>If you do not have an Account, Please click on button				to create an account.  <input type="submit" name="login_user" id="registration-page-link" value="Create Account" class="btn btn-md btn-primary registration-page-link"/></p>';
				jQuery('.first_name1,.reg_email,#otp1,#verify_phone').val('');
				jQuery(".login-form-box").removeClass("hide");
				jQuery(".registration").addClass("hide");
				jQuery(".alert-info").html(tag);            
				jQuery(".validation_errors_msg .alert-inbox-session span").text('');
				var txt = '<input class="btn btn-md btn-primary verify_number" type="button" id="verify_number" value="Verify Number"/><div class="text-danger"></div>';
				jQuery(".verify_number_part").html(txt);
				jQuery(".form-group-number").removeClass('has-error');
				jQuery(".text-danger-number").html('');
				jQuery(".form-group-username").removeClass('has-error');
				jQuery(".text-danger-username").html(''); 
				jQuery(".form-group-password").removeClass('has-error');
				jQuery(".text-danger-password").html('');				      
			});
			jQuery("body").on('click', ".registration-page-link,.create_new_account",function(){
			    var contact_number = jQuery('#contact_number').val();
				var tag = '<p>If you already have an account, please click here. <input type="submit" id="login-page-link" value="Login" class="btn btn-md btn-primary login-page-link"/></p>';
				jQuery('#contact_number,#username,#password_otp input,#otp').val(''); 
				jQuery('#verify_phone').val(contact_number);
				jQuery(".login-form-box").addClass("hide");
	            jQuery(".registration").removeClass("hide");
				jQuery(".alert-info").html(tag);
				jQuery(".validation_errors_msg .alert-inbox-session span").text('');
				jQuery('.reg_password').val('');
				//jQuery('.reg_confirm_password').val('');jQuery(".form-group-reg-password").addClass('hide');
                //jQuery(".form-group-reg-confirm-password").addClass('hide');
                //jQuery(".form-group-email").addClass('hide');
                jQuery(".create_user").attr("disabled", true);
                jQuery('.form-group-name').removeClass('has-error');
				jQuery('.text-danger-name').html('');
				jQuery('.form-group-phone').removeClass('has-error');
				jQuery('.text-danger-phone').html('');
				jQuery('.form-group-email').removeClass('has-error');
				jQuery('.text-danger-email').html('');
				jQuery('.form-group-reg-password').removeClass('has-error');
				jQuery('.text-danger-reg-password').html('');
				jQuery('.form-group-reg-confirm-password').removeClass('has-error');
				jQuery('.text-danger-confirm-password').html('');
			}); 
			 
			jQuery("#username").blur(function(){
				var email = jQuery.trim(jQuery('#username').val());
				jQuery.ajax({
			        'type':"POST",
			        'context':this,
			        'url':"<?php echo base_url('check_email_exist'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email},
			        'success': function(data) { 
			            console.log(data);
			            if(data==1){
			                jQuery(".form-group-username").addClass("has-error");
							var p = "<p>Email id is not registered.<strong class='create_new_account' style='color:#26A65B;cursor:pointer'> Create New Account</strong></p>";
						    jQuery(".text-danger-username").html(p);	
			            }
			    	}   
				});
			}); 
			 
			jQuery("#submit").on('click',function(){
			    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				var email = jQuery.trim(jQuery('#username').val());
				var password = jQuery.trim(jQuery('#password').val());
				var error = false;
				
				jQuery('.text-danger').html('');
				if(email == '') {
					jQuery(".form-group-username").addClass("has-error");
					var p = "<p>Please Enter Your Email ID</p>";
				    jQuery(".text-danger-username").html(p);	
				    error = true;
				}
				if(!re.test(String(email).toLowerCase())){
					jQuery(".form-group-username").addClass("has-error");
					var p = "<p>Please Enter Valid Email ID</p>";
				    jQuery(".text-danger-username").html(p);	
				    error = true;
				}
				if(password == ''){
					jQuery(".form-group-password").addClass("has-error");
					var p = "<p>Please Enter Your Password</p>";
				    jQuery(".text-danger-password").html(p);	
				    error = true;
				}
                
				if(error){
					return false;
				}else{
				    jQuery('.login-form').submit();    
				}
				
			    
			});
			 
			jQuery("body").on('click', '#submit_otp',function(){
	        	var contact_number = jQuery('#contact_number').val();
	        	var otp = jQuery('.otp-resend_btn #otp').val();    
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('verify_otp'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp },
			        'success' : function(data) {     //alert(data);         
			            if(data == 1){
    		        		jQuery(".otp-resend_btn").removeClass("has-error");
    		        		jQuery("#submit_login_otp").click();;
    		        	}
    		        	else if(data == 2){
    		        		jQuery(".otp-resend_btn").addClass("has-error");
    		        		var p = "<p>OTP is expired. Please try again.</p>"
    		                jQuery(".submit_login_otp_text").html(p);
    		        		return false; 
    		        	}
    		        	else if(data == 3){
    		        		jQuery(".otp-resend_btn").addClass("has-error");
    		        		var p = "<p>Incorrect OTP. Please try again.</p>"
    		                jQuery(".submit_login_otp_text").html(p);
    		        		return false; 
    		        	}
    		        	else{
    		        		jQuery(".otp-resend_btn").addClass("has-error");
    		        		var p = "<p>Error in login.</p>"
    		                jQuery(".submit_login_otp_text").html(p);
    		        		return false; 
    		        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			});

	        
	        jQuery("body").on('click',"#genrate_otp,#resend_otp",function(){
	        	var contact_number = jQuery('#contact_number').val();
	        	var pattern = /^\d+$/;
	        	var length = contact_number.toString().length;
	        	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
	        		jQuery(".form-group-number").addClass("has-error");
	        		var p = "<p>Please Enter the Valid Contact Number.</p>";
	                jQuery(".text-danger-number").html(p);
	        		return; 
	        	}   
	        	var btn_val = jQuery(this).val();        
				jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('otp_generation'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
		        success : function(data) {
		        	var data = JSON.parse(data);
		        	if(data.results == 2 || data == 2)
		        	{
		        		jQuery(".form-group-number").addClass("has-error");
		        		var p = "<p>Number is not Registered.<strong class='create_new_account' style='color:#26A65B;cursor:pointer'> Create New Account</strong></p>"
		                jQuery(".text-danger-number").html(p);
		        		return; 
		        	}
		        	else{
						var tag = '<div class="row otp-resend_btn"><div class="col-md-5"><label class="control-label">Enter OTP:</label>		<input type="text" class="form-control input-lg" id="otp" name="otp" /></div><div class="col-md-5"><input type="button" name="resend_otp" id="resend_otp" value="Resend OTP" class="btn btn-md btn-primary" style="margin-left: 40px; margin-top:33px;" /></div></div><div class="submit_login_otp_text"></div>';
						var tag1 = '<div class="form-group"><input type="button" name="user_otp_verification" id="submit_otp" value="Login With OTP" class="btn btn-md btn-primary" /><input type="submit" id="submit_login_otp" value="Login With OTP" class="btn btn-md btn-primary hide" /></div>';
						jQuery(".form-group-number").removeClass("has-error");
			            jQuery(".text-danger-number").removeClass("has-error");	
			            jQuery(".text-danger-number p").remove(); 
			        	jQuery("#otp").html(tag); 
						jQuery(".login_otp").html(tag1);
						if(btn_val=='Resend OTP')
							jQuery(".submit_login_otp_text").html('<p style="color:#26A65B">OTP sent to your number.</p>');
		        	}
		        },
		        error : function(request,error)
		        {
		            alert("Request : "+JSON.stringify(request));
		        }
		    });
			});

	        //genrate otp for registration
	        jQuery("body").on('click',"#verify_number",function(){
	        	var contact_number = jQuery('#verify_phone').val();
	        	var name =  jQuery('.first_name1').val();
	        	var pattern = /^\d+$/;
	        	var pattern_name = /^[a-zA-Z ]*$/;//alert(pattern_name.test(name));
	        	var length = contact_number.toString().length;
	        	if (name == '' || pattern_name.test(name) == false) {
				    jQuery(".form-group-name").addClass("has-error");
	        		var p = "<p>Please Enter Only Character.</p>"
	                jQuery(".text-danger-name").html(p);
	                if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
		        		jQuery(".form-group-phone").addClass("has-error");
		        		var p = "<p>Please Enter Valid Contact Number.</p>"
		                jQuery(".text-danger-phone").html(p);
		        		return; 
		        	}
		        	return;
				}
	        	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
	        		jQuery(".form-group-phone").addClass("has-error");
	        		var p = "<p>Please Enter Valid Contact Number.</p>"
	                jQuery(".text-danger-phone").html(p);
	                if (!name || pattern_name.test(name) == false) {
					    jQuery(".form-group-name").addClass("has-error");
		        		var p = "<p>Please Enter Only Character.</p>"
		                jQuery(".text-danger-name").html(p);
			        	return;
					}
	        		return; 
	        	}
				var tag = '<div class="row"><div class="col-md-5 form-group-reg-otp-div"><label class="control-label">Enter OTP:</label><input type="text" class="form-control input-lg form-group-reg-otp" id="otp1" name="otp" /><div class="text-danger text-danger-reg-otp"></div></div><div class="col-md-6"><input type="button" name="verify_register_otp" id="verify_register_otp" value="Verify OTP" class="btn btn-md btn-success" style="margin-left: 15px; margin-top:27px;" /><input type="button" name="reg_resend_otp" id="reg_resend_otp" value="Resend OTP" class="btn btn-md btn-primary" style="margin-left: 28px; margin-top:27px;" /></div></div>';

				jQuery.ajax({
				type : 'POST',
		        url : "<?php echo base_url('mobile_verification'); ?>",
		        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
		        success : function(data) {
		        	var data = JSON.parse(data);
		        	//console.log(data);
		        	if(data.results == "Number is Invalid.")
		        	{
		        		jQuery(".form-group-phone").addClass("has-error");
		        		var p = "<p>Number is Invalid.</p>"
		                jQuery(".text-danger-phone").html(p);
		        		return; 
		        	}
		        	if(data.results==1){
		        		jQuery(".form-group-phone").addClass("has-error");
		        		var p = "<p>Phone Number is Already Registered.</p>"
		        		jQuery(".text-danger-phone").html(p);
		        		return;
		        	}
		            jQuery(".form-group-phone").removeClass("has-error");
		            jQuery(".text-danger-phone").removeClass("has-error");	
		            jQuery(".text-danger-phone p").remove(); 
		        	jQuery(".verify_number_part").html(tag); 
		        },
		        error : function(request,error)
		        {
		            alert("Request : "+JSON.stringify(request));
		        }
		    });
			}); 
			//genrate otp for registration

	        jQuery("body").on('click',"#reg_resend_otp",function(){
	        	var contact_number = jQuery('#verify_phone').val();
	        	var name =  jQuery('.first_name1').val();
	        	var pattern = /^\d+$/;//alert('hi');
	        	var pattern_name = /^[a-zA-Z ]*$/;
	        	var length = contact_number.toString().length;
	        	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
	        		jQuery(".form-group-phone").addClass("has-error");
	        		var p = "<p>Please Enter Valid Contact Number.</p>"
	                jQuery(".text-danger-phone").html(p);
	        		return; 
	        	}
				if (name == '' || pattern_name.test(name) == false) {
				    jQuery(".form-group-name").addClass("has-error");
	        		var p = "<p>Please Enter Only Character.</p>"
	                jQuery(".text-danger-name").html(p);
	                return;
				}
				jQuery('.form-group-reg-otp-div').removeClass('has-error');
				jQuery('.text-danger-reg-otp').html('');
				jQuery('.form-group-reg-otp').val('');
				var btn_val = jQuery(this).val();  
	        	jQuery.ajax({
					type : 'POST',
			        url : "<?php echo base_url('mobile_verification'); ?>",
			        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
			        success : function(data) {
			        	var data = JSON.parse(data);
			        	if(btn_val=='Resend OTP')
							jQuery(".text-danger-phone").html('<p style="color:#26A65B">OTP sent to your number.</p>');
			        	if(data.results == "Number is Invalid.")
			        	{
			        		jQuery(".form-group-phone").addClass("has-error");
			        		var p = "<p>Number is Invalid.</p>"
			                jQuery(".text-danger-phone").html(p);
			        		return; 
			        	}
			        	if(data.results==1){
			        		jQuery(".form-group-phone").addClass("has-error");
			        		var p = "<p>Phone Number is Already Registered.</p>"
			        		jQuery(".text-danger-phone").html(p);
			        		return;
			        	}
			        	//alert(data.otp);
			        },
			        error : function(request,error)
			        {
			            alert("Request : "+JSON.stringify(request));
			        }
			    });
	        });	
	        //verify otp for registration part   
			jQuery("body").on('click', '#verify_register_otp', function(){
	        	var contact_number = jQuery('#verify_phone').val();
	        	var otp = jQuery('#otp1').val();
	        	var pattern = /^\d+$/;
	        	var length = otp.toString().length;
        		var hidden_val = jQuery("#verify_phone_hidden").val();
	        	jQuery("#verify_phone_hidden1").val(hidden_val);
	        	var phone_hidden1 = jQuery("#verify_phone_hidden1").val();
	        	if(!otp || !(pattern.test(otp)) || length<4 || length >4){
	        		jQuery(".form-group-reg-otp-div").addClass("has-error");
	        		var p = "<p>Please Enter Coreect OTP Number.</p>"
	                jQuery(".text-danger-reg-otp").html(p);
	        		return; 
	        	}
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('verify_register_otp'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp,phone_hidden1:phone_hidden1 },
			        'success' : function(data) {  
			        	if (data == 1) {
			        		tag = "<div class='alert alert-success'><p><strong>Success!</strong> Your Phone is Verified Successfully.</p></div>";
					        jQuery(".form-group-phone").removeClass("has-error");
				            jQuery(".text-danger-phone").removeClass("has-error");
				            jQuery("#verify_phone_hidden").val(contact_number);
				            jQuery(".text-danger-phone p").remove(); 
				        	jQuery(".verify_number_part").html(tag); 
				        	jQuery(".step1-next-btn").removeClass('hide'); 
				        	jQuery(".reg_email").attr('disabled',false);
				        	jQuery(".reg_password").attr('disabled',false);
				        	jQuery(".reg_confirm_password").attr('disabled',false);
			        	}
			        	if (data == 2) {
			        		jQuery(".form-group-reg-otp-div").addClass("has-error");
			        		var p = "<p>OTP is Expired.</p>"
			        		jQuery(".text-danger-reg-otp").html(p);
			        		return;
			        	}
			        	if (data == 3) {
			        		jQuery(".form-group-reg-otp-div").addClass("has-error");
			        		var p = "<p>Insert Correct OTP.</p>"
			        		jQuery(".text-danger-reg-otp").html(p);
			        		return;
			        	}
			        	if (data == 4) {
			        		jQuery(".text-danger-reg-otp").addClass("has-error");
			        		var p = "<p>Error.</p>"
			        		jQuery(".text-danger-reg-otp").html(p);
			        		return;
			        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			});
	    
	        jQuery("body").on('click', '#step1-next-btn', function(){
	        	var phone      = jQuery('#verify_phone').val();
	        	var phone_prev = jQuery('#verify_phone_hidden').val();
	        	var name       = jQuery('.first_name1').val();
	        	var email      = jQuery('.reg_email').val();
	        	var password   = jQuery('.reg_password').val();
		        var confirm_password = jQuery('.reg_confirm_password').val();
	        	var first_name = jQuery('.first_name1').val();
	        	var password_length = password.length;
	        	var error = false;
	        	if(name == '') {
					jQuery('.form-group-name').addClass('has-error');
					jQuery('.text-danger-name').text('Please Enter Your Name');
					error = true;
				}
				if(!isNaN(name)){
					jQuery('.form-group-name').addClass('has-error');
				    jQuery('.text-danger-name').text('Please Enter Only Characters');
					error = true;
				}
				if(name.length < 3){
					jQuery('.form-group-name').addClass('has-error');
				    jQuery('.text-danger-name').text('Please Enter Atleast Three Characters');
					error = true;
				}
	        	if (email == '') {
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please enter email id.</p>"
	                jQuery(".text-danger-email").html(p);
	                error = true;
				}
				if(!validateEmail(email)){
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please enter valid email id.</p>";
	                jQuery(".text-danger-email").html(p);
	                error = true;
				}
				if(password == '') {
    				jQuery(".form-group-reg-password").addClass("has-error");
	        		var p = "<p>Please enter password.</p>"
	                jQuery(".text-danger-reg-password").html(p);
    				error = true;
    			}
    			if (password_length < 8) {
    			    jQuery(".form-group-reg-password").addClass("has-error");
	        		var p = "<p>Password lenth should be at least 8 digit.</p>"
	                jQuery(".text-danger-password").html(p);
    				error = true;
    			}
    			if(confirm_password == '') {
    				jQuery(".form-group-reg-confirm-password").addClass("has-error");
	        		var p = "<p>Please enter confirm password.</p>"
	                jQuery(".text-danger-confirm-password").html(p);
    				error = true;
    			}
    			if (password != confirm_password) {
    			    jQuery(".form-group-reg-confirm-password").addClass("has-error");
	        		var p = "<p>Passwords do not match.</p>"
	                jQuery(".text-danger-confirm-password").html(p);
    				error = true;
    			}
    			if(error){
    			    return false;
    			}
	        	if (phone != phone_prev){
	        		var tag = '<input class="btn btn-md btn-primary verify_number" type="button" id="verify_number" value="Verify Number" />';
	        		jQuery('.verify_number_part').html(tag);
	        		jQuery('#step1-next-btn').addClass('hide');
	        	}
	        	else{ 
		        	jQuery.ajax({
				        'type':"POST",
				        'context':this,
				        'url':"<?php echo base_url('check_email_exist'); ?>",
				        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email},
				        'success': function(data) { 
				            console.log(data);
				            if(data==1){
				                jQuery(".step2").removeClass('hide');
				                jQuery(".step1").addClass('hide');
			                	jQuery(".create_user").attr("disabled", false);
			                	jQuery(".step2-tab").css("background", "#26A65B");
				            }
				            else{
				                jQuery('.text-danger-email').text('Email address is already exists. Please Enter another email.');
			    				jQuery(".form-group-email").addClass("has-error");
				            } 
				    	}   
					});
		    	}
	        });	
	        jQuery("body").on('click', '#step2-next-btn', function(){
	        	var email = jQuery('.reg_email').val();
	        	var password = jQuery('.reg_password').val();
		        var confirm_password = jQuery('.reg_confirm_password').val();
	        	var first_name = jQuery('.first_name1').val();
	        	var password_length = password.length;
	        	var error = false;
	        	if (email == '') {
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please enter email id.</p>"
	                jQuery(".text-danger-email").html(p);
	                error = true;
				}
				if(!validateEmail(email)){
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please enter valid email id.</p>"
	                jQuery(".text-danger-email").html(p);
	                error = true;
				}
				if(password == '') {
    				jQuery(".form-group-reg-password").addClass("has-error");
	        		var p = "<p>Please enter password.</p>"
	                jQuery(".text-danger-reg-password").html(p);
    				error = true;
    			}
    			if (password_length < 8) {
    			    jQuery(".form-group-reg-password").addClass("has-error");
	        		var p = "<p>Password lenth should be at least 8 digit.</p>"
	                jQuery(".text-danger-password").html(p);
    				error = true;
    			}
    			if(confirm_password == '') {
    				jQuery(".form-group-reg-confirm-password").addClass("has-error");
	        		var p = "<p>Please enter confirm password.</p>"
	                jQuery(".text-danger-confirm-password").html(p);
    				error = true;
    			}
    			if (password != confirm_password) {
    			    jQuery(".form-group-reg-confirm-password").addClass("has-error");
	        		var p = "<p>Passwords do not match.</p>"
	                jQuery(".text-danger-confirm-password").html(p);
    				error = true;
    			}
    			if(error){
    			    return false;
    			}
    			else{
                	jQuery(".step1").addClass('hide');
                	jQuery(".step2").addClass('hide');
                	jQuery(".step3").removeClass('hide');
                	jQuery(".create_user").removeClass('hide');
                	jQuery(".create_user").attr("disabled", false);
                	jQuery(".step2-tab").css("background", "#26A65B");
                }	
	        });
	        jQuery("body").on('click', '#step2-prev-btn', function(){
                jQuery(".step1").removeClass('hide');
                jQuery(".step3").addClass('hide');
            	jQuery(".step2-tab").css("background", "#DFDFDF");
	        });
	        jQuery("body").on('click', '#step2-prev-btn', function(){
                jQuery(".step1").removeClass('hide');
                jQuery(".step2").addClass('hide');
                jQuery(".step3").addClass('hide');
                jQuery(".step2-tab").css("background", "#DFDFDF");
	        });

			jQuery("body").on('click', '#create_user', function(e){
			    e.preventDefault();
	        	var email = jQuery('.reg_email').val();
	        	var password = jQuery('.reg_password').val();
		        var confirm_password = jQuery('.reg_confirm_password').val();
	        	var first_name = jQuery('.first_name1').val();
	        	var password_length = password.length;
	        	var country   = jQuery('#country').val();
				var state     = jQuery('#state').val();
				var city      = jQuery('#city').val();
				var locality  = jQuery('#locality').val();
				var pin       = jQuery('#pin').val();
				var apartment = jQuery('#apartment').val();
				var flat_no   = jQuery('#flat_no').val();
	        	var error = false;
	        	if (email == '') {
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please enter email id.</p>"
	                jQuery(".text-danger-email").html(p);
	                error = true;
				}
				if(!validateEmail(email)){
				    jQuery(".form-group-email").addClass("has-error");
	        		var p = "<p>Please enter valid email id.</p>"
	                jQuery(".text-danger-email").html(p);
	                error = true;
				}
				if(password == '') {
    				jQuery(".form-group-reg-password").addClass("has-error");
	        		var p = "<p>Please enter password.</p>"
	                jQuery(".text-danger-reg-password").html(p);
    				error = true;
    			}
    			if (password_length < 8) {
    			    jQuery(".form-group-reg-password").addClass("has-error");
	        		var p = "<p>Password lenth should be at least 8 digit.</p>"
	                jQuery(".text-danger-password").html(p);
    				error = true;
    			}
    			if(confirm_password == '') {
    				jQuery(".form-group-reg-confirm-password").addClass("has-error");
	        		var p = "<p>Please enter confirm password.</p>"
	                jQuery(".text-danger-confirm-password").html(p);
    				error = true;
    			}
    			if (password != confirm_password) {
    			    jQuery(".form-group-reg-confirm-password").addClass("has-error");
	        		var p = "<p>Passwords do not match.</p>"
	                jQuery(".text-danger-confirm-password").html(p);
    				error = true;
    			}
    			if(country == '0'){
				    jQuery('.text-danger-country').text('Please Select Country');
				    error = true;
				}
				if(state == '0'){
				    jQuery('.text-danger-state').text('Please Select State');
				    error = true;
				}
				if(city == '0' || city == '' || city == undefined){
				    jQuery('.text-danger-city').text('Please Select City');
				    error = true;
				}
				if(locality == '0' || locality == '' || locality == undefined){
				    jQuery('.text-danger-locality').text('Please Select Locality');
				    error = true;
				}
				if(apartment == '0' || apartment == '' || apartment == undefined){
				    jQuery('.text-danger-apartment').text('Please Select Apartment');
					error = true;
				}
				if(flat_no == ''){
				    jQuery('.text-danger-flat_no').text('Please Enter Flat Number');
					error = true;
				}
    			if(error){
    			    return false;
    			}
		        jQuery.ajax({
			        'type' : 'POST',
			        'url' : "<?php echo base_url('check_email'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email },
			        'success' : function(data) {  
			        	if (data == 1) {
			        		jQuery(".form-group-email").addClass("has-error");
			        		var p = "<p>Email is Already Exists.</p>"
			        		jQuery(".text-danger-email").html(p);
			        		return;
			        	}
			        	if(data == 2){
			        		jQuery(".form-group-email").removeClass("has-error");
			        		var p = "<p></p>"
			        		jQuery(".text-danger-email").html(p);
			        		jQuery('#register-form').submit();
			        	}
			        },
			        'error' : function(request,error)
			        {
			            alert("Request: "+JSON.stringify(request));
			        }
			    });
			});

			function fromRregister(email,phone,first_name,create_user){
			    jQuery.ajax({
			        'type':"POST",
			        'url':"<?php echo base_url('register'); ?>",
			        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',email:email,phone:phone,first_name:first_name,create_user:create_user },
			        'success': function(resp) { 
			        	window.location.href = "<?php echo base_url('login'); ?>"; 
			    	}   
				});
			}
			jQuery('.first_name1').on('focus', function(){
			   jQuery('.form-group-name').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-name').html(p);
			});
			jQuery('#verify_phone').on('focus', function(){
			   jQuery('.form-group-phone').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-phone').html(p);
			});
			jQuery('.reg_email').on('focus', function(){
			   jQuery('.form-group-email').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-email').html(p);
			});
			jQuery("body").on('focus', '.form-group-reg-otp', function(){
			   jQuery('.form-group-reg-otp-div').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-reg-otp').html(p);
			});
			jQuery('#username').on('focus', function(){
			   jQuery('.form-group-username').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-username').html(p);
			});
			jQuery('.form-group-password-otp').on('focus', function(){
			   jQuery('#password_otp').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-password').html(p);
			});
			jQuery('#contact_number').on('focus', function(){
			   jQuery('.form-group-number').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-number').html(p);
			});
			jQuery(".reg_password").focus(function(){
                jQuery('.form-group-reg-password').removeClass('has-error');
			    var p = '';
			    jQuery('.text-danger-reg-password').html(p);
        	});
        	jQuery(".reg_confirm_password").focus(function(){
        	   jQuery('.form-group-reg-confirm-password').removeClass('has-error');
			   var p = '';
			   jQuery('.text-danger-confirm-password').html(p);
        	});
        	jQuery("body").on('click',".otp-resend_btn #otp",function(){
        	   jQuery('.otp-resend_btn').removeClass('has-error');
			   var p = '';
			   jQuery('.submit_login_otp_text').html(p);
        	});
        	jQuery('#country').on('focus', function(){
				var txt = '';
				jQuery('.text-danger-country').html(txt);
			});
			jQuery('#state').on('focus', function(){
				var txt = '';
				jQuery('.text-danger-state').html(txt);
			});
			jQuery('#city').on('focus', function(){
				var txt = '';
				jQuery('.text-danger-city').html(txt);
			});
			jQuery('#locality').on('focus', function(){
				var txt = '';
				jQuery('.text-danger-locality').html(txt);
			});
			jQuery('#apartment').on('focus', function(){
				var txt = '';
				jQuery('.text-danger-apartment').html(txt);
				jQuery('.alert_msg1').html(txt);
			});
			jQuery('#flat_no').on('focus', function(){
				var txt = '';
				jQuery('.text-danger-flat_no').html(txt);
			});
	    });
	</script>

	<?php $this->load->view('public/templates/footer') ?>