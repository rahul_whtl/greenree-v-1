<?php $this->load->view('public/templates/header', array(
	'title' => 'Dashboard',
	'link' => 'account'
)) ?>
<div class="reset-password">
	<div class="container">
        <div id="infoMessage"><?php echo $message;?></div>
        <div class="page-header">
        	<h4 class="lead"><?php echo lang('reset_password_heading');?>Reset Your Password</h4>
        </div>
        <?php echo form_open('reset_password/'.$code);?>
        <div class="row change-password-part">
            <div class="col-md-12 col-sm-12">
                <div class="row user_login">
                    <!--<div class="col-md-12 forget-password-otp-text">-->
                    <!--	<div class="form-group">-->
                    <!--		<label class="control-label">OTP:</label>-->
                    <!--		<input type="text" name="new-otp-number" id="new-otp-number" class="form-control input-lg" value="<?php //echo $phone;?>">-->
                    <!--		<input type="text" name="new-otp" id="new-otp" class="form-control input-lg">-->
                    <!--		<div class="text-danger forget-password-otp"></div>-->
                    <!--	</div>-->
                    <!--</div>-->
                    <!--<div class="col-md-12 forget-password-otp-btn text-right">-->
                    <!--	<div class="form-group">-->
                    <!--	    <div class="reset-password-otp-msg"></div>-->
                    <!--		<input type="button" name="forget-password-otp-submit" id="forget-password-otp-submit" class="btn btn-lg btn-primary forget-password-otp-submit" value="Verify OTP">-->
                    <!--		<input type="button" name="forget-password-otp-resend" id="forget-password-otp-resend" class="btn btn-lg btn-primary forget-password-otp-resend" value="Resend OTP">-->
                    <!--	</div>-->
                    <!--</div>-->
                    <div class="col-md-12 forget-password-text">
                    	<div class="form-group">
                    		<label class="control-label">Password:</label>
                    		<input type="password" name="new" id="new" class="form-control input-lg name">
                    		<div class="text-danger forget-password"></div>
                    	</div>
                    </div>
                    <div class="col-md-12 forget-confirm-password-text">
                    	<div class="form-group">
                    		<label class="control-label">Confirm Password:</label>
                    		<input type="password" name="new_confirm" id="new_confirm" class="form-control input-lg" pattern="^.{8}.*$">
                    		<div class="text-danger forget-confirm-password"></div>
                    	</div>
                    </div>
                    <?php echo form_input($user_id);?>
                    <?php echo form_hidden($csrf); ?>
                    <div class="col-md-12">
                    	<div class="col-md-12 form-group forget-password-btn">
                    		<input type="submit" value="Submit" id="reset_password" name="submit" class="btn btn-lg btn-primary hide"/>
                    		<input type="button" value="Submit" class="btn btn-lg btn-primary reset_password_btn"/>
                    		<div class="text-danger forget-password-btn1"></div>
                    	</div>
                    </div>
                </div>
            </div>
        	<!--<div class="col-md-4 col-sm-4">-->
        	<!--	<div class="panel panel-default">-->
        	<!--		<div class="panel-heading">-->
        	<!--			<h4 class="panel-title">Password Tips</h4>-->
        	<!--		</div>-->
        	<!--		<div class="panel-body">-->
        	<!--			<ul class="row" style="padding-left: 15px;">-->
        	<!--				<li>-->
        	<!--					<strong>Use a strong password.</strong>-->
        	<!--					8 Characters and above, we recommend a mixture of alpha-numeric characters or symbols-->
        	<!--				</li>-->
        	<!--				<li>-->
        	<!--					<strong>Memorize your password.</strong>-->
        	<!--					Never write it down, we recommend logging in as much as possible without checking the "remember me".-->
        	<!--				</li>-->
        	<!--			</ul>-->
        	<!--		</div>-->
        	<!--	</div>-->
        	<!--</div>-->
    	</div>
<?php echo form_close();?>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery(".reset_password_btn").click(function(event){
			event.preventDefault();
			var new_confirm_password = jQuery('#new_confirm').val();
			var new_password = jQuery('#new').val();
			var password_length = new_password.length;
			if (new_password == '' || new_password == undefined) {
				var txt = '<p>Please Enter Valid Password.</p>';
				jQuery('.forget-password').html(txt);
				jQuery('.forget-password').css('color','#FF0000');
				if (new_password == '' || new_password == undefined) {
					var txt = '<p>Please Enter Valid Confirm Password.</p>';
					jQuery('.forget-confirm-password').html(txt);
					jQuery('.forget-confirm-password').css('color','#FF0000');
					return;	
				}
				else{
					var txt = '<p></p>';
					jQuery('.forget-confirm-password').html(txt);
					jQuery('.forget-confirm-password').css('color','#ffffff');
				}	
				return;	
			}
			else{
				var txt = '<p></p>';
				jQuery('.forget-password').html(txt);
				jQuery('.forget-password').css('color','#ffffff');
			}
			if (password_length < 8) {
				var txt = '<p>The above field must be at least 8 characters in length.</p>';
				jQuery('.forget-password').html(txt);
				jQuery('.forget-password').css('color','#FF0000');
				return;
			}
			else{
				var txt = '<p></p>';
					jQuery('.forget-password').html(txt);
					jQuery('.forget-password').css('color','#ffffff');
			}
			if (new_confirm_password == '' || new_confirm_password == undefined) {
				var txt = '<p>Please Enter Valid Confirm Password.</p>';
				jQuery('.forget-confirm-password').html(txt);
				jQuery('.forget-confirm-password').css('color','#FF0000');
				return;
			}
			else{
				var txt = '<p></p>';
				jQuery('.forget-confirm-password').html(txt);
				jQuery('.forget-confirm-password').css('color','#ffffff');
			}
			if (new_password != new_confirm_password) {
				var txt = '<p>Password is not Matched.</p>';
				jQuery('.forget-confirm-password').html(txt);
				jQuery('.forget-confirm-password').css('color','#FF0000');
				return;
			}
			else{
				var txt = '<p></p>';
				jQuery('.forget-confirm-password').html(txt);
				jQuery('.forget-confirm-password').css('color','#ffffff');
			}
			jQuery('.reset-password .reset_password_btn').addClass("hide");
			jQuery('.reset-password #reset_password').removeClass("hide");
			jQuery('.reset-password #reset_password').click();
		});
		//resend otp
// 		jQuery("body").on('click',"#forget-password-otp-resend",function(){
//         	var contact_number = jQuery('#contact_number').val();
//         	var pattern = /^\d+$/;
//         	var length = contact_number.toString().length;
//         	if(!contact_number || !(pattern.test(contact_number)) || length<10 || length >10){
//         		jQuery(".form-group-number").addClass("has-error");
//         		var p = "<p>Please Enter the Valid Contact Number.</p>";
//                 jQuery(".text-danger-number").html(p);
//         		return; 
//         	}          
// 			jQuery.ajax({
//     			type : 'POST',
//     	        url : "<?php //echo base_url('otp_generation'); ?>",
//     	        data : { '<?php// echo $this->security->get_csrf_token_name(); ?>':'<?php// echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number},
//     	        success : function(data) {
//     	        	var data = JSON.parse(data);
//     	        	if(data.results == 2 || data == 2)
//     	        	{
//     	        		jQuery(".form-group-number").addClass("has-error");
//     	        		var p = "<p>Number is not Registered.<strong class='create_new_account' style='color:#26A65B;cursor:pointer'> Create New Account</strong></p>"
//     	                jQuery(".text-danger-number").html(p);
//     	        		return; 
//     	        	}
//     	        	else{
//     					var tag = '<div class="row otp-resend_btn"><div class="col-md-5"><label class="control-label">Enter OTP:</label>		<input type="text" class="form-control input-lg" id="otp" name="otp" /></div><div class="col-md-5"><input type="button" name="resend_otp" id="resend_otp" value="Resend OTP" class="btn btn-lg btn-primary" style="margin-left: 40px; margin-top:28px;" /></div></div>';
//     					var tag1 = '<div class="form-group"><input type="submit" name="user_otp_verification" id="submit_otp" value="Login With OTP" class="btn btn-lg btn-primary" />		</div>';
//     					jQuery(".form-group-number").removeClass("has-error");
//     		            jQuery(".text-danger-number").removeClass("has-error");	
//     		            jQuery(".text-danger-number p").remove(); 
//     		        	jQuery("#otp").html(tag); 
//     					jQuery(".login_otp").html(tag1);
//     					alert(data.otp);
//     	        	}
//     	        },
//     	        error : function(request,error)
//     	        {
//     	            alert("Request : "+JSON.stringify(request));
//     	        }
//     	    });
//     	});
    	//verify otp
//     	jQuery("#forget-password-otp-submit").click(function(){
//         	var contact_number = jQuery('#new-otp-number').val();
//         	var otp = jQuery('#new-otp').val();
// 	        jQuery.ajax({
// 		        'type' : 'POST',
// 		        'url' : "<?php// echo base_url('verify_otp'); ?>",
// 		        'data' : { '<?php //echo $this->security->get_csrf_token_name(); ?>':'<?php// echo $this->security->get_csrf_hash(); ?>',contact_number:contact_number,otp:otp },
// 		        'success' : function(data) {     
// 		            if (data == 1) {
// 		        		tag = "<div class='alert alert-success'><p><strong>Success!</strong> Your OTP is verified successfully.</p></div>";
// 				        jQuery(".forget-password-otp-text").removeClass("has-error");	
// 			        	jQuery(".reset-password-otp-msg").html(tag); 
// 		                jQuery(".reset_password_btn").attr("disabled", false);
// 		        	}
// 		        	if (data == 2) {
// 		        		jQuery(".forget-password-otp-text").addClass("has-error");
// 		        		var p = "<p>OTP is Expired.</p>"
// 		        		jQuery(".reset-password-otp-msg").html(p);
// 		        		return;
// 		        	}
// 		        	if (data == 3) {
// 		        		jQuery(".forget-password-otp-text").addClass("has-error");
// 		        		var p = "<p>Insert Correct OTP.</p>"
// 		        		jQuery(".reset-password-otp-msg").html(p);
// 		        		return;
// 		        	}
// 		        	if (data == 4) {
// 		        		jQuery(".forget-password-otp-text").addClass("has-error");
// 		        		var p = "<p>Error.</p>"
// 		        		jQuery(".reset-password-otp-msg").html(p);
// 		        		return;
// 		        	}
// 		        },
// 		        'error' : function(request,error)
// 		        {
// 		            alert("Request: "+JSON.stringify(request));
// 		        }
// 		    });
// 		});
		jQuery(".reset-password #new").focus(function(){
    	    var txt = '';
        	jQuery('.reset-password .forget-password').html(txt);
    	});
    	jQuery(".reset-password #new_confirm").focus(function(){
    	    var txt = '';
        	jQuery('.reset-password .forget-confirm-password').html(txt);
    	});
    	
	});	
</script>
<?php $this->load->view('public/templates/footer') ?>