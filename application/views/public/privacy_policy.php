<?php $this->load->view('public/templates/header', array(
	'title' => 'Privacy Policy - GreenREE')) ?>
<div class="privacy_policy">
	<style type="text/css">
		.privacy_policy li{font-size: 20px;padding-bottom: 15px;}
		.privacy_policy ul{padding-left: 50px;}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 contact-us-title">
				<h1 class="text-center">Privacy Policy</h1> 
				<div class="introduction">
					<h3>Introduction</h3>
					<ul>
						<li>Greenree Proprietary, an Indian company(“GreenREE”, “Our” or “We”) recognizes the importance of Your privacy and is committed to ensuring that You are aware of how Your information is being used.
						</li>
						<li>
							The terms “You” or “Your”, shall include any individual or legal entity who accesses or uses www.greenree.com or the GreenREE mobile application (collectively “Website”).
						</li>
						<li>This policy (“Privacy Policy”) describes the types of information GreenREE may collect from You or that You may provide in relation to the use or access of the Website and the manner in which such information is collected, used, processed, disclosed and maintained.
						</li>
						<li>
							Please read and understand this Privacy Policy carefully. If You do not agree with Our policies and Our practices in the way we treat Your information (“User Information”, which may include Your name, nationality, date of birth, telephone number, email address, physical address and the like), Your choice is to not use the Website. Your use or access of the Website, shall constitute Your agreement to this Privacy Policy.
						</li>
						<li>
							By accepting this Privacy Policy you expressly consent to GreenREE use and disclosure of your personal information in accordance with this Privacy Policy.
						</li>
					</ul>
				</div>
				<div class="policy_applicability">
					<h3>Applicability of the policy</h3>
					<ul>
						<li>This Privacy Policy shall apply to all information You provide on the Website and all information that GreenREE collects on the Website including but not limited to any information You upload, emails that You exchange with Greenree and other users of the Website and any information submitted by You to GreenREE.
						</li>
						<li>
							This Privacy Policy does not apply to, nor does GreenREE take any responsibility for, any information that is collected by any third party either using the Website or through any links on the Website or through any advertisements on the Website.
						</li>
					</ul>
				</div>
				<div class="information_nature">
					<h3>Nature of information collected and manner of collection</h3>
					<ul>
						<li>In the use of the Website, as You navigate through the Website, GreenREE may collect different types of information. This may include User Information, information that is personally identifiable, other information which may not be personally identifiable, information on the usage patterns of any user including You, searches that You have done on the Website, advertisements or third party links that You have visited, any emails or other correspondence You have exchanged on the Website or with GreenREE.
						</li>
						<li>
							You understand that the information collected by GreenREE, may be collected directly or through tracking of your usage of the Website. The usage details may include IP addresses, details of Your computer equipment, browser, location, connections, any information that GreenREE may collect through the use of cookies and other tracking technologies. The collection of data may in most cases be automatic.

						</li>
						<li>
							“Cookies” are files that would be placed in Your system’s hard drive and are intended to improve Your User experience, by enabling Greenree to track Your usage and preference. These cookies may track Your Website usage, advertisement and links that You visit and other user data.Most cookies Greenree uses are limited to a session, which mean they will be automatically deleted when Your session closes. You may decline these cookies by changing Your browser settings, if permitted, however do note that this may impact certain features of the Website or Your user experience in using the Website.
						</li>
						<li>
							Advertisements on the Website may be posted by third-party advertisers, and such third parties may use cookies and other automatic tracking technologies to collect information about You, including but not limited to web behavioral information and patterns. Greenree does not control nor takes any responsibility for such third parties, their collection and use of information or their tracking technologies or how they may be used.
						</li>
						<li>
							You also may provide information to be published or displayed or posted on the Website, or transmitted to other users of the Website or third parties. Any such information is posted or transmitted to others at your own risk. Please be aware that Greenree cannot control the actions of other users of the Website with whom you may choose to share information with.
						</li>
					</ul>
				</div>
				<div class="information_use">
					<h3>Use of your information</h3>
					<ul>
						<li>The information that We collect on the Website will be used for the purposes of operating the Website, facilitating Your use of the Website, facilitating the transactions between the users, studying user behavior and for other business purposes of Greenree. The information including any User Information will be available to other registered users of the Website.
						</li>
						<li>
							Except as otherwise provided herein, no personally identifiable information will be disclosed or shared with any third party without Your express consent. For the purposes of this Privacy Policy, personally identifiable information shall mean name,age,gender, bank account information, telephone numbers, location data,email addresses,payment, billing or shipping information.
						</li>
						<li>
							Please do not include any personal information, personally identifiable information or sensitive personal information unless specifically requested by Greenree as part of the registration or other applicable processes. If Greenree determines that any information You have provided or uploaded violates the terms of this Privacy Policy, Greenree has the right, in its absolute discretion, to delete or destroy such information without incurring any liability to You.
						</li>
						<li>
							Greenree will not publish, sell or rent Your personal information to third parties for their marketing purposes without Your explicit consent
						</li>
						<li>
							Greenree may also use the information for analytical purposes, including but not limited to assessing usage data, usage patterns, estimate audience sizes and other similar activities.
						</li>
						<li>
							You agree that Your personal information may be used to contact You and deliver information or targeted advertisements, administrative notices and any other communication relevant to Your use of the Website. If You do not wish to receive these communications, You can at any time change Your profile settings.
						</li>
					</ul>
				</div>
				<div class="information_disclosure">
					<h3>Disclosure of your information</h3>
					<ul>
						<li>The User Information You provide on the Website may be disclosed by Greenree to the sellers on the Website, Greenree agents, employees, third party advertisers, subsidiaries and affiliates, or to other third party service providers of Greenree who require the information for the purposes of operating and maintaining the Website.
						</li>
						<li>
							Greenree will comply with requests and directions of all governmental, law enforcement or regulatory authorities, which it believes in good faith to be in accordance with any applicable law.Such compliance may include providing User Information, personally identifiable information or any other information to such agency or authority. By providing any information on the Website, You consent to Greenree providing such information to any governmental, law enforcement or regulatory authorities who exercise jurisdiction over Greenree and the Website.
						</li>
					</ul>
				</div>
				<div class="data_security">
					<h3>Data security</h3>
					<ul>
						<li>
							The information that You provide, subject to disclosure in accordance with this Privacy Policy shall be maintained in a safe and secure manner. Greenree databases and information are stored on secure servers with appropriate firewalls owned by Greenree or by third parties.
						</li>
						<li>
							As a user of the Website, You have the responsibility to ensure data security. You should use the Website in a responsible manner. Do not share Your username or password with any person. You are solely responsible for all acts done under the username You are registered under.
						</li>
						<li>
							Given the nature of internet transactions, Greenree does not take any responsibility for the transmission of information including User Information to the Website. Any transmission of User Information on the internet is done at Your risk. Greenree does not take any responsibility for You or any third party circumventing the privacy settings or security measures contained on the Website.
						</li>
						<li>
							While Greenree will use all reasonable efforts to ensure that Your User Information and other information submitted by You is safe and secure, it offers no representation, warranties or other assurances that the security measures are adequate, safe, fool proof or impenetrable.
						</li>
					</ul>
				</div>
				<div class="accessing_information">
					<h3>Accessing and updating your information</h3>
					<ul>
						<li>
							You may change, alter of otherwise modify or update Your User Information at any time by accessing the Website using Your registered username and accessing Your User account.
						</li>
						<li>
							You may also change and/or delete any of the information You have submitted. Do note however, Greenree reserves the rights to save any usage information and You are not entitled to seek the deletion of the same.
						</li>
						<li>
							Greenree at its sole discretion may permit or deny the change of any information, if it is believes the same is required to observe applicable laws.
						</li>
					</ul>
				</div>
				<div class="age_restriction">
					<h3>Age restrictions</h3>
					<ul>
						<li>
							The Website is only intended for users who are of 18 years of age or older and otherwise competent to enter into binding contracts. If You are not of the requisite age or otherwise unable to enter into binding contracts You are not to provide any User Information or other information. If it comes to Greenree attention that any User Information or information pertains to an individual under the age of 18 years or otherwise not eligible to enter into binding contracts, such User Information or information will be deleted without notice to You.
						</li>
					</ul>
				</div>
				<div class="limitation_liability">
					<h3>Limitation of liability</h3>
					<ul>
						<li>
							The aggregate liability of Greenree to You or anyone, whether in contract, tort, negligence or otherwise, howsoever arising, whether in connection with this Privacy Policy, Your access and use of the Website and its contents and functionalities shall not exceed Rs. 100/- (Indian Rupees One Hundred Only). In no event shall Greenree be liable for any loss of profits (anticipated or real), loss of business, loss of reputation, loss of data, loss of goodwill, any business interruption or any direct, indirect, special, incidental, consequential, punitive, tort or other damages, however caused, whether or not it has been advised of the possibility of such damages.
						</li>
					</ul>
				</div>
				<div class="amendments_policy">
					<h3>Amendments to the privacy policy</h3>
					<ul>
						<li>
							This Privacy Policy is subject to change at Greenree sole discretion. Any changes to this Privacy Policy will be notified by a notice on the home page.
						</li>
					</ul>
				</div>
				<div class="governing_law">
					<h3>Governing law</h3>
					<ul>
						<li>
							The Privacy Policy is governed by the laws of India (without regard to its conflict of laws principles). You irrevocably consent to the exclusive jurisdiction and venue of the competent courts located at Bangalore for all disputes arising out of or relating to the Privacy Policy.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>

