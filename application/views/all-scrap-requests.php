<?php $this->load->view('admin/templates/header', array(
	'link' => 'Scrap',
	'breadcrumbs' => array(
		0 => array('name'=>'Customer','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper{overflow-x:scroll !important; }
</style>

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">Scrap Request</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
    <!--<div class="output"></div>-->
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
<!-- //Modify Email part Start -->
<div id="modify_email" class="form-popup2">
  <div class="modal-content scrap_admin_message">
    <span class="close">&times;</span>
    <div>
    	<br>
        <label for="email"><b>Do You Want To Modify Send Notification Email?</b></label>
        <p>This is Your email</p><br>
        <div class="email_text"></div>
        <button class="btn modify_yes">Modify and Send</button>
        <button class="btn modify_no">Send Without Modification</button>
        <button class="btn dont_add">Cancel</button>
    </div>
  </div>
</div>

<div id="modify_email_text" class="form-popup2">
  <div class="modal-content scrap_admin_message">
    <span class="close">&times;</span>
    <div>
    	<br>
        <label for="email"><b>Write here Modify Text.</b></label><br>
         <textarea class="form-control" rows="5" id="email_comment"></textarea>
         <br/>
        <button class="btn modify_close">Send</button>
        <button class="btn dont_add">Cancel</button>
    </div>
  </div>
</div>

<script type="text/javascript">
jQuery(function() {
    var url      =  window.location.href;//alert(url);
    url = url.split("/");
    if (url[4] == 'edit') {
    	var reward_point = jQuery('input#reward_point').val();
    	var scrap_request_type = jQuery('#field-request_type').text();
    	if(scrap_request_type =='Non-Periodic'){
    	    jQuery('.periodic_request_status_pause').prop('checked', false);
    	    jQuery('#periodic_request_status_field_box').hide();
    	}
    	else{
    	    jQuery('#request_status_field_box').hide();
    	}
	    if (reward_point<1 || reward_point=='') {
	    	jQuery('input#reward_point').prop("readonly",false);
	    }
	    else{
	    	jQuery('input#reward_point').prop('readonly', true);
	    }
    }	
});
var modal = document.getElementById('modify_email_text');
var modal1 = document.getElementById('modify_email');
jQuery(document).ready(function(){
	jQuery('.close, .dont_add').click(function(){
		modal.style.display  = "none";
		modal1.style.display = "none";
	});
	jQuery("body").on('click','.close_scrap_notification,.close_scrap_success',function(event){
		modal.style.display  = "none";
		modal1.style.display = "none";
      	location.reload();
	});
	$(document).ready(function() {
  		$(function() {
			$('#datetimepicker6').datetimepicker({
                format: "YYYY-MM-DD HH:mm:ss"
			});
  		});
	});
	jQuery(document).ready(function() {
  		jQuery(function() {
			jQuery('#datetimepickermax').datetimepicker({
                format: "HH:mm:ss"
			});
  		});
	});
	// jQuery(document).ready(function() {
 //  		jQuery(function() {
	// 		jQuery('#datetimepickermin').datetimepicker({
 //                format: "HH:mm:ss"
	// 		});
 //  		});
	// });
});	
$(function () {
	
	//CHECK ALL BOXES
	$('.select-all').click(function () {
			
			if (!$(this).hasClass('checkall')) {
				var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
	            $('input[type="checkbox"]', allPages).prop('checked', false);
	        } else {
	        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
	        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
	        }
	        $(this).toggleClass('checkall');
				
	});
  	
	
	jQuery('document').ready(function(){
			
		var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
		$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
			
	});

</script>
<?php $this->load->view('admin/templates/footer'); ?>
