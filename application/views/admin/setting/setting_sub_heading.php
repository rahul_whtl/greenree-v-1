<ul class="nav nav-tabs" role="tablist" style="margin-bottom:3rem">
    <li role="presentation" class="<?php echo ($this->uri->segment(1) === 'pickup_config') ? 'active' : '' ?>">
        <?php echo anchor('pickup_config', 'Genral Pickup Settings') ?>
    </li>
    <li role="presentation" class="<?php echo ($this->uri->segment(1) === 'location_pickup_config') ? 'active' : '' ?>">
        <?php echo anchor('location_pickup_config', 'Location Pickup Setting') ?>
    </li>
    <li role="presentation" class="<?php echo ($this->uri->segment(1) === 'periodic_frequency_config') ? 'active' : '' ?>">
        <?php echo anchor('periodic_frequency_config', 'Periodic Frequency Setting') ?>
    </li>
</ul>