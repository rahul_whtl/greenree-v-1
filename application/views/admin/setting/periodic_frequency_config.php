<?php $this->load->view('admin/templates/header', array(
	'title' => 'GreenREE - Scrap Configuration',
	'link' => 'users'//,
	//'breadcrumbs' => array(
	//	0 => array('name'=>'Users','link'=>'admin-view-users'),
	//	1 => array('name'=>$person ? $person->username : 'Not found','link'=>FALSE),
	//)
)); ?> 

<?php //$this->load->view('admin/setting/setting_sub_heading'); ?>
<style type="text/css">
.breadcrumb{display: none;}
</style>
<?php $alert = $this->session->flashdata('custom_message');?>
<?php if (isset($alert['type'])): ?>
	<div class="alert alert-<?php echo $alert['type']; ?>">
		<?php echo $alert['message'] ?>
	</div>
<?php endif ?>
<div class="page-header b-0">
	<div class="lead pull-left">Periodic frequency settings</div>
	<div class="clearfix"></div>
</div>
<?php if (! empty($message)): ?>
	<?php if (validation_errors()): ?>
		<div class="alert alert-danger">Check the form for errors and try again.</div>
		<?php echo validation_errors(); ?>
	<?php else: ?>
		<div id="message"><?= $message; ?></div>
	<?php endif ?>
<?php endif ?>

<?php echo form_open(current_url()); ?>
<div class="container">
	<div class="periodic_frequency_config">
		<div class="periodic_frequency col-md-12">
			<div class="col-md-4 frequency_name_div">
				<label>Frequency Name<span class="red">*</span></label>
				<input type='text' name="frequency_name" id="frequency_name" class="form-group frequency_name form-control" placeholder="Enter frequency name" />
				<span class="frequency_name_validation"></span>
			</div>
			<div class="col-md-4 frequency_value_div">
				<label>Frequency value (In week)<span class="red">*</span>  </label>
              	<input type='text' name="frequency_value" id="frequency_value" class="form-group frequency_value form-control" placeholder="Enter frequency value in week"/>
              	<span class="frequency_value_validation"></span>
			</div>
			<input type="submit" name="insert_frequency" value="Submit" class="btn btn-success insert_frequency"/>
		</div>
		<div class="frequency_list">
			<h3>Frequency List</h3>
			<?php if(empty($periodic_frequency)):?>
				<p>There is no schedule periodic frequency list.</p>
			<?php else: ?>	
				<table class="table table-bordered">
			        <thead>
			          	<tr>
				            <th scope="col"></th>
				            <th scope="col">Frequency Name</th>
				            <th scope="col">Frequency value(in weeks)</th>
			          	</tr>
			        </thead>
			        <tbody>
			        	<?php foreach($periodic_frequency as $frequency_list): ?>
			          	<tr>
			            	<td>
				              	<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="selected[]" value="<?php echo $frequency_list->id; ?>">
				              	</div>
			            	</td>
			            	<td><?php echo $frequency_list->frequency_name; ?></td>
			            	<td>
			            		<?php echo $frequency_list->frequency_value; ?>
			            	</td>
			          	</tr>
			          <?php endforeach ?>
			        </tbody>    
		  		</table>
	  			<input type="submit" name="delete_frequency" value="Delete Frequency" class="btn btn-danger" style="float: right;"> 
		    <?php endif ?>
		</div>	
	</div>
</div>

<?php echo form_close(); ?>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery("body").on('click','.insert_frequency',function(event){
			var frequency_name  = jQuery('.frequency_name').val();
			var frequency_value = jQuery('.frequency_value').val();
			var error = false;
			if (frequency_name == '' || frequency_name == undefined){
				jQuery('.frequency_name_validation').addClass('text-danger');
	  	    	jQuery('.frequency_name_validation').text('Please enter periodic frequency name');
				error = true;
			}
			if (frequency_value == '' || frequency_value == undefined){
				jQuery('.frequency_value_validation').addClass('text-danger');
	  	    	jQuery('.frequency_value_validation').text('Please enter periodic frequency value');
				error = true;	
			}
			if (error) {
				return false;
			}
		});
		jQuery('#frequency_name').on('focus', function(){
			jQuery('.frequency_name_validation').text('');
		});
		jQuery('#frequency_value').on('focus', function(){
			jQuery('.frequency_value_validation').text('');
		});
	});
</script>
<?php $this->load->view('admin/templates/footer') ?>