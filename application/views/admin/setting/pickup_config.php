<?php $this->load->view('admin/templates/header', array(
	'title' => 'GreenREE - Scrap Configuration',
	'link' => 'users'
	//'breadcrumbs' => array(
	//	0 => array('name'=>'Users','link'=>'admin-view-users'),
	//	1 => array('name'=>$person ? $person->username : 'Not found','link'=>FALSE),
	//)
)); ?>

<?php //$this->load->view('admin/setting/setting_sub_heading'); ?>
<style type="text/css">
.breadcrumb{display: none;}
</style>
<?php $alert = $this->session->flashdata('custom_message');?>
<?php if (isset($alert['type'])): ?>
	<div class="alert alert-<?php echo $alert['type']; ?>">
		<?php echo $alert['message'] ?>
	</div>
<?php endif ?>

<div class="page-header b-0">
	<div class="lead pull-left">Non-pickup days settings</div>
	<div class="clearfix"></div>
</div>
<?php if (! empty($message)): ?>
	<?php if (validation_errors()): ?>
		<div class="alert alert-danger">Check the form for errors and try again.</div>
		<?php echo validation_errors(); ?>
	<?php else: ?>
		<div id="message"><?= $message; ?></div>
	<?php endif ?>
<?php endif ?>
<?php echo form_open(current_url()); ?>
<!-- days configuration part start -->
<select class="js-example-basic-multiple col-md-4" name="config_days[]" multiple="multiple">
	<option value="Sunday" <?php echo $pickup_config_data->sunday==1?'selected':''?>>Sunday</option>
	<option value="Monday" <?php echo $pickup_config_data->monday==1?'selected':''?>>Monday</option>
	<option value="Tuesday" <?php echo $pickup_config_data->tuesday==1?'selected':''?>>Tuesday</option>
	<option value="Wednesday" <?php echo $pickup_config_data->wednesday==1?'selected':''?>>Wednesday</option>
	<option value="Thursday" <?php echo $pickup_config_data->thursday==1?'selected':''?>>Thursday</option>
	<option value="Friday" <?php echo $pickup_config_data->friday==1?'selected':''?>>Friday</option>
	<option value="Saturday" <?php echo $pickup_config_data->saturday==1?'selected':''?>>Saturday</option>
</select>
<script type="text/javascript">
	$(document).ready(function() {
	    $('.js-example-basic-multiple').select2();
	});
</script>	
<input type="checkbox" name="apply_globally_days" class="apply_globally_days" value="1" style="margin-left: 30px" <?php echo $pickup_config_data->general_setting_global==1?'checked':''?>>
<label style="margin-right: 30px">Apply globally</label>
<input type="submit" name="update_days" value="Update Pickup Days" class="btn btn-success update_days"/>
<!-- days configuration part end -->

<!-- date configuration part start -->
<div class="page-header b-0">
	<div class="lead pull-left">Non-pickup date settings</div>
	<div class="clearfix"></div>
</div>	
<div class="holiday_config">
	<div class="holiday_date col-md-5">
		<div>
			<label>Holiday Name<span class="red">*</span>  </label>
			<input type='text' name="holiday_name" id="holiday_name" class="form-group form-control" placeholder="Enter holiday name" />
			<span class="holiday_name_validation"></span>
		</div>
		<div>
			<label>From Date<span class="red">*</span>  </label>
			<div class="pick_up_date form-group">
			    <div class='input-group date' id='datetimepicker1'>
	              	<input type='text' name="from_date" id="from_date" class="form-group form-control" placeholder="Select Pick Up Date" />
	              	<span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	              	</span>
	            </div>
              	<span class="from_date_validation"></span>
			</div>
		</div>
		<div>
			<label>To Date</label>
			<div class="pick_up_date form-group">
			    <div class='input-group date' id='datetimepicker2'>
	              	<input type='text' name="to_date" id="to_date" class="form-group form-control" placeholder="Select Pick Up Date" />
	              	<span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	              	</span>
	            </div>
				<span class="to_date_validation"></span>
			</div>
		</div>
		<div style="margin-bottom: 20px;">
			<input type="checkbox" name="apply_globally_date" class="apply_globally_date" value="1">
			<label>Apply globally</label>
		</div>
		<input type="submit" name="update_date" value="Update Date Settings" class="btn btn-success update_locations"/>
	</div>
	<div class="holiday_list col-md-5 col-md-offset-2">
		<?php if(empty($select_config_date)):?>
			<p>There no schedule holiday list.</p>
		<?php else: ?>	
			<table class="table table-bordered">
		        <thead>
		          	<tr>
			            <th scope="col"></th>
			            <th scope="col">Holiday Name</th>
			            <th scope="col">Date</th>
		          	</tr>
		        </thead>
		        <tbody>
		        	<?php foreach($select_config_date as $holiday_details): ?>
		          	<tr>
		            	<td>
			              	<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" name="selected[]" value="<?php echo $holiday_details->id; ?>">
			              	</div>
		            	</td>
		            	<td><?php echo $holiday_details->holiday_name; ?></td>
		            	<td>
		            		<?php echo $holiday_details->holiday_date == '0000-00-00' ? '--' : $holiday_details->holiday_date; ?>
		            	</td>
		          	</tr>
		          <?php endforeach ?>
		        </tbody>    
	  		</table>
  			<input type="submit" name="delete_hoilday" value="Delete Selected Items" class="btn btn-danger" style="float: right;">  		
	    <?php endif ?>
	</div>
</div>		
<!-- date configuration part end -->
<?php echo form_close(); ?>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		var date = new Date();
		date.setDate(date.getDate());
		jQuery(function() {
			jQuery('#datetimepicker1').datetimepicker({
                format: "YYYY-MM-DD",
                minDate: date,
                daysOfWeekDisabled: [<?php 
                	echo $pickup_config_data->sunday==1?'0,':'';
                	echo $pickup_config_data->monday==1?'1,':'';
                	echo $pickup_config_data->tuesday==1?'2,':'';
                	echo $pickup_config_data->wednesday==1?'3,':'';
                	echo $pickup_config_data->thursday==1?'4,':'';
                	echo $pickup_config_data->friday==1?'5,':'';
                	echo $pickup_config_data->saturday==1?'6':'';
                	?>],
                disabledDates: [
                		<?php foreach($select_config_date as $holiday_details){
                			echo 'moment("'.$holiday_details->holiday_date.'"),';	
                        }?>
                    ],
			});
  		});
  		jQuery(function() {
			jQuery('#datetimepicker2').datetimepicker({
                format: "YYYY-MM-DD",
                minDate: date,
                daysOfWeekDisabled: [<?php 
                	echo $pickup_config_data->sunday==1?'0,':'';
                	echo $pickup_config_data->monday==1?'1,':'';
                	echo $pickup_config_data->tuesday==1?'2,':'';
                	echo $pickup_config_data->wednesday==1?'3,':'';
                	echo $pickup_config_data->thursday==1?'4,':'';
                	echo $pickup_config_data->friday==1?'5,':'';
                	echo $pickup_config_data->saturday==1?'6':'';
                	?>],
                disabledDates: [
                		<?php foreach($select_config_date as $holiday_details){
                			echo 'moment("'.$holiday_details->holiday_date.'"),';	
                        }?>
                    ],
			});
  		});
  		jQuery("body").on('click', '.update_locations',function(event){
	  	    var holiday_name = jQuery('#holiday_name').val();
	  	    var from_date 	 = jQuery('#from_date').val();
	  	    var to_date   	 = jQuery('#to_date').val();
	  	    var error = false;
	  	    if(holiday_name=='' || holiday_name==undefined){
	  	    	jQuery('.holiday_name_validation').addClass('text-danger');
	  	    	jQuery('.holiday_name_validation').text('Please enter holiday name');
	  	    	error = true;
	  	    }
	  	    if(from_date == '' || from_date ==undefined){
	  	    	jQuery('.from_date_validation').addClass('text-danger');
	  	    	jQuery('.from_date_validation').text('Please select date');
	  	    	error = true;
	  	    }
	  	    // if(to_date == '' || to_date == undefined){
	  	    // 	jQuery('.to_date_validation').addClass('text-danger');
	  	    // 	jQuery('.to_date_validation').text('Please select date');
	  	    // 	error = true;
	  	    // }
	  	    if(to_date < from_date){
	  	    	jQuery('.to_date_validation').addClass('text-danger');
	  	    	jQuery('.to_date_validation').text('To date can not be less than from date');
	  	    	error = true;
	  	    }
	  	    if(error){
	  	    	return false;
	  	    }
	  	});
	  	
    });	
</script>
<?php $this->load->view('admin/templates/footer') ?>