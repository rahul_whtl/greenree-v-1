<?php $this->load->view('admin/templates/header', array(
	'title' => 'GreenREE - Scrap Configuration',
	'link' => 'users'
	//'breadcrumbs' => array(
	//	0 => array('name'=>'Users','link'=>'admin-view-users'),
	//	1 => array('name'=>$person ? $person->username : 'Not found','link'=>FALSE),
	//)
)); ?> 

<?php //$this->load->view('admin/setting/setting_sub_heading'); ?>
<style type="text/css">
.location_pickup_config .select_days{padding-left: 0}
.breadcrumb{display: none;}
</style>

<?php $alert = $this->session->flashdata('custom_message');?>
<?php if (isset($alert['type'])): ?>
	<div class="alert alert-<?php echo $alert['type']; ?>">
		<?php echo $alert['message'] ?>
	</div>
<?php endif ?>

<?php if (! empty($message)): ?>
	<?php if (validation_errors()): ?>
		<div class="alert alert-danger">Check the form for errors and try again.</div>
		<?php echo validation_errors(); ?>
	<?php else: ?>
		<div id="message"><?= $message; ?></div>
	<?php endif ?>
<?php endif ?>

<?php echo form_open(current_url()); ?>
<div class="container">
	<div class="location_pickup_config">
		<!-- particular location days configuration part start -->
		<div class="row location_address">
			<div class="page-header b-0">
				<div class="lead pull-left">Particular location settings</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-4 form-group-country">
			    <div class="form-group">
			        <label class="control-label" for="country">Country <span class="red">*</span></label>
			        <select name="country" id="country" class="form-control">
			            <?php if(sizeof($countries) == 1){ ?>
			    			    <option value="<?php echo $countries[0]['loc_name'];?>" data-val="<?php echo $countries[0]['loc_id'];?>" selected><?php echo $countries[0]['loc_name'];?></option> 
			            <?php }else{ foreach($countries as $country) { ?>
							<option value="<?php echo $country['loc_name'];?>" data-val="<?php echo $country['loc_id'];?>">
								<?php echo $country['loc_name'];?>
							</option>
			            <?php } } ?>
			        </select>
			        <span class="text-danger text-danger-country"></span>
			    </div>
			</div>
			<div class="col-md-4 form-group-state">
			    <div class="form-group">
			        <label class="control-label" for="state">State <span class="red">*</span></label>
			        <select name="state" id="state" class="form-control">
			            <?php if(sizeof($states) == 1){ ?>
			    			    <option value="<?php echo $states[0]['loc_name'];?>" data-val="<?php echo $states[0]['loc_id'];?>" selected><?php echo $states[0]['loc_name'];?></option> 
			            <?php }else{ foreach($states as $state) { ?>
							<option value="<?php echo $state['loc_name'];?>" data-val="<?php echo $state['loc_id'];?>">
								<?php echo $state['loc_name'];?>
							</option>
			            <?php } } ?>
					</select>
					<span class="text-danger text-danger-state"></span>
			    </div>
			</div>
			<div class="col-md-4 form-group-city">
			    <div class="form-group">
			        <label class="control-label" for="city">City <span class="red">*</span></label>
			        <input list="city_list" name="city" id="city" class="form-control" autocomplete="off" value="<?php if($user_address->city) echo $user_address->city;elseif($user_address->country == '' && sizeof($cities) == 1)echo $cities[0]['loc_name'];?>" placeholder="Select or Enter your City">
					<datalist id="city_list">
						<?php if(sizeof($cities) == 1){ ?>
						    <option value="<?php echo $cities[0]['loc_name'];?>" data-val="<?php echo $cities[0]['loc_id'];?>"    selected><?php echo $cities[0]['loc_name'];?></option>
						<?php }else{ foreach($cities as $city) { ?>
						<option value="<?php echo $city['loc_name'];?>" data-val="<?php echo $city['loc_id'];?>">
							<?php echo $city['loc_name'];?>
						</option>
					    <?php } ?>
			            <?php } ?>
					</datalist>
					<span class="text-danger text-danger-city"></span>
			    </div>
			</div>
			<div class="col-md-4 form-group-locality">
			    <div class="form-group">
			        <label class="control-label" for="locality">Locality <span class="red">*</span></label>
			        <input list="locality_list" name="locality" id="locality" class="form-control" autocomplete="off" value="<?php if($user_address->locality) echo $user_address->locality;elseif($user_address->country == '' && sizeof($localities) == 1)echo $localities[0]['loc_name'];?>" placeholder="Select or Enter your Locality">
			        <datalist id="locality_list">
					    <?php if(sizeof($localities) == 1){ ?>
							   <option value="<?php echo $localities[0]['loc_name'];?>" data-val="<?php echo $localities[0]['loc_id'];?>" selected><?php echo $localities[0]['loc_name'];?>
							   </option>
						<?php }else{ foreach($localities as $locality) { ?>
						    <option value="<?php echo $locality['loc_name'];?>" data-val="<?php echo $locality['loc_id'];?>">
							<?php echo $locality['loc_name'];?>
							</option>
					    <?php } ?>
			            <?php } ?>
			        </datalist>
			        <span class="text-danger text-danger-locality"></span>
			    </div>
			</div>
			<div class="col-md-4 form-group-apartment-name">
			    <div class="form-group">
			        <label class="control-label" for="apartment_name">Apartment</label>
			        <input list="apartment_list" name="apartment_name" id="apartment" class="form-control" autocomplete="off" value="<?php if($user_address->apartment_name) echo $user_address->apartment_name;elseif($user_address->country == '' && sizeof($apartments) == 1)echo $apartments[0]['loc_name'];?>" placeholder="Select or Enter your Apartment">
			        <datalist id="apartment_list"  data-dropup-auto="false">
						    <?php if(sizeof($apartments) == 1){ ?>
							    <option value="<?php echo $apartments[0]['loc_name'];?>" data-val="<?php echo $apartments[0]['loc_id'];?>" selected><?php echo $apartments[0]['loc_name'];?></option>
							<?php }else{ foreach($apartments as $apartment) { ?>
							<option value="<?php echo $apartment['loc_name'];?>" data-val="<?php echo $apartment['loc_id'];?>">
								<?php echo $apartment['loc_name'];?>
							</option>
						    <?php } } ?>	
			        </datalist>
			        <span class="text-danger text-danger-apartment"></span>
			    </div>
			</div>
		</div>
		<!-- particular location days configuration part end -->

		<!-- Panel heading start -->
		<div class="panel panel-default panel-inverse holidays_type">
			<div class="panel-heading">Holidays Type</div>
			<div class="panel-body">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="<?php echo $last_open_tab=='date_tab'?'':'active'?>">
						<a href="#holiday-days-list" aria-controls="holiday-days-list" role="tab" data-toggle="tab">
							Days
						</a>
					</li>
					<li role="presentation" class="<?php echo $last_open_tab=='date_tab'?'active':''?>">
						<a href="#holiday-dates-list" aria-controls="holiday-dates-list" role="tab" data-toggle="tab">
							Dates
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane <?php echo $last_open_tab=='date_tab'?'':'active'?>" id="holiday-days-list">
						<!-- days configuration part start -->
						<div class="days_setting">
							<div class="page-header col-md-12">
								<div class="lead pull-left">Non-pickup days settings</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-5 select_days">
								<label>Select days<span class="red">*</span></label>
								<select class="form-group form-control js-example-basic-multiple col-md-4" id="config_days" name="config_days[]" multiple="multiple">
									<option value="Sunday" <?php echo $pickup_config_data->sunday==1?'selected':''?>>Sunday</option>
									<option value="Monday" <?php echo $pickup_config_data->monday==1?'selected':''?>>Monday</option>
									<option value="Tuesday" <?php echo $pickup_config_data->tuesday==1?'selected':''?>>Tuesday</option>
									<option value="Wednesday" <?php echo $pickup_config_data->wednesday==1?'selected':''?>>Wednesday</option>
									<option value="Thursday" <?php echo $pickup_config_data->thursday==1?'selected':''?>>Thursday</option>
									<option value="Friday" <?php echo $pickup_config_data->friday==1?'selected':''?>>Friday</option>
									<option value="Saturday" <?php echo $pickup_config_data->saturday==1?'selected':''?>>Saturday</option>
								</select>
								<script type="text/javascript">
									$(document).ready(function() {
									    $('.js-example-basic-multiple').select2();
									});
								</script>	
								<span class="text-danger-config-day"></span>
							</div>	
							<input type="checkbox" name="location_globally_days" class="location_globally_days hide" value="1" style="margin-left: 30px">
							<label style="margin-right: 30px" class="hide">Apply globally</label>
							<input type="button" name="update_days" value="Update Pickup Days" class="btn btn-success update_days"/>
						<!-- Disapying diabled days list for apartment and locality part start -->
							<div class="holiday_list col-md-12">
								<div class="col-md-5" style="padding: 0">
									<h4>Holidays list for specific locality</h4>
									<?php if(empty($config_locality)):?>
											<p>There no schedule holiday list.</p>
										<?php else: ?>
										<table class="table table-bordered">
									        <thead>
									          	<tr>
										            <th scope="col"></th>
										            <th scope="col">Locality Name</th>
										            <th scope="col">Holidays Days</th>
									          	</tr>
									        </thead>
									        <tbody>
									        	<?php foreach($config_locality as $locality_details): ?>
									          	<tr>
									            	<td>
										              	<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input" name="locality_days[]" value="<?php echo $locality_details->id; ?>">
										              	</div>
									            	</td>
									            	<td><?php echo $locality_details->loc_name; ?></td>
									            	<td>
									            		<?php echo $locality_details->sunday? 'Sunday ' : '';
									            			  echo $locality_details->monday? 'Monday ' : '';
									            			  echo $locality_details->tuesday? 'Tuesday ' : '';	
									            			  echo $locality_details->wednesday? 'Wednesday ' : '';
									            			  echo $locality_details->thursday? 'Thursday ' : '';
									            			  echo $locality_details->friday? 'Friday ' : '';
									            			  echo $locality_details->saturday? 'Saturday ' : '';
									            		?>
									            	</td>
									          	</tr>
									          <?php endforeach ?>
									        </tbody>   
										</table>
										<input type="submit" name="delete_locality_days" value="Delete Selected Locality" class="btn btn-danger delete_locality_days">
								    <?php endif ?> 
							    </div>
							    <div class="col-md-5 col-md-offset-2" style="padding: 0">
									<h4>Holidays days list for specific apartment</h4>
									<?php if(empty($config_apartment)):?>
										<p>There is no schedule holiday list.</p>
									<?php else: ?>
										<table class="table table-bordered">	
									        <thead>
									          	<tr>
										            <th scope="col"></th>
										            <th scope="col">Locality Name</th>
										            <th scope="col">Apartment Name</th>
										            <th scope="col">Holidays Days</th>
									          	</tr>
									        </thead>
									        <tbody>
									        	<?php foreach($config_apartment as $apartment_details): ?>
									          	<tr>
									            	<td>
										              	<div class="custom-control custom-checkbox">
															<input type="checkbox" class="custom-control-input" name="apartment_days[]" value="<?php echo $apartment_details->id; ?>">
										              	</div>
									            	</td>
									            	<td><?php echo $apartment_details->locality_name; ?></td>
									            	<td><?php echo $apartment_details->loc_name; ?></td>
									            	<td>
									            		<?php echo $apartment_details->sunday? 'Sunday ' : '';
									            			  echo $apartment_details->monday? 'Monday ' : '';
									            			  echo $apartment_details->tuesday? 'Tuesday ' : '';	
									            			  echo $apartment_details->wednesday? 'Wednesday ' : '';
									            			  echo $apartment_details->thursday? 'Thursday ' : '';
									            			  echo $apartment_details->friday? 'Friday ' : '';
									            			  echo $apartment_details->saturday? 'Saturday ' : '';
									            		?>
									            	</td>
									          	</tr>
									          <?php endforeach ?>
									        </tbody>  
										</table>
										<input type="submit" name="delete_apartment_days" value="Delete Selected Apartment" class="btn btn-danger delete_apartment_days">
									<?php endif ?>  
								</div>	
							</div>
						<!-- Disapying diabled days list for apartment and locality part end -->
						</div>
						<!-- days configuration part end -->
					</div>
					<div role="tabpanel" class="tab-pane <?php echo $last_open_tab=='date_tab'?'active':''?>" id="holiday-dates-list">
						<!-- date configuration part start -->
						<div class="holiday_date_list">
							<div class="page-header col-md-12">
								<div class="lead pull-left">Non-pickup date settings</div>
								<div class="clearfix"></div>
							</div>	
							<div class="holiday_config">
								<div class="holiday_date col-md-12">
									<div class="col-md-3">
										<label>Holiday Name<!-- <span class="red">*</span> --></label>
										<input type='text' name="holiday_name" id="holiday_name" class="form-group form-control" placeholder="Enter holiday name" />
										<span class="holiday_name_validation"></span>
									</div>
									<div class="col-md-3 from_date_div">
										<label>From Date<span class="red">*</span>  </label>
										<div class="pick_up_date form-group">
										    <div class='input-group date' id='datetimepicker1'>
								              	<input type='text' name="from_date" id="from_date" class="form-group form-control" placeholder="Select Pick Up Date" />
								              	<span class="input-group-addon">
								                    <span class="glyphicon glyphicon-calendar"></span>
								              	</span>
								            </div>
							              	<span class="from_date_validation"></span>
										</div>
									</div>
									<div class="col-md-3 to_date_div">
										<label>To Date<!-- <span class="red">*</span> --></label>
										<div class="pick_up_date form-group">
										    <div class='input-group date' id='datetimepicker2'>
								              	<input type='text' name="to_date" id="to_date" class="form-group form-control" placeholder="Select Pick Up Date" />
								              	<span class="input-group-addon">
								                    <span class="glyphicon glyphicon-calendar"></span>
								              	</span>
								            </div>
											<span class="to_date_validation"></span>
										</div>
									</div>
									<div class="col-md-5">
										<input type="checkbox" name="location_globally_date" class="location_globally_date hide" value="1">
										<label style="margin-right: 30px" class="hide">Apply globally</label>
										<input type="button" name="update_date" value="Update Date Settings" class="btn btn-success update_date"/>
									</div>
								</div>
							</div>	
							<!-- Disapying diabled date list for apartment and locality part start -->
							<div class="holiday_list col-md-12">
								<div class="col-md-5" style="padding: 0">
								    <h4>Holidays date list for specific locality</h4>
								    <?php if(empty($config_date_locality)):?>
										<p>There no schedule holiday list.</p>
									<?php else: ?>	
									<table class="table table-bordered">
								        <thead>
								          	<tr>
									            <th scope="col"></th>
									            <th scope="col">Locality Name</th>
									            <th scope="col">Holiday Name</th>
									            <th scope="col">Date</th>
								          	</tr>
								        </thead>
								        <tbody>
								        	<?php foreach($config_date_locality as $holiday_details): ?>
								          	<tr>
								            	<td>
									              	<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="locality_dates[]" value="<?php echo $holiday_details->id; ?>">
									              	</div>
								            	</td>
								            	<td><?php echo $holiday_details->loc_name; ?></td>
								            	<td><?php echo $holiday_details->holiday_name; ?></td>
								            	<td>
								            		<?php echo $holiday_details->holiday_date == '0000-00-00' ? '--' : $holiday_details->holiday_date; ?>
								            	</td>
								          	</tr>
								          <?php endforeach ?>
								        </tbody>    
									</table>
									<input type="submit" name="delete_locality_dates" value="Delete Selected Locality" class="btn btn-danger delete_locality_dates">
								    <?php endif ?>
								</div> 
								<div class="col-md-5 col-md-offset-2" style="padding: 0">   
								    <h4>Holidays date list for specific apartment</h4>
									<?php if(empty($config_date_apartment)):?>
										<p>There is no schedule holiday list.</p>
									<?php else: ?>	
									<table class="table table-bordered">
								        <thead>
								          	<tr>
									            <th scope="col"></th>
									            <th scope="col">Locality Name</th>
									            <th scope="col">Apartment Name</th>
									            <th scope="col">Holiday Name</th>
									            <th scope="col">Date</th>
								          	</tr>
								        </thead>
								        <tbody>
								        	<?php foreach($config_date_apartment as $holiday_details): ?>
								          	<tr>
								            	<td>
									              	<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="apartment_dates[]" value="<?php echo $holiday_details->id; ?>">
									              	</div>
								            	</td>
								            	<td><?php echo $holiday_details->locality_name; ?></td>
								            	<td><?php echo $holiday_details->loc_name; ?></td>
								            	<td><?php echo $holiday_details->holiday_name; ?></td>
								            	<td>
								            		<?php echo $holiday_details->holiday_date == '0000-00-00' ? '--' : $holiday_details->holiday_date; ?>
								            	</td>
								          	</tr>
								          <?php endforeach ?>
								        </tbody>    
									</table>
									<input type="submit" name="delete_apartment_dates" value="Delete Selected Apartment" class="btn btn-danger delete_apartment_dates">
								    <?php endif ?>
								</div>    
							</div>
							<!-- Disapying diabled date list for apartment and locality part end -->
						</div>		
						<!-- date configuration part end -->
					</div>
				</div>
			</div>
		</div>
		<!-- Panel heading end -->		
	</div>
</div>
<!-- success model part start -->
<div id="holiday_modal" class="form-popup2">
	<div class="modal-content holiday_modal_message">
		<div class="close">&times;</div><br>
		<div>
			<br>
			<div class="alert alert-success">
			  	
			</div>
		    <button class="btn btn-success close_btn">Close</button>
	    </div>
	</div>
</div>
<!-- success model part end -->
<?php echo form_close(); ?>
<script type="text/javascript">
	var modal = document.getElementById('holiday_modal');
	jQuery(document).ready(function(){ 
		jQuery("body").on('click','.close,.close_btn',function(event){
			modal.style.display  = "none";
	      	location.reload();
		});
		//for address part end
		var country_value = jQuery('#country option:selected', this).val();
		var state_value = jQuery('#state option:selected', this).val();
		var city_value = jQuery('#city').val();
		var locality_value = jQuery('#locality', this).val();
		if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
        	jQuery('#state').attr("disabled", true);
        	jQuery('#city').attr("disabled", true);
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(state_value=='' || state_value=='- State -' || state_value=='0'){
        	jQuery('#city').attr("disabled", true);
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(city_value=='' || city_value=='- city -'){
        	jQuery('#locality').attr("disabled", true);
        	jQuery('#apartment').attr("disabled", true);
        }
        else if(locality_value=='' || locality_value=='- apartment -'){
        	jQuery('#apartment').attr("disabled", true);
        }
		//for address part end
		var date = new Date();
		date.setDate(date.getDate());
		jQuery(function() {
			jQuery('#datetimepicker1').datetimepicker({
                format: "YYYY-MM-DD",
                minDate: date,
			});
  		});
  		jQuery(function() {
			jQuery('#datetimepicker2').datetimepicker({
                format: "YYYY-MM-DD",
                minDate: date,
			});
  		});
  		jQuery("body").on('click', '.update_date ,.update_days',function(event){
	  	    var btn_value = jQuery(this).val();
	  	    var config_days = jQuery(".js-example-basic-multiple").select2("val");
	  	    var holiday_name = jQuery('#holiday_name').val();
	  	    var from_date 	 = jQuery('#from_date').val();
	  	    var to_date   	 = jQuery('#to_date').val();
	  	    var country    = jQuery('#country option:selected').val();
	  	    var country_id = jQuery('#country option:selected').attr('data-val');
			var state      = jQuery('#state option:selected').val();
			var state_id   = jQuery('#state option:selected').attr('data-val');
			var city         = jQuery('#city').val();
			var city_id      = jQuery('#city_list').find("[value='"+city+"']").attr('data-val');
			var locality     = jQuery('#locality').val();
			var locality_id  = jQuery('#locality_list').find("[value='"+locality+"']").attr('data-val');
			var apartment    = jQuery('#apartment').val();
			var apartment_id = jQuery('#apartment_list').find("[value='"+apartment+"']").attr('data-val');
			var location_globally_days = jQuery('.location_globally_days').val();
			var location_globally_date = jQuery('.location_globally_date').val();
	  	    var error = false;
	  	    if(btn_value=='Update Date Settings'){
		  	    // if(holiday_name=='' || holiday_name==undefined){
		  	    // 	jQuery('.holiday_name_validation').addClass('text-danger');
		  	    // 	jQuery('.holiday_name_validation').text('Please enter holiday name');
		  	    // 	error = true;
		  	    // }
		  	    if(from_date == '' || from_date ==undefined){
		  	    	jQuery('.from_date_validation').addClass('text-danger');
		  	    	jQuery('.from_date_validation').text('Please select date');
		  	    	error = true;
		  	    }
		  	    // if(to_date == '' || to_date == undefined){
		  	    // 	// 	jQuery('.to_date_validation').addClass('text-danger');
		  	    // // 	jQuery('.to_date_validation').text('Please select date');
		  	    // // 	error = true;
		  	    // }	
		  		if(to_date && to_date < from_date){
		  	    	jQuery('.to_date_validation').addClass('text-danger');
		  	    	jQuery('.to_date_validation').text('To date can not be less than from date');
		  	    	error = true;
		  	    }
	  	    }
	  	    else{
	  	    	if(config_days == '' || config_days ==undefined){
		  	    	jQuery('.text-danger-config-day').addClass('text-danger');
		  	    	jQuery('.text-danger-config-day').text('Please select day');
		  	    	error = true;
		  	    }
	  	    }	
	  	    if(country == '' || country == undefined){
	  	    	jQuery('.text-danger-country').addClass('text-danger');
	  	    	jQuery('.text-danger-country').text('Please select country');
	  	    	error = true;
	  	    }
	  	    if(state == '' || state ==undefined){
	  	    	jQuery('.text-danger-state').addClass('text-danger');
	  	    	jQuery('.text-danger-state').text('Please select state');
	  	    	error = true;
	  	    }
	  	    if(city == '' || city ==undefined){
	  	    	jQuery('.text-danger-city').addClass('text-danger');
	  	    	jQuery('.text-danger-city').text('Please select city');
	  	    	error = true;
	  	    }
	  	    if(locality == '' || locality ==undefined){
	  	    	jQuery('.text-danger-locality').addClass('text-danger');
	  	    	jQuery('.text-danger-locality').text('Please select locality');
	  	    	error = true;
	  	    }
	  	    // if(apartment == '' || apartment ==undefined){
	  	    // 	jQuery('.text-danger-apartment').addClass('text-danger');
	  	    // 	jQuery('.text-danger-apartment').text('Please select apartment');
	  	    // 	error = true;
	  	    // }
	  	    if(error){
	  	    	return false;
	  	    }
	  	    jQuery.ajax({
	        	type: 'POST',
	        	data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',country_id : country_id,state_id : state_id,city_id : city_id,locality_id : locality_id,apartment_id : apartment_id,config_days:config_days,holiday_name:holiday_name,from_date:from_date,to_date:to_date,btn_value:btn_value,location_globally_days:location_globally_days,location_globally_date:location_globally_date},
	            url: '<?php echo base_url('particular_location_config'); ?>',
	            cache: false,
	            success: function(response){console.log(response);
	            	if(response==1){
	            		var txt = 'Apartment name : '+apartment+' is disabled from : '+from_date+' to '+to_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	            	}
                    else if(response==2){
	            		var txt = 'Apartment name : '+apartment+' is disabled from : '+from_date+' to '+to_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==3){
	                	var txt = 'Locality name : '+locality+' is disabled from : '+from_date+' to '+to_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==4){
	                	var txt = 'Locality name : '+locality+' is disabled from : '+from_date+' to '+to_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==5){
	            		var txt = 'Apartment name : '+apartment+'is disabled on '+config_days;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	            	}
                    else if(response==6){
	            		var txt = 'Apartment name : '+apartment+'is disabled on '+config_days;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==7){
	                	var txt = 'Error in updating form';
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==8){
	                	var txt = 'Locality name : '+locality+' is disabled on '+config_days;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==9){
	                	var txt =  'Locality name : '+locality+' is disabled on '+config_days;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else{
	                	var txt = 'Error in updating form';
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	            }
	        });
	  	});
	  	jQuery('#country').on('change',function(){
	    	country_value = jQuery(this).val();
            var country_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            jQuery('#state').find('option').not(':first').remove();
            jQuery('#city').val('');
    		jQuery('#locality').val('');
    		jQuery('#apartment').val('');
    		jQuery('#city_list').html('');
    		jQuery('#locality_list').html('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',country : country_id},
	            url: '<?php echo base_url('get_state'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#state').attr("disabled", true);
	                jQuery('#city').attr("disabled", true);
	                jQuery('#locality').attr("disabled", true);
	                jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var state_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < state_list.length; i++) {
                        var obj = state_list[i];
                        jQuery('#state').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(country_value=='' || country_value==' - Country - ' || country_value=='0'){
                    	jQuery('#state').attr("disabled", true);
	                }
	                else{
                    	jQuery('#state').attr("disabled", false);
	                }
	            }
	        });
	    });
	    
	    jQuery('#state').on('change',function(){
	    	state_value = jQuery(this).val();
            var state_id = jQuery('option:selected', this).attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            jQuery('#city').find('option').not(':first').remove();
            jQuery('#city').val('');
    		jQuery('#locality').val('');
    		jQuery('#apartment').val('');
    		jQuery('#city_list').html('');
    		jQuery('#locality_list').html('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',state : state_id},
	            url: '<?php echo base_url('get_city'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#city').attr("disabled", true);
	                jQuery('#locality').attr("disabled", true);
                	jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var city_list = jQuery.parseJSON(response);
	            	jQuery('#city').append(jQuery("<option>sdfdsf</option>"));
	            	for(var i = 0; i < city_list.length; i++) {
                        var obj = city_list[i];
                        jQuery('#city_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(state_value=='' || state_value==' - State - ' || state_value=='0'){
                    	jQuery('#city').attr("disabled", true);
	                }
	                else{
                    	jQuery('#city').attr("disabled", false);
	                }
	            }
	        });
	    }); 
	    
	    jQuery('#city').on('change',function(){
            var city_value = jQuery(this).val();
    		var city_id = jQuery('#city_list').find("[value='" + city_value + "']").attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            jQuery('#locality').find('option').not(':first').remove();
            jQuery('#locality').val('');
    		jQuery('#apartment').val('');
    		jQuery('#locality_list').html('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',city : city_id},
	            url: '<?php echo base_url('get_locality'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#locality').attr("disabled", true);
                	jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var locality_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < locality_list.length; i++) {
                        var obj = locality_list[i];
                        jQuery('#locality_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    if(city_value==''){
	                	jQuery('#locality').attr("disabled", true);
	                }
	                else{
                    	jQuery('#locality').attr("disabled", false);
	                }
	            }
	        });
	    }); 
	   
        jQuery('#locality').on('change',function(){
            var locality_value = jQuery(this).val();
    		var locality_id = jQuery('#locality_list').find("[value='" + locality_value + "']").attr('data-val');
            var token = jQuery('input[name="global_cookiee"]').val();
            jQuery('#apartment').find('option').not(':first').remove();
    		jQuery('#apartment').val('');
    		jQuery('#apartment_list').html('');
            jQuery.ajax({
	        	type: 'POST',
	        	data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',locality : locality_id},
	            url: '<?php echo base_url('get_apartment'); ?>',
	            cache: false,
	            beforeSend : function(){
	                jQuery('#apartment').attr("disabled", true);
	            },
	            success: function(response){
	            	var apartment_list = jQuery.parseJSON(response);
	            	for(var i = 0; i < apartment_list.length; i++) {
                        var obj = apartment_list[i];
                        jQuery('#apartment_list').append(jQuery("<option></option>").attr("value",obj.loc_name).attr("data-val",obj.loc_id).text(obj.loc_name));
                    }
                    jQuery('#apartment').attr("disabled", false);
                    if(locality_value==''){
	                	jQuery('#apartment').attr("disabled", true);
	                }
	            }
	        });
	    });
	    jQuery("body").on('click', '#update_particular_location',function(event){
	  	    var country    = jQuery('#country option:selected').val();
	  	    var country_id = jQuery('#country option:selected').attr('data-val');
			var state      = jQuery('#state option:selected').val();
			var state_id   = jQuery('#state option:selected').attr('data-val');
			var city         = jQuery('#city').val();
			var city_id      = jQuery('#city_list').find("[value='"+city+"']").attr('data-val');
			var locality     = jQuery('#locality').val();
			var locality_id  = jQuery('#locality_list').find("[value='"+locality+"']").attr('data-val');
			var apartment    = jQuery('#apartment').val();
			var apartment_id = jQuery('#apartment_list').find("[value='"+apartment+"']").attr('data-val');
			var particular_date = jQuery('#particular_date').val();
	  	    var error = false;
	  	    if(country == '' || country == undefined){
	  	    	jQuery('.text-danger-country').addClass('text-danger');
	  	    	jQuery('.text-danger-country').text('Please select country');
	  	    	error = true;
	  	    }
	  	    if(state == '' || state ==undefined){
	  	    	jQuery('.text-danger-state').addClass('text-danger');
	  	    	jQuery('.text-danger-state').text('Please select state');
	  	    	error = true;
	  	    }
	  	    if(city == '' || city ==undefined){
	  	    	jQuery('.text-danger-city').addClass('text-danger');
	  	    	jQuery('.text-danger-city').text('Please select city');
	  	    	error = true;
	  	    }
	  	    if(locality == '' || locality ==undefined){
	  	    	jQuery('.text-danger-locality').addClass('text-danger');
	  	    	jQuery('.text-danger-locality').text('Please select locality');
	  	    	error = true;
	  	    }alert(city);
	  	    // if(apartment == '' || apartment ==undefined){
	  	    // 	jQuery('.text-danger-apartment').addClass('text-danger');
	  	    // 	jQuery('.text-danger-apartment').text('Please select apartment');
	  	    // 	error = true;
	  	    // }
	  	    if(particular_date == '' || particular_date ==undefined){
	  	    	jQuery('.text-danger-particular-date').addClass('text-danger');
	  	    	jQuery('.text-danger-particular-date').text('Please select date');
	  	    	error = true;
	  	    }
	  	    if(error){
	  	    	return false;
	  	    }
	  	    jQuery.ajax({
	        	type: 'POST',
	        	data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',country_id : country_id,state_id : state_id,city_id : city_id,locality_id : locality_id,apartment_id : apartment_id,particular_date:particular_date},
	            url: '<?php echo base_url('particular_location_config'); ?>',
	            cache: false,
	            success: function(response){
	            	if(response==5){
	            		var txt = 'Apartment is disabled on '+particular_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	            	}
                    else if(response==6){
	            		var txt = 'Locality is disabled on '+particular_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==7){
	                	var txt = 'Locality is disabled on '+particular_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==8){
	                	var txt = 'Locality is disabled on '+particular_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	                else if(response==9){
	                	var txt = 'Locality is disabled on '+particular_date;
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');s
	                }
	                else{
	                	var txt = 'Error in updating form';
	            		jQuery('#holiday_modal .alert').html(txt);
	            		jQuery('#holiday_modal').css('display','block');
	                }
	            }
	        });
	  	});

		jQuery('#apartment').focusin(function() {
		  	jQuery(this).val('');
		});
		jQuery('#locality').focusin(function() {
		  	jQuery(this).val('');
		});
		jQuery('#city').focusin(function() {
		  	jQuery(this).val('');
		});

	  	jQuery('#country').on('focus', function(){
			jQuery('.text-danger-country').html('');
		});
		jQuery('#state').on('focus', function(){
			jQuery('.text-danger-state').html('');
		});
		jQuery('.first-half #city').on('focus', function(){
			jQuery('.text-danger-city').html('');
		});
		jQuery('#locality').on('focus', function(){
			jQuery('.text-danger-locality').html('');
		});
		jQuery('#apartment').on('focus', function(){
			jQuery('.text-danger-apartment').html('');
		});
		jQuery('#particular_date').on('focus', function(){
			jQuery('.text-danger-particular-date').html('');
		});
		jQuery('#to_date').on('focus', function(){
  	    	jQuery('.to_date_validation').text('');
  	    	error = true;
  	    });
    });	
</script>
<?php $this->load->view('admin/templates/footer') ?>