<?php $this->load->view('admin/templates/header', array(
	'title' => 'Update Item Details',
	'link' => 'items',
	'breadcrumbs' => array(
		0 => array('name'=>'Items','link'=>'items'),
		1 => array('name'=>$product->name,'link'=>FALSE),
	)
)); ?>
<?php echo form_open_multipart(current_url()) ?>
	<?php echo form_hidden('id', $product->id) ?>
	<?php $this->load->view('admin/items/item_update_header', array(
		'id' => $product->id,
		'active' => 'details',
	)); ?>
	<?php if (validation_errors()): ?>
			<div class="alert alert-danger">Check the form for errors and try again.</div>
	<?php else: ?>
		<?php if (! empty($message)): ?>
			<div id="message"> <?=$message ?> </div>
		<?php endif ?>
	<?php endif ?>
	<div class="row">
    	<div class="col-sm-12 col-md-3 form-group <?php echo form_error('userfile') ? 'has-error' : '' ?>">
			<label class="control-label">Thumbnail</label>
			<?php echo form_hidden('crop_x', '') ?>
			<?php echo form_hidden('crop_y', '') ?>
			<?php echo form_hidden('crop_width', '') ?>
			<?php echo form_hidden('crop_height', '') ?>
			<div class="panel-image panel <?php echo form_error('userfile') ? 'panel-danger' : 'panel-default' ?>" style="margin:0">
				<div class="fileinput fileinput-item-thumb fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail text-muted">
						<img src="<?php echo $product->thumb ?>">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail">
					</div>
					<div class="btn-group btn-block">
						<div class="btn btn-sm btn-info btn-file">
							<span class="fileinput-new">Select</span>
							<span class="fileinput-exists">Change</span>
							<input type="file" name="userfile">
						</div>
						<a href="#" class="btn btn-sm btn-danger remove-image" data-dismiss="fileinput">Remove</a>
					</div>
				</div>
			</div>
			<p class="help-block"><?php echo form_error('userfile') ? form_error('userfile') : '&nbsp' ?></p>
	    </div>
        <div class="col-sm-12 col-md-9">
		    <div class="row">
		        <div class="col-sm-12 col-md-6">
		            <div class="form-group catlog-product-update-name <?php echo form_error('name') ? 'has-error' : '' ?>">
		                <label class="control-label" for="name">Name<span class="red">*</span></label>
		                <input type="text" class="form-control" name="name" value="<?php echo set_value('name') ? set_value('name') : $product->name ?>" />
		                <p class="help-block"><?php echo form_error('name') ? form_error('name') : '&nbsp' ?></p>
		            </div>
		        </div>
		        <?php /*<div class="col-sm-12 col-md-6">
		            <div class="form-group <?php echo form_error('tags') ? 'has-error' : '' ?>">
		                <label class="control-label" for="tags">Tags</label>
		                <input type="text" class="form-control" name="tags" value="<?php echo set_value('tags') ? set_value('tags') : $product->tags ?>" />
		                <p class="help-block"><?php echo form_error('tags') ? form_error('tags') : 'Separate each tag by a comma.' ?></p>
		            </div>
		        </div> */ ?>
				<div class="col-sm-12 col-md-6">
					<div class="form-group catlog-product-update-category <?php echo form_error('category') ? 'has-error' : '' ?>">
						<label class="control-label" for="category">Category<span class="red">*</span></label>
							<?php
							$options = array();
							foreach ($categories as $category)
							{
								$options[$category['id']] = $category['name'];
							}
							 ?>
						<?php echo form_dropdown('category', $options, $product->category_id, 'class="form-control"'); ?>
		                <p class="help-block"><?php echo form_error('category') ? form_error('category') : '&nbsp' ?></p>
					</div>
				</div>
                <div class="col-sm-6 col-md-6">
                   	<div class="form-group catlog-product-update-price  <?php echo form_error('price') ? 'has-error' : '' ?>">
                        <label class="control-label" for="price">Price<span class="red">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?php echo $this->flexi_cart_admin->currency_symbol(TRUE); ?></span>
                            <input type="text" class="form-control" id="inputPrice" placeholder="Item price" name="price" value="<?php echo set_value('price') ? set_value('price') : $product->price ?>"/>
                        </div>
		                <p class="help-block"><?php echo form_error('price') ? form_error('price') : '&nbsp' ?></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="form-group catlog-product-update-stock <?php echo form_error('stock') ? 'has-error' : '' ?>">
                        <label class="control-label" for="stock">Stock Quantity<span class="red">*</span></label>
                        <input type="number" class="form-control" name="stock" value="<?php echo set_value('stock') ? set_value('stock') : $product->quantity ?>" />
		                <p class="help-block"><?php echo form_error('Stock') ? form_error('Stock') : '&nbsp' ?></p>
                    </div>
                </div>
                <?php /* <div class="col-sm-6 col-md-6">
                    <div class="form-group <?php echo form_error('weight') ? 'has-error' : '' ?>">
                        <label class="control-label" for="weight">Weight (<?php echo $this->flexi_cart_admin->weight_symbol(); ?>)</label>
                        <input type="text" class="form-control" name="weight"  value="<?php echo set_value('weight') ? set_value('weight') : $product->weight ?>" />
		                <p class="help-block"><?php echo form_error('weight') ? form_error('weight') : '&nbsp' ?></p>
                    </div>
                </div> */ ?>
                <div class="col-sm-6 col-md-4">
                    <div class="form-group">
                        <input type="checkbox" name="featured" id="featured" value="<?php echo set_value('featured') ? set_value('featured') : $product->featured ?>" <?php echo $product->featured ? "checked" : "" ?> />
                        <label class="control-label" class="trigger-featured" for="featured">Featured Product</label>
                    </div>
                </div>
                <div class="col-md-12">
	                <div class="form-group">
						<label class="control-label" for="description">Description</label>
						<textarea rows="6" class="form-control" name="description"  id="editor"><?php echo set_value('description') ? set_value('description') : $product->description ?></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<?php echo form_submit('update_item', 'Update Item Details', 'class="btn btn-lg btn-success update_item"') ?>
				</div>
		    </div>
	    </div>
	</div>
	
<?php echo form_close() ?>
<script src="https://cdn.ckeditor.com/4.6.1/standard-all/ckeditor.js"></script>
<script type="text/javascript" src="<?= base_url('assets/js/cropper.min.js') ?>"></script>
<script>
	$(document).ready(function() {
	    CKEDITOR.replace('editor');
		$('.fileinput-item-thumb').on('change.bs.fileinput', function (e) {
			$('.fileinput-preview img').cropper({
				//aspectRatio: 1 / 1,
				crop: function(e) {
					$('input[name="crop_width"]').val(e.width);
					$('input[name="crop_height"]').val(e.height);
					$('input[name="crop_x"]').val(e.x);
					$('input[name="crop_y"]').val(e.y);
				}
			});
		})
        $('.fileinput-item-image').on('change.bs.fileinput', function (e) {
            var key = $(this).attr('data-key');
            $(this).find('.fileinput-preview img').cropper({
                aspectRatio: 1 / 1,
                crop: function(e) {
                    $('input[name="crop_width['+key+']"]').val(e.width);
                    $('input[name="crop_height['+key+']"]').val(e.height);
                    $('input[name="crop_x['+key+']"]').val(e.x);
                    $('input[name="crop_y['+key+']"]').val(e.y);
                }
            });
        });
        jQuery('.trigger-featured').on('click',function(){
		     jQuery('#featured').trigger('click');
		});
		jQuery('#featured').on('change', function() { 
		      if(jQuery(this).prop('checked') == true){
                  jQuery(this).val("1");
              }else{
                  jQuery(this).val("0");
              }
		});
		jQuery('.update_item').on('click', function(event) {
		    var name     = jQuery('.catlog-product-update-name input').val();
		    var category = jQuery('.catlog-product-update-category input').val();
		    var price    = jQuery('.catlog-product-update-price input').val();
		    var quantity = jQuery('.catlog-product-update-stock input').val();
		    var image    = jQuery('input[name="crop_width"]').val();
		    if (name == '') {
		    	jQuery('.catlog-product-update-name').addClass('has-error');
		    }
		    else if (category == '' || category == 'option') {
		    	jQuery('.catlog-product-update-category').addClass('has-error');
		    }
		    else if (price == '') {
		    	jQuery('.catlog-product-update-price').addClass('has-error');
		    }
		    else if (quantity == '') {
		    	jQuery('.catlog-product-update-stock').addClass('has-error');
		    }
		    else if (image == '') {
		    	jQuery('.panel-image').addClass('panel-danger');
		    }
		    else if (name == '' || category == '' || quantity == '' || quantity == '' || image == '') {
		        jQuery('.update_item').val('Update Item Details');
                jQuery('button[type="submit"]').html('Update Item Details');
		        return false;
		    }
		    else {
		        return true;
		    }
		});
		jQuery('.remove-image').click(function(){
			jQuery('.fileinput-new img').attr('src','');
		}); 
		jQuery('.catlog-product-update-name input').on('focus', function(event) { 
			jQuery('.catlog-product-update-name').removeClass('has-error');
		});
		jQuery('.catlog-product-update-category input').on('focus', function(event) { 
			jQuery('.catlog-product-update-category').removeClass('has-error');
		});
		jQuery('.catlog-product-update-price input').on('focus', function(event) { 
			jQuery('.catlog-product-update-price').removeClass('has-error');
		});
		jQuery('.catlog-product-update-stock input').on('focus', function(event) { 
			jQuery('.catlog-product-update-stock').removeClass('has-error');
		});
		jQuery('.panel-image input').on('click', function(event) { 
			jQuery('.panel-image').removeClass('panel-danger');
		});
	});
</script>
<?php $alert = $this->session->flashdata('alert') ?>
<?php if (isset($alert['location'])): ?>
	<script>
		$(document).ready(function() {
			$('#app-tabs a[href="#app-tab-<?php echo $alert['location'] ?>"]').tab("show")
		})
	</script>
<?php endif ?>
<?php $this->load->view('admin/templates/footer') ?>