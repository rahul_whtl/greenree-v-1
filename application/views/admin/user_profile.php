<?php $this->load->view('admin/templates/header', array(
	'title' => 'Admin Profile',
	'link' => 'admin',
	'sub_link' => 'profile',
	'breadcrumbs' => array(
		1 => array('name'=>'Profile','link'=>FALSE)
	)
)); ?>

<div class="lead page-header">
    <span class="glyphicon glyphicon-user" style="margin-right:10px"></span>
    Edit profile
</div>
<?php if (validation_errors()): ?>
	<div class="alert alert-danger animated fadeInDown" id="message">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<?php echo validation_errors(); ?>
	</div>
<?php endif ?>

<?php echo form_open_multipart(uri_string()); ?>
	<div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="form-group <?= form_error('userfile') ? 'has-error' : '' ?>">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <label class="panel-title control-label" for="userfile">
                        	LOGO
                        </label>
                    </div>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail text-warning">
                            <img src="<?= base_url($user->logo) ?>">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail">
                        </div>
                        <div class="btn-group btn-block">
                            <div class="btn btn-success btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="userfile">
                            </div>
                            <a href="#" class="btn btn-danger remove-image" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 admin-profile-form">
            <div class="row profile-data">
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="name">Username</label>
	                    <input class="form-control" type="text" name="username" class="username" id ='user_name' value="<?= set_value('username') ? set_value('username') : $user->username ?>">
	                    <span class="text-danger text-danger-name  <?= form_error('username') ? 'has-error' : '' ?>"><?=form_error('username') ? form_error('username') : '' ?>
	                    </span>
	                </div>
                </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="name">Password</label>
	                    <input class="form-control" type="text" name="password" value="<?= set_value('password') ? set_value('password') : '' ?>">
	                    <span class="text-danger text-danger-password  <?= form_error('password') ? 'has-error' : '' ?>"> <?= form_error('password') ? form_error('password') : '' ?>
	                    </span>
	                </div>
                </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="name">Repeat password </label>
	                    <input class="form-control" type="text" name="password_confirm" value="<?= set_value('password_confirm') ? set_value('password_confirm') : '' ?>">
	                    <span class="text-danger text-danger-password-confirm  <?= form_error('password_confirm') ? 'has-error' : '' ?>"> <?= form_error('password_confirm') ? form_error('password_confirm') : '' ?>
	                    </span>
	                </div>
                </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="name">Email address </label>
	                    <input class="form-control" type="text" id="email" class='email' name="email" value="<?= set_value('email') ? set_value('email') : $user->email ?>">
	                    <span class="text-danger text-danger-email  <?= form_error('email') ? 'has-error' : '' ?>"> 
	                        <?= form_error('email') ? form_error('email') : '' ?>
	                    </span>
	                </div>
	            </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="control-label" for="company_name">Company name </label>
                        <input class="form-control" type="text" name="company_name" class="company_name" id="company_name" value="<?= set_value('company_name') ? set_value('company_name') : $user->name ?>">
                        <span class="text-danger text-danger-company-name  <?= form_error('company_name') ? 'has-error' : '' ?>"> 
	                        <?= form_error('company_name') ? 'Company Name is required.' : '' ?>
	                    </span>
                    </div>
                </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="company_address">Physical address</label>
	                    <input class="form-control" type="text" name="company_address" value="<?= set_value('company_address') ? set_value('company_address') : $user->address ?>">
	                    <span class="text-danger text-danger-address  <?= form_error('company_address') ? 'has-error' : '' ?>"> 
	                         <?= form_error('company_address') ? '(company_address)' : '' ?>
	                    </span>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="company_p_o_box">Postal address</label>
	                    <input class="form-control" type="text" name="company_p_o_box" value="<?= set_value('company_p_o_box') ? set_value('company_p_o_box') : $user->postal ?>">
	                    <span class="text-danger text-danger-postal-address  <?= form_error('company_p_o_box') ? 'has-error' : '' ?>"> 
	                          <?= form_error('company_p_o_box') ? '(company_p_o_box)' : '' ?>
	                    </span>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="form-group">
	                    <label class="control-label" for="company_phone">Company phone</label>
	                    <input class="form-control" type="text" class = "company_phone" id="company_phone" name="company_phone" value="<?= set_value('company_phone') ? set_value('company_phone') : $user->phone ?>">
	                    <span class="text-danger text-danger-phone  <?= form_error('company_phone') ? 'has-error' : '' ?>"> 
	                           <?= form_error('company_phone') ? 'Company Phone is required.' : '' ?>
	                    </span>
	                </div>
	            </div>
            </div>
        </div>
    </div>

    <hr>

	<input type="button" class="btn update_admin_profile btn-lg btn-primary" value="UPDATE PROFILE" />


    <div class="modal_admin_profile" id="modal_admin_profile">
		<div class="modal-content">
    		<span class="close">&times;</span>
    		<h4 class="modal-title" id="myModalLabel">Enter your current password</h4>
			<div class="modal-body">
                <input class="form-control" type="password" name="old_password" value="">
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-lg btn-block btn-primary" name="edit_user" value="Continue" />
			</div>
		</div>
    </div>

<?php echo form_close() ?>
<script type="text/javascript">
    
    function validateEmail(email) {
       var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       return re.test(String(email).toLowerCase());
    }
	var modal_admin_profile = document.getElementById('modal_admin_profile');
	jQuery(document).ready(function(){
// 	    jQuery('.update_admin_profile').click(function(e){
// 			e.preventDefault();
// 			modal_admin_profile.style.display = "block";
// 		});
		jQuery('.close').click(function(){
			modal_admin_profile.style.display = "none";
		});
		window.onclick = function(event) {
	  		if (event.target == modal_admin_profile) {
	    		modal_admin_profile.style.display = "none";
	  		}
		}
		jQuery('.remove-image').click(function(){
			jQuery('.fileinput-new img').attr('src','');
		}); 
		jQuery(".update_admin_profile").click(function(event){
		    var name = jQuery('.admin-profile-form #user_name').val();
		    var email            = jQuery.trim(jQuery('.admin-profile-form #email').val());
		    var company_name     = jQuery('.admin-profile-form #company_name').val();
		    var company_phone    = jQuery('.admin-profile-form #company_phone').val();       
		    jQuery('.text-danger').text('');
		    error = false;
		    
		    if(name == '') {
				jQuery('.text-danger-name').text('Please Enter Your Name');
				error = true;
			}
			
			if(!isNaN(name)){
			    jQuery('.text-danger-name').text('Please Enter Only Characters');
				error = true;
			}
			if(name.length < 3){
			    jQuery('.text-danger-name').text('Please Enter Atleast Three Characters');
				error = true;
			}
			if(email == ''){
				jQuery('.text-danger-email').text('Please Enter Your Email');
				error = true;
			}
			if(!validateEmail(email)){
			 	jQuery('.text-danger-email').text('Please Enter Valid Email');
				error = true;
			}
		    if(company_name == ''){
			    jQuery('.text-danger-company-name').text('Please Enter Company Name');
			    error = true;
			}
			if(company_phone == ''){
			    jQuery('.text-danger-phone').text('Please Enter Phone No');
				error = true;
			}	
			if(error){
			    return false;
			}else{
			    modal_admin_profile.style.display = "block";
	            return true;
			}
		});
		jQuery(".admin-profile-form input").focus(function(){
		    var value =  jQuery(this).val();
		    if(value !== '0'){
		        jQuery(this).parent().removeClass('error');
		        jQuery(this).next().text('');
		    }else{
		        jQuery(this).parent().addClass('error');
		    }
		});
    });
</script>
<?php $this->load->view('admin/templates/footer') ?>