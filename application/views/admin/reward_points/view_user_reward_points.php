<?php $this->load->view('admin/templates/header', array(
	'link' => 'user',
	'breadcrumbs' => array(
		0 => array('name'=>'user','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">User Wallet Cash History</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>
<div style='height:60px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

<script type="text/javascript">
	jQuery(document).ready(function() {
	   var url      =  window.location.href;//alert(url);
	   url = url.split("/");//alert(url[6]);
	   if (url[5] == 'read' || url[5] == 'edit' || url[5] == 'add') {
	   	jQuery('#send_notification').hide();	   }
	});
</script>
    <script type="text/javascript">
$(function () {    
	jQuery('.send_notification').click(function(){
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select Vendors");
			return;
		}
		jQuery("#modify_email_text").css('display','block');
	});	
    
});
jQuery(document).ready(function(){
    $('.select-all').click(function () {
		
		if (!$(this).hasClass('checkall')) {
			var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
            $('input[type="checkbox"]', allPages).prop('checked', false);
        } else {
        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
        }
        $(this).toggleClass('checkall');
			
	});
 });
jQuery('document').ready(function(){

	var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
	$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
		
});
</script>
<style type="text/css">
	.open-button {background-color: #555;color: white;padding: 16px 20px;border: none;cursor: pointer;opacity: 0.8;position: fixed;bottom: 23px;right: 28px;width: 280px;}
	.form-popup2 {display: none;position: fixed;bottom: 40%; left:600px ;border: 3px solid #f1f1f1;z-index: 9;}
	.form-container2 {max-width: 300px;padding: 10px;background-color: white;}

	/* Full-width input fields */
	.form-container2 input[type=text], .form-container2 input[type=password] {width: 100%;padding: 15px;margin: 5px 0 22px 0;border: none;background: #f1f1f1;}
</style>
<?php $this->load->view('admin/templates/footer'); ?>