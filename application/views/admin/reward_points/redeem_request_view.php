<?php $this->load->view('admin/templates/header', array(
	'link' => 'Redeem',
	'breadcrumbs' => array(
		0 => array('name'=>'Redeem Request','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">Redeem Request List</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>
<div class="modify_status">
	<input type="button" name="modify_status_btn" class="btn btn-lg btn-primary modify_status_btn" id="modify_status_btn" value="Mark As Completed">
</div>	
<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
	   var url      =  window.location.href;//alert(url);
	   url = url.split("/");//alert(url[6]);
	   if (url[5] == 'read' || url[5] == 'edit' || url[5] == 'add') {
	   	jQuery('.modify_status').hide();	   }
	});
</script>
    <script type="text/javascript">
    jQuery(function() {
    	var allPages1 = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray1 = [];
		jQuery('input[type="checkbox"]', allPages1).each(function(){
			if(jQuery(this).parent().next().next().next().next().next().next().next().next().next().next().text()=='Completed'){
			    jQuery(this).parent().prev().text('');
				jQuery(this).parent().text('');
			}
		});	
	});
    jQuery(document).ready(function(){
        jQuery(".modify_status_btn").click(function(event){
        	var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
			var myarray = [];
			$('input[type="checkbox"]:checked', allPages).each(function(){
				myarray.push(jQuery(this).parent().next().text());
			});	
			if(jQuery.isEmptyObject(myarray)){
				alert("Please select Customers");
				return;
			}
            jQuery(".modify_status_btn").attr('disabled',true);
            jQuery.ajax({
		        'type' : 'POST',
		        'url' : "<?php echo base_url('modify_redeem_request'); ?>",
		        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray },
		        'success' : function(data) { 
					alert('Request Success');
					jQuery(".modify_status_btn").attr('disabled',false);
		        },
		        'error' : function(request,error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });          
		}); 
        });	
    jQuery(document).ready(function(){
        $('.select-all').click(function () {
			
			if (!$(this).hasClass('checkall')) {
				var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
	            $('input[type="checkbox"]', allPages).prop('checked', false);
	        } else {
	        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
	        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
	        }
	        $(this).toggleClass('checkall');
				
		});
	 });	
        jQuery('document').ready(function(){
		
			var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
			$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
				
		});
    </script>
<?php $this->load->view('admin/templates/footer'); ?>