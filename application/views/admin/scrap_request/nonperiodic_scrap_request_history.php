<?php $this->load->view('admin/templates/header', array(
	'link' => 'Scrap',
	'breadcrumbs' => array(
		0 => array('name'=>'Scrap','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">Non-Periodic Scrap Request History</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>
<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

   
    <script type="text/javascript">
// $(function () {    	
//     	jQuery('.blocked_customer_btn').click(function () {
//        // e.preventDefault();
// 		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
// 		var myarray = [];
// 		$('input[type="checkbox"]:checked', allPages).each(function(){
// 			myarray.push(jQuery(this).parent().next().text());
// 		});	
// 		if(jQuery.isEmptyObject(myarray)){
// 			alert("Please select Customers");
// 			return;
// 		}
// 		jQuery.ajax({
// 			type : 'POST',
// 	        url : "<?php //echo base_url('admin/blocked_customer'); ?>",
// 	        data : { '<?php// echo $this->security->get_csrf_token_name(); ?>':'<?php //echo $this->security->get_csrf_hash(); ?>',myarray:myarray},
// 	        'success' : function(data) {             
// 	            alert("success");
// 	        },
// 	        'error' : function(request,error)
// 	        {
// 	            alert("Request: "+JSON.stringify(request));
// 	        }
// 	    });
// 	});
	
// });
// jQuery(document).ready(function(){
//     $('.select-all').click(function () {
		
// 		if (!$(this).hasClass('checkall')) {
// 			var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
//             $('input[type="checkbox"]', allPages).prop('checked', false);
//         } else {
//         	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
//         	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
//         }
//         $(this).toggleClass('checkall');
			
// 	});
//  });
// jQuery('document').ready(function(){

// 	var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
// 	$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
		
// });
    </script>
<?php $this->load->view('admin/templates/footer'); ?>