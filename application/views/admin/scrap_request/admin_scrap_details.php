<?php $this->load->view('admin/templates/header', array(
	'link' => 'Scrap',
	'breadcrumbs' => array(
		0 => array('name'=>'Customer','link'=>FALSE)
	)
)); ?>
<?php //print_r($scrap_info); ?>
<div class="admin-scrap-details" style="margin-top: 50px;">
	<div class="row" style="font-size: 20px;">
	    <?php if(!empty($user_name)): ?>
		<div class="col-sm-6 text-left">
			<strong><p>Name : <?php echo $user_name;?></p></strong>
		</div>
		<?php endif ?>
		<div class="col-sm-6 text-right" style="float:right">
			<strong><p>Scrap Request Id: <?php echo $scrap_id;?> (<?php echo $scrap_info['0']->request_type;?>)</p></strong>
		</div>	
	</div>
    <div style="overflow-x:auto;">
        <table class="table" style="margin-top: 50px;">
            <thead>
              <tr style="font-size: 18px;">
                <th>Scrap ID</th>
                <th>Customer Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Flat</th>
                <th>Apartment</th>
                <th>Comment</th>
                <th>Locality</th>
                <th>Preferred Pickup Date</th>
                <th>Preferred Pickup Day</th>
                <th>Updated date</th>
                <th>Request Type</th>
                <th>Status</th>
                <th>OTP</th>
                <th>Customer Presence</th>
                <th>Customer Response</th>
                <th>List Created date</th>
                <th>Vendor Assigned</th>
                <th>Vendor Response</th>
                <th>Comments by Vendor</th>
                <th>Requested Date</th>
                <th>Last Pickup Date</th>
                <th>Next Pickup Date</th>
                <th>Reward Point</th>
                <th>List Created Group</th>
                <th>Scrap Weight</th>
                <th>Scrap Details</th>
                <th>Picked Up Date</th>
              </tr>
            </thead>
            <tbody>
           <?php if(empty($scrap_info_archive) && empty($scrap_info) ):?> 
                <tr>
                    <td colspan="9">
                        <?php echo 'There is no History for this Scrap Request. This is a new Scrap Request.'; ?>
                    </td>     
                </tr>
           <?php else: ?>
                <?php foreach($scrap_info as $key => $value): ?>    
                  <tr>
                    <td><?php echo $value->id; ?></td>
                    <td><?php echo $value->first_name.' '.$value->last_name; ?></td>
                    <td><?php echo $value->phone; ?></td>
                    <td><?php echo $value->email; ?></td>
                    <td><?php echo $value->flat_no; ?></td>
                    <td><?php echo $value->apartment_id; ?></td>
                    <td><?php echo $value->scrap_comment; ?></td>
                    <td><?php echo $value->locality; ?></td>
                    <td><?php echo $value->prefered_pickup_date_next; ?></td>
                    <td><?php echo $value->prefered_pickup_day; ?></td>
                    <td><?php echo $value->updated_date; ?></td>
                    <td><?php echo $value->request_type; ?></td>
                    <td><?php echo $value->request_status; ?></td>
                    <td><?php echo $value->apartment_otp; ?></td>
                    <td><?php echo $value->customer_presence; ?></td>
                    <td><?php echo $value->customer_response; ?></td>
                    <td>
                        <?php if((date('Y-m-d', strtotime($value->list_created_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->list_created_date))== '1970-01-01')):?>
                            <?php echo '--';?>
                        <?php else:?>
                            <?php echo $value->list_created_date; ?>
                        <?php endif ?>
                    </td>
                    <td><?php echo $value->vendor_name; ?></td>
                    <td><?php echo $value->vendor_response; ?></td>
                    <td><?php echo $value->not_picked_reason; ?></td>
                    <td><?php echo $value->requested_date; ?></td>
                    <td><?php echo $value->last_pickup_date; ?></td>
                    <td><?php echo $value->next_pickup_date; ?></td>
                    <td><?php echo $value->reward_point; ?></td>
                    <td><?php echo $value->list_created_group; ?></td>
                    <td><?php echo $value->scrap_weight; ?></td>
                    <td><?php echo $value->scrap_details; ?></td>
                    <td><?php echo $value->picked_up_date; ?></td>
                  </tr>
                <?php endforeach ?>
                <?php foreach($scrap_info_archive as $value_archive): ?>    
                  <tr>
                    <td><?php echo $value->id; ?></td>
                    <td><?php echo $value->first_name.' '.$value->last_name; ?></td>
                    <td><?php echo $value->phone; ?></td>
                    <td><?php echo $value->email; ?></td>
                    <td><?php echo $value->flat_no; ?></td>
                    <td><?php echo $value->apartment_id; ?></td>
                    <td><?php echo $value->scrap_comment; ?></td>
                    <td><?php echo $value->locality; ?></td>
                    <td><?php echo $value->prefered_pickup_date_next; ?></td>
                    <td><?php echo $value->prefered_pickup_day; ?></td>
                    <td><?php echo $value->updated_date; ?></td>
                    <td><?php echo $value->request_type; ?></td>
                    <td><?php echo $value->request_status; ?></td>
                    <td><?php echo $value->apartment_otp; ?></td>
                    <td><?php echo $value->customer_presence; ?></td>
                    <td><?php echo $value->customer_response; ?></td>
                    <td>
                        <?php if((date('Y-m-d', strtotime($value_archive->list_created_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value_archive->list_created_date))== '1970-01-01') || (date('Y-m-d', strtotime($value_archive->list_created_date))== '0000-00-00')):?>
                            <?php echo '--';?>
                        <?php else:?>
                            <?php echo $value_archive->list_created_date; ?>
                        <?php endif ?>     
                    </td>
                    <td><?php echo $value->vendor_name; ?></td>
                    <td><?php echo $value->vendor_response; ?></td>
                    <td><?php echo $value->not_picked_reason; ?></td>
                    <td><?php echo $value->requested_date; ?></td>
                    <td><?php echo $value->last_pickup_date; ?></td>
                    <td><?php echo $value->next_pickup_date; ?></td>
                    <td><?php echo $value->reward_point; ?></td>
                    <td><?php echo $value->list_created_group; ?></td>
                    <td><?php echo $value->scrap_weight; ?></td>
                    <td><?php echo $value->scrap_details; ?></td>
                    <td><?php echo $value->picked_up_date; ?></td>
                  </tr>
                <?php endforeach ?>
            <?php endif ?>    
            </tbody>
        </table>
    </div>
</div>


<?php $this->load->view('admin/templates/footer'); ?>