<?php $this->load->view('public/templates/header', array(
	'title' => 'GreenREE - Wishlist')) ?>
<?php $this->load->view('public/dashboard/dashboard_header', array('active' => 'wishlist')) ?>
<div class="wishlist-page">
	<div class="container">
		<?php if(!empty($this->session->flashdata('error'))){ ?> 
	<div class="message">
		<div class="alert alert-danger">
			 <?php echo $this->session->flashdata('error');  ?>			 
		</div>
	</div>
	<?php } ?>
	
<div class="product-list">
	<div class="container">
		<div class="lead page-header">
			My Wishlist
		</div>
		<div style="margin: 0 0 10px; float: right">
			<a href="<?php echo base_url('user_dashboard/add-wishlist'); ?>" class="btn btn-success btn-lg">Add New Wishlist</a>
		</div>
		
		<?php if(!empty($products)){ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						Wishlist 
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="Periodic">
								<table class="table table-striped" style="margin:0;font-size:100%">
									<thead class="text-muted">
										<tr>
											<th>
												<div data-toggle="tooltip" >
													Name
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Quantity Needed
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Status
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Category
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Action
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($products as $product){ ?>
										<tr>
											<td class="text-center">
												<?php echo $product->item_name ?>
											</td>
											<td class="text-center">
												<?php echo $product->quantity ?>
											</td>
											<td class="text-center">
												<?php 
												$today = strtotime(date('Y-m-d H:i:s'));
												$due_date = strtotime($product->expiry_datetime);
												if($product->status == 1 && $today < $due_date){?>
													ACTIVE
												<?php }else if($product->status == 1 && $today > $due_date){?>
													<span class="blinking">EXPIRED</span>
												<?php }else{ ?>
													CLOSED
												<?php } ?>				
											</td>
											<td class="text-center">
												<?php echo $product->category ?>
											</td>
											<td class="text-center">
							<?php if($product->status == 1 && $today < $due_date){ ?>
							
									<a href="<?php echo base_url(); ?>user_dashboard/edit-wishlist/<?php echo $product->id ?>" class="btn btn-sm btn-success edit_btn">Edit</a>
									<a href="<?php echo base_url(); ?>user_dashboard/close-wishlist/<?php echo $product->id ?>" class="btn btn-warning btn-sm remove_btn">Close Now</a>
									
								<?php }else if($product->status == 1 && $today > $due_date){ ?>
								
									<a href="<?php echo base_url(); ?>repost_product/<?php echo $product->id ?>" class="btn btn-success btn-sm edit_btn">Repost</a>
									<a href="<?php echo base_url(); ?>edit_repost_product/<?php echo $product->id ?>" class="btn btn-success btn-sm edit_btn">Edit and Repost</a>
									<a href="<?php echo base_url(); ?>user_dashboard/close-wishlist/<?php echo $product->id ?>" class="btn btn-warning btn-sm remove_btn">Close Now</a>
									
								<?php }else{ ?>
									<?php echo '--';?>
								<?php } ?>
			
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }else{ ?>
			<p>Sorry! There are no product.</p>
			<br /><br /><br />
		<?php } ?>
</div>
</div>
		<div class="text-center">
			<button id="nav-to-top" class="btn btn-lg btn-default" style="position: fixed;bottom:10px;right: 10px;z-index: 100;width:auto;">
				<span class="glyphicon glyphicon-chevron-up"></span> up
			</button>
		</div>
	</div>
</div>
<?php $this->load->view('public/templates/footer') ?>