<?php $this->load->view('admin/templates/header', array(
	'link' => 'Customer',
	'breadcrumbs' => array(
		0 => array('name'=>'Customer','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">Wishlist</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>

<input type="button" name="delete_item" id="delete_item" value="Delete Selected" class="btn btn-lg btn-primary delete_item">

<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

    
    <div id="wishlist-admin-modal" class="form-popup2">
        <div class="modal-content">
            <span class="close">&times;</span>
            <div>
            	<br>
                <label for="email"><b>Wishlist is deleted Successfully.</b></label><br>
                <button class="btn cancel">Close</button>
            </div>
        </div>
    </div>  
   
   
    <script type="text/javascript">
    var modal = document.getElementById('wishlist-admin-modal');
		jQuery(document).ready(function() {
		   var url      =  window.location.href;//alert(url);
		   url = url.split("/");//alert(url[5]);
		   if (url[5] == 'edit' || url[5] == 'add' || url[5] == 'read') {
		   	  jQuery('.delete_item').hide();
		   }
		});
// $(function () {    	
//     	jQuery('.blocked_customer_btn').click(function () {
//       // e.preventDefault();
// 		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
// 		var myarray = [];
// 		$('input[type="checkbox"]:checked', allPages).each(function(){
// 			myarray.push(jQuery(this).parent().next().text());
// 		});	
// 		if(jQuery.isEmptyObject(myarray)){
// 			alert("Please select Customers");
// 			return;
// 		}
// 		jQuery.ajax({
// 			type : 'POST',
// 	        url : "<?php echo base_url('admin/blocked_customer'); ?>",
// 	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray},
// 	        'success' : function(data) {             
// 	            alert("success");
// 	        },
// 	        'error' : function(request,error)
// 	        {
// 	            alert("Request: "+JSON.stringify(request));
// 	        }
// 	    });
// 	});
	
// });

jQuery('.delete_item').click(function(){
	var allPages = datatables_get_chosen_table(jQuery('.select-all').closest('.groceryCrudTable')).fnGetNodes();
	var myarray = [];
	jQuery('input[type="checkbox"]:checked', allPages).each(function(){
		myarray.push(jQuery(this).parent().next().text());
	});	
	if(jQuery.isEmptyObject(myarray)){
		alert("Please select atleast one item to delete");
		return;
	}
	
	if(confirm('Are You Sure. You want to delete these wishlists')) {
	    
        jQuery.ajax({
    		type : 'POST',
            url : "<?php echo base_url('admin/delete_user_wishlist'); ?>",
            data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray},
            'success' : function(data) {             
                jQuery("#wishlist-admin-modal").css('display','block');
            },
            'error' : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
            }
        });
        
    }else{
        return;
    }
	
});

jQuery(document).ready(function(){
    jQuery('.close,.cancel').click(function(){
		modal.style.display  = "none";
        location.reload();
	});
    $('.select-all').click(function () {
		if (!$(this).hasClass('checkall')) {
			var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
            $('input[type="checkbox"]', allPages).prop('checked', false);
        } else {
        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
        }
        $(this).toggleClass('checkall');
	});
});
jQuery('document').ready(function(){
	var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
	$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
		
});
    </script>
<?php $this->load->view('admin/templates/footer'); ?>