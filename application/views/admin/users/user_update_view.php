<?php $this->load->view('admin/templates/header', array(
	'title' => 'Users',
	'link' => 'users',
	'breadcrumbs' => array(
		0 => array('name'=>'Users','link'=>'admin-view-users'),
		1 => array('name'=>$person ? $person->username : 'Not found','link'=>FALSE),
	)
)); ?>
<div class="lead page-header b-0">
	<img src="<?php echo base_url($person->avatar) ?>" alt="" style="width:35px" class="img-circle">
	<?php echo $person->first_name.' '.$person->last_name.' (Id: '.$person->id.')' ?>
</div>
<?php if (validation_errors()): ?>
	<div class="alert alert-danger">
		Correct the errors in the form and try again
	</div>
	<?php var_dump(validation_errors()) ?>
<?php else: ?>
	<?php if (! empty($message)): ?>
		<div id="message"> <?=$message ?> </div>
	<?php endif ?>
<?php endif ?>
<?php if (! $person): ?>
<div class="alert alert-warning">This user has been removed</div>
<?php else: ?>
<ul class="nav nav-tabs profile-nav" role="tablist" style="margin-bottom:3rem">
    <li role="presentation" class="active">
        <a href="#details" aria-controls="details" role="tab" data-toggle="tab">User Details</a>
    </li>
    <li role="presentation" class="">
        <a href="#points" aria-controls="points" role="tab" data-toggle="tab">Reward Points</a>
    </li>
    <li role="presentation" class="">
        <a href="#scrap_requets" aria-controls="scrap_requets" role="tab" data-toggle="tab">Scrap Request</a>
    </li>
    <li role="presentation" class="">
        <a href="#pre-owned" aria-controls="pre-owned" role="tab" data-toggle="tab">Pre-Owned</a>
    </li>
    <li role="presentation" class="">
        <a href="#wishlist" aria-controls="wishlist" role="tab" data-toggle="tab">Wishlist</a>
    </li>
    <li role="presentation" class="">
        <a href="#my-orders" aria-controls="my-orders" role="tab" data-toggle="tab">Order History</a>
    </li>
</ul>
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="details">
		<?php echo form_open_multipart(current_url()) ?>
			<?php echo form_hidden('id', $person->id) ?>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="form-group">
						<?= form_hidden('crop_x', '') ?>
						<?= form_hidden('crop_y', '') ?>
						<?= form_hidden('crop_width', '') ?>
						<?= form_hidden('crop_height', '') ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<label class="title" for="userfile">Profile Photo (optional)</label>
							</div>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail text-warning">
								    <?php if($person->avatar){ ?>
			                        	<img src="<?=base_url('assets/images/my_profile/'.$person->avatar) ?>">
			                        <?php }else { ?>
			                        	<img src="<?=base_url('assets/images/my_profile/'.'avtar_image.jpg') ?>">
			                        <?php } ?>
									<img src="<?//= base_url('assets/images/my_profile'.'/'.$person->avatar) ?>">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail">
								</div>
								<div class="btn-group btn-block">
									<div class="btn btn-success btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile">
									</div>
									<a href="#" class="btn btn-danger remove-image" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-9">
					<div class="panel panel-default">
						<div class="panel-heading">
							Profile Information
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="form-group <?php echo form_error('first_name') ? 'has-error' : '' ?>">
										<label class="control-label" for="first_name">First Name</label>
										<input class="form-control" type="text" name="first_name" value="<?= set_value('first_name') ? set_value('first_name') : $person->first_name ?>" />
										<div class="text-danger"><?php echo form_error('first_name') ? form_error('first_name') : '&nbsp' ?></div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="form-group <?php echo form_error('last_name') ? 'has-error' : '' ?>">
										<label class="control-label" for="last_name">Last Name</label>
										<input class="form-control" type="text" name="last_name" value="<?= set_value('last_name') ? set_value('last_name') : $person->last_name ?>" />
										<div class="text-danger"><?php echo form_error('last_name') ? form_error('last_name') : '&nbsp' ?></div>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="alert alert-warning">
										<h4>Update user's Login details</h4>
										<p>The user will not be able to login unless informed of the changes.</p>
										<hr>
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-4">
												<div class="form-group <?php echo form_error('username') ? 'has-error' : '' ?>">
													<label class="control-label" for="username">Username</label>
													<input class="form-control" type="text" name="username" value="<?= set_value('username') ? set_value('username') : $person->username ?>" />
													<div class="text-danger"><?php echo form_error('username') ? form_error('username') : '&nbsp' ?></div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-4">
												<div class="form-group <?php echo form_error('password') ? 'has-error' : '' ?>">
													<label class="control-label" for="password1">Password</label>
													<input class="form-control" type="password" name="password" value="<?= set_value('password') ?>" />
													<div class="text-danger"><?php echo form_error('password') ? form_error('password') : '&nbsp' ?></div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-4">
												<div class="form-group <?php echo form_error('password_confirm') ? 'has-error' : '' ?>">
													<label class="control-label" for="password_confirm">confirm Password</label>
													<input class="form-control" type="password" name="password_confirm" value="<?= set_value('password_confirm') ?>" />
													<div class="text-danger"><?php echo form_error('password_confirm') ? form_error('password_confirm') : '&nbsp' ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="form-group <?php echo form_error('email') ? 'has-error' : '' ?>">
										<label class="control-label" for="email">Email</label>
										<input class="form-control" type="email" name="email" value="<?= set_value('email') ? set_value('email') : $person->email ?>" />
										<div class="text-danger"><?php echo form_error('email') ? form_error('email') : '&nbsp' ?></div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="form-group <?php echo form_error('phone') ? 'has-error' : '' ?>">
										<label class="control-label" for="phone">Phone</label>
										<input class="form-control" type="phone" name="phone" value="<?= set_value('phone') ? set_value('phone') : $person->phone ?>" />
										<div class="text-danger"><?php echo form_error('phone') ? form_error('phone') : '&nbsp' ?></div>
									</div>
								</div>
							<?php if(!empty($user_address[0]->country)): ?>	
								<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
									<label class="control-label" for="address">Country</label>
									<input class="form-control" type="text" name="country" value="<?php echo $user_address[0]->country; ?>" />
									<div class="text-danger"><?php  ?></div>
								</div>
							<?php endif ?>	
							</div>
							<!--<div class="row">-->
							<!--	<div class="col-xs-12 col-sm-12 col-md-12">-->
							<!--		<div class="form-group <?php// echo form_error('address') ? 'has-error' : '' ?>">-->
							<!--			<label class="control-label" for="address">Line Address</label>-->
							<!--			<input class="form-control" type="text" name="address" value="<?//= set_value('address') ? set_value('address') : $person->address ?>" />-->
							<!--			<div class="text-danger"><?php// echo form_error('address') ? form_error('address') : '&nbsp' ?></div>-->
							<!--		</div>-->
							<!--	</div>-->
							<!--</div> -->
							<?php foreach($user_address as $user_address ): ?>
							<?php if(!empty($user_address)): ?>
								<div class="row">					
									<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
										<label class="control-label" for="address">State</label>
										<input class="form-control" type="text" name="state" value="<?php echo $user_address->state;  ?>" />
										<div class="text-danger"><?php  ?></div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
										<label class="control-label" for="address">City</label>
										<input class="form-control" type="text" name="city" value="<?php echo $user_address->city; ?>" />
										<div class="text-danger"><?php  ?></div>
									</div>
									<!--<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
										<label class="control-label" for="address">Pin</label>
										<input class="form-control" type="text" name="pin" value="<?php //echo $user_address->pin; ?>" />
										<div class="text-danger"><?php  ?></div>
									</div>-->
									<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
										<label class="control-label" for="address">Locality</label>
										<input class="form-control" type="text" name="locality" value="<?php echo $user_address->locality; ?>" />
										<div class="text-danger"><?php  ?></div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
										<label class="control-label" for="address">Apartment</label>
										<input class="form-control" type="text" name="apartment_name" value="<?php echo $user_address->apartment_name; ?>" />
										<div class="text-danger"><?php  ?></div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4 form-group <?php ?>">
										<label class="control-label" for="address">Flat No.</label>
										<input class="form-control" type="text" name="flat_no" value="<?php echo $user_address->flat_no; ?>" />
										<div class="text-danger"><?php  ?></div>
									</div>
								</div>
							<?php endif ?>	
							<?php endforeach ?>	
						</div>
					</div>
				</div>
			</div>

			<input type="submit" name="edit_user" value="Update User Profile" class="btn btn-lg btn-primary" />
		<?php echo form_close(); ?>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="points">
		<div class="row">
			<!-- <div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Convert Points to Vouchers
					</div>
					<div class="panel-body">
						<?php //if ($conversion_tiers): ?>
							<?php //echo form_open(current_url()); ?>
								<div class="form-group help-block">
									<strong>
										<?php //echo $reward_data[$this->flexi_cart_admin->db_column('reward_points','total_points_active')]; ?>
									</strong>
									total active reward points.
								</div>
								<?php //$max_conversion_points = $this->flexi_cart_admin->calculate_conversion_reward_points($reward_data[$this->flexi_cart_admin->db_column('reward_points','total_points_active')]); ?>
								<div class="form-group">
									<select name="reward_points" class="form-control">
										<?php //foreach($conversion_tiers as $value) { ?>
											<option value="<?php// echo $value; ?>" <?php //echo set_select('reward_points', $value); ?>>
												<?php //echo $value; ?>
											</option>
										<?php //} ?>
									</select>
								</div>
								<a data-toggle="collapse" data-target="#convert-info"><span class="glyphicon glyphicon-info-sign"></span> more info</a>
								<div class="help-block collapse" id="convert-info">
									Set the number of points that are to be converted to a reward voucher. <br>
									<?php //foreach($conversion_tiers as $value) { ?>
										<div>
											<b><?php// echo $value; ?></b> points worth <b>&pound;<?php// echo $this->flexi_cart_admin->calculate_reward_point_value($value); ?></b>
										</div>
									<?php //} ?>
									Maximum available for this user is <b><?php// echo $max_conversion_points; ?></b> points, worth
									<b>&pound;<?php //echo $this->flexi_cart_admin->calculate_reward_point_value($max_conversion_points); ?></b>
								</div>
									
								<div class="text-right">
									<input type="submit" name="convert_reward_points" value="Convert" class="btn btn-primary"/>
								</div>
							<?php //echo form_close(); ?>
						<?php //else: ?>
							<div class="text-muted">This user does not have enough active reward points to convert to a voucher.</div>
						<?php //endif ?>
					</div>
				</div>
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						User Reward Points
					</div>
					<?php// echo form_open(current_url()); ?>
						<table class="table table-flat table-striped" style="margin:0;font-size:100%">
							<tbody>
								<tr>
									<th>
										Total
										<span class="glyphicon glyphicon-info-sign text-info" data-toggle="tooltip" title="The number of reward points that have been earnt by the user. Any cancelled or refunded items are not included in the total."></span>
									</th>
									<td class="text-right">
										<?php// echo $points_data['total_points']; ?>
									</td>
								</tr>
								<tr>
									<th>
										Pending
										<span class="glyphicon glyphicon-info-sign text-info" data-toggle="tooltip" title="The number of reward points that are pending activation. Once an ordered item has been 'Completed' (Shipped), the earnt points will be enabled after a set number of days."></span>
									</th>
									<td class="text-right">
										<?php //echo $points_data['total_points_pending']; ?>
									</td>
								</tr>
								<tr>
									<th>
										Active
										<span class="glyphicon glyphicon-info-sign text-info" data-toggle="tooltip"  title="The number of reward points that have been earnt by the user, which are active and can be converted to vouchers."></span>
									</th>
									<td class="text-right">
										<?php// echo $points_data['total_points_active']; ?>
									</td>
								</tr>
								<tr>
									<th>
										Expired
										<span class="glyphicon glyphicon-info-sign text-info" data-toggle="tooltip" title="The number of reward points that have expired before they were converted to a reward voucher."></span>
									</th>
									<td class="text-right">
										<?php// echo $points_data['total_points_expired']; ?>
									</td>
								</tr>
								<tr>
									<th>
										Converted
										<span class="glyphicon glyphicon-info-sign text-info" data-toggle="tooltip" title="The number of reward points that have been converted to reward vouchers by the user."></span>
									</th>
									<td class="text-right">
										<?php //echo $points_data['total_points_converted']; ?>
									</td>
								</tr>
								<tr>
									<th>
										Cancelled
										<span class="glyphicon glyphicon-info-sign text-info" data-toggle="tooltip" title="The number of reward points that have been cancelled due to an ordered item being cancelled or refunded."></span>
									</th>
									<td class="text-right">
										<?php //echo $points_data['total_points_cancelled']; ?>
									</td>
								</tr>
							</tbody>
						</table>				
					<?php// echo form_close(); ?>
				</div>
			</div> -->
			<div class="lead page-header">User Reward Points</div>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Available Reward Points : <?php foreach ($user_details as $value) {
							echo $value->total_reward_point;}?>
					</div>
					<div class="panel-body">
						<ul class="nav nav-tabs" role="tablist"> 
							<li role="presentation" class="active">
								<a href="#points-history" aria-controls="points-history" role="tab" data-toggle="tab">
									Reward Points History
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane" id="points-history" style="display: block;">
								<?php if (empty($reward_points)): ?>
									<div class="alert alert-warning" style="margin-top:20px;margin-bottom:0px;">
										There are no reward points available.
									</div>
								<?php else: ?>
									<table class="table table-striped" style="margin:0;font-size:100%">
										<thead class="text-muted">
											<tr>
												<th>
													<div data-toggle="tooltip" >
														Order Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Scrap Request Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Reddem Request Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Credit Reward Point
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Debit Reward Point
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Date Ordered
													</div>
												</th>
											</tr>
										</thead>
										<tbody class="reward_points_history">
										
										<?php foreach ($reward_points as $value){ ?>
											<?php if(empty($value->credit_reward_point) && empty($value->debit_reward_point)): continue;?>
											<?php else:?>
											<tr>
												<td>
													<?php if(empty($value->order_id)):?>
														<?php echo '--'; ?>
													<?php else:?>	
													<a href="<?php echo site_url().'user_dashboard/my-orders/'.$value->order_id ?>"><?php 
													echo $value->order_id; ?>
													</a><?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->scrap_request_id)):?>
														<?php echo '--'; ?>
													<?php else:?>
														<a href="<?php echo site_url().'admin/admin_scrap_details/'.$value->scrap_request_id ?>">
															<?php 
													 echo str_pad((string)$value->scrap_request_id, 8, "0", STR_PAD_LEFT); ?>
															</a>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->redeem_request_id)):?>
														<?php echo '--'; ?>
													<?php else:?>
														<a href="<?php echo site_url().'redeem_request_details' ?>"><?php echo 
													 str_pad((string)$value->redeem_request_id, 8, "0", STR_PAD_LEFT); ?>
															</a>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->credit_reward_point)):?>
														<?php echo '--'; ?>
													<?php else: ?>	
														<?php echo $value->credit_reward_point;?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->debit_reward_point)):?>
														<?php echo '--'; ?>
													<?php else: ?>
													<?php echo $value->debit_reward_point;?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php echo date('d-m-Y H:i:s' ,strtotime($value->date));?>
												</td>
											</tr>
										<?php endif ?>
										<?php } ?>
										</tbody>
									</table>
									<div class="text-center" style="margin-top: 20px;">
									<input type="button" name="reward_point_btn" class="show_button btn btn-primary" value="Show More"></div>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
				<div class="lead page-header">User Redeem History</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						Available Reward Points : <?php foreach($user_details as $result)echo $result->total_reward_point;?>
					</div>
					<div class="panel-body">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#points-history" aria-controls="points-history" role="tab" data-toggle="tab">
									Redeem History
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane" id="points-history" style="display: block;">
								<?php if (empty($redeem)): ?>
									<div class="alert alert-warning" style="margin-top:20px;margin-bottom:0px;">
										There are no reward points available.
									</div>
								<?php else: ?>
									<table class="table table-striped" style="margin:0;font-size:100%">
										<thead class="text-muted">
											<tr>
												<th>
													<div data-toggle="tooltip" >
														Reddem Id
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Total Reward Point
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Request Date
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Status	
													</div>
												</th>
												<th class="text-center">
													<div data-toggle="tooltip" >
														Transaction Completed Date
													</div>
												</th>
											</tr>
										</thead>
										<tbody class="redeem_points_history">
										<?php foreach ($redeem as $value){ ?>
											<tr>
												<td>
													<?php if(empty($value->id)):?>
														<?php echo '--'; ?>
													<?php else:?>	
													<?php echo $value->id; ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(empty($value->total_reward_point)):?>
														<?php echo '--'; ?>
													<?php else:?>
															<?php echo $value->total_reward_point; ?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(date('Y-m-d', strtotime($value->request_date))== '-0001-11-30'):?>
														<?php echo 'NOT AVAILABLE'; ?>
													<?php else:?><?php echo date('jS M Y' ,strtotime(
													 $value->request_date)); ?>
													<?php endif ?> 
												</td>
												<td class="text-center">
													<?php if(empty($value->status)):?>
														<?php echo '--'; ?>
													<?php else: ?>	
														<?php echo $value->status;?>
													<?php endif ?>
												</td>
												<td class="text-center">
													<?php if(date('Y-m-d', strtotime($value->status_modify_date))== '-0001-11-30'):?>
														<?php echo 'NOT AVAILABLE'; ?>
													<?php else:?><?php echo date('jS M Y' ,strtotime(
													 $value->status_modify_date)); ?>
													<?php endif ?>
												</td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
									<div class="text-center" style="margin-top: 20px;">
										<input type="button" name="redeem_show" class="redeem_show btn btn-primary" value="Show More">
									</div>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div role="tabpanel" class="tab-pane" id="scrap_requets">
		<div class="row">
			<div class="panel panel-default panel-inverse">
				<div class="panel-heading">
					Scrap Orders
				</div>
				<div class="panel-body">
					<ul class="nav nav-tabs scrap_list_table" role="tablist">
						<li role="presentation" class="active">
							<a href="#Periodic" class="periodic" aria-controls="Periodic" role="tab" data-toggle="tab">
								Periodic
							</a>
						</li>
						<li role="presentation">
							<a href="#Non-Periodic" class="non-periodic" aria-controls="Non-Periodic" role="tab" data-toggle="tab">
								Non Periodic
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="Periodic">
							<?php if (empty($periodic)): ?>
								<div class="alert alert-warning">
									There are no Periodic Request available.
								</div>
							<?php else: ?>
								<table class="table table-striped">
									<thead class="text-muted">
										<tr>
											<th>
												<div data-toggle="tooltip" >
													Scrap Id
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Requested Date
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Last Picked Up Date
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Next Pick Up Date	
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Not Picked Up Reason
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Request Status
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($periodic as $value){ ?>
										<tr>
											<td>
												<?php if(empty($value->id)):?>
													<?php echo '--'; ?>
												<?php else:?>
												<?php echo anchor('admin/admin_scrap_details/'.$value->id,$value->id); ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if((date('Y-m-d', strtotime($value->requested_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->requested_date))== '1970-01-01')):?>
													<?php echo '--'; ?>
												<?php else:?>
														<?php echo date('jS M Y' ,strtotime($value->requested_date)); ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if((date('Y-m-d', strtotime($value->last_pickup_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->last_pickup_date))== '1970-01-01')):?>
													<?php echo '--'; ?>
												<?php else:?><?php echo date('jS M Y' ,strtotime(
												 $value->last_pickup_date)); ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if((date('Y-m-d', strtotime($value->next_pickup_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->next_pickup_date))== '1970-01-01')):?>
													<?php echo '--'; ?>
												<?php else: ?>	
													<?php echo date('jS M Y' ,strtotime(
												 $value->next_pickup_date));?>
												<?php endif ?>
											</td>
											<td class="text-center">
											    <?php if(empty($value->not_picked_reason)):?>
											        <?php echo '--';?>
												<?php else:?>
														<?php echo $value->not_picked_reason; ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if($value->periodic_request_status==1):?>
													<?php echo 'ACTIVE'; ?>
												<?php else:?>
													<?php echo 'INACTIVE'; ?>
												<?php endif ?>	
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							<?php endif ?>
						</div>
						<div role="tabpanel" class="tab-pane" id="Non-Periodic">
							<?php if (empty($non_periodic)): ?>
								<div class="alert alert-warning">
									There are no Non-Periodic Request available.
								</div>
							<?php else: ?>
								<table class="table table-striped">
									<thead class="text-muted">
										<tr>
											<th>
												<div data-toggle="tooltip" >
													Scrap Id
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Requested Date
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Picked Up Date
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Scrap Weight	
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Scrap Details
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Points
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Not Picked Up Reason
												</div>
											</th>
											<!-- <th class="text-center">
												<div data-toggle="tooltip" >
													Scrap Details
												</div>
											</th> -->
											<th class="text-center">
												<div data-toggle="tooltip" >
													Request Status
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($non_periodic as $value){ ?>
										<tr>
											<td>
												<?php if(empty($value->id)):?>
													<?php echo '--'; ?>
												<?php else:?>
												<?php echo $value->id; ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if((date('Y-m-d', strtotime($value->requested_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->requested_date))== '1970-01-01')):?>
													<?php echo '--'; ?>
												<?php else:?>
														<?php echo date('jS M Y' ,strtotime($value->requested_date)); ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if((date('Y-m-d', strtotime($value->next_pickup_date))== '-0001-11-30') || (date('Y-m-d', strtotime($value->next_pickup_date))== '1970-01-01')):?>
													<?php echo '--'; ?>
												<?php else:?><?php echo date('jS M Y' ,strtotime(
												 $value->next_pickup_date)); ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if(empty($value->scrap_weight)):?>
													<?php echo '--'; ?>
												<?php else: ?>	
													<?php echo $value->scrap_weight;?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if(empty($value->scrap_details)):?>
													<?php echo '--'; ?>
												<?php else:?>
														<?php echo $value->scrap_details; ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if(empty($value->reward_point)):?>
													<?php echo '--'; ?>
												<?php else:?>
														<?php echo $value->reward_point; ?>
												<?php endif ?>
											</td>
											<td class="text-center">
												<?php if(empty($value->not_picked_reason)):?>
													<?php echo '--'; ?>
												<?php else:?>
														<?php echo $value->not_picked_reason; ?>
												<?php endif ?>
											</td>
											<?php /* <td class="text-center">
												<?php if(empty($value->scrap_details)):?>
													<?php echo '--'; ?>
												<?php else:?>
														<?php echo $value->not_picked_reason; ?>
												<?php endif ?>
											</td> */?>
											<td class="text-center">
												<?php if(empty($value->request_status)):?>
													<?php echo '--'; ?>
												<?php else:?>
													<?php echo $value->request_status; ?>
												<?php endif ?>				
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div role="tabpanel" class="tab-pane" id="pre-owned">
	    <?php if(!empty($products)){ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						Product List
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="Periodic">
								<table class="table table-striped" style="margin:0;font-size:100%;vertical-align:middle">
									<thead class="text-muted">
										<tr>
										    <th>
												<div  class="text-center" data-toggle="tooltip" >
													Image
												</div>
											</th>
											<th>
												<div class="text-center" data-toggle="tooltip" >
													Name
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Price
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Available Quantity
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Status
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Category
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Action
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($products as $product){ ?>
										<tr>
										    <td class="text-center" style="width:100px">
												<?php if ($product->images): ?>
                            					<img src="<?php echo base_url($this->data['app']['file_path_product'].$product->images) ?>" class="group list-group-image" style="margin-bottom:2px;width:100px; ">
                            				<?php else: ?>
                            					<img src="<?php echo base_url() ?>assets/system/no_image.jpg" class="group list-group-image" style="width:100px">
                            				<?php endif ?>
											</td>
											<td class="text-center">
												<?php echo $product->item_name ?>
											</td>
											<td class="text-center">
												<?php echo $product->estimated_price ?>
											</td>
											<td class="text-center">
												<?php echo $product->quantity ?>
											</td>
											<td class="text-center">
												<?php 
												$today = strtotime(date('Y-m-d H:i:s'));
												$due_date = strtotime($product->expiry_datetime);
												if($product->status == 1 && $today < $due_date){?>
													ACTIVE
												<?php }else if($product->status == 1 && $today > $due_date){?>
													<span class="blinking">EXPIRED</span>
												<?php }else{ ?>
													CLOSED
												<?php } ?>				
											</td>
											<td class="text-center">
												<?php echo $product->category ?>
											</td>
											<td class="text-center" style="min-width: 400px;">
							<?php if($product->status == 1 && $today < $due_date){ ?>
							
									<!--<a target="_blank" href="<?php// echo base_url(); ?>admin/admin_view_user_products/edit/<?php //echo $product->id ?>" class="btn btn-success btn-md edit_btn">Edit</a>-->
									<a target="_blank" href="<?php echo base_url(); ?>user_inquiries/<?php echo $product->id ?>" class="btn btn-warning btn-md ">View Inquiries</a>
									<a target="_blank" href="<?php echo base_url(); ?>admin/close_product/<?php echo $product->id ?>" class="btn btn-warning btn-md remove_btn">Close Now</a>
									
								<?php }else if($product->status == 1 && $today > $due_date){ ?>
								
									<a target="_blank" href="<?php echo base_url(); ?>admin/repost_product/<?php echo $product->id ?>" class="btn btn-success btn-md edit_btn">Repost</a>
									<a target="_blank" href="<?php echo base_url(); ?>user_inquiries/<?php echo $product->id ?>" class="btn btn-warning btn-md ">View Inquiries</a>
									<a target="_blank" href="<?php echo base_url(); ?>admin/close_product/<?php echo $product->id ?>" class="btn btn-warning btn-md remove_btn">Close Now</a>
									
								<?php }else{ ?>
									<a target="_blank" href="<?php echo base_url(); ?>user_inquiries/<?php echo $product->id ?>" class="btn btn-warning btn-md ">View Inquiries</a>
								<?php } ?>
			
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }else{ ?>
			<p>Sorry! There are no product.</p>
			<br /><br /><br />
		<?php } ?>
	</div>
	<div role="tabpanel" class="tab-pane" id="wishlist">
	    <?php if(!empty($wishlists)){ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default panel-inverse">
					<div class="panel-heading">
						Wishlist 
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="Periodic">
								<table class="table table-striped" style="margin:0;font-size:100%">
									<thead class="text-muted">
										<tr>
											<th>
												<div data-toggle="tooltip" >
													Name
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Quantity Needed
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Status
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Category
												</div>
											</th>
											<th class="text-center">
												<div data-toggle="tooltip" >
													Action
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($wishlists as $product){ ?>
										<tr>
											<td class="text-center">
												<?php echo $product->item_name ?>
											</td>
											<td class="text-center">
												<?php echo $product->quantity ?>
											</td>
											<td class="text-center">
												<?php 
												$today = strtotime(date('Y-m-d H:i:s'));
												$due_date = strtotime($product->expiry_datetime);
												if($product->status == 1 && $today < $due_date){?>
													ACTIVE
												<?php }else if($product->status == 1 && $today > $due_date){?>
													<span class="blinking">EXPIRED</span>
												<?php }else{ ?>
													CLOSED
												<?php } ?>				
											</td>
											<td class="text-center">
												<?php echo $product->category ?>
											</td>
											<td class="text-center">
							<?php if($product->status == 1 && $today < $due_date){ ?>
							
									<!--<a target="_blank" href="<?php //echo base_url(); ?>user/all-wishlist/edit/<?php //echo $product->id ?>" class="btn btn-sm btn-success edit_btn">Edit</a>-->
									<a target="_blank" href="<?php echo base_url(); ?>admin/close_wishlist/<?php echo $product->id ?>" class="btn btn-warning btn-sm remove_btn">Close Now</a>
									
								<?php }else if($product->status == 1 && $today > $due_date){ ?>
								
									<a target="_blank" href="<?php echo base_url(); ?>admin/repost_wishlist/<?php echo $product->id ?>" class="btn btn-success btn-sm edit_btn">Repost</a>
									<a target="_blank" href="<?php echo base_url(); ?>admin/close_wishlist/<?php echo $product->id ?>" class="btn btn-warning btn-sm remove_btn">Close Now</a>
									
								<?php }else{ ?>
									<?php echo '--';?>
								<?php } ?>
			
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }else{ ?>
			<p>Sorry! There are no wishlists.</p>
			<br /><br /><br />
		<?php } ?>
	</div>
	<div role="tabpanel" class="tab-pane" id="my-orders">
	    <?php if (empty($order_data)): ?>
			<div class="alert alert-warning">There are no orders available to view.</div>
		<?php else: ?>
		    <div class="panel-default panel-inverse">
    		    <div class="panel-heading">
    					<h4 class="panel-title">Orders History</h4>
    			</div>
			</div>
			<div class="panel panel-default panel-inverse">
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="">Order Number</th>
							<th class="text-center">Total Items</th>
							<th class="text-center">Total Value</th>
							<th class="text-center">Date</th>
							<th class="text-center">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($order_data as $row): $order_number = $row[$this->flexi_cart_admin->db_column('order_summary', 'order_number')]; ?>
						<tr>
							<td>
								<?php echo anchor('admin/order_details/'.$order_number, $order_number) ?>
							</td>
							<td class="text-center">
								<?php echo number_format($row[$this->flexi_cart_admin->db_column('order_summary', 'total_items')]); ?>
							</td>
							<td class="text-center">
								<?php echo '&#8377;'." ".$row[$this->flexi_cart_admin->db_column('order_summary', 'total')]; ?>
							</td>
							<td class="text-center">
								<?php echo date('jS M Y', strtotime($row[$this->flexi_cart_admin->db_column('order_summary', 'date')])); ?>
							</td>
							<td class="text-center">
								<?php echo $row[$this->flexi_cart_admin->db_column('order_status', 'status')]; ?>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		<?php endif; ?>	
	</div>
</div>



<script type="text/javascript" src="<?= base_url('assets/js/cropper.min.js') ?>"></script>
<script>
	$(document).ready(function() {
		$('.fileinput').on('change.bs.fileinput', function (e) {
			$('.fileinput-preview img').cropper({
				aspectRatio: 1 / 1,
				crop: function(e) {
					$('input[name="crop_width"]').val(e.width);
					$('input[name="crop_height"]').val(e.height);
					$('input[name="crop_x"]').val(e.x);
					$('input[name="crop_y"]').val(e.y);
				}
			});
		});
		jQuery('.remove-image').click(function(){
			jQuery('.fileinput-new img').attr('src','');
		});
	});
</script>
<?php endif ?>
<script type="text/javascript">
jQuery(document).ready(function(){	
    
    var hash = location.hash;
    //alert(hash);
    jQuery('.profile-nav li').each(function(){
        var href = jQuery(this).find('a').attr('href');
        if(href == hash){
            jQuery(this).find('a').click();
        }
    });
    
    
	jQuery('.reward_points_history tr:gt(2)').hide();
	jQuery('.show_button').click(function() {
	    jQuery('.reward_points_history tr:gt(2)').toggle(1000);
	    var text = jQuery('.show_button').attr('value') == 'Show More' ? 'Show Less' : 'Show More';
	    jQuery('.show_button').attr('value',text);
	});
	jQuery('.redeem_points_history tr:gt(2)').hide();
	jQuery('.redeem_show').click(function() {
	    jQuery('.redeem_points_history tr:gt(2)').toggle(1000);
	    var text = jQuery('.redeem_show').attr('value') == 'Show More' ? 'Show Less' : 'Show More';
	    jQuery('.redeem_show').attr('value',text);
	});
	jQuery("body").on('click', '#redeem_btn',function(){
		jQuery.ajax({
	        'type' : 'POST',
	        'url' : "<?php echo base_url('redeem_request'); ?>",
	        'data' : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' },
	        'success' : function(data) {           
				jQuery(".alert-inbox-container").removeClass("hide");
				if(data==1){
        		tag = "<p><strong>NOTICE! </strong> We Already Recieved Your Request. Please Wait For Some Time</p>";
        		jQuery(".alert-inbox-container").addClass("alert-danger");
        		jQuery(".alert-inbox-container").html(tag);
        		}
	        	if(data==2){
	        		tag = "<p><strong>Success! </strong> Successfully Sent Your Request.</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-success");
	        		jQuery(".alert-inbox-container").removeClass("alert-danger");
	        		jQuery(".alert-inbox-container").html(tag);	
	        	}
	        	if(data==3){
	        		tag = "<p><strong>Warning!</strong> User Not Availble.</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-danger");
	        		jQuery(".alert-inbox-container").removeClass("alert-success");
	        		jQuery(".alert-inbox-container").html(tag);
	        	}
	        	if(data==4){
	        		tag = "<p><strong>Warning!</strong> You Don't Have Suffcient Reward Point.</p>";
	        		jQuery(".alert-inbox-container").addClass("alert-danger");
	        		jQuery(".alert-inbox-container").removeClass("alert-success");
	        		jQuery(".alert-inbox-container").html(tag);
	        	}
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });          
	});
});

</script>
<?php $this->load->view('admin/templates/footer') ?>