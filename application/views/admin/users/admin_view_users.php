<?php $this->load->view('admin/templates/header', array(
    'title' => 'Manage User',
	'link' => 'Customer',
	'breadcrumbs' => array(
		0 => array('name'=>'Customer','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">User List</h2>

<div class="blocked_customer">
	<div class="col-md-3">	
	<input type="button" name="blocked_customer_btn" class="btn btn-lg btn-primary blocked_customer_btn" id="blocked_customer_btn" value="Block Customer">
	</div>
	<div class="col-md-3 text-center">
	    <input type="button" name="dlete_customer_btn" class="btn btn-lg btn-primary dlete_customer_btn" id="dlete_customer_btn" value="Delete Customer">
	</div>
	<div class="col-md-3">
	    <input type="button" name="unblock_customer_btn" class="btn btn-lg btn-primary unblock_customer_btn" id="unblock_customer_btn" value="Unblock Customer" style="float:right">
	</div>
	<div class="send_information_div col-md-3">
		<input type="button" name="send_information" id="send_information" value="Send Information" class="btn btn-lg btn-primary send_information" style="float: right;">
		<label style="float: right;">Send SMS or email to user</label>
	</div>
</div>	
<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


<div id="bolcked-customer-modal" class="form-popup2">
    <div class="modal-content">
        <span class="close">&times;</span>
        <div>
        	<br>
            <label for="email"><b>Customer is blocked Successfully.</b></label><br>
            <button class="btn cancel">Close</button>
        </div>
    </div>
</div>
   
<!-- Sending Information to user pop-up part start -->
<div id="send_info" class="form-popup2">
  	<div class="modal-content send_info_message">
    	<span class="close">&times;</span>
	    <div>
	    	<br>
	        <label for="email"><b>Write your message.</b></label><br>
	         <textarea class="form-control" rows="5" id="send_text"></textarea>
	        <div class="sms_inforamtion"><span class="total_character">0</span><span> Characters ( </span><span class="total_sms_credit">0</span><span> SMS credit )</span></div>
	        <br/>
	        <br/>
	        <button class="btn btn-info send_email" value="Email">Email</button>
	        <button class="btn btn-primary send_sms" value="SMS">SMS</button>
	        <button class="btn btn-success send_both" value="Email and SMS">Email and SMS</button>
	        <button class="btn btn-danger dont_send">Cancel</button>
	    </div>
  	</div>
</div>
<!-- Sending Information to user pop-up part end -->
   
<script type="text/javascript">
var modal = document.getElementById('bolcked-customer-modal');
var modal2 = document.getElementById('send_info');
jQuery(document).ready(function() {
   var url      =  window.location.href;
   url = url.split("/");
   if (url[5] == 'edit' || url[5] == 'add') {
   	jQuery('#blocked_customer_btn').hide();
   }
});
$(function () {    	
        jQuery("body").on('click','.close,.cancel',function(event){
    		modal.style.display  = "none";
    		location.reload();
    	});
    	jQuery('.blocked_customer_btn').click(function () {
       // e.preventDefault();
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select Customers");
			return;
		}
		var status = 'block';
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('blocked_customer'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,status:status},
	        'success' : function(data) {  
	            var txt = '<span class="close">&times;</span><div><br><label for="email"><b>Customer is blocked Successfully.</b></label><br><button class="btn cancel">Close</button></div>';
	            jQuery(".modal-content").html(txt);
	            jQuery("#bolcked-customer-modal").css('display','block');
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	jQuery('.unblock_customer_btn').click(function () {
       // e.preventDefault();
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select Customers");
			return;
		}
		var status = 'unblock';
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('blocked_customer'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,status:status},
	        'success' : function(data) {       
	            var txt = '<span class="close">&times;</span><div><br><label for="email"><b>Customer is Unblocked Successfully.</b></label><br><button class="btn cancel">Close</button></div>';
	            jQuery(".modal-content").html(txt);
	            jQuery("#bolcked-customer-modal").css('display','block');
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	jQuery('.dlete_customer_btn').click(function () {
       // e.preventDefault();
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select Customers");
			return;
		}
		var status = 'unblock';
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('delete_user'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,status:status},
	        'success' : function(data) {       
	            var txt = '<span class="close">&times;</span><div><br><label for="email"><b>Customer is Deleted Successfully.</b></label><br><button class="btn cancel">Close</button></div>';
	            jQuery(".modal-content").html(txt);
	            jQuery("#bolcked-customer-modal").css('display','block');
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	//Send Information to user part start
    jQuery('.close,.dont_send').click(function(){
		modal2.style.display = "none";
	});
	jQuery("body").on('click','.close_send_info_model,.close_send_info',function(event){
		modal2.style.display = "none";
      	location.reload();
	});
	jQuery('.send_information').click(function(){
	    var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return false;
		}
		jQuery("#send_text").val('');
		jQuery('.total_sms_credit').text('0');
		jQuery('.total_character').text('0');
		jQuery("#send_info").css('display','block');
	});
	jQuery('.send_email,.send_sms,.send_both').click(function () {
		var email = jQuery(this).val();
		var sms = jQuery(this).val();
		var both = jQuery(this).val();
		var send_text = jQuery('#send_text').val();
		var send_action = '';
		var flag = 'user';
		if(email!='' || email !=undefined){
			send_action = email;
		}
		else if(sms!='' || sms !=undefined){
			send_action = sms;
		}
		else if(both!='' || both !=undefined){
			send_action = both;
		}
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select list");
			return false;
		}
		if(send_text=='' || send_text==undefined){
			alert("Please write message.");
			return false;
		}
		if(send_action=='' || send_text==undefined){
			alert("Error.");
			return false;
		}
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('send_information'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,send_text:send_text,send_action:send_action,flag:flag},
	        'success' : function(data) { 
	        	if(data == 1){
		            var txt = "<span class='close close_send_info'>&times; </span><div><br><p>Messge is empty. Please write message and try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else if(data == 2){
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>An error has been occurred. Please try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else if(data == 3){
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>You did not selected any customer. Please select customer and try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else if(data == 4){
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>Messge sent to user/users successfully.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        	else{
	        		var txt = "<span class='close close_send_info'>&times; </span><div><br><p>An error has been occurred. Please try again.</p><br> <input type='button' class='btn btn-primary close_send_info_model' value='Close'>";
	            	jQuery('.send_info_message').html(txt);
	        	}
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	jQuery('#send_text').keyup(function () {
		var len = jQuery('#send_text').val().length;
		var total_sms_credit = len/160;
		jQuery('.total_character').text(len);
		jQuery('.total_sms_credit').text(Math.floor(total_sms_credit)+1);
		if(len%160==0){
			jQuery('.total_sms_credit').text(Math.floor(total_sms_credit));
		}
	});
	//Send Information to user part end
});
jQuery(document).ready(function(){
    $('.select-all').click(function () {
		if (!$(this).hasClass('checkall')) {
			var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
            $('input[type="checkbox"]', allPages).prop('checked', false);
        } else {
        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
        }
        $(this).toggleClass('checkall');
	});
 });
jQuery('document').ready(function(){
	var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
	$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
});
</script>
<?php $this->load->view('admin/templates/footer'); ?>