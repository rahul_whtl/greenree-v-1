<?php $this->load->view('admin/templates/header', array(
	'link' => 'Customer',
	'breadcrumbs' => array(
		0 => array('name'=>'Customer','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<div class="text-right">
<a href="<?php echo base_url('admin/admin_view_user_products')?>" class="btn btn-primary">Back to User Products</a>
</div>
<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

<?php $this->load->view('admin/templates/footer'); ?>