<?php $this->load->view('admin/templates/header', array(
	'link' => 'Vendor',
	'breadcrumbs' => array(
		0 => array('name'=>'Vendor','link'=>FALSE)
	)
)); ?>
<style type="text/css">
	.dataTables_wrapper { overflow-x: scroll !important;}
</style>
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<h2 class="text-center">Vendors List</h2>
<?php if(!empty($this->session->flashdata('message'))){ ?>
	<div class="alert alert-success">
		 <?php echo $this->session->flashdata('message');  ?>			 
	</div>
<?php } ?>
<div class="blocked_customer">
	<script type="text/javascript">
		// jQuery(document).ready(function() {
		//    var url      =  window.location.href;//alert(url);
		//    url = url.split("/");
		//    if (url[6] == 'edit' || url[6] == 'read' ) {
		//    	jQuery('#blocked_customer_btn').hide();
		//    }
		// });
	</script>
	<div class="assign_to_customer col-sm-4">
		<input type="button" name="send_notification" id="send_notification" value="Send Notification" class="btn btn-lg btn-primary send_notification">
	</div>
	<!-- //Modify Email part start -->
	<div class="form-popup2" id="modify_email_text">
  		<div class="modal-content vendor_admin_message">
            <span class="close">&times;</span>
            <div>
	    	    <br>
    	    	<label for="email"><b>Write here Message for Vendor.</b></label><br>
    	     	<textarea class="form-control" rows="5" id="email_comment"></textarea>
    	     	<br/>
    	    	<button class="btn modify_close">Send</button>
    	    	<button class="btn dont_send">Don't Send</button>
    	    </div>	
    	</div>
	</div>
    <!-- //Modify Email part End -->
</div>	
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    
<div class="output"></div>
    
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

<script type="text/javascript">
	jQuery(document).ready(function() {
	   var url      =  window.location.href;//alert(url);
	   url = url.split("/");//alert(url[6]);
	   if (url[4] == 'read' || url[4] == 'edit' || url[4] == 'add') {
	   	jQuery('#send_notification').hide();	   }
	});
</script>
<script type="text/javascript">
var modal = document.getElementById('modify_email_text');
jQuery(document).ready(function(){
	jQuery('.close, .dont_send').click(function(){
		modal.style.display  = "none";
	});
	jQuery("body").on('click','.close_vendor_success,.close_vendor_notification',function(event){
        modal.style.display  = "none";
        location.reload();
    });
});
$(function () {    
	jQuery('.send_notification').click(function(){
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select Vendors");
			return;
		}
		jQuery("#modify_email_text").css('display','block');
	});	
    jQuery('.modify_close').click(function () {
       // e.preventDefault();
		var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
		var myarray = [];
		var comment = jQuery('#email_comment').val();
		$('input[type="checkbox"]:checked', allPages).each(function(){
			myarray.push(jQuery(this).parent().next().text());
		});	
		if(jQuery.isEmptyObject(myarray)){
			alert("Please select Vendors");
			return;
		}
		jQuery.ajax({
			type : 'POST',
	        url : "<?php echo base_url('admin/vendor_notification'); ?>",
	        data : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',myarray:myarray,comment:comment},
	        'success' : function(data) {             
	            var txt = "<span class='close close_vendor_success'>&times; </span><div><br><p>Vendor has been Notified Successfully.</p><br> <input type='button' class='btn btn-primary close_vendor_notification' value='Close'>";
	            jQuery('.vendor_admin_message').html(txt);
	        },
	        'error' : function(request,error)
	        {
	            alert("Request: "+JSON.stringify(request));
	        }
	    });
	});
	
});
jQuery(document).ready(function(){
    $('.select-all').click(function () {
		
		if (!$(this).hasClass('checkall')) {
			var allPages = datatables_get_chosen_table($('.select-all').closest('.groceryCrudTable')).fnGetNodes();
            $('input[type="checkbox"]', allPages).prop('checked', false);
        } else {
        	var allPages = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));	
        	allPages.$('tr', {"filter": "applied"}).find('input[type="checkbox"]').prop('checked', true);
        }
        $(this).toggleClass('checkall');
			
	});
 });
jQuery('document').ready(function(){

	var allPagesLoad = datatables_get_chosen_table($(this).closest('.groceryCrudTable'));
	$('input[type="checkbox"]', allPagesLoad).prop('checked', false);
		
});
</script>
<?php $this->load->view('admin/templates/footer'); ?>