<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* Name: app Config
*
* Author: 	Yesigye Ignatius
* 			ignatiusyesigye@gmail.com

/*
|--------------------------------------------------------------------------
| File path
|--------------------------------------------------------------------------
| The path for your uploaded files. WITH a trailing slash:
*/
$config['app']['file_path_banner']  = 'assets/images/my_banner/';
$config['app']['file_path_profile'] = 'assets/images/my_profile/';
$config['app']['file_path_product'] = 'assets/images/my_product/';
$config['app']['from_mail_id']     = 'contact@greenree.com';
$config['app']['file_path']         = 'assets/images/greenree_products/';
$config['app']['admin_uri'] = 'greadmin';