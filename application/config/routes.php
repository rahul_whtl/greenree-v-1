<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']   = 'front/index';
$route['404_override']         = 'Errors/_404';
$route['translate_uri_dashes'] = FALSE;

// Pages
$route['page/(:any)'] = 'front/page/$1';

// Shop Products
$route['register_user']               = 'front/register_user';
$route['user-product-info']           = 'front/user_product_info';
$route['product/(:any)']              = 'front/product/$1';
$route['search']                      = 'front/search';
$route['category']                    = 'front/category';
$route['category/(:any)']             = 'front/category/$1';
$route['category/(:any)/(:any)']      = 'front/category/$1/$1';
$route['datatables_test']      = 'admin/datatables_test';

//Pre Owned
$route['my_product_view']                  = 'front/my_product_view';
$route['old-2-gold/product/(:any)']         = 'front/my_product/$1';
$route['old-2-gold/category']               = 'front/my_category';
$route['old-2-gold/category/(:any)']        = 'front/my_category/$1';
$route['old-2-gold/category/(:any)/(:any)'] = 'front/my_category/$1/$1';

//Wishlist
$route['wishlist/category']               = 'front/wishlist_category';
$route['wishlist/category/(:any)']        = 'front/wishlist_category/$1';
$route['wishlist/category/(:any)/(:any)'] = 'front/wishlist_category/$1/$1';

$route['wishlist_user_info']           = 'front/wishlist_user_info';
$route['wishlist_info_login']          = 'front/wishlist_info_login';
$route['add_new_wishlist']             = 'front/add_new_wishlist';
$route['wishlist']                     = 'front/common_wishlist_view';
$route['user_dashboard/edit-wishlist/(:any)'] = 'front/edit_my_wishlist/$1';
$route['user_dashboard/repost-wishlist/(:any)'] = 'front/repost_wishlist/$1';
$route['user_dashboard/edit-repost-wishlist/(:any)'] = 'front/edit_repost_wishlist/$1';
$route['user_dashboard/add-wishlist']  = 'front/wishlist_add_view';
$route['user_dashboard/my-wishlist']   = 'front/wishlist';
$route['view_wishlist_inquiry/(:any)'] = 'front/view_wishlist_inquiry/$1';
$route['user_dashboard/close-wishlist']= 'front/close_my_wishlist';
$route['user_dashboard/close-wishlist/(:any)'] = 'front/close_my_wishlist/$1';
$route['check_wishlist_expiry'] 		 = 'front/check_wishlist_expiry';

// User
$route['google_review']             = 'front/google_review';
$route['google_review_status/(:any)/(:any)'] = 'front/google_review_status/$1/$1';
$route['apartment_otp']              = 'front/apartment_otp';
$route['forgot_password_send_otp']   = 'front/forgot_password_send_otp';
$route['forgot_password_verify_otp'] = 'front/forgot_password_verify_otp';
$route['verify_otp']                 = 'front/verify_otp';
$route['check_email_exist']          = 'front/check_email_exist';
$route['download-app']               = 'front/app_view';
$route['contact_us_msg']             = 'front/contact_us_msg';
$route['contact_us']                 = 'front/contact_us';
$route['privacy_policy']             = 'front/privacy_policy';
$route['about_us']                   = 'front/about_us';
$route['about_us_section/(:any)']    = 'front/about_us_section/$1';
$route['faqs']		    			 = 'front/faqs';
$route['register_login']		     = 'front/register_login';	
$route['user_product_view/(:any)']   = 'front/user_product_view/$1';
$route['user_product_list']          = 'front/user_product_list';
$route['customer_availability/(:any)/(:any)/(:any)'] = 'front/customer_availability/$1/$2/$3';
$route['vendor_availability/(:num)/(:num)'] = 'front/vendor_availability/$1/$2';
$route['check_email']		         = 'front/check_email';		
$route['resend_verify_email']		 = 'front/resend_verify_email';
$route['verify_email_update/(:any)']   = 'front/verify_email_update/$1';
$route['scrap_order_action1']        = 'front/scrap_order_action1';
$route['scrap_order_action']         = 'front/scrap_order_action';
$route['redeem_request_details']     = 'front/redeem_request_details';
$route['scrap_details_view/(:any)']  = 'front/scrap_details_view/$1';
$route['repost_product/(:any)']      = 'front/repost_product/$1';
$route['edit_repost_product']        = 'front/edit_repost_product';
$route['edit_repost_product/(:any)'] = 'front/edit_repost_product/$1';
$route['close_my_product']           = 'front/close_my_product';
$route['close_my_product/(:any)']    = 'front/close_my_product/$1';
$route['check_product_expiry'] 		 = 'front/check_product_expiry';
$route['dashboard/add-new-product']  = 'front/add_new_product';
$route['edit_my_product'] 	         = 'front/edit_my_product';
$route['edit_my_product/(:any)']     = 'front/edit_my_product/$1';
$route['delete_my_product/(:any)']   = 'front/delete_my_product/$1';
$route['view_buyer_inquiry/(:any)']  = 'front/view_buyer_inquiry/$1';
$route['sell_product']       = 'front/sell_product';
$route['shop']               = 'front/shop';
$route['old-2-gold']         = 'front/user_products';
$route['buy_product']        = 'front/buy_product';
$route['redeem_request']     = 'front/redeem_request';
$route['update_request']     = 'front/update_request';
$route['sell-scrap']         = 'front/scrap_request_view';
$route['request_scrap']      = 'front/request_scrap';
$route['locality_list']      = 'front/locality_list';
$route['city_list']          = 'front/city_list';
$route['state_list']         = 'front/state_list';
$route['address_check']      = 'front/address_check';
$route['update_scrap_request/(:any)'] = 'front/update_scrap_request/$1';
$route['update_scrap']       = 'front/update_scrap';
$route['scrap_request']      = 'front/scrap_request';
$route['serviceAddressCheck']= 'front/serviceAddressCheck';
$route['load_calnder']       = 'front/load_calnder';

$route['greadmin'] = 'admin/index';
$route['greadmin/login'] = 'admin/login';

$route['pickup_config'] = 'admin/pickup_config';
$route['periodic_frequency_config']  = 'admin/periodic_frequency_config';
$route['location_pickup_config']     = 'admin/location_pickup_config';
$route['particular_location_config'] = 'admin/particular_location_config';
$route['non-periodic-history']       = 'admin/nonperiodic_scrap_request_history';
$route['non-periodic-history/add']      = 'admin/nonperiodic_scrap_request_history/add';
$route['non-periodic-history/insert'] = 'admin/nonperiodic_scrap_request_history/insert';
$route['non-periodic-history/insert_validation'] = 'admin/nonperiodic_scrap_request_history/insert_validation';
$route['non-periodic-history/success/(:any)'] = 'admin/nonperiodic_scrap_request_history/success/$1';
$route['non-periodic-history/update_validation/(:any)'] = 'admin/nonperiodic_scrap_request_history/update_validation/$1';
$route['non-periodic-history/update/(:any)'] = 'admin/nonperiodic_scrap_request_history/update/$1';
$route['non-periodic-history/edit/(:any)']   = 'admin/nonperiodic_scrap_request_history/edit/$1';
$route['non-periodic-history/read/(:any)']   = 'admin/nonperiodic_scrap_request_history/read/$1';
$route['non-periodic-history/delete/(:any)'] = 'admin/nonperiodic_scrap_request_history/delete/$1';
$route['vendor_notification']         = 'admin/vendor_notification';
$route['vendor_list']                 = 'admin/vendor_list';
$route['vendor_list/add']             = 'admin/vendor_list/add';
$route['vendor_list/insert']          = 'admin/vendor_list/insert';
$route['vendor_list/success/(:any)']  = 'admin/vendor_list/success/$1';
$route['vendor_list/update/(:any)']   = 'admin/vendor_list/update/$1';
$route['vendor_list/update_validation/(:any)'] = 'admin/vendor_list/update_validation/$1';
$route['vendor_list/insert_validation'] = 'admin/vendor_list/insert_validation';
$route['vendor_list/read/(:any)']     = 'admin/vendor_list/read/$1';
$route['vendor_list/edit/(:any)']     = 'admin/vendor_list/edit/$1';
$route['vendor_list/delete/(:any)']   = 'admin/vendor_list/delete/$1';
$route['items']                       = 'admin/items';
$route['insert_item']                 = 'admin/insert_item';
$route['admin_scrap_details_view/(:any)']  = 'admin/admin_scrap_details_view/$1';
// $route['categories/(:any)']  = 'admin/categories/$1';
$route['categories']                 = 'admin/categories';
$route['add_customer']               = 'admin/add_customer';
$route['insert_category']            = 'admin/insert_category';
$route['admin_scrap_details/(:any)'] = 'admin/admin_scrap_details/$1';
$route['edit_redeem_request/(:any)'] = 'admin/edit_redeem_request/$1';
$route['modify_redeem_request']      = 'admin/modify_redeem_request';
$route['redeem-request-view']        = 'admin/redeem_request_view';
$route['redeem-request-view/add']    = 'admin/redeem_request_view/add';
$route['redeem-request-view/insert'] = 'admin/redeem_request_view/insert';
$route['redeem-request-view/success'] = 'admin/redeem_request_view/success';
$route['redeem-request-view/insert_validation'] = 'admin/redeem_request_view/insert_validation';
$route['blocked_customer']           = 'admin/blocked_customer';
$route['admin-view-users']           = 'admin/admin_view_users';
$route['admin-view-users/ajax_list'] = 'admin/admin_view_users/ajax_list';
$route['admin-view-users/insert_validation'] = 'admin/admin_view_users/insert_validation';
$route['admin-view-users/delete/(:any)']     = 'admin/admin_view_users/delete/$1';
$route['admin-view-users/success/(:any)']     = 'admin/admin_view_users/success/$1';
$route['admin-view-users/insert'] = 'admin/admin_view_users/insert';
$route['admin-view-users/add']    = 'admin/admin_view_users/add';
$route['update_user/(:any)']      = 'admin/update_user/$1';
$route['send_notification']       = 'admin/send_notification';
$route['send_information']        = 'admin/send_information';
$route['scrap-request-admin']     = 'admin/scrap_request_admin';
$route['view-all-scrap-request']  = 'admin/view_all_scrap_request';
$route['periodic-history']        = 'admin/scrap_request_history';
$route['periodic-history/add']    = 'admin/scrap_request_history/add';
$route['periodic-history/insert'] = 'admin/scrap_request_history/insert';
$route['periodic-history/insert_validation'] = 'admin/scrap_request_history/insert_validation';
$route['periodic-history/success/(:any)'] = 'admin/scrap_request_history/success/$1';
$route['periodic-history/update_validation/(:any)'] = 'admin/scrap_request_history/update_validation/$1';
$route['periodic-history/update/(:any)'] = 'admin/scrap_request_history/update/$1';
$route['periodic-history/edit/(:any)']   = 'admin/scrap_request_history/edit/$1';
$route['periodic-history/read/(:any)']   = 'admin/scrap_request_history/read/$1';
$route['periodic-history/delete/(:any)'] = 'admin/scrap_request_history/delete/$1';
$route['user_inquiries/(:any)']        = 'admin/user_inquiries/$1';
$route['user_inquiries/(:any)/read/(:any)']  = 'admin/user_inquiries/$1/read/$1';
$route['user_inquiries/(:any)/delete/(:any)']  = 'admin/user_inquiries/$1/delete/$1';
$route['scrap-request-admin/add']      = "admin/scrap_request_admin/add";
$route['scrap-request-admin/insert']   = "admin/scrap_request_admin/insert"; 
$route['scrap-request-admin/insert_validation'] = "admin/scrap_request_admin/insert_validation";
$route['admin_view_user_products/add'] ="admin/admin_view_user_products/add";
$route['admin_view_user_products/insert_validation'] ="admin/admin_view_user_products/insert_validation";
$route['admin_view_user_products/insert'] ="admin/admin_view_user_products/insert";
$route['admin_view_user_products/success/(:any)'] ="admin/admin_view_user_products/success/$1";
$route['admin_view_user_products/update_validation/(:any)'] ="admin/admin_view_user_products/update_validation/$1";
$route['admin_view_user_products/update/(:any)'] ="admin/admin_view_user_products/update/$1";
$route['admin_view_user_products/read/(:any)'] ="admin/admin_view_user_products/read/$1";
$route['admin_view_user_products/edit/(:any)'] ="admin/admin_view_user_products/edit/$1";
$route['admin_view_user_products']      = "admin/admin_view_user_products";
$route['admin_view_user_products/delete/(:any)'] = "admin/admin_view_user_products/delete/(:any)";
$route['admin_view_user_products/upload_file/images']      = "admin/admin_view_user_products/upload_file/images";
$route['admin_view_user_products/delete_file/images/(:any)']      = "admin/admin_view_user_products/delete_file/images/(:any)";
$route['admin_view_product_categories'] = "admin/admin_view_product_categories";
$route['admin_view_product_categories/add'] = "admin/admin_view_product_categories/add";
$route['admin_view_product_categories/insert'] = "admin/admin_view_product_categories/insert";
$route['admin_view_product_categories/insert_validation']        = "admin/admin_view_product_categories/insert_validation";
$route['admin_view_product_categories/update_validation/(:any)'] = "admin/admin_view_product_categories/update_validation/$1";
$route['admin_view_product_categories/update/(:any)'] = "admin/admin_view_product_categories/update/$1";
$route['user/all-wishlist']        = "admin/admin_view_user_wishlist";
$route['user/all-wishlist/delete/(:any)']        = "admin/admin_view_user_wishlist/delete/(:any)";
$route['user/all-wishlist/insert'] = "admin/admin_view_user_wishlist/insert";
$route['user/all-wishlist/insert_validation'] = "admin/admin_view_user_wishlist/insert_validation";
$route['user/all-wishlist/success/(:any)']    = "admin/admin_view_user_wishlist/success/$1";
$route['user/all-wishlist/update_validation/(:any)'] = "admin/admin_view_user_wishlist/update_validation/$1";
$route['user/all-wishlist/update/(:any)'] = "admin/admin_view_user_wishlist/update/$1";
$route['user/all-wishlist/add']         = "admin/admin_view_user_wishlist/add";
$route['user/all-wishlist/read/(:any)'] = "admin/admin_view_user_wishlist/read/$1";
$route['user/all-wishlist/edit/(:any)'] = "admin/admin_view_user_wishlist/edit/$1";
$route['user/wishlist-categories']      = "admin/admin_view_wishlist_categories";
$route['user/wishlist-categories/insert_validation'] = "admin/admin_view_wishlist_categories/insert_validation";
$route['user/wishlist-categories/update_validation/(:any)'] = "admin/admin_view_wishlist_categories/update_validation/$1";
$route['user/wishlist-categories/update/(:any)'] = "admin/admin_view_wishlist_categories/update/$1";
$route['user/wishlist-categories/insert'] = "admin/admin_view_wishlist_categories/insert";
$route['user/wishlist-categories/delete/(:any)'] = "admin/admin_view_wishlist_categories/delete/$1";
$route['user/wishlist-categories/success/(:any)'] = "admin/admin_view_wishlist_categories/success/$1";
$route['user/wishlist-categories/add']  = "admin/admin_view_wishlist_categories/add";
$route['user/wishlist-categories/edit/(:any)']  = "admin/admin_view_wishlist_categories/edit/$1";
$route['user/wishlist-categories/read/(:any)']  = "admin/admin_view_wishlist_categories/read/$1";
$route['admin_view_product_categories/read/(:any)']   = "admin/admin_view_product_categories/read/$1";
$route['admin_view_product_categories/edit/(:any)']   = "admin/admin_view_product_categories/edit/$1";
$route['admin_view_product_categories/delete/(:any)'] = "admin/admin_view_product_categories/delete/$1";
$route['scrap-request-admin/edit/(:any)']   = "admin/scrap_request_admin/edit/$1"; 
$route['scrap-request-admin/read/(:any)']   = "admin/scrap_request_admin/read/$1"; 
$route['scrap-request-admin/delete/(:any)'] = "admin/scrap_request_admin/delete/$1";
$route['scrap-request-admin/clone/(:any)']  = "admin/scrap_request_admin/clone/$1";
$route['scrap-request-admin/update/(:any)']  = "admin/scrap_request_admin/update/$1";
$route['scrap-request-admin/update_validation/(:any)']  = "admin/scrap_request_admin/update_validation/$1";
$route['scrap-request-admin/insert_validation'] = "admin/scrap_request_admin/insert_validation";

$route['delete_user']      = 'admin/delete_user';
$route['assign-to-vendor'] = 'admin/scrap_assign_vendor';
$route['orders']           = 'admin/orders';
$route['orders/delete/(:any)'] = 'admin/orders/delete/$1';
$route['location_types']   = 'admin/location_types';
$route['insert-location']   = 'admin/insert_location';
$route['zones']            = 'admin/zones';
$route['shipping']         = 'admin/shipping';
$route['tax']              = 'admin/tax';
// $route['login'] = 'admin/login';
$route['item_discounts']         = 'admin/item_discounts';
$route['summary_discounts']      = 'admin/summary_discounts';
$route['discount_groups']        = 'admin/discount_groups';
$route['user_reward_points']     = 'admin/user_reward_points';
$route['user_reward_points/delete/(:any)'] = 'admin/user_reward_points/delete/$1';
$route['user_reward_points/add'] = 'admin/user_reward_points/add';
$route['user_reward_points/insert'] = 'admin/user_reward_points/insert';
$route['user_reward_points/insert_validation'] = 'admin/user_reward_points/insert_validation';
$route['user_reward_points/edit/(:any)']   = 'admin/user_reward_points/edit/$1';
$route['user_reward_points/add/(:any)']    = 'admin/user_reward_points/add/$1';
$route['user_reward_points/edit/(:any)']   = 'admin/user_reward_points/edit/$1';
$route['user_reward_points/update/(:any)'] = 'admin/user_reward_points/update/$1';
$route['user_reward_points/success/(:any)'] = 'admin/user_reward_points/success/$1';
$route['user_reward_points/update_validation/(:any)'] = 'admin/user_reward_points/update_validation/$1';
$route['vouchers']              = 'admin/vouchers';
$route['pages']                 = 'admin/pages';
$route['banners']               = 'admin/banners';
$route['banners/(:any)']        = 'admin/banners/$1';
$route['config']                = 'admin/config';
$route['defaults']              = 'admin/defaults';
$route['currency']              = 'admin/currency';
$route['order_status']          = 'admin/order_status';

$route['login']                 = 'front/login';
$route['verify_email/(:any)']   = 'front/verify_email/$1';
$route['login_otp']             = 'front/login_otp';
$route['otp_generation']        = 'front/otp_generation';
$route['mobile_verification']   = 'front/mobile_verification';
$route['verify_register_otp']   = 'front/verify_register_otp';
$route['logout']                = 'front/logout';
$route['change_password']       = 'front/change_password';
$route['forgot_password']       = 'front/forgot_password';
$route['forgot_pwd_redirect']       = 'front/forgot_pwd_redirect';
//$route['reset_password']        = 'front/reset_password';
$route['reset_password/(:any)'] = 'front/reset_password/$1';
$route['register']              = 'front/register';
/*$route['auth/login']            = 'front/login';
$route['auth/reset_password/(:any)'] = 'front/reset_password/$1';
$route['auth/forgot_password']       = 'front/forgot_password';*/


// User dashboard
$route['order-cancel/(:any)']             = 'front/user_order_cancel/$1';
$route['user-dashboard']                  = 'front/user_dashboard';
$route['user_dashboard/my-orders']        = 'front/orders';
$route['user_dashboard/my-orders/(:any)'] = 'front/order/$1';
$route['user_dashboard/carts']            = 'front/carts';
$route['user_dashboard/my-wallet-cash']   = 'front/points_vouchers';
$route['user_dashboard/my-scrap-orders']  = 'front/scrap_order_list';
$route['user_dashboard/my-products']      = 'front/my_products_view';
$route['profile']                         = 'front/profile';


// Pages
$route['page']        = 'front/page';
$route['page/(:any)'] = 'front/page/$1';

// Cart
$route['cart']                       = 'front/cart';
$route['cart/(:any)']                = 'front/cart/$1';
$route['cart/(:any)/(:any)']         = 'front/cart/$1/$1';
$route['delete_cart_item']           = 'front/delete_cart_item';
$route['delete_cart_item/(:any)']    = 'front/delete_cart_item/$1';
$route['unset_cart_discount']        = 'front/unset_cart_discount';
$route['unset_cart_discount/(:any)'] = 'front/unset_cart_discount/$1';
$route['get_state']                       = 'front/get_state';
$route['get_city']                       = 'front/get_city';
$route['get_locality']                       = 'front/get_locality';
$route['get_apartment']                       = 'front/get_apartment';

// Payments
$route['checkout_review']     = 'front/checkout_review';
$route['do_checkout_payment'] = 'front/do_checkout_payment';
$route['checkout_success']    = 'front/checkout_success';
$route['checkout_failed']     = 'front/checkout_failed';
$route['checkout']            = 'front/checkout';
$route['checkout_complete']   = 'front/checkout_complete';
$route['checkout_failed']     = 'front/checkout_failed';


// Cart Controls
$route['load_save_cart_data']          = 'front/load_save_cart_data';
$route['save_cart_data']               = 'front/save_cart_data';
$route['load_cart_data']               = 'front/load_cart_data';
$route['load_cart_data/(:any)']        = 'front/load_cart_data/$1';
$route['load_cart_data/(:any)/(:any)'] = 'front/load_cart_data/$1/$1';
$route['delete_cart_data']             = 'front/delete_cart_data';
$route['delete_cart_data/(:any)']      = 'front/delete_cart_data/$1';
$route['update_cart']                  = 'front/update_cart';
$route['clear_cart']                   = 'front/clear_cart';
$route['destroy_cart']                 = 'front/destroy_cart';


// Discounts
$route['update_discount_codes'] = 'front/update_discount_codes';
$route['set_discount']          = 'front/set_discount';
$route['set_discount/(:any)']   = 'front/set_discount/$1';
$route['remove_discount_code']  = 'front/remove_discount_code';
$route['remove_all_discounts']  = 'front/remove_all_discounts';
$route['unset_discount']        = 'front/unset_discount';
$route['unset_discount/(:any)'] = 'front/unset_discount/$1';
