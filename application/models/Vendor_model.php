<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->config('app');
		$this->app = $this->config->item('app');
	}

	public function vendors_list(){

        $this->db->select('id');
        $this->db->select('name');
        $this->db->where('status',true);
	    $query = $this->db->get('gre_vendors');
	    $result = $query->result_array();
		return $result;
	}
	public function edit_redeem_request($redeem_id){
	    $user_id = $this->db->select('user_id')->where('id',$redeem_id)->get('gre_redeem_request')->row()->user_id;
	    $redeem_reward_point = $this->db->select('total_reward_point')->where('id',$redeem_id)->get('gre_redeem_request')->row()->total_reward_point;
	    $total_reward_point  = $this->db->select('total_reward_point')->where('id',$user_id)->get('users')->row()->total_reward_point;
	    $left_reward_point   = $total_reward_point - $redeem_reward_point;
    	$data = array(
		         	'status'             => 'Completed',
		         	'status_modify_date' => date("Y-m-d H:i:s")
		            );
    	$data1 = array(
    		         'total_reward_point' => $left_reward_point );
		$data2 = array(
	         	'user_id'            => $user_id,
	         	'redeem_request_id'  => $redeem_id,
	         	'debit_reward_point' => $redeem_reward_point,
	         	'date'               => date("Y-m-d H:i:s")
	            );
		//Updating reward point table 
			$this->db->insert('gre_reward_point',$data2);
        //Updating users table 
		$this->db->where('id', $user_id);
			$this->db->update('users',$data1); 
        //Updating redeem_request table 
		$this->db->where('id', $redeem_id);
		$this->db->update('gre_redeem_request',$data); 

		$this->db->select('*');
	    $this->db->where('id',$user_id);
		$query_user = $this->db->get('users'); 		
	    $email = $query_user->row()->email;
			$data_email = array(
                        'redeem_id'   => $redeem_id,
                        'user'        => $query_user->row()->first_name.' '.$query_user->row()->last_name,
                        'redeem_cash' => $redeem_reward_point,
                        'email_for'   => 'user'
                    );
        if($query_user->row()->email_verified){            
            $config = Array(
                    'protocol' => 'smtp',
                    /*'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',*/
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $msg = $this->load->view('public/email/redeem_completed_mail', $data_email,  TRUE);
            $message = $msg;
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject('Redeem request completed ('.$redeem_id.')');
            $this->email->message($message);
            $this->email->send();
            $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
            $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
            //mail for admin
            $data_email1 = array(
                 'user_id'          => $user_id,
                 'redeem_cash'      => $redeem_reward_point,
                 'redeem_id'        => $redeem_id,
                 'user'             => $query_user->row()->first_name.' '.$query_user->row()->last_name,
                 'email_for'		=> 'admin'
             );
            $msg1 =$this->load->view('public/email/redeem_completed_mail', $data_email1,  TRUE);
            $message1 = $msg1;
        	$this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($admin_mail_id);
            $this->email->subject('Redeem request completed ('.$redeem_id.')');
            $this->email->message($msg1);
            $this->email->send();
            $message = 'We have credited your Wallet cash amount in to your Paytm account. Please check your Paytm account.. '.'Your Redeem id is '.$redeem_id;
            $sms_status = $this->users_model->common_sms_api($query_user->row()->phone, $message);
        }    
    }
    public function modify_redeem_request($modify_redeem_ids){
    	$data = array(
		         	'status'             => 'Completed',
		         	'status_modify_date' => date("Y-m-d H:i:s")
		            );
    	$data1 = array(
    		         'total_reward_point' => 0 );
		foreach ($modify_redeem_ids as $value){
			$this->db->select('*');
			$this->db->where('id', $value);
			$query              = $this->db->get('gre_redeem_request');
			$user_id            = $query->row()->user_id;
			$total_reward_point = $query->row()->total_reward_point;
            $redeem_status      = $query->row()->status;
            if ($redeem_status == 'Completed'){
                continue;
            }
            else{
    			$data2 = array(
    		         	'user_id'            => $user_id,
    		         	'redeem_request_id'  => $value,
    		         	'debit_reward_point' => $total_reward_point,
    		         	'date'               => date("Y-m-d H:i:s")
    		            );
    			//Updating reward point table 
      			$this->db->insert('gre_reward_point',$data2);
                //Updating users table 
    			$this->db->where('id', $user_id);
      			$this->db->update('users',$data1); 
                //Updating redeem_request table 
    			$this->db->where('id', $value);
      			$this->db->update('gre_redeem_request',$data); 
    
      			$this->db->select('*');
    		    $this->db->where('id',$user_id);
    		    $query_user = $this->db->get('users'); 		
    		    $email = $query_user->row()->email;
    		    $username = $query_user->row()->first_name.' '.$query_user->row()->last_name;
      			$data_email = array(
      			                'user'      => $username,
                                'redeem_id' => $value,
                                'email_for' => 'user',
                                'redeem_cash' => $query->row()->total_reward_point
                            );
                if($query_user->row()->email_verified){            
                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'localhost',
                        'smtp_port' => 25,
                        'smtp_auth' => FALSE,
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $msg = $this->load->view('public/email/redeem_completed_mail', $data_email,  TRUE);
                    $message = $msg;
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($email);
                    $this->email->subject('Redeem request completed ('.$value.')');
                    $this->email->message($message);
                    $this->email->send();
        
                    //mail for admin
                    $data_email1 = array(
                                    'user'      => $username,
                                    'redeem_id' => $value,
                                    'email_for'	=> 'admin',
                                    'user_id'   => $user_id,
                                    'redeem_cash' => $query->row()->total_reward_point
                     );
                    $msg1 = $this->load->view('public/email/redeem_completed_mail', $data_email1,  TRUE);
                    $message1 = $msg1;
                    $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                    $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                	$this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($admin_mail_id);
                    $this->email->subject('Redeem request completed ('.$value.')');
                    $this->email->message($message1);
                    $this->email->send();
                }
                $message = 'We have credited your Wallet cash amount in to your Paytm account. Please check your Paytm account. '.'Your Redeem id is '.$value;
                $sms_status = $this->users_model->common_sms_api($query_user->row()->phone, $message);
            }
		}
    }
	public function send_notification($scrap_array,$pick_up_date, $pick_up_max_time){
		$date         = date('Y-m-d H:i:s');
		$pick_up_date = date('Y-m-d H:i:s', strtotime($pick_up_date));
        $pick_up_max_time = date('H:i:s', strtotime($pick_up_max_time));
        $pick_up_min_time = date('H:i:s', strtotime($pick_up_date));
		foreach ($scrap_array as $value){
			//log_message('debug',$value);
			$this->db->select('user_id, request_type, first_pickup_date, last_pickup_date,  next_pickup_date');
	        $this->db->where('id',$value);
		    $query = $this->db->get('gre_scrap_request');
		    foreach ($query->result() as $var) {    	
		    	$first_pickup_date  = $var->first_pickup_date;
		    	$last_pickup_date   = $var->last_pickup_date;
		    	$next_pickup_date   = $var->next_pickup_date;
		    	$first_pickup_date  = date('Y-m-d  H:i:s', strtotime($first_pickup_date));
		    	$last_pickup_date   = date('Y-m-d H:i:s', strtotime($last_pickup_date));
		    	$next_pickup_date   = date('Y-m-d H:i:s', strtotime($next_pickup_date));
		    	$request_type       = $var->request_type;
                if((date('Y-m-d',strtotime($first_pickup_date)) =='1970-01-01') || (date('Y-m-d',strtotime($first_pickup_date)) =='-0001-11-30')){
                    $data = array(
					        'customer_response'  => 'No Response',
				         	'next_pickup_date'   => $pick_up_date,
				         	'first_pickup_date'  => $pick_up_date,
				         	'last_pickup_date'   => $pick_up_date,
                            'time_slot_min'      => $pick_up_min_time,
                            'time_slot_max'      => $pick_up_max_time
				            );
                    $this->db->where('id', $value);
					$this->db->update('gre_scrap_request',$data); 
                }
                else{
                 log_message('debug','next_pickup_date');
                
                	$data = array(
					        'customer_response'  => 'No Response',
				         	'next_pickup_date'   => $pick_up_date,
				         	'last_pickup_date'   => $next_pickup_date,
                            'time_slot_min'      => $pick_up_min_time,
                            'time_slot_max'      => $pick_up_max_time
				            );
                    $this->db->where('id', $value);
					$this->db->update('gre_scrap_request',$data); 
                }
		    }
		}	      
	}
    public function periodic_request_archive($scrapids){
        //for moving data into Periodic Request Archive Table
	 	foreach ($scrapids as $scrap_id){
	 	 	$this->db->select('id,periodic_interval,vendor_id,reward_point,scrap_weight,customer_presence,scrap_details,user_id,last_pickup_date,first_pickup_date,customer_response,not_picked_reason,list_created_date,list_created_group,apartment_otp,prefered_pickup_day,time_slot_min,time_slot_max,request_status');
            $this->db->where('id',$scrap_id);
	        $this->db->where('request_type','Periodic');
			$query = $this->db->get('gre_scrap_request');
			foreach ($query->result() as $value){
				$first_pickup_date = $value->first_pickup_date;
				$first_pickup_date = date('Y-m-d', strtotime($first_pickup_date));
					if (($first_pickup_date =='1970-01-01') || ($first_pickup_date =='-0001-11-30')) {log_message('debug','sadffds1');
			 	 		continue;
			 	 	}
			 	else{
			 		$customer_response = $value->customer_response; 
			 		if(empty($value->customer_response))
			 			$customer_response = "";
			 		$data1 = array(
        		         	'scrap_request_id'   => $value->id,
        		         	'periodic_interval'  => $value->periodic_interval,
        		         	'vendor_id'          => $value->vendor_id,
        		         	'reward_point'       => $value->reward_point,
        		         	'scrap_weight'       => $value->scrap_weight,
        		         	'customer_presence'  => $value->customer_presence,
        		         	'scrap_details'      => $value->scrap_details,
        		         	'user_id'            => $value->user_id,
        		         	'picked_up_date'     => $value->last_pickup_date,
        		         	'customer_response'  => $customer_response,
        		         	'not_picked_reason'  => $value->not_picked_reason,
        		         	'list_created_date'  => $value->list_created_date,
        		         	'list_created_group' => $value->list_created_group,
                            'archive_date'       => date('Y-m-d  H:i:s'),
                            'apartment_otp'      => $value->apartment_otp,
                            'prefered_pickup_day'=> $value->prefered_pickup_day,
                            'time_slot_min'      => $value->time_slot_min,
                            'time_slot_max'      => $value->time_slot_max
                            //'request_status'     => $value->request_status
        		        );	
        		    $this->db->insert('gre_periodic_request_archive',$data1);
		        }
		    }
      		$result = 1;//User is Available
	 	 	return $result;
   		}
    }
	public function blocked_customer($block_customer_ids,$status){
	    if($status == 'block')
    	    $data = array(
    		         	'blocked_customer' => 1
    		            );
        else  
            $data = array(
    		         	'blocked_customer' => 0
    		            );
		foreach ($block_customer_ids as $value){
			$this->db->where('id', $value);
  			$this->db->update('users',$data); 
		}	      
	}
	
}