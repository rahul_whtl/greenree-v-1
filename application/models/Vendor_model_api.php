<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model_api extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->config('app');
		$this->app = $this->config->item('app');
	}

	public function vendors_list(){

        $this->db->select('id');
        $this->db->select('name');
        $this->db->where('status',true);
	    $query = $this->db->get('gre_vendors');
	    $result = $query->result_array();
		return $result;
	}
	public function edit_redeem_request($redeem_id){
	    $user_id = $this->db->select('user_id')->where('id',$redeem_id)->get('gre_redeem_request')->row()->user_id;
	    $redeem_reward_point = $this->db->select('total_reward_point')->where('id',$redeem_id)->get('gre_redeem_request')->row()->total_reward_point;
	    $total_reward_point  = $this->db->select('total_reward_point')->where('id',$user_id)->get('users')->row()->total_reward_point;
	    $left_reward_point   = $total_reward_point - $redeem_reward_point;
    	$data = array(
		         	'status'             => 'Completed',
		         	'status_modify_date' => date("Y-m-d H:i:s")
		            );
    	$data1 = array(
    		         'total_reward_point' => $left_reward_point );
		$data2 = array(
	         	'user_id'            => $user_id,
	         	'redeem_request_id'  => $redeem_id,
	         	'debit_reward_point' => $redeem_reward_point,
	         	'date'               => date("Y-m-d H:i:s")
	            );
		//Updating reward point table 
			$this->db->insert('gre_reward_point',$data2);
        //Updating users table 
		$this->db->where('id', $user_id);
			$this->db->update('users',$data1); 
        //Updating redeem_request table 
		$this->db->where('id', $redeem_id);
		$this->db->update('gre_redeem_request',$data); 

		$this->db->select('*');
	    $this->db->where('id',$user_id);
		$query_user = $this->db->get('users'); 		
	    $email = $query_user->row()->email;
			$data_email = array(
                        'redeem_id'   => $redeem_id,
                        'user'        => $query_user->row()->first_name.' '.$query_user->row()->last_name,
                        'redeem_cash' => $redeem_reward_point,
                        'email_for'   => 'user'
                    );
        if($query_user->row()->email_verified){            
            $config = Array(
                    'protocol' => 'smtp',
                    /*'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',*/
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $msg = $this->load->view('public/email/redeem_completed_mail', $data_email,  TRUE);
            $message = $msg;
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject('Redeem request completed ('.$redeem_id.')');
            $this->email->message($message);
            $this->email->send();
            $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
            $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
            //mail for admin
            $data_email1 = array(
                 'user_id'          => $user_id,
                 'redeem_cash'      => $redeem_reward_point,
                 'redeem_id'        => $redeem_id,
                 'user'             => $query_user->row()->first_name.' '.$query_user->row()->last_name,
                 'email_for'		=> 'admin'
             );
            $msg1 =$this->load->view('public/email/redeem_completed_mail', $data_email1,  TRUE);
            $message1 = $msg1;
        	$this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($admin_mail_id);
            $this->email->subject('Redeem request completed ('.$redeem_id.')');
            $this->email->message($msg1);
            $this->email->send();
            $message = 'We have credited your Wallet cash amount in to your Paytm account. Please check your Paytm account.. '.'Your Redeem id is '.$redeem_id;
            $sms_status = $this->users_model->common_sms_api($query_user->row()->phone, $message);
        }    
    }
    public function modify_redeem_request($modify_redeem_ids){
    	$data = array(
		         	'status'             => 'Completed',
		         	'status_modify_date' => date("Y-m-d H:i:s")
		            );
    	$data1 = array(
    		         'total_reward_point' => 0 );
		foreach ($modify_redeem_ids as $value){
			$this->db->select('user_id,total_reward_point');
			$this->db->where('id', $value);
			$query              = $this->db->get('gre_redeem_request');
			$user_id            = $query->row()->user_id;
			$total_reward_point = $query->row()->total_reward_point;
			$data2 = array(
		         	'user_id'            => $user_id,
		         	'redeem_request_id'  => $value,
		         	'debit_reward_point' => $total_reward_point,
		         	'date'               => date("Y-m-d H:i:s")
		            );
			//Updating reward point table 
  			$this->db->insert('gre_reward_point',$data2);
            //Updating users table 
			$this->db->where('id', $user_id);
  			$this->db->update('users',$data1); 
            //Updating redeem_request table 
			$this->db->where('id', $value);
  			$this->db->update('gre_redeem_request',$data); 

  			$this->db->select('*');
		    $this->db->where('id',$user_id);
		    $query_user = $this->db->get('users'); 		
		    $email = $query_user->row()->email;
		    $username = $query_user->row()->first_name.' '.$query_user->row()->last_name;
  			$data_email = array(
  			                'user'      => $username,
                            'redeem_id' => $value,
                            'email_for' => 'user',
                            'redeem_cash' => $query->row()->total_reward_point
                        );
            if($query_user->row()->email_verified){            
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $msg = $this->load->view('public/email/redeem_completed_mail', $data_email,  TRUE);
                $message = $msg;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($email);
                $this->email->subject('Redeem request completed ('.$value.')');
                $this->email->message($message);
                $this->email->send();
    
                //mail for admin
                $data_email1 = array(
                                'user'      => $username,
                                'redeem_id' => $value,
                                'email_for'	=> 'admin',
                                'user_id'   => $user_id,
                                'redeem_cash' => $query->row()->total_reward_point
                 );
                $msg1 = $this->load->view('public/email/redeem_completed_mail', $data_email1,  TRUE);
                $message1 = $msg1;
                $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
            	$this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($admin_mail_id);
                $this->email->subject('Redeem request completed ('.$value.')');
                $this->email->message($message1);
                $this->email->send();
            }
            $message = 'We have credited your Wallet cash amount in to your Paytm account. Please check your Paytm account. '.'Your Redeem id is '.$value;
            $sms_status = $this->users_model->common_sms_api($query_user->row()->phone, $message);
		}
    }
	public function send_notification($scrap_array,$pick_up_date, $pick_up_max_time){
		$date         = date('Y-m-d H:i:s');
		$pick_up_date = date('Y-m-d H:i:s', strtotime($pick_up_date));
        $pick_up_max_time = date('H:i:s', strtotime($pick_up_max_time));
        $pick_up_min_time = date('H:i:s', strtotime($pick_up_date));
		foreach ($scrap_array as $value){
			//log_message('debug',$value);
			$this->db->select('user_id, request_type, first_pickup_date, last_pickup_date,  next_pickup_date');
	        $this->db->where('id',$value);
		    $query = $this->db->get('gre_scrap_request');
		    foreach ($query->result() as $var) {    	
		    	$first_pickup_date  = $var->first_pickup_date;
		    	$last_pickup_date   = $var->last_pickup_date;
		    	$next_pickup_date   = $var->next_pickup_date;
		    	$first_pickup_date  = date('Y-m-d  H:i:s', strtotime($first_pickup_date));
		    	$last_pickup_date   = date('Y-m-d H:i:s', strtotime($last_pickup_date));
		    	$next_pickup_date   = date('Y-m-d H:i:s', strtotime($next_pickup_date));
		    	$request_type       = $var->request_type;
                if((date('Y-m-d',strtotime($first_pickup_date)) =='1970-01-01') || (date('Y-m-d',strtotime($first_pickup_date)) =='-0001-11-30')){
                    $data = array(
					        'customer_response'  => 'No Response',
				         	'next_pickup_date'   => $pick_up_date,
				         	'first_pickup_date'  => $pick_up_date,
				         	'last_pickup_date'   => $pick_up_date,
                            'time_slot_min'      => $pick_up_min_time,
                            'time_slot_max'      => $pick_up_max_time
				            );
                    $this->db->where('id', $value);
					$this->db->update('gre_scrap_request',$data); 
                }
                else{
                 log_message('debug','next_pickup_date');
                
                	$data = array(
					        'customer_response'  => 'No Response',
				         	'next_pickup_date'   => $pick_up_date,
				         	'last_pickup_date'   => $next_pickup_date,
                            'time_slot_min'      => $pick_up_min_time,
                            'time_slot_max'      => $pick_up_max_time
				            );
                    $this->db->where('id', $value);
					$this->db->update('gre_scrap_request',$data); 
                }
		    }
		}	      
	}
    public function periodic_request_archive($scrapids){
        //for moving data into Periodic Request Archive Table
	 	foreach ($scrapids as $scrap_id){
	 	 	$this->db->select('id,periodic_interval,vendor_id,reward_point,scrap_weight,customer_presence,scrap_details,user_id,last_pickup_date,first_pickup_date,customer_response,not_picked_reason,list_created_date,list_created_group,apartment_otp,prefered_pickup_day,time_slot_min,time_slot_max');
            $this->db->where('id',$scrap_id);
	        $this->db->where('request_type','Periodic');
			$query = $this->db->get('gre_scrap_request');
			foreach ($query->result() as $value){
				$first_pickup_date = $value->first_pickup_date;
				$first_pickup_date = date('Y-m-d', strtotime($first_pickup_date));
					if (($first_pickup_date =='1970-01-01') || ($first_pickup_date =='-0001-11-30')) {log_message('debug','sadffds1');
			 	 		continue;
			 	 	}
			 	else{
			 		$customer_response = $value->customer_response; 
			 		if(empty($value->customer_response))
			 			$customer_response = "";
			 		$data1 = array(
        		         	'scrap_request_id'   => $value->id,
        		         	'periodic_interval'  => $value->periodic_interval,
        		         	'vendor_id'          => $value->vendor_id,
        		         	'reward_point'       => $value->reward_point,
        		         	'scrap_weight'       => $value->scrap_weight,
        		         	'customer_presence'  => $value->customer_presence,
        		         	'scrap_details'      => $value->scrap_details,
        		         	'user_id'            => $value->user_id,
        		         	'picked_up_date'     => $value->last_pickup_date,
        		         	'customer_response'  => $customer_response,
        		         	'not_picked_reason'  => $value->not_picked_reason,
        		         	'list_created_date'  => $value->list_created_date,
        		         	'list_created_group' => $value->list_created_group,
                            'archive_date'       => date('Y-m-d  H:i:s'),
                            'apartment_otp'      => $value->apartment_otp,
                            'prefered_pickup_day'=> $value->prefered_pickup_day,
                            'time_slot_min'      => $value->time_slot_min,
                            'time_slot_max'      => $value->time_slot_max
        		        );	
        		    $this->db->insert('gre_periodic_request_archive',$data1);
		        }
		    }
      		$result = 1;//User is Available
	 	 	return $result;
   		}
    }
	public function blocked_customer($block_customer_ids,$status){
	    if($status == 'block')
    	    $data = array(
    		         	'blocked_customer' => 1
    		            );
        else  
            $data = array(
    		         	'blocked_customer' => 0
    		            );
		foreach ($block_customer_ids as $value){
			$this->db->where('id', $value);
  			$this->db->update('users',$data); 
		}	      
	}
	
	public function validate_number($number){
	   
		$this->db->select('id');
		$this->db->where('phone',$number);
		$query   = $this->db->get('gre_vendors');
	    $user_id = $query->row()->id;
	    if(!empty($user_id)){//log_message('debug',"rahul");
	    	$result = 1;
	    	return $result;
	    }
	    else{
	    	$result = 2;
            return $result;
	    }
		
	}
	 public function check_mobile_unique($contact_number){
            $query=$this->db->query("SELECT otp FROM gre_mobile_verification WHERE phone='{$contact_number}' ORDER BY id DESC LIMIT 1");
			$resultRow = $query->row();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow->otp;
				return $response;
			}
			else{
			    return "0";
			}
    }
   
		
	public function user_details_data($id){
           
	        $query=$this->db->query("SELECT * FROM gre_vendors WHERE id=".$id);
			$resultRow = $query->row();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				
			}
		
		return $response;
					
				
	}	
	
		public function check_mobile_number($mobile){
           
	        $query=$this->db->query("SELECT id FROM gre_vendors WHERE mobile=".$mobile);
			$resultRow = $query->row();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow->id;
				return $response;
			}
			else{
			    $response="0";
			    return $response;
			}
		
	
				
	}	
	
	 public function new_register_otp($rand_num,$contact_number){
	   
      $data=array(
			    "phone"	=>$contact_number,
				"otp"	=>$rand_num,
				"otp_expire_time"		=>date('Y-m-d H:i:s',(time() + (5 * 60)))
			
			);
		
					$this->db->insert('gre_mobile_verification',$data);
					$id=$this->db->insert_id();
					 
					if($id)	{			
					
					    return "1";
					}
					else{
					    return "2";
					}
					
				
	}	
	
	 public function new_vendor_registration($contact_number){
	   
   
			        $data=array(
			            "mobile"	=>$contact_number,
			            "email"  =>$contact_number."@gmail.com",
			        	"created_date"		=>date('Y-m-d H:i:s')
			
		            	);

					$this->db->insert('gre_vendors',$data);
					$id=$this->db->insert_id();
					 
					if($id)	{			
					
					    return $id;
					}
					else{
					    return "0";
					}
					
				
	}	
	
	public function scrap_details($id,$request_status){
	   
            $response=array();
	    	$query=$this->db->query("SELECT * FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.request_status='".$request_status."' AND gre_scrap_request.vendor_response='yes' ORDER BY gre_scrap_request.id DESC");
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
		public function reject_group_count($id){
	   
            $response=array();
	    	$query=$this->db->query("SELECT count(gre_address.id) as reject_group_count FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id=".$id." AND gre_scrap_request.request_status='Not Pick' AND gre_scrap_request.vendor_response='yes'");
			$resultRow = $query->row();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
	/*	public function scrap_details($id,$request_status){
	   
            $response=array();
	    	$query=$this->db->query("SELECT *,COUNT(gre_address.id) AS group_count,GROUP_CONCAT(gre_scrap_request.id) AS selest_scap_id FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.request_status='".$request_status."' AND gre_scrap_request.vendor_response='yes'  GROUP BY gre_address.id");
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}	*/
	
	/*	public function scrap_details_all($id){
	   
            $response=array();
	    	$query=$this->db->query("SELECT *,COUNT(gre_address.id) AS group_count,GROUP_CONCAT(gre_scrap_request.id) AS selest_scap_id  FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' GROUP BY gre_address.id");
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}	*/
	
		public function scrap_details_all($id){
	   
            $response=array();
	    	$query=$this->db->query("SELECT * FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' AND (gre_scrap_request.request_status='Complete' OR gre_scrap_request.request_status='Not Pick') ORDER BY gre_scrap_request.id DESC");
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}	
	
		public function count_details_all($id){
	   // $now=date('Y-m-d');
            $response=array();
	    	$query=$this->db->query("SELECT count(gre_address.id) AS all_group_count FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' AND (gre_scrap_request.request_status='Complete' OR gre_scrap_request.request_status='Not Pick')");
			
		
			$resultRow = $query->row();
			
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}	
	
		public function scrap_details_new($id){
	        $now=date('Y-m-d');
            $response=array();
	    	$query=$this->db->query("SELECT *,COUNT(gre_address.id) AS group_count,GROUP_CONCAT(gre_scrap_request.id) AS selest_scap_id FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' and gre_scrap_request.prefered_pickup_date>='".$now."' and gre_scrap_request.vendor_response='' GROUP BY gre_address.id");
	    	
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
	public function scrap_details_pick_up_city($id){
	        $now=date('Y-m-d H:i:s');
            $response=array();
	    	$query=$this->db->query("SELECT *,COUNT(gre_address.id) AS group_count,GROUP_CONCAT(gre_address.id) AS select_scap_id FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' AND gre_scrap_request.request_status='process' and gre_scrap_request.prefered_pickup_date>='".$now."' GROUP BY gre_address.locality");
	    	
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
		public function scrap_details_pick_up_city_count($id){
	        $now=date('Y-m-d H:i:s');
            $response=array();
	    	$query=$this->db->query("SELECT COUNT(gre_address.id) AS new_group_count FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' AND gre_scrap_request.request_status='process' and gre_scrap_request.prefered_pickup_date>='".$now."'");
	    	
			$resultRow = $query->row();
			$resultCount=$query->num_rows();
			if($resultCount>0)			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
	
	
	public function scrap_details_pick_up_apartment($id,$city_name){
	        $now=date('Y-m-d H:i:s');
            $response=array();
	    	$query=$this->db->query("SELECT *,COUNT(gre_address.id) AS group_count,GROUP_CONCAT(gre_address.id) AS select_scap_id FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' AND gre_scrap_request.request_status='process' AND gre_address.locality='".$city_name."' and gre_scrap_request.prefered_pickup_date>='".$now."' GROUP BY gre_address.apartment_name");
	    	
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
	
	public function scrap_details_pick_up_apartment_city_details($id,$city_name,$apartment_name){
	       $now=date('Y-m-d H:i:s');
            $response=array();
	    	$query=$this->db->query("SELECT * FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.vendor_response='yes' AND gre_scrap_request.request_status='process' AND gre_address.locality='".$city_name."' AND gre_address.apartment_name='".$apartment_name."' and gre_scrap_request.prefered_pickup_date>='".$now."'");
	    	
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
			if($resultCount>0)			{
				$response=$resultRow;
				return $response;
			}
			else{
			    return $response;
			}			
				
	}
	
		public function dashboard_accept($scap_id){
		   
             $scap_id_array = explode(",", $scap_id);
             for($i=0;$i<sizeof($scap_id_array);$i++){
           
	                 $data = array(
					        'vendor_response'  => 'yes',
				         	'request_status'   => 'process'
				            );
                    $this->db->where('id', $scap_id_array[$i]);
					//$this->db->update('gre_scrap_request',$data); 
				$this->db->update('gre_scrap_request',$data);
             
					
             }
             $response['success']='1'; 
		     $response['message']="Scap Accept successfull";
		     $pdata1=$response;
		     echo json_encode($pdata1);
						
				
	}
	
	
	public function apartment_assign_details($id){		   
            			
              $response=array();
	    	$query=$this->db->query("SELECT gre_scrap_request.*,gre_address.*,users.first_name,users.last_name FROM `gre_scrap_request`,`gre_address`,`users` WHERE vendor_id='".$id."' AND vendor_response='yes' AND request_status='process' AND gre_address.id=gre_scrap_request.apartment_id AND users.id=gre_address.user_id ORDER BY gre_scrap_request.id DESC");
	    	
			$resultRow = $query->result();
			$resultCount=$query->num_rows();
		
			if($resultCount>0)			{
			//	$response=$resultRow;
				
				return $resultRow;
			}
			else{
			    return $response;
			}
						
				
	}
	
	
		public function dashboard_cancel($scap_id){
		   
             $scap_id_array = explode(",", $scap_id);
             for($i=0;$i<sizeof($scap_id_array);$i++){
           
	                 $data = array(
					        'vendor_response'  => 'no',
				         	'request_status'   => 'Canceled'
				            );
                    $this->db->where('id', $scap_id_array[$i]);
					//$this->db->update('gre_scrap_request',$data); 
				$this->db->update('gre_scrap_request',$data);
             
					
             }
             $response['success']='1'; 
		     $response['message']="Scap cancel successfull";
		     $pdata1=$response;
		     echo json_encode($pdata1);
						
				
	}
	
	public function scap_reject($id){
           
	    	$query=$this->db->query("SELECT * FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.request_status='Canceled'");
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				return $resultCount;
			}
			else{
			    return $resultCount;
			}			
				
	}	
	public function scap_pickup($id){
           
	    	$query=$this->db->query("SELECT * FROM `gre_address` INNER JOIN `gre_scrap_request` ON gre_address.id=gre_scrap_request.apartment_id WHERE gre_scrap_request.vendor_id='".$id."' AND gre_scrap_request.request_status='Complete'");
			$resultCount=$query->num_rows();
			if($resultCount>0)
			{
				return $resultCount;
			}
			else{
			    return $resultCount;
			}			
				
	}	
	
	public function	scap_request_reject($id,$description){
	    
           	$data = array(
					        'request_status'  => 'Not Pick',
				         	'not_picked_reason'   => $description
				            );
                    $this->db->where('id', $id);
					//$this->db->update('gre_scrap_request',$data); 
					
					if($this->db->update('gre_scrap_request',$data)){
					    return "1";
					}
					else{
					    return "0";
					}
	}
	
	public function scap_request_update($scap_id,$price,$description,$user_id){
           
	                $data = array(
					        'id'  => $scap_id,
				         	'reward_point'   => $price,
				         	'scrap_details'   => $description,
				         	'request_status'   => "Complete",
				            );
                    $this->db->where('id', $scap_id);
					if($this->db->update('gre_scrap_request',$data)){
					 //  echo "SELECT request_type FROM `gre_scrap_request` where id='$scap_id'";
					    $query=$this->db->query("SELECT request_type,periodic_interval,next_pickup_date,user_id FROM `gre_scrap_request` where id='$scap_id'");
            			$result=$query->row();
            			$resultCount=$query->num_rows();
            			if($resultCount>0)
            			{
            			    $final_user_id=$result->user_id;
            			    $total_reward_point_details=$this->db->query("SELECT total_reward_point FROM `users` where id='$final_user_id'");
            			    $total_reward_point_details_=$total_reward_point_details->row();
            		    	$total=$total_reward_point_details_->total_reward_point+$price;
            			    $total_reward = array(
    				         	'total_reward_point'   => $total
				            );
                         $this->db->where('id', $final_user_id);
					     $this->db->update('users',$total_reward);
					    
					     $reward_point_data=array(
    			            "user_id"	=>$final_user_id,
    			            "scrap_request_id"  =>$scap_id,
    			        	"credited_by"		=>"Vendor",
    			        	"credit_reward_point"		=>$price
    			
    		            	);

					$this->db->insert('gre_reward_point',$reward_point_data);
					$this->db->insert_id();
					if($result->request_type=="Periodic"){
					    $interval=$result->periodic_interval;
					    $date = new DateTime($result->next_pickup_date);
                        $date->add(new DateInterval('P'.$interval.'D'));
					     $next_date = array(
    				         	'next_pickup_date'   => $date->format('Y-m-d H:i:s')
				            );
                         $this->db->where('id', $scap_id);
					    if($this->db->update('gre_scrap_request',$next_date)){
					        
					        return "1";
    					}
    					else{
    					    return "0";
    					}
					    
            		}
            		else{
            			
            			return "1";  
            		}
					   
				}
				else{
					   
					   return "0";
				}
			}
			else{
			    return "0";
			}
				
	}
}
	
	
	
	
