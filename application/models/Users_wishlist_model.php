<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_wishlist_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->config('app');
		$this->app = $this->config->item('app');
	}

	public function menu($limit = NULL, $random = FALSE){
		if ($limit) $this->db->limit($limit);
		
		if ($random) $this->db->order_by('id', 'RANDOM');

		$this->db->select('id, name, slug');
		$this->db->where('parent_id', NULL);
		return $this->db->get('gre_wishlist_categories')->result();
	}
	function get_wishlist_category($product_id){
        $this->db->select('*');
	    $query = $this->db->get_where('gre_wishlist_category', array('product_id' => $product_id));
	    $result = $query->row();
	    return $result;
	}
	function get_user_wishlist($user_id){
		$this->db->order_by("id","desc");
        $this->db->select('*');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_wishlist_list');
	    $results = $query->result();
	    return $results;
	}
	function get_wishlist_categories(){
		$this->db->order_by("name","asc");
        $this->db->select('*');
	    $query = $this->db->get('gre_wishlist_categories');
	    $results = $query->result();
	    return $results;
	}
	function add_user_wishlist($product_details){
    	$status = $this->db->insert('gre_wishlist_list',$product_details);
    	$insertId = $this->db->insert_id();
    	return $insertId;
	}
	function get_wishlist_details($product_id){
        $this->db->select('*');
        $this->db->where('id',$product_id);
	    $query = $this->db->get('gre_wishlist_list');
	    $result = $query->row();
	    return $result;
	}
	function edit_user_wishlist($product_id, $product_details){
		$this->db->where('id', $product_id);
    	$status = $this->db->update('gre_wishlist_list',$product_details);
    	return $status;
    	//log_message('debug',$this->db->last_query());
	}
	function get_wishlist_inquiry($product_id){
		$this->db->order_by("created_date","desc");
        $this->db->select('*');
        $this->db->where('product_id',$product_id);
	    $query = $this->db->get('gre_wishlist_inquiry');
	    $results = $query->result();
	    return $results;
	}
	public function get_expired_wishlists(){
		$data = array(
			'expiry_datetime <' => date('Y-m-d'),
			'status' => 1
		);
		$this->db->select('id,item_name,expiry_datetime');
	    $result = $this->db->get_where('gre_wishlist_list',$data)->result();
		return $result;
	}
	function check_email_verified($product_id){
		$this->db->select('user_id');
        $this->db->where('id',$product_id);
	    $user_id = $this->db->get('gre_wishlist_list')->row()->user_id;
	    
	    $this->db->select('email_verified,email,first_name,last_name');
        $this->db->where('id',$user_id);
	    $result = $this->db->get('users')->row();
	    
	    return $result;
	}
	
	private $lineage = array();
	public function get_lineage($category_id = null, $get_data = TRUE)
	{
		// Get the category meta data.
		$category = $this->db->get_where('gre_wishlist_categories', array(
			'id' => $category_id
		))->result();
			
		// If this category has data (exists).
		if ( ! empty($category))
		{
			// Add category to lineage array.
			if ($get_data)
			{
				array_push($this->lineage, $category[0]);
			}
			else
			{
				array_push($this->lineage, $category[0]->id);
			}

			// Repeat this process.
			$this->get_lineage($category[0]->parent_id, $get_data);
		}

		// Reverse the lineage to get proper hierrachy.
		return array_reverse($this->lineage);
	}
	
	// Get categories
	public function get_categories($category_id = null, $verbose = TRUE)
	{
		if ( ! ctype_digit($category_id))
		{
			// When using slug to query. Get id of the category.
			$query = $this->db->get_where('gre_wishlist_categories', array(
				'slug' => $category_id
			))->result();

			if ($query)
			{
				$category_id = $query[0]->id;
			}
		}

		// Get data for category.
		$this->db->where('id', $category_id);
		$meta_data = $this->db->get('gre_wishlist_categories')->result();

		if ( ! $verbose)
		{
			return $meta_data;
		}

		if ( ! empty($meta_data))
		{
			// Sub-categories exist for this category.
			$meta_data = $meta_data[0];
		}
		else
		{
			// Sub-categories do not exist for this category.

			if ( ! $category_id)
			{
				// If this is the base category.
				$meta_data = new stdClass;
			
				// Add category id.
				$meta_data->id 	 = NULL;
				$meta_data->name = NULL;
				$meta_data->slug = NULL;
			}
		}

		if (isset($meta_data->id))
		{
		}
		
		// Add category pagination.
		$this->lineage = array();
		$meta_data->pagination = $this->get_lineage($category_id, TRUE);

		return $meta_data;
	}
	
	
	/**
	 * settings
	 *
	 * @access	public
	 * @return Array
	 **/
	public function settings($value = '')
	{
		$this->load->database();
		$result = $this->db->get('cart_config')->result();
		if ($result)
		{
			switch ($value) {
				case 'upload_limit':
					return $result[0]->config_user_files_limit;
					break;
				case 'pagination_limit':
					return $result[0]->config_pagination_limit;
					break;
				default:
					return $result[0]->config_user_files_limit;
					break;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function wishlists($product_options = array()){
		$this->load->model(array('product_model', 'category_model'));
		
		$page_limit = (isset($product_options['limit'])) ? $product_options['limit'] : $this->settings('pagination_limit');

		// Define options to query doctors.

		$product_options['limit'] = $page_limit;

		// Define other options
		// if ($this->input->get('order')) $product_options['order'] = $this->input->get('order');

		if ($this->input->get('cate')) $product_options['category_id'] = $this->input->get('cate');

		// if ($this->input->get('price')) $product_options['price_range'] = $this->input->get('price');

		// if ($this->input->get('seller')) $product_options['seller'] = $this->input->get('seller');

		// if ($this->input->post('price_range'))
		// {
		// 	// Update query URI string and redirect to back here.

		// 	// Remove lowest price or highest price options. They conflict with price range.
		// 	if ($this->input->get('order') !== 'lowpx')
		// 	{
		// 		$query_uri = current_url().'?'.preg_replace('/(^|&)order=[^&]*/', '', $_SERVER['QUERY_STRING']);
		// 	}
		// 	elseif ($this->input->get('order') !== 'hghpx')
		// 	{
		// 		$query_uri = current_url().'?'.preg_replace('/(^|&)order=[^&]*/', '', $_SERVER['QUERY_STRING']);
		// 	}

		// 	redirect($query_uri.'&price='.$this->input->post('price'), 'refresh');
		// }

		// if ($this->input->post('p_search'))
		// {
		// 	redirect(current_url().($_SERVER['QUERY_STRING'].($_SERVER['QUERY_STRING'] ? '&p_query=' : '?p_query=').$this->input->post('p_query')));
		// }
		
		// if ($this->input->get('p_query')) $product_options['search'] = $this->input->get('p_query');

		// if ($page_num = $this->input->get('per_page'))
		// {
		// 	$product_options['start'] = ( ($page_num*$product_options['limit'])-$product_options['limit'] ) + 1;
		// }
		// else
		// {
		// 	$product_options['start'] = 0;
		// }

		$products_meta = $this->get_products($product_options);
		$products = $products_meta->products;
		$result_total = $products_meta->count;

		// Add some pagination to this page.
		$this->load->library('pagination');

		// Construct Url
		$page_url = $this->input->get('per_page') ? preg_replace('/(^|&)per_page=[^&]*/', '', $_SERVER['QUERY_STRING']) : $_SERVER['QUERY_STRING'];

		$config['base_url'] = current_url().'?'.$page_url;
		$config['page_query_string'] = TRUE;
		$config['per_page'] = $page_limit;
		$config['total_rows'] = $result_total;
		$config['num_links']  = 4;
		$config['use_page_numbers']  = TRUE;
		$config['full_tag_open']  = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open']   = '<li>';
		$config['num_tag_close']  = '</li>';
		$config['cur_tag_open']   = '<li class="active"><a>';
		$config['cur_tag_close']  = '</a></li>';
		$config['prev_tag_open']  = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] 	  = 'prev';
		$config['next_link'] 	  = 'next';
		$config['next_tag_open']  = '<li class="next">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open']  = '<li class="last">';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		return array(
			'rows' => $products,
			'total' => $result_total,
			'pagination' => $this->pagination->create_links(),
		);
	}
	public function get_products($options = array())
	{
		$result = new stdClass;

		// Re-Initialize global variable.
		// Or else results of a previous call will collide
		$this->related = array();

		$IDs = array();

		// Array to store product IDs.
		$product_ids = array();

		if (isset($options['category_id']))
		{
			$this->db->select('gre_wishlist_categories.id')->limit(1);
			$query = $this->db->get_where('gre_wishlist_categories', array(
				'slug' => $options['category_id']
			))->result();
			
			if ( ! empty($query)) $options['category_id'] = $query[0]->id;
			
			// Create a pool of category IDs under this category.
			$category_ids_pool = $this->related_ids($options['category_id']);
		}

		// We will need to cache (remember) our where clauses.
		// This allows us to get the result row count and the result object array
		// without reapeating the same query building process
		$this->db->start_cache();

		// if (isset($options['attribute']))
		// {
		// 	// Adding clause for category.
		// 	// The where clause accepts an array of category IDs because a category may have
		// 	// multiple sub-categories which may also have other sub-categories and so on.
		// 	$this->db->where_in('product_attributes.attribute_description_id', $options['attribute']);
		// 	$this->db->join('product_attributes', 'product_attributes.product_id = gre_my_product_list.id');
		// }

		$this->db->select('gre_wishlist_list.id, gre_wishlist_list.user_id,gre_wishlist_list.item_name, gre_wishlist_list.slug, gre_wishlist_list.description, gre_wishlist_list.quantity');
		$this->db->where('status',1);
		$this->db->from('gre_wishlist_list');

		/**
		* JOINS
		*/
		// Select the product category id and name.
		$this->db->select('inner_1.category_id, inner_1.category');

		// Do a double inner join.
		// inner 2 (inner join) gets the category name as "category" based on the category id
		// inner 1 (outer join) 'returns' data from inner join based on the product id.
		$this->db->join(
		'(
			SELECT category, product_id, category_id FROM gre_wishlist_category
			LEFT JOIN
			(
				SELECT gre_wishlist_categories.id, gre_wishlist_categories.name AS category  
				FROM gre_wishlist_categories
			) inner_2
			ON
			gre_wishlist_category.category_id = inner_2.id
		) inner_1',
		'inner_1.product_id = gre_wishlist_list.id', 'left');

		// Add the stock quantity
		// $this->db->select('item_stock.stock_quantity AS quantity');
		// $this->db->join('item_stock', 'item_stock.stock_item_fk = gre_my_product_list.id', 'left');

		// Query products at random
		// if (isset($options['random'])) $this->db->order_by('id', 'RANDOM');
		
		if (isset($options['order']))
		{
			if ($options['order'] === 'latest')
			{
				// Query products at random
				$this->db->order_by('gre_wishlist_list.id', 'DESC');
			}
			elseif ($options['order'] === 'lowpx')
			{
				// Query products at random
				$this->db->order_by('gre_wishlist_list.estimated_price', 'ASC');
			}
			elseif ($options['order'] === 'hghpx')
			{
				// Query products at random
				$this->db->order_by('gre_wishlist_list.estimated_price', 'DESC');
			}
			elseif ($options['order'] === 'alpha')
			{
				// Query products at random
				$this->db->order_by('gre_wishlist_list.item_name', 'ASC');
			}
		}

		// Adding clause for seller.
		if (isset($options['seller'])) $this->db->where_in('company_id', $options['seller']);

		// Category Inheritance: The where in clause accepts an array of IDs
		// that includes the category ID and IDs of the category's parents.
		if (isset($options['category_id']))
		{
			// Adding clause for category.
			// The where clause accepts an array of category IDs because a category may have
			// multiple sub-categories which may also have other sub-categories and so on.
			$this->db->where_in('gre_wishlist_category.category_id', $category_ids_pool);
			$this->db->join('gre_wishlist_category', 'gre_wishlist_category.product_id = gre_wishlist_list.id');
		}

		// Adding clause for price range.
		//if (isset($options['price_range'])) $this->db->where('gre_wishlist_list.estimated_price <=', $options['price_range']);

		// Exclude a specific product.
		//if (isset($options['exclude'])) $this->db->where('gre_wishlist_list.id !=', $options['exclude']);

		// Search by keywords
		//if (isset($options['search']))
		// {
		// 	$this->db->like('gre_wishlist_list.item_name', $options['search']);
		// 	$this->db->or_like('gre_wishlist_list.estimated_price', $options['search']);
		// }


		// for sorting wishlist, In case user is deleted by admin but his wishlist is not deleted. 
		$this->db->join('users', 'users.id = gre_wishlist_list.user_id');

		// Count the items before applying pagination and limits.
		$result->count = $this->db->count_all_results();

		// Limit number of results results.
		if (isset($options['limit'])) $this->db->limit($options['limit'], $options['start']);

		$result->products = $this->db->get()->result();
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $result;
	}
	private $related = array();
	public function related_ids($category_id = null, $current = TRUE)
	{
		if ($current)
		{
			// Get the current category meta data.
			$current = $this->db->get_where('gre_wishlist_categories', array(
				'id' => $category_id
			))->result();

			if ($current)
			{
				// Add current category to children array.
				array_push($this->related, $current[0]->id);
			}
		}

		// Get the category meta data.
		$categories = $this->db->get_where('gre_wishlist_categories', array(
			'parent_id' => $category_id
		))->result();

		if ( ! empty($categories))
		{
			// If this category has data (exists).
			foreach ($categories as $key => $category)
			{
				// Add categories to children array.
				array_push($this->related, $category->id);
			}
			// Repeat this process using a parent id.
			$this->related_ids($categories[0]->id, FALSE);
		}

		return $this->related;
	}
}