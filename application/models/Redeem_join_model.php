<?php
class Redeem_join_model extends grocery_CRUD_Model  {
 
    function get_list()
    {
    	if($this->table_name === null)
    		return false;
    		
    	if($this->join){
			list($related_table1, $related_table2, $related_table3, $related_table4) = $this->join;
        	$select = "`{$this->table_name}`.*,$related_table1.first_name, $related_table1.last_name, $related_table1.id as customer_id, $related_table1.phone, $related_table1.email";	
		}else{
			$select = "`{$this->table_name}`.*";
		}
         if(!empty($this->relation))
         {
              foreach($this->relation as $relation)
              {
                   list($field_name , $related_table , $related_field_title) = $relation;
                   $unique_join_name = $this->_unique_join_name($field_name);
                   $unique_field_name = $this->_unique_field_name($field_name);
                  
                    if(strstr($related_field_title,'{'))
                    {	
                    	$related_field_title = str_replace(" ","&nbsp;",$related_field_title);
                        $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\'",$related_field_title))."') as $unique_field_name";
                    }
                    else
                    {  
                        $select .= ", $unique_join_name.$related_field_title as $unique_field_name";
                    }
                      
                   if($this->field_exists($related_field_title))
                   {
                        $select .= ", {$this->table_name}.$related_field_title as '{$this->table_name}.$related_field_title'";
                   }
              }
          }
         
         //set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
    	if(!empty($this->relation_n_n))
    	{
			$select = $this->relation_n_n_queries($select);
    	}
         
         if($this->join){
         	$this->join = array();
         }
         
         $this->db->select($select, false);
         $results = $this->db->get($this->table_name)->result();
         log_message('debug', 'Query - '.$this->db->last_query());
         return $results;
    }

}