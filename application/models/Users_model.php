<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->config('app');
		$this->app = $this->config->item('app');
	}
	public function common_sms_api($contact_number,$message){
      /*  $ch  = curl_init();
        $sms = curl_escape($ch, $message);
        $url = "http://sms1.fameitsolutions.in/index.php/smsapi/httpapi/?uname=naresh12&password=123456&sender=GRNREE&receiver=".$contact_number."&route=TA&msgtype=1&sms=".$sms;
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($status == 200){*/
        $sms_status = 1;	log_message('debug','sms_status correct');
       /* }
        else{
            $sms_status = 2;log_message('debug','sms_status incorrect');
        }
        curl_close($ch);*/
        return $sms_status;
	}    
	public function scrap_order_check($user_id){
		$this->db->select('id,periodic_request_status');
        $this->db->where('user_id',$user_id);
        $this->db->where('request_type','Periodic');
        $this->db->order_by('requested_date','desc');
        $this->db->limit(1);
	 	$query1 = $this->db->get('gre_scrap_request');
	 	$this->db->select('id,request_status,requested_date');
        $this->db->where('user_id',$user_id);
        $this->db->where('request_type','Non-Periodic');
        $this->db->order_by('requested_date','desc');
        $this->db->limit(1);
	 	$query2 = $this->db->get('gre_scrap_request');
	 	if ($query2->row()->request_status == 'Process') {
        	$results = 1;
        	if ($query1->row()->periodic_request_status) {
        		$results = 4;
        		return $results;
        	}
        	return $results;
        }
        elseif ($query2->row()->request_status == 'Placed') {
        	$results = 2;
        	if ($query1->row()->periodic_request_status) {
        		$results = 5;
        		return $results;
        	}
        	return $results;
        }
        elseif ($query2->row()->request_status == 'Not Picked') {
        	$results = 3;
        	if ($query1->row()->periodic_request_status) {
        		$results = 6;
        		return $results;
        	}
        	return $results;
        }
        elseif ($query1->row()->periodic_request_status) {
        	$results = 7;
        	return $results;
        }
        else{
        	$results = 8;
        	return $results;
        }
	}
	
	public function get_location_id($name){
	    $array = array('loc_name' => $name);
        $this->db->select('loc_id');
        $this->db->from('locations');
        $this->db->where($array);
        $query = $this->db->get();
        $loc_id = $query->row()->loc_id;
        return $loc_id;
	}
	
	public function scrap_order_action($user_id,$request_action,$scrap_id){
		$request_action = (int)$request_action;
		$this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $updated_from = 'system';
            if ($this->agent->is_mobile()) {
               $updated_from = 'mobile';
            }
        }
		$data = array(
		              'periodic_request_status' =>$request_action,
		              'updated_from'            => $updated_from
		             );
		$this->db->where('user_id', $user_id);
		$this->db->where('request_type', 'Periodic');
		$this->db->where('id', $scrap_id);
	    $this->db->update('gre_scrap_request',$data);
	}
	public function scrap_order_action1($user_id,$request_action,$scrap_id){
		$this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $updated_from = 'system';
            if ($this->agent->is_mobile()) {
               $updated_from = 'mobile';
            }
        }
		$data = array(
		                'request_status' =>$request_action,
		                'updated_from'            => $updated_from
		             );
		$this->db->where('user_id', $user_id);
		$this->db->where('request_type', 'Non-Periodic');
		$this->db->where('id', $scrap_id);
	    $this->db->update('gre_scrap_request',$data);
	}
	public function archive_history($scrap_id,$user_id){
		$this->db->select('customer_presence,picked_up_date, scrap_weight, scrap_details, reward_point, not_picked_reason');
        $this->db->where('user_id',$user_id);
        $this->db->where('scrap_request_id',$scrap_id);
        $query = $this->db->get('gre_periodic_request_archive');
        return $query->result();
	}
	public function scrap_order_list1($user_id){
		$this->db->select('id, requested_date, 	next_pickup_date, scrap_weight, scrap_details, reward_point ,not_picked_reason, request_status, scrap_details');
        $this->db->where('user_id',$user_id);
        $this->db->where('request_type','Non-Periodic');
        $query = $this->db->get('gre_scrap_request');
        return $query->result();
	}
	public function scrap_order_list($user_id){
		$this->db->select('id, requested_date, last_pickup_date, next_pickup_date, periodic_request_status, not_picked_reason, request_status');
        $this->db->where('user_id',$user_id);
        $this->db->where('request_type','Periodic');
        $query = $this->db->get('gre_scrap_request');
        return $query->result();
	}public function scrap_nonperiodic_id($user_id){
		$this->db->select('id,customer_presence,scrap_comment,prefered_pickup_day,prefered_pickup_date,prefered_pickup_date_next, request_status');
        $this->db->where('user_id',$user_id);
        $this->db->where('request_type','Non-Periodic');
        $this->db->where('request_status','Process');
        $query = $this->db->get('gre_scrap_request');
        if ($query->result()) {
        	return $query->result();
        }
        else{
        	$this->db->select('id,customer_presence,scrap_comment,prefered_pickup_day,prefered_pickup_date,prefered_pickup_date_next');
	        $this->db->where('user_id',$user_id);
	        $this->db->where('request_type','Non-Periodic');
	        $this->db->where_in('request_status',array('Placed','Under Review'));
	        $query = $this->db->get('gre_scrap_request');
	        return $query->result();
        }
	}
	public function scrap_periodic_id($user_id){
		$this->db->select('id,customer_presence,prefered_pickup_day,prefered_pickup_date,prefered_pickup_date_next');
        $this->db->where('user_id',$user_id);
        $this->db->where('request_type','Periodic');
        $query = $this->db->get('gre_scrap_request');
        return $query->result();
	}
	public function redeem_request_details($user_id){
		$this->db->select('*');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('request_date','desc');
        $query = $this->db->get('gre_redeem_request');
        return $query->result();
	}
	public function reward_history($scrap_id){
		$this->db->select('*');
        $this->db->where('scrap_request_id',$scrap_id);
        $query = $this->db->get('gre_reward_point');
        return $query->result();
	}
	public function scrap_details_view($scrap_id){
		$this->db->select('*');
        $this->db->where('id',$scrap_id);
        $query = $this->db->get('gre_scrap_request');
        return $query->result();
	}	
	public function users_address($user_id){
		$this->db->select('*');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get('gre_address');
        return $query->result();
	}
	public function user_details($user_id){
		$this->db->select('*');
        $this->db->where('id',$user_id);
        $query = $this->db->get('users');
        return $query->result();
	}
	public function get_locations($loc_type, $status){
	    $array = array('loc_type_fk' => $loc_type, 'loc_status' => $status);
        $this->db->select('loc_id,loc_name');
        $this->db->from('locations');
        $this->db->where($array);
        //$this->db->limit(2);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        
	}
	function get_state($country_id){      
        $array = array('loc_type_fk' => 2, 'loc_parent_fk' => $country_id, 'loc_status' => 1);
        $this->db->select('loc_id,loc_name');
        $this->db->from('locations');
        $this->db->where($array);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    function get_city($state_id){
        $array = array('loc_type_fk' => 3, 'loc_parent_fk' => $state_id, 'loc_status' => 1);
        $this->db->select('loc_id,loc_name');
        $this->db->from('locations');
        $this->db->where($array);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    function get_locality($city_id){
        $array = array('loc_type_fk' => 4, 'loc_parent_fk' => $city_id, 'loc_status' => 1);
        $this->db->select('loc_id,loc_name');
        $this->db->from('locations');
        $this->db->where($array);
        $this->db->order_by('loc_name','asc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    function get_apartment($locality_id){
        $array = array('loc_type_fk' => 5, 'loc_parent_fk' => $locality_id, 'loc_status' => 1);
        $this->db->select('loc_id,loc_name');
        $this->db->from('locations');
        $this->db->where($array);
        $this->db->order_by('loc_name','asc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
	function total_reward_point($user_id){
		$this->db->select('total_reward_point');
        $this->db->where('id',$user_id);
        $query1 = $this->db->get('users');
        $total_reward_point = $query1->row()->total_reward_point;
        return $total_reward_point;
        // $this->db->select('name,description,price,weight,quantity');
        // $this->db->where('id',$product_id);
        // $query = $this->db->get('products');
        // $price = $query->row()->price;
        // if ($total_reward_point<$price){
        // 	$result = 1;
        // 	return $result;
        // }
        // //update user table reward point
        // $data = array('total_reward_point' => $total_reward_point-$price );
        // $this->db->where('id',$user_id);
        // $this->db->update('users',$data);
        // //reward point table for debit history
        // $data1 = array('order_id'           => $order_id,
        //                'debit_reward_point' => $price,
        //                'date'               => date('Y-m-d H:i:s')
        //                                       );
        // $this->db->where('user_id',$user_id);
        // $this->db->update('gre_reward_point',$data1);
        // //update order details table
        // $ord_det_order_number_fk;
	}
	
	function get_user_product($user_id){
		$this->db->order_by("id","desc");
        $this->db->select('*');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_my_product_list');
	    $results = $query->result();
	    return $results;
	}
	function check_email_verified($product_id){
		$this->db->select('user_id');
        $this->db->where('id',$product_id);
	    $user_id = $this->db->get('gre_my_product_list')->row()->user_id;
	    
	    $this->db->select('email_verified,email,first_name,last_name');
        $this->db->where('id',$user_id);
	    $result = $this->db->get('users')->row();
	    
	    return $result;
	}
	
	function get_product_inquiry($product_id){
		$this->db->order_by("created_date","desc");
        $this->db->select('*');
        $this->db->where('product_id',$product_id);
	    $query = $this->db->get('gre_my_product_inquiry');
	    $results = $query->result();
	    return $results;
	}
	function get_product_details($product_id){
        $this->db->select('*');
        $this->db->where('id',$product_id);
	    $query = $this->db->get('gre_my_product_list');
	    $result = $query->row();
	    return $result;
	}
	function validate_product_token($token, $product_id){
		$this->db->select('user_id');
        $this->db->where('id',$product_id);
	    $user_id = $this->db->get('gre_my_product_list')->row()->user_id;
	    $this->db->select('email');
        $this->db->where('id',$user_id);
        $email = $this->db->get('users')->row()->email;
        
        if($token == md5($email)){
			return true;	
		}else{
			return false;
		}
	}
	function product_get_user($product_id){
		$this->db->select('user_id');
        $this->db->where('id',$product_id);
	    $user_id = $this->db->get('gre_my_product_list')->row()->user_id;
	    $this->db->select('email');
        $this->db->where('id',$user_id);
        $email = $this->db->get('users')->row()->email;
        return md5($email);
			
	}
	function add_user_product($product_details){
    	$status = $this->db->insert('gre_my_product_list',$product_details);
    	$insertId = $this->db->insert_id();
    	return $insertId;
	}
	function edit_user_product($product_id, $product_details){
		$this->db->where('id', $product_id);
    	$status = $this->db->update('gre_my_product_list',$product_details);
    	return $status;
    	//log_message('debug',$this->db->last_query());
	}
	function get_product_categories(){
		$this->db->order_by("name","asc");
        $this->db->select('*');
	    $query = $this->db->get('gre_my_product_categories');
	    $results = $query->result();
	    return $results;
	}
	function get_product_category($product_id){
        $this->db->select('*');
	    $query = $this->db->get_where('gre_my_product_category', array('product_id' => $product_id));
	    $result = $query->row();
	    return $result;
	}
	
	private $lineage = array();
	public function get_lineage($category_id = null, $get_data = TRUE)
	{
		// Get the category meta data.
		$category = $this->db->get_where('gre_my_product_categories', array(
			'id' => $category_id
		))->result();
			
		// If this category has data (exists).
		if ( ! empty($category))
		{
			// Add category to lineage array.
			if ($get_data)
			{
				array_push($this->lineage, $category[0]);
			}
			else
			{
				array_push($this->lineage, $category[0]->id);
			}

			// Repeat this process.
			$this->get_lineage($category[0]->parent_id, $get_data);
		}

		// Reverse the lineage to get proper hierrachy.
		return array_reverse($this->lineage);
	}
	
	// Get categories
	public function get_categories($category_id = null, $verbose = TRUE)
	{
		if ( ! ctype_digit($category_id))
		{
			// When using slug to query. Get id of the category.
			$query = $this->db->get_where('gre_my_product_categories', array(
				'slug' => $category_id
			))->result();

			if ($query)
			{
				$category_id = $query[0]->id;
			}
		}

		// Get data for category.
		$this->db->where('id', $category_id);
		$meta_data = $this->db->get('gre_my_product_categories')->result();

		if ( ! $verbose)
		{
			return $meta_data;
		}

		if ( ! empty($meta_data))
		{
			// Sub-categories exist for this category.
			$meta_data = $meta_data[0];
		}
		else
		{
			// Sub-categories do not exist for this category.

			if ( ! $category_id)
			{
				// If this is the base category.
				$meta_data = new stdClass;
			
				// Add category id.
				$meta_data->id 	 = NULL;
				$meta_data->name = NULL;
				$meta_data->slug = NULL;
			}
		}

		if (isset($meta_data->id))
		{
		}
		
		// Add category pagination.
		$this->lineage = array();
		$meta_data->pagination = $this->get_lineage($category_id, TRUE);

		return $meta_data;
	}
	
	
	public function get_attributes($category_id = null, $name = false, $inherit = false)
	{
		if ($name)
		{
			$this->db->where('name', $name);
		}

		if ($inherit)
		{
			// Reset value.
			$this->lineage = array();
			$this->db->where_in('category_id', $this->get_lineage($category_id, FALSE));
		}
		else
		{
			$this->db->where('category_id', $category_id);
		}

		$this->db->select('product_category_attributes.*, gre_my_product_categories.name AS category_name');
		$this->db->join('gre_my_product_categories', 'gre_my_product_categories.id = product_category_attributes.category_id');
		$attributes = $this->db->get('gre_product_category_attributes')->result();


		foreach ($attributes as $key => $attribute)
		{
			$attribute->descriptions =  $this->db->get_where(
				'product_category_attribute_descriptions',
				array(
					'attribute_id' => $attribute->id
				)
			)->result();
		}
		return $attributes;
	}
	
	/**
	 * menu
	 *
	 * @access	public
	 * @return object
	 **/
	public function menu($limit = NULL, $random = FALSE)
	{
		if ($limit) $this->db->limit($limit);
		
		if ($random) $this->db->order_by('id', 'RANDOM');

		$this->db->select('id, name, slug');
		$this->db->where('parent_id', NULL);
		return $this->db->get('gre_my_product_categories')->result();
	}
	
	/**
	 * products
	 *
	 * @access	public
	 * @return Array
	 **/
	public function products($product_options = array())
	{
		$this->load->model(array('product_model', 'category_model'));
		
		$page_limit = (isset($product_options['limit'])) ? $product_options['limit'] : $this->settings('pagination_limit');

		// Define options to query doctors.

		$product_options['limit'] = $page_limit;

		// Define other options
		if ($this->input->get('order')) $product_options['order'] = $this->input->get('order');

		if ($this->input->get('cate')) $product_options['category_id'] = $this->input->get('cate');

		if ($this->input->get('price')) $product_options['price_range'] = $this->input->get('price');

		if ($this->input->get('seller')) $product_options['seller'] = $this->input->get('seller');

		if ($this->input->post('price_range'))
		{
			// Update query URI string and redirect to back here.

			// Remove lowest price or highest price options. They conflict with price range.
			if ($this->input->get('order') !== 'lowpx')
			{
				$query_uri = current_url().'?'.preg_replace('/(^|&)order=[^&]*/', '', $_SERVER['QUERY_STRING']);
			}
			elseif ($this->input->get('order') !== 'hghpx')
			{
				$query_uri = current_url().'?'.preg_replace('/(^|&)order=[^&]*/', '', $_SERVER['QUERY_STRING']);
			}

			redirect($query_uri.'&price='.$this->input->post('price'), 'refresh');
		}

		if ($this->input->post('p_search'))
		{
			redirect(current_url().($_SERVER['QUERY_STRING'].($_SERVER['QUERY_STRING'] ? '&p_query=' : '?p_query=').$this->input->post('p_query')));
		}
		
		if ($this->input->get('p_query')) $product_options['search'] = $this->input->get('p_query');

		if ($page_num = $this->input->get('per_page'))
		{
			$product_options['start'] = ( ($page_num*$product_options['limit'])-$product_options['limit'] ) + 1;
		}
		else
		{
			$product_options['start'] = 0;
		}

		$products_meta = $this->get_products($product_options);
		$products = $products_meta->products;
		$result_total = $products_meta->count;
        
		// Add some pagination to this page.
		$this->load->library('pagination');

		// Construct Url
		$page_url = $this->input->get('per_page') ? preg_replace('/(^|&)per_page=[^&]*/', '', $_SERVER['QUERY_STRING']) : $_SERVER['QUERY_STRING'];

		$config['base_url'] = current_url().'?'.$page_url;
		$config['page_query_string'] = TRUE;
		$config['per_page'] = $page_limit;
		$config['total_rows'] = $result_total;
		$config['num_links']  = 4;
		$config['use_page_numbers']  = TRUE;
		$config['full_tag_open']  = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open']   = '<li>';
		$config['num_tag_close']  = '</li>';
		$config['cur_tag_open']   = '<li class="active"><a>';
		$config['cur_tag_close']  = '</a></li>';
		$config['prev_tag_open']  = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] 	  = 'prev';
		$config['next_link'] 	  = 'next';
		$config['next_tag_open']  = '<li class="next">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open']  = '<li class="last">';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		return array(
			'rows' => $products,
			'total' => $result_total,
			'pagination' => $this->pagination->create_links(),
		);
	}
	
	public function min_price($category_id = FALSE)
	{
		$this->db->limit(1);
		$this->db->select_min('estimated_price');
		$this->db->where('gre_my_product_category.category_id', $category_id);
		$this->db->join('gre_my_product_category', 'gre_my_product_category.product_id = gre_my_product_list.id');
		$min = $this->db->get('gre_my_product_list')->result();

		if ($min)
		{
			return $min[0]->estimated_price;
		}
		else
		{
			return FALSE;
		}
	}

	public function max_price($category_id = FALSE)
	{
		$this->db->limit(1);
		$this->db->select_max('estimated_price');
		$this->db->where('gre_my_product_category.category_id', $category_id);
		$this->db->join('gre_my_product_category', 'gre_my_product_category.product_id = gre_my_product_list.id');
		$max = $this->db->get('gre_my_product_list')->result();
		
		if ($max)
		{
			return $max[0]->estimated_price;
		}
		else
		{
			return FALSE;
		}
	}
	
	private $related = array();
	public function related_ids($category_id = null, $current = TRUE)
	{
		if ($current)
		{
			// Get the current category meta data.
			$current = $this->db->get_where('gre_my_product_categories', array(
				'id' => $category_id
			))->result();

			if ($current)
			{
				// Add current category to children array.
				array_push($this->related, $current[0]->id);
			}
		}

		// Get the category meta data.
		$categories = $this->db->get_where('gre_my_product_categories', array(
			'parent_id' => $category_id
		))->result();

		if ( ! empty($categories))
		{
			// If this category has data (exists).
			foreach ($categories as $key => $category)
			{
				// Add categories to children array.
				array_push($this->related, $category->id);
			}
			// Repeat this process using a parent id.
			$this->related_ids($categories[0]->id, FALSE);
		}

		return $this->related;
	}
	
	/**
	 * settings
	 *
	 * @access	public
	 * @return Array
	 **/
	public function settings($value = '')
	{
		$this->load->database();
		$result = $this->db->get('cart_config')->result();
		if ($result)
		{
			switch ($value) {
				case 'upload_limit':
					return $result[0]->config_user_files_limit;
					break;
				case 'pagination_limit':
					return $result[0]->config_pagination_limit;
					break;
				default:
					return $result[0]->config_user_files_limit;
					break;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_expired_products(){
		
		$data = array(
			'expiry_datetime <' => date('Y-m-d'),
			'status' => 1
		);
		
		$this->db->select('id,item_name,expiry_datetime');
		
	    $result = $this->db->get_where('gre_my_product_list',$data)->result();
	    
		return $result;
	}
	
	public function get_products($options = array())
	{
		$result = new stdClass;

		// Re-Initialize global variable.
		// Or else results of a previous call will collide
		$this->related = array();

		$IDs = array();

		// Array to store product IDs.
		$product_ids = array();

		if (isset($options['category_id']))
		{
			$this->db->select('gre_my_product_categories.id')->limit(1);
			$query = $this->db->get_where('gre_my_product_categories', array(
				'slug' => $options['category_id']
			))->result();
			
			if ( ! empty($query)) $options['category_id'] = $query[0]->id;
			
			// Create a pool of category IDs under this category.
			$category_ids_pool = $this->related_ids($options['category_id']);
		}

		// We will need to cache (remember) our where clauses.
		// This allows us to get the result row count and the result object array
		// without reapeating the same query building process
		$this->db->start_cache();

		if (isset($options['attribute']))
		{
			// Adding clause for category.
			// The where clause accepts an array of category IDs because a category may have
			// multiple sub-categories which may also have other sub-categories and so on.
			$this->db->where_in('product_attributes.attribute_description_id', $options['attribute']);
			$this->db->join('product_attributes', 'product_attributes.product_id = gre_my_product_list.id');
		}

		$this->db->select('gre_my_product_list.id, gre_my_product_list.item_name, gre_my_product_list.description, gre_my_product_list.slug, gre_my_product_list.estimated_price, gre_my_product_list.images');
		$this->db->where('status',1);
		$this->db->group_by('slug');
		$this->db->from('gre_my_product_list');

		/**
		* JOINS
		*/
		// Select the product category id and name.
		$this->db->select('inner_1.category_id, inner_1.category');

		// Do a double inner join.
		// inner 2 (inner join) gets the category name as "category" based on the category id
		// inner 1 (outer join) 'returns' data from inner join based on the product id.
		$this->db->join(
		'(
			SELECT category, product_id, category_id FROM gre_my_product_category
			LEFT JOIN
			(
				SELECT gre_my_product_categories.id, gre_my_product_categories.name AS category  
				FROM gre_my_product_categories
			) inner_2
			ON
			gre_my_product_category.category_id = inner_2.id
		) inner_1',
		'inner_1.product_id = gre_my_product_list.id', 'left');

		// Add the stock quantity
		$this->db->select('item_stock.stock_quantity AS quantity');
		$this->db->join('item_stock', 'item_stock.stock_item_fk = gre_my_product_list.id', 'left');

		// Query products at random
		if (isset($options['random'])) $this->db->order_by('id', 'RANDOM');
		
		if (isset($options['order']))
		{
			if ($options['order'] === 'latest')
			{
				// Query products at random
				$this->db->order_by('gre_my_product_list.id', 'DESC');
			}
			elseif ($options['order'] === 'lowpx')
			{
				// Query products at random
				$this->db->order_by('gre_my_product_list.estimated_price', 'ASC');
			}
			elseif ($options['order'] === 'hghpx')
			{
				// Query products at random
				$this->db->order_by('gre_my_product_list.estimated_price', 'DESC');
			}
			elseif ($options['order'] === 'alpha')
			{
				// Query products at random
				$this->db->order_by('gre_my_product_list.item_name', 'ASC');
			}
		}

		// Adding clause for seller.
		if (isset($options['seller'])) $this->db->where_in('company_id', $options['seller']);

		// Category Inheritance: The where in clause accepts an array of IDs
		// that includes the category ID and IDs of the category's parents.
		if (isset($options['category_id']))
		{
			// Adding clause for category.
			// The where clause accepts an array of category IDs because a category may have
			// multiple sub-categories which may also have other sub-categories and so on.
			$this->db->where_in('gre_my_product_category.category_id', $category_ids_pool);
			$this->db->join('gre_my_product_category', 'gre_my_product_category.product_id = gre_my_product_list.id');
		}

		// Adding clause for price range.
		if (isset($options['price_range'])) $this->db->where('gre_my_product_list.estimated_price <=', $options['price_range']);

		// Exclude a specific product.
		if (isset($options['exclude'])) $this->db->where('gre_my_product_list.id !=', $options['exclude']);

		// Search by keywords
		if (isset($options['search']))
		{
			$this->db->like('gre_my_product_list.item_name', $options['search']);
			$this->db->or_like('gre_my_product_list.estimated_price', $options['search']);
		}

		// for sorting product, In case user is deleted by admin but his product is not deleted. 
		$this->db->join('users', 'users.id = gre_my_product_list.user_id');
        
		// Count the items before applying pagination and limits.
		$result->count = $this->db->count_all_results();
		// Limit number of results results.
		if (isset($options['limit'])) $this->db->limit($options['limit'], $options['start']);

		$result->products = $this->db->get()->result();

		$this->db->stop_cache();
		$this->db->flush_cache();
		return $result;
	}
	
	
	function buy_product($user_id,$product_id,$price){
        $this->db->select('total_reward_point');
        $this->db->where('id',$user_id);
	    $query = $this->db->get('users');
	    $total_reward_point = $query->row()->total_reward_point;
	    $total_reward_point = $price;
	    $data = array(
    		    'total_reward_point' => $total_reward_point
    		    );
	    $this->db->where('id', $user_id);
	    $this->db->update('users',$data);
	    $order_id = rand(1000,9999);
    	//updating reward_point table
    	$data1 = array(
    		     'user_id'            => $user_id,
    		     'order_id'           => $order_id,
                 'debit_reward_point' => $price,
                 'date'               => date('Y-m-d')
    		    );
    	$this->db->insert('gre_reward_point',$data1);
	}
	function redeem_request($user_id,$redeem_id){
        $this->db->select('status');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_redeem_request');
	    if($query->num_rows()){
	    	foreach ($query->result() as $value) {
	    		if (empty($value->status)) {
	    			$result = 1;
	    			return $result;
	    		}
	    	}
	    }
    	$total_reward_point;
    	$this->db->select('total_reward_point');
        $this->db->where('id',$user_id);
	    $query              = $this->db->get('users');
	    $total_reward_point = $query->row()->total_reward_point;
	    if ($total_reward_point<1) {
        	$result = 4;//log_message('debug',"total_reward_point");
        	return $result;
	    }
    	
    	$data = array(
    		    'id'				 => $redeem_id,
    		    'user_id'            => $user_id,
    		    'total_reward_point' => $total_reward_point,
                'request_date'       => date('Y-m-d H:i:s')
    		    );
    	$this->db->insert('gre_redeem_request',$data);
    	$result = 2;
    	return $result;
	}
    public function check_blocked_customer($username){
    	$this->db->select('blocked_customer');
        $this->db->where('username',$username);
	    $query = $this->db->get('users');
	    foreach($query->result() as $value){
	        $blocked_customer = $value->blocked_customer;
	        return $blocked_customer;
	    }
    }
    public function update_request($user_id,$customer_presence,$interval,$request_type,$scrap_id,$prefered_pickup_day,$prefered_pickup_date,$scrap_comment){
        //for getting information, user is using which device(syatem or mobile)
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $updated_from = 'system';
            if ($this->agent->is_mobile()) {
               $updated_from = 'mobile';
            }
        }
		$data = array('customer_presence'   => $customer_presence,
					  'periodic_interval'   => $interval,
					  'customer_presence'   => $customer_presence,
					  'prefered_pickup_day' => $prefered_pickup_day,
					  'prefered_pickup_date' => $prefered_pickup_date,
					  'prefered_pickup_date_next' => $prefered_pickup_date,
					  'scrap_comment'		=> $scrap_comment,
                      'updated_from'        => $updated_from
				 );
		$this->db->where('id',$scrap_id);
		$this->db->update('gre_scrap_request',$data);
    }
    public function check_mobile_unique($contact_number){
        $this->db->select('id');
        $this->db->where('phone',$contact_number);
	    $query = $this->db->get('users');
	    if ($query->num_rows()) {
	    	$result = 1; //phone number is already available
	    	return $result;
	    }
	    $result = 2;
	    return $result;
    }
    public function address_check($user_id){
	    $this->db->select('id');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_address');
	    if ($query->num_rows()) {
	    	$var = "yes";
	    	return $var;    	
	    }
	    else{
	    	$var = "no";
            return $var; 
	    }
    }
   
    public function scrap_request($user_id,$customer_presence,$request_type,$interval,$scrap_id,$prefered_pickup_day,$prefered_pickup_date,$scrap_comment){
    	//checking customer previous request is active or not
        $periodic_request_status = true;
	    //for getting address id
	    $apartment_id;
        $this->db->select('id');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_address');
	    $num_row = 1;
	    if ($num_row ==1) {
	    	foreach($query->result() as $value){
		        $apartment_id = $value->id;
	        }
	    }
	    else{
	    	$result = 1;//error: more than one address for one user.
	    	return $result;
	    }
        //for getting information, user is using which device(syatem or mobile)
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $order_from = 'system';
            if ($this->agent->is_mobile()) {
               $order_from = 'mobile';
            }
        }
        if($request_type == "Periodic"){
		 	//for updating scrap request table 
            $this->db->select('id,periodic_request_status');
	        $this->db->where('user_id',$user_id);
	        $this->db->where('request_type',$request_type);
		 	$query = $this->db->get('gre_scrap_request');
		 	$scrap = $query->result();
		 	if ($query->num_rows()) {
		    	foreach($query->result() as $value){
			        $periodic_request_status = $value->periodic_request_status;
			        if ($periodic_request_status) {
			        	$result = 2;//You Already Made A Request As Periodic Customer. So you can't booked new Request;
			        	return $result;
			        }
			        else{
			        	$result = 3;//You Already Made A Request As Periodic Customer And Your Request Is Paused. Please Resume Your Request.;
			        	return $result;
			        }
		        }
		    }
     		else{
      	 		$data3 = array(
      	 				 'id'					     => $scrap_id,
                         'user_id'                   => $user_id,
                         'customer_presence'         => $customer_presence,
                         'request_type'              => $request_type,
                         'request_status'	         => 'Placed',
                         'requested_date'            => date('Y-m-d H:i:s'),
                         'updated_date'              => date('Y-m-d H:i:s'),
                         'apartment_id'              => $apartment_id,
                         'periodic_interval'         => $interval,
                         'periodic_request_status'   => $periodic_request_status,
                         'prefered_pickup_day'       => $prefered_pickup_day,
                         'prefered_pickup_date'      => $prefered_pickup_date,
                         'prefered_pickup_date_next' => $prefered_pickup_date,
                         'scrap_comment'             => $scrap_comment,
                         'order_from'                => $order_from
     		 	);
      	 		$this->db->insert('gre_scrap_request',$data3);
      	 		$result = 4;//Your Order Is Placed Successfully.
      	 		return $result;
      	 	}
	    }
	    else{
	    	$this->db->select('request_status,request_type');
	        $this->db->where('user_id',$user_id);
		    $query = $this->db->get('gre_scrap_request');
		    if ($query->num_rows()) {log_message('debug',"non-periodic");
		    	foreach($query->result() as $value){
			        $request_status = $value->request_status;
			        if (($request_status == 'Under Review' && $value->request_type == "Non-Periodic" ) || ($request_status == 'Placed' && $value->request_type == "Non-Periodic" ) || ($request_status == 'Process' && $value->request_type == "Non-Periodic" ) || ($request_status == 'Not Picked' && $value->request_type == "Non-Periodic" )) {
			        	$result = 5;//Your Previous Request is not completed. So you can't booked new Non-Periodic Request;
			        	log_message('debug',"non-periodic");
			        	return $result;
			        }
		        }
		        if ($result !=5) {
		        	$data3 = array(
		            		'id'				        => $scrap_id,	
		                    'user_id'                   => $user_id,
		                    'customer_presence'         => $customer_presence,
		                    'request_type'              => $request_type,
		                    'requested_date'            => date('Y-m-d H:i:s'),
                         	'updated_date'              => date('Y-m-d H:i:s'),
		                    'apartment_id'              => $apartment_id,
		                    'periodic_interval'         => $interval,
		                    'request_status'		    => 'Placed',
                         	'prefered_pickup_day'       => $prefered_pickup_day,
                         	'prefered_pickup_date'      => $prefered_pickup_date,
                         	'prefered_pickup_date_next' =>$prefered_pickup_date,
                         	'scrap_comment'             => $scrap_comment,
                            'order_from'                => $order_from
		 	             	);
		  		    $this->db->insert('gre_scrap_request',$data3);
		  		    $result = 4;//Your Order Is Placed Successfully.
		 		    return $result;
		        } 
		    }
		    else{
	            $data3 = array(
	            		'id'					 	=> $scrap_id,	
	                    'user_id'                	=> $user_id,
	                    'customer_presence'      	=> $customer_presence,
	                    'request_type'           	=> $request_type,
	                    'requested_date'         	=> date('Y-m-d H:i:s'),
                        'updated_date'              => date('Y-m-d H:i:s'),
	                    'apartment_id'           	=> $apartment_id,
	                    'periodic_interval'      	=> $interval,
	                    'request_status'		 	=> 'Placed',
                     	'prefered_pickup_day'    	=> $prefered_pickup_day,
                     	'prefered_pickup_date'   	=> $prefered_pickup_date,
                        'prefered_pickup_date_next' => $prefered_pickup_date,
                        'scrap_comment'             => $scrap_comment,
                        'order_from'                => $order_from
	 	             	);
	  		    $this->db->insert('gre_scrap_request',$data3);
	  		    $result = 4;//Your Order Is Placed Successfully.
	 		    return $result; 
 		    }
	    }
    }
    
    public function update_address($user_id,$country,$state,$city,$locality,$pin,$apartment,$flat_no,$email,$name){
        $name = (explode(" ",$name));
        $last_name = '';
        for ($i=0; $i < sizeof($name) ; $i++) {
            if ($i<1) {
                $first_name = $name[$i];
            }
            else{
                $last_name = $last_name.' '.$name[$i];
            }
        }
    	 //log_message('debug',$user_id);
	    $this->db->select('id');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_address');
	    if ($query->num_rows()) {
        $data1 = array(
	        'country' => $country,
	        'state'   => $state,
	        'city'    => $city,
	       // 'pin'     => $pin,
	        'locality'          => $locality,
	        'apartment_name'    => $apartment,
	        'flat_no'           => $flat_no,
	        'modified_date'     => date('Y-m-d H:i:s')
	    );
        $this->db->where('user_id', $user_id);
	    $this->db->update('gre_address',$data1);	    	
	    }
	    else{
	    	$data1 = array(
	    	'user_id'  => $user_id,	
	        'country'  => $country,
	        'state'    => $state,
	        'city'     => $city,
	        //'pin'      => $pin,
	        'locality' => $locality,
	        'apartment_name'    => $apartment,
	        'flat_no'           =>$flat_no,
	        'created_date'      => date('Y-m-d H:i:s'),
	        'modified_date'     => date('Y-m-d H:i:s')
	    );
        $this->db->insert('gre_address',$data1);
	    }
	    $email_check_status = 0;
        $query  = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows()){
            $email_check_status =1;
            $query1 = $this->db->get_where('users', array('email' => $email,'id' => $user->id));
            if($query1->num_rows()){
                $email_check_status =2;
            }
        }
        // if($email_check_status != 1){
        //     //log_message('debug','user_id');
        //     $data = array('first_name' => $first_name,
        //                   'last_name'  => $last_name,    
        //                   'email'      => $email,
        //                   'username'   => $email  
        //                  );
        //     $this->db->where('id',$user_id);
        //     $this->db->update('users',$data);
        // }
    }

    public function scrap_request_list($user_id,$customer_presence,$request_type,$interval,$scrap_id,$prefered_pickup_day,$prefered_pickup_date,$scrap_comment){
		
		$periodic_request_status = true;
        //for getting apartment id
        $apartment_id;
        $this->db->select('id');
        $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_address');
	    $num_row = 1;
	    if ($num_row ==1) {
	    	foreach($query->result() as $value){
		        $apartment_id = $value->id;
	        }
	    }
	    else{
	    	$result = 1;//error: more than one address for one user.
	    	return $result;
	    }
	    
        //for getting information, user is using which device(syatem or mobile)
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $order_from = 'system';
            if ($this->agent->is_mobile()) {
               $order_from = 'mobile';
            }
        }
        
	    if($request_type == "Periodic"){log_message('debug',"periodic");
		 	//for updating scrap request table 
            $this->db->select('id,periodic_request_status');
	        $this->db->where('user_id',$user_id);
	        $this->db->where('request_type',$request_type);
		 	$query = $this->db->get('gre_scrap_request');
		 	$scrap = $query->result();
		 	if ($query->num_rows()) {
		    	foreach($query->result() as $value){
			        $periodic_request_status = $value->periodic_request_status;
			        if ($periodic_request_status) {
			        	$result = 2;//You Already Made A Request As Periodic Customer. So you can't booked new Request;
			        	return $result;
			        }
			        else{
			        	$result = 3;//You Already Made A Request As Periodic Customer And Your Request Is Paused. Please Resume Your Request.;
			        	return $result;
			        }
		        }
		    }
     		else{
      	 		$data3 = array(
      	 				 'id'					   => $scrap_id,
                         'user_id'                 => $user_id,
                         'customer_presence'       => $customer_presence,
                         'request_type'            => $request_type,
                         'request_status'	       => 'Placed',
                         'requested_date'          => date('Y-m-d H:i:s'),
                         'updated_date'            => date('Y-m-d H:i:s'),
                         'apartment_id'            => $apartment_id,
                         'periodic_interval'       => $interval,
                         'periodic_request_status' => $periodic_request_status,
                         'prefered_pickup_day'     => $prefered_pickup_day,
                         'prefered_pickup_date'    => $prefered_pickup_date,
                         'prefered_pickup_date_next' => $prefered_pickup_date,
                         'scrap_comment'             => $scrap_comment,
                         'order_from'                => $order_from
     		 	);
      	 		$this->db->insert('gre_scrap_request',$data3);
      	 		$result = 4;//Your Order Is Placed Successfully.
      	 		return $result;
      	 	}
	    }
	    else{
	    	$this->db->select('request_status');
	        $this->db->where('user_id',$user_id);
		    $query = $this->db->get('gre_scrap_request');
		    if ($query->num_rows()) {
		    	foreach($query->result() as $value){
			        $request_status = $value->request_status;
			        if ( ($request_status == 'Under Review') || ($request_status == 'Placed') || ($request_status == 'Process') || ($request_status == 'Not Picked') && $request_type == "Non-Periodic") {
			        	$result = 5;//Your Previous Request is not completed. So you can't booked new Non-Periodic Request;
			        	return $result;
			        }
		        }
		        if ($result !=5) {
		        	$data3 = array(
		            		'id'				      => $scrap_id,	
		                    'user_id'                 => $user_id,
		                    'customer_presence'       => $customer_presence,
		                    'request_type'            => $request_type,
		                    'requested_date'          => date('Y-m-d H:i:s'),
                        	'updated_date'            => date('Y-m-d H:i:s'),
		                    'apartment_id'            => $apartment_id,
		                    'periodic_interval'       => $interval,
		                    'request_status'		  => 'Placed',
                         	'prefered_pickup_day'     => $prefered_pickup_day,
                         	'prefered_pickup_date'    => $prefered_pickup_date,
                         	'prefered_pickup_date_next'=>$prefered_pickup_date,
                         	'scrap_comment'            => $scrap_comment,
                            'order_from'               => $order_from
		 	             	);
		  		    $this->db->insert('gre_scrap_request',$data3);
		  		    $result = 4;//Your Order Is Placed Successfully.
		 		    return $result;
		        } 
		    }
		    else{
	            $data3 = array(
	            		'id'					   => $scrap_id,
	                    'user_id'                  => $user_id,
	                    'customer_presence'        => $customer_presence,
	                    'request_type'             => $request_type,
	                    'requested_date'           => date('Y-m-d H:i:s'),
                        'updated_date'             => date('Y-m-d H:i:s'),
	                    'apartment_id'             => $apartment_id,
	                    'periodic_interval'        => $interval,
	                    'request_status'		   => 'Placed',
                        'prefered_pickup_day'      => $prefered_pickup_day,
                        'prefered_pickup_date'     => $prefered_pickup_date,
                        'prefered_pickup_date_next' => $prefered_pickup_date,
                        'scrap_comment'             => $scrap_comment,
                        'order_from'                => $order_from 
	 	             	);
	  		    $this->db->insert('gre_scrap_request',$data3);
	  		    $result = 4;//Your Order Is Placed Successfully.
	 		    return $result; 
 		    }
	    }
	}	

    public function service_list(){
        $this->db->select('country,id');
		$query = $this->db->get('country');
		return $query;
	}
	public function state_list($country){
		$country_id;
        $this->db->select('id');
        $this->db->where('country',$country);
	    $query = $this->db->get('country');
	    foreach($query->result() as $value){
	        $country_id = $value->id;
	    }
        $this->db->select('state');
        $this->db->where('country_id',$country_id); 
		$query = $this->db->get('state');
		return $query;
	}
	public function city_list($state){
		$state_id;
        $this->db->select('id');
        $this->db->where('state',$state);
	    $query = $this->db->get('state');
	    foreach($query->result() as $value){
	        $state_id = $value->id;
	    }
        $this->db->select('city');
        $this->db->where('state_id',$state_id); 
		$query = $this->db->get('city');
		return $query;
	}
	public function locality_list($city,$state){
		$state_id;
        $this->db->select('id');
        $this->db->where('city',$city);
        $this->db->where('state',$state);
	    $query = $this->db->get('state');
	    foreach($query->result() as $value){
	        $state_id = $value->id;
	    }
        $this->db->select('locality');
        $this->db->where('state_id',$state_id); 
        $this->db->where('city',$city); 
		$query = $this->db->get('locality');
		return $query;
	}
	public function apartment_list($city,$state,$locality,$country){
		$state_id;
        $this->db->select('id');
        $this->db->where('country',$country);
        $this->db->where('city',$city);
        $this->db->where('state',$state);
        $this->db->where('locality',$locality);
	    $query = $this->db->get('state');
	    foreach($query->result() as $value){
	        $state_id = $value->id;
	    }
        $this->db->select('apartment');
        $this->db->where('state_id',$state_id); 
        $this->db->where('city',$city); 
		$query = $this->db->get('apartment');
		return $query;
	}
	
    public function new_register_otp($rand_num,$contact_number){
        log_message('debug',$contact_number);
        $otp_expire_time = date('Y-m-d H:i:s',(time() + (5 * 60)));
        $this->db->select('id');
        $this->db->where('phone',$contact_number);
        $query = $this->db->get('gre_mobile_verification');
        if ($query->num_rows()) {
        	$data = array(
        	    'phone' => $contact_number,
		        'otp' => $rand_num,
		        'otp_expire_time' => $otp_expire_time
			);log_message('debug',$contact_number);
			$this->db->where('phone',$contact_number);
			$this->db->update('gre_mobile_verification',$data);
        }
        else{
        	$data = array(
        	    		'phone' => $contact_number,
		        		'otp' => $rand_num,
		        		'otp_expire_time' => $otp_expire_time
					);log_message('debug',"contact_number");
        $this->db->insert('gre_mobile_verification',$data);
        }
	}	
	
    public function new_otp($rand_num,$contact_number)
	{
        $data = array(
		        'otp' => $rand_num
		);
        $this->db->where('phone', $contact_number);
		$this->db->update('users',$data); 

		$this->db->select('email,first_name');
		$this->db->where('phone',$contact_number); 
		$result = $this->db->get('users');
		return $result;
	}	

	public function validate_number($number){
		$this->db->select('id');
		$this->db->where('phone',$number);
		$query   = $this->db->get('users');
	    $user_id = $query->row()->id;
	    if(!empty($user_id)){//log_message('debug',"rahul");
	    	$result = 1;
	    	return $result;
	    }
	    else{
	    	$result = 2;
            return $result;
	    }
		
	}

	public function verify_otp($number,$user_otp){
		$username;
		$this->db->select('id');
		$this->db->where('phone',$number); 
		$this->db->where('otp',$user_otp);
		$query = $this->db->get('users');
		if ($query->num_rows()) {
			// if ($query->row()->email_verified ==1) {
			// 	$result =1;//Email is verified
			// 	$data = array('otp' =>'' );
			// 	$this->db->where('phone',$number); 
			// 	$this->db->update('users',$data);
			// 	return $result;
			// }
			//else{
				//$result =2;//Email is not verified
				$result = 1;
				return $result;
			//}
		}
	    else{
	    	$result =3;//Incorrect OTP
			return $result;
		}
	}

	public function verify_register_otp($number,$user_otp){
		$time = date('Y-m-d H:i:s');
		$username;
		$this->db->select('otp,otp_expire_time');
		$this->db->where('phone',$number); 
		$this->db->where('otp',$user_otp);
		$query = $this->db->get('gre_mobile_verification');
		log_message('debug','drama');
		log_message('debug',print_r($query->result(),TRUE));
		if ($query->num_rows()) {
			if ($time<$query->row()->otp_expire_time) {
				$results = 1;
				return $results;
			}
		    else{
		    	$results = 2;
		    	return $results;
		    }
		}
		else{
			$results = 3;
		    return $results;
		}
	}
    
    public function verify_email($code,$password){
    	$num  = 1;
    	$data = array(
		        'email_verified' => $num//,
		        //'password'       =>$password
		);
		$this->db->where('email_verification_code', $code);
	    $this->db->update('users',$data);
	}
    

	public function current()
	{
		$users = $this->ion_auth->user()->result();

		if ( ! empty($users))
		{
			$users[0]->type = 'person';
			return $users[0];
		}
	}

	public function owner()
	{
		$this->db->where('name', 'admin');
		$group = $this->db->get('groups')->result();

		// Data needed from users table
		$this->db->select('users.id');
		$this->db->select('users.username');
		$this->db->select('users.email');
		$this->db->select('users.phone');
		$this->db->select('users.address');
		$this->db->select('users.postal');
		$this->db->select('users.avatar AS logo');
		$this->db->select('users.first_name AS name');

		if ( ! empty($group))
		{
			// Only get users of specified group
			$this->db->where('group_id', $group[0]->id);
		}

		$this->db->from('users_groups');
		$this->db->join('users', 'users.id = users_groups.user_id');

		$users = $this->db->get()->result();

		if (! empty($users))
			$users = $users[0];

		return $users;
	}

	public function users($options = array())
	{
		$result = new stdClass();

		// Set option defaults
		$options['start'] = ( isset($options['start']) ) ? $options['start'] : 0;

		// We will need to cache (remember) our where clauses.
		// This allows us to get the result row count and the result object array
		// without reapeating the same query building process
		$this->db->start_cache();

		// Exclude the admin
		$this->db->where('users_groups.group_id !=', 1);
		
		// Unless otherwise defined, get only active user
		if ( ! isset($options['ignore_status'])) $this->db->where_in('users.active', 1);
		
		// Query by ascending or descending order.
		if (isset($options['order']))
		{
			if ($options['order'] === 'asc')
			{
				$this->db->order_by('users.id', 'ACS');
			}
			elseif ($options['order'] === 'desc')
			{
				$this->db->order_by('users.id', 'DESC');
			}
		}
		
		// Extra select fields.
		if (isset($options['select']))
		{
			foreach($options['select'] as $selectField)
			{
				$this->db->select('users.'.$selectField);
			}
		}
		

		// Data needed from users table
		$this->db->select('users.id');
		$this->db->select('users.avatar');
		$this->db->select('users.username');
		$this->db->select('users.email');
		$this->db->select('users.active');

		// Specific user by their ID numbers
		if (isset($options['ids'])) $this->db->where_in('users.id', $options['ids']);
		
		// Exclude user by their ID numbers
		if (isset($options['exclude'])) $this->db->where_not_in('users.id', $options['exclude']);

		if (isset($options['status']))
		{
			if ($options['status'] === 'active')
			{
				$this->db->where_in('users.active', 1);
			}
			elseif ($options['status'] === 'inactive')
			{
				$this->db->where_in('users.active', 0);
			}
		}

		if (isset($options['search']))
		{
			$this->db->like('users.username', $options['search']);
			$this->db->or_like('users.email', $options['search']);
		}

		$this->db->from('users_groups');
		$this->db->join('users', 'users.id = users_groups.user_id');

		// Count the items before applying pagination and limits.
		$result->count = $this->db->count_all_results();

		// Limit number of results results.
		if (isset($options['limit'])) $this->db->limit($options['limit'], $options['start']);

		$result->users = $this->db->get()->result();

		$this->db->stop_cache();
		$this->db->flush_cache();

		return $result;
	}
	
	public function get_products_details($slug)
	{	
		/**
		* JOINS
		*/
		// Add the category
		$this->db->select('gre_my_product_list.id AS id');
		$this->db->select('gre_my_product_list.item_name, gre_my_product_list.description, gre_my_product_list.slug, gre_my_product_list.estimated_price, gre_my_product_list.images, gre_my_product_list.location, gre_my_product_list.quantity');

		// Add the stock quantity
		$this->db->select('users.first_name, users.last_name, users.email, users.email_verified,users.phone');
		$this->db->join('users', 'users.id = gre_my_product_list.user_id');

		// Add the User address
		$this->db->select('gre_address.country, gre_address.state, gre_address.city, gre_address.locality, gre_address.apartment_name, gre_address.pin');
		$this->db->join('gre_address', 'gre_address.user_id = gre_my_product_list.user_id');

		// Add product category id
		$this->db->select('gre_my_product_category.category_id');
		$this->db->join('gre_my_product_category', 'gre_my_product_category.product_id = gre_my_product_list.id');
		
		(ctype_digit($slug)) ? $this->db->where('gre_my_product_list.id', $slug) : $this->db->where('gre_my_product_list.slug', $slug);

		$this->db->from('gre_my_product_list');

		$products = $this->db->get()->result();

		if ($products)
		{
			$product = $products[0];
			
			// Get Category details.
			$this->db->select('id')->limit(1);
			$query = $this->db->get_where('gre_my_product_categories', array(
				'id' => $product->category_id
			))->result();
			if (! empty($query))
				$category = $query[0];
			else
				$category = FALSE;

			// Add category to object.
			$product->category = $category;


			return $product;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_details($user_id)
	{
		// Data needed from users table
		$this->db->select('users.id');
		$this->db->select('users.username');
		$this->db->select('users.email');
		$this->db->select('users.phone');
		$this->db->select('users.address');
		$this->db->select('users.postal');
		$this->db->select('users.active');
		$this->db->select('users.avatar');
		$this->db->select('users.first_name');
		$this->db->select('users.last_name');

		$this->db->where('users.id', $user_id);
		$this->db->from('users');

		$user = $this->db->get()->result();

		if (!empty($user)) $user = $user[0];

		return $user;
	}
	
}