<?php
class Gc_join_model extends grocery_CRUD_Model  {
 
    function get_list()
    {
    	if($this->table_name === null)
    		return false;
    		
    	if($this->join){
			list($related_table1, $related_table2, $related_table3, $related_table4) = $this->join;
        	$select = "`{$this->table_name}`.*,$related_table1.apartment_name, $related_table1.locality, $related_table1.apartment_name, $related_table1.flat_no, $related_table2.first_name, $related_table2.id as customer_id, $related_table2.email,$related_table2.phone,$related_table3.name,$related_table4.list_created_group,gre_scrap_request.id as scrap_id,gre_scrap_request.last_pickup_date as last_pickup_date,gre_scrap_request.next_pickup_date as next_pickup_date,gre_scrap_request.list_created_group as list_created_group1,$related_table4.vendor_response as vendor_response";	
		}else{
			$select = "`{$this->table_name}`.*";
		}
         if(!empty($this->relation))
         {
              foreach($this->relation as $relation)
              {
                   list($field_name , $related_table , $related_field_title) = $relation;
                   $unique_join_name = $this->_unique_join_name($field_name);
                   $unique_field_name = $this->_unique_field_name($field_name);
                  
                    if(strstr($related_field_title,'{'))
                    {	
                    	$related_field_title = str_replace(" ","&nbsp;",$related_field_title);
                        $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\'",$related_field_title))."') as $unique_field_name";
                    }
                    else
                    {  
                        $select .= ", $unique_join_name.$related_field_title as $unique_field_name";
                    }
                      
                   if($this->field_exists($related_field_title))
                   {
                        $select .= ", {$this->table_name}.$related_field_title as '{$this->table_name}.$related_field_title'";
                   }
              }
          }
         
         //set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
    	if(!empty($this->relation_n_n))
    	{
			$select = $this->relation_n_n_queries($select);
    	}
         
         if($this->join){
         	$this->join = array();
         }
         
         $this->db->select($select, false);
         $results = $this->db->get($this->table_name)->result();
         log_message('debug', 'Query - '.$this->db->last_query());
         return $results;
    }

}