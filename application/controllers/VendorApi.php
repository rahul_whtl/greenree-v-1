<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VendorApi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // load Session Library
		$this->load->library('session');
        //load model
		//$this->load->model('api_model');
		// load url helper
		$this->load->database();
        $this->load->helper('url');
        header("Content-type:application/json");
        $timezone_offset_minutes = 330;
		$timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes*60, false);
		date_default_timezone_set($timezone_name);
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->load->model(array(
                    'users_model','Vendor_model_api'
                ));
        // Load required libraries.
        $this->load->library(array(
            'flexi_cart', 'session', 'form_validation', 'ion_auth', 'store'
        ));
		
    }
	public function signup()
	{
	   
	    $id=$this->input->post('id');
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$address=$this->input->post('address');
		$id_proof=$this->input->post('id_proof');
		
		$chk_user=$this->db->query('SELECT * FROM gre_vendors WHERE email="'.$email.'"');
		
		if($name=='')
		{
			$data['success']=0;
			$data['message']='Please Enter name';
			$pdata=$data;
			echo json_encode($pdata);
			
		}/*else if($pass==''){
			$data['success']=0;
			$data['message']='Please Enter password';
			$pdata=$data;
			echo json_encode($pdata);
			
			
		}*/else if($address==''){
			$data['success']=0;
			$data['message']='Please Enter your Address';
			$pdata=$data;
			echo json_encode($pdata);	
			
		}
		else if($id_proof==''){
			$data['success']=0;
			$data['message']='Please upload Govt. Id proof';
			$pdata=$data;
			echo json_encode($pdata);	
			
		}
			
		else if($chk_user->num_rows()>0){
		$data['status']=0;
			$data['message']='Email already exist';
			$pdata=$data;
			echo json_encode($pdata);
			
		}
		else{
		    $random = 'id_proof_'.rand().$id;
            $savefile = @file_put_contents("assets/uploads/vendor/$random.jpg", base64_decode($id_proof));
            $image_root = "assets/uploads/vendor/$random.jpg";
		   
             $update_data = array(
                    'name' => $name,
                    'address' => $address,
                    'email' => $email,
                    'government_id' => $image_root
                );
                
                $this->db->where('id', $id);
                $this->db->update('gre_vendors', $update_data);
                
			$data['success']=1;
			$data['message']='Thank You! Your account has been created successfully';
			$user_details=$this->Vendor_model_api->user_details_data($id);
			$data['user_details']=$user_details;
			$pdata=$data;
			echo json_encode($pdata);
		}
		
	}
public function login()
	{
	
	 
     	$mobile=$this->input->post('mobile');
		if($mobile=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		{
		    
		       $rand_num = rand(1000,9999);
                $message = 'This is your OTP for Login '.$rand_num.'. Please do not share with anyone.';
                $sms_status = $this->users_model->common_sms_api($mobile,$message);
                $sms_status = 1;
                if($sms_status == 1){
                    $res=$this->Vendor_model_api->new_register_otp($rand_num,$mobile);
                    if($res=="1"){
                         $response['success']='1'; 
                         $response['message']="OTP sent to your mobile";
                         $cdata=$response;
                         echo json_encode($cdata);
                    }
                    else{
                        
                         $response1['success']='0'; 
		                 $response1['message']="Some thing went to wrong";
		                 $c1data=$response1;
		                 echo json_encode($c1data);
                    }
              
                }
                else{
                
                   $response1['success']='0'; 
		           $response1['message']="You have enter wrong mobile no.";
		           $c1data=$response1;
			       echo json_encode($c1data);
                    
                }
           
		}
		
	}

function mobile_verification(){ 
            $otp     = @$this->input->post('otp');
            $mobile = $this->input->post('mobile');
            $db_otp   = $this->Vendor_model_api->check_mobile_unique($mobile);
            
            if($otp==$db_otp){
                  $chk_id   = $this->Vendor_model_api->check_mobile_number($mobile);
                  if($chk_id)
                  {
                          $user_details=$this->Vendor_model_api->user_details_data($chk_id);
					      $response1['success']='1'; 
		                  $response1['message']="OTP verify successfull";
		                  $response1['user_details']= $user_details;
		                  $c1data=$response1;
			              echo json_encode($c1data);
                  }
                  else{
                       
					 $id=$this->Vendor_model_api->new_vendor_registration($mobile);
					 $user_details=$this->Vendor_model_api->user_details_data($id);
					if($id)	{			
					       
					      $response1['success']='1'; 
		                  $response1['message']="OTP verify successfull";
		                  $response1['user_details']= $user_details;
		                  $c1data=$response1;
			              echo json_encode($c1data);
					}
					else{
					     $response1['success']='0'; 
		                  $response1['message']="Something went to wrong";
		                  $response1['user_details']= $user_details;
		                  $c1data=$response1;
			              echo json_encode($c1data);
					   
					}
					
                }
				
                
            }
           else{
                   $response1['success']='0'; 
		           $response1['message']="OTP not match";
		           $c1data=$response1;
			       echo json_encode($c1data);
           }
        }
        
 public function vendor_details()
    {
     	$id=$this->input->post('id');
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		{
		     $user_details=$this->Vendor_model_api->user_details_data($id);
		    
			 $response1['success']='1'; 
		     $response1['message']="User Details Found";
		     $response1['user_details']= $user_details;
		     $pdata=$response1;
		     echo json_encode($pdata);
		}
	}
public function vendor_scrap_request()
    {
     	$id=$this->input->post('id');
     	$request_status=$this->input->post('status');
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		    if($request_status=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters status';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
		else
		{
		   //  echo "ss";
		   if($request_status=="all"){
		     $scrap_details=$this->Vendor_model_api->scrap_details_all($id);
		   }
		   else{
		       //$request_status=="Not Pick";
		     $scrap_details=$this->Vendor_model_api->scrap_details($id,$request_status);
		   }
		    if($scrap_details){
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['scrap_details']= $scrap_details;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $scrap_details;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
	
	public function vendor_dashboard()
    {
     	$id=$this->input->post('vendor_id');
     
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
	
		else
		{
		 
		 
		     $scrap_details=$this->Vendor_model_api->scrap_details_new($id);
		  
		    if($scrap_details){
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['scrap_details']= $scrap_details;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $scrap_details;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
	
	public function dashboard_accept()
    {
     	$scap_id=$this->input->post('scap_id');
		if($scap_id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		{
		    $this->Vendor_model_api->dashboard_accept($scap_id);
		   
            
		}
	}
    public function dashboard_cancel()
    {
     	$scap_id=$this->input->post('scap_id');
		if($scap_id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		{
		    $this->Vendor_model_api->dashboard_cancel($scap_id);
		             $description="Rejected Scap Request";
		             $this->email_cancel($description,$scap_id);
		             $mobile="9686854300";
		             $message="Rejected Scap Request ".$scap_id;
		             $this->users_model->common_sms_api($mobile,$message);
		}
	}
	
public function dashboard_details()
    {
     	$id=$this->input->post('id');
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		{
		     $reject=$this->Vendor_model_api->scap_reject($id);
		     $pickup=$this->Vendor_model_api->scap_pickup($id);
		
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['reject']=(String) $reject;
		     $response['pickup']= (String) $pickup;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		   
		}
	}
	public function scap_accept()
    {
     	$scap_id=$this->input->post('scap_id');
     	$price=$this->input->post('price');
     	$description=$this->input->post('description');
     	$user_id=$this->input->post('user_id');
		if($scap_id=='')
		{
			$data['success']=0;
			$data['message']='Scap Id Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else if($price==''){
		    $data['success']=0;
			$data['message']='Price Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else if($description==''){
		    $data['success']=0;
			$data['message']='Description Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else if($user_id==''){
		    $data['success']=0;
			$data['message']='User Id Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		
		else
		{
		     $fun_all=$this->Vendor_model_api->scap_request_update($scap_id,$price,$description,$user_id);
			if($fun_all){
			     $response['success']='1'; 
    		     $response['message']="Scap Accept Successful";
    		     $pdata1=$response;
    		     echo json_encode($pdata1);
			}
			else{
			    $response['success']='0'; 
    		     $response['message']="Scap Not Accept";
    		     $pdata1=$response;
    		     echo json_encode($pdata1);
			}
		
		}
	}
	
public function scap_reject()  
    {
     	$id=$this->input->post('id');
     	$description=$this->input->post('description');
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Id Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else if($description==''){
		    $data['success']=0;
			$data['message']='Description Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		}
		else
		{
		     $value=$this->Vendor_model_api->scap_request_reject($id,$description);
		       if($value=="1"){
		            
        		     $response['success']='1'; 
        		     $response['message']="Reject reason submited successfully";
        		     $pdata1=$response;
        		     echo json_encode($pdata1);
		       }
		       else{
		           $response['success']='0'; 
        		     $response['message']="Reject reason submited Not successfully";
        		     $pdata1=$response;
        		     echo json_encode($pdata1);
		       }
			 
		}
	}
	
	public function support(){
	                  $vendor_id=$this->input->post('vendor_id');
                      $description=$this->input->post('description');
	    
	                  $this->email($description,$vendor_id);
	}
	public function email($message,$user_id){
       $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'localhost',
            'smtp_port' => 25,
            'smtp_auth' => FALSE,
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
             );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            
            //Add file directory if you need to attach a file
          //  $this->email->attach($file_dir_name);
            
            $this->email->from('contact@greenree.com', 'Green REE Support');
            $this->email->to("contact@greenree.com"); 
            
            $this->email->subject('Support Request');
            $this->email->message($message ." From user id ".$user_id); 
            
            if($this->email->send()){
               //Success email Sent
                $this->email->print_debugger();
                     $response['success']='1'; 
        		     $response['message']="Successfully send mail";
        		     $pdata1=$response;
        		     echo json_encode($pdata1);
            }else{
               //Email Failed To Send
               
                $this->email->print_debugger();
                $response['success']='0'; 
        		     $response['message']="Not send mail";
        		     $pdata1=$response;
        		     echo json_encode($pdata1);
            }
         
           
        }
        
public function email_cancel($message,$scap_id){
       $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'localhost',
            'smtp_port' => 25,
            'smtp_auth' => FALSE,
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
             );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            
            //Add file directory if you need to attach a file
          //  $this->email->attach($file_dir_name);
            
            $this->email->from('contact@greenree.com', 'Green REE Support');
            $this->email->to("contact@greenree.com"); 
            
            $this->email->subject('Cancel Request');
            $this->email->message($message ." for Scap Id ".$scap_id); 
            
            if($this->email->send()){
               //Success email Sent
                $this->email->print_debugger();
                     $response['success']='1'; 
        		     $response['message']="Successfully send mail";
        		     $pdata1=$response;
        		     echo json_encode($pdata1);
            }else{
               //Email Failed To Send
                $this->email->print_debugger();
                $response['success']='0'; 
        		     $response['message']="Not send mail";
        		     $pdata1=$response;
        		     echo json_encode($pdata1);
            }
         
           
        }
	public function date(){
	                    $interval="15";
	                     $date = new DateTime("2019-03-30 00:00:00");
                        $date->add(new DateInterval('P'.$interval.'D'));
                        echo $date->format('Y-m-d') ;
	}
	
	
	public function vendor_pickup_by_city()
    {
     	$id=$this->input->post('vendor_id');
     
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
	
		else
		{
		 
		 
		     $scrap_details=$this->Vendor_model_api->scrap_details_pick_up_city($id);
		  
		    if($scrap_details){
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['scrap_details']= $scrap_details;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $scrap_details;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
      
      public function vendor_pickup_by_apartment()
    {
     	$id=$this->input->post('vendor_id');
     	$city_name=$this->input->post('city_name');
     
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
	
		else
		{
		 
		 
		     $scrap_details=$this->Vendor_model_api->scrap_details_pick_up_apartment($id,$city_name);
		  
		    if($scrap_details){
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['scrap_details']= $scrap_details;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $scrap_details;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
	
	  public function vendor_pickup_by_apartment_city_details()
    {
     	$id=$this->input->post('vendor_id');
     	$city_name=$this->input->post('city_name');
     	$apartment_name=$this->input->post('apartment_name');
     
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
	
		else
		{
		 
		 
		     $scrap_details=$this->Vendor_model_api->scrap_details_pick_up_apartment_city_details($id,$city_name,$apartment_name);
		  
		    if($scrap_details){
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['scrap_details']= $scrap_details;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $scrap_details;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
	
	
  public function apartment_assign()
    {
     	$id=$this->input->post('vendor_id');    
     
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
	
		else
		{
		 
		 
		     $apartment_assign=$this->Vendor_model_api->apartment_assign_details($id);
		 
		    if(count($apartment_assign)>0){
		        
			 $response['success']='1'; 
		     $response['message']="Scarp Details Found";
		     $response['scrap_details']= $apartment_assign;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $apartment_assign;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
	
	 public function pick_up_count()
    {
     		$id=$this->input->post('vendor_id');
     
		if($id=='')
		{
			$data['success']=0;
			$data['message']='Required Parameters id';
			$pdata=$data;
			echo json_encode($pdata);
		}
	
	
		else
		{
		 
		 
		     $new_details=$this->Vendor_model_api->scrap_details_pick_up_city_count($id);
		      $all_details=$this->Vendor_model_api->count_details_all($id);
		      //  print_r($all_details);
		      $reject_details=$this->Vendor_model_api->reject_group_count($id);
		  
		    if(count($new_details)>0){
			 $response['success']='1'; 
		     $response['message']="Details Found";
		     $response1['new_count']= $new_details;
		     $response1['all_count']= $all_details;
		     $response1['reject_count']= $reject_details;
		     $response['total_count']=$response1;
		     $pdata1=$response;
		     echo json_encode($pdata1);
		    }
		    else{
		     $response1['success']='0'; 
		     $response1['message']="Scarp Details Not Found";
		     $response1['scrap_details']= $scrap_details;
		     $pdata=$response1;
		     echo json_encode($pdata); 
		    }
		}
	}
      
}

