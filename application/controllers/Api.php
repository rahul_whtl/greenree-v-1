<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        // load Session Library
		$this->load->library('session');
        //load model
		//$this->load->model('api_model');
		// load url helper
		$this->load->database();
        $this->load->helper('url');
        header("Content-type:application/json");
        $timezone_offset_minutes = 330;
		$timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes*60, false);
		date_default_timezone_set($timezone_name);
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

$this->load->model(array(
            'users_model','category_model','product_model','users_wishlist_model'
        ));
        // Load required libraries.
        $this->load->library(array(
            'flexi_cart', 'session', 'form_validation', 'ion_auth', 'store'
        ));
		
    }
	public function signup()
	{
	   
	    $current_date=date("Y-m-d H:i:s");
		$name=$this->input->post('name');
		$pass=$this->input->post('password');
		$address=$this->input->post('address');
		$zip=$this->input->post('zip');
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		$password=md5($pass);
		$chk_user=$this->db->query('SELECT * FROM tbl_member WHERE email="'.$email.'"');
		
		if($name=='')
		{
			$data['status']=0;
			$data['message']='Please Enter name';
			$pdata=$data;
			echo json_encode($pdata);
			
		}else if($pass==''){
			$data['status']=0;
			$data['message']='Please Enter password';
			$pdata=$data;
			echo json_encode($pdata);
			
			
		}else if($address==''){
			$data['status']=0;
			$data['message']='Please Enter your Address';
			$pdata=$data;
			echo json_encode($pdata);	
			
		}else if($zip==''){
			$data['status']=0;
			$data['message']='Please Enter Zip';
			$pdata=$data;
			echo json_encode($pdata);
			
		}else if($email==''){
			$data['status']=0;
			$data['message']='Please Enter Email';
			$pdata=$data;
			echo json_encode($pdata);
			
		}else if($chk_user->num_rows()>0){
			$data['status']=0;
			$data['message']='Email already exist';
			$pdata=$data;
			echo json_encode($pdata);
			
		}else if($phone==''){
			$data['status']=0;
			$data['message']='Please enter Phone';
			$pdata=$data;
			echo json_encode($pdata);
		
		} else{
			 
		
			$this->db->set('name', $name);
			$this->db->set('email', $email);
			$this->db->set('phone', $phone);
			$this->db->set('address', $address);
			$this->db->set('postal_code', $zip);
			$this->db->set('password', md5($pass));
			$this->db->set('add_date', $current_date);
			$this->db->insert('tbl_member');
			$id=$this->db->insert_id();
			$data['status']=1;
			$data['message']='Thank You! Your account has been created successfully';
			$cddata['user_id']=$id;
			$data['response']=$cddata;
			$pdata=$data;
			echo json_encode($pdata);
		}
		
	}
	public function login()
	{
		
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		
		if($email=='' || $password=='')
		{
			$data['status']=0;
			$data['message']='Required Parameters';
			$pdata=$data;
			echo json_encode($pdata);
		} else {
		    
			$chk=$this->db->query('SELECT * FROM users WHERE email="'.$email.'"');
			
			if($chk->num_rows()>0)
			{
			$arr=$chk->result();
			
		
			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), 0, 'username'))
                {
				
				$cdata['status']=1;
				$cdata['message']='Welcome, '.$arr[0]->first_name.' '.$arr[0]->last_name.' !';
				$cdata['response']=$arr;
				$pdata=$cdata;
				echo json_encode($pdata);
			}else{
			    $data['status']=0;
				$data['message']='Invalid password';
				$pdata=$data;
				echo json_encode($pdata);
			}
				
			}else {
				$data['status']=0;
				$data['message']='Invalid email ';
				$pdata=$data;
				echo json_encode($pdata);
			}
		}
	}
	public function user_profile()
	{
	    $id=$this->input->post('user_id');
		
		if($id!='')
		{
			$chk=$this->db->query('SELECT * FROM users WHERE id='.$id);
			if($chk->num_rows()>0)
			{
				$arr_user=$chk->result();
				$pdata['status']=1;
				$pdata['message']='Data available';
				$pdata['response']=$arr_user;
				echo json_encode($pdata);
			}else{
				$data['status']=0;
				$data['message']='Invalid User';
				$pdata=$data;
				echo json_encode($pdata);
			}
		}else{
				$data['status']=0;
				$data['message']='Provide UserID';
				$pdata=$data;
				echo json_encode($pdata);
		}
		
	}
	public function user_update()
	{
	    $datas = json_decode(file_get_contents('php://input'), true);
		$id=$datas["user_id"];
		$name=$datas["name"];
		$email=$datas["email"];
		$phone=$datas["phone"];
		$address=$datas["address"];
		$postal_code=$datas["postal_code"];
		if($id!='')
		{
		    $this->db->set('name', $name);
			$this->db->set('email', $email);
			$this->db->set('phone', $phone);
			$this->db->set('address', $address);
			$this->db->set('postal_code', $postal_code);
			$this->db->where('id',$id);
            $this->db->update('tbl_member');
			$data['status']=1;
			$data['message']='Thank You! Your account has been Updated successfully';
			$cddata['user_id']=$id;
			$data['response']=$cddata;
			$pdata=$data;
			echo json_encode($pdata);
		}else{
				$data['status']=0;
				$data['message']='Provide UserID';
				$pdata=$data;
				echo json_encode($pdata);
		}
		
	}
	public function shop()
	{
	   
			$chk=$this->db->query('SELECT * FROM products order by id desc');
			if($chk->num_rows()>0)
			{
				$arr_user=$chk->result();
				$pdata['status']=1;
				$pdata['message']='Data available';
				$pdata['response']=$arr_user;
				echo json_encode($pdata);
			}else{
				$data['status']=0;
				$data['message']='Data Not Found';
				$pdata=$data;
				echo json_encode($pdata);
			}
		
	}
		public function category()
	{
	   
			$chk=$this->db->query('SELECT * FROM product_categories order by id desc');
			if($chk->num_rows()>0)
			{
				$arr_user=$chk->result();
				$pdata['status']=1;
				$pdata['message']='Data available';
				$pdata['response']=$arr_user;
				echo json_encode($pdata);
			}else{
				$data['status']=0;
				$data['message']='Data Not Found';
				$pdata=$data;
				echo json_encode($pdata);
			}
		
	}
	function pre_owned(){
        
        $categories = $this->users_model->menu();

        foreach ($categories as $item)
        {
            $products = $this->users_model->products(array(
                'category_id' => $item->id
            ));
            $item->products = $products['rows'];
        }
        
        $products = $this->users_model->products(array(
            'order' => 'latest'
        ));
        
        
       $data['products'] = $products['rows'];
        $data['categories'] = $categories;
				$pdata['status']=1;
				$pdata['message']='Data available';
				$pdata['response']=$data;
				echo json_encode($pdata);
			
    }

}

