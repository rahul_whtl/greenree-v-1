<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();

		// Load CI libraries and helpers.
		$this->load->database();
		$this->load->helper('text');
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');

 		// Example of defining a specific language to return flexi carts status and error messages.
 		// The defined language file must be added to the CI application directory as 'application/language/[language_name]/flexi_cart_lang.php'.
 		// Alternatively, CI's default language can be set via the CI config. file.
 		// Note: This must be defined before $this->load->library('flexi_cart').
 		# $this->lang->load('flexi_cart', 'spanish');

 		// IMPORTANT! This global must be defined BEFORE the flexi cart library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi cart will not work.
		$this->flexi = new stdClass;
		
		$this->load->library(array('ion_auth','flexi_cart_admin', 'session'));
		$this->load->helper('cookie');
        $this->load->config('app');
		$this->data['app'] = $this->config->item('app');
    	// Check if admin user is logged in.
		if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
		{
			$this->load->model(array('users_model','vendor_model'));
			$this->data['user'] = $this->users_model->current();
			$this->data['base_url'] = base_url();
		}
		else
		{
			// redirect if user is not requesting login page.
			if ($this->uri->segment(2) !== 'login')
			{
				$this->session->set_userdata('login_redirect', current_url());
				redirect($this->data['app']['admin_uri'].'/login');
			}
		}
	}
    
    //send google review to user
	function google_review($user_id){
        $review_status = $this->db->select('google_review')->where(array('id'=>$user_id,'blocked_customer'=>0))->get('users')->row()->google_review;
        if ($review_status == 0) { 
            $email = $this->db->select('email')->where('id', $user_id)->get('users')->row()->email;
            $this->db->where('id',$user_id);
            $this->db->update('users',array('google_review' => 1));
            $current_time = date('Y-m-d H:i:s');
            $review_link = 'https://search.google.com/local/writereview?placeid=ChIJ0c76TAQVrjsRnY2cWcGCPP0';
            $data_email = array('user_name' => $this->data['user']->first_name.' '.$this->data['user']->last_name,'lnik' => $review_link);
            $config = Array(
                        'protocol'  => 'smtp',
                        'smtp_host' => 'localhost',
                        'smtp_port' => 25,
                        'smtp_auth' => FALSE,
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $message = $this->load->view('public/email/google_review', $data_email,  TRUE);
            $subject = 'GreenREE review';
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            $customer_phone = $this->db->select('phone')->where('id', $user_id)->get('users')->row()->phone;
            $message = 'If you want us to clean this planet, Review and comment us on Google : '.$review_link;
            $sms_status = $this->users_model->common_sms_api($customer_phone, $message);
        }
    }
    
    //Genral Scrap configuartion part is start.
    public function pickup_config(){
        //for inserting and updatig days part
        if($this->input->post('update_days')){
            $select_config_data = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_config')->row();
            $config_days = $this->input->post('config_days');
            $days_length = sizeof($config_days);
            //if ($days_length) {
                $data = array('sunday'=>0,'monday'=>0,'tuesday'=>0,'wednesday'=>0,'thursday'=>0,'friday'=>0,'saturday'=>0);
                for ($i=0; $i < $days_length; $i++) { 
                    switch ($config_days[$i]) {
                        case 'Sunday':
                            $data['sunday'] = 1;
                            break;
                        case 'Monday':
                            $data['monday'] = 1;
                            break;
                        case 'Tuesday':
                            $data['tuesday'] = 1;
                            break;
                        case 'Wednesday':
                            $data['wednesday'] = 1;
                            break;
                        case 'Thursday':
                            $data['thursday'] = 1;
                            break;
                        case 'Friday':
                            $data['friday'] = 1;
                            break;
                        case 'Saturday':
                            $data['saturday'] = 1;
                            break;
                        default:
                            break;
                    }
                }
                if (empty($select_config_data)) {
                    $data['created_date'] = date('Y-m-d H:i:s');
                    $data['greenree_id']  = 1;
                    $data['general_setting_global']  = $this->input->post('apply_globally_days');
                    $this->db->insert('gre_pickup_config',$data);
                    $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'GreenREE disabled days setting is added.'));
                    redirect("pickup_config", 'refresh');
                }
                else{
                    $data['modified_date'] = date('Y-m-d H:i:s');
                    $data['general_setting_global'] = $this->input->post('apply_globally_days');
                    $this->db->where('greenree_id', 1)->update('gre_pickup_config',$data);
                    $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'GreenREE disabled days setting is updated.'));
                    redirect("pickup_config", 'refresh');
                }
            //}
        }//for inserting and updatig date part
        elseif($this->input->post('update_date')){
            $holiday_name = $this->input->post('holiday_name');
            $from_date    = $this->input->post('from_date');
            if ($this->input->post('to_date')) {
                $to_date = $this->input->post('to_date');
                $period = new DatePeriod(
                                        new DateTime($from_date),
                                        new DateInterval('P1D'),
                                        new DateTime($to_date)
                                    );
                foreach ($period as $value) {
                    $data1 = array('greenree_id'=>1,'holiday_date'=>$value->format('Y-m-d'),'holiday_name'=>$holiday_name);
                    $data1['created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('gre_pickup_date_config',$data1);      
                }
                $data1 = array('greenree_id'=>1,'holiday_date'=>date('Y-m-d',strtotime($to_date)),'holiday_name'=>$holiday_name);
                $data1['general_setting_global'] = $this->input->post('apply_globally_date');
                $data1['created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('gre_pickup_date_config',$data1);
                $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'GreenREE disabled date setting is added.')); 
                redirect("pickup_config", 'refresh');
            }
            else{
                $to_date = '';    
                $data1 = array('greenree_id'=>1,'holiday_date'=>date('Y-m-d',strtotime($from_date)),'holiday_name'=>$holiday_name);
                $data1['general_setting_global'] = $this->input->post('apply_globally_date');
                $data1['created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('gre_pickup_date_config',$data1);
                $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'GreenREE disabled date setting is added.')); 
            }
            redirect("pickup_config", 'refresh');
        }//for deleting holidays list days part
        elseif($this->input->post('delete_hoilday')){
            $item_delete = $this->input->post('selected');
            for($i=0; $i < sizeof($item_delete); $i++){ 
                $this->db->where('id', $item_delete[$i]);
                $this->db->delete('gre_pickup_date_config');
            }
            $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'GreenREE selected list is deleted.'));
            redirect("pickup_config", 'refresh');
        }
        //for dispalying days
        $this->data['pickup_config_data'] = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_config')->row();
        //for dispalying date
        $this->data['select_config_date'] = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_date_config')->result();
        $this->load->view('admin/setting/pickup_config',$this->data);
    }
    //Genral Scrap configuartion part is end.
    
    //Specific Address Scrap configuartion part is start.
    public function location_pickup_config(){
        // for deleting holidays(days list) part
        if($this->input->post('delete_locality_days')){
            $item_delete = $this->input->post('locality_days');
            for($i=0; $i < sizeof($item_delete); $i++){ 
                $this->db->where('id', $item_delete[$i]);
                $this->db->delete('gre_pickup_config');
            }
            if(empty(sizeof($item_delete))){
                $this->session->set_flashdata('custom_message',
                        array('type' => 'danger', 'message' => 'Please select locality from list.'));
            }
            else{
                $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'Selected list is deleted.'));
            }    
            $this->session->set_userdata('last_open_tab','days_tab');
            redirect("location_pickup_config", 'refresh');
        }
        if($this->input->post('delete_apartment_days')){
            $item_delete = $this->input->post('apartment_days');
            for($i=0; $i < sizeof($item_delete); $i++){ 
                $this->db->where('id', $item_delete[$i]);
                $this->db->delete('gre_pickup_config');
            }
            if(empty(sizeof($item_delete))){
                $this->session->set_flashdata('custom_message',
                        array('type' => 'danger', 'message' => 'Please select apartment from list.'));
            }
            else{
                $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'Selected list is deleted.'));
            }
            $this->session->set_userdata('last_open_tab','days_tab');
            redirect("location_pickup_config", 'refresh');
        }
        if($this->input->post('delete_locality_dates')){
            $item_delete = $this->input->post('locality_dates');
            for($i=0; $i < sizeof($item_delete); $i++){ 
                $this->db->where('id', $item_delete[$i]);
                $this->db->delete('gre_pickup_date_config');
            }
            if(empty(sizeof($item_delete))){
                $this->session->set_flashdata('custom_message',
                        array('type' => 'danger', 'message' => 'Please select locality from list.'));
            }
            else{
                $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'Selected list is deleted.'));
            }
            $this->session->set_userdata('last_open_tab','date_tab');
            redirect("location_pickup_config", 'refresh');
        } 
        if($this->input->post('delete_apartment_dates')){
            $item_delete = $this->input->post('apartment_dates');
            for($i=0; $i < sizeof($item_delete); $i++){ 
                $this->db->where('id', $item_delete[$i]);
                $this->db->delete('gre_pickup_date_config');
            }
            if(empty(sizeof($item_delete))){
                $this->session->set_flashdata('custom_message',
                        array('type' => 'danger', 'message' => 'Please select apartment from list.'));
            }
            else{
                $this->session->set_flashdata('custom_message',
                        array('type' => 'success', 'message' => 'Selected list is deleted.'));
            }
            $this->session->set_userdata('last_open_tab','date_tab');
            redirect("location_pickup_config", 'refresh');
        } 
        //for selecting specific tab
        if (!empty($this->session->userdata('last_open_tab')))
            $this->data['last_open_tab'] = $this->session->userdata('last_open_tab');
        //for dispalying days
        $this->data['config_apartment'] = $this->db->select('gre_pickup_config.*,locations.loc_name, locations.loc_parent_fk')->from('gre_pickup_config')->join('locations', 'gre_pickup_config.apartment_id = locations.loc_id', 'inner')->where(array('apartment_id !='=>0,'greenree_id ='=>0,'locality_id !='=>0))->get()->result();
        for ($i=0; $i < sizeof($this->data['config_apartment']); $i++){ 
            $this->data['config_apartment'][$i]->locality_name = $this->db->select('loc_name')->where('loc_id',$this->data['config_apartment'][$i]->locality_id)->get('locations')->row()->loc_name;
        }
        $this->data['config_locality'] = $this->db->select('gre_pickup_config.*,locations.loc_parent_fk,locations.loc_name')->from('gre_pickup_config')->join('locations', 'gre_pickup_config.locality_id = locations.loc_id', 'inner')->where(array('locality_id !='=>0,'greenree_id ='=>0,'apartment_id ='=>0))->get()->result();
        //for dispalying date
        $this->data['config_date_apartment'] = $this->db->select('gre_pickup_date_config.*,locations.loc_parent_fk,locations.loc_name')->join('locations', 'gre_pickup_date_config.apartment_id = locations.loc_id', 'inner')->where(array('apartment_id !='=>0,'greenree_id ='=>0,'locality_id !='=>0))->get('gre_pickup_date_config')->result();
        for ($i=0; $i < sizeof($this->data['config_date_apartment']); $i++){ 
            $this->data['config_date_apartment'][$i]->locality_name = $this->db->select('loc_name')->where('loc_id',$this->data['config_date_apartment'][$i]->locality_id)->get('locations')->row()->loc_name;
        }
        $this->data['config_date_locality'] = $this->db->select('gre_pickup_date_config.*,locations.loc_parent_fk,locations.loc_name')->join('locations', 'gre_pickup_date_config.locality_id = locations.loc_id', 'inner')->where(array('locality_id !='=>0,'greenree_id ='=>0,'apartment_id ='=>0))->get('gre_pickup_date_config')->result();
        //for dispalying particular location address
        $this->data['countries'] = array();
        $this->data['states'] = array();
        $this->data['cities'] = array();
        $this->data['localities'] = array();
        $this->data['apartments'] = array();
        $this->data['countries'] = $this->users_model->get_locations(1,1);
        if(sizeof($this->data['countries']) == 1){
            $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
            $this->data['states'] = $this->users_model->get_state($country_id);
            if(sizeof($this->data['states']) == 1){
                $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                $this->data['cities'] = $this->users_model->get_city($state_id);
                if(sizeof($this->data['cities']) == 1){
                    $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                    $this->data['localities'] = $this->users_model->get_locality($city_id);
                    if(sizeof($this->data['localities']) == 1){
                        $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                        $this->data['apartments'] = $this->users_model->get_apartment($locality_id);
                    }
                }
                
            }
        }
        $this->load->view('admin/setting/location_pickup_config',$this->data);
    }
    //Specific Address Scrap configuartion part is end.

    //Scrap configuartion for inserting particular location part is start.
    public function particular_location_config(){
        if($this->input->post('btn_value')){
            if ($this->input->post('btn_value')=='Update Date Settings'){
                if (!empty($this->input->post('apartment_id'))){
                    $holiday_name = $this->input->post('holiday_name');
                    $from_date    = $this->input->post('from_date');
                    if ($this->input->post('to_date')){
                        $to_date = $this->input->post('to_date');
                        $period = new DatePeriod(
                                                new DateTime($from_date),
                                                new DateInterval('P1D'),
                                                new DateTime($to_date)
                                            );
                        foreach ($period as $value) {
                            $data1 = array(
                                'apartment_id'=>$this->input->post('apartment_id'),
                                'locality_id'=>$this->input->post('locality_id'),
                                'holiday_date'=>$value->format('Y-m-d'),
                                'holiday_name'=>$holiday_name);
                            $data1['created_date'] = date('Y-m-d H:i:s');
                            $this->db->insert('gre_pickup_date_config',$data1);      
                        }
                        $data1 = array(
                            'apartment_id'=>$this->input->post('apartment_id'),
                            'locality_id'=>$this->input->post('locality_id'),
                            'holiday_date'=>date('Y-m-d',strtotime($to_date)),
                            'holiday_name'=>$holiday_name);
                        $data1['created_date'] = date('Y-m-d H:i:s');
                        $this->db->insert('gre_pickup_date_config',$data1);
                        $result = 1;//multiple date inserted for apartment
                        echo json_encode($result);
                    }
                    else{
                        $to_date = '';    
                        $data1 = array(
                            'apartment_id'=>$this->input->post('apartment_id'),
                            'locality_id'=>$this->input->post('locality_id'),
                            'holiday_date'=>date('Y-m-d',strtotime($from_date)),
                            'holiday_name'=>$holiday_name);
                        $data1['created_date'] = date('Y-m-d H:i:s');
                        $this->db->insert('gre_pickup_date_config',$data1);  
                        $result = 2;//single date inserted for apartment
                        echo json_encode($result);
                    }
                }
                else{
                    $holiday_name = $this->input->post('holiday_name');
                    $from_date    = $this->input->post('from_date');
                    if ($this->input->post('to_date')) {
                        $to_date = $this->input->post('to_date');
                        $period = new DatePeriod(
                                                new DateTime($from_date),
                                                new DateInterval('P1D'),
                                                new DateTime($to_date)
                                            );
                        foreach ($period as $value) {
                            $data1 = array(
                                'locality_id'=>$this->input->post('locality_id'),
                                'holiday_date'=>$value->format('Y-m-d'),
                                'holiday_name'=>$holiday_name
                                );
                            $data1['created_date'] = date('Y-m-d H:i:s');
                            $this->db->insert('gre_pickup_date_config',$data1);
                        }
                        $data1 = array(
                            'locality_id'=>$this->input->post('locality_id'),
                            'holiday_date'=>date('Y-m-d',strtotime($to_date)),
                            'holiday_name'=>$holiday_name,
                            'locality_setting_global'=>$this->input->post('location_globally_date')
                            );
                        $data1['created_date'] = date('Y-m-d H:i:s');
                        $this->db->insert('gre_pickup_date_config',$data1);
                        $result = 3;//multiple date inserted for locality
                        echo json_encode($result);
                    }
                    else{
                        $to_date = '';    
                        $data1 = array(
                            'locality_id'=>$this->input->post('locality_id'),
                            'holiday_date'=>date('Y-m-d',strtotime($from_date)),
                            'holiday_name'=>$holiday_name,
                            'locality_setting_global' => $this->input->post('location_globally_date')
                            );
                        $data1['created_date'] = date('Y-m-d H:i:s');
                        $this->db->insert('gre_pickup_date_config',$data1); 
                        $result = 4;//single date inserted for locality
                        echo json_encode($result); 
                    }
                }
            }
            elseif($this->input->post('btn_value')=='Update Pickup Days'){
                if (!empty($this->input->post('apartment_id'))){
                    $select_config_data = $this->db->select('*')->where('apartment_id', $this->input->post('apartment_id'))->get('gre_pickup_config')->row();
                    $config_days = $this->input->post('config_days');
                    $days_length = sizeof($config_days);
                    if ($days_length) {
                        $data = array('sunday'=>0,'monday'=>0,'tuesday'=>0,'wednesday'=>0,'thursday'=>0,'friday'=>0,'saturday'=>0);
                        for ($i=0; $i < $days_length; $i++) { 
                            switch ($config_days[$i]) {
                                case 'Sunday':
                                    $data['sunday'] = 1;
                                    break;
                                case 'Monday':
                                    $data['monday'] = 1;
                                    break;
                                case 'Tuesday':
                                    $data['tuesday'] = 1;
                                    break;
                                case 'Wednesday':
                                    $data['wednesday'] = 1;
                                    break;
                                case 'Thursday':
                                    $data['thursday'] = 1;
                                    break;
                                case 'Friday':
                                    $data['friday'] = 1;
                                    break;
                                case 'Saturday':
                                    $data['saturday'] = 1;
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (empty($select_config_data)) {
                            $data['created_date'] = date('Y-m-d H:i:s');
                            $data['apartment_id'] = $this->input->post('apartment_id');
                            $data['locality_id']  = $this->input->post('locality_id');
                            $this->db->insert('gre_pickup_config',$data);
                            $result = 5;//days inserted for apartment
                            echo json_encode($result); 
                        }
                        else{
                            $data['modified_date'] = date('Y-m-d H:i:s');
                            $this->db->where('apartment_id', $this->input->post('apartment_id'))->update('gre_pickup_config',$data);
                            $result = 6;//days updated for apartment
                            echo json_encode($result); 
                        }
                    }
                    else{
                        $result = 7;//days length was empty for apartment
                        echo json_encode($result); 
                    }
                }
                else{
                    $select_config_data = $this->db->select('*')->where(array('locality_id'=>$this->input->post('locality_id'),'apartment_id'=>0))->get('gre_pickup_config')->row();
                    $config_days = $this->input->post('config_days');
                    $days_length = sizeof($config_days);
                    if ($days_length) {
                        $data = array('sunday'=>0,'monday'=>0,'tuesday'=>0,'wednesday'=>0,'thursday'=>0,'friday'=>0,'saturday'=>0);
                        for ($i=0; $i < $days_length; $i++) { 
                            switch ($config_days[$i]) {
                                case 'Sunday':
                                    $data['sunday'] = 1;
                                    break;
                                case 'Monday':
                                    $data['monday'] = 1;
                                    break;
                                case 'Tuesday':
                                    $data['tuesday'] = 1;
                                    break;
                                case 'Wednesday':
                                    $data['wednesday'] = 1;
                                    break;
                                case 'Thursday':
                                    $data['thursday'] = 1;
                                    break;
                                case 'Friday':
                                    $data['friday'] = 1;
                                    break;
                                case 'Saturday':
                                    $data['saturday'] = 1;
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (empty($select_config_data)) {
                            $data['created_date'] = date('Y-m-d H:i:s');
                            $data['locality_id']  = $this->input->post('locality_id');
                            $data['locality_setting_global'] = $this->input->post('location_globally_days');
                            $this->db->insert('gre_pickup_config',$data);
                            $result = 8;//days inserted for locality
                            echo json_encode($result); 
                        }
                        else{
                            $data['modified_date'] = date('Y-m-d H:i:s');
                            $data['locality_setting_global'] = $this->input->post('location_globally_days');
                            $this->db->where('locality_id', $this->input->post('locality_id'))->update('gre_pickup_config',$data);
                            $result = 9;//days updated for locality
                            echo json_encode($result); 
                        }
                    }
                    else{
                        $result = 10;//days length was empty for locality
                        echo json_encode($result); 
                    }
                }
            }
            else{
                $result = 11;
                echo json_encode($result);
            }
        }
        else{
            $result = 11;
            echo json_encode($result);
        }
    }
    //Scrap configuartion for inserting particular location part is end.

    //Periodic Frequency configuartion  part is start.
    public function periodic_frequency_config(){
        $this->data['periodic_frequency'] = $this->db->select('*')->get('gre_periodic_frequency_config')->result();
        if ($this->input->post('insert_frequency')) {
            $arr_data = array(
                    'frequency_name'  => $this->input->post('frequency_name'),
                    'frequency_value' => $this->input->post('frequency_value'),
                    'created_date'    => date('Y-m-d H:i:s')
                        );
            $this->db->insert('gre_periodic_frequency_config',$arr_data);
            redirect('periodic_frequency_config','refresh');
        }
        elseif($this->input->post('delete_frequency')) {
            $item_delete = $this->input->post('selected');
            for($i=0; $i < sizeof($item_delete); $i++){ 
                $this->db->where('id', $item_delete[$i]);
                $this->db->delete('gre_periodic_frequency_config');
            }
            if(empty(sizeof($item_delete))) {
                $this->session->set_flashdata('custom_message',
                        array('type' => 'danger', 'message' => 'Please select item.'));
            }
            else{
                $this->session->set_flashdata('custom_message',
                                    array('type' => 'success', 'message' => 'Periodic frequency slected list is deleted.'));
            }
            redirect('periodic_frequency_config','refresh');
        }
        $this->load->view('admin/setting/periodic_frequency_config',$this->data);
    }
    //Periodic Frequency configuartion part is end.
    
    //Display All vendors to Admin Part is Satrt
    public function vendor_list(){
    	$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('gre_vendors');
		$crud->columns('id','name','email','mobile','address','government_id','created_date');
		$crud->fields('name','email','mobile','address','government_id','status');
		$crud->unset_clone();
		$crud->set_subject('New Vendor');
		$crud->required_fields('name','mobile');
		$crud->order_by('id','asc');
        $crud->callback_after_insert(array($this,'vendor_created_date'));
		$output = $crud->render();
		$this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/vendors/vendor_list.php',(array)$output);
    }
    //Display All vendors to Admin Part is End
    public function vendor_created_date($post_array, $primary_key){
        $mobile = $post_array['mobile'];
        $created_date = date('Y-m-d H:i:s');
        $this->db->where('mobile',$mobile)->update('gre_vendors',array('created_date'=>$created_date));
    }

    //send Notification to  individual Vandor after/before assigning scrap request to vendor Part is Satrt
    public function vendor_notification(){
		$vendorids  = $this->input->post('myarray');
		$comment    = $this->input->post('comment');
		foreach ($vendorids as $vendor_id) {
			$this->db->select('email,name,mobile');
		    $this->db->where('id',$vendor_id);
		    $query_user = $this->db->get('gre_vendors'); 		
		    $email = $query_user->row()->email;
		    $name = $query_user->row()->name;
    		$data_email = array(
    				'name'	    => $name,
                    'comment'   => $comment,
                    'email_for' => 'vendor'
                );
            $config = Array(
                    'protocol' => 'smtp',
                    /*'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',*/
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $msg = $this->load->view('admin/email/vendor_notification_mail', $data_email,  TRUE);
            $message = $msg;
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject('Important information from admin');
            $this->email->message($message);
            $this->email->send();
            $message = $comment;
            $sms_status = $this->users_model->common_sms_api($query_user->row()->phone, $message);
        //     $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
        //     $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
        //     $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        //     $this->email->to($admin_mail_id);
        // // 	$this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        //   //  $this->email->to($this->data['app']['admin_mail_id']);
        //     $this->email->subject('Important notification');
        //     $this->email->message($message);
        //     $this->email->send();
		}
    }
    //send Notification to individual Vandor after/before assigning scrap request to vendor Part is End

    //Add Customer in existing list created group Part is Satrt
    public function add_customer(){	
    	$add_customers       = $this->input->post('myarray');
		$list_created_group  = $this->input->post('add_customer_group');
		$this->db->select('list_created_date, next_pickup_date,vendor_id');
		$this->db->where('list_created_group',$list_created_group);
		$this->db->where('request_type','Periodic');
		$this->db->limit(1);
		$query = $this->db->get('gre_scrap_request');
		$message = 'New scrap request is added to your list.';
		foreach ($add_customers as $scrap_id) {
			if(!empty($scrap_id) && $query->row()->vendor_id != ''){
				$data = array(
					'vendor_id'          => $query->row()->vendor_id,
					'list_created_date'  => date('Y-m-d H:i:s',strtotime($query->row()->list_created_date)),
	     			'list_created_group' => $list_created_group,
	     			'next_pickup_date'	 => date('Y-m-d H:i:s',strtotime($query->row()->next_pickup_date))
				);
				$data1 = array(
					'vendor_id'          => $query->row()->vendor_id,
					'list_created_date'  => date('Y-m-d  H:i:s',strtotime($query->row()->list_created_date)),
	     			'list_created_group' => $list_created_group,
	     			'request_status'	 => 'Process',
	     			'next_pickup_date'	 => date('Y-m-d H:i:s',strtotime($query->row()->next_pickup_date))
				);
				
				$this->db->where('id', $scrap_id);
				$this->db->where('request_type','Periodic');
	    		$this->db->update('gre_scrap_request',$data);
	    		$this->db->where('id', $scrap_id);
				$this->db->where('request_type','Non-Periodic');
	    		$this->db->update('gre_scrap_request',$data1);
			}
		}
		$vendor_phone = $this->db->select('*')->where('id',$query->row()->vendor_id)->get('gre_vendors')->row()->mobile;
		$sms_status = $this->users_model->common_sms_api($vendor_phone, $message);
    }
    //Add Customer in existing list created group Part is End
    
    //Non Periodic customer history table start
    public function nonperiodic_scrap_request_history(){	
    	$crud = new grocery_CRUD();
    	$CI =& get_instance();
    	
    	$address_table = 'j'.substr(md5('apartment_id'),0,8);
     	$users_table = 'j'.substr(md5('user_id'),0,8);
     	$vendors_table = 'j'.substr(md5('vendor_id'),0,8);
     	$list_group_table = 'j'.substr(md5('list_created_group'),0,8);
     	
     	$CI->join = array($address_table,$users_table,$vendors_table,$list_group_table);
     	
     	$crud->set_model('gc_join_model');
    	$crud->set_theme('datatables');
    	$crud->or_where('request_status','Canceled');
     	$crud->or_where('request_status','Complete');
    	$crud->set_table('gre_scrap_request');
    	
    	$crud->set_relation('apartment_id','gre_address','apartment_name');
    	$crud->set_relation('user_id','users','first_name');
    	$crud->set_relation('vendor_id','gre_vendors','name');
    	$crud->set_relation('list_created_group','gre_vendor_request_status','list_created_group');
    	
    	$crud->columns('scrap_id','customer_id','phone','email','flat_no','apartment_id','scrap_comment','locality','prefered_pickup_date_next','prefered_pickup_day','updated_date','request_type','request_status','apartment_otp','customer_presence','customer_response','list_created_date','vendor_id','vendor_response','not_picked_reason','requested_date','last_pickup_date','next_pickup_date','reward_point','list_created_group1');

    	$crud->set_subject('Scrap Request');
    	$crud->display_as('apartment_id','Apartment')->display_as('prefered_pickup_date_next','Prefered pickup date')->display_as('customer_id','Customer Name')->display_as('vendor_id','Vendor Name')->display_as('id','Scrap id')->display_as('reward_point','Wallet Cash')->display_as('user_id','Customer name')->display_as('periodic_interval','Pickup Frequency')->display_as('flat_no','Flat')->display_as('apartment_otp','OTP')->display_as('request_status','Status')->display_as('not_picked_reason','Comments by Vendor')->display_as('list_created_group1','List Created Group');
    	$crud->fields('scrap_id','customer_id','vendor_id','apartment_id', 'customer_presence','customer_response','request_status', 'periodic_request_status','not_picked_reason','request_type', 'periodic_interval','prefered_pickup_day','requested_date', 'first_pickup_date','last_pickup_date','next_pickup_date', 'reward_point','scrap_details','scrap_weight','list_created_date', 'list_created_group');
    	$crud->callback_column('requested_date',array($this,'_callback_date'));
    	$crud->order_by('requested_date','asc');
    	$crud->callback_column('customer_id',array($this,'_callback_combine_name'));
    	$crud->unset_clone();
    	$crud->unset_read();
    	$crud->unset_add();
    	$crud->unset_edit();
    	$crud->add_action('View', '', '','ui-icon-mail-closed',array($this,'custom_request_view_history1'));
    	$crud->field_type('scrap_id','readonly');
    	$crud->set_crud_url_path(site_url('non-periodic-history'));
    	//$crud->order_by('requested_date','asc');
    	$output = $crud->render();
    	$this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
    	$this->load->view('admin/scrap_request/nonperiodic_scrap_request_history.php',(array)$output);
	}
    //Non Periodic customer history table end
    //callign admin scrap_request_details Part is Satrt
    public function custom_request_view_history1($primary_key , $row)
    {   log_message('debug','primary_key'.print_r($row,TRUE));
        return site_url('admin/admin_scrap_details/'.$row->id);
    }
    
    
    //Display List of Scrap Request which are completed,not picked etc  Part is Satrt
	public function scrap_request_history(){	
    	$crud = new grocery_CRUD();
    	$CI =& get_instance();
    	$users_table = 'j'.substr(md5('user_id'),0,8);
     	$vendors_table = 'j'.substr(md5('vendor_id'),0,8);
     	$list_group_table = 'j'.substr(md5('list_created_group'),0,8);
    	$CI->join = array($users_table,$vendors_table,$list_group_table);
    	$crud->set_model('Admin_periodic_history_model');
    	$crud->set_theme('datatables');
    	$crud->set_table('gre_periodic_request_archive');
    	// $crud->set_relation('apartment_id','gre_service_address','apartment');
    	$crud->set_relation('user_id','users','first_name');
    	$crud->set_relation('vendor_id','gre_vendors','name');
    	$crud->set_relation('list_created_group','gre_vendor_request_status','list_created_group');
    	$crud->columns('scrap_request_id','customer_id','phone','email',/*'flat_no','apartment_name','locality',*/'apartment_otp','vendor_id','vendor_response','scrap_weight','customer_presence','scrap_details','picked_up_date','not_picked_reason','reward_point','list_created_date','list_created_group');
    	$crud->set_subject('Scrap Request');
    	$crud->display_as('apartment_id','Apartment Name')->display_as('customer_id','Customer Name')->display_as('vendor_id','Vendor Name')->display_as('scrap_request_id','Scrap id');
    	$crud->fields('id','customer_id','vendor_id','customer_presence','customer_response','not_picked_reason','reward_point','list_created_date','list_created_group');
    	$crud->callback_column('picked_up_date',array($this,'_callback_date'));
    	$crud->callback_column('customer_id',array($this,'_callback_combine_name'));
     	$crud->unset_clone();
     	$crud->unset_add();
     	$crud->unset_read();
     	$crud->unset_edit();
     	$crud->add_action('View', '', '','ui-icon-mail-closed',array($this,'custom_request_view_history'));
     	$crud->order_by('picked_up_date','desc');
    	$output = $crud->render();
    	$this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
    	$this->load->view('admin/scrap_request/scrap_request_history.php',(array)$output);
	}
	//Display List of Scrap Request which are completed,not picked etc  Part is End
	
	//Display List of New and periodic(only active) Scrap Request Part is start
	public function scrap_request_admin() {	
    	$crud = new grocery_CRUD();
    	$CI =& get_instance();
    	
    	$address_table = 'j'.substr(md5('apartment_id'),0,8);
     	$users_table = 'j'.substr(md5('user_id'),0,8);
     	$vendors_table = 'j'.substr(md5('vendor_id'),0,8);
     	$list_group_table = 'j'.substr(md5('list_created_group'),0,8);
     	
     	$CI->join = array($address_table,$users_table,$vendors_table,$list_group_table);
        $check_pickup_date = date('Y-m-d');
     	$check_pickup_date = date('Y-m-d', strtotime($check_pickup_date. ' + 7 days'));
     	$crud->set_model('gc_join_model');
    	$crud->set_theme('datatables');
        
        $crud->or_where("(next_pickup_date <= '".$check_pickup_date."' AND request_status ='Under Review')",null,FALSE);
        $crud->or_where("(next_pickup_date <= '".$check_pickup_date."' AND request_status ='Placed')",null,FALSE);
        $crud->or_where("(next_pickup_date <= '".$check_pickup_date."' AND request_status ='Process')",null,FALSE);
        $crud->or_where("(next_pickup_date <= '".$check_pickup_date."' AND request_status ='Not Picked')",null,FALSE);
        $crud->or_where("(next_pickup_date <= '".$check_pickup_date."' AND periodic_request_status = 1)",null,FALSE);

    	$crud->set_table('gre_scrap_request');
    	
    	$crud->set_relation('apartment_id','gre_address','apartment_name');
    	$crud->set_relation('user_id','users','first_name');
    	$crud->set_relation('vendor_id','gre_vendors','name');
    	$crud->set_relation('list_created_group','gre_vendor_request_status','list_created_group');
    	
        $crud->columns('scrap_id','customer_id','phone','email','flat_no','apartment_id','scrap_comment','locality','prefered_pickup_date_next','prefered_pickup_day','updated_date','request_type','request_status','apartment_otp','customer_presence','customer_response','list_created_date','vendor_id','vendor_response','not_picked_reason','requested_date','last_pickup_date','next_pickup_date','reward_point','list_created_group1');

    	$crud->set_subject('Scrap Request');
    	$crud->display_as('apartment_id','Apartment')->display_as('prefered_pickup_date_next','Prefered pickup date')->display_as('customer_id','Customer Name')->display_as('vendor_id','Vendor Name')->display_as('id','Scrap id')->display_as('reward_point','Wallet Cash')->display_as('user_id','Customer name')->display_as('periodic_interval','Pickup Frequency')->display_as('flat_no','Flat')->display_as('apartment_otp','OTP')->display_as('request_status','Status')->display_as('not_picked_reason','Comments by Vendor')->display_as('list_created_group1','List Created Group');
    	$crud->fields('id', 'user_id', 'phone', 'locality', 'apartment_id', 'flat_no', 'email', 'request_type', 'vendor_id', 'customer_presence','customer_response','request_status','periodic_request_status','not_picked_reason','prefered_pickup_day','requested_date','first_pickup_date','last_pickup_date','next_pickup_date',  'time_slot_max', 'reward_point','scrap_details','scrap_weight','list_created_date','list_created_group');
    	$crud->unset_clone();
    	$crud->unset_read();
    	$crud->unset_add();
    	$crud->callback_column('customer_id',array($this,'_callback_combine_name'));
        $crud->callback_column('updated_date',array($this,'_callback_date'));
    	$crud->callback_column('requested_date',array($this,'_callback_date'));
    	$crud->callback_column('prefered_pickup_date_next',array($this,'_callback_date'));
    	$crud->callback_column('next_pickup_date',array($this,'_callback_time_slot'));
    	$crud->callback_after_update(array($this,'update_total_wallet_cash'));
    	$crud->field_type('request_type','readonly')->field_type('user_id','readonly')->field_type('customer_id','readonly')->field_type('apartment_id','readonly')->field_type('phone','readonly')->field_type('locality','readonly')->field_type('flat_no','readonly')->field_type('email','readonly')->field_type('requested_date','readonly');
    	$crud->callback_edit_field('phone',array($this,'_callback_customer_phone'));
        $crud->callback_edit_field('user_id',array($this,'_callback_combine_name'));
        $crud->callback_edit_field('locality',array($this,'_callback_customer_locality'));
        $crud->callback_edit_field('email',array($this,'_callback_customer_email'));
        $crud->callback_edit_field('flat_no',array($this,'_callback_customer_flat_no'));
        $crud->callback_edit_field('customer_presence',array($this,'radio_edit_callback'));
        $crud->callback_edit_field('customer_response',array($this,'radio_edit_callback1'));
        $crud->callback_edit_field('prefered_pickup_day',array($this,'radio_edit_callback2'));
        $crud->callback_edit_field('reward_point',array($this,'wallet_cash_callback'));
        $crud->callback_edit_field('periodic_request_status',array($this,'radio_edit_callback4'));
        $crud->callback_edit_field('request_status',array($this,'radio_edit_callback3'));
    	$crud->add_action('View', '', '','ui-icon-mail-closed',array($this,'custom_request_view'));
    	$crud->set_crud_url_path(site_url('scrap-request-admin'));
    	$crud->order_by('prefered_pickup_date_next','desc');
        $current_url = current_url();
        $current_url = explode("/",$current_url);
        $current_url = $current_url[5];
        $crud->set_lang_string('update_success_message',
             'Your data has been successfully stored into the database.
             <script type="text/javascript">
              window.location = "'.base_url('scrap-request-admin/edit/'.$current_url).'";
             </script>
             <div style="display:none">
             '
       );
    	$output = $crud->render();
    	$this->data['owner'] = $this->users_model->owner();
    		$this->load->view('admin/templates/header', $this->data);
    	$data['vendors_list'] = $this->vendor_model->vendors_list();
    	if ($this->session->userdata('last_page')) {
            $last_page = $this->session->userdata('last_page');
            $last_page = explode("/",$last_page);
            $last_page = $last_page[3];
            if ($last_page=='assign-to-vendor') {
                $data['already_assigned_id'] = $this->session->userdata('already_assigned_id');
                $data['already_assigned'] = $this->session->userdata('already_assigned');
                $data['vendor_id'] = $this->session->userdata('vendor_id');
                $data['scrapids']  = $this->session->userdata('scrapids');
                //destorying session related to assign to vendor
                $this->session->unset_userdata('last_page');
                $this->session->unset_userdata('already_assigned_id');
                $this->session->unset_userdata('already_assigned');
                $this->session->unset_userdata('scrapids');
                $this->session->unset_userdata('vendor_id');
            }
        }
    	$output->data = $data;
    	$this->_example_output($output);
	}
	//Display List of New and periodic(only active) Scrap Request Part is End
	
	//Display all scrap request
	public function view_all_scrap_request() {	
    	$crud = new grocery_CRUD();
    	$CI =& get_instance();
    	
    	$address_table = 'j'.substr(md5('apartment_id'),0,8);
     	$users_table = 'j'.substr(md5('user_id'),0,8);
     	$vendors_table = 'j'.substr(md5('vendor_id'),0,8);
     	$list_group_table = 'j'.substr(md5('list_created_group'),0,8);
     	
     	$CI->join = array($address_table,$users_table,$vendors_table,$list_group_table);
     	
     	$crud->set_model('gc_join_model');
    	$crud->set_theme('datatables');
    	//$crud->where('customer_id !=', ''); 
    	$crud->set_table('gre_scrap_request');
    	
    	$crud->set_relation('apartment_id','gre_address','apartment_name');
    	$crud->set_relation('user_id','users','first_name');
    	$crud->set_relation('vendor_id','gre_vendors','name');
    	$crud->set_relation('list_created_group','gre_vendor_request_status','list_created_group');
    	
    	$crud->columns('scrap_id','customer_id','phone','email','flat_no','apartment_id','scrap_comment','locality','prefered_pickup_date_next','prefered_pickup_day','updated_date','request_type','request_status','apartment_otp','customer_presence','customer_response','list_created_date','vendor_id','vendor_response','not_picked_reason','requested_date','last_pickup_date','next_pickup_date','reward_point','list_created_group1');
    
    	$crud->set_subject('Scrap Request');
    	$crud->display_as('apartment_id','Apartment')->display_as('prefered_pickup_date_next','Prefered pickup date')->display_as('customer_id','Customer Name')->display_as('vendor_id','Vendor Name')->display_as('id','Scrap id')->display_as('reward_point','Wallet Cash')->display_as('user_id','Customer name')->display_as('periodic_interval','Pickup Frequency')->display_as('flat_no','Flat')->display_as('apartment_otp','OTP')->display_as('request_status','Status')->display_as('not_picked_reason','Comments by Vendor')->display_as('list_created_group1','List Created Group');
    	$crud->fields('id', 'user_id', 'phone', 'locality', 'apartment_id', 'flat_no', 'email', 'request_type', 'vendor_id', 'customer_presence','customer_response','request_status','periodic_request_status','not_picked_reason','prefered_pickup_day','requested_date','first_pickup_date','last_pickup_date','next_pickup_date',  'time_slot_max', 'reward_point','scrap_details','scrap_weight','list_created_date','list_created_group');
    	$crud->unset_clone();
    	$crud->unset_read();
    	$crud->unset_add();
    	$crud->unset_delete();
        $crud->unset_edit();
    	$crud->callback_column('customer_id',array($this,'_callback_combine_name'));
    	$crud->callback_column('periodic_request_status',array($this,'_callback_request_status'));
    	$crud->callback_column('requested_date',array($this,'_callback_date'));
    	$crud->callback_column('next_pickup_date',array($this,'_callback_time_slot'));
    	$crud->callback_after_update(array($this,'update_total_wallet_cash'));
    	$crud->field_type('request_type','readonly')->field_type('user_id','readonly')->field_type('customer_id','readonly')->field_type('apartment_id','readonly')->field_type('phone','readonly')->field_type('locality','readonly')->field_type('flat_no','readonly')->field_type('email','readonly')->field_type('requested_date','readonly');
    	$crud->callback_edit_field('phone',array($this,'_callback_customer_phone'));
        $crud->callback_edit_field('user_id',array($this,'_callback_combine_name'));
        $crud->callback_edit_field('locality',array($this,'_callback_customer_locality'));
        $crud->callback_edit_field('email',array($this,'_callback_customer_email'));
        $crud->callback_edit_field('flat_no',array($this,'_callback_customer_flat_no'));
        $crud->callback_edit_field('customer_presence',array($this,'radio_edit_callback'));
        $crud->callback_edit_field('customer_response',array($this,'radio_edit_callback1'));
        $crud->callback_edit_field('prefered_pickup_day',array($this,'radio_edit_callback2'));
        $crud->callback_edit_field('reward_point',array($this,'wallet_cash_callback'));
        $crud->callback_edit_field('periodic_request_status',array($this,'radio_edit_callback4'));
        $crud->callback_edit_field('request_status',array($this,'radio_edit_callback3'));
    	$crud->add_action('View', '', '','ui-icon-mail-closed',array($this,'custom_request_view'));
    	$crud->set_crud_url_path(site_url('scrap-request-admin'));
    	$crud->order_by('requested_date','desc');
        $current_url = current_url();
        $current_url = explode("/",$current_url);
        $current_url = $current_url[5];
        $crud->set_lang_string('update_success_message',
             'Your data has been successfully stored into the database.
             <script type="text/javascript">
              window.location = "'.base_url('scrap-request-admin/edit/'.$current_url).'";
             </script>
             <div style="display:none">
             '
       );
    	$output = $crud->render();
    	$this->data['owner'] = $this->users_model->owner();
    		$this->load->view('admin/templates/header', $this->data);
    	$data['vendors_list'] = $this->vendor_model->vendors_list();
    	$output->data = $data;
    	$this->load->view('all-scrap-requests.php',(array)$output);
	}
	
	function _callback_request_status($value, $row){
	    if($row->request_type == 'Non-Periodic'){
	        return "";
	    }elseif($value == 1){
	        return 'Active';   
	    }elseif($value == 0){
	        return 'Paused'; 
	    }
	}
	
	//Display List of New and periodic(only active) Scrap Request Part is End
    
    function _callback_time_slot($value, $row){
        if($value != '0000-00-00 00:00:00'){
        $time_slot_max = $this->db->select('time_slot_max')->where('id',$row->id)->get('gre_scrap_request')->row()->time_slot_max;
        return date('d/m/Y H:i:s', strtotime($value)).' - '.$time_slot_max;
        }else{
            return "";
        }
    }
    
    function _callback_date($value, $row){
	return "<span style='visibility:hidden;display:none;'>".date('Y-m-d H:i:s', strtotime($value))."</span>".date('d/m/Y H:i:s', strtotime($value));
	}
    
    public function _callback_customer_phone($value, $row){
    	$user_id = $this->db->select('user_id')->where('id',$row)->get('gre_scrap_request')->row()->user_id;
    	$phone = $this->db->select('phone')->where('id',$user_id)->get('users')->row()->phone;
        return $phone;
    }

    public function _callback_customer_locality($value, $row){
    	$user_id = $this->db->select('user_id')->where('id',$row)->get('gre_scrap_request')->row()->user_id;
    	$locality = $this->db->select('locality')->where('user_id',$user_id)->get('gre_address')->row()->locality;
        return $locality;
    }

    public function _callback_customer_flat_no($value, $row){
    	$user_id = $this->db->select('user_id')->where('id',$row)->get('gre_scrap_request')->row()->user_id;
    	$flat_no = $this->db->select('flat_no')->where('user_id',$user_id)->get('gre_address')->row()->flat_no;
        return $flat_no;
    }

    public function _callback_customer_email($value, $row){
    	$user_id = $this->db->select('user_id')->where('id',$row)->get('gre_scrap_request')->row()->user_id;
    	$email = $this->db->select('email')->where('id',$user_id)->get('users')->row()->email;
        return $email;
    }

    
    //Add first name and last name
    public function _callback_combine_name($value, $row)
    {
        $this->db->select('first_name,last_name');
        $this->db->where('id',$value);
        $query = $this->db->get('users');
        $user = $query->row();
        return $user->first_name.' '.$user->last_name;
    }
    
    
    //update customer total reward point after adding reward point from manage scrap request table
    public function update_total_wallet_cash($post_array){
	    $wallet_cash = $post_array['reward_point'];
	    $this->db->where('id',$post_array['id']);
        $this->db->update('gre_scrap_request',array('time_slot_min'=>date('H:i:s', strtotime($post_array['next_pickup_date']))));
	    if(!empty($wallet_cash)){
	        $wallet_cash1 = $post_array['reward_point_hidden'];
            if ($wallet_cash1<1) {
    	        $user_id = $this->db->select('user_id')->where('id',$post_array['id'])->get('gre_scrap_request')->row()->user_id;
                //Assaigning new scrap pick date 
                $scrap_row_details = $this->db->select('*')->where('id',$post_array['id'])->get('gre_scrap_request')->row();
                $scrap_request_data = array(
                                    'request_status'   => $post_array['request_status'],
                                    'last_pickup_date' => $scrap_row_details->next_pickup_date,
                                    'next_pickup_date' => date('Y-m-d H:i:s', strtotime($scrap_row_details->prefered_pickup_date_next. ' +'.$scrap_row_details->periodic_interval.' days')),
                                    'prefered_pickup_date_next' => date('Y-m-d H:i:s', strtotime($scrap_row_details->prefered_pickup_date_next. ' +'.$scrap_row_details->periodic_interval.' days')));
                if ($scrap_row_details->request_type == 'Periodic') {
                    $this->db->where('id',$post_array['id']);
                    $this->db->update('gre_scrap_request',$scrap_request_data);
                }

    	        $total_wallet_cash = $this->db->select('total_reward_point')->where('id',$user_id)->get('users')->row()->total_reward_point;
    	        $phone = $this->db->select('phone')->where('id',$user_id)->get('users')->row()->phone;
    	        $email = $this->db->select('email')->where('id',$user_id)->get('users')->row()->email;
                $email_verified = $this->db->select('email_verified')->where('id',$user_id)->get('users')->row()->email_verified;
    	        $first_name = $this->db->select('first_name')->where('id',$user_id)->get('users')->row()->first_name;
    	        $last_name = $this->db->select('last_name')->where('id',$user_id)->get('users')->row()->last_name;
                $data = array('scrap_request_id'    => $post_array['id'],
                              'credit_reward_point' => $wallet_cash,
                              'user_id'             => $user_id
                              );
                $this->db->insert('gre_reward_point',$data);
    	        $total_wallet_cash = $wallet_cash+$total_wallet_cash;
    	        $this->db->set('total_reward_point', $total_wallet_cash);
                $this->db->where('id', $user_id);
                $this->db->update('users');
                $this->google_review($user_id);
                $message = 'Admin has credited cash in your wallet for your scrap sell. Your Scrap request id is '.$post_array['id'].' , Credited amount : Rs'.$wallet_cash.'. Please check your wallet.';
                $sms_status = $this->users_model->common_sms_api($phone, $message);
            	$data_email = array(
    	      		'name'  => $first_name.' '.$last_name,
    	            'credit_point'       => $wallet_cash,
    	            'scrap_request_id'   => $post_array['id'],
    	            'credited_for'       => 'update',
    	            'credited'           => 'Yes',
    	            'total_reward_point' => $total_wallet_cash
                       		 );
           		$added_order_id = $post_array['id'];
    	        $msg = $this->load->view('public/email/point_debit_credit', $data_email,  TRUE);
    	        $message  = $msg;
    	        $user_mail_id =  $email;
                if ($email_verified) {
        	        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        	        $this->email->to($user_mail_id);
        	        $this->email->subject('Wallet cash update ('.$added_order_id.')');
        	        $this->email->message($message);
        	        $this->email->send();
                }
            }
	    }
        return $post_array;
	}
	
    public function wallet_cash_callback($value) {
        return '<input type="text" id="reward_point" name="reward_point" value="'.$value.'"/>&nbsp;
           <input type="hidden"  name="reward_point_hidden" id="reward_point_hidden" value="'.$value.'" />';
    }
    public function radio_edit_callback($value) {
        if($value == 'Yes') {
            return '<input type="radio" name="customer_presence" value="'.$value.'" checked="checked" /> Yes &nbsp;
           <input type="radio"  name="customer_presence" value="No" /> No ';
        }
        elseif($value=='No') {
            return '<input type="radio" name="customer_presence" value="Yes" /> Yes &nbsp;
            <input type="radio" name="customer_presence" value="'.$value.'" checked="checked" /> No';
        }
        else{
            return '<input type="radio" name="customer_presence" value="Yes" /> Yes &nbsp;
            <input type="radio" name="customer_presence" value="No" /> No';
        }
    }
    public function radio_edit_callback1($value){
        if($value == 'Yes') {
            return '<input type="radio" name="customer_response" value="'.$value.'" checked="checked" /> Yes &nbsp;
           <input type="radio"  name="customer_response" value="No" /> No &nbsp; 
           <input type="radio"  name="customer_response" value="No Response" /> No Response &nbsp; 
           <input type="radio"  name="customer_response" value="Late Response" /> Late Response';
        } 
        elseif($value=='No') {
            return '<input type="radio" name="customer_response" value="Yes" /> Yes &nbsp;
            <input type="radio" name="customer_response" value="'.$value.'" checked="checked" /> No  &nbsp; 
           <input type="radio"  name="customer_response" value="No Response" /> No Response &nbsp; 
           <input type="radio"  name="customer_response" value="Late Response" /> Late Response';
        }
        elseif($value=='No Response') {
            return '<input type="radio" name="customer_response" value="Yes" /> Yes &nbsp;
            <input type="radio" name="customer_response" value="No" /> No &nbsp; 
           <input type="radio"  name="customer_response" value="'.$value.'"  checked="checked"/> No Response &nbsp; 
           <input type="radio"  name="customer_response" value="Late Response" /> Late Response';
        }
        elseif($value=='No Response') {
            return '<input type="radio" name="customer_response" value="Yes" /> Yes &nbsp;
            <input type="radio" name="customer_response" value="No" /> No &nbsp; 
           <input type="radio"  name="customer_response" value="No Response" /> No Response &nbsp; 
           <input type="radio"  name="customer_response" value="'.$value.'"   checked="checked"/> Late Response';
        }
        else{
            return '<input type="radio" name="customer_response" value="Yes" /> Yes &nbsp;
            <input type="radio" name="customer_response" value="No" /> No &nbsp; 
           <input type="radio"  name="customer_response" value="No Response" /> No Response &nbsp; 
           <input type="radio"  name="customer_response" value="Late Response" /> Late Response';
        }
    }
    public function radio_edit_callback2($value) {
        if($value == 'Saturday') {
            return '<input type="radio" name="prefered_pickup_day" value="'.$value.'" checked="checked" /> Saturday &nbsp;
           <input type="radio"  name="prefered_pickup_day" value="Sunday" /> Sunday ';
        } 
        elseif($value=='Sunday') {
            return '<input type="radio" name="prefered_pickup_day" value="Saturday" /> Saturday &nbsp;
            <input type="radio" name="prefered_pickup_day" value="'.$value.'" checked="checked" /> Sunday';
        }
        else{
            return '<input type="radio" name="prefered_pickup_day" value="Yes" /> Yes &nbsp;
            <input type="radio" name="prefered_pickup_day" value="Sunday" /> Sunday';
        }
    }
    public function radio_edit_callback3($value) {
    	if($value == 'Under Review') {
            return '<input type="radio" name="request_status" value="'.$value.'" checked="checked" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="Placed" /> Placed &nbsp;
           <input type="radio"  name="request_status" value="Process" /> Process &nbsp;
           <input type="radio"  name="request_status" value="Complete" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="Canceled" /> Cancel &nbsp;
           <input type="radio"  name="request_status" value="Rejected"/> Rejected';
        }
        elseif($value == 'Placed') {
            return '<input type="radio" name="request_status" value="Under Review" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="'.$value.'" checked="checked" /> Placed &nbsp;
           <input type="radio"  name="request_status" value="Process" /> Process &nbsp;
           <input type="radio"  name="request_status" value="Complete" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="Canceled" /> Cancel &nbsp;
           <input type="radio"  name="request_status" value="Rejected"/> Rejected';
        } 
        elseif($value=='Process') {
            return '<input type="radio" name="request_status" value="Under Review" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="Placed"/> Placed &nbsp;
           <input type="radio"  name="request_status" value="'.$value.'"  checked="checked"  /> Process &nbsp;
           <input type="radio"  name="request_status" value="Complete" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="Canceled" /> Cancel &nbsp;
           <input type="radio"  name="request_status" value="Rejected"/> Rejected';
        }
        elseif($value=='Complete') {
            return '<input type="radio" name="request_status" value="Under Review" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="Placed"/> Placed &nbsp;
           <input type="radio"  name="request_status" value="Process"/> Process &nbsp;
           <input type="radio"  name="request_status" value="'.$value.'"  checked="checked" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="Canceled" /> Cancel &nbsp;
           <input type="radio"  name="request_status" value="Rejected"/> Rejected';
        }
        elseif($value=='Cancel') {
            return '<input type="radio" name="request_status" value="Under Review" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="Placed"/> Placed &nbsp;
           <input type="radio"  name="request_status" value="Process"/> Process &nbsp;
           <input type="radio"  name="request_status" value="Complete" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="'.$value.'"  checked="checked" /> Cancel &nbsp;
           <input type="radio"  name="request_status" value="Rejected"/> Rejected';
        }
        elseif($value=='Rejected'){
            return '<input type="radio" name="request_status" value="Under Review" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="Placed"/> Placed &nbsp;
           <input type="radio"  name="request_status" value="Process" /> Process &nbsp;
           <input type="radio"  name="request_status" value="Complete" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="Canceled" /> Cancel &nbsp;
           <input type="radio"  name="request_status" value="'.$value.'" checked="checked"/> Rejected';
        }else{
			return '<input type="radio" name="request_status" value="Under Review" /> Under Review &nbsp;
            <input type="radio" name="request_status" value="Placed"/> Placed &nbsp;
           <input type="radio"  name="request_status" value="Process" /> Process &nbsp;
           <input type="radio"  name="request_status" value="Complete" /> Complete &nbsp;
           <input type="radio"  name="request_status" value="Canceled" /> Cancel  &nbsp;
           <input type="radio"  name="request_status" value="Rejected"/> Rejected';
		}
    }

    public function radio_edit_callback4($value) {
        if($value == 1) {
            return '<input type="radio" class="periodic_request_status_active" name="periodic_request_status" value="'.$value.'"  checked="checked" /> Active &nbsp;
           <input type="radio" class="periodic_request_status_pause" name="periodic_request_status" value="0" /> Paused';
        } 
        elseif($value== 0) {
            return '<input type="radio" class="periodic_request_status_active" name="periodic_request_status" value="1" /> Active &nbsp;
           <input type="radio" class="periodic_request_status_pause" name="periodic_request_status" value="'.$value.'"  checked="checked" /> Paused';
        }
        else{
            return '<input type="radio" class="periodic_request_status_active" name="periodic_request_status" value="1"/> Active &nbsp;
           <input type="radio" class="periodic_request_status_pause" name="periodic_request_status" value="0" /> Paused';
        }
    }

	//callign admin scrap_request_details Part is Satrt
	public function custom_request_view_history($primary_key , $row)
	{   
	    return site_url('admin/admin_scrap_details/'.$row->scrap_request_id);
	}
	//callign admin scrap_request_details Part is End
	
	public function custom_request_view($primary_key , $row)
	{   
	    return site_url('admin/admin_scrap_details/'.$primary_key);
	}
	

	//Display list of users to admin part is start
    public function admin_view_users() {
	$crud = new grocery_CRUD();
	$crud->set_theme('datatables');
	$crud->set_table('users');
	$crud->set_primary_key('user_id','users_groups');
	$crud->set_relation('id','users_groups','user_id');
	$crud->where('group_id',2);

	$crud->columns('id','username','email','email_verified','phone','phone_verified','created_on','last_login','total_reward_point','blocked_customer');
	$crud->set_subject('Users Details For Admin');
	$crud->fields('email','email_verified','first_name','last_name','phone','phone_verified');
	$crud->set_read_fields('id','username','email','email_verified','created_on','last_login','first_name','last_name','phone','phone_verified');
	$crud->field_type('email_verified', 'true_false', array('No', 'Yes'));
	$crud->field_type('phone_verified', 'true_false', array('No', 'Yes'));
	$crud->field_type('blocked_customer', 'true_false', array('No', 'Yes'));
	$crud->display_as('blocked_customer','Customer Blocked')->display_as('username','Customer Nmae');
    $crud->unset_clone();
    $crud->unset_add();
    $crud->unset_read();
    $crud->unset_edit();    
    $crud->callback_column(
	        'username', function ($value, $row) {
            $this->db->select('first_name,last_name');
            $this->db->where('username',$value);
            $query = $this->db->get('users');
            $user = $query->row();
	        return $user->first_name.' '.$user->last_name;
	        }
	    );
    $crud->required_fields('email','phone');
    $crud->field_type('id','readonly');
    $crud->add_action('View', '', 'update_user','ui-icon-mail-closed');
    $crud->callback_column('last_login',array($this,'callback_date_convert'));
    $crud->callback_column('created_on',array($this,'callback_date_convert'));
    $crud->callback_before_delete(array($this,'delete_user_product_wishlist'));
    $crud->order_by('users.id','desc');
	$output = $crud->render();
   // log_message('debug',$this->db->last_query());
	$this->data['owner'] = $this->users_model->owner();
	$this->load->view('admin/templates/header', $this->data);
	$this->load->view('admin/users/admin_view_users.php',(array)$output);
	}
	//Display list of users to admin part is end
    
    //converting in date and time
    public function callback_date_convert($input_date, $row)
    {
        $input_date = date("Y-m-d H:i:s", $input_date);
        return $input_date;
    }
    
    //converting in date  
    public function callback_dateonly_convert($input_date, $row)
    {
        return date('Y-m-d', strtotime($input_date));
    }
    
    public function callback_datetime_convert($input_date, $row)
    {
        return date('Y-m-d H:i:s', strtotime($input_date));
    }
    
    function delete_user_product_wishlist($user_id){
        $this->delete_user_wishlist1($user_id);
        $this->delete_user_product($user_id);
        $this->profile_image_unlink($user_id);
        return true;
    }
    
    function delete_user_product_inquiry($product_id){
    	$this->db->where('product_id', $product_id);
    	$this->db->delete('gre_my_product_inquiry');	
	}
    
    function profile_image_unlink($user_id){
        $uploadPath = $this->data['app']['file_path_profile'];
        $oldfilename = $this->db->select('avatar')->where('id',$user_id)->get('users')->row()->avatar;log_message('debug',$oldfilename);
        if ($oldfilename){
            if($oldfilename != "avtar_image.jpg")
                unlink($uploadPath.'/'.$oldfilename);
        }
    }
    
	//Display list of users products to admin part is start
    public function admin_view_user_products() {
		$crud = new grocery_CRUD();
		$CI =& get_instance();
	 	$users_table = 'j'.substr(md5('user_id'),0,8);
	 	$CI->join = array($users_table);
	 	$crud->set_model('Admin_user_product_model');
		$crud->set_theme('datatables');
		$crud->set_table('gre_my_product_list');
		$crud->set_relation('user_id','users','first_name');
		//$crud->set_primary_key('product_id','gre_my_product_category');
		//$crud->set_relation_n_n('category','gre_my_product_category','gre_my_product_categories','product_id','category_id','name');
		$crud->columns('id','item_name','customer_id','email','phone','estimated_price','quantity','images','status','expiry_datetime');
		$crud->set_subject('Users Product');
		$crud->fields('item_name','user_id','estimated_price','quantity','images','status','closed_reason','closed_comment','expiry_datetime','description');
		$crud->set_read_fields('id','item_name','user_id','estimated_price','quantity','images','status','closed_reason','closed_comment','expiry_datetime','description');
		$crud->callback_column('expiry_datetime',array($this,'_callback_date'));
		$crud->display_as('customer_id','Customer Name')->display_as('id','Product ID')->display_as('item_name','Product Name');
		$crud->field_type('status', 'true_false', array('Closed', 'Active'));
		$crud->callback_before_delete(array($this,'user_product_delete_check'));
		$crud->add_action('View Enquiries', '', '','ui-icon-info',array($this,'callback_view_enquiries'));
	// 	$crud->add_action('Repost', '', '','ui-icon-newwin',array($this,'callback_repost'));
		
	// 	$crud->add_action('Close', '', '','ui-icon-close',array($this,'callback_close'));
		$crud->callback_column('customer_id',array($this,'_callback_profile_url'));
		/*$crud->callback_column(
	        'expiry_datetime', function ($value, $row) {
	        return date('d-m-Y', strtotime($value));
	        }
	    );*/
		
		$crud->set_field_upload('images','assets/images/my_product/');
		$crud->unset_add();
	//	$crud->unset_edit();
	    $crud->unset_clone();
		$output = $crud->render();
	    $this->data['owner'] = $this->users_model->owner();
			$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/users/admin_view_user_products.php',(array)$output);
	}
	
	public function _callback_profile_url($value, $row)
    {
        //log_message('debug', 'user_id : '.$value);
        //log_message('debug', 'user_id : '.print_r($row,true));
        
        $this->db->select('first_name,last_name');
        $this->db->where('id',$value);
        $query = $this->db->get('users');
        $user = $query->row();
        
        return "<a target='_blank' href='".site_url('update_user/'.$value)."'>".$user->first_name." ".$user->last_name."</a>";
    }
	
	function user_product_delete_check($primary_key){
	    
		$this->db->select('images');
		$this->db->where('id', $primary_key);
		$query = $this->db->get('gre_my_product_list');
		$product = $query->row();
		if(!empty($product)){
		
		$this->db->where('product_id', $primary_key);
        $this->db->delete('gre_my_product_category');     
	    unlink('assets/images/my_product/'.$product->images);
		}
	    
	}
	
	function delete_user() {
	    if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
	    {
		 	redirect('auth', 'refresh');
		}
	    $item_ids = $this->input->post('myarray');
	    foreach($item_ids as $key => $value){
            $this->delete_user_wishlist1($value);
            $this->delete_user_product($value);
            $this->profile_image_unlink($value);
	        $this->db->where('id', $value);
            $this->db->delete('users');
	    }
	}
	
	function delete_user_product($user_id){
	    $this->db->select('id,images');
	    $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_my_product_list');
	    $result = $query->result();
	    foreach($result as $product_id){
	        $this->delete_user_product_inquiry($product_id->id);
	        $this->db->where('product_id', $product_id->id);
            $this->db->delete('gre_my_product_category');
            if ($product_id->images)
                {
                    $uploadPath = $this->data['app']['file_path_product'];
                    unlink($uploadPath.'/'.$product_id->images);
                }
	    }
	    $this->db->where('user_id', $user_id);
        $this->db->delete('gre_my_product_list');
	}
	
	function delete_user_wishlist1($user_id){
	    $this->db->select('id');
	    $this->db->where('user_id',$user_id);
	    $query = $this->db->get('gre_wishlist_list');
	    $result = $query->result();
	    foreach($result as $product_id){
	        $this->db->where('product_id', $product_id->id);
            $this->db->delete('gre_wishlist_category');
	    }
	    $this->db->where('user_id', $user_id);
        $this->db->delete('gre_wishlist_list');
	}
	
	function delete_user_item() {
	    if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
	    {
		 	redirect('auth', 'refresh');
		}
	    $item_ids      = $this->input->post('myarray');
	    foreach($item_ids as $key => $value){
	        $this->user_product_delete_check($value);
	        $this->db->where('id', $value);
            $this->db->delete('gre_my_product_list'); 
	    }
	    
	}
	
	
	//Display list of users products to admin part is end
	
	//Display users wishlists to admin part is start
    public function admin_view_user_wishlist() {
	$crud = new grocery_CRUD();
	$CI =& get_instance();
 	$users_table = 'j'.substr(md5('user_id'),0,8);
 	$CI->join = array($users_table);
 	$crud->set_model('Admin_user_wishlist_model');
	$crud->set_theme('datatables');
	$crud->set_table('gre_wishlist_list');
	$crud->set_relation('user_id','users','first_name');
	$crud->columns('id','item_name','customer_id','email','phone','quantity','status','expiry_datetime');
	$crud->set_subject('Users Wishlist');
	$crud->fields('item_name','user_id','quantity','status','closed_reason','closed_comment','expiry_datetime','description');
	$crud->set_read_fields('id','item_name','customer_id','quantity','status','closed_reason','closed_comment','expiry_datetime','description');
	$crud->display_as('customer_id','Customer Name')->display_as('id','Product ID')->display_as('item_name','Product Name')->display_as('user_id','Customer Name');
	$crud->field_type('status', 'true_false', array('Closed', 'Active'));
	$crud->callback_before_delete(array($this,'wishlist_delete_check'));
	//$crud->add_action('Repost', '', '','ui-icon-newwin',array($this,'callback_repost_wishlist'));
    $crud->callback_column('customer_id',array($this,'_callback_profile_url'));
    $crud->callback_column('expiry_datetime',array($this,'_callback_date'));
	/*$crud->callback_column(
        'expiry_datetime', function ($value, $row) {
        return date('d-m-Y', strtotime($value));
        }
    );*/

	//$crud->add_action('Close', '', '','ui-icon-close',array($this,'callback_close_wishlist'));
	
	$crud->set_field_upload('images','assets/images/my_product/');
    $crud->unset_clone();
    $crud->unset_add();
   // $crud->unset_edit();
    $crud->order_by('id','desc');
	$output = $crud->render();
    $this->data['owner'] = $this->users_model->owner();
	$this->load->view('admin/templates/header', $this->data);
	$this->load->view('admin/users/wishlist/admin_view_user_wishlist.php',(array)$output);
	}
	
	function wishlist_delete_check($primary_key){
		
		$this->db->where('product_id', $primary_key);
        $this->db->delete('gre_wishlist_category');     
	    
	}
	
	function delete_user_wishlist() {
	    
	    if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
	    {
		 	redirect('auth', 'refresh');
		}
	    
	    $item_ids      = $this->input->post('myarray');
	    
	    foreach($item_ids as $key => $value){
	        
	        //Delete product category
	        $this->wishlist_delete_check($value);
	        
	        $this->db->where('id', $value);
            $this->db->delete('gre_wishlist_list'); 
	        
	    }
	    
	}
	
// 	function reward_point_update_callback($post_array, $primary_key) {
// 		if(!empty($post_array['credit_reward_point']))
// 		{	
// 			$total_reward_point = $this->db->select('total_reward_point')->where('id',$post_array['user_id'])->get('users')->row()->total_reward_point;
// 			$previous_credit_point = $this->db->select('credit_reward_point')->where('id',$post_array['id'])->get('gre_reward_point')->row()->credit_reward_point;
// 			if($previous_credit_point==0){
// 			    $net_reward_point = $post_array['credit_reward_point'];
// 			    $total_reward_point = $total_reward_point+$post_array['credit_reward_point'];
// 			    $this->db->where('id',$post_array['user_id'])->update('users',array('total_reward_point'=> $total_reward_point));
// 			}
// 			elseif ($previous_credit_point > $post_array['credit_reward_point']) {
// 				$net_reward_point = $previous_credit_point-$post_array['credit_reward_point'];
// 				if ($total_reward_point<$net_reward_point) {
// 				    $this->db->where('id',$post_array['user_id'])->update('users',array('total_reward_point' =>0 ));
// 				}
// 				else{
// 					$total_reward_point = $total_reward_point-$net_reward_point;
// 					$this->db->where('id',$post_array['user_id'])->update('users',array('total_reward_point' =>$total_reward_point ));
// 					}
// 				$net_reward_point = $post_array['credit_reward_point'];
// 			}
// 			else{
// 				$net_reward_point = $post_array['credit_reward_point']-$previous_credit_point;
// 				if ($total_reward_point<$net_reward_point) {
// 					$this->db->where('id',$post_array['user_id'])->update('users',array('total_reward_point' =>0 ));
// 				}
// 				else{
// 					$total_reward_point = $total_reward_point+$net_reward_point;
// 					$this->db->where('id',$post_array['user_id'])->update('users',array('total_reward_point' =>$total_reward_point ));
// 					}
// 				$net_reward_point = $post_array['credit_reward_point'];
// 			}
// 			$post_array['credit_reward_point']=$net_reward_point;
// 			return $post_array;
// 		}
// 		else
// 		{
// 			return $post_array;
// 		}
// 	}


	function reward_point_check_ids_callback($post_array) {
	    if($post_array['order_id'] == '' && $post_array['scrap_request_id'] == '' && $post_array['redeem_request_id'] == ''){
		    $this->form_validation->set_message('reward_point_check_ids_callback', 'Please Enter Order id or Redeem id or Scrap request id');
            return false;    
        }
        elseif($post_array['credit_reward_point'] == '' && $post_array['debit_reward_point'] == ''){
		    $this->form_validation->set_message('reward_point_check_ids_callback', 'Please Enter Debit rewar point or Credit reward point.');
            return false;    
        }
        else{
            if (!empty($post_array['order_id'])) {
                $user_id = $this->db->select('ord_user_fk')->where('ord_order_number',$post_array['order_id'])->get('order_summary')->row()->ord_user_fk;
            }
            elseif (!empty($post_array['scrap_request_id'])) {
                $user_id = $this->db->select('user_id')->where('id',$post_array['scrap_request_id'])->get('gre_scrap_request')->row()->user_id;
            }
            else{
                $user_id = $this->db->select('user_id')->where('id',$post_array['redeem_request_id'])->get('gre_redeem_request')->row()->user_id;
            }
            $total_reward_point = $this->db->select('*')->where('id',$user_id)->get('users')->row()->total_reward_point;
            if ($total_reward_point<$post_array['debit_reward_point']) {
                $this->form_validation->set_message('reward_point_check_ids_callback', 'User wallet cash is less than debit wallet cash.');
                return false;  
            }
            return true;
        }
	}

	public function reward_point_add_callback($post_array, $primary_key){
		if(!empty($post_array['credit_reward_point'])){	
            if (!empty($post_array['order_id'])) {
                $user_id = $this->db->select('ord_user_fk')->where('ord_order_number',$post_array['order_id'])->get('order_summary')->row()->ord_user_fk;
            }
            elseif (!empty($post_array['scrap_request_id'])) {
                $user_id = $this->db->select('user_id')->where('id',$post_array['scrap_request_id'])->get('gre_scrap_request')->row()->user_id;
            }
            else{
                $user_id = $this->db->select('user_id')->where('id',$post_array['redeem_request_id'])->get('gre_redeem_request')->row()->user_id;
            }
			$user_details = $this->db->select('*')->where('id',$user_id)->get('users');
			$total_reward_point = $user_details->row()->total_reward_point;
		    $total_reward_point = $total_reward_point+$post_array['credit_reward_point'];
		    $this->db->where('id',$user_id)->update('users',array('total_reward_point'=> $total_reward_point));
		    $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
		    if (!empty($post_array['order_id'])) {
		    	$data_email = array(
	      		'name'  => $user_details->row()->first_name.' '. $user_details->row()->first_name,
	            'credit_point'   => $post_array['credit_reward_point'],
	            'order_id'       => $post_array['order_id'],
	            'customer_msg'   => $post_array['comment'],
	            'credited_for'   => 'shoping',
	            'credited'       => 'Yes',
	            'total_reward_point' => $total_reward_point
	                   		 );
       		    $added_order_id = $post_array['order_id'];
       		    $message = 'Admin has credited cash in your wallet for your  GreenREE product order. Your order id is '.$post_array['order_id'].' , Credited amount : Rs '.$post_array['credit_reward_point'].'. Please check your wallet.';
                //updating user_id in redeem point table
                $this->db->where('id',$primary_key);
                $this->db->update('gre_reward_point',array('user_id'=>$user_id));
		    }
	        elseif (!empty($post_array['scrap_request_id'])) {
		    	$data_email = array(
	      		'name'  => $user_details->row()->first_name.' '. $user_details->row()->first_name,
	            'credit_point'     => $post_array['credit_reward_point'],
	            'scrap_request_id' => $post_array['scrap_request_id'],
	            'customer_msg'     => $post_array['comment'],
	            'credited_for'     => 'scrap',
	            'credited'         => 'Yes',
	            'total_reward_point' => $total_reward_point
	                   		 );
	            $added_order_id = $post_array['scrap_request_id'];
	            $message = 'Admin has credited cash in your wallet for your  scrap sell. Your Scrap request id is '.$post_array['scrap_request_id'].' , Credited amount : Rs '.$post_array['credit_reward_point'].'. Please check your wallet.';
                $reward_point = $this->db->select('*')->where('id',$post_array['scrap_request_id'])->get('gre_scrap_request')->row()->reward_point;
                $reward_point = $reward_point+$post_array['credit_reward_point'];
                $this->db->where('id',$post_array['scrap_request_id']);
                $this->db->update('gre_scrap_request',array('reward_point'=>$reward_point));
                //updating user_id in redeem point table
                $this->db->where('id',$primary_key);
                $this->db->update('gre_reward_point',array('user_id'=>$user_id));
		    }
		    else{
		    	$data_email = array(
	      		'name'  => $user_details->row()->first_name.' '. $user_details->row()->first_name,
	            'credit_point'       => $post_array['credit_reward_point'],
	            'redeem_request_id'  => $post_array['redeem_request_id'],
	            'customer_msg'       => $post_array['comment'],
	            'credited_for'       => 'redeem',
	            'credited'           => 'Yes',
	            'total_reward_point' => $total_reward_point
	                   		 );
           		$added_order_id = $post_array['redeem_request_id'];
	            $message = 'Admin has credited cash in your wallet for your Redeem request. Your Redeem request id is '.$post_array['redeem_request_id'].' , Credited amount : Rs '.$post_array['credit_reward_point'].'. Please check your wallet.';
                //updating user_id in redeem point table
                $this->db->where('id',$primary_key);
                $this->db->update('gre_reward_point',array('user_id'=>$user_id));
		    }
	        $msg = $this->load->view('public/email/point_debit_credit', $data_email,  TRUE);
	        $message  = $msg;
	        $user_mail_id =  $user_details->row()->email;
	        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
	        $this->email->to($user_mail_id);
	        $this->email->subject('Wallet cash update ('.$added_order_id.')');
	        $this->email->message($message);
	        $this->email->send();
	        //send sms to user
            $sms_status = $this->users_model->common_sms_api($phone, $message);
		}
		else if(!empty($post_array['debit_reward_point']))
		{   
            if (!empty($post_array['order_id'])) {
                $user_id = $this->db->select('ord_user_fk')->where('ord_order_number',$post_array['order_id'])->get('order_summary')->row()->ord_user_fk;
            }
            elseif (!empty($post_array['scrap_request_id'])) {
                $user_id = $this->db->select('user_id')->where('id',$post_array['scrap_request_id'])->get('gre_scrap_request')->row()->user_id;
            }
            else{
                $user_id = $this->db->select('user_id')->where('id',$post_array['redeem_request_id'])->get('gre_redeem_request')->row()->user_id;
            }
			$user_details = $this->db->select('*')->where('id',$user_id)->get('users');
			$total_reward_point = $user_details->row()->total_reward_point;
		    $total_reward_point = $total_reward_point - $post_array['debit_reward_point'];
		    if($total_reward_point > 0)
		        $this->db->where('id',$user_id)->update('users',array('total_reward_point'=> $total_reward_point));
		    else
		        $this->db->where('id',$user_id)->update('users',array('total_reward_point'=> 0));
		    if (!empty($post_array['order_id'])) {
		    	$data_email = array(
	      		'name'  => $user_details->row()->first_name.' '. $user_details->row()->first_name,
	            'debit_point'    => $post_array['debit_reward_point'],
	            'order_id'       => $post_array['order_id'],
	            'customer_msg'   => $post_array['comment'],
	            'credited_for'   => 'shoping',
	            'credited'       => 'no',
	            'total_reward_point' => $total_reward_point
	                   		 );
           		$added_order_id = $post_array['order_id'];
           		$message = 'Admin has debited cash from your wallet for your  GreenREE product order. Your order id is '.$post_array['order_id'].' , Debited amount : Rs '.$post_array['debit_reward_point'].'. Please check your wallet.';
                //updating user_id in redeem point table
                $this->db->where('id',$primary_key);
                $this->db->update('gre_reward_point',array('user_id'=>$user_id));
		    }
	        elseif (!empty($post_array['scrap_request_id'])) {
		    	$data_email = array(
	      		'name'  => $user_details->row()->first_name.' '. $user_details->row()->first_name,
	            'debit_point'      => $post_array['debit_reward_point'],
	            'scrap_request_id' => $post_array['scrap_request_id'],
	            'customer_msg'     => $post_array['comment'],
	            'credited_for'     => 'scrap',
	            'credited'         => 'no',
	            'total_reward_point' => $total_reward_point
	                   		 );
       		    $added_order_id = $post_array['scrap_request_id'];
       		    $message = 'Admin has debited cash from your wallet for your  scrap sell. Your Scrap request id is '. $post_array['scrap_request_id'].' , Debited amount : Rs '.$post_array['debit_reward_point'].'. Please check your wallet.';
                $reward_point = $this->db->select('*')->where('id',$post_array['scrap_request_id'])->get('gre_scrap_request')->row()->reward_point;
                $reward_point = $reward_point-$post_array['debit_reward_point'];
                $this->db->where('id',$post_array['scrap_request_id']);
                $this->db->update('gre_scrap_request',array('reward_point'=>$reward_point));
                //updating user_id in redeem point table
                $this->db->where('id',$primary_key);
                $this->db->update('gre_reward_point',array('user_id'=>$user_id));
		    }
		    else{
		    	$data_email = array(
	      		'name'  => $user_details->row()->first_name.' '. $user_details->row()->first_name,
	            'debit_point'        => $post_array['debit_reward_point'],
	            'redeem_request_id'  => $post_array['redeem_request_id'],
	            'customer_msg'       => $post_array['comment'],
	            'credited_for'       => 'redeem',
	            'credited'           => 'no',
	            'total_reward_point' => $total_reward_point
	                   		 );
	            $added_order_id = $post_array['redeem_request_id'];
	            $message = 'Admin has debited cash from your wallet for your Redeem request. Your Redeem request id is '.$post_array['redeem_request_id'].' , Debited amount : Rs'.$post_array['debit_reward_point'].'. Please check your wallet.';
                //updating user_id in redeem point table
                $this->db->where('id',$primary_key);
                $this->db->update('gre_reward_point',array('user_id'=>$user_id));
		    }
	        $msg = $this->load->view('public/email/point_debit_credit', $data_email,  TRUE);
	        $message  = $msg;
	        $user_mail_id =  $user_details->row()->email;
	        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
	        $this->email->to($user_mail_id);
	        $this->email->subject('Wallet cash update ('.$added_order_id.')');
	        $this->email->message($message);
	        $this->email->send();//send sms to user
            $sms_status = $this->users_model->common_sms_api($phone, $message);
		}
		return $post_array;
	}
	
	//Display users wishlists to admin part is end

	public function callback_view_enquiries($primary_key , $product){
		return site_url('user_inquiries/'.$product->id);
	}
	
	public function callback_repost($primary_key , $product){
		return site_url('admin/repost_product/'.$product->id);
	}
	
	public function callback_view_user_reward($primary_key , $product){
		return site_url('admin/update_user/'.$product->user_id.'#points');
	}
	public function callback_close($primary_key , $product){
		return site_url('admin/close_product/'.$product->id);
	}
	public function callback_close_wishlist($primary_key , $product){
		return site_url('admin/close_wishlist/'.$product->id);
	}
	public function callback_repost_wishlist($primary_key , $product){
		return site_url('admin/repost_wishlist/'.$product->id);
	}

	//Display users products inquiries to admin part is start
    public function user_inquiries($product_id) {
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('gre_my_product_inquiry');
		$crud->where('product_id',$product_id);
		$crud->columns('id','name','email','phone','product_id','created_date');
		$crud->set_subject('Product Inquiry');
		$crud->set_read_fields('id','name','email','phone','product_id','created_date');
		$crud->unset_edit();
	    $crud->unset_clone();
	    $crud->unset_add();
		$output = $crud->render();
	    $this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/users/admin_view_product_inquiry',(array)$output);
	}
	//Display users products inquiries to admin part is End

	//Allow admin to repost the user product part is Start
	function repost_product($product_id){
		$this->load->model('users_model');
		$product = $this->users_model->get_product_details($product_id);
		$current_expiry = $product->expiry_datetime;
		$this->db->set('expiry_datetime', date('Y-m-d H:i:s', strtotime($current_expiry.' + 30 days')));
		$this->db->set('status', 1);
		$this->db->set('closed_reason','');
		$this->db->set('closed_comment','');
		$this->db->where('id', $product_id);
		$result = $this->db->update('gre_my_product_list');
		if($result)
		$this->session->set_flashdata('message', 'Product has been reposted successfully!');
		redirect('admin/admin_view_user_products');
	}
	//Allow admin to repost the user product part is End
    
    //Allow admin to close the product part is start
    function close_product($product_id){
    		
		$data = array(
			'status' => 0,
        	'closed_reason' => 'Other',
        	'closed_comment' => 'No response',
        );
        
        $this->db->where('id',$product_id);
    	$status = $this->db->update('gre_my_product_list',$data);
		
		if($status){
	    	$this->session->set_flashdata('message', 'Product has been closed successfully!');
	    	redirect('admin/admin_view_user_products');
		}else{
			$this->session->set_flashdata('message', 'Please try again!');
			redirect('admin/admin_view_user_products');
		}
		
	}
	//Allow admin to close the product part is end

	//Allow admin to close the wishlist part is start
    function close_wishlist($product_id){
    		
		$data = array(
			'status' => 0,
        	'closed_reason' => 'Other',
        	'closed_comment' => 'No response',
        );
        
        $this->db->where('id',$product_id);
    	$status = $this->db->update('gre_wishlist_list',$data);
		
		if($status){
	    	$this->session->set_flashdata('message', 'Product has been closed successfully!');
	    	redirect('admin/admin_view_user_wishlist');
		}else{
			$this->session->set_flashdata('message', 'Please try again!');
			redirect('admin/admin_view_user_wishlist');
		}	
	}
	//Allow admin to close the wishlist part is end

	//Allow admin to repost the user wishlist part is start
	function repost_wishlist($product_id){
		$this->load->model('users_wishlist_model');
		$product = $this->users_wishlist_model->get_wishlist_details($product_id);
		$current_expiry = $product->expiry_datetime;
		$this->db->set('expiry_datetime', date('Y-m-d H:i:s', strtotime($current_expiry.' + 30 days')));
		$this->db->set('status', 1);
		$this->db->set('closed_reason','');
		$this->db->set('closed_comment','');
		$this->db->where('id', $product_id);
		$result = $this->db->update('gre_wishlist_list');
		log_message('debug',print_r($result,TRUE));
		if($result)
		$this->session->set_flashdata('message', 'Product has been reposted successfully!');
		redirect('admin/admin_view_user_wishlist');
	}
	//Allow admin to repost the user wishlist part is end

	//display user's product categories to admin part is start
    public function admin_view_product_categories() {
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('gre_my_product_categories');
		$crud->columns('id','name','slug');
		$crud->callback_before_insert(array($this,'product_slug'));
		$crud->callback_before_update(array($this,'product_slug'));
		$crud->required_fields('name','slug');
		$crud->set_subject('Product Category For User');
		$crud->fields('name','slug');
		$crud->set_read_fields('id','name','slug');
	    $crud->unset_clone();
	    $crud->set_crud_url_path(site_url('admin_view_product_categories'));
		$output = $crud->render();
		$this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/users/admin_view_product_categories.php',(array)$output);
	}
	//display user's product categories to admin part is end
    
    public function product_slug($post_array){
	    $this->load->helper('url');
	    if($post_array['slug'] != ''){
    	    $title = strip_tags($post_array['slug']);
            $titleURL = strtolower(url_title($title));
            $post_array['slug'] = $titleURL;
	    }else{
	        $title = strip_tags($post_array['name']);
            $titleURL = strtolower(url_title($title));
            $post_array['slug'] = $titleURL;
	    }
        return $post_array;
	}
    
	//display user's wishlist categories to admin part is start
    public function admin_view_wishlist_categories() {
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('gre_wishlist_categories');
		$crud->columns('id','name','slug');
		$crud->set_subject('Wishlist Category For User');
		$crud->fields('name','slug');
		$crud->set_read_fields('id','name','slug');
		$crud->callback_before_insert(array($this,'wishlist_slug'));
		$crud->callback_before_update(array($this,'wishlist_slug'));
		$crud->required_fields('name','slug');
		$crud->display_as('id','Category Id')->display_as('name','Category Name')->display_as('slug','Category Slug');
	    $crud->unset_clone();
		$output = $crud->render();
	    $this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/users/wishlist/admin_view_wishlist_categories.php',(array)$output);
	}
	//display user's wishlist categories to admin part is end
	
	public function wishlist_slug($post_array){
	    $this->load->helper('url');
	    if($post_array['slug'] != ''){
    	    $title = strip_tags($post_array['slug']);
            $titleURL = strtolower(url_title($title));
            $post_array['slug'] = $titleURL;
	    }else{
	        $title = strip_tags($post_array['name']);
            $titleURL = strtolower(url_title($title));
            $post_array['slug'] = $titleURL;
	    }
        return $post_array;
	}
	
    // display list of redeem request to admin part is start
    public function redeem_request_view(){
    	$crud = new grocery_CRUD();
		$CI =& get_instance();
		
	 	$users_table = 'j'.substr(md5('user_id'),0,8);
	 	
	 	$CI->join = array($users_table);
	 	
	 	$crud->set_model('redeem_join_model');
		$crud->set_theme('datatables');
		$crud->set_table('gre_redeem_request');
		$crud->set_relation('user_id','users','id');

		$crud->columns('id','user_id','customer_id','phone','flat_no','apartment','locality','email','request_date','status','status_modify_date','total_reward_point');
		$crud->display_as('customer_id','Customer Name');
		$crud->fields('id','user_id','total_reward_point','status');
		$crud->set_subject('Redeem Request Details For Admin');
		$crud->callback_column('customer_id',array($this,'_callback_combine_name'));
		$crud->callback_column('request_date',array($this,'_callback_date'));
		$crud->callback_column('locality',array($this,'_callback_user_locality1'));
        $crud->callback_column('apartment',array($this,'_callback_user_apartment1'));
        $crud->callback_column('flat_no',array($this,'_callback_user_flat_no1'));
		$crud->order_by('request_date','desc');
	    $crud->unset_clone();
	    $crud->unset_edit();
	    $crud->unset_add();
	    $crud->unset_delete();
	    $crud->unset_read();
	    $crud->add_action('Mark As Completed', '', 'admin/edit_redeem_request','ui-icon-mail-closed');
		$output = $crud->render();
		$this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/reward_points/redeem_request_view',(array)$output);
	}
	// display list of redeem request to admin part is end
    
    //callback function for displaying address of user in redeem request start
    public function _callback_user_city1($value, $row){
        $city = $this->db->select('city')->where('user_id',$row->user_id)->get('gre_address')->row()->city;
        return $city;
    }
    
    public function _callback_user_locality1($value, $row){
        $locality = $this->db->select('locality')->where('user_id',$row->user_id)->get('gre_address')->row()->locality;
        return $locality;
    }
    public function _callback_user_apartment1($value, $row){
        $apartment_name = $this->db->select('apartment_name')->where('user_id',$row->user_id)->get('gre_address')->row()->apartment_name;
        return $apartment_name;
    }
    public function _callback_user_flat_no1($value, $row){
        $flat_no = $this->db->select('flat_no')->where('user_id',$row->user_id)->get('gre_address')->row()->flat_no;
        return $flat_no;
    }
    //callback function for displaying address of user in redeem request end
    
	//Display Scrap Details to Admin for specific request id part is start
	public function admin_scrap_details(){
		$this->load->helper('url');
        $scrap_id = $this->uri->segment('3');
        $this->data['scrap_id'] = $scrap_id;
        /*$this->db->select('*');
        $this->db->where('id',$scrap_id);
        $query = $this->db->get('gre_scrap_request');
        $this->db->select('apartment_name,locality,flat_no');
        $this->db->where('id',$query->row()->apartment_id);
        $query_address = $this->db->get('gre_address');
        $this->data['scrap_address'] = $query_address->result();
        $this->db->select('first_name,last_name,email,phone');
        log_message("Debug","User ID - ".$query->row()->user_id);
        $this->db->where('id',$query->row()->user_id);
        $query_user = $this->db->get('users');
        $this->data['scrap_user'] = $query_user->row();
        log_message("Debug","User - ".print_r($this->data['scrap_user'], true));
        $this->db->select('name');
        $this->db->where('id',$query->row()->vendor_id);
        $query_vendor = $this->db->get('gre_vendors');
        $this->data['scrap_vendor'] = $query_vendor->row();
        $this->db->select('vendor_response');
        $this->db->where('list_created_group',$query->row()->list_created_group);
        $query_vendor_status = $this->db->get('gre_vendor_request_status');
        $this->data['scrap_vendor_status'] = $query_vendor_status->result();
        $this->data['scrap_info'] = $query->result();
        $this->db->select('*');
        $this->db->where('scrap_request_id',$scrap_id);
        $query2 = $this->db->get('gre_periodic_request_archive');
        $this->data['scrap_info_archive'] = $query2->result();*/
        
    $this->db->select('a.*,b.apartment_name,b.locality,b.flat_no,c.first_name,c.last_name,c.phone,c.email,d.name As vendor_name');
	    $this->db->from('gre_scrap_request a'); 
	    $this->db->join('gre_address b', 'b.id=a.apartment_id', 'left');
	    $this->db->join('users c', 'c.id=a.user_id', 'left');
	    $this->db->join('gre_vendors d', 'd.id=a.vendor_id', 'left');
	    $this->db->join('gre_vendor_request_status e', 'e.list_created_group=a.list_created_group', 'left');
	    $this->db->where('a.id',$scrap_id);
	    $this->db->order_by('a.prefered_pickup_date_next','desc');
	    $query1 = $this->db->get();
        $this->data['scrap_info'] = $query1->result();
        
    $this->db->select('a.*,b.apartment_name,b.locality,b.flat_no,c.first_name,c.last_name,c.phone,c.email,d.name As vendor_name');
	    $this->db->from('gre_periodic_request_archive a'); 
	    $this->db->join('gre_address b', 'b.user_id=a.user_id', 'left');
	    $this->db->join('users c', 'c.id=a.user_id', 'left');
	    $this->db->join('gre_vendors d', 'd.id=a.vendor_id', 'left');
	    $this->db->join('gre_vendor_request_status e', 'e.list_created_group=a.list_created_group', 'left');
	    $this->db->where('a.id',$scrap_id);
	    $this->db->order_by('a.picked_up_date','desc');
	    $query2 = $this->db->get();
        $this->data['scrap_info_archive'] = $query2->result();
        
        $this->load->view('admin/scrap_request/admin_scrap_details.php',$this->data);
	}
	//Display Scrap Details to Admin for specific request id part is end

	//modify redeem request for individual user part is start
	public function edit_redeem_request(){
		$this->load->helper('url');
        $redeem_id = $this->uri->segment('3');
        $user_id = $this->db->select('user_id')->where('id',$redeem_id)->get('gre_redeem_request')->row()->user_id;
        $redeem_reward_point = $this->db->select('total_reward_point')->where('id',$redeem_id)->get('gre_redeem_request')->row()->total_reward_point;
        $user_available_reward_point = $this->db->select('total_reward_point')->where('id',$user_id)->get('users')->row()->total_reward_point;
        $left_reward_point   = $user_available_reward_point - $redeem_reward_point;
        if($left_reward_point<0){
        	//$this->flexi_cart->set_error_message('User does not have sufficient Amount.', 'public', TRUE);
        	//$this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));
        	$this->session->set_flashdata('alert', array(
						'type' => 'danger',
						'message' => 'User does not have sufficient Amount.'
					));
	        redirect('redeem-request-view');
		}
		else{
	        $this->vendor_model->edit_redeem_request($redeem_id);
	        $this->session->set_flashdata('message', 'Redeem request is completed successfully.');
	        redirect('redeem-request-view');
		}
	}
	//modify redeem request for individual user part is end

	//modify redeem request for one or more than one user part is start
    public function modify_redeem_request(){
    	$modify_redeem_ids = $this->input->post('myarray');
		$this->vendor_model->modify_redeem_request($modify_redeem_ids);
    }
    //modify redeem request for one or more than one user part is end

    //assaign scrap request to vendor part is start
	public function scrap_assign_vendor(){
        $force_assign = @$this->input->post('force_assign');

        $scrapids = $this->input->post('scrap_ids');
        $scrap_array = explode(",",$scrapids);
        $vendor_id = $this->input->post('vendor_id');
        $already_assigned = 0;
            
        if(empty($force_assign)) {
            //checking scrap request is already assigned to vendor or not
            foreach($scrap_array as $scrap_id){
                $scrap_assigned_info = $this->db->select('vendor_id,vendor_response')->where('id', $scrap_id)->get('gre_scrap_request')->row();
                if(!empty($scrap_assigned_info->vendor_id) && $scrap_assigned_info->vendor_response != 'Rejected'){
                    $already_assigned_id[$already_assigned] = $scrap_id;
                    $already_assigned++;
                }
            }            
        }

        if($already_assigned>0){
            $this->session->set_userdata('last_page', current_url());
            $this->session->set_userdata('already_assigned_id', $already_assigned_id );
            $this->session->set_userdata('already_assigned', 1);
            $this->session->set_userdata('scrapids', $scrapids);
            $this->session->set_userdata('vendor_id', $vendor_id);
            redirect('scrap-request-admin');
        }else{
            $this->db->select_max('list_created_group');
            $query = $this->db->get('gre_scrap_request');
            $list_created_group = $query->row()->list_created_group;
            $list_created_group++;

            $rand_num = rand(1000,9999);
            $contact_number = $this->db->select('mobile')->where('id',$vendor_id)->get('gre_vendors')->row()->mobile;
            $message = 'You have new scrap request assigned. Please check the vendor app.';
            //$sms_status = $this->users_model->common_sms_api($contact_number,$message);
            if(!empty($scrap_array) && $vendor_id != ''){
                $vendor_table_query = $this->db->select('*')->where('id',$vendor_id)->get('gre_vendors')->row();
                $data = array(
                    'vendor_id'          => $vendor_id,
                    'list_created_date'  => date('Y-m-d H:i:s'),
                    'list_created_group' => $list_created_group,
                    'request_status'     => 'Process'
                );
                $data1 = array(
                    'vendor_id'               => $vendor_id,
                    'list_created_date'       => date('Y-m-d H:i:s'),
                    'list_created_group'      => $list_created_group,
                    'request_status'          => 'Process'
                );
                
                foreach($scrap_array as $key => $scrap_id){
                    $this->db->where('id', $scrap_id);
                    $this->db->where('request_type','Periodic');
                    $this->db->update('gre_scrap_request',$data);
                    $this->db->where('id', $scrap_id);
                    $this->db->where('request_type','Non-Periodic');
                    $this->db->update('gre_scrap_request',$data1);
                }
                $this->db->select('apartment_id');
                $this->db->distinct('apartment_id');
                $this->db->where_in('id', $scrap_array);
                $query = $this->db->get('gre_scrap_request');
                if ($query->num_rows() > 0) {
                    $data1 = array(
                        'vendor_id' => $vendor_id,
                        'list_created_group' => $list_created_group,
                        'vendor_response' => false
                        );
                    $this->db->insert('gre_vendor_request_status',$data1);
                    $vendor_availability = array(
                                    'vendor_id' => $vendor_id,
                                    'name'      => $vendor_table_query->name,
                                    'login_otp' => $rand_num,
                                    'yes'       => "Yes",
                                    'no'        => "No"
                                    );  
                    $vendor_details = $this->db->select('*')->where('id',$vendor_id)->get('gre_vendors')->row();       
                    $msg = $this->load->view('admin/email/vendor_availability', $vendor_availability,  TRUE);
                    $config = Array(
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'localhost',
                                    'smtp_port' => 25,
                                    'smtp_auth' => FALSE,
                                    'mailtype'  => 'html', 
                                    'charset'   => 'iso-8859-1'
                                );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $message = $msg;
                    $subject = 'Admin assigned Scrap request list';
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($vendor_details->email);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    $this->email->send();
                    $this->session->set_flashdata('message', 'Vendor has been Notified successfully!');
                }
            }
            redirect('scrap-request-admin');
        }
	}
	//assaign scrap request to vendor part is end
    
	//Block  customers part is start
	public function blocked_customer(){
		$block_customer_ids = $this->input->post('myarray');
		$status             = $this->input->post('status');
		$this->vendor_model->blocked_customer($block_customer_ids,$status);
	}
	//Block  customers part is end
    
    //send information to customer part start
    public function send_information(){
        $send_text    = $this->input->post('send_text'); 
        $ids          = $this->input->post('myarray');
        $send_action  = $this->input->post('send_action');
        $flag         = @$this->input->post('flag');
        if (empty($send_text)) {
            $result = 1;
            echo json_encode($result);
            return;
        }
        elseif(empty($send_action)) {
            $result = 2;
            echo json_encode($result);
            return;
        }
        elseif(empty($ids)) {
            $result = 3;
            echo json_encode($result);
            return;
        }
        foreach ($ids as $id){
            if($flag=='user'){
                $user_details = $this->db->select('*')->where('id',$id)->get('users')->row();
            }
            elseif($flag=='manage_order'){
                $user_id = $this->db->select('ord_user_fk')->where('ord_order_number',$id)->get('order_summary')->row()->ord_user_fk;
                $user_details = $this->db->select('*')->where('id',$user_id)->get('users')->row();
            }
            else{
                $user_id = $this->db->select('user_id')->where('id',$id)->get('gre_scrap_request')->row()->user_id;
                $user_details = $this->db->select('*')->where('id',$user_id)->get('users')->row();
            }
            if($send_action == 'SMS' || $send_action == 'Email and SMS'){
                $sms_status = $this->users_model->common_sms_api($user_details->phone, $send_text);
            }
            if($send_action == 'Email' || $send_action == 'Email and SMS'){
                $data_email = array(
                                    'name'          => $user_details->first_name.''.$user_details->last_name,
                                    'send_text'     => $send_text,
                                    'email_for'     => 'user'
                                   );
                $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $message = $this->load->view('public/email/user_information', $data_email,  TRUE);
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($user_details->email);
                $this->email->subject('Important information from admin');
                $this->email->message($message);
                if($user_details->email_verified){
                    $this->email->send();
                }
            }
        }    
        $result = 4;
        echo json_encode($result);
    }
    //send information to customer part end
    
	//send notification to customer for pick up availability response part is start
	public function send_notification(){
		$email_comment = $this->input->post('email_comment'); 
		$scrapids      = $this->input->post('myarray');
		$pick_up_date  = $this->input->post('pick_up_date');
		$pick_up_max_time = $this->input->post('pick_up_max_time');
		$this->vendor_model->periodic_request_archive($scrapids);
		$this->vendor_model->send_notification($scrapids,$pick_up_date, $pick_up_max_time);
		foreach ($scrapids as $value) {
			$scrap_id = $value;
			$scrap_table_query = $this->db->select('*')->where('id',$scrap_id)->get('gre_scrap_request')->row();
			$this->db->select('user_id');
		    $this->db->where('id',$scrap_id);
		    $query = $this->db->get('gre_scrap_request');
		    $user_id = $query->row()->user_id;
			$this->db->select('email,email_verified,first_name,last_name,phone');
		    $this->db->where('id',$user_id);
		    $query_user = $this->db->get('users');
		    $date = explode(" ", $pick_up_date);$time = $date[1]; $date = $date[0];
		    $yes_response = ' For Yes - '.base_url('customer_availability'.'/Yes'.'/'.$scrap_id.'/'.date('Y-m-d', strtotime($date))); 
		    $no_response = ' For No - '.base_url('customer_availability'.'/No'.'/'.$scrap_id.'/'.date('Y-m-d', strtotime($date)));
		    if ($scrap_table_query->customer_presence) {
                $message_text = 'stuff is available at this time. If NO, please inform us by call or Whatsapp on 9804940000 OR send a text message saying "NO."';
            }
            else{
                $message_text = 'your scrap is available at this time. If NO, please inform us by call or Whatsapp on 9804940000 OR send a text message saying "NO"';
            }
            
		    $message = 'Your scheduled pick up on '.$date.' and between this time '.date('H:i:s', strtotime($pick_up_date)).' - '.$pick_up_max_time.'. Please make sure '.$message_text;
            $sms_status = $this->users_model->common_sms_api($query_user->row()->phone,$message);
		    $email = $query_user->row()->email;
		    $email_status = $query_user->row()->email_verified;
    		$data_email = array(
                    'scrap_id'      => $scrap_id,
                    'name'		    => $query_user->row()->first_name.' '.$query_user->row()->last_name,
                    'yes'		    => 'Yes',
                    'no'		    => 'no',
                    'customer_presence' => $scrap_table_query->customer_presence,
                    'request_type'  => $scrap_table_query->request_type,
                    'date'		    => date('Y-m-d H:i:s'),
                    'pick_up_date'  => $pick_up_date,
                    'pick_up_max_time' => $pick_up_max_time,
                    'pick_up_min_time' => date('H:i:s', strtotime($pick_up_date)),
                    'email_comment' => $email_comment,
                    'email_for'		=> 'user'
                );
            if($email_status){
	            $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
	            $msg = $this->load->view('public/email/customer_availability', $data_email,  TRUE);
	            $message = $msg;
	            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
	            $this->email->to($email);
	            $this->email->subject('Scrap pick up schedule');
	            $this->email->message($message);
	            $this->email->send();
	            //mail for admin
	            $data_email = array(
	                 'scrap_id'      => $scrap_id,
                    'name'		    => $query_user->row()->first_name.' '.$query_user->row()->last_name,
                    'yes'		    => 'Yes',
                    'no'		    => 'no',
                    'customer_presence' => $scrap_table_query->customer_presence,
                    'request_type'  => $scrap_table_query->request_type,
                    'user_id'       => $scrap_table_query->user_id,
                    'date'		    => date('Y-m-d H:i:s'),
                    'pick_up_max_time' => $pick_up_max_time,
                    'pick_up_min_time' => date('H:i:s', strtotime($pick_up_date)),
                    'pick_up_date'  => $pick_up_date,
                    'email_comment' => $email_comment,
                    'email_for'		=> 'admin'
                 );
	            $msg1 = $this->load->view('public/email/customer_availability', $data_email,  TRUE);
	            $message1 = $msg1;
            	$admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($admin_mail_id);
	            $this->email->subject('Scrap pick up schedule');
	            $this->email->message($message1);
	            $this->email->send();
			}   
            if ($scrap_table_query->request_type=='Periodic') { 
                $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('scrap_weight' =>'', 'reward_point'=>'', 'scrap_details'=>'', 'vendor_response'=>'', 'request_status'=>''));
            }
		}
	} 

	public function _example_output($output = null)
	{
		$this->load->view('example.php',(array)$output);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// USER AUTHENTICATION
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

		/**
		 * login
		 * login a user
		 */
		function login()
		{
		    if ($this->uri->segment(1) == $this->data['app']['admin_uri'])
			{
			    
			$this->load->library(array('form_validation'));

			//validate form input
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			// log_message("Debug","Before Validation");
			if ($this->form_validation->run() == true)
			{
				// log_message("Debug","After Validation");
				// check for "remember me"
				$remember = (bool) $this->input->post('remember');

				if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember, 'username'))
				{
					// log_message("Debug","After Login");
					//The login is successful

					// Check if the user is not an admin!
					if (!$this->ion_auth->is_admin())
					{
						// log_message("Debug","Is not");
						// Log the admin out
						$this->ion_auth->logout();

						// Let admin know what happened
						$this->flexi_cart->set_error_message('You are not an administrator', 'public', TRUE);
						$this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));

						// Reload login page.
						redirect($this->data['app']['admin_uri'].'/login');
					}

					$this->load->model('users_model');
					$user = $this->users_model->current();
                    
                    $cookie= array(
			           'name'   => 'gre_valid',
			           'value'  => 'ojTUe7kNBYSb3hQkjeV6ZwOKR7YCelBg',
			           'expire' => '86400',
			       );
			 
			        $this->input->set_cookie($cookie);
                    
					// Welcome user by first name.
					$this->flexi_cart_admin->set_status_message('Hi, '.$user->first_name.'. welcome back!', 'public', TRUE);
					$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('public'));

					if ($this->session->userdata('login_redirect'))
					{
						redirect($this->session->userdata('login_redirect'));
					}
					redirect($this->data['app']['admin_uri']);
				}
				else
				{
					// Login was un-successful, set appropriate message
					$this->flexi_cart_admin->set_error_message($this->ion_auth->errors(), 'public', TRUE);
					$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('public'));

					// use redirects instead of loading views for compatibility with MY_Controller libraries
					redirect($this->data['app']['admin_uri'].'/login', 'refresh');
				}
			}

			$this->load->model('users_model');

			$this->data['owner'] = $this->users_model->owner();

			// Get any status message that may have been set.
			$this->data['message'] = $this->session->flashdata('message');

			$this->load->view('admin/login_view', $this->data);
			
			}else{
				redirect('/');
			}
		}

		/**
		 * logout
		 * logout the user
		 */
		function logout()
		{
			// log the user out
			$logout = $this->ion_auth->logout();
			delete_cookie("gre_valid");
			redirect($this->data['app']['admin_uri'].'/login');
		}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// ADMIN DASHBOARD
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * index
	 * View and manage all the available admin functions within flexi cart.
	 */ 
	function index()
	{
		$this->load->helper('text');

		// Get latest users.
		$this->load->library('store');
		$users = $this->store->users(array(
			'ignore_status' => FALSE, // Both active and inactive users
			'order' => 'desc',
			'limit' => 5,
			'select' => array('created_on'),
		));
		$latestUsers = $users['rows'];
		foreach ($latestUsers as $key => $user) {
			$user->created = $this->time_elapsed_string($user->created_on);
		}
		$this->data['latest_users'] = $latestUsers;
		
		//Get latest Scrap Request
		$this->db->select('id,requested_date');
		$this->db->order_by('requested_date', 'DESC');
		$this->db->limit('5');
		$query = $this->db->get('gre_scrap_request');
		$latest_scrap_request = $query->result(); 
		foreach ($latest_scrap_request as $key => $scrap_request) {
			$now = time();
			$scrap_request->requested_on = $this->time_elapsed_string($scrap_request->requested_date);
		}
		$this->data['latest_scrap_request'] = $latest_scrap_request;
		
		//Get total number of redeem_request
		//$this->db->where('status','');
		$total_redeem_request = $this->db->count_all_results('gre_redeem_request');
		$this->data['total_redeem_request'] = $total_redeem_request;
		
		//Get total number of scrap_request
		$this->db->join('users', 'users.id = gre_scrap_request.user_id');
		$total_scrap_request = $this->db->count_all_results('gre_scrap_request');
		log_message('debug',print_r($this->db->last_query(),true));
		$this->data['total_scrap_request'] = $total_scrap_request;
		
		//Get total number of users
		$this->db->where('id != ','1');
		$total_users = $this->db->count_all_results('users');
		$this->data['total_users'] = $total_users;
		
		//Get total number of wishlist
		$total_wishlist = $this->db->count_all_results('gre_wishlist_list');
		$this->data['total_wishlist'] = $total_wishlist;
		
		//Get total number of users product list
		$total_preowned_items = $this->db->count_all_results('gre_my_product_list');
		$this->data['total_preowned_items'] = $total_preowned_items;
		
		//Get total number of vendors
		$this->db->select('*');
		$total_vendors = $this->db->get('gre_vendors');
		$this->data['total_vendors'] = $total_vendors->num_rows();
		//Get latest Redeem Request
		$this->db->select('id,request_date');
		$this->db->where('status','');
		$this->db->order_by('request_date', 'DESC');
		$this->db->limit('5');
		$query = $this->db->get('gre_redeem_request');
		$latest_redeem_request = $query->result(); 
		foreach ($latest_redeem_request as $key => $redeem_request) {
			// log_message('debug',unix_to_human($scrap_request->requested_date));
			$redeem_request->requested_on = $this->time_elapsed_string($redeem_request->request_date);
		}
		$this->data['latest_redeem_request'] = $latest_redeem_request;

		// Get latest products.
		$this->load->model('product_model');
		$this->data['latest_products'] = $this->product_model->get_latest_in_category(3);

		// Get monthly orders revenue
		$this->load->model('dashboard_model');
		$order_data = $this->dashboard_model->get_monthly_revenue(date('Y'), 12);
		$this->data['order_data'] = json_encode($order_data);

		// Get total orders revenue
		$this->data['order_total'] = $this->dashboard_model->get_orders_total();

		// Get total products number
		$this->data['products_total'] = $this->db->count_all('products');

		// Get total users number
		$this->data['users_total'] = $this->data['total_users'];

		$this->load->view('admin/dashboard_view', $this->data);

		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}
    
    function user_email_check($email,$user_id){
        $query  = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows()){
            $query1  = $this->db->get_where('users', array('email' => $email,'id' => $user_id));
            if ($query1->num_rows()){log_message('debug',"asd3");
                return true;
            }
             $this->form_validation->set_message('user_email_check', 'Email is invalid or Already Exists.');
            return false;
        }
        else{
            return true;
        }
    }
    
	function profile()
	{
		$this->load->model('users_model');
		$this->load->library(array('ion_auth', 'form_validation'));

		// if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		// {
		// 	redirect('auth', 'refresh');
		// }

		$this->data['user'] = $this->users_model->owner();

		// validate form input
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('company_name', 'company name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|callback_user_email_check['.$this->data['user']->id.']');
		$this->form_validation->set_rules('company_phone', 'Phone Number', 'required');
		$this->form_validation->set_rules('old_password', 'Old password', 'required|callback_password_check');

		if ($this->input->post('edit_user'))
		{
			// Additional rules.
			// Rules for the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}


			if ($this->form_validation->run() === TRUE)
			{
				$user_data = array(
					'username' => $this->input->post('username'),
					'first_name' => $this->input->post('company_name'),
					'email'	=> $this->input->post('email'),
					'phone' => $this->input->post('company_phone'),
					'postal' => $this->input->post('company_p_o_box'),
					'address' => $this->input->post('company_address'),
				);

				if ($_FILES['userfile']['size'] > 0)
				{
					// Get the image name.
					$filename = $this->data['user']->logo;
					$avatar = $this->upload_image($filename);

					if ( ! $avatar['error'])
					{
						$user_data['avatar'] = $avatar['path'];
					}
				}

				// update the password if it was posted
				if ($this->input->post('password'))
					$user_data['password'] = $this->input->post('password');

				// check to see if we are updating the user
			   if($this->ion_auth->update($this->data['user']->id, $user_data))
			    {
					// Set a success message.
					$this->flexi_cart_admin->set_status_message($this->ion_auth->messages(), 'admin', TRUE);
					$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
			    }
			    else
			    {log_message('debug','vikasa111');
					// Set an error message.
				// 	$this->flexi_cart_admin->set_error_message($this->ion_auth->errors(), 'admin', TRUE);
				// 	$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
				 $this->session->set_flashdata(
                            array('type' => 'danger', 'message' => 'Profile cannot be update.')
                        );
			    }
				redirect('admin/profile', 'refresh');
			}
		}

		$this->load->view('admin/user_profile', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// USER MANAGEMENT
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * View and manage store items and categories
	*/
	function users()
	{
		if ($this->input->post('activate_user'))
		{
			if ($this->input->is_ajax_request())
			{
				if ($this->ion_auth->activate($this->input->post('id')))
				{
					echo json_encode(array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
					return TRUE;
				}
				else
				{
					echo json_encode(array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors() ? $this->ion_auth->errors() : 'Something went wrong'
					));
					return FALSE;
				}
			}
			else
			{
				if ($this->ion_auth->activate($this->input->post('id')))
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
				}
				else
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors()
					));
				}
				redirect(current_url());
			}
		}

		if ($this->input->post('deactivate_user'))
		{
			if ($this->input->is_ajax_request())
			{
				if ($this->ion_auth->deactivate($this->input->post('id')))
				{
					echo json_encode(array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
					return TRUE;
				}
				else
				{
					echo json_encode(array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors() ? $this->ion_auth->errors() : 'Something went wrong'
					));
					return FALSE;
				}
			}
			else
			{
				if ($this->ion_auth->deactivate($this->input->post('id')))
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
				}
				else
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors()
					));
				}
				redirect(current_url());
			}
		}

		// Delete multiple selected users
		if ($this->input->post('delete_selected'))
		{
			if ($selected = $this->input->post('selected'))
			{
				foreach ($selected as $key => $id)
				{
					$this->ion_auth->delete_user($id);
				}

				if ($this->ion_auth->messages())
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
				}
				else
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors() ? $this->ion_auth->errors() : 'Something went wrong'
					));
				}
				redirect(current_url());
			}
			else
			{
				$this->session->set_flashdata('alert', array(
					'type' => 'danger',
					'message' => 'Nothing was selected'
				));
			}
		}

		if ($this->input->post('insert_user'))
		{
			$response = $this->healthcare_model->add_company_doctor();
			
			if ($this->input->is_ajax_request())
			{
				echo json_encode($response['alert']);
				return TRUE;
			}
			else
			{
				$this->session->set_flashdata('alert', $response['alert']);
				redirect(current_url());
			}
		}

		if ($this->input->post('remove_user'))
		{
			$response = $this->healthcare_model->remove_company_doctor();
			
			if ($this->input->is_ajax_request())
			{
				echo json_encode($response['alert']);
				return TRUE;
			}
			else
			{
				$this->session->set_flashdata('alert', $response['alert']);
				redirect(current_url());
			}
		}

		if ($this->input->post('delete_user'))
		{
			if ($this->input->is_ajax_request())
			{
				if ($this->ion_auth->delete_user($this->input->post('id')))
				{
					echo json_encode(array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
					return TRUE;
				}
				else
				{
					echo json_encode(array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors() ? $this->ion_auth->errors() : 'Something went wrong'
					));
					return FALSE;
				}
			}
			else
			{
				if ($this->ion_auth->delete_user($this->input->post('id')))
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'success',
						'message' => $this->ion_auth->messages()
					));
				}
				else
				{
					$this->session->set_flashdata('alert', array(
						'type' => 'danger',
						'message' => $this->ion_auth->errors() ? $this->ion_auth->errors() : 'Something went wrong'
					));
				}
				redirect(current_url());
			}
		}

		// Get users.
		$this->load->library('store');

		$users = $this->store->users(array(
			'ignore_status' => TRUE // Both active and inactive users
		));

		$this->data['users'] = $users['rows'];	
		$this->data['users_total'] = $users['total'];	
		$this->data['pagination'] = $users['pagination'];
		
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');

		$this->load->view('admin/users/users_view', $this->data);
	}
	
	/**
	 * Update user specific data
	 */
	function update_user($user_id)
	{
		$this->load->model('users_model');
		// Variable "person" is used because "user" is already being used for currently signed in user.
		$this->data['person']  = $this->users_model->get_details($user_id);
		$this->load->library('form_validation');

		if ($this->input->post('edit_user'))
		{
			$tables = $this->config->item('tables','ion_auth');
			$identity_column = $this->config->item('identity','ion_auth');
			$this->data['identity_column'] = $identity_column;
			// validate form input
			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}
			// validate form input
			//$this->form_validation->set_rules('username', 'above', 'required');
			$this->form_validation->set_rules('email', 'above', 'required|valid_email');
			//$this->form_validation->set_rules('address', 'above', 'required');
			$this->form_validation->set_rules('phone', 'above', 'required|trim');

			// Add data for different user groups.
			//$this->form_validation->set_rules('first_name', 'above', 'required');
			//$this->form_validation->set_rules('last_name', 'above', 'required');

			if ($this->form_validation->run() == true)
			{
				$email    = strtolower($this->input->post('email'));
				$identity = $this->input->post('username');
				$password = $this->input->post('password');

				$profile = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'address' => $this->input->post('address'),
					'phone' => $this->input->post('phone'),
					'postal' => $this->input->post('postal'),
				);
				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$profile['password'] = $this->input->post('password');
				}
				// update the address if it was posted
				if ($this->input->post('country'))
				{
					$data = array(
						'country'  => $this->input->post('country'),
						'state'    =>$this->input->post('state'),
						'city'     =>$this->input->post('city'),
						'locality' =>$this->input->post('locality'),
						'apartment_name' =>$this->input->post('apartment_name'),
						'flat_no'  =>$this->input->post('flat_no')
								   );
					$this->db->where('user_id',$user_id);
        			$this->db->update('gre_address',$data);

				}

				$group = array('2'); // Sets user to public.

				if ($_FILES['userfile']['size'] > 0)
				{
					$filename = $this->data['person']->avatar;
					$avatar = $this->upload_image($filename);
					if ( ! $avatar['error'])
						$profile['avatar'] = $avatar['path'];
				}

				if ($this->ion_auth->update($user_id, $profile))
				{
					$this->flexi_cart_admin->set_status_message($this->ion_auth->messages() ? $this->ion_auth->messages() : 'User Data was updated.', 'admin', TRUE);
				}
				else
				{
					$this->flexi_cart_admin->set_error_message($this->ion_auth->errors() ? $this->ion_auth->errors() : 'User Data was could not be updated.', 'admin', TRUE);
				}

				// Set messages and redirect back to user update page.
				$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
				redirect(current_url());
			}
		}

		$this->load->model('demo_cart_admin_model');

		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('convert_reward_points'))
		{
			$this->demo_cart_admin_model->demo_convert_reward_points($user_id);
			// Set a message to the CI flashdata so that it is available after the page redirect.
			$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages());
			redirect(current_url());
		}

		// Get an array of all the reward vouchers filtered by the id in the url.
		// Using flexi cart SQL functions, join the demo user table with the discount table.
		$sql_where = array($this->flexi_cart_admin->db_column('discounts', 'user') => $user_id);
		// $this->flexi_cart_admin->sql_join('demo_users', 'user_id = '.$this->flexi_cart_admin->db_column('discounts', 'user'));
		$this->data['voucher_data_array'] = $this->flexi_cart_admin->get_db_voucher_query(FALSE, $sql_where)->result_array();

		// Get user remaining reward points.
		$summary_data = $this->flexi_cart_admin->get_db_reward_point_summary($user_id);
		$this->data['points_data'] = array(
			'total_points' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points')],
			'total_points_pending' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_pending')],
			'total_points_active' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')],
			'total_points_active' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')],
			'total_points_expired' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_expired')],
			'total_points_converted' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_converted')],
			'total_points_cancelled' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_cancelled')]
		);

		// Get an array of a demo user and their related reward points from a custom demo model function, filtered by the id in the url.
		$reward_data = $this->demo_cart_admin_model->demo_reward_point_summary($user_id);
		
		// Note: The custom function returns a multi-dimensional array, of which we only need the first array, so get the first row '$user_data[0]'.
		$this->data['reward_data'] = $reward_data[0];

		// Get the conversion tier values for converting reward points to vouchers.
		$conversion_tiers = $this->data['reward_data'][$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')];
		$this->data['conversion_tiers'] = $this->flexi_cart_admin->get_reward_point_conversion_tiers($conversion_tiers);

		// Get an array of all reward points for a user.
		$sql_select = array(
			$this->flexi_cart_admin->db_column('reward_points', 'order_number'),
			$this->flexi_cart_admin->db_column('reward_points', 'description'),
			$this->flexi_cart_admin->db_column('reward_points', 'order_date')
		);	
		$sql_where = array($this->flexi_cart_admin->db_column('reward_points', 'user') => $user_id);
		$this->data['points_awarded_data'] = $this->flexi_cart_admin->get_db_reward_points_query($sql_select, $sql_where)->result_array();
		
		// Call a custom function that returns a nested array of reward voucher codes and the reward point data used to create the voucher.
		$this->data['points_converted_data'] = $this->demo_cart_admin_model->demo_converted_reward_point_history($user_id);
		/* user Reward and redeem point history part start*/
		$this->db->select('order_id,scrap_request_id, redeem_request_id,credit_reward_point,debit_reward_point,date');
     	$this->db->where('user_id',$user_id);
        $this->db->order_by('date','desc');
     	$query = $this->db->get('gre_reward_point');	
        $result = $query->result();
        $this->data['reward_points'] = $query->result();
        $this->data['user_details'] = $this->users_model->user_details($user_id);
        $this->data['redeem']  = $this->users_model->redeem_request_details($user_id);
        $this->data['user_address'] = $this->users_model->users_address($user_id);
		/* user Reward and redeem point history part end*/

		/* user scrap request part start*/
    	$this->data['periodic']     = $this->users_model->scrap_order_list($user_id);
        $this->data['non_periodic'] = $this->users_model->scrap_order_list1($user_id);
        /* user scrap request part end*/

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
        
        $products = $this->users_model->get_user_product($user_id);
        foreach ($products as $item)
        {
            $category = $this->users_model->get_product_category($item->id);
            $cat_name = $this->db->get_where('gre_my_product_categories', array('id' => $category->category_id))->row()->name;
            $item->category = $cat_name;    
        }
        $this->data['products'] = $products;
        
        $this->load->model('users_wishlist_model');
        $wishlists = $this->users_wishlist_model->get_user_wishlist($user_id);
        foreach ($wishlists as $item)
        {
            $category = $this->users_wishlist_model->get_wishlist_category($item->id);
            $cat_name = $this->db->get_where('gre_wishlist_categories', array('id' => $category->category_id))->row()->name;
            $item->category = $cat_name;    
        }
        $this->data['wishlists'] = $wishlists;
        
        $this->flexi_cart_admin->sql_where($this->flexi_cart_admin->db_column('order_summary', 'user'), $user_id);
        $this->flexi_cart_admin->sql_order_by($this->flexi_cart_admin->db_column('order_summary', 'date'), 'desc');
        $this->data['order_data'] = $this->flexi_cart_admin->get_db_order_query()->result_array();
        
		$this->load->view('admin/users/user_update_view', $this->data);
	}
	function admin_scrap_details_view($scrap_id){
    	$this->db->select('user_id');
    	$this->db->where('id',$scrap_id);
    	$this->db->where('request_type','Periodic');
    	$query   = $this->db->get('gre_scrap_request');
    	$user_id = $query->row()->user_id;
    	$data['scrap']  = $this->users_model->scrap_details_view($scrap_id);
        $data['result'] = $this->users_model->archive_history($scrap_id,$user_id);
        $data['user']   = $this->users_model->user_details($user_id);
    	$this->load->view('admin/users/scrap_details_view',$data);
    }
	/**
	 * insert_user
	 * Register a new user
	 */
	function insert_user()
	{
		if ($this->input->post('create_user'))
		{
			$tables = $this->config->item('tables','ion_auth');
			$identity_column = $this->config->item('identity','ion_auth');
			$this->data['identity_column'] = $identity_column;

			$this->load->library('form_validation');

			// validate form input
			$this->form_validation->set_rules('username', 'above', 'required|is_unique['.$tables['users'].'.'.$identity_column.']');
			$this->form_validation->set_rules('password_confirm', 'above', 'required');
			$this->form_validation->set_rules('password', 'above', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
			$this->form_validation->set_rules('email', 'above', 'required|valid_email');
			$this->form_validation->set_rules('address', 'above', 'required');
			$this->form_validation->set_rules('phone', 'above', 'required|trim');

			// Add data for different user groups.
			$this->form_validation->set_rules('first_name', 'above', 'required');
			$this->form_validation->set_rules('last_name', 'above', 'required');

			if ($this->form_validation->run() == true)
			{
				$email    = strtolower($this->input->post('email'));
				$identity = $this->input->post('username');
				$password = $this->input->post('password');

				$profile = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'address' => $this->input->post('address'),
					'phone' => $this->input->post('phone'),
				);

				$group = array('2'); // Sets user to public.

				if ($_FILES['userfile']['size'] > 0)
				{
					$avatar = $this->upload_image();
					if ( ! $avatar['error'])
						$profile['avatar'] = $avatar['path'];
				}

				$user_id = $this->ion_auth->register($identity, $password, $email, $profile, $group);

				if ($user_id)
				{
					// Define profile information.
					// $this->users_model->add($profile);

					$this->flexi_cart_admin->set_status_message('User has been added', 'admin', TRUE);
					$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));

					// redirect them back to the login page
					redirect("admin/users", 'refresh');
				}
				else
				{
					$this->flexi_cart_admin->set_error_message(($this->ion_auth->errors() ? $this->ion_auth->errors() : 'User could not be added'), 'admin', TRUE);
					$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));

					// redirect them back to the login page
					redirect("admin/insert_user", 'refresh');
				}
			}
		}

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');

		// display the create user form
		$this->load->view('admin/users/user_insert', $this->data);
	}




	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// PRODUCT MANAGEMENT
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * View and manage store items and categories
	 */
	function items()
	{
		$this->load->model('product_model');

		// Get products.
		$this->load->library('store');
		$products = $this->store->products($options = array(
			'order' => 'alpha'
		));
		$this->data['products'] = $products['rows'];	
		$this->data['products_total'] = $products['total'];	
		$this->data['pagination'] = $products['pagination'];
		
		// Deleting product(s).
		if ($this->input->post('delete_selected'))
		{
			$this->load->library('form_validation');
			// Set validation rules.
			$this->form_validation->set_rules('selected[]', 'above', 'required');
			$this->form_validation->set_message('required', 'Select some items first.');

			if ($this->form_validation->run())
			{
				foreach ($this->input->post('selected') as $key => $id)
				{
					// Deleting each of the selected products.
					$this->product_model->delete_product($id);
				}
					// Reload the page.
				redirect(current_url(), 'refresh');
			}
		}
		
		// Update product(s).
		if ($this->input->post('update_items'))
		{
			// Deleting each of the selected products.
			$this->product_model->update_products();
			// Reload the page.
			redirect(current_url(), 'refresh');
		}

		$this->data['categories'] = $this->category_model->get_categories_list();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/items_view', $this->data);
	}

	/**
	 * Insert a new item
	 */
	function insert_item()
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model', 'category_model', 'tree_model'));

		// $this->data['category'] = $this->category_model->get_category($category_id);
		
		if ($this->input->post('add_item'))
		{
			$this->load->library('form_validation');

			// Set validation rules.
			$this->form_validation->set_rules('name', 'above', 'required');
			$this->form_validation->set_rules('price', 'above', 'required');
			$this->form_validation->set_rules('stock', 'above', 'required');
			//$this->form_validation->set_rules('weight', 'above', 'required');
			//$this->form_validation->set_rules('description', 'above', 'required');
			$this->form_validation->set_rules('userfile', 'above', 'callback_userfile_check');

			$this->form_validation->set_error_delimiters('', '');

			if ($this->form_validation->run())
			{
				// Form passed validation. Add the product.
				if ($id = $this->product_model->add_product())
				{
					redirect('admin/items/'.$id);
				}
				else
				{
					redirect('admin/add_item');
				}
			}
		}
		$this->data['categories'] = $this->category_model->get_categories_list();
		
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_add_view', $this->data);
	}

	/**
	 * Delete an item
	 */
	function delete_item($id)
	{
		$this->load->model(array('product_model'));
		
		// Form passed validation. Add the product.
		if ($this->product_model->delete_product($id))
		{
			redirect('admin/items');
		}
		else
		{
			redirect(current_url());
		}
	}

	/**
	 * Update an item
	 */
	function update_item($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model', 'category_model'));

		$this->data['product'] = $this->product_model->get_details($id);
		$this->data['categories'] = $this->category_model->get_categories_list();

		if ($this->input->post('update_item'))
		{
			if (count($_FILES['files']['name']) > $this->data['upload_limit'])
			{
				$this->flexi_cart_admin->set_error_message('You can only upload '.$this->data['upload_limit'].' images', 'admin', TRUE);
				$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
				$this->session->set_flashdata('alert', array('location' => 'images'));
				redirect(current_url());
			}

			$this->load->library('form_validation');

			// Set validation rules.
			$this->form_validation->set_rules('name', 'above', 'required');
			$this->form_validation->set_rules('price', 'above', 'required');
			$this->form_validation->set_rules('stock', 'above', 'required');
			//$this->form_validation->set_rules('weight', 'above', 'required');
			//$this->form_validation->set_rules('description', 'above', 'required');

			$this->form_validation->set_error_delimiters('', '');

			if ($this->form_validation->run())
			{
				// Form passed validation. Update the product.
				$response = $this->product_model->update_product($this->input->post('id'));
				
				// Update tax rates
				$this->load->model('demo_cart_admin_model');
				$this->demo_cart_admin_model->demo_update_item_tax();
				
				redirect(current_url());
			}
		}

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_details', $this->data);		
	}

	/**
	 * Update an item attributes
	 */
	function update_item_attributes($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model', 'category_model'));

		$this->data['product'] = $this->product_model->get_details($id);

		if ($this->input->post('update_attributes'))
		{
			$this->product_model->update_product_attributes($this->input->post('id'), $this->input->post('attributes'));
			redirect(current_url());
		}

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_attributes', $this->data);		
	}

	/**
	 * Update an item options
	 */
	function update_item_options($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model('product_model');

		if ($this->input->post('add_option'))
		{
			$this->product_model->add_product_option($this->input->post('id'));
			redirect(current_url());
		}

		if ($this->input->post('update_options'))
		{
			$this->product_model->update_product($this->input->post('id'));
			redirect(current_url());
		}

		if ($this->input->post('update_defaults'))
		{
			$this->product_model->update_product_defaults($this->input->post('id'));
			redirect(current_url());
		}

		$this->data['product'] = $this->product_model->get_details($id);
		$this->data['product_options']  = $this->product_model->get_product_options($id);
		$this->data['product_variants'] = $this->product_model->get_product_variants($id);
		$this->data['product_defaults'] = $this->product_model->get_product_defaults($id);

		$this->load->library('store');
		$this->data['upload_limit'] = $this->store->settings('upload_limit') - count($this->data['product']->images);

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_options', $this->data);		
	}

	/**
	 * Update an item option
	 */
	function update_item_option($product_id, $option_id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model('product_model');

		if ($this->input->post('update_option'))
		{
			$this->load->library('form_validation');

			// Set validation rules.
			$this->form_validation->set_rules('values[]', 'Select Attributes', 'required');
			$this->form_validation->set_error_delimiters('', '');

			if ($this->form_validation->run())
			{
				$this->product_model->update_product_option($option_id);
				redirect(current_url());
			}
		}

		if ($this->input->post('delete_item_image'))
		{
			$this->product_model->delete_product_option_image($this->input->post('id'));
			exit();
		}

		$this->data['product'] = $this->product_model->get_details($product_id);
		$this->data['product_option']  = $this->product_model->get_product_option($option_id);
		$this->data['product_options']  = $this->product_model->get_product_options($product_id);

		$this->load->library('store');
		$this->data['upload_limit'] = $this->store->settings('upload_limit') - count($this->data['product_option']->images);

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_option', $this->data);		
	}

	/**
	 * Update an item options
	 */
	function delete_item_options($product_id, $option_id)
	{
		$this->load->model('product_model');

		$this->product_model->delete_product_option($option_id);
		redirect('admin/update_item_options/'.$product_id);
	}

	/**
	 * Update an item images
	 */
	function update_item_images($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model'));

		if ($this->input->post('delete_item_image'))
		{
			$this->product_model->delete_product_image($this->input->post('id'));
			exit();
		}

		$this->data['product'] = $this->product_model->get_details($id);

		$this->load->library('store');
		$this->data['upload_limit'] = $this->store->settings('upload_limit') - count($this->data['product']->images);

		if ($this->input->post('update_images'))
		{
			if (count($_FILES['files']['name']) > $this->data['upload_limit'])
			{
				$this->flexi_cart_admin->set_error_message('You can only upload '.$this->data['upload_limit'].' images', 'admin', TRUE);
				$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
				$this->session->set_flashdata('alert', array('location' => 'images'));
				redirect(current_url());
			}

			$this->product_model->update_product_images($this->input->post('id'));
			redirect(current_url());
		}

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_images', $this->data);		
	}

	/**
	 * Update an item tax
	 */
	function update_item_tax($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model'));

		$this->data['product'] = $this->product_model->get_details($id);

		if ($this->input->post('insert_tax'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_item_tax($id);
			redirect(current_url());
		}

		if ($this->input->post('update_tax'))
		{
			// Update tax rates
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_item_tax();
			$this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
			redirect(current_url());
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all tax zones.
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');

		// Get an array of all the item tax rates filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('item_tax', 'item') => $id);
		$this->data['item_tax_data'] = $this->flexi_cart_admin->get_db_item_tax_query(FALSE, $sql_where)->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_tax', $this->data);		
	}

	/**
	 * Insert an item shipping rules
	 */
	function insert_item_shipping($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model'));

		$this->data['product'] = $this->product_model->get_details($id);

		if ($this->input->post('insert_shipping'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_item_shipping($id);
			redirect('admin/update_item_shipping/'.$id);
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all shipping zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');

		// Get the row array of the demo item filtered by the id in the url.
		$sql_where = array('id' => $id);
		$this->data['item_data'] = $this->flexi_cart_admin->get_db_table_data_query('products', FALSE, $sql_where)->result_array();
		if(!empty($this->data['item_data'])) $this->data['item_data'] = $this->data['item_data'][0];

		// Get an array of all item shipping rules filtered by the id in the url.		
		$sql_where = array($this->flexi_cart_admin->db_column('item_shipping', 'item') => $id);
		$this->data['item_shipping_data'] = $this->flexi_cart_admin->get_db_item_shipping_query(FALSE, $sql_where)->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_insert_shipping', $this->data);		
	}

	/**
	 * Update an item shipping
	 */
	function update_item_shipping($id)
	{
		$this->load->config('app');
		$this->data['app'] = $this->config->item('app');

		$this->load->model(array('product_model'));

		$this->data['product'] = $this->product_model->get_details($id);

		if ($this->input->post('update_shipping'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_item_shipping();
			redirect(current_url());
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all shipping zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');

		// Get the row array of the demo item filtered by the id in the url.
		$sql_where = array('id' => $id);
		$this->data['item_data'] = $this->flexi_cart_admin->get_db_table_data_query('products', FALSE, $sql_where)->result_array();
		if (!empty($this->data['item_data'])) $this->data['item_data'] = $this->data['item_data'][0];

		// Get an array of all item shipping rules filtered by the id in the url.		
		$sql_where = array($this->flexi_cart_admin->db_column('item_shipping', 'item') => $id);
		$this->data['item_shipping_data'] = $this->flexi_cart_admin->get_db_item_shipping_query(FALSE, $sql_where)->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/items/item_update_shipping', $this->data);		
	}

	/**
	 * View and edit item categories
	*/
	function categories($category_id = NULL)
	{
		$this->load->model(array('category_model'));

		if ($this->input->post('delete'))
		{
			$response = $this->category_model->delete_category();
			redirect('admin/categories/'.$category_id);
		}

		if ($this->input->post('update_categories'))
		{
			$response = $this->category_model->update_categories();
			redirect('admin/categories/'.$category_id);
			
		}

		// Load the categories page.
		$this->data['categories'] = $this->category_model->get_all_categories();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/categories/categories_view', $this->data);	
	}

	/**
	 * Add a new item category
	*/
	public function insert_category($category_id = NULL)
	{
		$this->load->model(array('product_model', 'category_model'));

		if ($this->input->post('add_category'))
		{
			$this->load->library('form_validation');

			// Set validation rules.
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('name', 'Category Name', 'required|is_unique[product_categories.name]');

			if ($this->form_validation->run() == true)
			{
				$response = $this->category_model->add_category();
				$this->session->set_flashdata('alert', $response['alert']);
				
				if ( ! $response['error'])
				{
					redirect('admin/categories/'.$category_id);
				}
			}
		}

		// Load the categories page.
		$this->data['category'] = $this->category_model->get_categories($category_id);
		$this->data['categories'] = $this->category_model->get_all_categories();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/categories/category_insert_view', $this->data);
	}

	public function update_attribute($category_id = NULL)
	{
		$this->load->model(array('product_model', 'category_model'));

		if ($this->input->post('delete_attribute'))
		{
			foreach ($this->input->post('delete_attribute') as $key => $attr)
			{
				$this->category_model->delete_attribute($key);
				redirect(current_url());
			}
		}

		if ($this->input->post('update_category'))
		{
			$this->category_model->update_attributes();
			redirect(current_url());
		}

		if ($this->input->post('add_attributes'))
		{
			$this->category_model->add_attributes($category_id);
			redirect(current_url());
		}

		// Load the categories page.
		$this->data['category'] = $this->category_model->get_categories($category_id);
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('admin/categories/attribute_update_view', $this->data);
	}

	public function get_attributes($id = NULL)
	{
		$attributes = $this->Category_model->get_attributes($id, FALSE, TRUE);
		
		// For ajax calls, return a template.
		if ($this->input->is_ajax_request())
		{
			if ($attributes)
			{
				// Load category tiles template.
				$this->load->view(
					'admin/categories/attribute_tiles',
					array(
						'template' => 'form',
						'attributes' => $attributes
					)
				);
			}
		}
		else
		{
			show_404();
		}
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// CUSTOM ITEM TABLE
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * items
	 * Whilst flexi cart takes care of online ordering, shipping rates, tax rates, discounts and currencies, it leaves the database structure for item and category 
	 * tables completely up to the design of the developer.
	 * For the purposes of demonstrating some of flexi carts features, a demo item, and category table have been included that are then linked to some of the cart functions.
	 */ 
	// function items()
	// {
	// 	$this->load->model('demo_cart_admin_model');
		
	// 	// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
	// 	if ($this->input->post('update_items'))
	// 	{
	// 		$this->demo_cart_admin_model->demo_update_item_stock();
	// 	}

	// 	// Get an array of all demo items from a custom demo model function.
	// 	$this->data['item_data'] = $this->demo_cart_admin_model->demo_get_item_data();
		
	// 	// Get any status message that may have been set.
	// 	$this->data['message'] = $this->session->flashdata('message');	

	// 	$this->load->view('admin/items/items_view', $this->data);
	// }	
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// ORDER MANAGEMENT
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * orders
	 * View and manage customer orders that have been saved by flexi cart
	 */ 
	function orders(){
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('order_summary');
		$crud->set_relation('ord_status','order_status','ord_status_description');
		$crud->columns('ord_order_number','ord_user_fk','ord_demo_ship_name','ord_demo_email','ord_demo_phone','ord_demo_ship_city','ord_demo_ship_address_02','ord_demo_ship_address_01','ord_total_items','ord_item_summary_total','ord_total','ord_date','ord_status');
		$crud->set_subject("Manage User's Order");
		$crud->display_as('ord_order_number','Order Number')->display_as('ord_user_fk','Customer ID')->display_as('ord_demo_email','Customer Email')->display_as('ord_demo_phone','Customer Phone')->display_as('ord_total_items','Total Items')->display_as('ord_total','Grand Total')->display_as('ord_date','Ordered Date')->display_as('ord_status','Order Status')->display_as('ord_demo_ship_city','City')->display_as('ord_demo_ship_address_02','Locality')->display_as('ord_demo_ship_address_01','Apartment Name')->display_as('ord_item_summary_total','Total');
        $crud->callback_column('ord_date',array($this,'_callback_date'));
        $crud->order_by('ord_date','desc');
        $crud->add_action('View', '', '','ui-icon-image',array($this,'manage_orders_view'));
        $crud->callback_before_delete(array($this,'delete_order_details'));
	    $crud->unset_clone();
	    $crud->unset_edit();
	    $crud->unset_add();
	    $crud->unset_read();
		$output = $crud->render();
	    $this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/orders/manage_orders_view.php',(array)$output);
	}
    
    public function manage_orders_view($primary_key , $product){
		return site_url('admin/order_details/'.$product->ord_order_number);
	}
	
// 	public function _callback_order_status($value, $row)
//     {
//         //log_message('debug', 'user_id : '.$value);
//         //log_message('debug', 'user_id : '.print_r($row,true));
        
//         $this->db->select('first_name,last_name');
//         $this->db->where('id',$value);
//         $query = $this->db->get('users');
//         $user = $query->row();
        
//         return "<a target='_blank' href='".site_url('update_user/'.$value)."'>".$user->first_name." ".$user->last_name."</a>";
//     }
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * order_details
	 * Displays all data related to a saved order, including the users billing and shipping details, the cart contents and the cart summary.
	 * This demo includes an example of indicating to flexi cart which items have been shipped or cancelled since the order was receieved, flexi cart can then use this data 
	 * to manage item stock and user reward points.
	 */ 
	function order_details($order_number) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_order'))
		{   
            $update_status = $this->input->post('update_status');
            $order_details = $this->db->select('*')->where('ord_order_number',$order_number)->get('order_summary')->row();
            $prev_order_status = $order_details->ord_status;
            $user_id = $order_details->ord_user_fk;
            $this->load->model('demo_cart_admin_model');
            $this->demo_cart_admin_model->demo_update_order_details($order_number);
		    if($update_status){
                $new_order_status = $update_status;            
                if ($new_order_status != $prev_order_status) {
                    $order_status = '';
                    switch ($new_order_status) {
                        case 1:
                            $order_status = 'Awaiting Payment';
                            $new_order_status1 = 'Awaiting Payment';
                            break;
                        case 2:
                            $order_status = 'New Order';
                            $new_order_status1 = 'New Order';
                            break;
                        case 3:
                            $order_status = 'Processing Order';
                            $new_order_status1 = 'Processing Order';
                            break;
                        case 4:
                            $order_status = 'Order Complete';
                            $new_order_status1 = 'Order Complete';
                            break;
                        case 5:
                            $order_status = 'Order Cancelled';
                            $new_order_status1 = 'Order Cancelled';
                            break;
                    }
                    $query_user = $this->db->select('*')->where('id',$user_id)->get('users')->row();
                    // for adjusting total revenue and user wallet cash when status becomes cancel
                    if($new_order_status == 5){
                        $refund_data = $this->flexi_cart_admin->get_refund_summary_query($order_number)->result_array();
                        $refund_amount = $refund_data[0]['ord_det_price'];
                        $order_details = $this->db->select('*')->where('ord_order_number' , $order_number)->get('order_summary')->row();
                        $order_balance_due = $order_details->ord_det_cash_collect;
                        $ord_det_wallet_cash_debited = $order_details->ord_det_wallet_cash_debited;
                        $order_total_cash  = $order_details->ord_item_summary_total;
                        $user_wallet_cash  = $this->db->select('total_reward_point')->where('id',$user_id)->get('users')->row()->total_reward_point;
                        $credit_reward_point = 0;
                        if($order_balance_due == 0 || empty($order_balance_due)){
                            $user_wallet_cash += $refund_amount;
                            $credit_reward_point = $refund_amount;
                        }
                        elseif($ord_det_wallet_cash_debited == 0 || empty($ord_det_wallet_cash_debited)){
                            $user_wallet_cash += 0;
                        }
                        else{
                            $refund_amount = $refund_amount-$order_balance_due;
                            if ($refund_amount>0) {
                                $credit_reward_point = $refund_amount;
                                $user_wallet_cash += $refund_amount;
                            }
                        }
                        $this->db->where('id',$user_id)->update('users',array('total_reward_point'=>$user_wallet_cash));
                        $this->db->where('ord_order_number',$order_number)->update('order_summary',array('ord_total'=>0));
                        $data = array(  'order_id'            => $order_number,
                                        'credit_reward_point' => $credit_reward_point,
                                        'user_id'             => $user_id
                                      );
                        $this->db->insert('gre_reward_point',$data);
                    }
                    // for adjusting total revenue and user wallet cash when status becomes complete
                    if($new_order_status == 4){
                        $refund_data = $this->flexi_cart_admin->get_refund_summary_query($order_number)->result_array();
                        $refund_amount = $refund_data[0]['ord_det_price'];
                        $order_details = $this->db->select('*')->where('ord_order_number' , $order_number)->get('order_summary')->row();
                        $order_balance_due = $order_details->ord_det_cash_collect;
                        $ord_det_wallet_cash_debited = $order_details->ord_det_wallet_cash_debited;
                        $order_total_cash  = $order_details->ord_item_summary_total;
                        $user_wallet_cash  = $this->db->select('total_reward_point')->where('id',$user_id)->get('users')->row()->total_reward_point;
                        $credit_reward_point = 0;
                        $order_total_cash  = $order_total_cash-$refund_amount;
                        if($order_balance_due == 0 || empty($order_balance_due)){
                            $user_wallet_cash += $refund_amount;
                            $credit_reward_point = $refund_amount;
                        }
                        elseif($ord_det_wallet_cash_debited == 0 || empty($ord_det_wallet_cash_debited)){
                            $user_wallet_cash += 0;
                        }
                        else{
                            $refund_amount = $refund_amount-$order_balance_due;
                            if ($refund_amount>0) {
                                $credit_reward_point = $refund_amount;
                                $user_wallet_cash += $refund_amount;
                            }
                        }
                        $this->db->where('id',$user_id)->update('users',array('total_reward_point'=>$user_wallet_cash));
                        $this->db->where('ord_order_number',$order_number)->update('order_summary',array('ord_total'=>$order_total_cash));
                        $data = array(  'order_id'            => $order_number,
                                        'credit_reward_point' => $credit_reward_point,
                                        'user_id'             => $user_id
                                     );
                        $this->db->insert('gre_reward_point',$data);
                    }

                    if ($query_user->email_verified){
                        $email = $query_user->email;
                        $name  = $query_user->first_name.' '.$query_user->last_name;
                        $data_email = array(
                                'username'    => $name,
                                'order_id'    => $order_number,
                                'ord_status'  => $new_order_status1
                            );
                        $config = Array(
                                    'protocol'  => 'smtp',
                                    'smtp_host' => 'localhost',
                                    'smtp_port' => 25,
                                    'smtp_auth' => FALSE,
                                    'mailtype'  => 'html', 
                                    'charset'   => 'iso-8859-1'
                                );
                        $this->load->library('email', $config);
                        $this->email->set_newline("\r\n");
                        $msg = $this->load->view('public/email/order_status_email', $data_email,  TRUE);
                        $message = $msg;
                        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                        $this->email->to($email);
                        $this->email->subject('Your order status is changed ('.$order_number.')');
                        $this->email->message($message);
                        $this->email->send();
                    }
                    $message = 'Your Order status is changed. Current order status : '.$new_order_status1.'. Your Order Id is '.$order_number;
                    $sms_status = $this->users_model->common_sms_api($query_user->phone, $message);
                }
			}
            // $this->load->model('demo_cart_admin_model');
            // $this->demo_cart_admin_model->demo_update_order_details($order_number);
            $this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('admin'));
            redirect(current_url());
		}
		
		// Get the row array of the order filtered by the order number in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('order_summary', 'order_number') => $order_number);
		$this->data['summary_data'] = $this->flexi_cart_admin->get_db_order_summary_query(FALSE, $sql_where)->result_array();
		$this->data['summary_data'] = $this->data['summary_data'][0];

		// Get an array of all order details related to the above order, filtered by the order number in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('order_details', 'order_number') => $order_number);
		$this->data['item_data'] = $this->flexi_cart_admin->get_db_order_detail_query(FALSE, $sql_where)->result_array();
		
		//Get User details
		$user_id = $this->db->select('')->where('ord_order_number',$order_number)->get('order_summary')->row()->ord_user_fk;
		$this->load->model('users_model');
		$this->data['user_details'] = $this->users_model->user_details($user_id);
		
		// Get an array of all order statuses that can be set for an order.
		// The data is then to be displayed via a html select input to allow the user to update the orders status.
		$this->data['status_data'] = $this->flexi_cart_admin->get_db_order_status_query()->result_array();

		// Get the row array of any refund data that may be available for the order, filtered by the order number in the url.
		$this->data['refund_data'] = $this->flexi_cart_admin->get_refund_summary_query($order_number)->result_array();
		$this->data['refund_data'] = $this->data['refund_data'][0];
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');	
		$this->load->view('admin/orders/order_details_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * update_order_details
	 * Reloads the saved cart data from a saved order into the users current cart session.
	 * Once the saved cart data is reloaded, the user can browse the store adding and updating items to the cart as normal.
	 * When the cart is resaved, the new cart data will update and overwrite the original saved order.
	 * The page includes an example of listing items that can be further added to the cart, and examples of how to apply discounts and surcharges all from within the same page.
	 *
	 * This page is accessed from the 'Order Details' page via the 'Edit Order' link.
	 */ 
	function update_order_details($order_number)
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_order'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_resave_order($order_number);
		}
		
		// Get the row array of the original order details, filtered by the order number in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('order_summary', 'order_number') => $order_number);
		$this->data['current_order_data'] = $this->flexi_cart_admin->get_db_order_summary_query(FALSE, $sql_where)->result_array();
		$this->data['current_order_data'] = $this->data['current_order_data'][0];

		// Get the id of the loaded cart data.
		$cart_data_id = $this->data['current_order_data'][$this->flexi_cart_admin->db_column('order_summary', 'cart_data')];

		// To prevent re-reloading the saved cart data (And losing any changes) every time the page is refreshed, check if the current CI session contains 
		// the cart data array matching the saved order data that is to be updated.		
		if ($this->flexi_cart_admin->cart_data_id() != $cart_data_id)
		{
			// Load saved cart data array from the confirmed order.
			// This data is loaded into the browser session as if you were shopping with the cart as a customer.
			$this->flexi_cart_admin->load_cart_data($cart_data_id, TRUE);
		}
		
		// This demo includes a list of items from the demo item table that can be added to the reloaded cart.
		// For simplicity, rather than including all example items that can be found in the demo, only items from the 'demo_items' table are used.
		// $this->load->model('demo_cart_model');
		// $this->data['item_data'] = $this->demo_cart_model->demo_get_item_data();

		// Get required data on cart items, summary discounts and cart surcharges for use on the cart.
		// Note: This demo requires the 'get_shipping_options()' function being loaded from the standard flexi cart library.

		$this->load->model('flexi_cart_model');
		$shipping_options = $this->flexi_cart_model->shipping_options($this->flexi->cart_contents['settings']['shipping']['location']);
		$shipping_options = $this->flexi_cart_model->rename_shipping_columns($shipping_options);
		
		$this->data['update_shipping_options'] = $shipping_options; 
		$this->data['update_cart_items'] = $this->flexi_cart_admin->cart_items(FALSE, TRUE, TRUE);
		$this->data['update_reward_vouchers'] = $this->flexi_cart_admin->reward_voucher_data(TRUE, TRUE);
		$this->data['update_discounts'] = $this->flexi_cart_admin->summary_discount_data(FALSE, TRUE, TRUE);
		$this->data['update_surcharges'] = $this->flexi_cart_admin->surcharge_data(FALSE, TRUE, TRUE);
		
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');	
		
		$this->load->view('admin/orders/order_details_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * unset_discount
	 * Removes a specific active item or summary discount from the cart. 
	 * This function is accessed from the 'Update Order Details' page via a 'Remove' link located in the description of an active discount.
	 */ 
	function unset_discount($discount_id = FALSE, $order_number = FALSE)
	{
		$this->load->library('flexi_cart');
		
		// If a discount id is submitted, then only that specific discount will be unset, if submitted as FALSE, all discounts are unset.
		$this->flexi_cart->unset_discount($discount_id);
		
		redirect('admin/update_order_details/'.$order_number);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * unset_surcharge
	 * Removes a specific surcharge from the cart.
	 * This function is accessed from the 'Update Order Details' page via a 'Remove' link located in the description of a surcharge.
	 */ 
	function unset_surcharge($surcharge_id = FALSE, $order_number = FALSE)
	{
		$this->load->library('flexi_cart');

		// If a surcharge id is submitted, then only that specific surcharge will be unset, if submitted as FALSE, all surcharges will be unset.
		$this->flexi_cart->unset_surcharge($surcharge_id);
		
		redirect('admin/update_order_details/'.$order_number);
	}	
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOCATIONS AND ZONES
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * LOCATIONS AND ZONES
	 * Location Types act as a parent grouping for locations, for example a location type of 'Country' would act as the parent to locations like 'United States', 'United Kingdom'.
	 * Locations can be setup to identify a users specific location. Shipping and tax rates can then be applied to each location.
	 * Zones can be setup so the shipping and tax rates can be applied to a range of locations, rather than each specific location. For example, EU and non EU European countries.
	 */
	
	/**
	 * location_types
	 * Displays a manageable list of all 'Locations Types'. Each row can be updated or deleted. 
	 */ 
	function location_types() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_location_types'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_location_types();
		}
	
		// Get an array of all location types.		
		$this->data['location_type_data'] = $this->flexi_cart_admin->get_db_location_type_query()->result_array();
		
		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];

		$this->load->view('admin/locations/location_type_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * insert_location_type
	 * Inserts new location types to the database. 
	 * This page is accessed via the 'Location' page via a link titled 'Insert New Location Type'.
	 */ 
	function insert_location_type() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_location_type'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_location_type();
		}
		
		// Get an array of all location types.		
		$this->data['location_type_data'] = $this->flexi_cart_admin->get_db_location_type_query()->result_array();

		$this->load->view('admin/locations/location_type_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * locations
	 * Displays a manageable list of all 'Locations'. Each row can be updated or deleted.
	 * This page is accessed via the 'Location Type' page via a link on the row of the locations 'parent' (Location type).
	 */ 
	function locations($location_type_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_locations'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_locations();
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get arrays of all shipping and tax zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');
	
		// Get the row array of the location type filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('location_type', 'id') => $location_type_id);
		$this->data['location_type_data'] = $this->flexi_cart_admin->get_db_location_type_query(FALSE, $sql_where)->result_array();
		$this->data['location_type_data'] = $this->data['location_type_data'][0];

		// Get an array of all locations filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('locations', 'type') => $location_type_id);
		$this->data['location_data'] = $this->flexi_cart_admin->get_db_location_query(FALSE, $sql_where)->result_array();
	
		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		
		$this->load->view('admin/locations/location_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * insert_location
	 * Inserts new locations to the database. 
	 * This page is accessed via the 'Location Type' page via a link on the row of the locations 'parent' (Location type), followed by a link similar to 'Insert New Location'.
	 */ 
	function insert_location($location_type_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_location'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_location($location_type_id);
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get arrays of all shipping and tax zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');
	
		// Get the row array of the location type filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('location_type', 'id') => $location_type_id);
		$this->data['location_type_data'] = $this->flexi_cart_admin->get_db_location_type_query(FALSE, $sql_where)->result_array();
		$this->data['location_type_data'] = $this->data['location_type_data'][0];
		
		$this->load->view('admin/locations/location_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * zones
	 * Displays a manageable list of all 'Zones'. Each row can be updated or deleted.
	 */ 
	function zones() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_zones'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_zones();
		}
		
		// Get an array of all zones.
		$this->data['location_zone_data'] = $this->flexi_cart_admin->get_db_location_zone_query()->result_array();
	
		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
	
		$this->load->view('admin/locations/zone_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * insert_zone
	 * Inserts new location based zones to the database. 
	 * This page is accessed via the 'Zones' page via a link titled 'Insert New Zone'.
	 */ 
	function insert_zone() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_zone'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_zones();
		}

		$this->load->view('admin/locations/zone_insert_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// SHIPPING OPTIONS AND RATES
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * SHIPPING OPTIONS AND RATES
	 * Shipping can be setup to return a selection of different shipping options and rates related to a customers location and the weight and value of the cart.
	 */
	
	/**
	 * shipping
	 * Displays a manageable list of all shipping options. Each row can be updated or deleted.
	 */ 
	function shipping() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_shipping'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_shipping();
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'tiered' into the location type groups, so locations can be listed 
		// over multiple html select menus.
		$this->data['locations_tiered'] = $this->flexi_cart_admin->locations_tiered();

		// Get an array of all shipping zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');
	
		// Get an array of all shipping option data.
		$this->data['shipping_data'] = $this->flexi_cart_admin->get_db_shipping_query()->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		
		$this->load->view('admin/shipping/shipping_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * insert_shipping
	 * Inserts new shipping options to the database. 
	 * This page is accessed via the 'Shipping Options' page via a link titled 'Insert New Shipping Option'.
	 */ 
	function insert_shipping() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_option') && $this->input->post('insert_rate'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_shipping();
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'tiered' into the location type groups, so locations can be listed 
		// over multiple html select menus.
		$this->data['locations_tiered'] = $this->flexi_cart_admin->locations_tiered();

		// Get an array of all shipping zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');
	
		$this->load->view('admin/shipping/shipping_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * shipping_rates
	 * Displays a manageable list of all shipping rates for a specific shipping option. Each row can be updated or deleted.
	 * This page is accessed via the 'Shipping Options' page via a link titled 'Manage'.
	 */ 
	function shipping_rates($shipping_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_shipping_rates'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_shipping_rate();
		}
		
		// Get the row array of the shipping option filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('shipping_options', 'id') => $shipping_id);
		$this->data['shipping_data'] = $this->flexi_cart_admin->get_db_shipping_query(FALSE, $sql_where)->result_array();
		$this->data['shipping_data'] = $this->data['shipping_data'][0];
		
		// Get an array of all shipping rates filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('shipping_rates', 'parent') => $shipping_id);
		$this->data['shipping_rate_data'] = $this->flexi_cart_admin->get_db_shipping_rate_query(FALSE, $sql_where)->result_array();
		
		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
	
		$this->load->view('admin/shipping/shipping_rate_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * insert_shipping_rate
	 * Inserts new shipping rates to a specific shipping option in the database. 
	 * This page is accessed via the 'Shipping Options' page via a link titled 'Insert New Rates'.
	 */ 
	function insert_shipping_rate($shipping_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_shipping_rate'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_shipping_rate($shipping_id);
		}
		
		// Get the row array of the shipping option filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('shipping_options', 'id') => $shipping_id);
		$this->data['shipping_data'] = $this->flexi_cart_admin->get_db_shipping_query(FALSE, $sql_where)->result_array();
		$this->data['shipping_data'] = $this->data['shipping_data'][0];
	
		$this->load->view('admin/shipping/shipping_rate_insert_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * item_shipping
	 * Displays a manageable list of all shipping rates for a specific item. Each row can be updated or deleted.
	 * This page is accessed via the 'Items' page via a link titled 'Manage' in the 'Item Shipping Rules' table column.	 
	 */ 
	function item_shipping($item_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_item_shipping'))
		{		
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_item_shipping();
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all shipping zones.
		$this->data['shipping_zones'] = $this->flexi_cart_admin->location_zones('shipping');

		// Get the row array of the demo item filtered by the id in the url.
		$sql_where = array('item_id' => $item_id);
		$this->data['item_data'] = $this->flexi_cart_admin->get_db_table_data_query('demo_items', FALSE, $sql_where)->result_array();
		$this->data['item_data'] = $this->data['item_data'][0];

		// Get an array of all item shipping rules filtered by the id in the url.		
		$sql_where = array($this->flexi_cart_admin->db_column('item_shipping', 'item') => $item_id);
		$this->data['item_shipping_data'] = $this->flexi_cart_admin->get_db_item_shipping_query(FALSE, $sql_where)->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
	
		$this->load->view('admin/items/item_shipping_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// TAXES
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * TAXES
	 * Taxes can be setup to return a tax rate related to a customers location.
	 */
	
	/**
	 * tax
	 * Displays a manageable list of all tax rates. Each row can be updated or deleted.
	 */ 
	function tax() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_tax'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_tax();
		}
	
		// Get an array of location data formatted with all sub-locations displayed 'tiered' into the location type groups, so locations can be listed 
		// over multiple html select menus.
		$this->data['locations_tiered'] = $this->flexi_cart_admin->locations_tiered();

		// Get an array of all tax zones.
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');

		// Get an array of all tax rates.
		$this->data['tax_data'] = $this->flexi_cart_admin->get_db_tax_query()->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
	
		$this->load->view('admin/tax/tax_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * insert_tax
	 * Inserts new tax rate to the database. 
	 * This page is accessed via the 'Taxes' page via a link titled 'Insert New Tax'.
	 */ 
	function insert_tax() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_tax'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_tax();
		}
	
		// Get an array of location data formatted with all sub-locations displayed 'tiered' into the location type groups, so locations can be listed 
		// over multiple html select menus.
		$this->data['locations_tiered'] = $this->flexi_cart_admin->locations_tiered();

		// Get an array of all tax zones.
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');

		$this->load->view('admin/tax/tax_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * item_tax
	 * Displays a manageable list of all tax rates for a specific item. Each row can be updated or deleted.
	 * This page is accessed via the 'Items' page via a link titled 'Manage' in the 'Item Taxes' table column.
	 */ 
	function item_tax($item_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_item_tax'))
		{		
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_item_tax();
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all tax zones.
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');

		// Get the row array of the demo item filtered by the id in the url.
		$sql_where = array('item_id' => $item_id);
		$this->data['item_data'] = $this->flexi_cart_admin->get_db_table_data_query('demo_items', FALSE, $sql_where)->result_array();
		$this->data['item_data'] = $this->data['item_data'][0];

		// Get an array of all the item tax rates filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('item_tax', 'item') => $item_id);
		$this->data['item_tax_data'] = $this->flexi_cart_admin->get_db_item_tax_query(FALSE, $sql_where)->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
	
		$this->load->view('admin/items/item_tax_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * insert_item_tax
	 * Inserts new item tax rates for a specific item in the database. 
	 * This page is accessed via the 'Items' page via a link titled 'Manage' in the 'Item Taxes' table column, followed by a link titled 'Insert New Item Tax Rates'.
	 */ 
	function insert_item_tax($item_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_item_tax'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_item_tax($item_id);
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all tax zones.
		$this->data['tax_zones'] = $this->flexi_cart_admin->location_zones('tax');

		// Get the row array of the demo item filtered by the id in the url.
		$sql_where = array('item_id' => $item_id);
		$this->data['item_data'] = $this->flexi_cart_admin->get_db_table_data_query('demo_items', FALSE, $sql_where)->result_array();
		$this->data['item_data'] = $this->data['item_data'][0];
	
		$this->load->view('admin/items/item_tax_insert_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// DISCOUNTS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * DISCOUNTS
	 * Discounts can be setup with a wide range of rule conditions.
	 * The discounts can then be applied to specific items, groups of items or can be applied across the entire cart.
	 */ 
	
	/**
	 * item_discounts
	 * Displays a manageable list of all item discounts. Each row can be updated or deleted.
	 */ 
	function item_discounts() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_discounts'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_discounts();
		}
		
		// Get an array of all discounts filtered by a 'type' of 1 ('item discounts') and for purposes of this demo, have an id of 32+.
		$sql_where = array(
			$this->flexi_cart_admin->db_column('discounts', 'id').' >=' => 32,
			$this->flexi_cart_admin->db_column('discounts', 'type') => 1
		);
		$this->data['discount_data'] = $this->flexi_cart_admin->get_db_discount_query(FALSE, $sql_where)->result_array();
		
		// Set a variable to indicate on the html page that the discount is an 'item' discount.
		$this->data['discount_type'] = 'item';
	
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');

		$this->load->view('admin/discounts/discounts_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * summary_discounts
	 * Displays a manageable list of all summary discounts. Each row can be updated or deleted.
	 */ 
	function summary_discounts() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_discounts'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_discounts();
		}

		// Get an array of all discounts filtered by a 'type' of 2 ('summary discounts').
		$sql_where = array($this->flexi_cart_admin->db_column('discounts', 'type') => 2);
		$this->data['discount_data'] = $this->flexi_cart_admin->get_db_discount_query(FALSE, $sql_where)->result_array();
		
		// Set a variable to indicate on the html page that the discount is an 'summary' discount.
		$this->data['discount_type'] = 'summary';
	
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');

		$this->load->view('admin/discounts/discounts_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * update_discount
	 * Updates data for an existing discount in the database. 
	 * This page is accessed via either the 'Item Discounts' or 'Summary Discounts' page via a link titled 'Edit'.
	 */ 
	function update_discount($discount_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_discount'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_discount($discount_id);
		}

		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all zones.
		$this->data['zones'] = $this->flexi_cart_admin->location_zones();
		
		// Get an array of all discount types.		
		$this->data['discount_types'] = $this->flexi_cart_admin->get_db_discount_type_query()->result_array();
		
		// Get an array of all discount methods.	
		$this->data['discount_methods'] = $this->flexi_cart_admin->get_db_discount_method_query()->result_array();
		
		// Get an array of all discount tax methods.		
		$this->data['discount_tax_methods'] = $this->flexi_cart_admin->get_db_discount_tax_method_query()->result_array();
		
		// Get an array of all discount groups.		
		$this->data['discount_groups'] = $this->flexi_cart_admin->get_db_discount_group_query()->result_array();
		
		// Get an array of all demo items.		
		$this->data['items'] = $this->flexi_cart_admin->get_db_table_data_query('products')->result_array();

		// Get the row array of the discount filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('discounts', 'id') => $discount_id);
		$this->data['discount_data'] = $this->flexi_cart_admin->get_db_discount_query(FALSE, $sql_where)->result_array();
		$this->data['discount_data'] = $this->data['discount_data'][0];

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		
		$this->load->view('admin/discounts/discount_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * insert_discount
	 * Inserts a new item or summary discount to the database. 
	 * This page is accessed via either the 'Item Discounts' or 'Summary Discounts' page via a link titled 'Insert New Discount'.
	 */ 
	function insert_discount() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_discount'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_discount();
		}

		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all zones.
		$this->data['zones'] = $this->flexi_cart_admin->location_zones();
		
		// Get an array of all discount types.		
		$this->data['discount_types'] = $this->flexi_cart_admin->get_db_discount_type_query()->result_array();
		
		// Get an array of all discount methods.	
		$this->data['discount_methods'] = $this->flexi_cart_admin->get_db_discount_method_query()->result_array();
		
		// Get an array of all discount tax methods.		
		$this->data['discount_tax_methods'] = $this->flexi_cart_admin->get_db_discount_tax_method_query()->result_array();
		
		// Get an array of all discount groups.		
		$this->data['discount_groups'] = $this->flexi_cart_admin->get_db_discount_group_query()->result_array();
		
		// Get an array of all demo items.		
		$this->data['items'] = $this->flexi_cart_admin->get_db_table_data_query('products')->result_array();
	
		$this->load->view('admin/discounts/discount_insert_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * discount_groups
	 * Displays a manageable list of all discount groups. Each row can be updated or deleted.
	 */ 
	function discount_groups() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_discount_groups'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_discount_groups();
		}
	
		// Get an array of all discount groups.		
		$this->data['discount_group_data'] = $this->flexi_cart_admin->get_db_discount_group_query()->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
	
		$this->load->view('admin/discounts/discount_groups_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * update_discount_group
	 * Updates data for an existing discount group and its related discount group items in the database. 
	 * This page is accessed via the 'Discount Groups' page via a link titled 'Manage Items in Group'.
	 */ 
	function update_discount_group($group_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_discount_group_items'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_discount_group($group_id);
		}
		
		// Get the row array of the discount group filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('discount_groups', 'id') => $group_id);
		$this->data['group_data'] = $this->flexi_cart_admin->get_db_discount_group_query(FALSE, $sql_where)->result_array();
		$this->data['group_data'] = $this->data['group_data'][0];
		
		// Get an array of all the discount group items filtered by the id in the url.
		// Using flexi cart SQL functions, join the demo item table with the discount group items and then order the data by item id.
		$this->flexi_cart_admin->sql_join('products', 'products.id = '.$this->flexi_cart_admin->db_column('discount_group_items', 'item')); 
		$this->flexi_cart_admin->sql_order_by('id');
		$sql_where = array($this->flexi_cart_admin->db_column('discount_group_items', 'group') => $group_id);		
		$this->data['group_item_data'] = $this->flexi_cart_admin->get_db_discount_group_item_query(FALSE, $sql_where)->result_array();
		
		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		
		$this->load->view('admin/discounts/discount_group_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * insert_discount_group
	 * Inserts a new discount group and its related discount group items to the database. 
	 * This page is accessed via the 'Discount Groups' page via a link titled 'Insert New Group'.
	 */ 
	function insert_discount_group() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_discount_group'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_discount_group();
		}
	
		$this->load->view('admin/discounts/discount_group_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * insert_discount_group_items
	 * Inserts new discount group items to the database. 
	 * This page is accessed via the 'Discount Groups' page via a link titled 'Insert New Items to Group'.
	 */ 
	function insert_discount_group_items($group_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.

		if ($this->input->post('insert_selected'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_discount_group_items($group_id);
			redirect('admin/update_discount_group/'.$group_id);
		}
		
		// Get the row array of the discount group filtered by the id in the url.
		$sql_where = array($this->flexi_cart_admin->db_column('discount_groups', 'id') => $group_id);
		$this->data['group_data'] = $this->flexi_cart_admin->get_db_discount_group_query(FALSE, $sql_where)->result_array();
		$this->data['group_data'] = $this->data['group_data'][0];

		// Get product items to choose from.
		// Get products.
		$this->load->library('store');
		$products = $this->store->products($options = array(
			'order' => 'alpha'
		));
		$this->data['products'] = $products['rows'];	
		$this->data['products_total'] = $products['total'];	
		$this->data['pagination'] = $products['pagination'];

		$this->data['message'] = $this->session->flashdata('message');

		$this->load->view('admin/discounts/discount_group_items_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// REWARD POINTS AND VOUCHERS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * REWARD POINTS AND VOUCHERS
	 * Customers can earn reward points when purchasing cart items. The reward points can then be converted to vouchers that can be used to buy other items.
	 */ 
	
	/**
	 * user_reward_points
	 * Displays a summary list of all users and their reward points.
	 */ 
    function user_reward_points() {
    	$crud = new grocery_CRUD();
    	$CI =& get_instance();
	 	$users_table = 'j'.substr(md5('user_id'),0,8);
	 	$CI->join = array($users_table);
	 	$crud->set_model('Admin_user_reward_point_model');
		$crud->set_theme('datatables');
		$crud->set_table('gre_reward_point');
		$crud->set_relation('user_id','users','first_name');
		$crud->columns('id','customer_id','phone','flat_no','apartment','locality','email','order_id','scrap_request_id','redeem_request_id','credit_reward_point','debit_reward_point','total_reward_point','credited_by','comment','date');
		$crud->fields('id','scrap_request_id','credit_reward_point','debit_reward_point','credited_by','comment');
		$crud->display_as('order_id','Scrap order id')->display_as('customer_id','Customer Name');
		$crud->field_type('id','readonly')->field_type('customer_id','readonly');
		$crud->unset_clone();
		$crud->unset_read();
		$crud->unset_edit();
		$crud->add_action('View', '', '','ui-icon-mail-closed',array($this,'callback_view_user_reward'));
        $crud->callback_before_insert(array($this,'reward_point_check_ids_callback'));
		$crud->callback_after_insert(array($this,'reward_point_add_callback'));
		$crud->callback_column('customer_id',array($this,'_callback_combine_name'));
		$crud->callback_column('date',array($this,'_callback_date'));
        $crud->callback_column('locality',array($this,'_callback_user_locality1'));
        $crud->callback_column('apartment',array($this,'_callback_user_apartment1'));
        $crud->callback_column('flat_no',array($this,'_callback_user_flat_no1'));
		$crud->order_by('date','desc');
		$output = $crud->render();
		$this->data['owner'] = $this->users_model->owner();
		$this->load->view('admin/templates/header', $this->data);
		$this->load->view('admin/reward_points/view_user_reward_points.php',(array)$output);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * user_reward_point_history
	 * Displays an itemised list of all earnt and converted user reward points.
	 * This page is accessed via the 'Reward Points' page via a link titled 'View' in the 'History' table column.
	 */ 
	function user_reward_point_history($user_id)
	{
		$this->load->model('demo_cart_admin_model');
		
		// Get the row array of the demo users filtered by the id in the url.
		$sql_where = array('id' => $user_id);
		$this->data['user_data'] = $this->flexi_cart_admin->get_db_table_data_query('users', FALSE, $sql_where)->result_array();
		if (!empty($this->data['user_data'])) $this->data['user_data'] = $this->data['user_data'][0];
	
		// Get an array of all reward points for a user filtered by the id in the url.
		// The 'get_user_reward_points()' function only returns the minimum required fields, therefore define the other required table fields via an SQL SELECT statement.
		$sql_select = array(
			$this->flexi_cart_admin->db_column('reward_points', 'order_number'),
			$this->flexi_cart_admin->db_column('reward_points', 'description'),
			$this->flexi_cart_admin->db_column('reward_points', 'order_date')
		);	
		$sql_where = array($this->flexi_cart_admin->db_column('reward_points', 'user') => $user_id);
		$this->data['points_awarded_data'] = $this->flexi_cart_admin->get_db_reward_points_query($sql_select, $sql_where)->result_array();
		
		// Call a custom function that returns a nested array of reward voucher codes and the reward point data used to create the voucher.
		$this->data['points_converted_data'] = $this->demo_cart_admin_model->demo_converted_reward_point_history($user_id);
		
		$this->load->view('admin/reward_points/user_reward_point_history_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * user_vouchers
	 * Displays a list of all reward vouchers for a specific user. Each row can be updated.
	 * This page is accessed via the 'Reward Points' page via a link titled 'View' in the 'Vouchers' table column.
	 */ 
	function user_vouchers($user_id) 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_vouchers'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_voucher();
		}
	
		// Get the row array of the demo user filtered by the id in the url.
		$sql_where = array('user_id' => $user_id);
		$this->data['user_data'] = $this->flexi_cart_admin->get_db_table_data_query('demo_users', FALSE, $sql_where)->result_array();
		if(!empty($this->data['user_data'])) $this->data['user_data'] = $this->data['user_data'][0];

		// Get an array of all the reward vouchers filtered by the id in the url.
		// Using flexi cart SQL functions, join the demo user table with the discount table.
		$sql_where = array($this->flexi_cart_admin->db_column('discounts', 'user') => $user_id);
		$this->flexi_cart_admin->sql_join('demo_users', 'user_id = '.$this->flexi_cart_admin->db_column('discounts', 'user'));
		$this->data['voucher_data'] = $this->flexi_cart_admin->get_db_voucher_query(FALSE, $sql_where)->result_array();
		
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/reward_points/user_vouchers_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * vouchers
	 * Displays a list of all reward vouchers. Each row can be updated.
	 */ 
	function vouchers() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_vouchers'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_voucher();
		}
	
		// Get an array of all reward vouchers.
		// Using flexi cart SQL functions, join the demo users table with the discount table.
		$this->flexi_cart_admin->sql_join('users', 'id = '.$this->flexi_cart_admin->db_column('discounts', 'user'));
		$this->data['voucher_data'] = $this->flexi_cart_admin->get_db_voucher_query()->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/reward_points/vouchers_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * convert_reward_points
	 * Converts a submitted number of reward points into a reward voucher.
	 * This page is accessed via the 'Reward Points' page via a link titled 'Convert' in the 'Vouchers' table column.
	 */ 
	function convert_reward_points($user_id) 
	{
		$this->load->model('demo_cart_admin_model');
		
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('convert_reward_points'))
		{
			$this->demo_cart_admin_model->demo_convert_reward_points($user_id);
			redirect('admin/user_vouchers/'.$user_id);
		}

		// Get an array of a demo user and their related reward points from a custom demo model function, filtered by the id in the url.
		$user_data = $this->demo_cart_admin_model->demo_reward_point_summary($user_id);
		
		// Note: The custom function returns a multi-dimensional array, of which we only need the first array, so get the first row '$user_data[0]'.
		$this->data['user_data'] = $user_data[0];
		
		// Get the conversion tier values for converting reward points to vouchers.
		$conversion_tiers = $this->data['user_data'][$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')];
		$this->data['conversion_tiers'] = $this->flexi_cart_admin->get_reward_point_conversion_tiers($conversion_tiers);
		
		$this->load->view('admin/reward_points/user_reward_point_convert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// CURRENCY
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * currency
	 * Displays a manageable list of all currencies. Each row can be updated or deleted.
	 */ 
	function currency() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_currency'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_currency();
		}

		// Get an array of all currencies.
		$this->data['currency_data'] = $this->flexi_cart_admin->get_db_currency_query()->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		
		$this->load->view('admin/currency/currency_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * insert_currency
	 * Inserts new currencies to the database. 
	 * This page is accessed via the 'Currency' page via a link titled 'Insert New Currency'.
	 */ 
	function insert_currency() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_currency'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_currency();
		}

		$this->load->view('admin/currency/currency_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// ORDER STATUS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * order_status
	 * Displays a manageable list of all order statuses. Each row can be updated or deleted.
	 */ 
	function order_status() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_order_status'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_order_status();
		}

		// Get an array of all order statuses.
		$this->data['order_status_data'] = $this->flexi_cart_admin->get_db_order_status_query()->result_array();

		// Get any status message that may have been set.
		$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		
		$this->load->view('admin/orders/order_status_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * insert_order_status
	 * Inserts new order statuses to the database. 
	 * This page is accessed via the 'Order Status' page via a link titled 'Insert New Order Status'.
	 */ 
	function insert_order_status()
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('insert_order_status'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_insert_order_status();
		}

		$this->load->view('admin/orders/order_status_insert_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// CART CONFIGURATION AND DEFAULTS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * config
	 * Updates the carts configuration data in the database. 
	 */ 
	function config() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_config'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_config();
		}
		
		// Get the row array of the config table.
		$this->data['config'] = $this->flexi_cart_admin->get_db_config_query()->result_array();
		if(!empty($this->data['config'])) $this->data['config'] = $this->data['config'][0];

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/config/config_update_view', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * defaults
	 * Sets the default cart values for the currency, shipping and tax tables. 
	 */ 
	function defaults() 
	{
		// Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
		if ($this->input->post('update_defaults'))
		{
			$this->load->model('demo_cart_admin_model');
			$this->demo_cart_admin_model->demo_update_defaults();
		}
		
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		// Alternatively, the location data could have been formatted with all sub-locations displayed 'tiered' into the location type groups.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of all currencies.
		$this->data['currency_data'] = $this->flexi_cart_admin->get_db_currency_query()->result_array();

		// Get an array of all shipping options.
		$this->data['shipping_data'] = $this->flexi_cart_admin->get_db_shipping_query()->result_array();

		// Get an array of all tax rate.
		$this->data['tax_data'] = $this->flexi_cart_admin->get_db_tax_query()->result_array();

		// Get current cart defaults.
		$this->data['default_currency'] = $this->flexi_cart_admin->get_db_currency_query(FALSE, array('curr_default' => 1))->result_array();
		if(!empty($this->data['default_currency'])) $this->data['default_currency'] = $this->data['default_currency'][0];
		$this->data['default_ship_location'] = $this->flexi_cart_admin->get_db_location_query(FALSE, array('loc_ship_default' => 1))->result_array();
		if (!empty($this->data['default_ship_location'])) $this->data['default_ship_location'] = $this->data['default_ship_location'][0];
		$this->data['default_tax_location'] = $this->flexi_cart_admin->get_db_location_query(FALSE, array('loc_tax_default' => 1))->result_array();
		if (!empty($this->data['default_tax_location'])) $this->data['default_tax_location'] = $this->data['default_tax_location'][0];
		$this->data['default_ship_option'] = $this->flexi_cart_admin->get_db_shipping_query(FALSE, array('ship_default' => 1))->result_array();
		$this->data['default_tax_rate'] = $this->flexi_cart_admin->get_db_tax_query(FALSE, array('tax_default' => 1))->result_array();
		if (!empty($this->data['default_tax_rate'])) $this->data['default_tax_rate'] = $this->data['default_tax_rate'][0];

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/config/defaults_update_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOCATION MENU EXAMPLE
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * demo_location_menus
	 * A demo example of how location data can be displayed via html select menus with a JavaScript and non Javascript example.
	 */ 
	function demo_location_menus()
	{
		// Get an array of location data formatted with all sub-locations displayed 'inline', so all locations can be listed in one html select menu.
		$this->data['locations_inline'] = $this->flexi_cart_admin->locations_inline();
		
		// Get an array of location data formatted with all sub-locations displayed 'tiered' into the location type groups, so locations can be listed 
		// over multiple html select menus.
		$this->data['locations_tiered'] = $this->flexi_cart_admin->locations_tiered();

		$this->load->view('admin/locations/location_menu_demo_view', $this->data);		
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// USER CREATED PAGES
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * pages
	 * View and edit user pages.
	 */ 
	public function pages()
	{
		$this->load->model('pages_model');

		$this->load->library('form_validation');

		if ($this->input->post('delete_selected'))
		{
			$this->form_validation->set_rules('selected[]', 'above', 'required');

			if ($this->form_validation->run())
			{
				$response = $this->pages_model->delete_multiple($this->input->post('selected[]'));
				redirect(current_url());
			}
		}

		$this->data['pages'] = $this->pages_model->get_pages();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/pages/pages_view', $this->data);
	}	
	
	/**
	 * update_page
	 * View and edit user page.
	 */ 
	public function update_page($id)
	{
		$this->load->model('pages_model');

		$this->load->library('form_validation');

		if ($this->input->post('update_page'))
		{
			$this->form_validation->set_rules('name', 'above', 'required');
			$this->form_validation->set_rules('body', 'above', 'required');

			if ($this->form_validation->run())
			{
				$response = $this->pages_model->update_page($id);
				redirect(current_url());
			}
		}

		$this->data['page_data'] = $this->pages_model->get_pages($id);

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/pages/pages_update_view', $this->data);
	}

	/**
	 * insert_page
	 * Insert a new page.
	 */ 
	public function insert_page()
	{
		$this->load->model('pages_model');

		$this->load->library('form_validation');

		if ($this->input->post('insert_page'))
		{
			$this->form_validation->set_rules('name', 'above', 'required|is_unique[pages.name]', array('is_unique' => 'A Page with this name already exists'));
			$this->form_validation->set_rules('body', 'above', 'required');

			if ($this->form_validation->run())
			{
				if ($this->pages_model->add_page())
				{
					redirect('admin/pages');
				}
				else
				{
					redirect(current_url());
				}
			}
		}

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/pages/pages_insert_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// USER BANNERS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * banners
	 * View and edit user banners.
	 */ 
	public function banners($id = NULL)
	{
		$this->load->model('banners_model');

		$this->load->library('form_validation');

		if ($this->input->post('delete_selected'))
		{
			$this->form_validation->set_rules('selected[]', 'above', 'required');

			if ($this->form_validation->run())
			{
				$response = $this->banners_model->delete_multiple($this->input->post('selected[]'));
				redirect(current_url());
			}
		}

		if ($this->input->post('update_page'))
		{
			$this->form_validation->set_rules('name', 'above', 'required');
			$this->form_validation->set_rules('body', 'above', 'required');

			if ($this->form_validation->run())
			{
				$response = $this->pages_model->update_page($id);
				redirect(current_url());
			}
		}

		$this->data['banners'] = $this->banners_model->get_banners();

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/banners/banners_view', $this->data);
	}

	/**
	 * insert_banner
	 * Insert a new page.
	 */ 
	public function insert_banner()
	{
		$this->load->model('banners_model');

		$this->load->library('form_validation');

		if ($this->input->post('insert_banner'))
		{
			$this->form_validation->set_rules('title', 'Title', 'required');
// 			$this->form_validation->set_rules('start_date', 'Start Date', 'required|callback_is_date_valid');
// 			$this->form_validation->set_rules('end_date', 'End Date', 'required|callback_is_date_valid');

			if ($this->form_validation->run())
			{
				$is_banner_inserted = $this->banners_model->insert_banner();

				if ($this->input->post('add_another') OR !$is_banner_inserted)
				{
					// Display insert banner page if user indicated so
					// or if the banner was not added successfully
					redirect(current_url());
				}
				else
				{
					redirect('admin/banners');
				}
			}
		}

		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/banners/banner_insert_view', $this->data);
	}

	/**
	 * update_banner
	 * Update a banner.
	 */ 
	public function update_banner($id)
	{
		$this->load->model('banners_model');

		$this->load->library('form_validation');

		if ($this->input->post('update_banner'))
		{
			$this->form_validation->set_rules('title', 'Title', 'required');
// 			$this->form_validation->set_rules('start_date', 'Start Date', 'required|callback_is_date_valid');
// 			$this->form_validation->set_rules('end_date', 'End Date', 'required|callback_is_date_valid');

			if ($this->form_validation->run())
			{
				$this->banners_model->update_banner($id);
				redirect(current_url());
			}
		}

		$this->data['banner']  = $this->banners_model->get_banner_details($id);
		// Get any status message that may have been set.
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->view('admin/banners/banner_update_view', $this->data);
	}

	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// MINI CART DATA
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * mini_cart_data
	 * This function is called by the '__construct()' to set item data to be displayed on the 'Mini Cart' menu.
	 */ 
	private function mini_cart_data()
	{
		$this->data['mini_cart_items'] = $this->flexi_cart_admin->cart_items();
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// CUSTOM VALIDATION CALLBACKS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * userfile_check
	 * Custom validation for user uploaded files
	 */
	function userfile_check()
	{
		if ($_FILES['userfile']['size'] > 0)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('userfile_check', 'The {field} file is required');
			return FALSE;
		}
	}

	/**
	 * is_date_valid
	 * Custom Validation for date not to be in past
	 */
	function is_date_valid($str)
	{
		$this->load->helper('date');

		if (mysql_to_unix(date('Y-m-d H:i:s')) <= mysql_to_unix($str))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('is_date_valid', 'The {field} must not be in the past');
			return FALSE;
		}
	}


	/**
	 * password_check
	 * Custom validation for user password match
	 */
	function password_check($old_password)
	{
		if ($this->ion_auth->hash_password_db($this->data['user']->id, $old_password))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('password_check', 'Sorry, the password did not match');
			return FALSE;
		}
	}

	/**
	 * upload_image
	 * Upload image to the server
	 */
	private function upload_image($filename = NULL)
	{
		// Path that the logo image will be uploaded to
		$uploadPath = $this->data['app']['file_path'];

		// Ensure that the upload directory exists
		if ( ! file_exists($uploadPath) )
		{
			if ( ! mkdir($uploadPath, 0777, true) )
			{
				// Return an error if it should occur
				return array(
					"error" => 'create directory "assets/images/store" and retry'
				);
			}
		}
		// Load the CI upload library
		$config['upload_path'] 		= $uploadPath;
        $config['allowed_types']	= 'gif|jpg|png';
        $config['max_width'] 		= 0;
        $config['max_height'] 		= 0;
        $config['max_size'] 		= 5500;
		$config['remove_spaces'] 	= FALSE;
        $config['encrypt_name'] 	= TRUE;
        $config['overwrite'] 		= FALSE;
        $this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			// Return an error if it should occur
			return array(
				'error' => $this->upload->display_errors()
			);
		}
		else
		{
			// Remove the old logo.
			if ($filename AND is_file($filename))
			{
				unlink($uploadPath.'/'.$filename);
			}

			// Get file properties data.
			$imageData = $this->upload->data();

			if ($this->input->post('crop_width') AND $this->input->post('crop_height'))
			{
				// Crop Banner image.
				$this->load->library('image_lib');
				$config['source_image']	  = $uploadPath.$imageData['file_name'];
				$config['maintain_ratio'] = FALSE;
				$config['width'] 	= $this->input->post('crop_width');
				$config['height'] 	= $this->input->post('crop_height');
				$config['x_axis'] 	= $this->input->post('crop_x');
				$config['y_axis'] 	= $this->input->post('crop_y');

				$this->image_lib->initialize($config); 

				if ( ! $this->image_lib->crop())
				{
					echo $this->image_lib->display_errors();
				}
			}


			// Resize the logo.
			$config['image_library'] 	= 'gd2';
			$config['source_image']		= $uploadPath.$imageData['file_name'];
			$config['file_name']		= $uploadPath.$imageData['file_name'];
			$config['create_thumb'] 	= FALSE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']	 		= 500;
			$config['height']			= 500;

			$this->load->library('image_lib');
			$this->image_lib->initialize($config);
			$this->image_lib->resize($config);

			return array(
				'error' => FALSE,
				'path' => $uploadPath.$imageData['file_name']
			);
		}
	}

	private function time_elapsed_string($datetime, $full = false) {
		$this->load->helper('date');
		$check = strrchr($datetime,'-');
		if (empty($check)) {
			$datetime = unix_to_human($datetime);
		}
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);
		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		// log_message('debug','hiiiii');
		// log_message('debug',$datetime);
		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
			);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */