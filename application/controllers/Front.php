<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

    function __construct() 
    {
        parent::__construct();

        // IMPORTANT! This global must be defined BEFORE the flexi cart library is loaded! 
        // It is used as a global by the library, without it, flexi cart will not work.
        $this->flexi = new stdClass;

        // Load required libraries.
        $this->load->library(array(
            'flexi_cart', 'session', 'form_validation', 'ion_auth', 'store'
        ));
        // Load required models.
        $this->load->model(array(
            'users_model','category_model','product_model','users_wishlist_model'
        ));
        $this->load->helper('file');
        // Load required config files.
        $this->load->config('app');

        // Initialize data used by view pages.
        $this->data = array(
            'app'   => $this->config->item('app'),
            'owner' => $this->users_model->owner(),
            'user'  => NULL,
        );

        // Check if a non admin user is logged in.
        if ($this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
        {
            $this->data['user'] = $this->users_model->current();
        }
    }

    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // USER AUTHENTICATION
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
    /*
         New code start
    */
function register_user(){
        $this->load->view('public/auth/register_user');
    }
    
    function user_product_view($slug){
        $product = $this->product_model->get_details($slug);
    
        $this->data['category'] = $this->category_model->get_categories($product->category->id);
    
        /**
        *
        * Handle submitted data for Cart insert.
        *
         */
        if ($this->input->post('add_to_cart'))
        {
            $this->flexi_cart->insert_items(array(
                'id'      => $this->input->post('id'),
                'name'    => $this->input->post('name'),
                'thumb'   => $this->input->post('thumb'),
                'quantity'=> $this->input->post('quantity'),
                'price'   => $this->input->post('price'),
                'weight'  => $this->input->post('weight'),
                'options' => $this->input->post('options')
            ));
    
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());
    
            redirect(current_url());
        }

        /**
        *
        * Handle submitted data for Product inquirues.
        *
         */
        if ($this->input->post('inquiry_product'))
        {
            // validate form input
            $this->form_validation->set_rules('target_price', 'above', 'required');
            $this->form_validation->set_rules('order_quantity', 'above', 'required');
            $this->form_validation->set_rules('inquiry', 'above', 'required');
            
            if ( ! $this->data['user'])
            {
                // For users who are no logged in, require more form details.
                $this->form_validation->set_rules('name', 'above', 'required');
                $this->form_validation->set_rules('email', 'above', 'required|valid_email');
                $this->form_validation->set_rules('phone', 'above', 'required|trim');
            }
            else
            {
                $user = $this->users_model->get_details($this->data['user']->id);
            }
    
    
            if ($this->form_validation->run() == true)
            {
                if ($this->data['user'])
                {
                    // Get users details from the database.
                    $this->data['buyer_name']       = (isset($user->first_name)) ? ($user->first_name.' '.$user->first_name) : $user->company_name;
                    $this->data['buyer_email']      = (isset($user->email)) ? $user->email : $user->company_email;
                    $this->data['buyer_phone']      = (isset($user->phone)) ? $user->phone : $user->company_phone;
                    $this->data['buyer_location']   = (isset($user->address)) ? $user->address : $user->company_address;
                }
                else
                {
                    // Get users details from the form.
                    $this->data['buyer_name']  = $this->input->post('name');
                    $this->data['buyer_email'] = $this->input->post('email');
                    $this->data['buyer_phone'] = $this->input->post('phone');
                    $this->data['buyer_location'] = $this->input->post('location');
                }
                
                $this->data['buyer_price'] = $this->input->post('target_price');
                $this->data['buyer_order'] = $this->input->post('order_quantity');
                $this->data['buyer_inquiry'] = $this->input->post('inquiry');
    
                
                $config = Array(
                    'protocol' => 'smtp',
                    /*'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',*/
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                //$this->load->library('email');
    
                $this->email->clear();
                $this->email->from($this->data['app']['from_mail_id'],'GreenREE');
                $this->email->to($product->seller->company_email);
                $this->email->subject('['.$this->data['app']['name'].'] Your item received an inquiry');
                $this->email->message($this->load->view('public/email/product_inquiry', $this->data, TRUE));
                
                if($product->email_verified){
                    // Send email to the vendor.
                    if ($this->email->send())
                    {
                        $this->session->set_flashdata('alert',
                            array('type' => 'success', 'message' => 'Your inquiry has been sent to the seller')
                        );
                        redirect(current_url(), 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('alert',
                            array('type' => 'danger', 'message' => 'Your inquiry could not been sent.')
                        );
                        redirect(current_url(), 'refresh');
                    }
                }
            }
        }

        /**
        *
        * Formatting Product Variable to create thumbnails with links
        * which can then be used to point to the product variants.
        *
         */
        // Get related items.
        $related_meta = $this->product_model->get_products(array(
            'exclude' => $product->id,
            'limit' => 5,
            'start' => 0,
            'category_id' => $product->category->id,
        ));
        $related_products = $related_meta->products;
    
        /**
        *
        * Get some random items for the user to view
        *
         */
        $user_items_meta = $this->product_model->get_products(array(
            'limit' => 5,
            'random' => TRUE,
            'start' => 0,
        ));
        $random_products = $user_items_meta->products;
    
        
    
        /**
        *
        * Formatting Product Variable to create thumbnails with links
        * which can then be used to point to the product variants.
        *
         */
        // Thumbnails linking to a product variant
        $variant_thumbs = array();
    
        $product_variants = $this->product_model->get_product_variants($product->id);
        foreach ($product_variants as $variant)
        {
            // Generate the URI string to show product variants
            $variant_slug = implode("-", $variant->options_set);
            $variant_link = current_url().'?&OPT='.$variant_slug;
    
            $thumb_img = (!empty($variant->images)) ? $variant->images[0]->url : NULL ;
            // Add Formatted link and image to thumbnail variation array
            array_push($variant_thumbs, array(
                'link'  => $variant_link,
                'image' => $thumb_img
            ));
        }
    
        /**
        *
        * Formatting Product Options to add a link which can then be used to point
        * to the product variant.
        *
         */
        // Get Product Options
        $product_options  = $this->product_model->get_product_options($product->id);
        // Get Product Defaults
        $product_defaults = $this->product_model->get_product_defaults($product->id);
    
        // Get selected variant. Typical selected by the user, or if set, use admin selected default.
        if ($this->input->get('OPT'))
        {
            $variant_selected = explode('-', $this->input->get('OPT'));
        }
        else
        {
            $variant_selected = $product_defaults;
        }
    
        foreach ($product_options as $key => $option)
        {
            $curr_ids  = array();
            foreach ($option['values'] as $index => $value)
            {
                array_push($curr_ids, $value['id']);
            }
    
            // Define current option attributes.
            $curr_attr = $variant_selected;
    
            // Add a link to each option attribute value that the user will in
            // Selecting the product variation.
            foreach ($option['values'] as $index => $value)
            {
                // Define option attributes IDs in the URI string that are different from
                // those in the current option attributes ID. (There should only be one ID per option):
                $curr_attr = array_diff($curr_attr, $curr_ids);
                // Add current option attribute id.
                array_push($curr_attr, $value['id']);
                // Add the link to the option attribute values.
                $product_options[$key]['values'][$index]['link'] = current_url().'?&OPT='.implode('-', $curr_attr);
            }
        }
    
        /**
        *
        * Get the product variant - this is just the variated image, price and weight
        *
         */
        if ($this->input->get('OPT'))
        {
            // For user defined options
            $product_variant = $this->product_model->get_product_variant_by_slug($product->id, $this->input->get('OPT'));
        }
        else
        {
            // For default options
            $product_variant = $this->product_model->get_product_variant_by_slug($product->id, implode('-', $product_defaults));
        }
    
    
        /**
        *
        * Collect all data for the page view file.
        *
         */
        $this->data['variant_selected'] = $variant_selected;
        $this->data['product_options']  = $product_options;
        $this->data['product_variants'] = $product_variants;
        $this->data['$product_defaults'] = $product_defaults;
        $this->data['variant_thumbs']   = $variant_thumbs;
        $this->data['product']          = $product;
        $this->data['random_products']  = $random_products;
        $this->data['related_products'] = $related_products;
        $this->data['variant'] = $product_variant;
        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');
        
        $this->load->view('public/user_products/user_product_view', $this->data);
    }
    
    //generate unique number
    function randomPassword($length,$count, $characters) { 
        $symbols = array();
        $passwords = array();
        $used_symbols = '';
        $pass = '';
        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = '!?~@#-_+<>[]{}';
     
        $characters = explode(",",$characters);
        foreach ($characters as $key=>$value) {
            $used_symbols .= $symbols[$value];
        }
        $symbols_length = strlen($used_symbols) - 1;
        for ($p = 0; $p < $count; $p++) {
            $pass = '';
            for ($i = 0; $i < $length; $i++) {
                $n = rand(0, $symbols_length); 
                $pass .= $used_symbols[$n]; 
            }
            $passwords[] = $pass;
        }
        return $passwords; // return the generated password
    }
 
    
    function user_product_list(){
        if ( ! $this->data['user'])
        {   
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }

        $categories = $this->store->menu();

            foreach ($categories as $item)
            {
                $products = $this->store->products(array(
                    'limit' => 10,
                    'category_id' => $item->id,
                ));
                $item->products = $products['rows'];    
            }
            $this->data['categories'] = $categories;
            $this->data['latest'] = $this->product_model->get_latest_in_category(4);

            $this->load->model('banners_model');
            $this->data['banners'] = $this->banners_model->get_banners(array(
                'limit' => 4,
                'running' => TRUE
            ));
        $this->data['message'] = $this->session->flashdata('message');

        $this->load->view('public/user_products/user_product_list', $this->data);
    }
    function about_us(){
        $data['user'] = $this->users_model->current();
        $this->load->view('public/about_us',$data);
    }
    function contact_us_msg(){
        $customer_name  = $this->input->post('customer_name');
        $customer_email = $this->input->post('customer_email');
        $customer_phone = $this->input->post('customer_phone');
        $customer_msg   = $this->input->post('customer_msg');
        $config = Array(
                    'protocol' => 'smtp',
                    /*'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',*/
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $data_email = array(
                        'customer_name'  => $customer_name,
                        'customer_email' => $customer_email,
                        'customer_phone' => $customer_phone,
                        'customer_msg'   => $customer_msg
                    );
        $msg = $this->load->view('public/email/contact_us_email', $data_email,  TRUE);
        $message = $msg;
        $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
        $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        $this->email->to($admin_mail_id);
        $this->email->subject('Notification : Contact us');
        $this->email->message($message);
        $this->email->send();
        $message = 'Your Query sent to admin successfully . We will contact you soon.';
        $sms_status = $this->users_model->common_sms_api($customer_phone, $message);
    }
    function contact_us(){
        $data['user'] = $this->users_model->current();
        $this->load->view('public/contact_us',$data);
    }
    function privacy_policy(){
        $data['user'] = $this->users_model->current();
        $this->load->view('public/privacy_policy',$data);
    }
    function about_us_section(){
        $this->load->helper('url');
        $section_id = $this->uri->segment('2');
        $section    = 'about_us#'.$section_id;
        redirect($section);
    }
    function customer_availability(){
        $this->load->helper('url');
        $customer_response = $this->uri->segment('2');
        $scrap_id          = $this->uri->segment('3');
        $date              = explode("%", $this->uri->segment('4'));
        $time              = $date[1];
        $date              = $date[0];
        if ( ! $this->data['user'])
        {   
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        $user_check = $this->db->select('*')->where(array('id'=>$scrap_id,'user_id'=>$this->data['user']->id))->get('gre_scrap_request')->row()->id;
        $this->db->select('request_type, next_pickup_date, last_pickup_date, requested_date');
        $this->db->where('id',$scrap_id);
        $query = $this->db->get('gre_scrap_request');
        $pickup_date_check = date('Y-m-d H:i:s',strtotime($query->row()->next_pickup_date));
        $pickup_date_check = date('Y-m-d H:i:s', strtotime('-30 minute', strtotime($pickup_date_check)));
        $next_pickup_date = date('Y-m-d',strtotime($query->row()->next_pickup_date));
        $last_pickup_date = date('Y-m-d',strtotime($query->row()->last_pickup_date));
        if (empty($user_check) || $next_pickup_date != $date || $next_pickup_date < date('Y-m-d')){
            $this->data['response'] = '';   
            $this->load->view('public/email/customer_response_mail',$this->data);
        }
        elseif ($next_pickup_date == $last_pickup_date) { 
            if ($pickup_date_check > date('Y-m-d H:i:s')) { 
                $data = array(
                'customer_response' => $customer_response
                );    
                $this->db->where('id', $scrap_id);
                $this->db->update('gre_scrap_request',$data);  
                $this->data['response'] = $customer_response;  
                $this->data['scrap_id'] = $scrap_id;   
                $this->load->view('public/email/customer_response_mail',$this->data);
            }
            else{
                $data = array(
                'customer_response' => 'Late Response'
                );    
                $this->db->where('id', $scrap_id);
                $this->db->update('gre_scrap_request',$data);  
                $this->data['response_status'] = 'Late Response'; 
                $this->data['response'] = $customer_response;  
                $this->data['scrap_id'] = $scrap_id;   
                $message = 'Your response is delayed. Your pick up might not happen on schedule time. Either cancel and book new scrap request or call on our helpline no. 9804940000';
                $sms_status = $this->users_model->common_sms_api($this->data['user']->phone, $message);
                $this->load->view('public/email/customer_response_mail',$this->data);
            }
        }
        else{ 
            if (($pickup_date_check > date('Y-m-d H:i:s')) && ($date > $last_pickup_date)){ 
                $data = array(
                'customer_response' => $customer_response
                );    
                $this->db->where('id', $scrap_id);
                $this->db->update('gre_scrap_request',$data);  
                $this->data['response'] = $customer_response;
                $this->data['scrap_id'] = $scrap_id;     
                $this->load->view('public/email/customer_response_mail',$this->data);
            }
            else{
                $data = array(
                'customer_response' => 'Late Response'
                );    
                $this->db->where('id', $scrap_id);
                $this->db->update('gre_scrap_request',$data);  
                $this->data['response_status'] = 'Late Response'; 
                $this->data['response'] = $customer_response;  
                $this->data['scrap_id'] = $scrap_id;   
                $message = 'Your response is delayed. Your pick up might not happen on schedule time. Either cancel and book new scrap request or call on our helpline no. 9804940000';
                $sms_status = $this->users_model->common_sms_api($this->data['user']->phone, $message);
                $this->load->view('public/email/customer_response_mail',$this->data);
            }
        }
    }    
    
    function apartment_otp(){
        $apartment_otp = $this->input->post('apartment_otp');
        $scrap_id      = $this->input->post('scrap_id');
        if ( ! $this->data['user'])
        {   
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if (!empty($apartment_otp)) {
            $this->db->where('id', $scrap_id);
            $this->db->update('gre_scrap_request',array('apartment_otp' =>$apartment_otp));
            $result = 'yes';
            echo json_encode($result);
        }
        else{
            $result = 'no';
            echo json_encode($result);
        }
    }
    
    function scrap_order_list(){
        if ( ! $this->data['user'])
        {
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $data['scrp_request'] = @$this->session->flashdata('scrp_request');
        $user                 = $this->users_model->current();
        $user_id              = $user->id;
        $data['periodic']     = $this->users_model->scrap_order_list($user_id);
        $data['non_periodic'] = $this->users_model->scrap_order_list1($user_id);
        $data['user']         = $this->users_model->current();
        $this->load->view('public/scrap_request/scrap_order_list',$data);
    }     
    function redeem_request_details(){
        $user            = $this->users_model->current();
        $data['user']    = $this->users_model->current();
        $user_id         = $user->id;
        $data['redeem']  = $this->users_model->redeem_request_details($user_id);
        $this->load->view('public/dashboard/redeem_request_details',$data);
    }     
    function scrap_details_view($scrap_id){
        if ( ! $this->data['user'])
        {
            $this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user       = $this->users_model->current();
        $user_id    = $user->id;
        $data['scrap']  = $this->users_model->scrap_details_view($scrap_id);
        $data['result'] = $this->users_model->archive_history($scrap_id,$user_id);
        
        if($user_id == $data['scrap'][0]->user_id){
        
            $data['user']   = $this->users_model->user_details($user_id);
            // $data['reward_history'] = $this->users_model->reward_history($scrap_id);
           // log_message('debug',print_r($data,true));
            $this->load->view('public/scrap_request/scrap_details_view',$data);
        
        }else{
            redirect('user_dashboard/my-scrap-orders');
        }
    }
    
    //if useris not login want to send inquiry for pre-owned item
    public function my_product_view(){
        $this->load->library('user_agent');
        $this->session->set_userdata('last_page', $this->agent->referrer());
        $this->session->set_flashdata('message','<p class="alert alert-info">Please Login to get details of Product</p>');
        redirect('login');
    }
    
    function my_products_view(){
        if ( ! $this->data['user'])
        {
            //$this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user     = $this->users_model->current();
        $user_id  = $user->id;
        $products = $this->users_model->get_user_product($user_id);
        foreach ($products as $item)
        {
            $category = $this->users_model->get_product_category($item->id);
            $cat_name = $this->db->get_where('gre_my_product_categories', array('id' => $category->category_id))->row()->name;
            $item->category = $cat_name;    
        }
        $data['products'] = $products;
        $data['user']   = $user;
        $this->load->view('public/user_products/user_product_view',$data);
    }
    function view_buyer_inquiry($product_id){
        if ( ! $this->data['user'])
        {
            //$this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_model->get_product_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-products');
            exit();
        }
        
        $inquiries = $this->users_model->get_product_inquiry($product_id);
        $data['product'] = $this->db->get_where('gre_my_product_list', array('id' => $product_id))->row()->item_name;
        $data['inquiries'] = $inquiries;
        $data['user']   = $this->users_model->current();
        $this->load->view('public/user_products/user_product_inquiry',$data);
    }
    
    function check_product_expiry(){
        $products = $this->users_model->get_expired_products();
        //print_r($products);
        
        foreach($products as $key => $product){
            $expiry_date = $product->expiry_datetime;
            
            $due_date = date($expiry_date, strtotime("+7 days"));
            $today_date = date('Y-m-d H:i:s');
            
            /*$old_date = new DateTime($expiry_date);
            $now = new DateTime(Date('Y-m-d H:i:s'));
            $interval = $old_date->diff($now);
            $date_diff = $interval->d;
            */
          
            $due_date = date('Y-m-d', strtotime($due_date));
            $today_date = date('Y-m-d', strtotime($today_date));
            $expiry_date = date('Y-m-d', strtotime($expiry_date));
 
            $diff = abs(strtotime($today_date) - strtotime($expiry_date));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $date_diff = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            
            /*(echo "Due Date :".$due_date."<br />";
            echo "Expiry Date : ".$expiry_date."<br />";
            echo "Today Date : ".$today_date."<br />";
            echo 'date diff - '.$date_diff.'<br /><br />';*/
            
            
            if((strtotime($due_date) < strtotime($today_date)) && $date_diff > 7){
                
                $this->db->set('status', 0);
                $this->db->where('id', $product->id);
                $this->db->update('gre_my_product_list');
                
            }else if($date_diff == 1){
                
                $row = $this->users_model->check_email_verified($product->id);
                
                $data['product_id']   = $product->id;
                $data['product_name'] = $product->item_name;
                $data['username']     = $row->first_name.' '.$row->last_name;
                $data['email_for']    = 'user'; 
                
                if($row->email_verified){
                
                    $this->load->helper('email');
                    // Send email to the seller.
                    $this->email->clear();
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($row->email);
                    $this->email->subject('Product expiry advanced notice');
                    $this->email->message($this->load->view('public/email/product_expiry', $data, TRUE));
                    $this->email->send();
                    log_message('debug','Cron Job :: check_product_expiry :: Product ID('.$product->id.') - mail sent');
                    //echo 'Product ID('.$product->id.') - mail sent';

                    //mail for admin
                    // $data_email = array(
                 //        'email_for'      => 'admin'
                 //    );
                    // $msg1 = $this->load->view('public/email/customer_availability', $data_email,  TRUE);
                    // $message1 = $msg1;
                    // $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    // $this->email->to();
                    // $this->email->subject('Product expiry advanced notice');
                    // $this->email->message($this->load->view('public/email/product_expiry', $data, TRUE));
                    // $this->email->send();
                }   
            }
            
        }
        
    }
    
    function check_wishlist_expiry(){
        $products = $this->users_wishlist_model->get_expired_wishlists();
        //print_r($products);
        
        foreach($products as $key => $product){
            $expiry_date = $product->expiry_datetime;
            
            $due_date = date($expiry_date, strtotime("+7 days"));
            $today_date = date('Y-m-d H:i:s');
            
            /*$old_date = new DateTime($expiry_date);
            $now = new DateTime(Date('Y-m-d H:i:s'));
            $interval = $old_date->diff($now);
            $date_diff = $interval->d;*/
            
            $due_date = date('Y-m-d', strtotime($due_date));
            $today_date = date('Y-m-d', strtotime($today_date));
            $expiry_date = date('Y-m-d', strtotime($expiry_date));
 
            $diff = abs(strtotime($today_date) - strtotime($expiry_date));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $date_diff = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            
            if((strtotime($due_date) < strtotime($today_date)) && $date_diff > 7){
                
                $this->db->set('status', 0);
                $this->db->where('id', $product->id);
                $this->db->update('gre_wishlist_list');
                
            }else if($date_diff == 1){
                
                //log_message('debug','Inside Day Diff');
                
                $row = $this->users_wishlist_model->check_email_verified($product->id);
                
                $data['product_id']   = $product->id;
                $data['product_name'] = $product->item_name;
                $data['username']     = $row->first_name.' '.$row->last_name;
                $data['email_for']    = 'user'; 
                
                if($row->email_verified){
                    
                    log_message('debug','Inside Email Verified');
                    
                    $this->load->helper('email');
                    // Send email to the seller.
                    $this->email->clear();
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($row->email);
                    $this->email->subject('Wishlist expiry advanced notice');
                    $this->email->message($this->load->view('public/email/wishlist_expiry', $data, TRUE));
                    $this->email->send();
                    log_message('debug','Cron Job :: check_wishlist_expiry :: Wishlist ID('.$product->id.') - mail sent');
                    //echo 'Wishlist ID('.$product->id.') - mail sent';

                    //mail for admin
                    // $data_email = array(
                 //        'email_for'      => 'admin'
                 //    );
                    // $msg1 = $this->load->view('public/email/customer_availability', $data_email,  TRUE);
                    // $message1 = $msg1;
                    /*$this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to();
                    $this->email->subject('Wishlist expiry advanced notice');
                    $this->email->message($this->load->view('public/email/wishlist_expiry', $data, TRUE));
                    $this->email->send();*/
                }   
            }
            
        }
        
    }
    
    function add_new_product(){
        if ( ! $this->data['user']){
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user = $this->users_model->current();
        $user_id = $user->id;
        $data['user']   = $this->users_model->current();
        $data['categories'] = $this->users_model->get_product_categories();
        if($this->input->method() === 'post'){
            $this->load->helper('file');
            $this->form_validation->set_rules('name', 'above', 'required|min_length[3]|trim');
            $this->form_validation->set_rules('price', 'above', 'required|numeric|trim');
            $this->form_validation->set_rules('quantity', 'above', 'required|numeric|trim');
            $this->form_validation->set_rules('category', 'above', 'required|trim');
            $this->form_validation->set_rules('product_img', '', 'callback_product_file_check');
            if($this->input->post('email')){
                $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|callback_user_email_check['.$this->data['user']->id.']');
            }    
            if ($this->form_validation->run() == true){
                if($this->input->post('email')){
                    $email = $this->input->post('email');
                    $name  = $this->input->post('user_name');
                    $name  = (explode(" ",$name));
                    $last_name = '';
                    for ($i=0; $i < sizeof($name) ; $i++) {
                        if ($i<1) {
                            $first_name = $name[$i];
                        }
                        else{
                            $last_name = $last_name.' '.$name[$i];
                        }
                    }
                    $data_user = array('last_name'  => $last_name,
                                       'first_name' => $first_name,
                                        'email'     => $email,
                                        'username'  => $email);
                    $this->db->where('id',$this->data['user']->id);
                    $this->db->update('users',$data_user);  
                    $code =  md5($this->input->post('email'));
                    $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $data_email = array(
                                        'code'      => $code,
                                        'email_for' => 'verify'
                                    );
                    $msg = $this->load->view('public/email/mail', $data_email,TRUE);
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('GreenREE email verification');
                    $this->email->message($msg);
                    $this->email->send();
                    $data_email_verify = array('email_verification_code' =>$code,
                        'email_verified' => 0 );
                    $this->db->where('id',$this->data['user']->id);
                    $this->db->update('users',$data_email_verify);
                    $this->session->set_flashdata('alert',
                    array('type' => 'success', 'message' => 'Your Profile has been Updated. Please Verify Your Email.')
                    );
                }
            if($this->input->post('country')){
            $email             = $user->email;
            $country           = $this->input->post('country');
            $state             = $this->input->post('state');
            $city              = $this->input->post('city');
            $locality          = $this->input->post('locality');
            $pin               = @$this->input->post('pin');
            $apartment         = $this->input->post('apartment');
            $flat_no           = $this->input->post('flat_no');
            $this->users_model->update_address($user_id,$country,$state,$city,$locality,$pin,$apartment,$flat_no,$email,$this->input->post('user_name'));  
            }
            $order_from = 'website';
            $product_details = array(
                'item_name'       => $this->input->post('name'),
                'user_id'         => $user->id,
                'slug'            => $this->create_slug($product_id, $this->input->post('name')),
                'description'     => $this->input->post('description'),
                'estimated_price' => $this->input->post('price'),
                'quantity'        => $this->input->post('quantity'),
                'status'          => TRUE,
                'order_from'      => $order_from,
                'expiry_datetime' => date('Y-m-d H:i:s', strtotime("+30 days"))
            );
            $result = $this->users_model->add_user_product($product_details);
            if($result){
                // Handle uploaded image
                if ($_FILES['product_img']['size'] > 0){
                    // Path that the avatar image will be uploaded to
                    $uploadPath = $this->data['app']['file_path_product'];
                    $upload = $this->upload_image('product_img','',$uploadPath);
                    if ( ! $upload['error']){
                        $product_details['images'] = $upload['path'];
                        $image_name = $result.'-'.$product_details['images'];
                        rename($upload['full_path'],$upload['file_path'].$image_name);
                        $this->db->set('images', $image_name);
                        $this->db->where('id', $result);
                        $this->db->update('gre_my_product_list');
                    }
                }  
                $product_category = array(
                    'product_id' => $result,
                    'category_id' => $this->input->post('category')
                );
                $this->db->insert('gre_my_product_category',$product_category);
                $this->session->set_flashdata('message', 'Product has been added successfully!');
                redirect('user_dashboard/my-products');
                exit();
            }else{
                $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                $data['users_address'] = $this->users_model->users_address($user_id);
                $data['countries'] = array();
                $data['states'] = array();
                $data['cities'] = array();
                $data['localities'] = array();
                $data['apartments'] = array();
                if(empty($data['users_address'])){ 
                    $data['countries'] = $this->users_model->get_locations(1,1);
                    //new code for address part start
                    if(sizeof($data['countries']) == 1){
                        $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                        $data['states'] = $this->users_model->get_state($country_id);
                        if(sizeof($data['states']) == 1){
                            $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                            $data['cities'] = $this->users_model->get_city($state_id);
                            if(sizeof($data['cities']) == 1){
                                $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                                $data['localities'] = $this->users_model->get_locality($city_id);
                                if(sizeof($data['localities']) == 1){
                                    $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                                    $data['apartments'] = $this->users_model->get_apartment($locality_id);
                                }
                            }
                        }
                    }
                    //new code for address part end
                }
                $this->load->view('public/user_products/product_add_view', $data);
            }
            }else{
                $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                $data['users_address'] = $this->users_model->users_address($user_id);
                $data['countries'] = array();
                $data['states'] = array();
                $data['cities'] = array();
                $data['localities'] = array();
                $data['apartments'] = array();
                if(empty($data['users_address'])){ 
                    $data['countries'] = $this->users_model->get_locations(1,1);
                    //new code for address part start
                    if(sizeof($data['countries']) == 1){
                        $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                        $data['states'] = $this->users_model->get_state($country_id);
                        if(sizeof($data['states']) == 1){
                            $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                            $data['cities'] = $this->users_model->get_city($state_id);
                            if(sizeof($data['cities']) == 1){
                                $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                                $data['localities'] = $this->users_model->get_locality($city_id);
                                if(sizeof($data['localities']) == 1){
                                    $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                                    $data['apartments'] = $this->users_model->get_apartment($locality_id);
                                }
                            }
                            
                        }
                    }
                    //new code for address part end
                }
                $this->load->view('public/user_products/product_add_view', $data);
            }
        }else{
            $data['users_address'] = $this->users_model->users_address($user_id);
            $data['countries'] = array();
            $data['states'] = array();
            $data['cities'] = array();
            $data['localities'] = array();
            $data['apartments'] = array();
            if(empty($data['users_address'])){ 
                $data['countries'] = $this->users_model->get_locations(1,1);
                    //new code for address part start
                    if(sizeof($data['countries']) == 1){
                        $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                        $data['states'] = $this->users_model->get_state($country_id);
                        if(sizeof($data['states']) == 1){
                            $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                            $data['cities'] = $this->users_model->get_city($state_id);
                            if(sizeof($data['cities']) == 1){
                                $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                                $data['localities'] = $this->users_model->get_locality($city_id);
                                if(sizeof($data['localities']) == 1){
                                    $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                                    $data['apartments'] = $this->users_model->get_apartment($locality_id);
                                }
                            }
                            
                        }
                    }
                    //new code for address part end
            }
            $this->load->view('public/user_products/product_add_view', $data);
        }
    }
    
    function edit_my_product($product_id){
        
        
        if ( ! $this->data['user'])
        {
            //$this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_model->get_product_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-products');
            exit();
        }
        
        $this->data['categories'] = $this->users_model->get_product_categories();
        if($this->input->method() === 'post'){
            $this->load->helper('file');
            $this->form_validation->set_rules('name', 'above', 'required|min_length[3]|trim');
            $this->form_validation->set_rules('category', 'above', 'required|trim');
            if($this->input->post('product_img')){
                $this->form_validation->set_rules('product_img', '', 'callback_product_file_check');
            }
            
            
            if ($this->form_validation->run() == true)
            {
            $updated_from = 'website';
            
            $product_id = $this->input->post('id');
            $old_img = $this->input->post('old_img');
            $product_details = array(
                'item_name'       => $this->input->post('name'),
                'slug'            => $this->create_slug($product_id, $this->input->post('name')),
                'user_id'         => $user_id,
                'description'     => $this->input->post('description'),
                'estimated_price' => $this->input->post('price'),
                'quantity'        => $this->input->post('quantity'),
                'modified_date'   => date('Y-m-d H:i:s'),
                'updated_from'      => $updated_from
            );
            
            $result = $this->users_model->edit_user_product($product_id, $product_details);
            
            if($result){
                
                // Handle uploaded image
                if ($_FILES['product_img']['size'] > 0)
                {
                    $uploadPath = $this->data['app']['file_path_product'];
                    $upload = $this->upload_image('product_img',$old_img,$uploadPath);
    
                    if ( ! $upload['error'])
                    {
                        
                        $product_details['images'] = $upload['path'];
                        $image_name = $product_id.'-'.$product_details['images'];
                        rename($upload['full_path'],$upload['file_path'].$image_name);
                        $this->db->set('images', $image_name);
                        $this->db->where('id', $product_id);
                        $this->db->update('gre_my_product_list');
                    }
                }
                
                $category_id = $this->input->post('category');
                $this->db->set('category_id', $category_id);
                $this->db->where('product_id', $product_id);
                $this->db->update('gre_my_product_category');
                
                $this->session->set_flashdata('message', 'Product has been updated successfully!');
                redirect('user_dashboard/my-products');
                exit();
            }else{
                $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                 $this->data['product'] = $product;
                 $this->data['product_cat'] = $this->users_model->get_product_category($product_id);
                $this->load->view('public/user_products/product_edit_view', $this->data);
            }
            
            }else{
                $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                 $this->data['product'] = $product;
                 $this->data['product_cat'] = $this->users_model->get_product_category($product_id);
                 $this->load->view('public/user_products/product_edit_view', $this->data);
            }
        }else{
            
             $this->data['product'] = $product;
             $this->data['product_cat'] = $this->users_model->get_product_category($product_id);
             $this->load->view('public/user_products/product_edit_view',  $this->data);
        }

    }
    
    public function create_slug($id, $name)
    {
        $count = 0;
        $name = url_title($name);
        $slug_name = $name;  // Create temp name
        while(true) 
        {
            $this->db->select('id');
            $this->db->where('id !=', $id);
            $this->db->where('slug', $slug_name);   // Test temp name
            $query = $this->db->get('gre_my_product_list');
            if ($query->num_rows() == 0) break;
        
            $slug_name = $name . '-' . (++$count);  // Recreate new temp name
            $slug_name = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug_name)));
        }
        return $slug_name;      // Return temp name
    }
    
    function repost_product($product_id){
        
        if ( ! $this->data['user'])
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_model->get_product_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-products');
            exit();
        }
        
        if($product->status == 0){
            $this->session->set_flashdata('message', 'Product has been already expired!');
            redirect('user_dashboard/my-products');
            exit();
        }
    
        $current_expiry = $product->expiry_datetime;
        $this->db->set('expiry_datetime', date('Y-m-d H:i:s', strtotime($current_expiry.' + 30 days')));
        $this->db->where('id', $product_id);
        $this->db->update('gre_my_product_list');
        $this->session->set_flashdata('message', 'Product has been reposted successfully!');
        redirect('user_dashboard/my-products');
        
    }
    
    function edit_repost_product($product_id){
        
        if ( ! $this->data['user'])
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_model->get_product_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-products');
            exit();
        }
        
        if($product->status == 0){
            $this->session->set_flashdata('message', 'Product has been already expired!');
            redirect('user_dashboard/my-products');
            exit();
        }
        
        $this->data['categories'] = $this->users_model->get_product_categories();
        if($this->input->method() === 'post'){
            
            $this->form_validation->set_rules('name', 'above', 'required|min_length[3]|trim');
            $this->form_validation->set_rules('category', 'above', 'required|trim');
            
            if ($this->form_validation->run() == true)
            {
            
            $updated_from = 'website';
            
            $current_expiry = $product->expiry_datetime;
            $product_id = $this->input->post('id');
            $old_img = $this->input->post('old_img');
            $product_details = array(
                'item_name'       => $this->input->post('name'),
                'slug'            => $this->create_slug($product_id, $this->input->post('name')),
                'user_id'         => $user_id,
                'description'     => $this->input->post('description'),
                'estimated_price' => $this->input->post('price'),
                'quantity'        => $this->input->post('quantity'),
                'expiry_datetime' => date('Y-m-d H:i:s', strtotime($current_expiry.' + 30 days')),
                'modified_date'   => date('Y-m-d H:i:s'),
                'updated_from'    => $updated_from
            );
            
            
            $result = $this->users_model->edit_user_product($product_id, $product_details);
            
            if($result){
                
                // Handle uploaded image
                if ($_FILES['product_img']['size'] > 0)
                {
                    $uploadPath = $this->data['app']['file_path_product'];
                    $upload = $this->upload_image('product_img',$old_img,$uploadPath);
    
                    if ( ! $upload['error'])
                    {
                        
                        $product_details['images'] = $upload['path'];
                        $image_name = $product_id.'-'.$product_details['images'];
                        rename($upload['full_path'],$upload['file_path'].$image_name);
                        $this->db->set('images', $image_name);
                        $this->db->where('id', $product_id);
                        $this->db->update('gre_my_product_list');
                    }
                }
                
                $category_id = $this->input->post('category');
                $this->db->set('category_id', $category_id);
                $this->db->where('product_id', $product_id);
                $this->db->update('gre_my_product_category');
                
                $this->session->set_flashdata('message', 'Product has been updated & reposted successfully!');
                $this->load->view('public/user_products/product_edit_repost_view',  $this->data);
                redirect('user_dashboard/my-products');
                exit();
               }else{
                   $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                    $this->data['product'] = $product;
                    $this->data['product_cat'] = $this->users_model->get_product_category($product_id);
                   $this->load->view('public/user_products/product_edit_repost_view',  $this->data);
               }
            }else{
                 $this->data['product'] = $product;
                 $this->data['product_cat'] = $this->users_model->get_product_category($product_id);
                $this->load->view('public/user_products/product_edit_repost_view',  $this->data);
            }
        }else{
            
             $this->data['product'] = $product;
             $this->data['product_cat'] = $this->users_model->get_product_category($product_id);
             $this->load->view('public/user_products/product_edit_repost_view',  $this->data);
        }
        
    }
    
    function close_my_product($product_id){
        
        if ( ! $this->data['user'])
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_model->get_product_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-products');
            exit();
        }
        $this->data['product_id'] = $product_id;
        /**
        *
        * Handle submitted data for closing Product.
        *
         */
        if ($this->input->post('close_product'))
        {
            
            $this->form_validation->set_rules('reason', 'above', 'required|trim');

            if ($this->form_validation->run() == true)
            {
                
                $reason  = $this->input->post('reason');
                $comment = $this->input->post('close-comment');
                $product_id = $this->input->post('prod_id');
                
                $data = array(
                    'status' => 0,
                    'closed_reason' => $reason,
                    'closed_comment' => $comment,
                );
                
                $this->db->where('id',$product_id);
                $status = $this->db->update('gre_my_product_list',$data);
                
                if($status){
                    $this->session->set_flashdata('message', 'Product has been closed successfully!');
                    redirect('user_dashboard/my-products');
                    exit();
                    
                }else{
                    redirect(current_url(), 'refresh');
                }
            }
        }
        
        $this->load->view('public/user_products/close_product_view', $this->data);
        
    }

    //repost wishlist for user    
    function repost_wishlist($product_id){
        
        if ( ! $this->data['user'])
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_wishlist_model->get_wishlist_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-wishlist');
            exit();
        }
        
        if($product->status == 0){
            $this->session->set_flashdata('message', 'Wishlist has been already expired!');
            redirect('user_dashboard/my-products');
            exit();
        }

        $current_expiry = $product->expiry_datetime;
        $this->db->set('expiry_datetime', date('Y-m-d H:i:s', strtotime($current_expiry.' + 30 days')));
        $this->db->where('id', $product_id);
        $this->db->update('gre_wishlist_list');
        $this->session->set_flashdata('message', 'Wishlist has been reposted successfully!');
        redirect('user_dashboard/my-wishlist');
        
    }
    
    //edit & reposting wishlist for user
    function edit_repost_wishlist($product_id){
        
        if ( ! $this->data['user'])
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_wishlist_model->get_wishlist_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-wishlist');
            exit();
        }
        
        if($product->status == 0){
            $this->session->set_flashdata('message', 'Wishlist has been already expired!');
            redirect('user_dashboard/my-products');
            exit();
        }

        
        $this->data['categories'] =  $this->users_wishlist_model->get_wishlist_categories();
        if($this->input->method() === 'post'){
            
            $this->form_validation->set_rules('name', 'above', 'required|min_length[3]|trim');
            $this->form_validation->set_rules('category', 'above', 'required|trim');
            $this->form_validation->set_rules('description', 'above', 'required|trim');


            if ($this->form_validation->run() == true)
            {
            $updated_from = 'website';    
                
            $current_expiry = $product->expiry_datetime;
            $product_id = $this->input->post('id');
            $product_details = array(
                'item_name' => $this->input->post('name'),
                'slug' => '',
                'user_id' => $user_id,
                'description' => $this->input->post('description'),
                'quantity' => $this->input->post('quantity'),
                'expiry_datetime' => date('Y-m-d H:i:s', strtotime($current_expiry.' + 30 days')),
                'modified_date' => date('Y-m-d H:i:s'),
                'updated_from'  => $updated_from
            );
            
            
            $result = $this->users_wishlist_model->edit_user_wishlist($product_id, $product_details);
            
            if($result){
                
                $category_id = $this->input->post('category');
                $this->db->set('category_id', $category_id);
                $this->db->where('product_id', $product_id);
                $this->db->update('gre_wishlist_category');
                
                $this->session->set_flashdata('message', 'Wishlist has been updated & reposted successfully!');
                redirect('user_dashboard/my-wishlist');
                exit();
               }else{
                   $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                   $this->data['product'] = $product;
                   $this->data['product_cat'] = $this->users_wishlist_model->get_wishlist_category($product_id);
                   $this->load->view('public/wishlist/wishlist_edit_repost_view',  $this->data);
               }
            }else{
                $this->data['product'] = $product;
                $this->data['product_cat'] = $this->users_wishlist_model->get_wishlist_category($product_id);
                $this->load->view('public/wishlist/wishlist_edit_repost_view',  $this->data);
            }
        }else{
            
             $this->data['product'] = $product;
             $this->data['product_cat'] = $this->users_wishlist_model->get_wishlist_category($product_id);
             $this->load->view('public/wishlist/wishlist_edit_repost_view',  $this->data);
        }
        
    }

    //closing wishlist of the user
    function close_my_wishlist($product_id){
        
        if ( ! $this->data['user'])
        {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_wishlist_model->get_wishlist_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-wishlist');
            exit();
        }
        $this->data['product_id'] = $product_id;
        /**
        *
        * Handle submitted data for closing Product.
        *
         */
        if ($this->input->post('close_product'))
        {
            $this->form_validation->set_rules('reason', 'above', 'required|trim');
            if ($this->form_validation->run() == true)
            {
                $reason  = $this->input->post('reason');
                $comment = $this->input->post('close-comment');
                $product_id = $this->input->post('prod_id');
                $data = array(
                    'status' => 0,
                    'closed_reason' => $reason,
                    'closed_comment' => $comment,
                );
                $this->db->where('id',$product_id);
                $status = $this->db->update('gre_wishlist_list',$data);
                if($status){
                    $this->session->set_flashdata('message', 'Wishlist has been closed successfully!');
                    redirect('user_dashboard/my-wishlist');
                    exit();
                }else{
                    redirect(current_url(), 'refresh');
                }
            }
        }
        $this->load->view('public/wishlist/close_wishlist_view', $this->data);
    }

    
    /**
     * Category Page.
     * Browse and filter items by category and/or attributes
     */ 
    function my_category($slug = FALSE)
    {
        if ($this->input->post('price_range'))
        {
            $url = current_url().'?'.($this->input->get('price') ? preg_replace('/(^|&)price=[^&]*/', '&price='.$this->input->post('price'), $_SERVER['QUERY_STRING']) : $_SERVER['QUERY_STRING'].'&price='.$this->input->post('price'));
            // redirect to back here.
            redirect($url, 'refresh');
        }
        
        $categories = $this->users_model->menu();

        foreach ($categories as $item)
        {
            $products = $this->users_model->products(array(
                'limit' => 1,
                'order' => 'latest',
                'category_id' => $item->id,
            ));
            $item->products = $products['rows'];    
        }
        $this->data['categories'] = $categories;
        
        $this->data['category'] = $this->users_model->get_categories($slug);
        
        if (! $this->data['category'])
        {

            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');
            $this->load->view('public/errors_view', $this->data);
        }
        else
        {
            $product_options = array(
                'attribute' => $this->input->get('ATB'),
                'order' => 'latest'
            );
            if ($slug) $product_options['category_id'] = $this->data['category']->id;
            
            // Get products.
            $products = $this->users_model->products($product_options);
            
            $this->data['products'] = $products['rows'];    
            $this->data['total'] = $products['total'];  
            $this->data['pagination'] = $products['pagination'];
            
            
            $this->data['min_price'] = $this->users_model->min_price($this->data['category']->id);
            
           
            
            $this->data['max_price'] = $this->users_model->max_price($this->data['category']->id);
            
           
            
            $this->data['price_range'] = ($this->input->get('price')) ? $this->input->get('price') : $this->data['max_price'];
            
            
            
            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');

            $this->load->view('public/user_products/category_view', $this->data);
        }
    }
    
    /**
     * Category Page.
     * Browse and filter items by category and/or attributes
     */ 
    function wishlist_category($slug = FALSE)
    {
        
        $categories =$this->users_wishlist_model->menu();

        foreach ($categories as $item)
        {
            $products = $this->users_wishlist_model->wishlists(array(
                'limit' => 1,
                'order' => 'latest',
                'category_id' => $item->id,
            ));
            $item->products = $products['rows'];    
        }
        $this->data['categories'] = $categories;
        
        $this->data['category'] = $this->users_wishlist_model->get_categories($slug);

        if (! $this->data['category'])
        {

            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');
            $this->load->view('public/errors_view', $this->data);
        }
        else
        {
            $product_options = array(
                'attribute' => $this->input->get('ATB'),
                'order' => 'latest'
            );
            if ($slug) $product_options['category_id'] = $this->data['category']->id;
            
            // Get products.
            $products = $this->users_wishlist_model->wishlists($product_options);
            
            $this->data['products'] = $products['rows'];    
            $this->data['total'] = $products['total'];  
            $this->data['pagination'] = $products['pagination'];
            
            
            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');

            $this->load->view('public/wishlist/category_view', $this->data);
        }
    }
    
    public function otp_check($number, $user_otp){
        if (!empty($number) && !empty($user_otp)) { 
            $var = $this->users_model->verify_register_otp($number,$user_otp);
            if($var == 1){
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
    }
    public function user_product_info(){
        $slug = $this->input->post('slug');
        if (empty($slug)) {
            $result = 1; 
            echo json_encode($result);
        }
        else{
            $result = $this->users_model->get_products_details($slug);
            echo json_encode($result);
        }
    }
    
    /**
     * Product Page.
     * View a single product.
     */
   function my_product($slug)
    {
        $product = $this->users_model->get_products_details($slug);

        $this->data['category'] = $this->users_model->get_categories($product->category->id);
        
        
        /**
        *
        * Handle submitted data for Product inquirues.
        *
         */
        if ($this->input->post('inquiry_product'))
        {
       
            if ( ! $this->data['user'])
            {
                $number   = $this->input->post('phone');
                // For users who are no logged in, require more form details.
                $this->form_validation->set_rules('name', 'above', 'required');
                $this->form_validation->set_rules('email', 'above', 'required|valid_email');
                $this->form_validation->set_rules('phone', 'above', 'required|trim');
                $this->form_validation->set_rules('otp', 'above', 'trim|required|callback_otp_check['.$number.']');
                $this->form_validation->set_message('otp_check','OTP is invalid or expired');
            }
            else
            {
                $user = $this->users_model->get_details($this->data['user']->id);
            }

            if ($this->data['user'])
            {
                // Get users details from the database.
                $this->data['buyer_name']       = (isset($user->first_name)) ? ($user->first_name.' '.$user->last_name) : "";
                $this->data['buyer_email']      = (isset($user->email)) ? $user->email : "";
                $this->data['buyer_phone']      = (isset($user->phone)) ? $user->phone : "";
            }
            else
            {
                // Get users details from the form.
                $this->data['buyer_name']  = $this->input->post('name');
                $this->data['buyer_email'] = $this->input->post('email');
                $this->data['buyer_phone'] = $this->input->post('phone');
            }
            
            $this->data['seller_name'] = $product->first_name.' '.$product->last_name;
            
            $data = array(
                'product_id'   => $product->id,
                'name'         => $this->data['buyer_name'],
                'email'        => $this->data['buyer_email'],
                'phone'        => $this->data['buyer_phone'],
                'created_date' => date('Y-m-d H:i:s')
            );
            $data1 = array(
                'product_id'   => $product->id,
                'name'         => $this->data['buyer_name'],
                'email'        => $this->data['buyer_email'],
                'phone'        => $this->data['buyer_phone'],
                'seller_name'  => $this->data['seller_name'],
                'created_date' => date('Y-m-d H:i:s'),
                'email_for'    => 'user'
            );
            $this->db->select('id');
            $this->db->where('product_id',$product->id);
            $this->db->where('email',$this->data['buyer_email']);
            $query_inquiry_check = $this->db->get('gre_my_product_inquiry');
            if(empty($query_inquiry_check->result())){
                $status = $this->db->insert('gre_my_product_inquiry',$data);
                $this->load->helper('email');
                // Send email to the seller.
                $this->email->clear();
                $this->email->from($this->data['app']['from_mail_id'],'GreenREE');
                $this->email->to($product->email);
                $this->email->subject('Your item ('.$product->item_name.') received an inquiry');
                $this->email->message($this->load->view('public/email/product_inquiry', $data1, TRUE));
                $this->email->send();
                echo json_encode($product);
                exit();
            }else{
                echo json_encode($product);
                exit();
            }
        }
        

        /**
        *
        * Collect all data for the page view file.
        *
         */
        $this->data['variant_selected'] = "";
        $this->data['product_options']  = "";
        $this->data['product_variants'] = "";
        $this->data['$product_defaults'] = "";
        $this->data['variant_thumbs']   = "";
        $this->data['product']          = $product;
        $this->data['random_products']  = "";
        $this->data['related_products'] = "";
        $this->data['variant'] = "";
        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');
        $this->load->view('public/user_products/product_view', $this->data);
    }
    
    function user_products(){
        
        $categories = $this->users_model->menu();

        foreach ($categories as $item)
        {
            $products = $this->users_model->products(array(
                'limit' => 1,
                'category_id' => $item->id
            ));
            $item->products = $products['rows'];
        }
        
        $products = $this->users_model->products(array(
            'limit' => 20,
            'order' => 'latest'
        ));
        
        log_message('debug',print_r($products,TRUE));
        $this->data['products'] = $products['rows'];
        $this->data['categories'] = $categories;
        //$this->data['latest'] = $this->product_model->get_latest_in_category(4);
        
        $this->data['message'] = $this->session->flashdata('message');
        $this->load->view('public/user_products/shop_user_products',$this->data);
    }
    
    function shop(){
        $categories = $this->store->menu();
            
        foreach ($categories as $item)
        {
            $products = $this->store->products(array(
                'limit' => 1,
                'order' => 'latest',
                'category_id' => $item->id
            ));
            $item->products = $products['rows'];    
        }
        
        $products = $this->store->products(array(
            'limit' => 20,
            'featured' => true,
            'order' => 'latest'
        ));
        
        $this->data['products'] = $products['rows'];
        $this->data['categories'] = $categories;
        $this->data['latest'] = $this->product_model->get_latest_in_category(4);
        $this->data['message'] = $this->session->flashdata('message');
        $this->load->view('public/shop/product_sell',$this->data);
        
    }    
    
    function buy_product(){
        $price      = $this->input->post('price');
        $product_id = $this->input->post('product_id');
        $user       = $this->users_model->current();
        $user_id    = $user->id;
        if (!empty($user_id)) {
            if ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            $result = $this->users_model->buy_product($user_id,$product_id,$price);
            echo json_encode($result);
        }
        else{log_message('debug',"result2");
             $result = 3;
             echo json_encode($result);
        }
    } 
    function redeem_request(){
        $suffix  = FALSE;
        $prefix  = "R";
        $user    = $this->users_model->current();
        $user_id = $user->id;
        $email   = $user->email; 
        if (!empty($user_id)) {
            if ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            $redeem_id = $this->flexi_cart_admin_model->generate_order_number($prefix,$suffix,$user_id);
            //log_message('debug',$redeem_id);
            $result = $this->users_model->redeem_request($user_id,$redeem_id);
            if ($result == 2) {
                $data_email = array(
                                'user'              => $user->first_name.' '.$user->last_name,              
                                'redeem_id'         => $redeem_id,
                                'total_wallet_cash' => $user->total_reward_point,
                                'email_for'         => 'user'
                            );
                $data_email1 = array(
                                'redeem_id'         => $redeem_id,
                                'user_id'           => $user->id,
                                'total_wallet_cash' => $user->total_reward_point,
                                'user'              => $user->first_name.' '.$user->last_name,   
                                'email_for'         => 'admin'
                            );
                if($user->email_verified){
                    $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $msg = $this->load->view('public/email/redeem_mail', $data_email,  TRUE);
                    $message = $msg;
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($email);
                    $this->email->subject('Redeem request submitted ('.$redeem_id.')');
                    $this->email->message($message);
                    $this->email->send();
                }
                $message = 'Your Redeem request is submitted successfully. '.'Your Redeem id is '.$redeem_id;
                $sms_status = $this->users_model->common_sms_api($user->phone, $message);
                
                $msg_admin = $this->load->view('public/email/redeem_mail', $data_email1,  TRUE);
                $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($admin_mail_id);
                $this->email->subject('New redeem request submitted ('.$redeem_id.')');
                $this->email->message($msg_admin);
                $this->email->send();
                $message = 'One Redeem request is submitted. Redeem Id is '.$redeem_id.' User name '.$user->first_name.''.$user->last_name.' Email '.$user->email.' Phone '.$user->phone;
                $admin_phone = $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
                $sms_status = $this->users_model->common_sms_api($admin_phone, $message);
            }
            echo json_encode($result);
        }
        else{
             $result = 3;
             echo json_encode($result);
        }
    }     

//checking user address is in our service area or not for notifying user
    function serviceAddressCheck(){
        $user_id = $this->input->post('user_id');
        $newUser = $this->input->post('newUser');
        if($newUser==1){
            $country  = $this->input->post('country');
            $state    = $this->input->post('state');
            $city     = $this->input->post('city');
            $locality = $this->input->post('locality');
            $apartment  = $this->input->post('apartment');
            $country_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $country,'loc_parent_fk'=> 0))->get('locations')->row()->loc_id;
            if(empty($country_id)){
                $result  = 1;
                echo json_encode($result);
                return;
            }
            $state_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $state,'loc_parent_fk'=>$country_id))->get('locations')->row()->loc_id;
            if(empty($state_id)){
                $result  = 2;
                echo json_encode($result);
                return;
            }
            $city_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $city,'loc_parent_fk'=>$state_id))->get('locations')->row()->loc_id;
            if(empty($city_id)){
                $result  = 3;
                echo json_encode($result);
                return;
            }
            $locality_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $locality,'loc_parent_fk'=>$city_id))->get('locations')->row()->loc_id;
            if(empty($locality_id)){
                $result  = 4;
                echo json_encode($result);
                return;
            }
            $apartment_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $apartment,'loc_parent_fk'=>$locality_id))->get('locations')->row()->loc_id;
            if(empty($apartment_id)){
                $result  = 5;
                echo json_encode($result);
                return;
            }
            else{
                $result  = 6;                
                echo json_encode($result);
                return;
            }
        }
        else{
            $result  = $this->location_address_check($user_id);
            echo json_encode($result);
            return;
        }
    }

    //check user address is available or not in locaiotn table
    function location_address_check($user_id){
        $user_address_details = $this->db->select('*')->where('user_id',$user_id)->get('gre_address')->row();
        $country_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $user_address_details->country,'loc_parent_fk'=> 0))->get('locations')->row()->loc_id;
        if(empty($country_id)){
            return 1;
        }
        $state_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $user_address_details->state,'loc_parent_fk'=>$country_id))->get('locations')->row()->loc_id;
        if(empty($state_id)){
            return 2;
        }
        $city_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $user_address_details->city,'loc_parent_fk'=>$state_id))->get('locations')->row()->loc_id;
        if(empty($city_id)){
            return 3;
        }
        $locality_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $user_address_details->locality,'loc_parent_fk'=>$city_id))->get('locations')->row()->loc_id;
        if(empty($locality_id)){
            return 4;
        }
        $apartment_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $user_address_details->apartment_name,'loc_parent_fk'=>$locality_id))->get('locations')->row()->loc_id;
        if(empty($apartment_id)){
            return 5;
        }
        else{
            return 6;
        }
    } 
    function scrap_order_action(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user              = $this->users_model->current();
        $user_id           = $user->id;
        $email             = $user->email;
        $request_action    = $this->input->post('request_action');
        $scrap_id          = $this->input->post('scrap_id');
        $user_name         = $user->first_name;
        $user_name         = $user_name.' '.$user->last_name;
        $request_type      = 'Periodic';
        $this->users_model->scrap_order_action($user_id,$request_action,$scrap_id);
        if($request_action == 1){
            $user_action = 'resume';
            $data_email = array(
                            'scrap_id'     => $scrap_id,
                            'email_for'    => 'user',
                            'request_type' => 'resume',
                            'user_name'    => $user_name
                            );
            $data_email1 = array(
                                'scrap_id'     => $scrap_id,
                                'email_for'    => 'admin',
                                'request_type' => 'resume',
                                'user_name'    => $user_name
                            );
        
        }
        else{
            $user_action = 'paused';
            $data_email = array(
                                'scrap_id'     => $scrap_id,
                                'email_for'    => 'user',
                                'request_type' => 'Periodic',
                                'user_name'    => $user_name
                                );
            $data_email1 = array(
                                'scrap_id'     => $scrap_id,
                                'email_for'    => 'admin',
                                'request_type' => 'Periodic',
                                'user_name'    => $user_name
                            );
        }
        $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $msg = $this->load->view('public/email/update_email', $data_email,  TRUE);
        $message = $msg;
        if($request_type =='Non-Periodic'){
            $request_type_subject = 'One time sell';} 
        else{
            $request_type_subject = 'Periodic';
        }
        if ($user->email_verified) {
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject($request_type_subject.' scrap request update ('.$scrap_id.')');
            $this->email->message($message);
            $this->email->send();
        }
        $msg1 = $this->load->view('public/email/update_email', $data_email1,  TRUE);
        $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
        $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        $this->email->to($admin_mail_id);
        $this->email->subject('One '.$request_type_subject.' scrap request update  ('.$scrap_id.')');
        $this->email->message($msg1);
        $this->email->send();
        $message = 'Your Scrap request is '.$user_action.' Successfully. '.'Your Scrap Id is '.$scrap_id;
        $sms_status = $this->users_model->common_sms_api($user->phone, $message);
        $message = 'One Scrap request is '.$user_action.' Successfully. '.'Scrap Id is '.$scrap_id.' Request type '.$request_type.' User name '.$user->first_name.' '.$user->last_name.' Email '.$user->email.' Phone '.$user->phone;
        $admin_phone = $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
        $sms_status = $this->users_model->common_sms_api($admin_phone, $message);
    }
    function scrap_order_action1(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user              = $this->users_model->current();
        $user_id           = $user->id;
        $email             = $user->email;
        $request_action    = $this->input->post('request_action');
        $scrap_id          = $this->input->post('scrap_id');
        $user_name         = $user->first_name;
        $user_name         = $user_name.' '.$user->last_name;
        $request_type      = 'Non-Periodic';
        $this->users_model->scrap_order_action1($user_id,$request_action,$scrap_id);
        $data_email = array(
                            'scrap_id'     => $scrap_id,
                            'email_for'    => 'user',
                            'request_type' => 'Non-Periodic',
                            'user_name'    => $user_name
                            );
        $data_email1 = array(
                            'scrap_id'     => $scrap_id,
                            'email_for'    => 'admin',
                            'request_type' => 'Non-Periodic',
                            'user_name'    => $user_name
                        );
        $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $msg = $this->load->view('public/email/update_email', $data_email,  TRUE);
        $message = $msg;
        if($request_type =='Non-Periodic'){
            $request_type_subject = 'One time sell';} 
        else{
            $request_type_subject = 'Periodic'; 
        }
        if ($user->email_verified) {
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject($request_type_subject.' scrap request update ('.$scrap_id.')');
            $this->email->message($message);
            $this->email->send();
        }    
        $msg1 = $this->load->view('public/email/update_email', $data_email1,  TRUE);
         $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
        $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        $this->email->to($admin_mail_id);
        $this->email->subject('One '.$request_type_subject.' scrap request update  ('.$scrap_id.')');
        $this->email->message($msg1);
        $this->email->send();
        $message = 'Your Scrap request is cancelled successfully. '.'Your Scrap Id is '.$scrap_id;
        $sms_status = $this->users_model->common_sms_api($user->phone, $message);
        $message = 'One Scrap request is cancelled Successfully. '.'Scrap Id is '.$scrap_id.' Request type '.$request_type.' User name '.$user->first_name.' '.$user->last_name.' Email '.$user->email.' Phone '.$user->phone;
        $admin_phone = $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
        $sms_status = $this->users_model->common_sms_api($admin_phone, $message);
    }
    function update_request(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user                 = $this->users_model->current();
        $email                = $user->email;
        $scrap_id             = $this->input->post('scrap_id');
        $user_id              = $this->input->post('user_id');
        $customer_presence    = $this->input->post('customer_presence');
        $interval             = $this->input->post('interval');
        $request_type         = $this->input->post('request_type');
        $cartoon_boxes        = $this->input->post('cartoon_boxes');
        $paper                = $this->input->post('paper');
        $prefered_pickup_day  = $this->input->post('prefered_pickup_day');
        $prefered_pickup_date = $this->input->post('prefered_pickup_date');
        $scrap_comment        = $this->input->post('scrap_comment');
        $scrap_details        = '';
        if (empty($cartoon_boxes) && empty($paper)) {
            $scrap_details     = '';
        }
        elseif(empty($cartoon_boxes) || empty($paper)){
            if (empty($cartoon_boxes)) {
               $scrap_details = $paper;
            }
            else{
                $scrap_details = $cartoon_boxes;
            }
        }
        else{
            $scrap_details     = $paper.','.$cartoon_boxes; 
        }
        $this->users_model->update_request($user_id,$customer_presence,$interval,$request_type,$scrap_id,$prefered_pickup_day,$prefered_pickup_date,$scrap_comment);
        $data_email = array(
                            'user'                => $user->first_name.' '.$user->last_name,
                            'scrap_id'            => $scrap_id,
                            'request_type'        => $request_type,
                            'prefered_pickup_day' => $prefered_pickup_day,
                            'email_for'           => 'user',
                            'user_name'           => $user_name
                        );
        $data_email1 = array(
                            'user'                => $user->first_name.' '.$user->last_name,
                            'scrap_id'            => $scrap_id,
                            'request_type'        => $request_type,
                            'prefered_pickup_day' => $prefered_pickup_day,
                            'email_for'           => 'admin',
                            'user_name'           => $user_name
                        );
        if($user->email_verified){
            $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'localhost',
                        'smtp_port' => 25,
                        'smtp_auth' => FALSE,
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $msg = $this->load->view('public/email/scrap_update', $data_email,  TRUE);
            $message = $msg;
            if($request_type =='Non-Periodic'){
                $request_type_subject = 'One time sell';} 
            else{
                $request_type_subject = 'Periodic'; }
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($email);
            $this->email->subject($request_type_subject.' scrap request update ('.$scrap_id.')');
            $this->email->message($message);
            $this->email->send();
        }
        $message = 'Your Scrap request is updated Successfully. '.'Your Scrap Id is '.$scrap_id;
        $sms_status = $this->users_model->common_sms_api($user->phone, $message);
        //send information to admin
        $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
        $admin_phone =  $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
        $message = 'One Scrap request is updated Successfully. '.'Scrap Id is '.$scrap_id.' Request type '.$request_type.' User name '.$user->first_name.' '.$user->last_name.' Email '.$user->email.' Phone '.$user->phone;
        $sms_status = $this->users_model->common_sms_api($admin_phone, $message);
        $msg1 = $this->load->view('public/email/scrap_update', $data_email1,  TRUE);
        $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        $this->email->to($admin_mail_id);
        $this->email->subject('One '.$request_type_subject.' scrap request update ('.$scrap_id.')');
        $this->email->message($msg1);
        $this->email->send();
    }     
    function login_check(){
        $user = $this->users_model->current();
        if(empty($user->first_name)){
            $var = "yes";
            exit();
        }
        else{
            $var = "no";
            exit();
        }
    }
    function update_scrap(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $info["user"] = $this->users_model->current();
        $scrap_id     = $this->input->post('scrap_id');
        
        $result = '';
        $info['scrap_details'] = $this->users_model->scrap_details_view($scrap_id);
        
        if(($info['scrap_details'][0]->request_status == 'Placed' || $info['scrap_details'][0]->request_status == 'Under Review' || $info['scrap_details'][0]->request_type == 'Periodic')){
	        $updated_from = 'website';
	        $data = array('periodic_interval'   => $this->input->post('interval'),
	                      'request_type'        => $this->input->post('request_type'),
	                      'customer_presence'   => $this->input->post('customer_presence'),
	                      'scrap_comment'   	=> $this->input->post('scrap_comment'),
	                      'prefered_pickup_day' => $this->input->post('prefered_pickup_day'),
	                      'prefered_pickup_date'=> $this->input->post('prefered_pickup_date'),
	                      'prefered_pickup_date_next'=> $this->input->post('prefered_pickup_date'),
	                      'updated_from'        => $updated_from
	                );
	        $this->db->where('id',$scrap_id);
	        $result = $this->db->update('gre_scrap_request',$data);
        }
        
        if($info['scrap_details'][0]->request_status == 'Canceled'){
			$result = 'Canceled';
		}
		
		if($info['scrap_details'][0]->request_status == 'Complete'){
			$result = 'Complete';
		}
		
		if($info['scrap_details'][0]->request_status == 'Process'){
			$result = 'Process';
		}
        
        echo $result;
    }
    function update_scrap_request(){
        if ( ! $this->data['user']){
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $info["user"] = $this->users_model->current();
        $this->load->helper('url');
        $info['scrap_id']      = $this->uri->segment('2');
        $info['scrap_details'] = $this->users_model->scrap_details_view($info['scrap_id']);
        if($info["user"]->id == $info['scrap_details'][0]->user_id && ($info['scrap_details'][0]->request_status == 'Placed' || $info['scrap_details'][0]->request_status == 'Under Review' || $info['scrap_details'][0]->request_type == 'Periodic')){
            $info['users_address'] = $this->users_model->users_address($info["user"]->id );
            $info['periodic']     = $this->users_model->scrap_periodic_id($info["user"]->id);
            $user_pickup_frequency = $this->db->select('periodic_interval')->where('id',$info['periodic']['0']->id)->get('gre_scrap_request')->row()->periodic_interval/7;
            $pickup_frequency = $this->db->select('*')->get('gre_periodic_frequency_config')->result();
            $info['pickup_frequency'] = '';
            foreach ($pickup_frequency as $frequency) {
                if ($frequency->frequency_value == $user_pickup_frequency) {
                    $info['pickup_frequency'] .= '<option value="'.$frequency->frequency_value.'" selected>'.$frequency->frequency_name.'</option>';
                }
                else{
                    $info['pickup_frequency'] .= '<option value="'.$frequency->frequency_value.'">'.$frequency->frequency_name.'</option>';
                }
            }
            
            $info['countries'] = array();
            $info['states'] = array();
            $info['cities'] = array();
            $info['localities'] = array();
            $info['apartments'] = array();
        
            if(empty($info['users_address'])){ 
                $info['countries'] = $this->users_model->get_locations(1,1);
                if(sizeof($info['countries']) == 1){
                    $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                    $info['states'] = $this->users_model->get_state($country_id);
                    if(sizeof($info['states']) == 1){
                        $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                        $info['cities'] = $this->users_model->get_city($state_id);
                        if(sizeof($info['cities']) == 1){
                            $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                            $info['localities'] = $this->users_model->get_locality($city_id);
                            if(sizeof($info['localities']) == 1){
                                $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                                $info['apartments'] = $this->users_model->get_apartment($locality_id);
                            }
                        }
                        
                    }
                }
                //for displaying calender setting
                $info['pickup_config_date'] = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_date_config')->result();
                $info['pickup_config_days'] = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_config')->result();
            }
            else{
                //for getting user's address ids
                $country_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->country))->get('locations')->row()->loc_id;
                $state_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->state,'loc_parent_fk'=>$country_id))->get('locations')->row()->loc_id;
                $city_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->city,'loc_parent_fk'=>$state_id))->get('locations')->row()->loc_id;
                $locality_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->locality,'loc_parent_fk'=>$city_id))->get('locations')->row()->loc_id;
                $apartment_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->apartment_name,'loc_parent_fk'=>$locality_id))->get('locations')->row()->loc_id;
                //serching holidays for apartment
                $data_general_date = $this->db->select('*')->where(array('greenree_id'=>1,'general_setting_global'=>1))->get('gre_pickup_date_config')->result();
                $data_general_days = $this->db->select('*')->where(array('greenree_id'=>1,'general_setting_global'=>1))->get('gre_pickup_config')->result();

                $data_locality_date = $this->db->select('*')->where(array('locality_id'=>$locality_id,'general_setting_global'=>1))->get('gre_pickup_date_config')->result();
                $data_locality_days = $this->db->select('*')->where(array('locality_id'=>$locality_id,'locality_setting_global'=>1,'apartment_id'=>0))->get('gre_pickup_config')->result();
                $pickup_config_days = array_merge($data_general_days,$data_locality_days);
                $info['pickup_config_date'] = array_merge($data_general_date,$data_locality_date);
                $data_apartment_date = $this->db->select('*')->where('apartment_id', $apartment_id)->get('gre_pickup_date_config')->result();
                $data_apartment_days = $this->db->select('*')->where('apartment_id', $apartment_id)->get('gre_pickup_config')->result();

                $info['pickup_config_days'] = array_merge($pickup_config_days,$data_apartment_days);
                $info['pickup_config_date'] = array_merge($data_general_date,$data_apartment_date);
            }
            $this->load->view('public/scrap_request/scrap_request_view',$info);
        }
        else{
            redirect('user_dashboard/my-scrap-orders');
        }
    }
    function address_check(){
        $user_id    = $this->input->post('user_id'); 
        $var_return = $this->users_model->address_check($user_id);
        if ($var_return=="yes") {
            echo "yes";
            exit();
        }
        else{
            echo "no";
            exit();
        }
    }
    
    //loading calender for scrap request page part is start
    function load_calnder(){
        $country_id   = $this->input->post('country_id');
        $state_id     = $this->input->post('state_id');
        $city_id      = $this->input->post('city_id');
        $locality_id  = $this->input->post('locality_id');
        $apartment_id = $this->input->post('apartment_id');
        //checking email is unique or not
        $result['email_checked'] = $this->db->select('*')->where('email',$this->input->post('email'))->where('id !=',$this->data['user']->id)->get('users')->num_rows();
        //serching holidays for apartment
        $data_general_date = $this->db->select('*')->where(array('greenree_id'=>1,'general_setting_global'=>1))->get('gre_pickup_date_config')->result();
        $data_general_days = $this->db->select('*')->where(array('greenree_id'=>1,'general_setting_global'=>1))->get('gre_pickup_config')->result();

        $data_locality_date = $this->db->select('*')->where(array('locality_id'=>$locality_id,'general_setting_global'=>1))->get('gre_pickup_date_config')->result();
        $data_locality_days = $this->db->select('*')->where(array('locality_id'=>$locality_id,'locality_setting_global'=>1,'apartment_id'=>0))->get('gre_pickup_config')->result();
        $pickup_config_days = array_merge($data_general_days,$data_locality_days);
        $pickup_config_date = array_merge($data_general_date,$data_locality_date);
        $data_apartment_date = $this->db->select('*')->where('apartment_id', $apartment_id)->get('gre_pickup_date_config')->result();
        $data_apartment_days = $this->db->select('*')->where('apartment_id', $apartment_id)->get('gre_pickup_config')->result();

        $result['pickup_config_days'] = array_merge($pickup_config_days,$data_apartment_days);
        $result['pickup_config_date'] = array_merge($data_general_date,$data_apartment_date);
        echo json_encode($result);
    }
    //loading calender for scrap request page part is start
    
    function scrap_request(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user                 = $this->users_model->current();
        $email                = $user->email; 
        $user_id              = $this->input->post('user_id');
        $customer_presence    = $this->input->post('customer_presence');
        $request_type         = $this->input->post('request_type');
        $interval             = $this->input->post('interval');
        $paper                = $this->input->post('paper');
        $cartoon_boxes        = $this->input->post('cartoon_boxes');
        $scrap_details        = '';
        $prefered_pickup_day  = $this->input->post('prefered_pickup_day');
        $prefered_pickup_date = $this->input->post('prefered_pickup_date');
        $scrap_comment        = $this->input->post('scrap_comment');
        
        if (empty($cartoon_boxes) && empty($paper)) {
            $scrap_details     = '';
        }
        elseif(empty($cartoon_boxes) || empty($paper)){
            if (empty($cartoon_boxes)) {
               $scrap_details = $paper;
            }
            else{
                $scrap_details = $cartoon_boxes;
            }
        }
        else{
            $scrap_details     = $paper.','.$cartoon_boxes; 
        }
        $suffix = FALSE;
        $prefix = "S";
        $prefix_status = 1;
        $scrap_id = $this->flexi_cart_admin_model->generate_order_number($prefix,$suffix,$user_id,$prefix_status);
        $result['results'] = $this->users_model->scrap_request($user_id,$customer_presence,$request_type,$interval,$scrap_id,$prefered_pickup_day,$prefered_pickup_date,$scrap_comment);
        $result['scrap_id'] = $scrap_id;
        //checking user address is different from our working area
        $check_other_address = $this->location_address_check($user->id);
        if($check_other_address<6){
            $result['check_other_address'] = $check_other_address;
            if($check_other_address==1){
                $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
            }
            if($check_other_address==2){
                $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
            }
            if($check_other_address==3){
                $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
            }
            if($check_other_address==4){
                $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
            }
            if($check_other_address==5){
                $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
            }
        }
        else{
            $result['check_other_address'] = $check_other_address;
        }
         //for sending mail
        if($result){
            $users_address = $this->db->select('*')->where('user_id',$user_id)->get('gre_address')->row();
            $data_email = array(
                                'user'                => $user->first_name.' '.$user->last_name,
                                'scrap_id'            => $scrap_id,
                                'request_type'        => $request_type,
                                'prefered_pickup_day' => $prefered_pickup_day,
                                'email_for'           => 'user',
                                'check_other_address' => $check_other_address
                            );
            $data_email1 = array(
                        'user'                => $user->first_name.' '.$user->last_name,
                        'scrap_id'            => $scrap_id,
                        'request_type'        => $request_type,
                        'locality'            => $users_address->locality,
                        'apartment_name'      => $users_address->apartment_name,
                        'flat_no'             => $users_address->flat_no,
                        'city'                => $users_address->city,
                        'prefered_pickup_day' => $prefered_pickup_day,
                        'email_for'           => 'admin',
                        'check_other_address' => $check_other_address
                            );
            if($user->email_verified){
                if($result['results'] == 4){
                   $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $msg = $this->load->view('public/email/scrap_submited_mail', $data_email,  TRUE);
                    $message = $msg;
                    if($request_type =='Non-Periodic'){
                        $request_type_subject = 'One time sell';}
                    else{
                        $request_type_subject = 'Periodic';
                    }    
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($email);
                    $this->email->subject($request_type_subject.' scrap request submitted ('.$scrap_id.')');
                    $this->email->message($message);
                    $this->email->send();
                }
            }
            if($check_other_address<6){
                $message = 'Your scrap request is under review. You will be notified once approved by GreenREE admin';
                $admin_message = 'New Scrap request is submitted and its address is different from our operational Area. Scrap request id  '.$scrap_id.', Type '.$request_type.', pickup '.$prefered_pickup_date.', name '.$user->first_name.', Ph '.$user->phone.', Apt '.$users_address->apartment_name;
            }
            else{
                $message = 'Your Scrap Request is Successfully Submitted. Your Scrap Request Id is '.$scrap_id;
                $admin_message = 'Type '.$request_type.', Pick up '.$prefered_pickup_date.', Pickup day '.$prefered_pickup_day.', Name '.$user->first_name.', Phone '.$user->phone.', Apartment '.$users_address->apartment_name.', Flat '.$users_address->flat_no;
            }
            $sms_status = $this->users_model->common_sms_api($user->phone, $message);
            //Admin information part
            $msg1 = $this->load->view('public/email/scrap_submited_mail', $data_email1,  TRUE);
            $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
            $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
            $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
            $this->email->to($admin_mail_id);
            $this->email->subject('New '.$request_type_subject.' scrap request submitted ('.$scrap_id.')');
            $this->email->message($msg1);
            $this->email->send();
            $admin_phone =  $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
            $admin_sms_status = $this->users_model->common_sms_api($admin_phone, $admin_message);
        }
        //mail part end
        echo json_encode($result);
    } 
    function request_scrap(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
            }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user              = $this->users_model->current();
        $user_id           = $user->id;    
        $email             = trim(@$this->input->post('email'));
        $name              = trim(@$this->input->post('name'));
        $interval          = $this->input->post('interval');
        $user_id           = trim($this->input->post('user_id'));
        $country           = trim($this->input->post('country'));
        $state             = trim($this->input->post('state'));
        $city              = trim($this->input->post('city'));
        $locality          = trim($this->input->post('locality'));
        $pin               = $this->input->post('pin');
        $apartment         = trim($this->input->post('apartment'));
        $flat_no           = trim($this->input->post('flat_no'));
        $customer_presence = $this->input->post('customer_presence');
        $request_type      = $this->input->post('request_type');
        $paper             = $this->input->post('paper');
        $cartoon_boxes     = $this->input->post('cartoon_boxes');
        $prefered_pickup_day  = $this->input->post('prefered_pickup_day');
        $prefered_pickup_date = $this->input->post('prefered_pickup_date');
        $scrap_comment        = $this->input->post('scrap_comment');
        $scrap_details        = '';
        if (empty($cartoon_boxes) && empty($paper)) {
            $scrap_details     = '';
        }
        elseif(empty($cartoon_boxes) || empty($paper)){
            if (empty($cartoon_boxes)) {
               $scrap_details = $paper;
            }
            else{
                $scrap_details = $cartoon_boxes;
            }
        }
        else{
            $scrap_details     = $paper.','.$cartoon_boxes; 
        }  
        $suffix = FALSE;
        $prefix = "S";
        $prefix_status = 1;
        $email_check_status = 0;
        $query  = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows()){
            $email_check_status = 1;
            $query1 = $this->db->get_where('users', array('email' => $email,'id' => $user->id));
            if($query1->num_rows()){
                $email_check_status =2;
            }
        }
        if($email_check_status != 1){
            $name = (explode(" ",$name));
            $last_name = '';
            for ($i=0; $i < sizeof($name) ; $i++) {
                if ($i<1) {
                    $first_name = $name[$i];
                }
                else{
                    $last_name = $last_name.' '.$name[$i];
                }
            }
            $pre_update_email = $this->db->select('email')->where('id',$user->id)->get('users')->row()->email;
            $data = array('first_name' => $first_name,
                          'last_name'  => $last_name,  
                          'email'      => $email,
                          'username'   => $email  
                         );
            $this->db->where('id',$user->id);
            $this->db->update('users',$data);
            
            $scrap_id = $this->flexi_cart_admin_model->generate_order_number($prefix,$suffix,$user_id,$prefix_status);
            $this->users_model->update_address($user_id,$country,$state,$city,$locality,$pin,$apartment,$flat_no,$email,$name);
            $result['results'] = $this->users_model->scrap_request_list($user_id,$customer_presence,$request_type,$interval,$scrap_id,$prefered_pickup_day,$prefered_pickup_date,$scrap_comment);
            $result['scrap_id'] = $scrap_id;
            //checking user address is different from our working area
            $check_other_address = $this->location_address_check($user->id);
            if($check_other_address<6){
                $result['check_other_address'] = $check_other_address;
                if($check_other_address==1){
                    $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
                }
                if($check_other_address==2){
                    $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
                }
                if($check_other_address==3){
                    $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
                }
                if($check_other_address==4){
                    $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
                }
                if($check_other_address==5){
                    $this->db->where('id',$scrap_id)->update('gre_scrap_request',array('periodic_request_status' => 0,'request_status' => 'Under Review'));
                }
            }
            else{
                $result['check_other_address'] = $check_other_address;
            }
            //for sending mail
            if($result['results'] == 4){
                //send email for verifying email address
                if($pre_update_email != $email){
                    $code = md5($email);
                    $data_email = array('email_verification_code' => $code );
                    $this->db->where('id',$user->id);
                    $this->db->update('users',$data_email);
                    
                    $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $data_email = array(
                                            'code'      => $code,
                                            'email_for' => 'verify'
                                        );
                    $msg = $this->load->view('public/email/mail', $data_email,TRUE);
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('GreenREE email verification');
                    $this->email->message($msg);
                    $this->email->send();
                }
                $data_email = array(
                                'scrap_id'            => $scrap_id,
                                'request_type'        => $request_type,
                                'prefered_pickup_day' => $prefered_pickup_day,
                                'email_for'           => 'user',
                                'check_other_address' => $check_other_address,
                                );
                $data_email1 = array(
                                'scrap_id'            => $scrap_id,
                                'request_type'        => $request_type,
                                'prefered_pickup_day' => $prefered_pickup_day,  
                                'locality'            => $locality,
                                'apartment_name'      => $apartment,
                                'flat_no'             => $flat_no,
                                'email_for'           => 'admin',
                                'city'                => $city,
                                'check_other_address' => $check_other_address,
                                );
                $config = Array(
                                'protocol'  => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                if($request_type =='Non-Periodic'){
                    $request_type_subject = 'One time sell';}
                else{
                    $request_type_subject = 'Periodic';
                }   
                if($user->email_verified){                
                    $msg = $this->load->view('public/email/scrap_submited_mail', $data_email,  TRUE);
                    $message = $msg; 
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($email);
                    $this->email->subject($request_type_subject.' scrap request submitted ('.$scrap_id.')');
                    $this->email->message($message);
                    $this->email->send();
                }
                if($check_other_address<6){
                    $message = 'Your scrap request is under review. You will be notified once approved by GreenREE admin';
                    $admin_message = 'New Scrap request is submitted and its address is different from our operational Area. Scrap id  '.$scrap_id.', Type '.$request_type.', pickup '.$prefered_pickup_date.', Name '.$user->first_name.', Ph '.$user->phone.', Apt '.$users_address->apartment_name;
                }
                else{
                    $message = 'Your Scrap Request is Successfully Submitted. Your Scrap Request Id is '.$scrap_id;
                    $admin_message = 'Type '.$request_type_subject.', Pickup date '.$prefered_pickup_date.', Pickup day '.$prefered_pickup_day.', Name '.$first_name.', Phone '.$user->phone.', Apartment '.$apartment.', Flat '.$flat_no;
                }
                $sms_status = $this->users_model->common_sms_api($user->phone, $message);
                //admin information for scrap request
                $msg1 = $this->load->view('public/email/scrap_submited_mail', $data_email1,  TRUE);
                 $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($admin_mail_id);
                $this->email->subject('New '.$request_type_subject.' scrap request submitted ('.$scrap_id.')');
                $this->email->message($msg1);
                $this->email->send();
                $admin_phone =  $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
                $admin_sms_status = $this->users_model->common_sms_api($admin_phone, $admin_message);
            }
        }
        else{
            $result['results'] = 0;
        }
        // //mail part end
        echo json_encode($result);
    }
    function state_list(){
        $country = $this->input->post('country');
        $var1    =$this->users_model->state_list($country);
        $count   = 0;

        $arr=array();
        foreach ($var1->result() as $row){
           $arr[$count] = $row->state; 
           $count++;
        }
        echo json_encode($arr);
        exit();         
    }
    function city_list(){
        $state = $this->input->post('state');
        $var1  = $this->users_model->city_list($state);
        $count = 0;
        $arr=array();
        foreach ($var1->result() as $row){
           $arr[$count] = $row->city; 
           $count++;
        }
        echo json_encode($arr);
        exit();         
    }
    function locality_list(){
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $var1 =$this->users_model->locality_list($city,$state);
                    $count = 0;
                    $arr=array();
                    foreach ($var1->result() as $row){
                       $arr[$count] = $row->city; 
                       $count++;
                    }
        echo json_encode($arr);
        exit();         
    }
    function apartment_list(){
        $city     = $this->input->post('city');
        $state    = $this->input->post('state');
        $country  = $this->input->post('city');
        $locality = $this->input->post('state');
        $var1 =$this->users_model->apartment_list($city,$state,$locality,$country);
                    $count = 0;
                    $arr=array();
                    foreach ($var1->result() as $row){
                       $arr[$count] = $row->city; 
                       $count++;
                    }
        echo json_encode($arr);
        exit();         
    }
    function wishlist_info_login(){
        $current_url = base_url('wishlist');
        $this->session->set_userdata('last_page', $current_url);
        $this->session->set_flashdata('message','<p class="alert alert-info">Please Login to get details of wishlist</p>');
        redirect('login');
    }
    function wishlist_user_info(){
        if ( ! $this->data['user']){
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $users_data    = $this->users_model->current();
        $customer_data = $this->users_model->user_details($this->input->post('user_id'));
        $wishlist_item = $this->input->post('wishlist');
        $customer_address = $this->users_model->users_address($this->input->post('user_id'));
        $users_info = array(
                       'item' => $wishlist_item,
                       'seller_name'    => $users_data->first_name,
                       'customer_name'  => $customer_data['0']->first_name,
                       'phone'          => $customer_data['0']->phone,
                       'email'          => $customer_data['0']->email,
                       'locality'       => $customer_address['0']->locality,
                       'city'           => $customer_address['0']->city,
                       'state'          => $customer_address['0']->state,
                       'country'        => $customer_address['0']->country,
                       'pin'            => $customer_address['0']->pin,
                       'apartment_name' => $customer_address['0']->apartment_name
                      );
        //for sending mail
        $config = Array(
                    'protocol' => 'smtp',
                    /*'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',*/
                    'smtp_host' => 'localhost',
                    'smtp_port' => 25,
                    'smtp_auth' => FALSE,
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $email = $users_data->email;
        $msg   = $this->load->view('public/email/customer_info_mail', $users_info,  TRUE);
        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        $this->email->to($email);
        $this->email->subject('Customer information for selling products');
        $this->email->message($msg);
        $this->email->send();
        $message = 'Your Wishlist is checked by a seller. Your wishlist is '.$wishlist_item;
        $sms_status = $this->users_model->common_sms_api($user->phone, $message);
        echo json_encode($users_info);
    }
    function wishlist(){
        if ( ! $this->data['user']){
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user     = $this->users_model->current();
        $user_id  = $user->id;
        $products = $this->users_wishlist_model->get_user_wishlist($user_id);
        foreach ($products as $item)
        {
            $category = $this->users_wishlist_model->get_wishlist_category($item->id);
            $cat_name = $this->db->get_where('gre_wishlist_categories', array('id' => $category->category_id))->row()->name;
            $item->category = $cat_name;    
        }
        $data['products'] = $products;
        $data['user']   = $this->users_model->current();
        $this->load->view('public/wishlist/wishlist',$data);
    }
    function wishlist_add_view(){
        if ( ! $this->data['user']){
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $data['user']   = $this->users_model->current();
        $data['categories'] = $this->users_wishlist_model->get_wishlist_categories();
        $products = $this->users_wishlist_model->get_user_wishlist($data['user']->id);
        foreach ($products as $item){
            $category = $this->users_wishlist_model->get_wishlist_category($item->id);
            $cat_name = $this->db->get_where('gre_wishlist_categories', array('id' => $category->category_id))->row()->name;
            $item->category = $cat_name;    
        }
        $data['products'] = $products;
        $data['users_address'] = $this->users_model->users_address($data['user']->id);
        $data['countries'] = array();
        $data['states'] = array();
        $data['cities'] = array();
        $data['localities'] = array();
        $data['apartments'] = array();
        if(empty($data['users_address'])){ 
            $data['countries'] = $this->users_model->get_locations(1,1);
            //new code for address part start
            if(sizeof($data['countries']) == 1){
                $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                $data['states'] = $this->users_model->get_state($country_id);
                if(sizeof($data['states']) == 1){
                    $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                    $data['cities'] = $this->users_model->get_city($state_id);
                    if(sizeof($data['cities']) == 1){
                        $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                        $data['localities'] = $this->users_model->get_locality($city_id);
                        if(sizeof($data['localities']) == 1){
                            $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                            $data['apartments'] = $this->users_model->get_apartment($locality_id);
                        }
                    }
                    
                }
            }
            //new code for address part end
        }
        $this->load->view('public/wishlist/wishlist_add_view',$data);
    }
    function common_wishlist_view(){
        $this->load->model('users_wishlist_model');
        $categories = $this->users_wishlist_model->menu();

        foreach ($categories as $item){
            $products = $this->users_wishlist_model->wishlists(array(
                'limit' => 1,
                'category_id' => $item->id,
            ));
            $item->products = $products['rows'];    
        }
        
        $products = $this->users_wishlist_model->wishlists(array(
            'limit' => 30,
            'order' => 'latest'
        ));
        
        $this->data['products'] = $products['rows'];
        
        $user     = $this->users_model->current();
        if ($user) {
            $this->data['user'] = $this->users_model->current();
        }
        $this->data['categories'] = $categories;
        //$this->data['latest'] = $this->product_model->get_latest_in_category(4);
        
        $this->data['message'] = $this->session->flashdata('message');
        $this->load->view('public/wishlist/common_wishlist_view',$this->data);
    }
    function add_new_wishlist(){
        if ( ! $this->data['user'])
        {
            //$this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user         = $this->users_model->current();
        $user_id      = $user->id;
        $data['user'] = $this->users_model->user_details($user_id);
        $data['categories'] = $this->users_wishlist_model->get_wishlist_categories();
        if($this->input->method() === 'post'){
            
            $this->form_validation->set_rules('name', 'above', 'required|min_length[3]|trim');
            $this->form_validation->set_rules('category', 'above', 'required|trim');
            //$this->form_validation->set_rules('description', 'above', 'required|trim');
            if($this->input->post('email')){
                $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|callback_user_email_check['.$this->data['user']->id.']');
            }
            
            if ($this->form_validation->run() == true)
            {
                if($this->input->post('email')){
                    $email = $this->input->post('email');
                    $name  = $this->input->post('user_name');
                    $name  = (explode(" ",$name));
                    $last_name = '';
                    for ($i=0; $i < sizeof($name) ; $i++) {
                        if ($i<1) {
                            $first_name = $name[$i];
                        }
                        else{
                            $last_name = $last_name.' '.$name[$i];
                        }
                    }
                    $data_user = array('last_name'  => $last_name,
                                       'first_name' => $first_name,
                                        'email'     => $email,
                                        'username'  => $email);
                    $this->db->where('id',$this->data['user']->id);
                    $this->db->update('users',$data_user);  
                    $code =  md5($this->input->post('email'));
                    $config = Array(
                                'protocol' => 'smtp',
                                /*'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => $this->data['app']['from_mail_id'],
                                'smtp_pass' => 'My123abc',*/
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $data_email = array(
                                        'code'      => $code,
                                        'email_for' => 'verify'
                                    );
                    $msg = $this->load->view('public/email/mail', $data_email,TRUE);
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('GreenREE email verification');
                    $this->email->message($msg);
                    $this->email->send();
                    $data_email_verify = array('email_verification_code' =>$code,
                        'email_verified' => 0 );
                    $this->db->where('id',$this->data['user']->id);
                    $this->db->update('users',$data_email_verify);
                    $this->session->set_flashdata('alert',
                    array('type' => 'success', 'message' => 'Your Profile has been Updated. Please Verify Your Email.')
                    );
                }
                if($this->input->post('country')){           
                    $country           = $this->input->post('country');
                    $state             = $this->input->post('state');
                    $city              = $this->input->post('city');
                    $locality          = $this->input->post('locality');
                    $pin               = @$this->input->post('pin');
                    $apartment         = $this->input->post('apartment');
                    $flat_no           = $this->input->post('flat_no');
                
                    $this->users_model->update_address($user_id,$country,$state,$city,$locality,$pin,$apartment,$flat_no,$email,$name);
                }    
                $order_from = 'website';
                
                $product_details = array(
                    'item_name'       => $this->input->post('name'),
                    'user_id'         => $user->id,
                    'slug'            => '',
                    'description'     => $this->input->post('description'),     
                    'order_from'      => $order_from,
                    'quantity'        => $this->input->post('quantity'),
                    'status'          => TRUE,
                    'expiry_datetime' => date('Y-m-d H:i:s', strtotime("+30 days"))
                );
            
                $result = $this->users_wishlist_model->add_user_wishlist($product_details);
            
                if($result){
                    $wishlist_category = array(
                        'product_id' => $result,
                        'category_id' => $this->input->post('category')
                    );
                    $this->db->insert('gre_wishlist_category',$wishlist_category);
                    
                    $this->session->set_flashdata('message', 'Product has been added successfully!');
                    
                    redirect('user_dashboard/my-wishlist');
                    exit();
                }
                else{
                    $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                    $data['users_address'] = $this->users_model->users_address($user_id);
                
                    $data['countries'] = array();
                    $data['states'] = array();
                    $data['cities'] = array();
                    $data['localities'] = array();
                    $data['apartments'] = array();
                    
                    if(empty($data['users_address'])){ 
                     
                        $data['countries'] = $this->users_model->get_locations(1,1);
                        if(sizeof($data['countries']) == 1){
                            $data['states'] = $this->users_model->get_locations(2,1);
                            if(sizeof($data['states']) == 1){
                                $data['cities'] = $this->users_model->get_locations(3,1);
                                if(sizeof($data['cities']) == 1){
                                    $data['localities'] = $this->users_model->get_locations(4,1);
                                    if(sizeof($data['localities']) == 1){
                                        $data['apartments'] = $this->users_model->get_locations(5,1);
                                    }
                                }
                                
                            }
                        }
                    }
                    
                    $this->load->view('public/wishlist/wishlist_add_view', $data);
                }
            }
            else{
                $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                
                $data['users_address'] = $this->users_model->users_address($user_id);
                $data['countries'] = array();
                $data['states'] = array();
                $data['cities'] = array();
                $data['localities'] = array();
                $data['apartments'] = array();
                
                if(empty($data['users_address'])){ 
                 
                    $data['countries'] = $this->users_model->get_locations(1,1);
                    if(sizeof($data['countries']) == 1){
                        $data['states'] = $this->users_model->get_locations(2,1);
                        if(sizeof($data['states']) == 1){
                            $data['cities'] = $this->users_model->get_locations(3,1);
                            if(sizeof($data['cities']) == 1){
                                $data['localities'] = $this->users_model->get_locations(4,1);
                                if(sizeof($data['localities']) == 1){
                                    $data['apartments'] = $this->users_model->get_locations(5,1);
                                }
                            }
                            
                        }
                    }
                }
                
                $this->load->view('public/wishlist/wishlist_add_view', $data);
            }
        }
        else{
             
            $data['users_address'] = $this->users_model->users_address($user_id);
            
            $data['countries'] = array();
            $data['states'] = array();
            $data['cities'] = array();
            $data['localities'] = array();
            $data['apartments'] = array();
            
            if(empty($data['users_address'])){ 
             
                $data['countries'] = $this->users_model->get_locations(1,1);
                if(sizeof($data['countries']) == 1){
                    $data['states'] = $this->users_model->get_locations(2,1);
                    if(sizeof($data['states']) == 1){
                        $data['cities'] = $this->users_model->get_locations(3,1);
                        if(sizeof($data['cities']) == 1){
                            $data['localities'] = $this->users_model->get_locations(4,1);
                            if(sizeof($data['localities']) == 1){
                                $data['apartments'] = $this->users_model->get_locations(5,1);
                            }
                        }
                        
                    }
                }
            }
            
             $this->load->view('public/wishlist/wishlist_add_view', $data);
        }

    }
    function view_wishlist_inquiry($product_id){
        if ( ! $this->data['user'])
        {
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_wishlist_model->get_wishlist_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-wishlist');
            exit();
        }
        
        $inquiries = $this->users_wishlist_model->get_wishlist_inquiry($product_id);
        $data['product'] = $this->db->get_where('gre_wishlist_list', array('id' => $product_id))->row()->item_name;
        $data['inquiries'] = $inquiries;
        $data['user']   = $this->users_model->current();
        $this->load->view('public/wishlist/wishlist_inquiry',$data);
    }
    function edit_my_wishlist($product_id){
        if ( ! $this->data['user'])
        {
            redirect('login');
            exit();
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $product = $this->users_wishlist_model->get_wishlist_details($product_id);
        $user_id = $this->data['user']->id;
        if( $user_id != $product->user_id){
            redirect('user_dashboard/my-wishlist');
            exit();
        }
        
        $this->data['categories'] = $this->users_wishlist_model->get_wishlist_categories();
        if($this->input->method() === 'post'){
            
            $this->form_validation->set_rules('name', 'above', 'required|trim');
            $this->form_validation->set_rules('category', 'above', 'required|trim');
            $this->form_validation->set_rules('description', 'above', 'required|trim');
            
            if ($this->form_validation->run() == true)
            {
            $updated_from = 'website';    
                
            $product_id = $this->input->post('id');
            $old_img = $this->input->post('old_img');
            $product_details = array(
                'item_name'     => $this->input->post('name'),
                'slug'          => '',
                'user_id'       => $user_id,
                'description'   => $this->input->post('description'),
                'quantity'      => $this->input->post('quantity'),
                'modified_date' => date('Y-m-d H:i:s'),
                'updated_from'  => $updated_from
            );
            
            $result = $this->users_wishlist_model->edit_user_wishlist($product_id, $product_details);
            
            if($result){
                
                $category_id = $this->input->post('category');
                $this->db->set('category_id', $category_id);
                $this->db->where('product_id', $product_id);
                $this->db->update('gre_wishlist_category');
                
                $this->session->set_flashdata('message', 'Wishlist has been updated successfully!');
                redirect('user_dashboard/my-wishlist');
                exit();
            }else{
                $this->session->set_flashdata('error','Sorry! Something went wrong. Please try again');
                $this->data['product'] = $product;
                $this->data['product_cat'] = $this->users_wishlist_model->get_wishlist_category($product_id);
                $this->load->view('public/wishlist/wishlist_edit_view', $this->data);
            }
            
            }else{
                $this->data['product'] = $product;
                $this->data['product_cat'] = $this->users_wishlist_model->get_wishlist_category($product_id);
                $this->load->view('public/wishlist/wishlist_edit_view', $this->data);
            }
        }else{
            
             $this->data['product'] = $product;
             $this->data['product_cat'] = $this->users_wishlist_model->get_wishlist_category($product_id);
             $this->load->view('public/wishlist/wishlist_edit_view',  $this->data);
        }

    }
    function faqs(){
        $data['user'] = $this->users_model->current();
        $this->load->view('public/faqs_view',$data);
    }
    function app_view(){
        $data['user'] = $this->users_model->current();
        $this->load->view('public/app_view',$data);
    }
    /*
         New code End
    */
        function scrap_request_view(){
            if ( ! $this->data['user'])
            {
                $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
                redirect('login');
            }
            if ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            if($this->input->post()){
                $name         = $this->input->post('name');
                $email        = $this->input->post('email');
                $phone        = $this->input->post('phone');
                $request_type = $this->input->post('request_type');
                $country      = $this->input->post('country');
                $state        = $this->input->post('state');
                $city         = $this->input->post('city');
                $customer_presence = $this->input->post('customer_presence');
                $locality  = $this->input->post('locality');
                $pin       = $this->input->post('pin');
                $apartment = $this->input->post('apartment');
                $flat_no   = $this->input->post('flat_no');
                
                $this->users_model->scrap_request_list($name,$email,$phone,$request_type,$country,$state,$city,$customer_presence,$locality,$pin,$apartment,$flat_no);
                $this->data['message'] = $this->session->flashdata('message');
                                
                $array = array('loc_parent_fk' => 0, 'loc_status' => 1);
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $query = $this->db->get();
                $this->data['countries'] = $query->result_array();
                
                $this->load->view('public/scrap_request/scrap_request',$this->data); 
            }
            else{
                $user = $this->users_model->current();
                if(!empty($user->username)){
                    $info['user'] = $this->users_model->current();
                    $user_id = $user->id;
                    $info['scrap_order_check'] = $this->users_model->scrap_order_check($user_id);
                    $info['periodic']     = $this->users_model->scrap_periodic_id($user_id);
                    $info['non_periodic'] = $this->users_model->scrap_nonperiodic_id($user_id);
                    $info['user_details'] = $this->users_model->user_details($user_id);
                    $info['users_address'] = $this->users_model->users_address($user_id);
                    $user_pickup_frequency = $this->db->select('periodic_interval')->where('id',$info['periodic']['0']->id)->get('gre_scrap_request')->row()->periodic_interval/7;
                    $pickup_frequency = $this->db->select('*')->get('gre_periodic_frequency_config')->result();
                    $info['pickup_frequency'] = '';
                    foreach ($pickup_frequency as $frequency) {
                        if ($frequency->frequency_value == $user_pickup_frequency) {
                            $info['pickup_frequency'] .= '<option value="'.$frequency->frequency_value.'" selected>'.$frequency->frequency_name.'</option>';
                        }
                        else{
                            $info['pickup_frequency'] .= '<option value="'.$frequency->frequency_value.'">'.$frequency->frequency_name.'</option>';
                        }
                    }
                    $info['countries'] = array();
                    $info['states'] = array();
                    $info['cities'] = array();
                    $info['localities'] = array();
                    $info['apartments'] = array();
                
                    if(empty($info['users_address'])){ 
                        $info['countries'] = $this->users_model->get_locations(1,1);
                        if(sizeof($info['countries']) == 1){
                            $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                            $info['states'] = $this->users_model->get_state($country_id);
                            if(sizeof($info['states']) == 1){
                                $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                                $info['cities'] = $this->users_model->get_city($state_id);
                                if(sizeof($info['cities']) == 1){
                                    $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                                    $info['localities'] = $this->users_model->get_locality($city_id);
                                    if(sizeof($info['localities']) == 1){
                                        $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                                        $info['apartments'] = $this->users_model->get_apartment($locality_id);
                                    }
                                }
                                
                            }
                        }
                        //for displaying calender setting
                        $info['pickup_config_date'] = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_date_config')->result();
                        $info['pickup_config_days'] = $this->db->select('*')->where('greenree_id', 1)->get('gre_pickup_config')->result();
                        $this->load->view('public/scrap_request/scrap_request',$info);
                    }
                    else{
                        //for getting user's address ids
                        $country_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->country))->get('locations')->row()->loc_id;
                        $state_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->state,'loc_parent_fk'=>$country_id))->get('locations')->row()->loc_id;
                        $city_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->city,'loc_parent_fk'=>$state_id))->get('locations')->row()->loc_id;
                        $locality_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->locality,'loc_parent_fk'=>$city_id))->get('locations')->row()->loc_id;
                        $apartment_id = $this->db->select('loc_id')->where(array('loc_status' => 1,'loc_name' => $info['users_address'][0]->apartment_name,'loc_parent_fk'=>$locality_id))->get('locations')->row()->loc_id;
                        //serching holidays for apartment
                        $data_general_date = $this->db->select('*')->where(array('greenree_id'=>1,'general_setting_global'=>1))->get('gre_pickup_date_config')->result();
                        $data_general_days = $this->db->select('*')->where(array('greenree_id'=>1,'general_setting_global'=>1))->get('gre_pickup_config')->result();

                        $data_locality_date = $this->db->select('*')->where(array('locality_id'=>$locality_id,'general_setting_global'=>1))->get('gre_pickup_date_config')->result();
                        $data_locality_days = $this->db->select('*')->where(array('locality_id'=>$locality_id,'locality_setting_global'=>1,'apartment_id'=>0))->get('gre_pickup_config')->result();
                        $pickup_config_days = array_merge($data_general_days,$data_locality_days);
                        $info['pickup_config_date'] = array_merge($data_general_date,$data_locality_date);
                        $data_apartment_date = $this->db->select('*')->where('apartment_id', $apartment_id)->get('gre_pickup_date_config')->result();
                        $data_apartment_days = $this->db->select('*')->where('apartment_id', $apartment_id)->get('gre_pickup_config')->result();

                        $info['pickup_config_days'] = array_merge($pickup_config_days,$data_apartment_days);
                        $info['pickup_config_date'] = array_merge($data_general_date,$data_apartment_date);
                        $this->load->view('public/scrap_request/scrap_request',$info);
                    }
                }
                else{
                    $this->session->set_flashdata('message', '<p class="alert alert-danger">Please login to book a scrap request. <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
                    $this->session->set_userdata('sell_now','sell_now');
                    redirect('login');
                }
            }
        }
        
        function otp_generation(){ 
            $contact_number  = $this->input->post('contact_number');
            $var1['results'] = $this->users_model->validate_number($contact_number);
            if ($var1['results']==1) {
                $rand_num = rand(1000,9999);
                $message = 'This is your OTP for Login '.$rand_num.'. Please do not share with anyone.';
                $sms_status = $this->users_model->common_sms_api($contact_number,$message);
                $sms_status = 1;
                if($sms_status == 1){
                    $var1['otp'] = $rand_num;
                    $this->users_model->new_register_otp($rand_num,$contact_number);
                    echo json_encode($var1);
                }
                else{
                    $var1['otp'] = 'Invalid Phone Number';
                    echo json_encode($var1);
                }
            }
            else{
                $var1['otp'] ='Invalid Phone Number';
                echo json_encode($var1);
            }
        }
        
        function mobile_verification(){ 
            $resend_otp     = @$this->input->post('resend_otp');
            $contact_number = $this->input->post('contact_number');
            $result['results']   = $this->users_model->check_mobile_unique($contact_number);
            if ($result['results']==1) {
                echo json_encode($result);
            }
            else{
                $rand_num = rand(1000,9999);
                $message = 'This is your OTP for Mobile Verification '.$rand_num.'. Please do not share with anyone.';
                $sms_status = $this->users_model->common_sms_api($contact_number,$message);
                if($sms_status == 1){
                    $result['otp'] = $rand_num;
                    $this->users_model->new_register_otp($rand_num,$contact_number);
                    echo json_encode($result);
                }
                else{
                    $result['otp'] = 'Error';
                    echo json_encode($result);
                }
            }
        }
        
        function verify_register_otp(){
            $number   = $this->input->post('contact_number');
            $user_otp = $this->input->post('otp');
            $var;// log_message('debug','drama');
            if (!empty($number)) { 
                if (!empty($user_otp)) {
                    $var = $this->users_model->verify_register_otp($number,$user_otp);
                    if($var == 1){
                        $result = 1;
                        echo json_encode($result);
                    }
                    else{
                        $result = $var;
                        echo json_encode($result);
                    }
                }
            }
            else{
                $result = 4;
                echo json_encode($result);
            }
        }
        
        // function update_phone_otp_generation(){
        //     $contact_number  = $this->input->post('contact_number');
        //     $var1['results'] = $this->users_model->validate_number($contact_number);
        //     if ($var1['results']==1) {
        //         $rand_num = rand(1000,9999);
        //         // $message = 'This is your OTP for Login '.$rand_num.'. Please do not share with anyone.';
        //         // $sms_status = $this->users_model->common_sms_api($contact_number,$message);
        //         $sms_status = 1;
        //         if($sms_status == 1){
        //             $var1['otp'] = $rand_num;
        //             $this->users_model->new_otp($rand_num,$contact_number);
        //             echo json_encode($var1);
        //         }
        //         else{
        //             $var1['otp'] = 'Invalid Phone Number';
        //             echo json_encode($var1);
        //         }
        //     }
        //     else{
        //         $var1['otp'] ='Invalid Phone Number';
        //         echo json_encode($var1);
        //     }
        // }
        
        function verify_otp(){
            $time     = date('Y-m-d H:i:s');
            $number   = $this->input->post('contact_number');
            $user_otp = $this->input->post('otp');
    		$this->db->select('otp,otp_expire_time');
    		$this->db->where('phone',$number); 
    		$this->db->where('otp',$user_otp);
    		$query = $this->db->get('gre_mobile_verification');
    		if ($query->num_rows()) {
    			if ($time<$query->row()->otp_expire_time) {
    				$results = 1;
    				echo json_encode($results);
    			}
    		    else{
    		    	$results = 2;
    		    	echo json_encode($results);
    		    }
    		}
    		else{
    			$results = 3;
    		    echo json_encode($results);
    		}
        }
        function forgot_password_send_otp(){
            $email   = $this->input->post('email');
            $this->db->select('id');
    		$this->db->where('email',$email); 
    		$query = $this->db->get('users');
    // 		log_message('debug','asdfghj');
    // 		log_message('debug',$email);
            if($query->num_rows()){
        		$contact_number = $this->db->select('phone')->where('email',$email)->get('users')->row()->phone;
                $rand_num = rand(1000,9999);
                $otp_expire_time = date('Y-m-d H:i:s',(time() + (5 * 60)));
                $data = array(
        		        'otp' => $rand_num,
        		        'otp_expiry_time' => $otp_expire_time
        		);
        		$this->db->where('phone', $contact_number);
                $this->db->update('users',$data); 
                $message = 'This is your OTP for reset password '.$rand_num.'. Please do not share with anyone.';
                $sms_status = $this->users_model->common_sms_api($contact_number,$message);
                if($this->db->affected_rows()){
                    $result =array('otp'     => $rand_num,
                                    'result' => 1
                                   );
        		    echo json_encode($result);//success
                }
                else{
                    $result =array('otp'     => $rand_num,
                                    'result' => 2
                                   );
                    echo json_encode($result);//error got to update otp
                }
            }
            else{
                $result =array('otp'     => $rand_num,
                                    'result' => 3
                               );
                echo json_encode($result);//email does not exist
            }
        }
        function forgot_password_verify_otp(){
            $email   = $this->input->post('email');
            $otp     = $this->input->post('otp');
            $this->db->select('id');
    		$this->db->where('email',$email); 
    		$query = $this->db->get('users');
            if($query->num_rows()){
        		$contact_number = $this->db->select('phone')->where('email',$email)->get('users')->row()->phone;
                $time     = date('Y-m-d H:i:s');
        		$this->db->select('otp,otp_expiry_time');
        		$this->db->where('phone',$contact_number); 
        		$this->db->where('otp',$otp);
        		$query = $this->db->get('users');
        		if ($query->num_rows()) {
        			if ($time<$query->row()->otp_expiry_time) {
        				$results = 1;
        				echo json_encode($results);
        			}
        		    else{
        		    	$results = 2;
        		    	echo json_encode($results);
        		    }
        		}
        		else{
        			$results = 3;
        		    echo json_encode($results);
        		}
            }
            else{
                echo json_encode(4);//email does not exist
            }
        }

        function register_login(){
            if ($this->input->post('create_user'))
            {
                $tables = $this->config->item('tables','ion_auth');
                $identity_column = $this->config->item('identity','ion_auth');
                $this->data['identity_column'] = $identity_column;
                    $identity       = $this->input->post('phone');
                    $password       = rand(1000,9999);
                    $email_verification_code = rand(1000,9999);
                    $email          = $this->input->post('phone');

                    $profile = array(
                        'phone'          => $this->input->post('phone'),
                        'phone_verified' => 1
                    );

                    $group = array('2'); // Sets user to public.

                    if ($_FILES['userfile']['size'] > 0)
                    {
                        $avatar = $this->upload_image();
                        if ( ! $avatar['error'])
                            $profile['avatar'] = $avatar['path'];
                    }
                    $user_id = $this->ion_auth->register($identity, $password, $email, $profile, $group);

                    $this->load->library('flexi_cart_admin');

                    if ($user_id)
                    {   
                        $data_array = array('email' =>''  );
                        $this->db->update('users',$data_array);
                        $this->db->where('id',$user_id);
                        $identity = "";
                        $password = "";
                        $remember = FALSE;
                        $number   = $this->input->post('phone');
                        $new_arr=$this->ion_auth->login($identity, $password, $remember=FALSE,$number);
                        $user = $this->users_model->current();
                        $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));
                        return;
                    }
                    else
                    {
                        $this->flexi_cart_admin->set_error_message(($this->ion_auth->errors() ? $this->ion_auth->errors() : 'Email is Already Exists'), 'public', TRUE);
                        $this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('public'));
                        $register_value = 1;//for again going on registraion part
                        // redirect them back to the login page
                        redirect('login','refresh');
                    }
                //}
            }
           // $this->data['registeration'] = array('register_value' => 1 );
            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');
            //$this->data['registeration'] = array('register_value' => 1 );
            // display the create user form
            $this->load->view('public/auth/login_view', $this->data);
        } 

        function login_otp(){
            $number   = $this->input->post('contact_number');
            $user_otp = $this->input->post('otp');
            $sell_now = @$this->input->post('sell_now');
            $var; 
            if (!empty($number)) {
                if (!empty($user_otp)) {
                    $blocked_customer = $this->db->select('blocked_customer')->where('phone',$number)->get('users')->row()->blocked_customer;
                    $var = 1;//till now setting value 1 default$this->users_model->verify_otp($number,$user_otp);
                    if($blocked_customer==1){
                        $this->flexi_cart->set_error_message('Your Account is Blocked', 'public', TRUE);
                        $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));
                            redirect('login');
                    }
                    elseif($var == 1){
                        $identity="";
                        $password ="";
                        $remember=FALSE;
                        $new_arr=$this->ion_auth->login($identity, $password, $remember=FALSE,$number);
                        $user = $this->users_model->current();
                        $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));
                        $redirect_url = '';
                        if($this->session->userdata('last_page')){
                            $redirect_url = $this->session->userdata('last_page');
                        }
                        if ($sell_now==1) {
                            $var1  =$this->users_model->service_list();
                            $count = 0;
                            $info['country'] = array();
                            foreach ($var1->result() as $row){
                               $info['country'][$count] = array(
                                           'country'=>$row->country
                            ); 
                               $count++;
                            } //print_r($arr);exit();
                            $info['user'] = $this->users_model->current();
                            $this->load->view('public/scrap_request/scrap_request',$info);
                        }else if($redirect_url){
                            redirect($redirect_url);
                        }
                        else{
                            redirect(); 
                        }
                    }
                    elseif ($var == 2) {
                        $msg = "Please check your inbox and verify your email address so you can receive email notifications from GreenREE.";
                        $this->session->set_flashdata('msg',$msg);
                        redirect('login');
                    }
                    else{
                        $msg = "Incorrect OTP.";
                        $this->session->set_flashdata('msg1',$msg);
                        redirect('login');
                    }
                }
                else{
                    $msg = "Incorrect OTP.";
                    $this->session->set_flashdata('msg2',$msg);
                    redirect('login');
                }
            }
            else{
                $msg = "Incorrect Contact Number.";
                $this->session->set_flashdata('msg3',$msg);
                redirect('login');
            }
        }
        
    function check_email(){
        $email = $this->input->post('email');
        $this->db->select('username');
        $this->db->where('email',$email);
        $query  = $this->db->get('users');
        $result = $query->row()->username;
        if (!empty($result)) {
             echo json_encode(1);
        }
        else
        echo json_encode(2); 
    }
    
    function verify_email(){
        if (!$this->data['user']){
            $this->session->set_flashdata('message', '<p class="alert alert-danger">You must login to verify your email.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
        }
        $this->load->helper('url');
        $code = $this->uri->segment('2');
        $email_data = array('email_verification_code' =>'',
                                'email_verified' => 1 );
        $email_verified = $this->db->select('email_verified')->where(array('email_verification_code' => $code, 'id' => $this->data['user']->id))->get('users')->row()->email_verified;
        if ($email_verified == 1) {
            $this->db->where(array('email_verification_code' => $code, 'id' => $this->data['user']->id));
            $this->db->update('users',$email_data);
            $this->session->set_flashdata('message', '<p class="alert alert-success">Your Email is verified successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            redirect('profile');
            exit();
        }
        elseif($email_verified == 0){
            $this->db->where(array('email_verification_code' => $code, 'id' => $this->data['user']->id));
            $this->db->update('users',$email_data);
            $this->session->set_flashdata('message', '<p class="alert alert-success">Your Email is verified successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            redirect('profile');
            exit();
        }
        else{
            $this->session->set_flashdata('message', '<p class="alert alert-danger">Invalid email verified link.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            redirect('logout');
        }
    }
        
    function verify_email_update(){
        if (!$this->data['user']){
            $this->session->set_flashdata('message', '<p class="alert alert-danger">You must login to verify your email.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
        }
        $this->load->helper('url');
        $code = $this->uri->segment('2');
        $email_data = array('email_verification_code' =>'',
                                'email_verified' => 1 );
        $email_verified = $this->db->select('email_verified')->where(array('email_verification_code' => $code, 'id' => $this->data['user']->id))->get('users')->row()->email_verified;
        if ($email_verified == 1) {
            $this->db->where(array('email_verification_code' => $code, 'id' => $this->data['user']->id));
            $this->db->update('users',$email_data);
            $this->session->set_flashdata('message', '<p class="alert alert-success">Your Email is verified successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            redirect('profile');
            exit();
        }
        elseif($email_verified == 0){
            $this->db->where(array('email_verification_code' => $code, 'id' => $this->data['user']->id));
            $this->db->update('users',$email_data);
            $this->session->set_flashdata('message', '<p class="alert alert-success">Your Email is verified successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            redirect('profile');
            exit();
        }
        else{
            $this->session->set_flashdata('message', '<p class="alert alert-danger">Invalid email verified link.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            redirect('logout');
        }
    }
       
        /**
         * login
         * login a user
         */
    function login(){   
        $username = $this->input->post('username');
        $blocked_customer;
        //check customer is blocked or not
        if(!empty($username)){
            $blocked_customer = $this->users_model->check_blocked_customer($username);
        }
        if($blocked_customer==1){
            $this->flexi_cart->set_error_message('Your Account is Blocked', 'public', TRUE);
        $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));
            redirect('login');
        }

        $sell_now = @$this->session->userdata('sell_now');
        // Redirect logged in users and admins to the home page.
        if ($this->ion_auth->logged_in() OR $this->ion_auth->is_admin()) redirect();
        //validate form input
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
                    
        if ($this->form_validation->run() == true)
        {
            // check for "remember me"
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember, 'username'))
            {
                //The login is successful

                // Check if the user is an admin!
                if ($this->ion_auth->is_admin())
                {
                    // Log the admin out
                    $this->ion_auth->logout();

                    // Let admin know what happened
                    $this->flexi_cart->set_error_message('You cannot log in here', 'public', TRUE);
                    $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));

                    // Reload login page.
                    redirect('login');
                }

                $this->load->model('users_model');
                $user = $this->users_model->current();
                $redirect_url = '';
                if($this->session->userdata('last_page')){
                    $redirect_url = $this->session->userdata('last_page');
                }
                // Welcome user by first name.
                // $this->flexi_cart->set_status_message('Hi, '.$user->first_name.'. welcome back!', 'public', TRUE);
                $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));
                $sell_now = @$this->session->userdata('sell_now');
                $checkout = @$this->session->userdata('checkout');
                $points_vouchers = @$this->session->userdata('points_vouchers');
                if ($sell_now=='sell_now') {
                    $var1  =$this->users_model->service_list();
                    $count = 0;
                    $info['country'] = array();
                    foreach ($var1->result() as $row){
                       $info['country'][$count] = array(
                                   'country'=>$row->country
                    ); 
                       $count++;
                    }
                    $user_id = $user->id;
                    $info['scrap_order_check'] = $this->users_model->scrap_order_check($user_id);
                    $info['user'] = $this->users_model->current();
                    $this->load->view('greenree/scrap_request',$info);
                }else if($checkout =='checkout'){
                    redirect('cart');
                }
                else if($points_vouchers){
                    redirect('user_dashboard/my-wallet-cash');
                }
                else if($redirect_url){
                    redirect($redirect_url);
                }
                else{
                    redirect();
                }
            }
            else
            {
                // Login was un-successful, set appropriate message
                $this->flexi_cart->set_error_message($this->ion_auth->errors(), 'public', TRUE);
                $this->session->set_flashdata('message', $this->flexi_cart->get_messages('public'));

                // use redirects instead of loading views for compatibility with MY_Controller libraries
                redirect('login', 'refresh');
            }
        }
        else{
            $this->data['countries'] = array();
            $this->data['states'] = array();
            $this->data['cities'] = array();
            $this->data['localities'] = array();
            $this->data['apartments'] = array();
            $this->data['countries'] = $this->users_model->get_locations(1,1);
            if(sizeof($this->data['countries']) == 1){
                $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                $this->data['states'] = $this->users_model->get_state($country_id);
                if(sizeof($this->data['states']) == 1){
                    $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                    $this->data['cities'] = $this->users_model->get_city($state_id);
                    if(sizeof($this->data['cities']) == 1){
                        $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                        $this->data['localities'] = $this->users_model->get_locality($city_id);
                        if(sizeof($this->data['localities']) == 1){
                            $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                            $this->data['apartments'] = $this->users_model->get_apartment($locality_id);
                        }
                    }
                    
                }
            }
            $this->data['message']  = $this->session->flashdata('message');
            $this->load->view('public/auth/login_view', $this->data);
        }
    }
        
        /**
         * 
         * 
         */
        
        function forgot_pwd_redirect(){
            if($this->input->post('email') != ''){
                $email = $this->input->post('email');
                $this->session->set_userdata('forgot_email',$email);
                echo "success";   
            }
        }
        
        /**
         * logout
         * logout the user
         */
        function logout()
        {
            // log the user out
            $logout = $this->ion_auth->logout();
            $this->load->config('ion_auth', TRUE);
            $identity = $this->config->item('identity', 'ion_auth');
            $this->session->unset_userdata( array($identity, 'id', 'user_id') );
            $this->session->sess_destroy();
            if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
				session_start();
			}
			$this->session->sess_regenerate(TRUE);
            redirect('login');
        }

        /**
         * forgot_password
         * reset forgotten password
         */
        function forgot_password()
        {
            // setting validation rules by checking whether identity is email
           $this->form_validation->set_rules('identity', 'Email', 'required|valid_email');

            if ($this->form_validation->run() == false)
            {
                $this->data['type'] = $this->config->item('identity','ion_auth');
                // Get any status message that may have been set.
                $this->data['message'] = $this->session->flashdata('message');
            }
            else
            {
                $identity = $this->ion_auth->where('email', $this->input->post('identity'))->users()->row();

                if(empty($identity)) {

                    if($this->config->item('identity', 'ion_auth') != 'email')
                    {
                        //$this->data['message'] = 'No record of that username';
                        $this->session->set_flashdata('alert',
                        array('type' => 'danger', 'message' => 'Sorry! No account exist with this email address. Please check and try again.')
                    );
                    }
                    else
                    {
                        $this->data['message'] = 'No record of that email address';
                    }

                }
                else
                {
                    // run the forgotten password method to email an activation code to the user
                    $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

                    if ($forgotten)
                    {   
                //         $contact_number = $this->db->select('phone')->where('email',$this->input->post('identity'))->get('users')->row()->phone;
                //         $rand_num = rand(1000,9999);
                //         $otp_expire_time = date('Y-m-d H:i:s',(time() + (5 * 60)));
                //         $data = array(
                // 		        'otp' => $rand_num,
                // 		        'otp_expiry_time' => $otp_expire_time
                // 		);
                // 		$this->db->where('phone', $contact_number);
		              //  $this->db->update('users',$data); 
                //         // $message = 'This is your OTP for reset password '.$rand_num.'. Please do not share with anyone.';
                //         // $sms_status = $this->users_model->common_sms_api($contact_number,$message);
                        $config = Array(
                                'protocol' => 'smtp',
                                /*'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => $this->data['app']['from_mail_id'],
                                'smtp_pass' => 'My123abc',*/
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                        $this->load->library('email', $config);
                        $this->email->set_newline("\r\n");
                        $data_email = array('email_for' => 'admin');
                        $msg = $this->load->view('public/email/forgot_password_admin', $data_email,  TRUE);
                        $message = $msg;
                         $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                        $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                        $this->email->to($admin_mail_id);
                        $this->email->subject('One user requested for forgot password.');
                        $this->email->message($message);
                        //$this->email->send();
                        // if there were no errors
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        $this->session->set_flashdata('message', '<p class="alert alert-info">Password Reset Email Sent.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
                        redirect("login", 'refresh'); //we should display a confirmation page here instead of the login page
                    }
                    else
                    {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect(current_url(), 'refresh');
                    }
                }
            }
            $this->data['email'] = $this->session->userdata('forgot_email');
            
            $this->load->view('public/auth/forgot_password', $this->data);
        }
        function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }
    function _valid_csrf_nonce()
{log_message('debug',print_r($this->input->post($this->session->flashdata('csrfkey')),TRUE) );
log_message('debug',print_r($this->input->post($this->session->flashdata('csrfkey')),TRUE));
log_message('debug',$this->session->flashdata('csrfvalue'));
log_message('debug','csrfvalue');
    if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
        return TRUE;
    } else {
        return FALSE;
    }
}

        /**
         * reset_password
         * final step for forgotten password
         */
        function reset_password($code = NULL)
        {
            if (!$code)
            {
                show_404();
            }

            $user = $this->ion_auth->forgotten_password_check($code);

            if ($user)
            {
                // if the code is valid then display the password reset form

                $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
                $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

                if ($this->form_validation->run() == false)
                {
                    // display the form
                    // set the flash data error message if there is one
                    $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                    $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                    $this->data['new_password'] = array(
                        'name' => 'new',
                        'id'   => 'new',
                        'type' => 'password',
                        'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
                    );
                    $this->data['new_password_confirm'] = array(
                        'name'    => 'new_confirm',
                        'id'      => 'new_confirm',
                        'type'    => 'password',
                        'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
                    );
                    $this->data['user_id'] = array(
                        'name'  => 'user_id',
                        'id'    => 'user_id',
                        'type'  => 'hidden',
                        'value' => $user->id,
                    );
                    $this->data['phone'] = $user->phone;
                    $this->data['csrf'] = $this->_get_csrf_nonce();
                    $this->data['code'] = $code;
                    // render
                    $this->load->view('public/auth/reset_password', $this->data);
                }
                else
                {
                    // do we have a valid request?
                    if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
                    {

                        // something fishy might be up
                        $this->ion_auth->clear_forgotten_password_code($code);

                        show_error($this->lang->line('error_csrf'));
                    }
                    else
                    {
                        // finally change the password
                        $identity = $user->{$this->config->item('identity', 'ion_auth')};

                        $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                        if ($change)
                        {
                            // if the password was successfully changed
                            $this->session->set_flashdata('message', $this->ion_auth->messages());
                            redirect("login", 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('message', $this->ion_auth->errors());
                            redirect('reset_password/' . $code, 'refresh');
                        }
                    }
                }
            }
            else
            {
                // if the code is invalid then send them back to the forgot password page
                // $this->session->set_flashdata('alert',
                //     array('type' => 'success', 'message' =>  '<p>'.$this->ion_auth->errors().'</p>')
                //     );
                $this->session->set_flashdata('message', $this->ion_auth->errors('public'));
                redirect("forgot_password", 'refresh');
            }
        }

        /**
         * register
         * Register a new user
         */
    function register(){
        if($this->input->post('create_user')){
            if($this->data['user']){
                if($this->ion_auth->in_group('public')){
                    $this->session->set_flashdata('alert', array(
                        'type' => 'danger',
                        'message' => 'You are already registered.'
                    ));
                    // redirect them back to this page.
                    redirect(current_url(), 'refresh');
                }
            }
            $tables = $this->config->item('tables','ion_auth');
            $identity_column = $this->config->item('identity','ion_auth');
            $this->data['identity_column'] = $identity_column;

            // validate form input
            // $this->form_validation->set_rules('phone', 'above', 'required|is_unique['.$tables['users'].'.'.$identity_column.']'); 
            // $this->form_validation->set_rules('email', 'above', 'required|valid_email|is_unique['.$tables['users'].'.'.$identity_column.']');
            // $this->form_validation->set_rules('phone', 'above', 'required|trim');

            // // Add data for different user groups.
            // $this->form_validation->set_rules('first_name', 'above', 'required');
            // // $this->form_validation->set_rules('last_name', 'above', 'required');
//             log_message('debug','hi2');
//             log_message('debug',$this->form_validation->run());
            // if ($this->form_validation->run() == true)
            // {   
            $phone_verified = 1;
            $email          = strtolower($this->input->post('email'));
            $identity       = $this->input->post('email');
            $password       = $this->input->post('password');
            $email_verification_code = md5($email);
            $name = $this->input->post('first_name');
            $name = (explode(" ",$name));
            $last_name = '';
            for ($i=0; $i < sizeof($name) ; $i++) {
                if ($i<1) {
                    $first_name = $name[$i];
                }
                else{
                    $last_name = $last_name.' '.$name[$i];
                }
            }

            $profile = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                // 'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
                'phone_verified' => $phone_verified,
                'email_verification_code' => $email_verification_code
            );

            $group = array('2'); // Sets user to public.

            if($_FILES['userfile']['size'] > 0){
                $avatar = $this->upload_image();
                if( ! $avatar['error'])
                    $profile['avatar'] = $avatar['path'];
            }

            $user_id = $this->ion_auth->register($identity, $password, $email, $profile, $group);

            $this->load->library('flexi_cart_admin');

            if ($user_id){ 
                //Storing user's address
                $user_address = array(
                            'user_id'        => $user_id,
                            'country'        => $this->input->post('country'),
                            'state'          => $this->input->post('state'),
                            'city'           => $this->input->post('city'),
                            'locality'       => $this->input->post('locality'),
                            'apartment_name' => $this->input->post('apartment'),
                            'flat_no'        => $this->input->post('flat_no'),
                            'created_date'   => date('Y-m-d H:i:s')
                                );
                $this->db->insert('gre_address',$user_address);

                $user_details = $this->db->select('*')->where('id',$user_id)->get('users')->row();
                $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'localhost',
                        'smtp_port' => 25,
                        'smtp_auth' => FALSE,
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                //for sending mail
                $code = $email_verification_code;
                $data_email = array(
                    'code'      => $code,
                    'email_for' => 'user',
                    'name'      => $user_details->first_name.' '.$user_details->last_name
                );
                
                $data_email_admin = array(
                    'user_id'   => $user_id,
                    'email_for' => 'admin',
                    'name'      => $user_details->first_name.' '.$user_details->last_name
                );
                $msg_admin = $this->load->view('public/email/mail', $data_email_admin,  TRUE);
                
                $msg = $this->load->view('public/email/mail', $data_email,  TRUE);
                $message = $msg;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($email);
                $this->email->subject('Verify  your email');
                $this->email->message($message);
                $this->email->send();
                 $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($admin_mail_id);
                $this->email->subject('One new user registered');
                $this->email->message($msg_admin);
                $this->email->send();
                //mail part end

                //$msg1 = "Verify Your Email Before Login.";
                $this->flexi_cart_admin->set_status_message("Please check your inbox and verify your email address so you can receive email notifications from GreenREE.", 'public', TRUE);
                $this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('public'));

               $remember = false;
                // redirect them to home page
                $this->ion_auth->login($this->input->post('email'), $password, $remember, 'username');
                if($this->session->userdata('last_page')){
                    $redirect_url = $this->session->userdata('last_page');
                    redirect($redirect_url);
                }
                    else
                        redirect();
            }
            else{
                $this->flexi_cart_admin->set_error_message(($this->ion_auth->errors() ? $this->ion_auth->errors() : 'Email is Already Exists'), 'public', TRUE);
                $this->session->set_flashdata('message', $this->flexi_cart_admin->get_messages('public'));
                $register_value = 1;//for again going on registraion part
                // redirect them back to the login page
                redirect('login','refresh');
            }
            //}
        }
        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');
        //$this->data['registeration'] = array('register_value' => 1 );
        // display the create user form
        $this->load->view('public/auth/login_view', $this->data);
    }
    
    /**
    * 
    * Resend verification email
    */
    function resend_verify_email(){
        if(!$this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $this->data['app']['from_mail_id'],
                    'smtp_pass' => 'My123abc',
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        //for sending mail
        $email = $this->data['user']->email;
        $code = $this->data['user']->email_verification_code;
        if($code == ''){
            $code = md5($email);
            log_message('debug','testing '.$code);
            $data = array('email_verification_code' => $code );
        }
        $data = array(
            'email_verification_code' => $code
        );
        $this->db->update('users', $data, array('id' => $this->data['user']->id));
        log_message('debug','testing '.$this->db->last_query());
        $data_email = array(
            'code' => $code,
            'email_for' => 'verify'
        );
        $msg = $this->load->view('public/email/mail', $data_email,  TRUE);
        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
        $this->email->to($email);
        $this->email->subject('GreenREE email verification');
        $this->email->message($msg);
        $this->email->send();
        $this->session->set_flashdata('message', 'Email verification link has been sent to your inbox.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span></button>');
        // redirect them back to the login page
        redirect("user-dashboard");
    }
    
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // USER DASHBOARD
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
        /**
         * User Dashboard.
         * Overview of user functions
         */ 
    function user_dashboard(){
        if ( ! $this->data['user'])
        {
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $this->load->library('flexi_cart_admin');

        $reward_data = $this->flexi_cart_admin->get_db_reward_point_summary($this->data['user']->id);
        
        $this->data['reward_points'] = $reward_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')];
        $this->data['max_points_value'] = $this->flexi_cart_admin->calculate_conversion_reward_points($this->data['reward_points']);

        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');

        $this->load->view('public/dashboard/dashboard_view', $this->data);
    }

    /**
     * Profile.
     * Manage user's saved carts
     */ 
    function profile(){
        if ( ! $this->data['user'])
        {
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }

        $this->load->model('users_model');
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->data['user'] = $this->users_model->get_details($this->data['user']->id);
       
       
        // validate form input
        // $this->form_validation->set_rules('username', 'Username', 'required');
                    
        // $this->form_validation->set_rules('first_name', 'first name', 'required');
       //$this->form_validation->set_rules('pin', 'Pin', 'required');
       $this->form_validation->set_rules('apartment_name', 'Apartment Name', 'required');
       $this->form_validation->set_rules('locality', 'Locality', 'required');
       $this->form_validation->set_rules('flat_no', 'Flat No', 'required');
        //$this->form_validation->set_rules('last_name', 'last name', 'required');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|callback_user_email_check['.$this->data['user']->id.']');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|callback_user_phone_check['.$this->data['user']->id.']');
        $this->form_validation->set_rules('userfile', 'Profile Photo', 'callback_user_file_check');
        
        //$this->form_validation->set_rules('old_password', 'Old password', 'required|callback_password_check');

        if ($this->input->post('edit_user'))
        { 
            // Additional rules.
            // Rules for the password if it was posted
            if ($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->input->post('flat_no'))
            {   
                $data = array(
                    'country'  => $this->input->post('country'),
                    'state'    =>$this->input->post('state'),
                    'city'     =>$this->input->post('city'),
                    'locality' =>$this->input->post('locality'),
                    'apartment_name'=>$this->input->post('apartment_name'),
                    'flat_no'  =>$this->input->post('flat_no')
                               );
                $this->db->select('user_id');
                $this->db->where('user_id',$this->data['user']->id);
                $query = $this->db->get('gre_address');
                if($query->result()){
                    $this->db->where('user_id',$this->data['user']->id);
                    $this->db->update('gre_address',$data);
                }else{
                    $data1 = array(
                    'country'        => $this->input->post('country'),
                    'state'          => $this->input->post('state'),
                    'city'           => $this->input->post('city'),
                    'locality'       => $this->input->post('locality'),
                    'apartment_name' => $this->input->post('apartment_name'),
                    'flat_no'        => $this->input->post('flat_no'),
                    'user_id'        => $this->data['user']->id,
                    'created_date'   => date('Y-m-d H:i:s')
                               );
                    $this->db->insert('gre_address',$data1);
                }
            }


            if ($this->form_validation->run() === TRUE)
            {   
                $name = $this->input->post('first_name');
                $name = (explode(" ",$name));
                $last_name = '';
                for ($i=0; $i < sizeof($name) ; $i++) {
                    if ($i<1) {
                        $first_name = $name[$i];
                    }
                    else{
                        $last_name = $last_name.' '.$name[$i];
                    }
                }
                // Set the user profile data.
                $user_data = array(
                    'username'   => $this->input->post('email'),
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone')
                );
                
                // Handle uploaded image(avatar).
                if ($_FILES['userfile']['size'] > 0)
                {
                    // Get the image name.
                    $filename = $this->data['user']->avatar;
                    // Path that the avatar image will be uploaded to
                    $uploadPath = $this->data['app']['file_path_profile'];
                    $avatar = $this->upload_image('userfile', $filename, $uploadPath);

                    if ( ! $avatar['error'])
                    {
                        $user_data['avatar'] = $avatar['path'];
                        $image_name = $this->data['user']->id.'-'.$user_data['avatar'];
                        rename($avatar['full_path'],$avatar['file_path'].$image_name);
                        $data_image = array('avatar' => $image_name);
                        $this->db->where('id',$this->data['user']->id);
                        $this->db->update('users',$data_image);
                    }
                }else{
                    if($this->input->post('remove_flag')==1){
                        $user_data1 = array(
                                        'avatar' => 'avtar_image.jpg'
                                        );
                        $this->db->where('id',$this->data['user']->id);
                        $this->db->update('users',$user_data1);
                    }
                }
                // else{
                //     $user_data1 = array(
                //     'avatar' => 'avtar_image.jpg'
                //     );
                //     $this->db->where('id',$this->data['user']->id);
                //     $this->db->update('users',$user_data1);
                // }

                // Set appropriate messages following user update
               if($this->ion_auth->update($this->data['user']->id, $user_data))
                {
                    // $this->flexi_cart->set_status_message($this->ion_auth->messages(), 'public', TRUE);
                    $this->session->set_flashdata('alert',
                        array('type' => 'success', 'message' => 'Your profile has been updated.')
                    );
                    if ($this->input->post('email1') != $this->input->post('email')) {
                        $code =  md5($this->input->post('email'));
                        $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                        $this->load->library('email', $config);
                        $this->email->set_newline("\r\n");
                        $data_email = array(
                                            'code'      => $code,
                                            'email_for' => 'verify'
                                        );
                        $msg = $this->load->view('public/email/mail', $data_email,TRUE);
                        $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                        $this->email->to($this->input->post('email'));
                        $this->email->subject('GreenREE email verification');
                        $this->email->message($msg);
                        $this->email->send();
                        $data_email_verify = array('email_verification_code' =>$code,
                            'email_verified' => 0 );
                        $this->db->where('id',$this->data['user']->id);
                        $this->db->update('users',$data_email_verify);
                        $this->session->set_flashdata('alert',
                        array('type' => 'success', 'message' => 'Your profile has been updated. Please verify your email.')
                        );
                    }
                    $this->db->where('id',$this->data['user']->id);
                    $this->db->update('users',$data_image);
                }
                else
                {
                    // $this->flexi_cart->set_error_message($this->ion_auth->errors(), 'public', TRUE);
                    $this->session->set_flashdata('alert',
                        array('type' => 'danger', 'message' => 'Your Profile is not Updated.')
                    );

                }
                redirect('profile');
            }
        }
         //users address
        $this->data['user_address'] = $this->users_model->users_address($this->data['user']->id);
        if (empty($this->data['user_address'])) {
            $this->data['user_address'] = array('user_id' => $this->data['user']->id);
        }
        
        
        
        $this->data['countries'] = array();
        $this->data['states'] = array();
        $this->data['cities'] = array();
        $this->data['localities'] = array();
        $this->data['apartments'] = array();
        
        if(empty($this->data['user_address'][0]->country)){ 
            
            //log_message('debug','country empty');
            
            $this->data['countries'] = $this->users_model->get_locations(1,1);
            //new code for address part start
            if(sizeof($this->data['countries']) == 1){
                $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                $this->data['states'] = $this->users_model->get_state($country_id);
                if(sizeof($this->data['states']) == 1){
                    $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                    $this->data['cities'] = $this->users_model->get_city($state_id);
                    if(sizeof($this->data['cities']) == 1){
                        $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                        $this->data['localities'] = $this->users_model->get_locality($city_id);
                        if(sizeof($this->data['localities']) == 1){
                            $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                            $this->data['apartments'] = $this->users_model->get_apartment($locality_id);
                        }
                    }
                    
                }
            }
            //new code for address part end


            // if(sizeof($this->data['countries']) == 1){
            //     $this->data['states'] = $this->users_model->get_locations(2,1);
            //     if(sizeof($this->data['states']) == 1){
            //         $this->data['cities'] = $this->users_model->get_locations(3,1);
            //         if(sizeof($this->data['cities']) == 1){
            //             $this->data['localities'] = $this->users_model->get_locations(4,1);
            //             if(sizeof($this->data['localities']) == 1){
            //                 $this->data['apartments'] = $this->users_model->get_locations(5,1);
            //             }
            //         }
                    
            //     }
            // }
            
        }else {
        
            $array = array('loc_parent_fk' => 0, 'loc_status' => 1,'loc_type_fk' =>1);
            $this->db->select('loc_id,loc_name');
            $this->db->from('locations');
            $this->db->where($array);
            $this->db->order_by('loc_name','asc');
            $query = $this->db->get();
            $this->data['countries'] = $query->result_array();
            
            if($this->data['user_address'][0]->state != '' && $this->data['user_address'][0]->country != ''){
                
                $country_id = $this->users_model->get_location_id($this->data['user_address'][0]->country);
                    
                $array = array('loc_parent_fk' => $country_id, 'loc_status' => 1);
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $this->data['states'] = $query->result_array();   
            }
            
            if($this->data['user_address'][0]->city != ''){
                
                $state_id = $this->users_model->get_location_id($this->data['user_address'][0]->state);
                
                $array = array('loc_parent_fk' => $state_id, 'loc_status' => 1);
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $this->data['cities'] = $query->result_array();
                
            }
            
            if($this->data['user_address'][0]->locality != ''){
                
                $city_id = $this->users_model->get_location_id($this->data['user_address'][0]->city);
                
                $array = array('loc_parent_fk' => $city_id, 'loc_status' => 1);
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $this->data['localities'] = $query->result_array();
                
            }
            
            if($this->data['user_address'][0]->apartment_name != ''){
                
                $locality_id = $this->users_model->get_location_id($this->data['user_address'][0]->locality);
                
                $array = array('loc_parent_fk' => $locality_id, 'loc_status' => 1);
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $this->data['apartments'] = $query->result_array();
                
            }
        
        }
        
        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');
        // log_message('debug',$this->session->flashdata('message'));
        // log_message('debug','my_message');
        // log_message('debug',print_r($this->data,TRUE));
        $this->load->view('public/dashboard/user_profile_view', $this->data);
    }

    /**
     * change_password
     * Change a user's password
     */
    function change_password(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login first.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        if ($this->input->post('change_password')){
            // $this->form_validation->set_rules('old_password', 'above', 'required');
            $this->form_validation->set_rules('password', 'above', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'above', 'required');
            $user_details = $this->users_model->current();

            if ($this->form_validation->run())
            {   if($user_details->email){
                    $identity = $user_details->email;
                }
                else{
                    $identity = $this->session->userdata('identity');
                }
                $old_pass = $this->input->post('old_password');
                $new_pass = $this->input->post('password');

                if ($this->ion_auth->change_password($identity, $old_pass, $new_pass))
                {
                    $this->flexi_cart->set_status_message($this->ion_auth->messages(), 'public', TRUE);
                    // Logout so the user can re-login.
                    $this->logout();
                }
                else
                {
                    $this->flexi_cart->set_error_message($this->ion_auth->errors(), 'public', TRUE);
                    redirect(current_url());
                }
            }
        }

        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
        $this->load->view('public/dashboard/change_password', $this->data);
    }

        /**
         * User Orders.
         * View user's orders
         */ 
    function orders(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        // The load/save/delete cart data functions require the flexi cart ADMIN library.
        $this->load->library('flexi_cart_admin');

        // Get an array of all saved orders. 
        // Using a flexi cart SQL function, set the order the order data so that dates are listed newest to oldest.
        $this->flexi_cart_admin->sql_where($this->flexi_cart_admin->db_column('order_summary', 'user'), $this->data['user']->id);
        $this->flexi_cart_admin->sql_order_by($this->flexi_cart_admin->db_column('order_summary', 'date'), 'desc');
        $this->data['order_data'] = $this->flexi_cart_admin->get_db_order_query()->result_array();

        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');

        $this->load->view('public/dashboard/orders_view', $this->data);
    }

        /**
         * User Order.
         * View user's order details
         */ 
    function order($order_number){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login to view orders.', 'public', TRUE);
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        // The load/save/delete cart data functions require the flexi cart ADMIN library.
        $this->load->library('flexi_cart_admin');

        // Get the row array of the order filtered by the order number in the url.
        $sql_where = array($this->flexi_cart_admin->db_column('order_summary', 'order_number') => $order_number);
        $this->data['summary_data'] = $this->flexi_cart_admin->get_db_order_summary_query(FALSE, $sql_where)->result_array();
        $this->data['summary_data'] = $this->data['summary_data'][0];

        // Get an array of all order details related to the above order, filtered by the order number in the url.
        $sql_where = array($this->flexi_cart_admin->db_column('order_details', 'order_number') => $order_number);
        $this->data['item_data'] = $this->flexi_cart_admin->get_db_order_detail_query(FALSE, $sql_where)->result_array();
        //getting debited reward point
        $this->db->select('debit_reward_point');
        $this->db->where('order_id',$order_number);
        $query = $this->db->get('gre_reward_point');
        $this->data['debit_reward_point'] = $query->row()->debit_reward_point;
        log_message('debug',$this->data['debit_reward_point']);
        // Get an array of all order statuses that can be set for an order.
        // The data is then to be displayed via a html select input to allow the user to update the orders status.
        $this->data['status_data'] = $this->flexi_cart_admin->get_db_order_status_query()->result_array();

        // Get the row array of any refund data that may be available for the order, filtered by the order number in the url.
        $this->data['refund_data'] = $this->flexi_cart_admin->get_refund_summary_query($order_number)->result_array();
        $this->data['refund_data'] = $this->data['refund_data'][0];

        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');

        $this->load->view('public/dashboard/order_details_view', $this->data);
    }

        /**
         * User Carts.
         * Manage user's saved carts
         */ 
    function carts(){
        if ( ! $this->data['user']){
            $this->flexi_cart->set_error_message('You must login to view saved carts.', 'public', TRUE);
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        // The load/save/delete cart data functions require the flexi cart ADMIN library.
        $this->load->library('flexi_cart_admin');

        // Create an SQL WHERE clause to list all previously saved cart data for a specific user.
        // This examples also prevents cart session data from confirmed orders being loaded, by checking the readonly status is set at '0'.
        $sql_where = array(
            $this->flexi_cart->db_column('db_cart_data', 'user') => $this->data['user']->id,
            $this->flexi_cart->db_column('db_cart_data', 'readonly_status') => 0
        );
        // Get a list of all saved carts that match the SQL WHERE statement.
        $this->data['saved_cart_data'] = $this->flexi_cart_admin->get_db_cart_data_query(FALSE, $sql_where)->result_array();

        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');

        $this->load->view('public/dashboard/carts_view', $this->data);
    }

        /**
         * Points and Vouchers.
         * Manage user's points and vouchers
         */ 
    function points_vouchers(){
        $data = $this->users_model->current();
        if (  ! $this->data['user'])
        {
            $this->session->set_flashdata('message', '<p class="alert alert-danger">You must login to view wallet cash.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">× </span><span class="sr-only">Close</span> </button></p>');
            $this->session->set_userdata('points_vouchers','points_vouchers');
            $this->session->set_userdata('last_page', current_url());
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        $user_id = $this->data['user']->id;

        $this->load->library('flexi_cart_admin');

        $this->load->model('demo_cart_admin_model');

        // Check if POST data has been submitted, if so, call the custom demo model function to handle the submitted data.
        if ($this->input->post('convert_reward_points'))
        {
            $this->demo_cart_admin_model->demo_convert_reward_points($user_id);
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());
            redirect(current_url());
        }

        $this->load->model('users_model');

        // Variable "person" is used because "user" is already being used for currently signed in user.
        $this->data['person'] = $this->users_model->get_details($user_id);

        // Get an array of all the reward vouchers filtered by the id in the url.
        // Using flexi cart SQL functions, join the demo user table with the discount table.
        $sql_where = array($this->flexi_cart_admin->db_column('discounts', 'user') => $user_id);
        // $this->flexi_cart_admin->sql_join('demo_users', 'user_id = '.$this->flexi_cart_admin->db_column('discounts', 'user'));
        $this->data['voucher_data_array'] = $this->flexi_cart_admin->get_db_voucher_query(FALSE, $sql_where)->result_array();

        // Get user remaining reward points.
        $summary_data = $this->flexi_cart_admin->get_db_reward_point_summary($user_id);
        $this->data['points_data'] = array(
            'total_points' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points')],
            'total_points_pending' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_pending')],
            'total_points_active' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')],
            'total_points_active' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')],
            'total_points_expired' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_expired')],
            'total_points_converted' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_converted')],
            'total_points_cancelled' => $summary_data[$this->flexi_cart_admin->db_column('reward_points', 'total_points_cancelled')]
        );

        // Get an array of a demo user and their related reward points from a custom demo model function, filtered by the id in the url.
        $reward_data = $this->demo_cart_admin_model->demo_reward_point_summary($user_id);
        
        // Note: The custom function returns a multi-dimensional array, of which we only need the first array, so get the first row '$user_data[0]'.
        $this->data['reward_data'] = $reward_data[0];

        // Get the conversion tier values for converting reward points to vouchers.
        $conversion_tiers = $this->data['reward_data'][$this->flexi_cart_admin->db_column('reward_points', 'total_points_active')];
        $this->data['conversion_tiers'] = $this->flexi_cart_admin->get_reward_point_conversion_tiers($conversion_tiers);

        // Get an array of all reward points for a user.
        $sql_select = array(
            $this->flexi_cart_admin->db_column('reward_points', 'order_number'),
            $this->flexi_cart_admin->db_column('reward_points', 'description'),
            $this->flexi_cart_admin->db_column('reward_points', 'order_date')
        );  
        $sql_where = array($this->flexi_cart_admin->db_column('reward_points', 'user') => $user_id);
        $this->data['points_awarded_data'] = $this->flexi_cart_admin->get_db_reward_points_query($sql_select, $sql_where)->result_array();
        
        // Call a custom function that returns a nested array of reward voucher codes and the reward point data used to create the voucher.
        $this->data['points_converted_data'] = $this->demo_cart_admin_model->demo_converted_reward_point_history($user_id);
        
        // Reward points History from Reward Point Table
        $this->db->select('order_id, scrap_request_id , redeem_request_id, credit_reward_point,debit_reward_point,date');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('date','desc');
        $query = $this->db->get('gre_reward_point');    
        $result = $query->result();
        $this->data['reward_points'] = $query->result();
        $this->data['user_details'] = $this->users_model->user_details($user_id);
        $user            = $this->users_model->current();
        $user_id         = $user->id;
        $this->data['redeem']  = $this->users_model->redeem_request_details($user_id);
        //log_message('debug',print_r($result,true));
        // Get any status message that may have been set.
        $this->data['message'] = $this->session->flashdata('message');

        $this->load->view('public/dashboard/points_and_vouchers_view', $this->data);
    }


    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
    // PRODUCTS
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

        /**
         * Index Page
         * Home or Landing page
         */
        function index($slug = FALSE){   
            
            $categories = $this->store->menu();
            
            foreach ($categories as $item)
            {
                $products = $this->store->products(array(
                    'limit' => 10,
                    'category_id' => $item->id,
                ));
                $item->products = $products['rows'];    
            }
            $this->data['categories'] = $categories;
            $this->data['latest'] = $this->product_model->get_latest_in_category(4);
            
            $date = date_create(date("Y-m-d h:i:s"));
            date_sub($date,date_interval_create_from_date_string("10 days"));
            $date = date_format($date,"Y-m-d");
            $this->data['new_wishlist'] = $new_pre_owned_items = $this->db->where('created_date >=', $date)->from("gre_wishlist_list")->count_all_results();
            $this->data['new_pre_owned_items'] = $this->db->where('created_date >=', $date)->where('status >=', 1)->from("gre_my_product_list")->count_all_results();
            
            $this->load->model('banners_model');
            $this->data['banners'] = $this->banners_model->get_banners(array(
                'limit' => 10,
                'running' => TRUE
            ));

            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');

            $this->load->view('public/home_view', $this->data);
        }

        /**
         * Search Page.
         */ 
        function search()
        {
            $product_options = array(
                'search' => $this->input->get('q')
            );
            
            // Get products.
            $products = $this->store->products($product_options);
            
            $this->data['products'] = $products['rows'];    
            $this->data['total'] = $products['total'];  
            $this->data['pagination'] = $products['pagination'];

            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');

            $this->load->view('public/products/search_view', $this->data);
        }

        /**
         * Category Page.
         * Browse and filter items by category and/or attributes
         */ 
        function category($slug = FALSE)
        {
            if ($this->input->post('price_range'))
            {
                $url = current_url().'?'.($this->input->get('price') ? preg_replace('/(^|&)price=[^&]*/', '&price='.$this->input->post('price'), $_SERVER['QUERY_STRING']) : $_SERVER['QUERY_STRING'].'&price='.$this->input->post('price'));
                // redirect to back here.
                redirect($url, 'refresh');
            }
            
            $categories = $this->store->menu();

            foreach ($categories as $item)
            {
                $products = $this->store->products(array(
                    'limit' => 1,
                    'order' => 'latest',
                    'category_id' => $item->id,
                ));
                $item->products = $products['rows'];    
            }
            $this->data['categories'] = $categories;
            
            $this->data['category'] = $this->category_model->get_categories($slug);

            if (! $this->data['category'])
            {

                // Get any status message that may have been set.
                $this->data['message'] = $this->session->flashdata('message');
                $this->load->view('public/errors_view', $this->data);
            }
            else
            {
                $product_options = array(
                    'attribute' => $this->input->get('ATB'),
                    'order' => 'latest'
                );
                if ($slug) $product_options['category_id'] = $this->data['category']->id;
                
                // Get products.
                $products = $this->store->products($product_options);
                
                $this->data['products'] = $products['rows'];    
                $this->data['total'] = $products['total'];  
                $this->data['pagination'] = $products['pagination'];

                $this->data['min_price'] = $this->product_model->min_price($this->data['category']->id);
                $this->data['max_price'] = $this->product_model->max_price($this->data['category']->id);
                $this->data['price_range'] = ($this->input->get('price')) ? $this->input->get('price') : $this->data['max_price'];

                // Get any status message that may have been set.
                $this->data['message'] = $this->session->flashdata('message');

                $this->load->view('public/products/category_view', $this->data);
            }
        }
            
        /**
         * Product Page.
         * View a single product.
         */
        function product($slug)
        {
            $product = $this->product_model->get_details($slug);

            $this->data['category'] = $this->category_model->get_categories($product->category->id);

            /**
            *
            * Handle submitted data for Cart insert.
            *
             */
            if ($this->input->post('add_to_cart'))
            {
                $this->flexi_cart->insert_items(array(
                    'id'      => $this->input->post('id'),
                    'name'    => $this->input->post('name'),
                    'thumb'   => $this->input->post('thumb'),
                    'quantity'=> $this->input->post('quantity'),
                    'price'   => $this->input->post('price'),
                    'weight'  => $this->input->post('weight'),
                    'options' => $this->input->post('options')
                ));

                // Set a message to the CI flashdata so that it is available after the page redirect.
                $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

                redirect(current_url());
            }

            /**
            *
            * Handle submitted data for Product inquirues.
            *
             */
            if ($this->input->post('inquiry_product'))
            {
                // validate form input
                $this->form_validation->set_rules('target_price', 'above', 'required');
                $this->form_validation->set_rules('order_quantity', 'above', 'required');
                $this->form_validation->set_rules('inquiry', 'above', 'required');
                
                if ( ! $this->data['user'])
                {
                    // For users who are no logged in, require more form details.
                    $this->form_validation->set_rules('name', 'above', 'required');
                    $this->form_validation->set_rules('email', 'above', 'required|valid_email');
                    $this->form_validation->set_rules('phone', 'above', 'required|trim');
                }
                else
                {
                    $user = $this->users_model->get_details($this->data['user']->id);
                }


                if ($this->form_validation->run() == true)
                {
                    if ($this->data['user'])
                    {
                        // Get users details from the database.
                        $this->data['buyer_name']       = (isset($user->first_name)) ? ($user->first_name.' '.$user->first_name) : $user->company_name;
                        $this->data['buyer_email']      = (isset($user->email)) ? $user->email : $user->company_email;
                        $this->data['buyer_phone']      = (isset($user->phone)) ? $user->phone : $user->company_phone;
                        $this->data['buyer_location']   = (isset($user->address)) ? $user->address : $user->company_address;
                    }
                    else
                    {
                        // Get users details from the form.
                        $this->data['buyer_name']  = $this->input->post('name');
                        $this->data['buyer_email'] = $this->input->post('email');
                        $this->data['buyer_phone'] = $this->input->post('phone');
                        $this->data['buyer_location'] = $this->input->post('location');
                    }
                    
                    $this->data['buyer_price'] = $this->input->post('target_price');
                    $this->data['buyer_order'] = $this->input->post('order_quantity');
                    $this->data['buyer_inquiry'] = $this->input->post('inquiry');


                    $this->load->helper('email');

                    $this->email->clear();
                    $this->email->from($this->data['app']['from_mail_id'],'GreenREE');
                    $this->email->to($product->seller->company_email);
                    $this->email->subject('['.$this->data['app']['name'].'] Your item received an inquiry');
                    $this->email->message($this->load->view('public/email/product_inquiry', $this->data, TRUE));

                    // Send email to the vendor.
                    if ($this->email->send() )
                    {
                        $this->session->set_flashdata('alert',
                            array('type' => 'success', 'message' => 'Your inquiry has been sent to the seller')
                        );
                        redirect(current_url(), 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('alert',
                            array('type' => 'danger', 'message' => 'Your inquiry could not been sent.')
                        );
                        redirect(current_url(), 'refresh');
                    }
                }
            }

            /**
            *
            * Formatting Product Variable to create thumbnails with links
            * which can then be used to point to the product variants.
            *
             */
            // Get related items.
            $related_meta = $this->product_model->get_products(array(
                'exclude' => $product->id,
                'limit' => 5,
                'start' => 0,
                'category_id' => $product->category->id,
            ));
            $related_products = $related_meta->products;

            /**
            *
            * Get some random items for the user to view
            *
             */
            $user_items_meta = $this->product_model->get_products(array(
                'limit' => 5,
                'random' => TRUE,
                'start' => 0,
            ));
            $random_products = $user_items_meta->products;

            

            /**
            *
            * Formatting Product Variable to create thumbnails with links
            * which can then be used to point to the product variants.
            *
             */
            // Thumbnails linking to a product variant
            $variant_thumbs = array();

            $product_variants = $this->product_model->get_product_variants($product->id);
            foreach ($product_variants as $variant)
            {
                // Generate the URI string to show product variants
                $variant_slug = implode("-", $variant->options_set);
                $variant_link = current_url().'?&OPT='.$variant_slug;

                $thumb_img = (!empty($variant->images)) ? $variant->images[0]->url : NULL ;
                // Add Formatted link and image to thumbnail variation array
                array_push($variant_thumbs, array(
                    'link'  => $variant_link,
                    'image' => $thumb_img
                ));
            }

            /**
            *
            * Formatting Product Options to add a link which can then be used to point
            * to the product variant.
            *
             */
            // Get Product Options
            $product_options  = $this->product_model->get_product_options($product->id);
            // Get Product Defaults
            $product_defaults = $this->product_model->get_product_defaults($product->id);

            // Get selected variant. Typical selected by the user, or if set, use admin selected default.
            if ($this->input->get('OPT'))
            {
                $variant_selected = explode('-', $this->input->get('OPT'));
            }
            else
            {
                $variant_selected = $product_defaults;
            }

            foreach ($product_options as $key => $option)
            {
                $curr_ids  = array();
                foreach ($option['values'] as $index => $value)
                {
                    array_push($curr_ids, $value['id']);
                }

                // Define current option attributes.
                $curr_attr = $variant_selected;

                // Add a link to each option attribute value that the user will in
                // Selecting the product variation.
                foreach ($option['values'] as $index => $value)
                {
                    // Define option attributes IDs in the URI string that are different from
                    // those in the current option attributes ID. (There should only be one ID per option):
                    $curr_attr = array_diff($curr_attr, $curr_ids);
                    // Add current option attribute id.
                    array_push($curr_attr, $value['id']);
                    // Add the link to the option attribute values.
                    $product_options[$key]['values'][$index]['link'] = current_url().'?&OPT='.implode('-', $curr_attr);
                }
            }

            /**
            *
            * Get the product variant - this is just the variated image, price and weight
            *
             */
            if ($this->input->get('OPT'))
            {
                // For user defined options
                $product_variant = $this->product_model->get_product_variant_by_slug($product->id, $this->input->get('OPT'));
            }
            else
            {
                // For default options
                $product_variant = $this->product_model->get_product_variant_by_slug($product->id, implode('-', $product_defaults));
            }


            /**
            *
            * Collect all data for the page view file.
            *
             */
            $this->data['variant_selected'] = $variant_selected;
            $this->data['product_options']  = $product_options;
            $this->data['product_variants'] = $product_variants;
            $this->data['$product_defaults'] = $product_defaults;
            $this->data['variant_thumbs']   = $variant_thumbs;
            $this->data['product']          = $product;
            $this->data['random_products']  = $random_products;
            $this->data['related_products'] = $related_products;
            $this->data['variant'] = $product_variant;
            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');
            
            $this->load->view('public/products/product_view', $this->data);
        }
        
        /**
         * View Cart and Cart actions.
         */
        function cart($slug = NULL){    
            $user       = $this->users_model->current();
            $user_id    = $user->id;
            if (empty($user_id)) {
                $this->session->set_userdata('checkout','checkout');
                $this->session->set_userdata('last_page', current_url());
                redirect('login');
            }
            if ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            // Remove PayPal result data.
            $this->session->unset_userdata('PayPalResult');
            
            if ($slug == 'set_currency')
            {
                $this->flexi_cart->update_currency($this->input->get('currency'));
                redirect($this->input->get('redirect'));
            }
            elseif ($slug == 'add_to_cart')
            {
                $this->flexi_cart->insert_items(array(
                    'id'      => $this->input->post('id'),
                    'name'    => $this->input->post('name'),
                    'thumb'   => $this->input->post('thumb'),
                    'quantity'=> $this->input->post('quantity'),
                    'price'   => $this->input->post('price'),
                    'options' => array()
                ));
                
                if ($this->input->is_ajax_request())
                {

                    // Get any status message that may have been set.
                    $this->data['message'] = $this->session->flashdata('message');

                    $this->load->view('public/cart/cart_data', array(
                        'cart_data' => $this->flexi_cart->cart_contents()
                    ));
                }
                else
                {
                    redirect($this->input->get('redirect'));
                }
            }
            else
            {
                // Update cart contents and settings.
                if ($this->input->post('update'))
                {
                    $this->update_cart();
                }
                // Update discount codes.
                else if ($this->input->post('update_discount'))
                {
                    $this->update_discount_codes();
                }
                // Remove discount code.
                else if ($this->input->post('remove_discount_code'))
                {
                    $this->remove_discount_code();
                }
                // Remove all discount codes.
                else if ($this->input->post('remove_all_discounts'))
                {
                    $this->remove_all_discounts();
                }
                // Clear / Empty cart contents.
                else if ($this->input->post('clear'))
                {
                    $this->clear_cart();
                }
                // Destroy all cart items and settings and reset to default settings.
                else if ($this->input->post('destroy'))
                {
                    $this->destroy_cart();
                }
                //else if ($this->input->post('checkout'))
                else if ($this->input->post('is_checkout')=="true")
                {
                    $this->checkout();
                }
                ###+++++++++++++++++++++++++++++++++###
                
                // Get required data on cart items, discounts and surcharges to display in the cart.
                $this->data['cart_items'] = $this->flexi_cart->cart_items(); 
                $this->data['reward_vouchers'] = $this->flexi_cart->reward_voucher_data();
                $this->data['discounts'] = $this->flexi_cart->summary_discount_data();
                $this->data['surcharges'] = $this->flexi_cart->surcharge_data();

                ###+++++++++++++++++++++++++++++++++###

                // This example shows how to lookup countries, states and post codes that can be used to calculate shipping rates.
                $sql_select = array($this->flexi_cart->db_column('locations', 'id'), $this->flexi_cart->db_column('locations', 'name'));    
                $this->data['countries'] = $this->flexi_cart->get_shipping_location_query($sql_select, 0)->result_array();
                $this->data['states'] = $this->flexi_cart->get_shipping_location_query($sql_select, 1)->result_array();
                $this->data['postal_codes'] = $this->flexi_cart->get_shipping_location_query($sql_select, 2)->result_array();
                $this->data['shipping_options'] = $this->flexi_cart->get_shipping_options(); 
                
                // Uncomment the lines below to use the manual shipping example. Read more below.
                # $this->load->model('demo_cart_model');
                # $this->data['shipping_options'] = $this->demo_cart_model->demo_manual_shipping_options(); 
                
                /**
                 * By default, this demo is setup to show how to implement shipping rates with a database.
                 * In the 2 steps below is an example showing how to manually set and define shipping options and rates.
                 *
                 * To use this example follow these steps:
                 * #1: Replace the four "$this->data" arrays set above with "$this->data['shipping_options'] = $this->demo_cart_model->demo_manual_shipping_options();".
                 * #2: Set "$config['database']['shipping_options']['table']" and "$config['database']['shipping_rates']['table']" to FALSE via the config file.
                */
                
                ###+++++++++++++++++++++++++++++++++###

                // The load/save/delete cart data functions require the flexi cart ADMIN library.
                $this->load->library('flexi_cart_admin');
                

                if ($this->data['user'])
                {
                    $total_reward_point = $this->users_model->total_reward_point($this->data['user']->id);
                    $this->data['total_reward_point'] = $total_reward_point;
                    // Create an SQL WHERE clause to list all previously saved cart data for a specific user.
                    // This examples also prevents cart session data from confirmed orders being loaded, by checking the readonly status is set at '0'.
                    $sql_where = array(
                        $this->flexi_cart->db_column('db_cart_data', 'user') => $this->data['user']->id,
                        $this->flexi_cart->db_column('db_cart_data', 'readonly_status') => 0
                    );

                    // Get a list of all saved carts that match the SQL WHERE statement.
                    $this->data['saved_cart_data'] = $this->flexi_cart_admin->get_db_cart_data_query(FALSE, $sql_where)->result_array();
                }
                else
                {
                    $this->data['saved_cart_data'] = array();   
                }

                ###+++++++++++++++++++++++++++++++++###
                
                 $this->data['user_details'] = $this->users_model->user_details($user_id);
                 $this->data['users_address'] = $this->users_model->users_address($user_id);
                
                $this->data['countries'] = array();
                $this->data['states'] = array();
                $this->data['cities'] = array();
                $this->data['localities'] = array();
                $this->data['apartments'] = array();
                if(empty($this->data['users_address'])){ 
                    $this->data['countries'] = $this->users_model->get_locations(1,1);
                    if(sizeof($this->data['countries']) == 1){
                        $country_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 1, 'loc_status' => 1))->get('locations')->row()->loc_id;
                        $this->data['states'] = $this->users_model->get_state($country_id);
                        if(sizeof($this->data['states']) == 1){
                            $state_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 2, 'loc_status' => 1,'loc_parent_fk' => $country_id))->get('locations')->row()->loc_id;
                            $this->data['cities'] = $this->users_model->get_city($state_id);
                            if(sizeof($this->data['cities']) == 1){
                                $city_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 3, 'loc_status' => 1,'loc_parent_fk' => $state_id))->get('locations')->row()->loc_id;
                                $this->data['localities'] = $this->users_model->get_locality($city_id);
                                if(sizeof($this->data['localities']) == 1){
                                    $locality_id = $this->db->select('loc_id')->where(array('loc_type_fk' => 4, 'loc_status' => 1,'loc_parent_fk' => $city_id))->get('locations')->row()->loc_id;
                                    $this->data['apartments'] = $this->users_model->get_apartment($locality_id);
                                }
                            }
                            
                        }
                    }
                }
                // Get any status message that may have been set.
                $this->data['message'] = $this->session->flashdata('message');
                
                $this->load->view('public/cart/cart_view', $this->data);
            }
        }

        /**
         * Load Store Pages.
         */
        function page($slug = NULL)
        {
            $this->load->model('pages_model');

            $this->data['page_data'] = $this->pages_model->get_pages($slug);
            // Get any status message that may have been set.
            $this->data['message'] = $this->session->flashdata('message');

            $this->load->view('public/page_view', $this->data);
        }

        
        function get_state()
        {
            if ($this->input->is_ajax_request())
            {
                $country_id = $this->input->post('country');       
                $array = array('loc_type_fk' => 2, 'loc_parent_fk' => $country_id, 'loc_status' => 1);
                
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $state_list = $query->result_array();
                echo json_encode($state_list);
            }
        }
        
        
        function get_city()
        {
            if ($this->input->is_ajax_request())
            {
                
                $state_id = $this->input->post('state');
                $array = array('loc_type_fk' => 3, 'loc_parent_fk' => $state_id, 'loc_status' => 1);
                
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $city_list = $query->result();
                
                echo json_encode($city_list);
            }
        }
        
        
        function get_locality()
        {
            if ($this->input->is_ajax_request())
            {
                $city_id = $this->input->post('city');
                $array = array('loc_type_fk' => 4, 'loc_parent_fk' => $city_id, 'loc_status' => 1);
                
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $locality_list = $query->result_array();
                echo json_encode($locality_list);
            }
        }
        
        function get_apartment()
        {
            if ($this->input->is_ajax_request())
            {
                $locality_id = $this->input->post('locality');
                $array = array('loc_type_fk' => 5, 'loc_parent_fk' => $locality_id, 'loc_status' => 1);
                
                $this->db->select('loc_id,loc_name');
                $this->db->from('locations');
                $this->db->where($array);
                $this->db->order_by('loc_name','asc');
                $query = $this->db->get();
                $apartment_list = $query->result_array();
                echo json_encode($apartment_list);
            }
        }
        
        
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // CART CONTROLS
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
        
        /**
         * update_cart
         * Gets the carts item and shipping data from form inputs, and updates the cart.
         */ 
        function update_cart()
        {
            // Load custom demo function to retrieve data from the submitted POST data and update the cart.
            $this->load->model('demo_cart_model');
            $this->demo_cart_model->demo_update_cart();
            
            // If the cart update was posted by an ajax request, do not perform a redirect.
            if (! $this->input->is_ajax_request())
            {
                // Set a message to the CI flashdata so that it is available after the page redirect.
                $this->session->set_flashdata('message', $this->flexi_cart->get_messages());
                        
                redirect('cart');
            }
        }
        
        /**
         * delete_item
         * Deletes and item from the cart using the '$row_id' supplied via the url link.
         */ 
        function delete_cart_item($row_id = FALSE) 
        {
            $this->flexi_cart->delete_items($row_id);
            
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            redirect('cart');       
        }
        
        /**
         * clear_cart
         * Clears (Empties) all item, discount and surcharge data from the cart.
         */ 
        function clear_cart()
        {
            // The 'empty_cart()' function allows an argument to be submitted that will also reset all shipping data if 'TRUE'.
            $this->flexi_cart->empty_cart(TRUE);

            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            redirect('cart');
        }
        
        /**
         * destroy_cart
         * Destroys all cart items and settings and resets cart to its default settings.
         */ 
        function destroy_cart()
        {
            $this->flexi_cart->destroy_cart();

            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());
            
            redirect('cart');
        }


    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
    // SAVE / LOAD CART DATA
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

        /**
         * save_cart_data
         * Saves the users current cart to the database so that it can be reloaded at a later date.
         */ 
        function save_cart_data() 
        {
            if (!$this->data['user'])
            {
                // Users that are not logged in cannot save cart data.
                $this->flexi_cart->set_error_message('You must be logged in to save your cart.', 'public');
            }
            elseif ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            else
            {
                // The load/save/delete cart data functions require the flexi cart ADMIN library.
                $this->load->library('flexi_cart_admin');
            
                // Save the cart data to the database.
                $this->flexi_cart_admin->save_cart_data($this->data['user']->id);
            }

            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());
            
            redirect('cart');
        }

        /**
         * load_cart_data
         * Loads saved cart data into the users current cart, overwriting any existing cart data in their current session.
         * A custom function 'demo_update_loaded_cart_data()' has been included to ensure that all loaded item data is up-to-date with the current item database table. 
         */ 
        function load_cart_data($cart_data_id = 0, $dashboard_redirect = FALSE) 
        {
            if (!$this->data['user'])
            {
                // Users that are not logged in cannot save cart data.
                $this->flexi_cart->set_error_message('You must be logged in to load saved carts.', 'public');
            }
            elseif ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            else
            {
                // The load/save/delete cart data functions require the flexi cart ADMIN library.
                $this->load->library('flexi_cart_admin');
                $this->load->model('demo_cart_model');

                // Load saved cart data array.
                // This data is loaded into the browser session as if you were shopping with the cart as normal.
                $this->flexi_cart_admin->load_cart_data($cart_data_id);
                
                // To ensure that the prices and other data of all loaded items are still correct, a custom demo function has been made to loop through each item in the cart, 
                // query the demo item database table and retrieve the current item data.
                // As flexi cart does not manage item tables, this function has to be custom made to suit each sites requirements, this is an example of how it can be achieved.
                // Note that cart items including selectable options would potentially require a more complex query.    
                $this->demo_cart_model->demo_update_loaded_cart_data();
            }
            
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            ($dashboard_redirect) ? redirect('user_dashboard/carts') : redirect('cart');
        }

        /**
         * delete_cart_data
         * Deletes specific saved cart data from the database.
         * This function is accessed from the 'Save / Load Cart Data' page.
         */ 
        function delete_cart_data($cart_data_id = 0, $dashboard_redirect = FALSE) 
        {
            // The load/save/delete cart data functions require the flexi cart ADMIN library.
            $this->load->library('flexi_cart_admin');

            // Delete the saved cart data from the database.
            $this->flexi_cart_admin->delete_db_cart_data($cart_data_id);
            
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            ($dashboard_redirect) ? redirect('user_dashboard/carts') : redirect('cart');
        }
 

    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // DISCOUNTS
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

        /**
         * update_discount_codes
         * Updates all discount codes that have been submitted to the cart.
         * This function is accessed from the 'View Cart' page via a form input field named 'update_discount'.
         */ 
        function update_discount_codes()
        {
            // Get the discount codes from the submitted POST data.
            $discount_data = $this->input->post('discount');
            
            // The 'update_discount_codes()' function will validate each submitted code and apply the discounts that have activated their quantity and value requirements.
            // Any previously set codes that have now been set as blank (i.e. no longer present) will be removed.
            // Note: Only 1 discount can be applied per item and per summary column. 
            // For example, 2 discounts cannot be applied to the summary total, but 1 discount could be applied to the shipping total, and another to the summary total.
            $this->flexi_cart->update_discount_codes($discount_data);
            
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            redirect('cart');
        }

        /**
         * set_discount
         * Set a manually defined discount to the cart, rather than using the discount database table.
         * This function is accessed from the 'Discounts / Surcharges' page.
         * The settings for each discount are defined via the custom demo function 'demo_set_discount()'.
         */ 
        function set_discount($discount_id = FALSE)
        {
            $this->load->model('demo_cart_model');
            
            $this->demo_cart_model->demo_set_discount($discount_id);
            
            redirect('cart');
        }
        
        /**
         * remove_discount_code
         * Removes a specific discount code from the cart.
         * This function is accessed from the 'View Cart' page via a form input field named 'remove_discount_code'.
         */ 
        function remove_discount_code()
        {
            // This examples gets the discount code from the array key of the submitted POST data.
            $discount_code = key($this->input->post('remove_discount_code'));

            // The 'unset_discount()' function can accept an array of either discount ids or codes to delete more than one discount at a time.
            // Alternatively, if no data is submitted, the function will delete all discounts that are applied to the cart.
            // This example uses the 1 discount code that was supplied via the POST data.
            // $this->flexi_cart->unset_userdata_discount($discount_code); // Can't remeber why this 'stock code' didn't work - said 'undefined method' or something        
            $this->flexi_cart->unset_discount($discount_code);
            
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            redirect('cart');
        }
        
        /**
         * remove_all_discounts
         * Removes all discounts from the cart, including discount codes, manually applied discounts and reward vouchers.
         * This function is accessed from the 'View Cart' page via a form input field named 'remove_all_discounts'.
         */ 
        function remove_all_discounts()
        {
            // The 'unset_discount()' function can accept an array of either discount ids or codes to delete more than one discount at a time.
            // Alternatively, if no data is submitted, the function will delete all discounts that are applied to the cart.
            // This example removes all discount data.
            // $this->flexi_cart->unset_userdata_discount(); // Can't remeber why this 'stock code' didn't work - said 'undefined method' or something      
            $this->flexi_cart->unset_discount();
            
            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());

            redirect('cart');
        }

        /**
         * unset_discount
         * Removes a specific active item or summary discount from the cart.
         * This function is accessed from the 'View Cart' page via a 'Remove' link located in the description of an active discount.
         */ 
        function unset_discount($discount_id = FALSE)
        {
            // The 'unset_discount()' function can accept an array of either discount ids or codes to delete more than one discount at a time.
            // Alternatively, if no data is submitted, the function will delete all discounts that are applied to the cart.
            // This example uses the 1 discount id that was supplied via the url link.
            // $this->flexi_cart->unset_userdata_discount($discount_id); // Can't remeber why this 'stock code' didn't work - said 'undefined method' or something      
            $this->flexi_cart->unset_discount($discount_code);

            // Set a message to the CI flashdata so that it is available after the page redirect.
            $this->session->set_flashdata('message', $this->flexi_cart->get_messages());
            
            redirect('cart');
        }
    
    
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // SURCHARGES
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
    
        /**
         * set_surcharge
         * Set a manually defined surcharge to the cart.
         * This function is accessed from the 'Discounts / Surcharges' page.
         * The settings for each surcharge are defined via the custom demo function 'demo_set_surcharge()'.
         */ 
        function set_surcharge($surcharge_id = FALSE)
        {
            $this->load->model('demo_cart_model');
            
            $this->demo_cart_model->demo_set_surcharge($surcharge_id);
            
            redirect('cart');
        }

        /**
         * unset_surcharge
         * Removes a specific surcharge from the cart.
         * This function is accessed from the 'View Cart' page via a 'Remove' link located in the description of a surcharge.
         */ 
        function unset_surcharge($surcharge_id = FALSE)
        {
            // The 'unset_surcharge()' function can accept an array of surcharge ids to delete more than one surcharge at a time.
            // Alternatively, if no data is submitted, the function will delete all surcharges that are applied to the cart.
            // This example uses the 1 surcharge id that was supplied via the url link.
            $this->flexi_cart->unset_userdata_surcharge($surcharge_id);
            
            redirect('cart');
        }



    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // PAYMENT PROCESSING
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

        /**
         * checkout
         * Review final cart and add shipping, handling and tax amounts.
         */ 
    function check_email_exist(){
        $user       = $this->users_model->current();
        $email = $this->input->post('email');
        $query  = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows()){
            $email_check_status =1;
            $query1 = $this->db->get_where('users', array('email' => $email,'id' => $user->id));
            if($query1->num_rows()){
                $email_check_status =2;
            }
        }
        else
            $email_check_status =2;
        if($email_check_status != 1){
           $result = 1;
           echo json_encode($result);
        }
        else{
            $result = 2;
            echo json_encode($result); 
        }
    } 
        function checkout() 
        {
            $user       = $this->users_model->current();
            $user_id    = $user->id;
            if (empty($user_id)) {
                redirect('login');
            }
            if ($this->data['user']->blocked_customer == 1){   
                $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
                redirect('logout');
            }
            $email_check_status = 0;
            $query  = $this->db->get_where('users', array('email' => $this->input->post('email')));
            if(!empty($this->input->post('email'))){
                if ($query->num_rows()){
                    $email_check_status =1;
                    $query1 = $this->db->get_where('users', array('email' => $email,'id' => $user->id));
                    if($query1->num_rows()){
                        $email_check_status =2;
                    }
                }
                if($email_check_status == 1){
                    $this->session->set_flashdata( 'message', 'Email is Already Exist.');
                    redirect('cart');
                    exit();
                }
                else{
                    $email = $this->input->post('email');
                    $name  = $this->input->post('name');
                    $name  = (explode(" ",$name));
                    $last_name = '';
                    for ($i=0; $i < sizeof($name) ; $i++) {
                        if ($i<1) {
                            $first_name = $name[$i];
                        }
                        else{
                            $last_name = $last_name.' '.$name[$i];
                        }
                    }
                    $data_user = array('last_name'  => $last_name,
                                       'first_name' => $first_name,
                                        'email'     => $email,
                                        'username'  => $email);
                    $this->db->where('id',$this->data['user']->id);
                    $this->db->update('users',$data_user);  
                    $code = md5($this->input->post('email'));
                    $data_email = array('email_verification_code' => $code );
                    $this->db->where('id',$user->id);
                    $this->db->update('users',$data_email);
                    
                    $config = Array(
                                'protocol' => 'smtp',
                                /*'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => $this->data['app']['from_mail_id'],
                                'smtp_pass' => 'My123abc',*/
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $data_email = array(
                                            'code'      => $code,
                                            'email_for' => 'verify'
                                        );
                    $msg = $this->load->view('public/email/mail', $data_email,TRUE);
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('GreenREE email verification');
                    $this->email->message($msg);
                    $this->email->send();
                }
            }
            $total_reward_point = $this->users_model->total_reward_point($user_id);
            
            
            if(@$this->input->post('email')){
                
            $email             = $this->input->post('email');
            $name              = $this->input->post('name');
            $country           = $this->input->post('country');
            $state             = $this->input->post('state');
            $city              = $this->input->post('city');
            $locality          = $this->input->post('locality');
            $pin               = @$this->input->post('pin');
            $apartment         = $this->input->post('apartment');
            $flat_no           = $this->input->post('flat_no');
            //log_message('debug','hisingh1');//exit();
            $this->users_model->update_address($user_id,$country,$state,$city,$locality,$pin,$apartment,$flat_no,$email,$name);
            
            }
            
            $total_price = $this->flexi_cart->total();
            $total_price = str_replace("&#x20B9;", " ", $total_price);
            // $total_price = preg_replace('/[^0-9.]/','',$total_price);log_message('debug',$total_price);
            $total_price = substr($total_price, 0, -1) . '';
            $total_price = str_replace("&#x20B9;", "", $total_price);
            $total_price = str_replace(",", "", $total_price);
            if ($total_reward_point<$total_price){
       //if we want to show error and don't use cash provision for transaction use this code         
       //       $this->data['errors'] = array('Errors'=>"Your Total Reward Point is less than Total items price");
       //       $this->load->library('store');
                
                // $products = $this->store->products(array(
                //  'random' => TRUE,
                //  'limit' => 12
                // ));
                
                // $this->data['products'] = $products['rows']; 
                // $this->data['total'] = $products['total'];   
                // $this->data['pagination'] = $products['pagination'];
       //       $this->load->view('public/cart/checkout_error', $this->data);
                $this->session->set_flashdata('total_price',$total_price);
                redirect('checkout_complete');
            }
            else{
                $this->session->set_flashdata('total_price',$total_price);
                redirect('checkout_complete');
            }
        }

        /**
         * checkout_review
         */
        function checkout_review()
        {
            // Load flexi admin to save the order to the database.
            $this->load->library('flexi_cart_admin');

            // Load PayPal library config file.
            $this->config->load('paypal');

            $paypal_config = array(
                // Sandbox / testing mode option.
                'Sandbox'       => $this->config->item('Sandbox'),
                // PayPal API username of the API caller
                'APIUsername'   => $this->config->item('APIUsername'),
                // PayPal API password of the API caller
                'APIPassword'   => $this->config->item('APIPassword'),
                // PayPal API signature of the API caller
                'APISignature'  => $this->config->item('APISignature'),
                // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                'APISubject'    => '',
                // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                'APIVersion'    => $this->config->item('APIVersion'),
                'DeviceID'      => $this->config->item('DeviceID'),
                'ApplicationID' => $this->config->item('ApplicationID'),
                'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
            );

            // Load PayPal library
            $this->load->library('paypal/paypal_pro', $paypal_config);

            // Get PayPal data from session userdata
            $SetExpressCheckoutPayPalResult = $this->session->userdata('PayPalResult');
            $PayPal_Token = $SetExpressCheckoutPayPalResult['TOKEN'];

            /**
             * Now we pass the PayPal token that we saved to a session variable
             * in the SetExpressCheckout.php file into the GetExpressCheckoutDetails
             * request.
             */
            $PayPalResult = $this->paypal_pro->GetExpressCheckoutDetails($PayPal_Token);

            /**
             * Now we'll check for any errors returned by PayPal, and if we get an error,
             * we'll save the error details to a session and redirect the user to an
             * error page to display it accordingly.
             *
             * If the call is successful, we'll save some data we might want to use
             * later into session variables.
             */
            if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
            {
                $this->data['errors'] = array('Errors'=>$PayPalResult['ERRORS']);

                // Get products.
                $this->load->library('store');
                
                $products = $this->store->products(array(
                    'random' => TRUE,
                    'limit' => 12
                ));
                
                $this->data['products'] = $products['rows'];    
                $this->data['total'] = $products['total'];  
                $this->data['pagination'] = $products['pagination'];

                $this->load->view('public/cart/checkout_error', $this->data);
            }
            else
            {
                // Successful call.

                /**
                 * Here we'll pull out data from the PayPal response.
                 * Refer to the PayPal API Reference for all of the variables available
                 * in $PayPalResult['variablename']
                 *
                 * https://developer.paypal.com/docs/classic/api/merchant/GetExpressCheckoutDetails_API_Operation_NVP/
                 *
                 * Again, Express Checkout allows for parallel payments, so what we're doing here
                 * is usually the library to parse out the individual payments using the GetPayments()
                 * method so that we can easily access the data.
                 *
                 * We only have a single payment here, which will be the case with most checkouts,
                 * but we will still loop through the $Payments array returned by the library
                 * to grab our data accordingly.
                 */
                $cart['paypal_payer_id'] = isset($PayPalResult['PAYERID']) ? $PayPalResult['PAYERID'] : '';
                $cart['phone_number'] = isset($PayPalResult['PHONENUM']) ? $PayPalResult['PHONENUM'] : '';
                $cart['email'] = isset($PayPalResult['EMAIL']) ? $PayPalResult['EMAIL'] : '';
                $cart['first_name'] = isset($PayPalResult['FIRSTNAME']) ? $PayPalResult['FIRSTNAME'] : '';
                $cart['last_name'] = isset($PayPalResult['LASTNAME']) ? $PayPalResult['LASTNAME'] : '';
                $cart['status'] = isset($PayPalResult['CHECKOUTSTATUS']) ? $PayPalResult['CHECKOUTSTATUS'] : '';
                $cart['date'] = isset($PayPalResult['TIMESTAMP']) ? $PayPalResult['TIMESTAMP'] : '';

                foreach($PayPalResult['PAYMENTS'] as $payment)
                {
                    $cart['currency'] = isset($PayPalResult['CURRENCYCODE']) ? $PayPalResult['CURRENCYCODE'] : '';
                    $cart['shipping_name'] = isset($payment['SHIPTONAME']) ? $payment['SHIPTONAME'] : '';
                    $cart['shipping_street'] = isset($payment['SHIPTOSTREET']) ? $payment['SHIPTOSTREET'] : '';
                    $cart['shipping_city'] = isset($payment['SHIPTOCITY']) ? $payment['SHIPTOCITY'] : '';
                    $cart['shipping_state'] = isset($payment['SHIPTOSTATE']) ? $payment['SHIPTOSTATE'] : '';
                    $cart['shipping_zip'] = isset($payment['SHIPTOZIP']) ? $payment['SHIPTOZIP'] : '';
                    $cart['shipping_country_code'] = isset($payment['SHIPTOCOUNTRYCODE']) ? $payment['SHIPTOCOUNTRYCODE'] : '';
                    $cart['shipping_country_name'] = isset($payment['SHIPTOCOUNTRYNAME']) ? $payment['SHIPTOCOUNTRYNAME'] : '';
                }

                /**
                 * At this point, we now have the buyer's shipping address available in our app.
                 * We could now run the data through a shipping calculator to retrieve rate
                 * information for this particular order.
                 *
                 * This would also be the time to calculate any sales tax you may need to
                 * add to the order, as well as handling fees.
                 *
                 * We're going to set static values for these things in our static
                 * shopping cart, and then re-calculate our grand total.
                 */
                // $cart['shopping_cart']['shipping'] = 0;
                // $cart['shopping_cart']['handling'] =0;
                // $cart['shopping_cart']['tax'] =0;

                // $cart['shopping_cart']['grand_total'] = 0;

                /**
                 * Now we will redirect the user to a final review
                 * page so they can see the shipping/handling/tax
                 * that has been added to the order.
                 */
                // Set example cart data into session
                $this->session->set_userdata('shopping_cart', $cart);

                $this->data['cart'] = $cart;

                // Example - Load Review Page
                $this->do_checkout_payment();
            }
        }

        /**
         * DoExpressCheckoutPayment
         */
        function do_checkout_payment()
        {
            $cart = $this->session->userdata('shopping_cart');
            /**
             * Now we'll setup the request params for the final call in the Express Checkout flow.
             * This is very similar to SetExpressCheckout except that now we can include values
             * for the shipping, handling, and tax amounts, as well as the buyer's name and
             * shipping address that we obtained in the GetExpressCheckoutDetails step.
             *
             * If this information is not included in this final call, it will not be
             * available in PayPal's transaction details data.
             *
             * Once again, the template for DoExpressCheckoutPayment provides
             * many more params that are available
             */

            // Get cart data from session userdata
            $SetExpressCheckoutPayPalResult = $this->session->userdata('PayPalResult');
            $PayPal_Token = $SetExpressCheckoutPayPalResult['TOKEN'];

            $DECPFields = array(
                // Required.  A timestamped token, the value of which was returned by a previous SetExpressCheckout call.
                'token' => $PayPal_Token,
                // Required.  Unique PayPal customer id of the payer.  Returned by GetExpressCheckoutDetails, or if you used SKIPDETAILS it's returned in the URL back to your RETURNURL.
                'payerid' => $cart['paypal_payer_id'],
            );

            /**
             * Just like with SetExpressCheckout, we need to gather our $Payment
             * data to pass into our $Payments array.  This time we can include
             * the shipping, handling, tax, and shipping address details that we
             * now have.
             */
            $Payments = array();
            $Payment = array(
                // Required.  The total cost of the transaction to the customer.  If shipping cost and tax charges are known, include them in this value.  If not, this value should be the current sub-total of the order.
                'amt' => $this->flexi_cart->total($discounts = TRUE, $format_value = FALSE),
                // Subtotal of items only.
                'itemamt' => $this->flexi_cart->item_summary_total($discounts = TRUE, $format_value = FALSE),
                // A three-character currency code.  Default is USD.
                'currencycode' => $this->flexi_cart->currency_name(),
                // Total shipping costs for this order.  If you specify SHIPPINGAMT you mut also specify a value for ITEMAMT.
                'shippingamt' => $this->flexi_cart->shipping_total($discounts = TRUE, $format_value = FALSE),
                // Total handling costs for this order.  If you specify HANDLINGAMT you mut also specify a value for ITEMAMT.
                // 'handlingamt' => 0,
                // Required if you specify itemized L_TAXAMT fields.  Sum of all tax items in this order.
                // 'taxamt' => $this->flexi_cart->tax_total($discounts = TRUE, $format_value = FALSE),
                // Required if shipping is included.  Person's name associated with this address.  32 char max.
                'shiptoname' => $cart['shipping_name'],
                // Required if shipping is included.  First street address.  100 char max.
                'shiptostreet' => $cart['shipping_street'],
                // Required if shipping is included.  Name of city.  40 char max.
                'shiptocity' => $cart['shipping_city'],
                // Required if shipping is included.  Name of state or province.  40 char max.
                'shiptostate' => $cart['shipping_state'],
                // Required if shipping is included.  Postal code of shipping address.  20 char max.
                'shiptozip' => $cart['shipping_zip'],
                // Required if shipping is included.  Country code of shipping address.  2 char max.
                'shiptocountrycode' => $cart['shipping_country_code'],
                // Phone number for shipping address.  20 char max.
                'shiptophonenum' => $cart['phone_number'],
                // How you want to obtain the payment.  When implementing parallel payments, this field is required and must be set to Order.
                'paymentaction' => 'Sale',
            );

            /**
             * Here we push our single $Payment into our $Payments array.
             */
            array_push($Payments, $Payment);

            $OrderItems = array();
            foreach ($this->flexi_cart->cart_items() as $key => $item)
            {
                array_push($OrderItems, array(
                    'l_name' => $item['name'], // Item Name. 127 char max.
                    // 'l_desc' => '', // Item description. 127 char max.
                    'l_amt' => ($item['discount_quantity'] > 0) ? $item['discount_price_total'] : $item['price_total'], // Cost of individual item.
                    'l_number' => $item['id'], // Item Number. 127 char max.
                    'l_qty' => $item['quantity'], // Item quantity. Must be any positive integer.
                    // 'l_taxamt' => '', // Item's sales tax amount.
                ));
            }

            /**
             * Now we gather all of the arrays above into a single array.
             */
            $PayPalRequestData = array(
                'DECPFields' => $DECPFields,
                'Payments' => $Payments,
                'OrderItems' => $OrderItems,
            );

            /**
             * Here we are making the call to the DoExpressCheckoutPayment function in the library,
             * and we're passing in our $PayPalRequestData that we just set above.
             */
            $PayPalResult = $this->paypal_pro->DoExpressCheckoutPayment($PayPalRequestData);

            /**
             * Now we'll check for any errors returned by PayPal, and if we get an error,
             * we'll save the error details to a session and redirect the user to an
             * error page to display it accordingly.
             *
             * If the call is successful, we'll save some data we might want to use
             * later into session variables, and then redirect to our final
             * thank you / receipt page.
             */
            if( ! $this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
            {
                $this->data['errors'] = array('Errors'=>$PayPalResult['ERRORS']);

                // Get products.
                $this->load->library('store');
                
                $products = $this->store->products(array(
                    'random' => TRUE,
                    'limit' => 12
                ));
                
                $this->data['products'] = $products['rows'];    
                $this->data['total'] = $products['total'];  
                $this->data['pagination'] = $products['pagination'];

                $this->load->view('public/cart/checkout_error', $this->data);
            }
            else
            {
                // Remember what PayPal returned.
                $this->session->set_userdata('PayPalResult', $PayPalResult);

                // Successful Order
                redirect('checkout_complete');
            }
        }

        /**
         * Order Complete - Pay Return Url
         */
    function checkout_complete(){
        if ( ! $this->data['user']){
            redirect('login');
        }
        if ($this->data['user']->blocked_customer == 1){   
            $this->flexi_cart->set_error_message('Your Account is blocked.', 'public', TRUE);
            redirect('logout');
        }
        //$PayPalResult = $this->session->userdata('PayPalResult');
        $cash_collect = 0;
        $debit_point  = 0;
        $total_price  = @$this->session->flashdata('total_price');
        $total_price  = ceil($total_price);
        $user         = $this->users_model->current();
        $user_id      = $user->id;
        $email        = $user->email;
        if($total_price>0){
            $this->data['total_price'] = $total_price;
            $this->db->select('locality,city,flat_no,apartment_name,state,pin,country');
            $this->db->where('user_id',$user_id);
            $query = $this->db->get('gre_address');
            if ($user->total_reward_point < $total_price) {
                $cash_collect = $total_price-$user->total_reward_point;
                $debit_point  = $total_price-$cash_collect;
            }else{
                $cash_collect = 0;
                $debit_point  = $total_price;
            }
            
            $order_from = 'website';
                
            // Assign all billing, shipping and contact details to their corresponding database columns.
            // All summary data within the cart array will automatically be saved when using the 'save_order()' function, 
            // provided the corresponding $config['database']['order_summary']['columns']['xxx'] is set via the config file.
            $custom_summary_data = array(
                'ord_user_fk'              => $user->id,
                'ord_demo_ship_name'       => $user->first_name.' '.$user->last_name,
                'flat_no'                  => $query->row()->flat_no,
                'ord_demo_ship_address_01' => $query->row()->apartment_name,
                'ord_demo_ship_address_02' => $query->row()->locality,
                'ord_demo_ship_city'       => $query->row()->city,
                'ord_demo_ship_state'      => $query->row()->state,
                'ord_demo_ship_post_code'  => $query->row()->pin,
                'ord_demo_ship_country'    => $query->row()->country,
                'ord_demo_phone'           => $user->phone,
                'ord_demo_email'           => $user->email,
                'order_from'               => $order_from
            );

            // Create an array of any user defined columns that were added to cart items.
            // This example checks to see if any items have a custom column called 'sku' (Example item #116 has this column).
            // Note: This hand coded method of saving custom item data is only required if the custom column has not been defined in the config file
            // via the '$config['cart']['items']['custom_columns']' and '$config['database']['order_details']['custom_columns']' arrays.
            $custom_item_data = array();
            
            // Save cart and customer details.
            $this->load->library('flexi_cart_admin');
            //$this->flexi_cart_admin->save_order($custom_summary_data, $custom_item_data, $PayPalResult['PAYMENTINFO_0_TRANSACTIONID']);
            $this->flexi_cart_admin->save_order($custom_summary_data, $custom_item_data);

            // Remove the shopping cart.
            $this->flexi_cart->destroy_cart();

            $this->data['cash_collect']        = $cash_collect;
            $this->data['debit_reward_point']  = $debit_point;
            
            
            //$this->data['order_number'] = $PayPalResult['PAYMENTINFO_0_TRANSACTIONID'];
            $this->db->select('ord_order_number');
            $this->db->where('ord_user_fk',$user_id);
            $this->db->order_by('ord_order_number',"desc");
            $this->db->limit(1);
            $query = $this->db->get('order_summary');
            $this->data['order_number'] = $query->row()->ord_order_number;
            $order_number = $query->row()->ord_order_number;
            //update user table reward point
            $total_reward_point = $user->total_reward_point-$total_price;
            if ($total_reward_point<1) {
                $total_reward_point = 0;
            }
            $data1 = array('total_reward_point' => $total_reward_point );
            $this->db->where('id',$user_id);
            $this->db->update('users',$data1);
            //update remaining balance as cash if user reward point was not sufficient
            $data2 = array('ord_det_cash_collect' => $cash_collect,
                           'ord_det_wallet_cash_debited' => $debit_point );
            $this->db->where('ord_order_number',$order_number);
            $this->db->update('order_summary',$data2);
            //reward point table for debit history
            $data2 = array('order_id'           =>$query->row()->ord_order_number,
                           'debit_reward_point' => $debit_point,
                           'date'               => date('Y-m-d H:i:s'),
                           'user_id'            => $user_id
                                                  );
            $this->db->insert('gre_reward_point',$data2);
             //for sending mail
            if($order_number){
                $this->db->select('*');
                $this->db->where('ord_det_order_number_fk',$order_number);
                $order_new_details = $this->db->get('order_details');
                $order_details = $order_new_details->result();
                $user_address  = $this->users_model->users_address($user_id);
                $this->db->select('*');
                $this->db->where('ord_order_number',$order_number);
                $order_summary_mail = $this->db->get('order_summary');
                $this->data['ord_total'] = $order_summary_mail->row()->ord_total;
                $this->data['ord_total_items'] = $order_summary_mail->row()->ord_total_items;
                $data_email = array(
                                'order_id'     => $order_number,
                                'username'     => $user->first_name,
                                'email'        => $user->email,
                                'phone'        => $user->phone,
                                'cash_collect' => $cash_collect,
                                'order_details' => $order_details,
                                'order_summary_mail' => $order_summary_mail,
                                'debit_reward_point'   => $debit_point,
                                'country'   => $user_address[0]->country,
                                'city'      => $user_address[0]->city,
                                'state'     => $user_address[0]->state,
                                'locality'  => $user_address[0]->locality,
                                'apartment_name' => $user_address[0]->apartment_name,
                                'flat_no'  => $user_address[0]->flat_no,
                                'email_for' => 'user',
                                'ord_total' => $order_summary_mail->row()->ord_total,
                                'cash_collect' => $cash_collect,
                                'savings_total'=> $order_summary_mail->row()->ord_savings_total
                                );
                $data_email1 = array(
                                'order_id'     => $order_number,
                                'username'     => $user->first_name,
                                'email'        => $user->email,
                                'phone'        => $user->phone,
                                'cash_collect' => $cash_collect,
                                'order_details' => $order_details,
                                'order_summary_mail' => $order_summary_mail,
                                'debit_reward_point'   => $debit_point,
                                'country'   => $user_address[0]->country,
                                'city'      => $user_address[0]->city,
                                'state'     => $user_address[0]->state,
                                'locality'  => $user_address[0]->locality,
                                'apartment_name' => $user_address[0]->apartment_name,
                                'flat_no'  => $user_address[0]->flat_no,
                                'email_for' => 'admin',
                                'ord_total' => $order_summary_mail->row()->ord_total,
                                'cash_collect' => $cash_collect,
                                'savings_total'=> $order_summary_mail->row()->ord_savings_total
                              ); 
                if($user->email_verified){
                    $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'localhost',
                                'smtp_port' => 25,
                                'smtp_auth' => FALSE,
                                'mailtype'  => 'html', 
                                'charset'   => 'iso-8859-1'
                            );
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $msg = $this->load->view('public/email/checkout_mail', $data_email,  TRUE);
                    $message = $msg;
                    $subject = 'GreenREE Order Details ('.$order_number.')';
                    $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                    $this->email->to($email);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    $this->email->send();
                }
                //send mail to admin
                $msg1 = $this->load->view('public/email/checkout_mail', $data_email1,  TRUE);
                 $admin_id = $this->db->select('user_id')->where('group_id',1)->get('users_groups')->row()->user_id;
                $admin_mail_id =  $this->db->select('email')->where('id',$admin_id)->get('users')->row()->email;
                $this->email->from($this->data['app']['from_mail_id'], 'GreenREE');
                $this->email->to($admin_mail_id);
                $this->email->subject('GreenREE new order details ('.$order_number.')');
                $this->email->message($msg1);
                $this->email->send();
                $message = 'New order is placed successfully. Order id '.$order_number.' User name '.$user->first_name.' '.$user->last_name.' Phone '.$user->phone.' Email '.$user->email;
                $admin_phone = $this->db->select('phone')->where('id',$admin_id)->get('users')->row()->phone;
                $sms_status = $this->users_model->common_sms_api($admin_phone, $message);
                //send sms to user
                $message = 'Your Order is Placed Successfully. Your Order Id is '.$order_number;
                $sms_status = $this->users_model->common_sms_api($user->phone, $message);
            }
        }    
        else{
            $this->db->select('*');
            $this->db->where('ord_user_fk',$this->data['user']->id);
            $this->db->order_by('ord_order_number','desc');
            $this->db->limit('1');
            $query = $this->db->get('order_summary');
            $this->db->select('*');
            $this->db->where('order_id',$query->row()->ord_order_number);
            $query1 = $this->db->get('gre_reward_point');
            $this->data['total_price']  = $query->row()->ord_total;
            $this->data['cash_collect'] = $query->row()->ord_det_cash_collect;
            $this->data['cash_debited'] = $query->row()->ord_det_wallet_cash_debited;
            $this->data['order_number'] = $query->row()->ord_order_number;
            $this->data['debit_reward_point'] = $query1->row()->debit_reward_point;
           
            //redirect('user_dashboard/my-orders');
        }    
        // Get products.
        $this->load->library('store');
        $products = $this->store->products(array(
            'random' => TRUE,
            'limit' => 12
        ));
        
        $this->data['products']            = $products['rows'];    
        $this->data['total']               = $products['total'];  
        $this->data['pagination']          = $products['pagination'];

        // Display to the user the success page.
        $this->load->view('public/cart/checkout_complete', $this->data);
    }
        /**
         * checkout_failed
         * Checkout could not be completed
         */ 
        function checkout_failed() 
        {
            $this->data['order_number'] = $this->input->get('token');

            // Get products.
            $this->load->library('store');
            
            $products = $this->store->products(array(
                'limit' => 12
            ));
            
            $this->data['products'] = $products['rows'];    
            $this->data['total'] = $products['total'];  
            $this->data['pagination'] = $products['pagination'];
            
            $this->load->view('public/cart/checkout_cancel', $this->data);
        }


    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###  
    // CUSTOM VALIDATION CALLBACKS
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

        /**
         * userfile_check
         * Custom validation for user uploaded files
         */ 
    function user_file_check($str)
    {
        $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['userfile']['name']);
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['name']!=""){
            if(!in_array($mime, $allowed_mime_type_arr)){
                $this->form_validation->set_message('user_file_check', 'Please select only gif/jpg/png file.');
                return false;
            }
        }else if($_FILES['userfile']['size'] > 1024){
            $this->form_validation->set_message('user_file_check', 'Please choose a file less than 1MB to upload.');
            return false;
        }else{
            return true;
        }
    }
    
    function user_email_check($email,$user_id){
        $query  = $this->db->get_where('users', array('email' => $email));
        if ($query->num_rows()){
            $query1  = $this->db->get_where('users', array('email' => $email,'id' => $user_id));
            if ($query1->num_rows()){log_message('debug',"asd3");
                return true;
            }
             $this->form_validation->set_message('user_email_check', 'Email is invalid or Already Exists.');
            return false;
        }
        else{
            return true;
        }
    }
    
     function user_phone_check($phone,$user_id){
        $query  = $this->db->get_where('users', array('phone' => $phone));
        if ($query->num_rows()){
            $query1  = $this->db->get_where('users', array('phone' => $phone,'id' => $user_id));
            if ($query1->num_rows()){
                return true;
            }
             $this->form_validation->set_message('user_phone_check', 'phone is invalid or Already Exists.');
            return false;
        }
        else{
            return true;
        }
    }
    
    /*
     * file value and type check during validation
     */
    public function product_file_check($str){//log_message('debug','asdfghj');
        $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['product_img']['name']);//log_message('debug',$_FILES['product_img']['name']);
        if(isset($_FILES['product_img']['name']) && $_FILES['product_img']['name']!=""){
            if(!in_array($mime, $allowed_mime_type_arr)){log_message('debug','hisingh2');
                $this->form_validation->set_message('product_file_check', 'Please select only gif/jpg/png file.');
                return false;
            }
            if($_FILES['product_img']['size'] > 5600000){
                $this->form_validation->set_message('product_file_check', 'Please choose a file less than 1MB to upload.');
                return false;
            }
            return true;
        }
        else{

            $this->form_validation->set_message('product_file_check', 'Please choose a file.');
            return false;
        }
    }
        
        /**
         * password_check
         * Custom validation for user password match
         */ 
        function password_check($old_password)
        {
            if ($this->ion_auth->hash_password_db($this->data['user']->id, $old_password))
            {
                return TRUE;
            }
            else
            {
                $this->form_validation->set_message('password_check', 'Sorry, the password did not match');
                return FALSE;
            }
        }

        /**
         * upload_image
         * Upload image to the server
         */
        private function upload_image( $filename = NULL, $oldfilename = NULL, $uploadPath = NULL)
        {log_message('debug','crop_height2');

            // Ensure that the upload directory exists
            if ( ! file_exists($uploadPath) )
            {
                if ( ! mkdir($uploadPath, 0777, true) )
                {
                    // Return an error if it should occur
                    return array(
                        "error" => 'create directory "assets/images/store" and retry'
                    );
                }
            }
            // Load the CI upload library
            $config['upload_path']      = $uploadPath;
            $config['allowed_types']    = 'gif|jpeg|jpg|png';
            $config['max_width']        = 0;
            $config['max_height']       = 0;
            $config['max_size']         = 5500;
            $config['remove_spaces']    = FALSE;
            $config['encrypt_name']     = FALSE;
            $config['overwrite']        = FALSE;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload($filename))
            {
                // Return an error if it should occur
                return array(
                    'error' => $this->upload->display_errors()
                );
            }
            else
            {
                // Remove the old logo.
                if ($oldfilename)
                {
                    if($oldfilename != "avtar_image.jpg")
                        unlink($uploadPath.'/'.$oldfilename);
                }

                // Get file properties data.
                $imageData = $this->upload->data();

                if ($this->input->post('crop_width') AND $this->input->post('crop_height'))
                {log_message('debug','crop_height');
                    // Crop Banner image.
                    $this->load->library('image_lib');
                    $config['source_image']   = $uploadPath.$imageData['file_name'];
                    $config['maintain_ratio'] = FALSE;
                    $config['width']    = $this->input->post('crop_width');
                    $config['height']   = $this->input->post('crop_height');
                    $config['x_axis']   = $this->input->post('crop_x');
                    $config['y_axis']   = $this->input->post('crop_y');

                    $this->image_lib->initialize($config); 

                    if ( ! $this->image_lib->crop())
                    {
                        echo $this->image_lib->display_errors();
                    }
                }


                // Resize the logo.
                $config['image_library']    = 'gd2';
                $config['source_image']     = $uploadPath.$imageData['file_name'];
                $config['file_name']        = $uploadPath.$imageData['file_name'];
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = TRUE;
                $config['width']            = 500;
                $config['height']           = 500;
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                ini_set ('gd.jpeg_ignore_warning', 1);
                $this->image_lib->resize($config);

                return array(
                    'error' => FALSE,
                    'path' => $imageData['file_name'],
                    'full_path' => $imageData['full_path'],
                    'file_path' => $imageData['file_path']
                );
            }
        }
}

/* End of file Front.php */
/* Location: ./application/controllers/Front.php */